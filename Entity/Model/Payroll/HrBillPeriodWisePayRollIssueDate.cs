﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class HrBillPeriodWisePayRollIssueDate : BaseEntity
    {
        public int PayRollId { get; set; }
        public int PeriodId { get; set; }
        //public int PeriodTypeId { get; set; }
        public DateTime? IssueDate { get; set; }
    }
}
