﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entity.Model
{
    public class AttendanceEmployee
    {
        public int EmpHeaderId { get; set; }
        public DateTime PunchDate { get; set; }

        [StringLength(50)]
        public string CardNumber { get; set; }

        [StringLength(50)]
        public string StartTime { get; set; }

        [StringLength(50)]
        public string EndTime { get; set; }

        [StringLength(50)]
        public string InTime { get; set; }

        [StringLength(50)]
        public string OutTime { get; set; }

        public int? TimeLate { get; set; }

        public int? EarlyLeave { get; set; }

        public int? DoorNumber { get; set; }
    }
}
