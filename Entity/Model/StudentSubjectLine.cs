﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class StudentSubjectLine : BaseEntity
    {
        [Key]
        public int StudentSubjectLineId { get; set; }
        public int StudentHeaderId { get; set; }
        public int? CompulsorySubjectId { get; set; }
        public int? OptionalSubjectId { get; set; }
        public int StudentSubjectMasterId { get; set; }
        public StudentSubjectMaster StudentSubjectMaster { get; set; }
        public List<StudentWiseSubject> StudentWiseSubjects { get; set; }
        public virtual Student Student { get; set; }
    }
}
