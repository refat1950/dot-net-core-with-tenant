﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Mail;
using Business;
using Entity.Model;
using Repository.Context;
using Services.View_Model;
using data.UnitOfWork;
using Fluentx;

namespace Services
{
    public class AttendanceService
    {
        public static int EmployeeTotalWorkingDay = 30;
        public static int PrimaryShiftTypeId = 5123;
        public static int RosterShiftTypeId = 5124;
        private const int ApprovedStatusFlex = 1;
        private IUnitOfWork _unitOfWork;
  

        public AttendanceService( IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        #region Student
        public void DailyAttendanceProcess()
        {
            var punchDate = DateTime.UtcNow.Date;//DateTime.ParseExact("2016-10-02", "yyyy-MM-dd", null);//
            DailyAttendanceProcess(punchDate);
        }

        public void AttendanceProcessOnDate(DateTime processDate, int classId = 0, int sectionId = 0)
        {
            DailyAttendanceProcess(processDate, classId, sectionId);
        }

        public void AttendanceProcessbetweenDates(DateTime startDate, DateTime endDate)
        {
            for (var date = startDate; date.Date <= endDate.Date; date = date.AddDays(1))
            {
                DailyAttendanceProcess(date);
            }
        }

        public bool DailyAttendanceProcess(DateTime punchDate, int classId = 0, int sectionId = 0)
        {
            //var _unitOfWork = new ProjectDbContext();
            try
            {
                var currentSession = _unitOfWork.SessionRepo.FirstOrDefault(f => f.Status == true);

                if (currentSession != null)
                {
                    var holidayShortCode = new[] { "HOLI", "EXAM", "SCRIC", "Report card" };

                    var isRunToday = IsRunToday(punchDate, holidayShortCode);

                    var classSection = _unitOfWork.ClassSectionRepo;

                    if (isRunToday)
                    {
                        var allStudent = _unitOfWork.StudentRepo.Find(w => w.Status == true && w.ClassId < 15);

                        if (classId != 0)
                        {
                            allStudent = allStudent.Where(w => w.ClassId == classId);
                        }
                        if (sectionId != 0)
                        {
                            allStudent = allStudent.Where(w => w.SectionId == sectionId);
                        }

                        var studentIds = allStudent.Select(s => s.StudentHeaderId).ToArray();

                        var todaysProcessedAttendance = _unitOfWork.AttendanceStudentRepo.Find(w => w.PunchDate == punchDate && studentIds.Contains(w.StudentHeaderId));

                        var isUpdate = todaysProcessedAttendance.Any();

                        var allAttendance = _unitOfWork.AttendancesRepo.Find(w => w.PunchDate == punchDate);
                        var processedAttendance = allStudent.AsEnumerable().Select(s =>
                        {
                            var bssClassSection = classSection.FirstOrDefault(f => f.ClassId == s.ClassId && f.SectionId == s.SectionId && f.CampusId == s.CampusId);
                            var studentPunchs = allAttendance.Where(w => w.CardNumber == s.ProximityNum);

                            string inTimeSTring = null;
                            string outTimeSTring = null;
                            int? earlyLeave = null;
                            int? timeLate = null;
                            int? doorNumber = null;

                            if (bssClassSection != null && studentPunchs.Any())
                            {
                                var punchTimes = studentPunchs.AsEnumerable().Select(p => TimeSpan.Parse(p.PunchTime)).ToArray();
                                var inTime = punchTimes.Min();
                                var outTime = punchTimes.Max();


                                var endTime = TimeSpan.Parse(bssClassSection.EndTime);
                                earlyLeave = (int)(endTime - outTime).TotalMinutes;

                                var startTime = TimeSpan.Parse(bssClassSection.StartTime);
                                timeLate = (int)(inTime - startTime).TotalMinutes;

                                inTimeSTring = inTime.ToString(@"hh\:mm");
                                outTimeSTring = outTime.ToString(@"hh\:mm");

                                var firstPunch = studentPunchs.FirstOrDefault();
                                if (firstPunch != null) doorNumber = firstPunch.DoorNumber;
                            }

                            return bssClassSection != null ? new AttendanceStudent
                            {
                                StudentHeaderId = s.StudentHeaderId,
                                CardNumber = s.ProximityNum,
                                StartTime = bssClassSection.StartTime,
                                EndTime = bssClassSection.EndTime,
                                InTime = inTimeSTring,
                                OutTime = outTimeSTring,
                                EarlyLeave = earlyLeave,
                                TimeLate = timeLate,
                                DoorNumber = doorNumber,
                                PunchDate = punchDate
                            } : null;
                        })
                        .Where(w => w != null)
                        .ToList();

                        if (isUpdate)
                        {
                            todaysProcessedAttendance.ForEach(f =>
                            {
                                var bssAttendanceStudent = processedAttendance.FirstOrDefault(ff => ff.StudentHeaderId == f.StudentHeaderId);
                                if (bssAttendanceStudent != null)
                                {
                                    f.StartTime = bssAttendanceStudent.StartTime;
                                    f.OutTime = bssAttendanceStudent.OutTime;
                                    f.InTime = bssAttendanceStudent.InTime;
                                    f.OutTime = bssAttendanceStudent.OutTime;
                                    f.EarlyLeave = bssAttendanceStudent.EarlyLeave;
                                    f.TimeLate = bssAttendanceStudent.TimeLate;
                                }
                            });

                            var updatedStudentsId = todaysProcessedAttendance.Select(s => s.StudentHeaderId).ToArray();
                            var notUpdatedStudentsId = processedAttendance.Where(w => !updatedStudentsId.Contains(w.StudentHeaderId));
                            _unitOfWork.AttendanceStudentRepo.AddRange(notUpdatedStudentsId);

                        }
                        else
                        {
                            _unitOfWork.AttendanceStudentRepo.AddRange(processedAttendance);
                        }
                        _unitOfWork.SaveChanges();
                    }

                }
                _unitOfWork.Dispose();
                return true;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                var time = DateTime.UtcNow;


                var mailMessage = "At : " + time + Environment.NewLine + "Message : " + msg + Environment.NewLine + "Detail : " + ex;

                var bssSchool = _unitOfWork.SchoolRepo.FirstOrDefault();

                if (bssSchool != null)
                    SendDone("Student AttendanceVm Daily Process Error", mailMessage, true);


                _unitOfWork.Dispose();
                return false;
            }

        }
        #endregion

        public bool SendEmail(string toAddress, string subject, string body, string ccAddress = "")
        {
            try
            {
                //var _unitOfWork = new ProjectDbContext();
                var bssSchool = _unitOfWork.SchoolRepo.FirstOrDefault();
                if (bssSchool != null)
                {
                    var fromAddress = bssSchool.SchoolInquiryMail;
                    var senderName = bssSchool.SchoolInquiryMail;
                    var userName = bssSchool.SchoolInquiryMail;
                    var password = bssSchool.SchoolInquiryMailPass;

                    //var toAddresses = bssSchool.SchoolEMail.Split(',').Last();
                    //const string ccAddress = "";

                    var client = new SmtpClient("smtp.gmail.com", 587)
                    {
                        EnableSsl = true,
                        Credentials = new NetworkCredential(userName, password)
                    };
                    var from = new MailAddress(fromAddress, senderName);
                    var to = new MailAddress(toAddress);
                    var message = new MailMessage(@from, to);

                    if (ccAddress.Trim() != "")
                    {
                        var strArray = ccAddress.Trim().Split(';');
                        foreach (var str in strArray)
                        {
                            message.CC.Add(str.Trim());
                        }
                    }

                    message.Subject = subject;
                    message.IsBodyHtml = true;
                    message.Body = body;

                    client.Send(message);
                    return true;
                }
            }
            catch (Exception)
            {
                // ignored
            }

            return false;
        }

        /// <summary>
        /// Send mail
        /// [Attribute1] -> daily/monthly process mail
        /// [Attribute2] -> Error mail
        /// </summary>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        /// <param name="isError"></param>
        /// <returns></returns>
        public bool SendDone(string subject, string body, bool isError)
        {
            try
            {
                //var _unitOfWork = new ProjectDbContext();
                var bssSchool = _unitOfWork.SchoolRepo.FirstOrDefault();
                if (bssSchool != null)
                {
                    var fromAddress = bssSchool.SchoolInquiryMail;
                    var senderName = bssSchool.SchoolInquiryMail;
                    var userName = bssSchool.SchoolInquiryMail;
                    var password = bssSchool.SchoolInquiryMailPass;

                    var allAddress = !isError ? bssSchool.Attribute1.Split(',') : bssSchool.Attribute2.Split(',');
                    var toAddresses = allAddress.FirstOrDefault();
                    string ccAddress = string.Join(",", allAddress.Skip(1).ToArray());

                    if (toAddresses != null)
                    {
                        var client = new SmtpClient("smtp.gmail.com", 587)
                        {
                            EnableSsl = true,
                            Credentials = new NetworkCredential(userName, password)
                        };
                        var from = new MailAddress(fromAddress, senderName);
                        var to = new MailAddress(toAddresses);
                        var message = new MailMessage(@from, to);

                        if (ccAddress.Trim() != "")
                        {
                            var strArray = ccAddress.Trim().Split(';');
                            foreach (var str in strArray)
                            {
                                message.CC.Add(str.Trim());
                            }
                        }

                        message.Subject = subject;
                        message.IsBodyHtml = true;
                        message.Body = body;
                        try
                        {
                            client.Send(message);
                            return true;
                        }
                        catch
                        {
                            // ignored
                        }
                    }
                }
            }
            catch (Exception)
            {
                //var msg = "At : " + DateTime.UtcNow.AddHours(6) + "<br/>";
                //msg += ex.Message + "<br/>";
                //msg += ex.ToString();

                //SendEmail("nabil@bssitbd.com", "Process Error", msg);
            }
            return true;
        }


        private bool IsRunToday(DateTime punchDate, string[] holidayShortCode, bool isSaturdayCount = false)
        {
            //var _unitOfWork = new ProjectDbContext();
            var isRunToday = false;

            var currentSession = _unitOfWork.SessionRepo.FirstOrDefault(f => f.Status == true);

            if (currentSession != null)
            {
                var currentSessionId = currentSession.SessionId;

                var todayName = punchDate.ToString("dddd").ToUpper();

                var processDays = _unitOfWork.PlannerDayRepo.Find(w => w.IsWeeklyHoliday == false).ToList();

                var holidaysEventTypes = _unitOfWork.PlannerEventTypeRepo.Find(w => holidayShortCode.Contains(w.ShortName));
                var holidaysEventTypesIds = holidaysEventTypes.Select(s => s.EventTypeId).ToArray();

                var holidayEvents = _unitOfWork.PlannerEventRepo.Find(w => holidaysEventTypesIds.Contains(w.EventTypeId.Value));
                var holidayEventsIds = holidayEvents.Select(s => s.EventId).ToArray();

                var holidayEventDetls = _unitOfWork.PlannerEventDetailsRepo.Find(w => w.SessionId == currentSessionId && holidayEventsIds.Contains(w.EventId));

                var isWorkingDay = processDays.Any(a => a.DayName == todayName);

                var isPlannerHoliday = holidayEventDetls.Any(a => a.StartDate <= punchDate && punchDate <= a.EndDate);

                isRunToday = (isWorkingDay || isSaturdayCount) && !isPlannerHoliday;
            }
            return isRunToday;
        }

        public string GenProcessEmail(DateTime punchDate, int rowInserted, int rowUpdated)
        {
            var mailMessage = "At : " + punchDate + "<br/>";
            mailMessage += "Process Name : Daily Employee AttendanceVm Process <br/>";
            mailMessage += "Status: Successful <br/>";
            mailMessage += "Row Inserted: " + rowInserted + "<br/>";
            mailMessage += "Row Updated: " + rowUpdated;
            return mailMessage;
        }

        public void DailyEmployeeAttendanceProcess()
        {
            var punchDate = DateTime.UtcNow.AddHours(6).Date;
            var i = EmployeeAttendancePrecess(punchDate);

            //var _unitOfWork = new ProjectDbContext();
            var mailMessage = GenProcessEmail(punchDate, i.RowInserted, i.RowUpdated);

            var bssSchool = _unitOfWork.SchoolRepo.FirstOrDefault();

            if (bssSchool != null)
                //SendEmail("shopon@bssitbd.com", "Employee AttendanceVm Process Done", mailMessage);
                SendDone("Employee AttendanceVm Process Done", mailMessage, false);
        }

        public void MonthlyEmployeeAttendanceProcess(string month)
        {
            var rowInserted = 0;
            var rowUpdated = 0;
            var startDate = DateTime.ParseExact(month, "MMMyy", null);
            var endDate = startDate.AddMonths(1).AddDays(-1);

            if (endDate > DateTime.Today)
            {
                endDate = DateTime.Today;
            }

            for (var date = startDate; date <= endDate; date = date.AddDays(1))
            {
                var i = EmployeeAttendancePrecess(date);
                rowInserted += i.RowInserted;
                rowUpdated += i.RowUpdated;
            }
            //var _unitOfWork = new ProjectDbContext();
            var bssSchool = _unitOfWork.SchoolRepo.FirstOrDefault();
            if (bssSchool != null)
            {
                var mailMessage = GenProcessEmail(DateTime.UtcNow.AddHours(6), rowInserted, rowUpdated);
                //SendEmail("nabil@bssitbd.com", "Employee AttendanceVm Daily Process Done", mailMessage);
                SendDone("Employee AttendanceVm Daily Process Done", mailMessage, false);
            }
        }

        public ProcessStatus EmployeeAttendancePrecess(DateTime punchDate)
        {
            //var _unitOfWork = new ProjectDbContext();
            var processStatus = new ProcessStatus
            {
                RowInserted = 0,
                RowUpdated = 0
            };
            try
            {
                var todayName = punchDate.ToString("dddd").ToUpper();

                TimeSpan starttime;
                TimeSpan endtime;

                var holidayShortCode = new[] { "HOLI" };
                var isRunToday = IsRunToday(punchDate, holidayShortCode, true);
                if (!isRunToday)
                {
                    return processStatus;
                }

                var excertionShift = _unitOfWork.HrExcepShiftRepo.Find(w => w.ShiftDate == punchDate);

                if (punchDate.DayOfWeek != DayOfWeek.Friday || excertionShift.Any())
                {
                    var empShiftSetup = GetShiftSetup(_unitOfWork, todayName, punchDate).ToList();

                    var empShiftIds = empShiftSetup.Select(s => s.EmpHeaderId).ToArray();

                    var leaveEmpIds = _unitOfWork.LeaveRepo.Find(w => w.FromDate <= punchDate && punchDate <= w.ToDate)
                                                    .Select(s => s.ReferenceId)
                                                    .ToArray();

                    var allAttendance = _unitOfWork.AttendancesRepo.Find(w => w.PunchDate == punchDate);

                    var allEmp = _unitOfWork.EmployeeRepo
                                    //.Include(i => i.HrExcepShifts)
                                    .Find(w => empShiftIds.Contains(w.EmpHeaderId) || excertionShift.Any(a => a.EmpHeaderId == w.EmpHeaderId))
                                    .Where(w => (w.Status == true && !string.IsNullOrEmpty(w.PunchCardId) && !leaveEmpIds.Contains(w.EmpHeaderId)) ||
                                                (w.Status == false && w.JobEndDate >= punchDate));

                    if (punchDate.DayOfWeek == DayOfWeek.Friday && excertionShift.Any())
                    {
                        var excertionShiftEmpIds = excertionShift.Select(s => s.EmpHeaderId).ToArray();
                        allEmp = allEmp.Where(w => excertionShiftEmpIds.Contains(w.EmpHeaderId));
                    }


                    var fixPunch = _unitOfWork.HrAttendenceFixRepo.Find(w => w.Status == ApprovedStatusFlex && w.PunchDate == punchDate && w.ApprovedBy != null);

                    var todayAllPunch = allEmp.AsEnumerable().Select(s =>
                    {
                        var shifts = empShiftSetup.Where(w => w.EmpHeaderId == s.EmpHeaderId && w.StartDate <= punchDate && (w.EndDate == null || punchDate <= w.EndDate)).ToList();

                        var empPunchs = allAttendance.Where(w => w.CardNumber == s.PunchCardId).ToList();

                        var empFixPunch = fixPunch.OrderByDescending(o => o.FixId).FirstOrDefault(w => w.CardNumber == s.PunchCardId);
                        var inTime = new TimeSpan();
                        var outTime = new TimeSpan();

                        if (empFixPunch != null)
                        {
                            var a = TimeSpan.TryParse(empFixPunch.PunchTime, out inTime);
                            var b = TimeSpan.TryParse(empFixPunch.OutTime, out outTime);
                        }
                        else if (empPunchs.Any())
                        {
                            var punchTimes = empPunchs.AsEnumerable().Select(p => TimeSpan.Parse(p.PunchTime)).ToArray();
                            inTime = punchTimes.Min();
                            outTime = punchTimes.Max();
                        }

                        var exepShift = s.HrExcepShifts.FirstOrDefault(f => f.ShiftDate == punchDate);
                        if (exepShift == null)
                        {
                            var finalShift = GetValidShift(shifts, todayName, s.EmpHeaderId, punchDate);

                            starttime = finalShift.StartTime;
                            endtime = finalShift.EndTime;
                        }
                        else
                        {
                            starttime = exepShift.ShiftStartTime.ToTimeSpan();
                            endtime = exepShift.ShiftEndTime.ToTimeSpan();
                        }

                        var timeLate = starttime.IsZero() || inTime.IsZero() ? null : (int?)(inTime - starttime).TotalMinutes;
                        var earlyLeave = endtime.IsZero() || outTime.IsZero() ? null : (int?)(endtime - outTime).TotalMinutes;
                        var inTimeSTring = !inTime.IsZero() ? TimeSpanHelper.ToString(inTime) : null;
                        var outTimeSTring = !outTime.IsZero() ? TimeSpanHelper.ToString(outTime) : null;

                        int? doorNumber = null;
                        var firstPunch = empPunchs.FirstOrDefault();
                        if (firstPunch != null) doorNumber = firstPunch.DoorNumber;

                        // inTimeSTring null -> no punch ->absent
                        return new AttendanceEmployee
                        {
                            EmpHeaderId = s.EmpHeaderId,
                            PunchDate = punchDate,
                            CardNumber = s.PunchCardId,
                            InTime = inTimeSTring,
                            OutTime = outTimeSTring,
                            EarlyLeave = earlyLeave,
                            TimeLate = timeLate,
                            StartTime = TimeSpanHelper.ToString(starttime),
                            EndTime = TimeSpanHelper.ToString(endtime),
                            DoorNumber = doorNumber
                        };
                    });

                    var notEmplyPunch = todayAllPunch.ToList();//.Where(w => !(w.StartTime == "00:00" && w.InTime == null)).ToList();



                    foreach (var punch in notEmplyPunch)
                    {
                        _unitOfWork.AttendanceEmployeeRepo.Update(punch);
                    }
                    foreach (var hrAttendanceFix in fixPunch)
                    {
                        hrAttendanceFix.IsProcessed = true;
                    }

                    if (leaveEmpIds.Count() > 0)
                    {
                        var empOnLeave = _unitOfWork.AttendanceEmployeeRepo.Find(w => leaveEmpIds.Contains(w.EmpHeaderId) && w.PunchDate == punchDate);
                        if (empOnLeave.Any())
                        {
                            _unitOfWork.AttendanceEmployeeRepo.RemoveRange(empOnLeave);
                        }
                    }

                    var previousCount = _unitOfWork.AttendanceEmployeeRepo.GetAll().Count(c => c.PunchDate == punchDate);
                    var newCount = notEmplyPunch.Count();
                    processStatus.RowInserted = newCount - previousCount;
                    processStatus.RowUpdated = previousCount;

                    _unitOfWork.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                var mailMessage = "At : " + punchDate + "<br/>"
                    + "Process Date : " + punchDate.ToString("dd-MMM-yy") + "<br/>"
                    + "Message : " + msg + "<br/>"
                    + "Detail : " + ex;

                var bssSchool = _unitOfWork.SchoolRepo.FirstOrDefault();

                if (bssSchool != null)
                    SendDone("AttendanceVm Process Error", mailMessage, true);
                _unitOfWork.Dispose();
            }
            finally
            {
                _unitOfWork.Dispose();
            }
            return processStatus;
        }

        private IEnumerable<HrEmployeeShift> GetShiftSetup(IUnitOfWork _unitOfWork, string todayName, DateTime punchDate)
        {
            var todaysShifts = _unitOfWork.HrShiftRepo.Find(w => w.DayOfWeek.ToUpper().Contains(todayName));
            var todaysShiftsIds = todaysShifts.Select(s => s.ShiftId).ToArray();

            var empShiftSetup = _unitOfWork.HrEmployeeShiftRepo
                                    .Find(w => w.Status &&
                                                todaysShiftsIds.Contains(w.ShiftId) &&
                                                w.StartDate <= punchDate &&
                                                (w.EndDate == null || punchDate <= w.EndDate));

            return empShiftSetup;
        }

        public ShiftTimes GetValidShift(List<HrEmployeeShift> hrShifts, string dayName, int empId, DateTime punchDate)
        {
            var primaryShift = hrShifts.FirstOrDefault(w => w.ShiftType == PrimaryShiftTypeId);
            var rosterShift = hrShifts.FirstOrDefault(w => w.ShiftType == RosterShiftTypeId);

            if (rosterShift == null && primaryShift != null)
            {
                return SetupToTimes(primaryShift);
            }
            if (rosterShift != null)
            {
                var weekOfMonth = rosterShift.HrShift.WeekOfMonth;
                var dayOfMonth = rosterShift.HrShift.DayOfMonth;

                if (!string.IsNullOrEmpty(weekOfMonth) && !string.IsNullOrWhiteSpace(weekOfMonth))//first check weeek of day
                {
                    var weekNumber = punchDate.GetWeekOfMonth();
                    var weekString = weekNumber.ToString();

                    var isInWeek = weekOfMonth.Contains(weekString) || IsInLastWeek(punchDate, weekNumber, weekOfMonth);
                    if (isInWeek)
                    {
                        return SetupToTimes(rosterShift);
                    }
                }
                else if (!string.IsNullOrEmpty(dayOfMonth) && !string.IsNullOrWhiteSpace(dayOfMonth))
                {
                    var isInDate = dayOfMonth.Contains(punchDate.Day.ToString());
                    if (isInDate)
                    {
                        return SetupToTimes(rosterShift);
                    }
                }
                else
                {
                    return SetupToTimes(rosterShift);
                }
            }
            return SetupToTimes(primaryShift);
        }

        private bool IsInLastWeek(DateTime punchDate, int weekNumber, string weekOfMonth)
        {
            if (weekOfMonth.ToUpper().Contains("LAST"))
            {
                var lastWeekNumber = punchDate.AddMonths(1).AddDays(-1 * punchDate.Day).GetWeekOfMonth();
                if (weekNumber == lastWeekNumber) return true; // 5th week alawys last ..
            }
            return false;
        }

        private ShiftTimes SetupToTimes(HrEmployeeShift hrEmployeeShift)
        {
            if (hrEmployeeShift == null)
            {
                return new ShiftTimes();
            }


            return new ShiftTimes
            {
                StartTime = hrEmployeeShift.HrShift.ShiftStartTime.ToTimeSpan(),
                EndTime = hrEmployeeShift.HrShift.ShiftEndTime.ToTimeSpan()
            };
        }

        public IEnumerable<MonthlyAttendance> AttendanceReport(int[] empList, string month)
        {
            /* const string usCulture = "en-US";
             //var _unitOfWork = new ProjectDbContext();
             var tmpLeave = _unitOfWork.TempLeaveRepo.GetAll();

             var mangmentIds = new[] { "0707060018", "0510080008", "0501150003", "0501150002", "0501150001", "0501150004", "1407010596", "1401270550" };
             var mangmentHeaderIds = _unitOfWork.EmployeeRepo.Find(w => mangmentIds.Contains(w.EmpId)).Select(s => s.EmpHeaderId).ToArray();

             var allPolicy = _unitOfWork.AttendancePolicyRepo;

             var startDate = DateTime.ParseExact(month, "MMMyy", new CultureInfo(usCulture, false));
             var endDate = startDate.AddMonths(1).AddDays(-1);
             int workingDay; //GetBusinessDays(startDate, endDate);

             //var leaveList = _unitOfWork.Leave.Where(w => empList.Contains(w.ReferenceId.Value) && startDate <= w.FromDate && w.FromDate <= endDate);

             var monthlyAttendance = _unitOfWork.AttendanceEmployeeRepo
                         .Find(w => empList.Contains(w.EmpHeaderId) && startDate <= w.PunchDate && w.PunchDate <= endDate)//.Join(_unitOfWork.HrEmployee, atten => atten.Empheaderid, emp => emp.Empheaderid, (atten , emp) => new { atten, emp })
                         .Where(w => !mangmentHeaderIds.Contains(w.EmpHeaderId))
                         .GroupBy(g => g.EmpHeaderId)
                         .AsEnumerable()
                         .Select(s =>
                         {
                             var remarks = "";
                             workingDay = EmployeeTotalWorkingDay;

                             var emp = (from empl in _unitOfWork.EmployeeRepo
                                        join fle in _unitOfWork.FlexValueRepo on empl.DesignationId equals fle.FlexValueId into gj
                                        from flex in gj.DefaultIfEmpty()
                                        where empl.EmpHeaderId == s.Key
                                        select new { empl, flex }).FirstOrDefault();
                             if (emp == null || emp.empl == null)
                             {
                                 return null;
                             }

                             DateTime rejoinDate;
                             DateTime.TryParseExact(emp.empl.Attribute1, "dd-MMM-yy", CultureInfo.InvariantCulture, DateTimeStyles.None, out rejoinDate);

                             if (emp.empl.JoinningDate > startDate || (rejoinDate.Year != 1 && rejoinDate > startDate))
                             {
                                 var joinDate = new DateTime();
                                 if (rejoinDate >= startDate)
                                 {
                                     joinDate = rejoinDate;
                                 }
                                 else if (emp.empl.JoinningDate != null)
                                 {
                                     joinDate = emp.empl.JoinningDate;
                                 }
                                 var dayCount = joinDate.Year != 1 ? workingDay - joinDate.Day : 0;
                                 workingDay = dayCount + 1;
                                 remarks += "Joined on " + joinDate.ToString("dd-MM-yy");
                             }
                             if (emp.empl.Status == false && (emp.empl.JobEndDate > startDate && emp.empl.JobEndDate <= endDate))
                             {
                                 if (emp.empl.JobEndDate != null)
                                 {
                                     var dayCount = workingDay == EmployeeTotalWorkingDay ?
                                                     (int)(emp.empl.JobEndDate - startDate).Value.TotalDays :
                                                     (int)(emp.empl.JobEndDate - emp.empl.JoinningDate).Value.TotalDays;
                                     workingDay = dayCount;
                                     remarks = string.IsNullOrEmpty(remarks) ? "" : " ";
                                     remarks += "Left Job on " + emp.empl.JobEndDate.Value.ToString("dd-MM-yy");
                                 }
                             }

                             var empPlicy = allPolicy.FirstOrDefault(w => w.DesigId == emp.empl.DesignationId && w.DeptId == emp.empl.DepartmentId);

                             var empLatePolicy = empPlicy != null ? empPlicy.MaxLate : 0;
                             var empEarlyLeavePolicy = empPlicy != null ? empPlicy.MaxEarly : 0;

                             var empLeaveTmp = tmpLeave.LastOrDefault(f => f.Empheaderid == s.Key);

                             var empLeave = empLeaveTmp != null ? empLeaveTmp.NoOfLeave : 0; //s.Count(w => w.InTime == null);//EmpLeaveCount(leaveList.Where(w => w.ReferenceId == emp.empl.Empheaderid), endDate);
                             var absentDateDay = empLeaveTmp != null ? empLeaveTmp.LeaveDays : "";//EmpLeaveDays(s.ToList());
                             var leaveWoutPay = 0;

                             if (empLeaveTmp != null) int.TryParse(empLeaveTmp.Attribute1, out leaveWoutPay);

                             var earlyLeave = s.Count(c => c.EarlyLeave > 0);
                             var late = s.Count(c => c.TimeLate > 0);

                             var deductionLate = empLatePolicy != null && empLatePolicy != 0 ? late / empLatePolicy.Value : 0;
                             var deductionEarlyLeave = empEarlyLeavePolicy != null && empEarlyLeavePolicy != 0 ? earlyLeave / empEarlyLeavePolicy.Value : 0;
                             return new MonthlyAttendance
                             {
                                 Wd = workingDay,
                                 EmployeId = emp.empl.EmpId,
                                 Name = (emp.empl.EmpFirstName + " " + emp.empl.EmpMiddleName + " " + emp.empl.EmpLastName).Replace("  ", " "),
                                 Designation = emp.flex != null ? emp.flex.FlexValue : "",
                                 EarlyLeave = earlyLeave,
                                 Late = late,
                                 Leave = empLeave,
                                 AbsentDateDay = absentDateDay,
                                 DeductionEarlyLeave = deductionEarlyLeave,
                                 DeductionLate = deductionLate,
                                 Totaldeduction = deductionEarlyLeave + deductionLate + leaveWoutPay,
                                 TatalPay = workingDay - (deductionEarlyLeave + deductionLate + leaveWoutPay),
                                 Remarks = remarks,
                                 LeaveWoutPay = leaveWoutPay
                             };
                         })
                         .Where(w => w != null)
                         .OrderBy(o => o.Name)
                         .ToList();*/
            return new List<MonthlyAttendance>();
        }

        public int GetBusinessDays(DateTime startD, DateTime endD)
        {
            var calcBusinessDays = 1 + ((endD - startD).TotalDays * 5 - (startD.DayOfWeek - endD.DayOfWeek) * 2) / 7;

            if (endD.DayOfWeek == DayOfWeek.Saturday) calcBusinessDays--;
            if (startD.DayOfWeek == DayOfWeek.Friday) calcBusinessDays--;

            return (int)calcBusinessDays;
        }

        public int EmpLeaveCount(IEnumerable<Leave> empLeave, DateTime monthEndDate)
        {
            var count = 0;
            foreach (var bssLeaf in empLeave)
            {
                if (bssLeaf.FromDate != null && bssLeaf.ToDate != null)
                    count = GetBusinessDays(bssLeaf.FromDate.Value, bssLeaf.ToDate <= monthEndDate ? bssLeaf.ToDate.Value : monthEndDate);
            }
            return count;
        }

        public string EmpLeaveDays(IEnumerable<Leave> empLeave, DateTime monthEndDate)
        {
            var leaveString = string.Empty;
            foreach (var bssLeaf in empLeave)
            {
                if (bssLeaf.FromDate != null && bssLeaf.ToDate != null)
                    leaveString += GetActiveDayNames(bssLeaf.FromDate.Value, bssLeaf.ToDate <= monthEndDate ? bssLeaf.ToDate.Value : monthEndDate);
            }

            return leaveString;
        }

        public string EmpLeaveDays(IEnumerable<AttendanceEmployee> empLeave)
        {
            var leaveString = string.Empty;
            foreach (var bssLeaf in empLeave.Where(w => w.InTime == null))
            {
                leaveString += bssLeaf.PunchDate.ToString("dd") + ", ";
            }

            return leaveString;
        }

        public string GetActiveDayNames(DateTime startD, DateTime endD)
        {
            var dayString = string.Empty;

            for (var date = startD; date <= endD; date = date.AddDays(1))
                dayString += date.ToString("dd") + ", ";

            return dayString;
        }

        public IEnumerable<EmpWiseAttendance> GetEmployeeAttendance(int empHeaderHeadId, DateTime stratDate, DateTime endDate)
        {
            // var _unitOfWork = new ProjectDbContext();
            var allAtt = _unitOfWork.AttendanceEmployeeRepo.Find(w => w.EmpHeaderId == empHeaderHeadId && w.PunchDate >= stratDate && w.PunchDate <= endDate);
            var allValues = allAtt
                .AsEnumerable()
                .Select(s => new EmpWiseAttendance
                {
                    SHIFT_TIME = s.StartTime + "-" + s.EndTime,
                    OFFICE_TIME = s.PunchDate,
                    IN_TIME = s.InTime,
                    OUT_TIME = s.OutTime,
                    EARLY_LEAVE = s.EarlyLeave,
                    LATE_TIME = s.TimeLate != null && s.TimeLate > 0 ? DateTimeExt.MinuteTohour(s.TimeLate.Value) : "",
                    ATTENDANCE_CODE = s.InTime == null ? "Absent" : (s.TimeLate > 0 ? "Late" : (s.EarlyLeave > 0 ? "Early Leave" : "Present"))
                }).ToList();
            return allValues;
        }

        public Tuple<int, List<RequestedAttendance>> RequestedAttendance(string requestType, string month, long?[] hrRefIds)
        {
            //var _unitOfWork = new ProjectDbContext();
            /*var today = DateTime.Now;
            const int pendingStatusFlex = 0;

            var monthInt = month.Equals("CM") ? today.Month : (today.Month - 1);
            var allValues = _unitOfWork.HrAttendenceFixRepo.Find(w => w.PunchDate.Month == monthInt);

            switch (requestType)
            {
                case "RBU":
                    {
                        allValues = allValues.Where(w => !hrRefIds.Contains(w.CreatedBy) && w.Status != pendingStatusFlex);
                        break;
                    }
                case "ABH":
                    {
                        allValues = allValues.Where(w => hrRefIds.Contains(w.ApprovedBy) && w.Status != pendingStatusFlex);
                        break;
                    }
                case "UBH":
                    {
                        allValues = allValues.Where(w => hrRefIds.Contains(w.CreatedBy) && w.Status != pendingStatusFlex);
                        break;
                    }
            }
            //var allEmployeeIds = allValues.Select(s => s.Empheaderid).ToList();
            //var allEmployee = _unitOfWork.HrEmployee.Where(w => allEmployeeIds.Contains(w.Empheaderid)).ToList();

            var result = allValues
                        .Join(_unitOfWork.EmployeeRepo, fix => fix.EmpHeaderId, emp => emp.EmpHeaderId, (fix, emp) => new { fix, emp })
                        .Select(s => new RequestedAttendance
                        {
                            Name = s.emp.EmpFirstName + " " + s.emp.EmpLastName,
                            EmpId = s.emp.EmpId,
                            PunchDate = s.fix.PunchDate,
                            PunchTime = s.fix.PunchTime,
                            OutTime = s.fix.OutTime,
                        })
                        .ToList();
            var count = result.Count();
            return Tuple.Create(count, result);*/
            return Tuple.Create(5, new List<RequestedAttendance>());
        }
    }

    public class ShiftTimes
    {
        public TimeSpan StartTime { get; set; }
        public TimeSpan EndTime { get; set; }
    }

    public static class TimeSpanHelper
    {
        public static TimeSpan ToTimeSpan(this string timeString)
        {
            var dt = DateTime.Parse(timeString);//, @"hh\:mm", null);
            return dt.TimeOfDay;
        }

        public static string ToString(this TimeSpan time)
        {
            if (time.Hours == 0 && time.Minutes == 0)
            {
                return "00:00";
            }
            else
            {
                return time.ToString(@"hh\:mm");
            }
        }
    }

    public static class DateTimeExtensions
    {
        static readonly GregorianCalendar Gc = new GregorianCalendar();
        public static DayOfWeek FirstDayOfWeek = DayOfWeek.Saturday;

        public static int GetWeekOfMonth(this DateTime time)
        {
            var first = new DateTime(time.Year, time.Month, 1);
            return time.GetWeekOfYear() - first.GetWeekOfYear() + 1;
        }

        static int GetWeekOfYear(this DateTime time)
        {
            return Gc.GetWeekOfYear(time, CalendarWeekRule.FirstDay, FirstDayOfWeek);
        }

        public static bool IsZero(this TimeSpan thisTime)
        {
            return thisTime.ToString() == TimeSpan.Zero.ToString();
        }
    }

    public class MonthlyAttendance
    {
        public string Sl { get; set; }
        public string EmployeId { get; set; }
        public string Name { get; set; }
        public string Designation { get; set; }
        public int Wd { get; set; }
        public int Late { get; set; }
        public int EarlyLeave { get; set; }
        public int Leave { get; set; }
        public string AbsentDateDay { get; set; }
        public int DeductionLate { get; set; }
        public int DeductionEarlyLeave { get; set; }
        public int LeaveWoutPay { get; set; }
        public int Totaldeduction { get; set; }
        public int TatalPay { get; set; }
        public string Remarks { get; set; }
    }

    public class ProcessStatus
    {
        public int RowInserted { get; set; }
        public int RowUpdated { get; set; }
    }

    public class RequestedAttendance
    {
        public string Name { get; set; }
        public string EmpId { get; set; }
        public DateTime PunchDate { get; set; }
        public string PunchTime { get; set; }
        public string OutTime { get; set; }
    }
}
