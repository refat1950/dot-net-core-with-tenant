﻿using data.UnitOfWork;
using Entity.Model;
using Repository.Context;
using Service.Helper;
using Service.Service.Interfaces.Payroll;
using Service.Services.Interfaces;
using Service.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Services.Payroll
{
    public class AnnualPeriodGeneration : IPeriodGeneration
    {
        private IUnitOfWork unitOfWork;
        private IMigratedPayrollDataProvider migratedPayrollDataProvider;
        public AnnualPeriodGeneration(IUnitOfWork unitOfWork, IMigratedPayrollDataProvider migratedPayrollDataProvider)
        {
            this.unitOfWork = unitOfWork;
            this.migratedPayrollDataProvider = migratedPayrollDataProvider;
        }

        public void Generate(bool autoActiveCurrentYear)
        {
            // fix all existing period if need
            migratedPayrollDataProvider.FixAllPeriodStartEndDate();


            var bdDateNow = BdDateTime.Now();
            var currentYear = bdDateNow.Year;
            for (int year = currentYear - 1; year <= currentYear + 1; year++)
            {
                string periodName = "";
                var thisYear = year.ToString();
                var year_lastTwoDigit = thisYear.Substring(thisYear.Length - 2, 2);
                periodName = "Jan" + "-" + year_lastTwoDigit + " " + "To" + " " + "Dec" + "-" + year_lastTwoDigit;
                var periodtypeId = BillPeriodType.Annual;
                var firstDayOfYear = new DateTime(year, 1, 1);
                var lastDayOfYear = new DateTime(year, 12, 31);
                
                // update PeriodTypeId for this type of annual period
                var thisPeriod = unitOfWork.PeriodRepo.FirstOrDefault(w => w.StartDate == firstDayOfYear && w.EndDate == lastDayOfYear);
                if (thisPeriod != null)
                {
                    if (thisPeriod.PeriodTypeId == null)
                    {
                        thisPeriod.PeriodTypeId = periodtypeId;
                    }
                }
                else
                {
                    BillPeriod billPeriod = new BillPeriod();
                    billPeriod.MonthId = 0;
                    billPeriod.PeriodName = periodName;
                    billPeriod.Year = year;
                    if (autoActiveCurrentYear && currentYear == year) // if this is current year
                        billPeriod.IsVisible = true;
                    else
                        billPeriod.IsVisible = false;
                    billPeriod.PeriodTypeId = periodtypeId;
                    billPeriod.StartDate = firstDayOfYear;
                    billPeriod.EndDate = lastDayOfYear;
                    unitOfWork.PeriodRepo.Add(billPeriod);
                }
            }
            unitOfWork.SaveChanges();
        }
    }
}
