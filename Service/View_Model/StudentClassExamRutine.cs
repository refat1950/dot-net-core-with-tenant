﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.View_Model
{
    public class StudentClassExamRutine
    {
        public string Serial { get; set; }
        public string SubjectName { get; set; }
        public string Day { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }
        public string ClassNo { get; set; }
        public string DocumentType { get; set; }
        public string DocumentUrl { get; set; }
    }
}
