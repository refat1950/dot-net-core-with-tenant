﻿using Entity.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Services.Interfaces
{
    public interface ISessionService
    {
        bool isExists(int year);
        Session GetActiveSession();
        List<VmSelectLists> GetAllIsVisibleSessions();
    }
}
