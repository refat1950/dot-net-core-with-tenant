﻿using Service.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Web.Models;

namespace Service.Service.Interfaces.Payroll
{
    public interface IMigratedPayrollDataProvider
    {
        bool IsPayrollMigrated();
        decimal GetTotalPaidAmount(DateTime fromDate, DateTime toDate);
        void FixAllPeriodStartEndDate();
        bool DeletePayslip(int empid, int periodid, int positionid, int payrollid);
        bool ResetPayslip();
        void StorPositionChangeRequestLog(VmEmployee employee, int userid);
    }
}
