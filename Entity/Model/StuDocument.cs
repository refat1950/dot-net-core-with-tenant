using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entity.Model
{
    public partial class StuDocument : BaseEntity
    {
        [Key]
        public int DocId { get; set; }
        [DisplayFormat(DataFormatString = "MM/dd/yyyy", ApplyFormatInEditMode = true)]
        public DateTime? ActivateDate { get; set; }

        [StringLength(10)]
        public string UserType { get; set; }

        public int? ReferenceId { get; set; }

        //[ForeignKey("Campus")]
        public int? CampusId { get; set; }

        public int? ClassId { get; set; }

        public int? SectionId { get; set; }

        public int? TypeId { get; set; }

        public int? ShiftId { get; set; }

        public int? SubjectId { get; set; }

        public int? SessionId { get; set; }

        [StringLength(150)]
        public string Path { get; set; }

        [StringLength(20)]
        public string FileType { get; set; }

        [StringLength(10)]
        public string FileSize { get; set; }

        //public virtual Campus Campus { get; set; }
    }
}
