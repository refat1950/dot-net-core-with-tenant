using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entity.Model
{
    public partial class EmergencyMedicalAction : BaseEntity
    {
        [Key]
        public int EmergencyMedicalActionId { get; set; }

        [StringLength(256)]
        public string Name { get; set; }

        //[Column(TypeName = "ntext")]
        public string Description { get; set; }

        public int? MedicalHistoryId { get; set; }


    }
}
