using System;
using System.ComponentModel.DataAnnotations;

namespace Entity.Model
{
    public partial class StudentAdmissionTest : BaseEntity
    {
        [Key]
        public int StudentAdmissionId { get; set; }

        [Required]
        [StringLength(30)]
        public string StudentFirstName { get; set; }

        [StringLength(30)]
        public string StudentMiddleName { get; set; }

        [StringLength(30)]
        public string StudentLastName { get; set; }

        [StringLength(30)]
        public string StudentNickName { get; set; }

        public int? AdmissionClassId { get; set; }

        public int? SessionId { get; set; }

        [StringLength(30)]
        public string FatherFirstName { get; set; }

        [StringLength(30)]
        public string FatherMiddleName { get; set; }

        [StringLength(30)]
        public string FatherLastName { get; set; }

        [StringLength(30)]
        public string FatherMobile { get; set; }

        [StringLength(30)]
        public string FatherNationalId { get; set; }

        [StringLength(30)]
        public string MotherFirstName { get; set; }

        [StringLength(30)]
        public string MotherMiddleName { get; set; }

        [StringLength(30)]
        public string MotherLastName { get; set; }

        [StringLength(30)]
        public string MotherMobile { get; set; }

        [StringLength(30)]
        public string MotherNationalId { get; set; }

        [StringLength(30)]
        public string PresHoldingNo { get; set; }

        [StringLength(50)]
        public string PresStreet { get; set; }

        [StringLength(50)]
        public string PresArea { get; set; }

        [StringLength(10)]
        public string PermPostCode { get; set; }

        public int? PresThanaId { get; set; }

        public int? PresCityId { get; set; }

        public int? PresDivisionId { get; set; }

        public int? PresCountryId { get; set; }

        public DateTime? Dob { get; set; }

        [StringLength(1)]
        public string Gender { get; set; }

        [StringLength(30)]
        public string Mobile { get; set; }

        [StringLength(30)]
        public string ResPhone { get; set; }

        [StringLength(50)]
        public string Email { get; set; }

        [StringLength(10)]
        public string Religion { get; set; }

        [StringLength(10)]
        public string BloodGroup { get; set; }

        [StringLength(50)]
        public string Nationality { get; set; }

        [StringLength(30)]
        public string BirthCertificateNo { get; set; }

        public int? BirthCity { get; set; }

        public int? BirthCountry { get; set; }

        public int? CampusId { get; set; }

        [StringLength(30)]
        public string CampusFloor { get; set; }

        [StringLength(10)]
        public string RoomNo { get; set; }

        public DateTime? ExamDate { get; set; }

        [StringLength(20)]
        public string StartTime { get; set; }

        [StringLength(20)]
        public string EndTime { get; set; }

        public DateTime? ResultDate { get; set; }

        public double? TotalMarks { get; set; }

        public double? MarksObtained { get; set; }

        public bool? Status { get; set; }

        [StringLength(100)]
        public string ImageUrl { get; set; }

    }
}
