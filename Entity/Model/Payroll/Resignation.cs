﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class Resignation : BaseEntity
    {
        [Key]
        public int ResignationId { get; set; }
        public string LatterPath { get; set; }
        public DateTime? LatterReceivedDate { get; set; }
        public DateTime ResignDate { get; set; }
        public int EmployeeHeaderId { get; set; }
        public ResignationType ResignationType { get; set; }
        [Required]
        public string Reason { get; set; }
        public string Activities { get; set; } // bad or good
        public bool IsResignationApplied { get; set; }
        public virtual ICollection<OfficialResignRules> OfficialResignRules { get; set; }
    }
    public class OfficialResignRules : BaseEntity
    {
        [Key]
        public int ResignationPropertiesId { get; set; }
        public int FlexValueId { get; set; }
        public bool IsApplied { get; set; }
        public int ResignationId { get; set; }
        public virtual Resignation Resignation { get; set; }
    }
    public enum ResignationType
    {
        Resign = 1,
        Terminate = 2
    }
}
