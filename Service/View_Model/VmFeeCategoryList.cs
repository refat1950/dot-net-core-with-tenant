﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.View_Model
{
    public class VmFeeCategoryList
    {
        public int FeeCategoryId { get; set; }
        public string FeeCategoryName { get; set; }
        public string ReceiptPrefix { get; set; }
        public string Description { get; set; }
    }
}
