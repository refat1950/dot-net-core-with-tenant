using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entity.Model
{
    public partial class ClassBook : BaseEntity
    {
        public int BookId { get; set; }
        public int ClassId { get; set; }

    }
}
