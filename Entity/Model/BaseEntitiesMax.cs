﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class BaseEntitiesMax
    {
        [StringLength(250)]
        public string Attribute1 { get; set; }
        [StringLength(250)]
        public string Attribute2 { get; set; }
        [StringLength(250)]
        public string Attribute3 { get; set; }
        [StringLength(250)]
        public string Attribute4 { get; set; }
        [StringLength(250)]
        public string Attribute5 { get; set; }
        [StringLength(250)]
        public string Attribute6 { get; set; }
        [StringLength(250)]
        public string Attribute7 { get; set; }
        [StringLength(250)]
        public string Attribute8 { get; set; }
        [StringLength(250)]
        public string Attribute9 { get; set; }
        [Column(TypeName = "text")]
        public string Attribute10 { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreationDate { get; set; }
        public long? LastUpdateBy { get; set; }
        public DateTime? LastUpdateDate { get; set; }
        public DateTime? EffectiveFrom { get; set; }
        public DateTime? EffectiveTo { get; set; }
    }
}
