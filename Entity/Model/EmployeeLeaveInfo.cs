﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class EmployeeLeaveInfo : BaseEntity
    {
        [Key]
        public int LeaveInfoId { get; set; }
        public int EmployeeHeaderId { get; set; }
        public int LeaveTypeId { get; set; }
        public decimal Balance { get; set; }
        public decimal LeaveApplied { get; set; }
    }
}
