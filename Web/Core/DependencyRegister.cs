﻿using Entity.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Repository.Context;
using Services.EmailServices;
using Services.UserService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Tenant;
using Tenant.Context;
using Tenant.Provider;

namespace Web.Core
{
    public static class DependencyRegister
    {
        public static void DependencyRegisterLayer(this IServiceCollection services, IConfiguration Configuration)
        {
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>(); 
            services.AddDbContext<TenantContext>(options => options.UseMySQL(Configuration.GetConnectionString("tenantconnectionstring"), b => b.MigrationsAssembly(typeof(TenantContext).Assembly.FullName)));
            services.AddTransient<ITenantProvider, DatabaseTenantProvider>();
            services.AddTransient<ProjectDbContext, ProjectDbContext>();
            services.AddTransient<DbContext, ProjectDbContext>();
            services.AddTransient<IUserStore<User>, UserStore<User>>();
            services.AddTransient<UserManager<User>, UserManager<User>>();
            
            services.AddTransient<Emailer, Emailer>();
        }
    }
}
