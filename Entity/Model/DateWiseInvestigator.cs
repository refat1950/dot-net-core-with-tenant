﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class DateWiseInvestigator : BaseEntity
    {
        [Key]
        public int DateWiseInvestigatorId { get; set; }
        public DateTime Date { get; set; }
        public int InvestigatorId { get; set; }
        public int ExamRoomInvestigatorId { get; set; }
        public ExamRoomInvestigator ExamRoomInvestigator { get; set; }
    }
}
