﻿using System;

namespace Business
{
    public static class StringExt
    {
        public static string StringRemoveString(string sourceString, string removeString)
        {
            var index = sourceString.IndexOf(removeString, StringComparison.Ordinal);
            var cleanPath = (index < 0) ? sourceString
                                        : sourceString.Remove(index, removeString.Length);
            return cleanPath;
        }
    }
}