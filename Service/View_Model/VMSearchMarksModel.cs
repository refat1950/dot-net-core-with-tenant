﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.View_Model
{
    public class VMSearchMarksModel
    {
        public int  SessionId { get; set; }
        public int ClassId { get; set; }
        public int SectionId { get; set; }
        public int  ShiftId { get; set; }
        public int SubjectId { get; set; }
        public int ExamId { get; set; }
        public int SubgroupId { get; set; }
        public int TeacherId { get; set; }
        public bool IsAdmin { get; set; }
    }
}
