﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class Device : BaseEntity
    {
        [Key]
        public int DeviceHeaderId { get; set; }
        public int DeviceCode { get; set; }
        public string DeviceName { get; set; }
        public string DeviceDetails { get; set; }
        public bool DeviceStatus { get; set; }
    }
}
