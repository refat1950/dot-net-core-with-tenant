using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entity.Model
{
    public class StudentBillMaster
    {
        [Key]
        public int BillHeaderId { get; set; }

        [StringLength(10)]
        public string BillNo { get; set; }

        public DateTime? BillDate { get; set; }

        public int? SessionId { get; set; }

        public int? StudentHeaderId { get; set; }

        public int? ClassId { get; set; }

        public int? SectionId { get; set; }

        public int? Shift { get; set; }

        public int? SchoolId { get; set; }

        public int? BranchId { get; set; }

        public decimal? Discount { get; set; }

        public int? PeriodId { get; set; }
        [StringLength(250)]
        public string Attribute1 { get; set; }
        [StringLength(250)]
        public string Attribute2 { get; set; }
        [StringLength(250)]
        public string Attribute3 { get; set; }
        [StringLength(250)]
        public string Attribute4 { get; set; }
        [StringLength(250)]
        public string Attribute5 { get; set; }

        public int? CreatedBy { get; set; }


        /// <summary>
        /// WHO Column
        /// </summary>
        [DataType(DataType.Date, ErrorMessage = "Date format should be MM/DD/YYYY")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? CreationDate { get; set; }


        public int? LastUpdateBy { get; set; }

        [DataType(DataType.Date, ErrorMessage = "Date format should be MM/DD/YYYY")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? LastUpdateDate { get; set; }

        public virtual ICollection<StudentBillLine> StudentBillLines { get; set; }
    }
}
