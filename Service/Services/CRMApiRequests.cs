﻿using data.UnitOfWork;
using Newtonsoft.Json;
using Repository.Provider.Interface;
using RestSharp;
using Service.Helper;
using Service.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Entity.Model;
using Repository.Context;
using Repository.DefaultValues;

namespace Service.Services
{
    public class CRMApiRequests : ICRMApiRequests
    {
        private string BASE_URL = ServiceResource.BASE_URL;
        private string apiKey = ServiceResource.ApiKey;
        //public BulkSmsRequests(IUnitOfWork unitOfWork, ITenantProvider tenantProvider)
        //{
        //    _unitOfWork = unitOfWork;
        //    this.tenantProvider = tenantProvider;
        //}
        private IUnitOfWork _unitOfWork;
     


        public CRMApiRequests(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public int StoreInvoiceAndSmsBalance(decimal balance, string transactionId)
        {
            var school = _unitOfWork.SchoolRepo.FirstOrDefault();
            VmApiCustomer vmApiCustomer = new VmApiCustomer();
            vmApiCustomer.CompanyName = school.SchoolName;
            vmApiCustomer.EmailAddress = school.SchoolEMail;
            vmApiCustomer.MobileNumber = school.Mobile1;
            vmApiCustomer.PhoneNumber = school.Mobile2;
            vmApiCustomer.AddressOne = school.Address1;
            vmApiCustomer.AddressTwo = school.Address2;
            vmApiCustomer.Attribute1 = school.SchoolTagLine; // School Tag Line
            vmApiCustomer.CustomerUserName = school.CustomerUserName;
            vmApiCustomer.CustomerUserPasword = school.CustomerUserPasword;
            vmApiCustomer.balance = balance;
            vmApiCustomer.transactionId = transactionId;

            Uri myUri = new Uri(BASE_URL + "api/ApiAddInvoice");
            var client = new RestClient(myUri);
            var request = new RestRequest(Method.POST);
            request.AddHeader("accept", "application/json");
            request.AddQueryParameter("apikey", apiKey);
            var username = school.CustomerUserName;
            var password = school.CustomerUserPasword;
            var authValue = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(Encoding.UTF8.GetBytes($"{username}:{password}"))).ToString();
            request.AddHeader("Authorization", authValue);
            request.AddJsonBody(vmApiCustomer);
            IRestResponse response = client.Execute(request);
            //string balanceString = response.Content;

            var smsTransaction = new SmsBuyingLog
            {
                CompanyName = school.SchoolName,
                EmailAddress = school.SchoolEMail,
                MobileNumber = school.Mobile1,
                PhoneNumber = school.Mobile2,
                AddressOne = school.Address1,
                AddressTwo = school.Address2,
                Attribute1 = school.SchoolTagLine, // school tag line
                CustomerUserName = school.CustomerUserName,
                CustomerUserPasword = school.CustomerUserPasword,
                balance = balance,
                transactionId = transactionId,
                TransactionStatus = response.StatusCode.ToString(),
                CreationDate = DateTime.Now
            };
            _unitOfWork.SmsBuyingLogsRepo.Add(smsTransaction);
            _unitOfWork.SaveChanges();
            if (response.StatusCode != 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        //Need timeout exception handling
        public VmApiSmsTransection GetBalance()
        {
            var school = _unitOfWork.SchoolRepo.FirstOrDefault();
            VmApiCustomerLoginData vmApiCustomer = new VmApiCustomerLoginData();
            vmApiCustomer.CustomerUserName = school.CustomerUserName;
            vmApiCustomer.CustomerUserPasword = school.CustomerUserPasword;

            Uri myUri = new Uri(BASE_URL + "api/GetSmsBalance");
            var client = new RestClient(myUri);
            var request = new RestRequest(Method.POST);
            request.AddHeader("accept", "application/json");
            request.AddQueryParameter("apikey", apiKey);
            var username = school.CustomerUserName;
            var password = school.CustomerUserPasword;
            var authValue = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(Encoding.UTF8.GetBytes($"{username}:{password}"))).ToString();
            request.AddHeader("Authorization", authValue);
            request.AddJsonBody(vmApiCustomer);

            IRestResponse response = client.Execute(request);
            if (response.StatusCode != 0)
            {
                try
                {
                    string contentString = response.Content;
                    var content = JsonConvert.DeserializeObject<VmApiSmsTransection>(contentString);
                    if (content != null)
                    {
                        VmApiSmsTransection vmAccountBalance = new VmApiSmsTransection();
                        vmAccountBalance.balance = content.balance;
                        vmAccountBalance.CumulativeBalance = content.CumulativeBalance;
                        vmAccountBalance.Cost = content.Cost;
                        return vmAccountBalance;
                    }
                    else
                    {
                        VmApiSmsTransection vmAccountBalance = new VmApiSmsTransection();
                        vmAccountBalance.balance = 0;
                        vmAccountBalance.CumulativeBalance = 0;
                        vmAccountBalance.Cost = 0;
                        return vmAccountBalance;
                    }
                }
                catch (Exception)
                {

                    return null;
                }
                
            }
            else
            {
                return null;
            }
        }

        public List<VmApiSmsTransectionHistory> GetSmsTransectionHistory(int yearId)
        {
            var school = _unitOfWork.SchoolRepo.FirstOrDefault();
            VmApiCustomerLoginData vmApiCustomer = new VmApiCustomerLoginData();
            vmApiCustomer.CustomerUserName = school.CustomerUserName;
            vmApiCustomer.CustomerUserPasword = school.CustomerUserPasword;
            vmApiCustomer.YearId = yearId;

            Uri myUri = new Uri(BASE_URL + "api/GetSmsTransectionHistory");
            var client = new RestClient(myUri);
            var request = new RestRequest(Method.POST);
            request.AddHeader("accept", "application/json");
            request.AddQueryParameter("apikey", apiKey);
            var username = school.CustomerUserName;
            var password = school.CustomerUserPasword;
            var authValue = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(Encoding.UTF8.GetBytes($"{username}:{password}"))).ToString();
            request.AddHeader("Authorization", authValue);
            request.AddJsonBody(vmApiCustomer);

            IRestResponse response = client.Execute(request);
            if (response != null && response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                string contentString = response.Content;
                var content = JsonConvert.DeserializeObject<List<VmApiSmsTransectionHistory>>(contentString);

                List<VmApiSmsTransectionHistory> vmAccountBalanceList = new List<VmApiSmsTransectionHistory>();
                if (content != null)
                {
                    foreach (var item in content)
                    {
                        VmApiSmsTransectionHistory vmAccountBalance = new VmApiSmsTransectionHistory();
                        vmAccountBalance.Balance = item.Balance;
                        vmAccountBalance.TransactionDate = item.TransactionDate;
                        vmAccountBalanceList.Add(vmAccountBalance);
                    }
                    return vmAccountBalanceList;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        public List<VmApiSmsSendHistory> GetSmsSendHistory(string monthYear)
        {
            var school = _unitOfWork.SchoolRepo.FirstOrDefault();
            VmApiCustomerLoginData vmApiCustomer = new VmApiCustomerLoginData();
            vmApiCustomer.CustomerUserName = school.CustomerUserName;
            vmApiCustomer.CustomerUserPasword = school.CustomerUserPasword;
            vmApiCustomer.MonthYear = monthYear;

            Uri myUri = new Uri(BASE_URL + "api/GetSmsSendHistory");
            var client = new RestClient(myUri);
            var request = new RestRequest(Method.POST);
            request.AddHeader("accept", "application/json");
            request.AddQueryParameter("apikey", apiKey);
            var username = school.CustomerUserName;
            var password = school.CustomerUserPasword;
            var authValue = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(Encoding.UTF8.GetBytes($"{username}:{password}"))).ToString();
            request.AddHeader("Authorization", authValue);
            request.AddJsonBody(vmApiCustomer);

            IRestResponse response = client.Execute(request);
            if (response != null && response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                string contentString = response.Content;
                var content = JsonConvert.DeserializeObject<List<VmApiSmsSendHistory>>(contentString);

                List<VmApiSmsSendHistory> vmApiSmsSendHistories = new List<VmApiSmsSendHistory>();
                if (content != null)
                {
                    foreach (var item in content)
                    {
                        VmApiSmsSendHistory vmApiSmsSendHistory = new VmApiSmsSendHistory();
                        vmApiSmsSendHistory.TotalSend = item.TotalSend;
                        vmApiSmsSendHistory.SendnDate = item.SendnDate;
                        vmApiSmsSendHistories.Add(vmApiSmsSendHistory);
                    }
                    return vmApiSmsSendHistories;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        public int StoreQueue(List<SenderInfo> senderInfos, int queueType)
        {
            var school = _unitOfWork.SchoolRepo.FirstOrDefault();
            VmQueue vmSendSms = new VmQueue();
            vmSendSms.senderInfos = senderInfos;
            vmSendSms.QueueType = queueType;
            vmSendSms.SendOn = DateTime.Now.ToString("MM/dd/yy HH:mm:ss");
            vmSendSms.CustomerUserName = school.CustomerUserName;
            vmSendSms.CustomerUserPasword = school.CustomerUserPasword;

            Uri myUri = new Uri(BASE_URL + "api/StoreQueue");
            var client = new RestClient(myUri);
            var request = new RestRequest(Method.POST);
            request.AddHeader("accept", "application/json");
            request.AddQueryParameter("apikey", apiKey);
            var username = school.CustomerUserName;
            var password = school.CustomerUserPasword;
            var authValue = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(Encoding.UTF8.GetBytes($"{username}:{password}"))).ToString();
            request.AddHeader("Authorization", authValue);
            request.AddJsonBody(vmSendSms);

            IRestResponse response = client.Execute(request);
            string responseString = response.Content;
            var status = 0;
            if (response != null && response.StatusCode == System.Net.HttpStatusCode.OK && !string.IsNullOrEmpty(responseString))
            {
                status = (int)QueueRequestStatus.Success;
            }
            else if (response != null && response.StatusCode != System.Net.HttpStatusCode.OK && !string.IsNullOrEmpty(responseString))
                status = (int)QueueRequestStatus.Pending;
            else
            {
                status = (int)QueueRequestStatus.Failed;
            }
            return status;
        }

        public VmPaymentGatewaySettings GetPaymentSettings(int providerId)
        {
            var school = _unitOfWork.SchoolRepo.FirstOrDefault();
            VmApiCustomerLoginData vmApiCustomer = new VmApiCustomerLoginData();
            vmApiCustomer.CustomerUserName = school.CustomerUserName;
            vmApiCustomer.CustomerUserPasword = school.CustomerUserPasword;
            vmApiCustomer.ProviderId = providerId;

            Uri myUri = new Uri(BASE_URL + "api/GetPaymentSettings");
            var client = new RestClient(myUri);
            var request = new RestRequest(Method.POST);
            request.AddHeader("accept", "application/json");
            request.AddQueryParameter("apikey", apiKey);
            var username = school.CustomerUserName;
            var password = school.CustomerUserPasword;
            var authValue = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(Encoding.UTF8.GetBytes($"{username}:{password}"))).ToString();
            request.AddHeader("Authorization", authValue);
            request.AddJsonBody(vmApiCustomer);

            IRestResponse response = client.Execute(request);
            string responseString = response.Content;
            if (response != null && response.StatusCode == System.Net.HttpStatusCode.OK && !string.IsNullOrEmpty(responseString))
            {
                string contentString = response.Content;
                var content = JsonConvert.DeserializeObject<VmPaymentGatewaySettings>(contentString);
                if (content != null)
                {
                    VmPaymentGatewaySettings vmdata = new VmPaymentGatewaySettings();
                    vmdata.MerchantNumber = content.MerchantNumber;
                    vmdata.APIorURL = content.APIorURL;
                    vmdata.UserName = content.UserName;
                    vmdata.Password = content.Password;
                    vmdata.AccessOrAppkey = content.AccessOrAppkey;
                    vmdata.SecretkeyOrToken = content.SecretkeyOrToken;
                    vmdata.CustomerOrClientCode = content.CustomerOrClientCode;
                    vmdata.PartnerCode = content.PartnerCode;
                    vmdata.BankName = content.BankName;
                    vmdata.AccountNumber = content.AccountNumber;
                    vmdata.PaymentStatus = content.PaymentStatus;
                    vmdata.PaymentMode = content.PaymentMode;
                    vmdata.Attribute1 = content.Attribute1;
                    vmdata.Attribute2 = content.Attribute2;
                    vmdata.Attribute3 = content.Attribute3;
                    vmdata.Attribute4 = content.Attribute4;
                    vmdata.Attribute5 = content.Attribute5;
                    vmdata.Attribute6 = content.Attribute6;
                    vmdata.Attribute7 = content.Attribute7;
                    vmdata.Attribute8 = content.Attribute8;
                    vmdata.Attribute9 = content.Attribute9;
                    vmdata.Attribute10 = content.Attribute10;
                    vmdata.EffectiveFrom = content.EffectiveFrom;
                    vmdata.EffectiveTo = content.EffectiveTo;
                    return vmdata;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }
    }
}
