﻿using data.UnitOfWork;
using Entity.Model;
using Repository.Context;
using Service.Services.Interfaces;
using Service.View_Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Services
{
    public class FeeSubCategoryWaiverService: IFeeSubCategoryWaiverService
    {
        FlexValueService flexService;
        private IUnitOfWork _unitOfWork;
       

        public FeeSubCategoryWaiverService(FlexValueService flexService, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            this.flexService = flexService;
        }

        public int Add(VmFeeSubCategoryWaiverAdd model, int userId)
        {
            if (model.FeeSubCategoryWaiverId == 0)
            {
                FeeSubCategoryWaiver newData = new FeeSubCategoryWaiver();
                newData.FeeSubCategoryWaiverId = 0;
                newData.FeeSubCategoryId = model.FeeSubCategoryId;
                newData.WaiverType = model.WaiverType == (int)WaiverType.Category? WaiverType.Category: WaiverType.Gender;
                newData.Category_GenderID = model.WaiverType == (int)WaiverType.Category? (model.CategoryId??0): (model.GenderId??0);
                newData.Percentage = model.Percentage;
                newData.CreatedBy = userId;
                newData.LastUpdateBy = userId;
                newData.CreationDate = DateTime.Now;
                newData.LastUpdateDate = DateTime.Now;
                _unitOfWork.FeeSubCategoryWaiverRepo.Add(newData);
            }
            else
            {
                var newData = _unitOfWork.FeeSubCategoryWaiverRepo.Get(model.FeeSubCategoryWaiverId);
                if (newData != null)
                {
                    newData.FeeSubCategoryId = model.FeeSubCategoryId;
                    newData.WaiverType = model.WaiverType == (int)WaiverType.Category ? WaiverType.Category : WaiverType.Gender;
                    newData.Category_GenderID = model.WaiverType == (int)WaiverType.Category ? (model.CategoryId ?? 0) : (model.GenderId ?? 0);
                    newData.Percentage = model.Percentage;
                    newData.LastUpdateBy = userId;
                    newData.LastUpdateDate = DateTime.Now;
                }
            }
            return _unitOfWork.SaveChanges();
        }

        public List<VmFeeSubCategoryWaiverList> GetBetween(int start, int displayLength, string searchValue, out int totalLength)
        {
            var allValues = _unitOfWork.FeeSubCategoryWaiverRepo.GetAllQueryable();

            if (!string.IsNullOrEmpty(searchValue))
            {
                allValues = allValues.Where(w => (!string.IsNullOrEmpty(w.WaiverType.ToString()) && w.WaiverType.ToString().ToLower().Contains(searchValue.ToLower())) || (!string.IsNullOrEmpty(w.Percentage.ToString()) && w.Percentage.ToString().ToLower().Contains(searchValue.ToLower())));
            }
            
            var result = allValues.ToList().Select(s => new VmFeeSubCategoryWaiverList
            {
                FeeCategoryId = s.FeeSubCategory.FeeCategoryId,
                FeeCategoryName = s.FeeSubCategory.FeeCategory.FeeCategoryName,
                WaiverType = (int)s.WaiverType,
                WaiverTypeName = s.WaiverType.ToString(),
                FeeSubCategoryId = s.FeeSubCategoryId,
                FeeSubCategoryName = s.FeeSubCategory.FeeSubCategoryName,
                Category_GenderName = flexService.GetFlexValueByFlexValueId(s.Category_GenderID),
                Category_GenderID = s.Category_GenderID,
                FeeSubCategoryWaiverId = s.FeeSubCategoryWaiverId,
                Percentage = s.Percentage,
            });
            var displayedValues = displayLength == -1 ? result
                .OrderBy(o => o.FeeSubCategoryWaiverId).Skip(start).OrderBy(o => o.FeeSubCategoryWaiverId).ToList() : result
                .OrderBy(o => o.FeeSubCategoryWaiverId).Skip(start)
                .Take(displayLength).ToList();
            totalLength = allValues.Count();
            return displayedValues;
        }
    }
}
