﻿using Entity.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Web.Models.View_Model
{
    public class VmExamList
    {
        public int ExamId { get; set; }
        public string ExamName { get; set; }
    }

    public class VmExamTypeList
    {
        public int ExamTypeId { get; set; }
        public int? ParentExamTypeId { get; set; }
        public string parentyName { get; set; }
        public string ExamTypeName { get; set; }
        public SubExamMarkBase? SubExamMarkBase { get; set; }
    }

    public class VmClassSubjectWiseExamTypeList
    {
        public int ClassSubjectWiseExamTypeId { get; set; }
        public string Class { get; set; }
        public int ClassId { get; set; }
        public string Exam { get; set; }
        public int ExamId { get; set; }
        public string Subject { get; set; }
        public int SubjectId { get; set; }
        public string ExamType { get; set; }
        public int ExamTypeId { get; set; }
        public decimal BaseMark { get; set; }
        public decimal PassMark { get; set; }
    }

    public class VmLoadMarksEntryTable
    {
        public int ClassId { get; set; }
        public int SectionId { get; set; }
        public int SubjectId { get; set; }
        public int ExamId { get; set; }
        public List<VmExamTypeOfClassSectionSubject> examTypeList { get; set; }
        public List<VmStudentMark> studentsList { get; set; }
        public bool? IsPublished { get; set; }
        public bool? IsEditAble { get; set; }
    }

    public class VmLoadMarksEditTable
    {
        public int ClassId { get; set; }
        public int SectionId { get; set; }
        public int SubjectId { get; set; }
        public int ExamId { get; set; }
    }

    public class VmSaveMarks
    {
        public int MarksId { get; set; }
        public int SessionId { get; set; }
        public int StudentHeaderId { get; set; }
        public int ClassId { get; set; }
        public int SectionId { get; set; }
        public int Shift { get; set; }
        public int TeacherId { get; set; }
        public int SubjectId { get; set; }
        public int? EventId { get; set; }
        public decimal OutOfMarks { get; set; }
        public decimal? ObtainedMarks { get; set; }
    }

    //exam grade
    public class VmExamGrade
    {
        public int GradeId { get; set; }
        public string Description { get; set; }
        public string ShortDesc { get; set; }
        public string AltName { get; set; }
        public decimal FromScore { get; set; }
        public decimal ToScore { get; set; }
        public string Point { get; set; }
    }

    //Mark sheet
    public class VmMarkSheetExtend
    {
        public int ClassId { get; set; }
        public int SectionId { get; set; }
        public int SubjectId { get; set; }
    }

    public class VmMarkSheet
    {
        public int StudentHeaderId { get; set; }
        public int SessionId { get; set; }
        public string StudentName { get; set; }
        public int StudentRoll { get; set; }
        public int? ClassId { get; set; }
        public string ClassName { get; set; }
        public int? SectionId { get; set; }
        public string SectionName { get; set; }
        public string Attendence { get; set; }
        public int? CampusId { get; set; }
        public string CampusName { get; set; }
        public int ExamId { get; set; }
        public string ExamName { get; set; }
        public string AvarageGrade { get; set; }
        public decimal TotalBaseMark { get; set; }
        public decimal TotalObtainmarks { get; set; }
        public string AvaragePoint { get; set; }

        public List<VmMarkSheetSubject> vmSubjectWiseMarks { get; set; }
    }

    public class VmMarkSheetExam
    {
        public int ExamId { get; set; }
        public string ExamName { get; set; }
    }

    public class VmMarkSheetSubject
    {
        public int SubjectId { get; set; }
        public string SubjectName { get; set; }
        public decimal SubjectTotalMarks { get; set; }
        public string SubjectTotalPoint { get; set; }
        public string SubjectTotalGrade { get; set; }
        public List<VmMarkSheetExamTypeWiseMarks> vmMarkSheetExamTypeWiseMarks { get; set; }
    }
    public class VmMarkSheetData
    {
        public List<VmMarkSheet>  vmMarkSheets{ get; set; }
        public List<VmMarkSheetExamGrade> vmMarkSheetExamGrades { get; set; }
    }

    public class VmMarkSheetExamTypeWiseMarks 
    {
        public int ExamTypeId { get; set; }
        public string ExamTypeName { get; set; }
        public decimal BaseMarks { get; set; }
        public decimal ObtainedMarks { get; set; }
        public string ObtainedMarksText { get; set; }
    }

    public class VmMarkSheetExamGrade
    {
        public int GradeId { get; set; }
        public string AltName { get; set; }
        public decimal FromScore { get; set; }
        public decimal ToScore { get; set; }
        public string Point { get; set; }
    }
    public class VmResultAnalysis
    {
        public string SectionName { get; set; }
        public int TotalStudent { get; set; }
        public List<int> gradelist { get; set; }

    }

    public class VmExamTypeOfClassSectionSubject
    {
        public VmExamTypeOfClassSectionSubject()
        {
            ExamTypeList = new List<ExamType>();
        }
        public int ClassSubjectWiseExamTypeId { get; set; }
        public decimal BaseMark { get; set; }
        public int ExamTypeId { get; set; }
        public int ExamId { get; set; }
        public string ExamTypeName { get; set; }
        public string SubExamMarkBase { get; set; }
        public List<ExamType> ExamTypeList { get; set; }
        public int SubjectId { get; set; }
    }

    public class VmStudentMark
    {
        public int StudentHeaderId { get; set; }
        public string StudentId { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string ImgUrl { get; set; }
        public List<VmMark> VmMarks { get; set; }
    }

    public class VmMark
    {
        public decimal? ObtainedMark { get; set; }
        public AttendanceType? AttendanceType { get; set; }
        public int ExamTypeId { get; set; }
        public int ExamId { get; set; }
        public int SessionId { get; set; }
        public int SubjectId { get; set; }
        public bool? IsPublished { get; set; }
    }
}