﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tenant.Entities;

namespace Tenant.Context
{
    public class TenantContext : DbContext
    {
        public TenantContext(DbContextOptions<TenantContext> options) : base(options)
        {

        }
        public virtual DbSet<TenantInfo> TenantInfo { get; set; }
        public virtual DbSet<SmsProvider> SmsProviders { get; set; }
        public virtual DbSet<MultitenantProcess> Process { get; set; }
        public virtual DbSet<MultitenantProcessLog> ProcessLog { get; set; }
        public virtual DbSet<FndMenu> FndMenu { get; set; }
        public virtual DbSet<FndMenuGroup> FndMenuGroups { get; set; }


        private static DbContextOptions GetOptions(string connectionString)
        {
            return MySQLDbContextOptionsExtensions.UseMySQL(new DbContextOptionsBuilder(), connectionString).Options;
        }
    }
}
