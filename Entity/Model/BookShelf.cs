using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Entity.Model
{
    [Table("BookShelfs")]
    public class BookShelfs : BaseEntity
    {
        [Key]
        public int ShelfId { get; set; }
        public string ShelfName { get; set; }
    }
}
