﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class AttendancePolicy : BaseEntity
    {
        [Key]
        public int PolicyId { get; set; }
        public int? DeptId { get; set; }
        public int? DesigId { get; set; }
        public int? MaxLate { get; set; }
        public int? MaxEarly { get; set; }
        public string RainyDay { get; set; }
        public bool Status { get; set; }
    }
}
