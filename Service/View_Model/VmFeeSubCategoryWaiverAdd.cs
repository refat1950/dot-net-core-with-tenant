﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.View_Model
{
    public class VmFeeSubCategoryWaiverAdd
    {
        public int FeeSubCategoryWaiverId { get; set; }
        public int FeeSubCategoryId { get; set; }
        public int WaiverType { get; set; }
        public int? CategoryId { get; set; }
        public int? GenderId { get; set; }
        public decimal Percentage { get; set; }
    }
}
