﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model.Workflow
{
    public class WorkFlow : BaseEntity
    {
        [Key]
        public int WorkFlowId { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
    }
}
