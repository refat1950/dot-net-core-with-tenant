﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class MenuGroup : BaseEntity
    {
        public int MenuGroupId { get; set; }
        public string MenuGroupName { get; set; }
    }
}
