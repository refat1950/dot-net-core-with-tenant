﻿using Microsoft.EntityFrameworkCore.Migrations;
using MySql.EntityFrameworkCore.Metadata;
using System;

namespace Tenant.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "FndMenu",
                columns: table => new
                {
                    MenuId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    MenuName = table.Column<string>(type: "text", nullable: false),
                    Url = table.Column<string>(type: "text", nullable: false),
                    Sequence = table.Column<int>(type: "int", nullable: false),
                    IsActive = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    ParentMenuId = table.Column<int>(type: "int", nullable: true),
                    RolesId = table.Column<int>(type: "int", nullable: true),
                    GroupId = table.Column<int>(type: "int", nullable: true),
                    MenuType = table.Column<int>(type: "int", nullable: false),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FndMenu", x => x.MenuId);
                });

            migrationBuilder.CreateTable(
                name: "FndMenuGroups",
                columns: table => new
                {
                    MenuGroupId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    GroupSerial = table.Column<int>(type: "int", nullable: false),
                    GroupName = table.Column<string>(type: "text", nullable: true),
                    Status = table.Column<int>(type: "int", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FndMenuGroups", x => x.MenuGroupId);
                });

            migrationBuilder.CreateTable(
                name: "Process",
                columns: table => new
                {
                    ProcessHeaderId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    ProcessName = table.Column<string>(type: "text", nullable: true),
                    IsActive = table.Column<bool>(type: "tinyint(1)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Process", x => x.ProcessHeaderId);
                });

            migrationBuilder.CreateTable(
                name: "ProcessLog",
                columns: table => new
                {
                    ProcessLogHeaderId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    ProcessHeaderId = table.Column<int>(type: "int", nullable: false),
                    MACResellerId = table.Column<int>(type: "int", nullable: true),
                    ProcessTime = table.Column<DateTime>(type: "datetime", nullable: false),
                    Status = table.Column<int>(type: "int", nullable: false),
                    Error = table.Column<string>(type: "text", nullable: true),
                    ErrorFileName = table.Column<string>(type: "text", nullable: true),
                    ErrorLineNumber = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProcessLog", x => x.ProcessLogHeaderId);
                });

            migrationBuilder.CreateTable(
                name: "SmsProviders",
                columns: table => new
                {
                    ProviderId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    ProviderName = table.Column<string>(type: "text", nullable: true),
                    ProviderUrl = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SmsProviders", x => x.ProviderId);
                });

            migrationBuilder.CreateTable(
                name: "TenantInfo",
                columns: table => new
                {
                    TenantId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    Url = table.Column<string>(type: "text", nullable: true),
                    ConnectionStringName = table.Column<string>(type: "text", nullable: true),
                    AlternateUrl1 = table.Column<string>(type: "text", nullable: true),
                    AlternateUrl2 = table.Column<string>(type: "text", nullable: true),
                    ConnectionString = table.Column<string>(type: "text", nullable: true),
                    ServerUrl = table.Column<string>(type: "text", nullable: true),
                    DbName = table.Column<string>(type: "text", nullable: true),
                    DbPassword = table.Column<string>(type: "text", nullable: true),
                    IsActive = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    SchoolName = table.Column<string>(type: "text", nullable: true),
                    MobileNo = table.Column<string>(type: "text", nullable: true),
                    MonthlyCharge = table.Column<decimal>(type: "decimal(18, 2)", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true),
                    Attribute1 = table.Column<string>(type: "text", nullable: true),
                    Attribute2 = table.Column<string>(type: "text", nullable: true),
                    Attribute3 = table.Column<string>(type: "text", nullable: true),
                    Attribute4 = table.Column<string>(type: "text", nullable: true),
                    Attribute5 = table.Column<string>(type: "text", nullable: true),
                    MinDeviceId = table.Column<int>(type: "int", nullable: true),
                    MaxDeviceId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TenantInfo", x => x.TenantId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "FndMenu");

            migrationBuilder.DropTable(
                name: "FndMenuGroups");

            migrationBuilder.DropTable(
                name: "Process");

            migrationBuilder.DropTable(
                name: "ProcessLog");

            migrationBuilder.DropTable(
                name: "SmsProviders");

            migrationBuilder.DropTable(
                name: "TenantInfo");
        }
    }
}
