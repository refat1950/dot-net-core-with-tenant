﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model.Workflow
{
    public class ApprovalFlow : BaseEntity
    {
        [Key]
        public int ApprovalFlowId { get; set; }
        //[Key, Column(Order = 0)]Work
        public int Department { get; set; }
        public int WorkFlowId { get; set; }
        //[Key, Column(Order = 1)]
        public int Sequence { get; set; }
        public int Approver { get; set; }
        public int Isactive { get; set; }
    }
}
