﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Entity.Model
{
    public class FeeCategory : BaseEntity
    {
        [Key]
        public int FeeCategoryId { get; set; }
        public string FeeCategoryName { get; set; }
        public string ReceiptPrefix { get; set; }
        public string Description { get; set; }
        public virtual ICollection<FeeSubCategory> FeeSubCategory { get; set; }
    }
}
