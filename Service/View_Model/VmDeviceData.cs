﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.View_Model
{
    public class VmDeviceData
    {
        public int DeviceHeaderId { get; set; }
        public int DeviceCode { get; set; }
        public string DeviceName { get; set; }
        public string DeviceDetails { get; set; }
        public bool Status { get; set; }
    }
}
