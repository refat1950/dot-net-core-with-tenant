using System;
using System.ComponentModel.DataAnnotations;

namespace Entity.Model
{
    public partial class PlannerEventClass : BaseEntity
    {
        [Key]
        public int EventDtlsId { get; set; }

        public int ClassId { get; set; }

        public int Shift { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

    }
}
