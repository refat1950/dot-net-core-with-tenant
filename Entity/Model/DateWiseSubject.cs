﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class DateWiseSubject : BaseEntity
    {
        [Key]
        public int DateWiseSubjectId { get; set; }
        public DateTime Date { get; set; }
        public bool IsDateAssigned { get; set; }
        public int SubjectId { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public int ExamScheduleLineId { get; set; }
        public virtual ExamScheduleLine ExamScheduleLine { get; set; }
    }
}