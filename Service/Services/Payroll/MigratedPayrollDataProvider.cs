﻿using data.UnitOfWork;
using Entity.Model;
using Repository.Context;
using Service.DataService;
using Service.Helper;
using Service.Service.Interfaces.Payroll;
using Service.ViewModel;
using Services.CoreServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Web.Models;

namespace Service.Service.Payroll
{
    public class MigratedPayrollDataProvider : IMigratedPayrollDataProvider
    {
        #region "Declared objects & Constructor"
        private IUnitOfWork unitOfWork;
        private ProjectDbContext db;
        public MigratedPayrollDataProvider(IUnitOfWork unitOfWork, ProjectDbContext db)
        {
            this.unitOfWork = unitOfWork;
            this.db = db;
        }
        #endregion

        //public bool IsPayrollMigrated(int? branchId)
        //{
        //    var flex = unitOfWork.FlexSetRepo.GetFirstWithOutBranch(f => f.FlexValueShortName == "pmp");
        //    return flex != null ? flex.FndFlexValue != null ? flex.FndFlexValue.Where(w => w.BranchId == branchId).Any() : false : false;
        //}
        public bool IsPayrollMigrated()
        {
            var flex = unitOfWork.FlexSetRepo.FirstOrDefault(f => f.FlexValueShortName == "pmp");
            return flex != null ? unitOfWork.FlexValueRepo.Find(f => f.FlexValueSetId == flex.FlexValueSetId).Any() : false;
        }

        public decimal GetEmpBasicSalary(int empHeaderId)
        {
            var basicSalaryId = unitOfWork.HrPayHeadsRepo.FirstOrDefault(s => s.PayHeadShortValue == "bs") != null ? unitOfWork.HrPayHeadsRepo.FirstOrDefault(s => s.PayHeadShortValue == "bs").PayHeadId : 0;
            var salary = unitOfWork.HrEmployeePayHeadsRepo.FirstOrDefault(s => s.EmpHeaderId == empHeaderId && s.PayHeadId == basicSalaryId);
            return salary != null ? salary.Unit : 0;
        }

        public decimal GetTotalPaidAmount(DateTime fromDate, DateTime toDate)
        {
            return unitOfWork.HrPaySlipPayLogsRepo.Find(s => s.Attribute1 == null && CoreServices.TruncateTime(s.PaidDate) >= fromDate && CoreServices.TruncateTime(s.PaidDate) <= toDate).Select(s => s.PaidAmount).Sum();
        }

        public void FixAllPeriodStartEndDate()
        {
            var allPeriods = db.BillPeriod.Where(s => s.Year > 2017 && s.PeriodTypeId != BillPeriodType.Weekly).ToList();
            foreach (var thisPeriod in allPeriods)
            {
                if (thisPeriod.StartDate == null || thisPeriod.EndDate == null)
                {
                    var lastDateOfmonth = new DateTime(thisPeriod.Year, thisPeriod.MonthId, 1).AddMonths(1).AddDays(-1).AddHours(23).AddMinutes(59).AddSeconds(59);
                    var lastDayOfmonth = lastDateOfmonth.Day;

                    var periodStartdate = new DateTime(thisPeriod.Year, thisPeriod.MonthId, 1);
                    var periodEnddate = new DateTime(thisPeriod.Year, thisPeriod.MonthId, lastDayOfmonth);

                    thisPeriod.StartDate = periodStartdate;
                    thisPeriod.EndDate = periodEnddate;
                }
            }
            db.SaveChanges();
        }

        public bool DeletePayslip(int empid, int periodid, int positionid, int payrollid)
        {
            try
            {
                var PrevMasterData = unitOfWork.HrPaySlipTransactionMastersRepo.FirstOrDefault(w => w.EmpHeaderId == empid && w.PeriodId == periodid && w.PositionId == positionid && w.PayrollId == payrollid);
                if (PrevMasterData != null)
                {
                    var payslipTransation = PrevMasterData.HrPaySlipTransactions;
                    if (payslipTransation.Any())
                    {
                        unitOfWork.HrPaySlipTransactionsRepo.RemoveRange(payslipTransation);
                    }

                    // remove log master tables all child data
                    var payslipTransactionlog = PrevMasterData.HrPaySlipTransactionMasterLog;
                    foreach (var item in payslipTransactionlog)
                    {
                        var masterLogTransactionsAllTransactionlog = item.HrPaySlipTransactionLog;
                        if (masterLogTransactionsAllTransactionlog.Any())
                        {
                            unitOfWork.HrPaySlipTransactionLogsRepo.RemoveRange(masterLogTransactionsAllTransactionlog);
                        }
                    }
                    //remove log master table data
                    if (payslipTransactionlog.Any())
                    {
                        unitOfWork.HrPaySlipTransactionMasterLogsRepo.RemoveRange(payslipTransactionlog);
                    }


                    // remove log master history tables all child data
                    var payslipTransactionloghistory = PrevMasterData.GetHrPaySlipTransactionMasterLogHistories;
                    foreach (var item in payslipTransactionloghistory)
                    {
                        var masterLogTransactionsAllTransactionlog = item.HrPaySlipTransactionLogHistories;
                        if (masterLogTransactionsAllTransactionlog.Any())
                        {
                            unitOfWork.HrPaySlipTransactionLogHistorysRepo.RemoveRange(masterLogTransactionsAllTransactionlog);
                        }
                    }
                    //remove log master table data
                    if (payslipTransactionloghistory.Any())
                    {
                        unitOfWork.HrPaySlipTransactionMasterLogHistorysRepo.RemoveRange(payslipTransactionloghistory);
                    }
                    //******** end remove all previous data except master table


                    // delete original master
                    unitOfWork.HrPaySlipTransactionMastersRepo.Remove(PrevMasterData);
                    var status = unitOfWork.SaveChanges();
                    return status > 0 ? true : false;
                }
                return false;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool ResetPayslip()
        {
            var allPayslips = unitOfWork.HrPaySlipTransactionMastersRepo.GetAll().ToList();
            foreach (var payslips in allPayslips)
            {
                var status = DeletePayslip(payslips.EmpHeaderId, payslips.PeriodId, payslips.PositionId, payslips.PayrollId);
            }
            return true;
        }

        public void StorPositionChangeRequestLog(VmEmployee employee, int userid)
        {
            if (employee.IsPositionChangeShowed ?? false) // if position change date showed
            {

                var positionChangeLastRequest = unitOfWork.PositionChangeRequestsRepo.GetAll().OrderByDescending(s => s.PositionChangeRequestId).FirstOrDefault(s => s.EmpHeaderId == employee.EmpHeaderId);

                var PositionAssignFrom = DateTimeHelperService.ConvertBDDateStringToDateTimeObject("1/" + employee.PositionAssignFrom);
                var PositionAssignFromDate = Convert.ToDateTime(PositionAssignFrom).Date;

                if (positionChangeLastRequest != null)
                {
                    if (employee.PositionCancelReqClicked) // if requsted to cancel
                    {
                        var newlog = new PositionChangeRequest
                        {
                            EmpHeaderId = employee.EmpHeaderId,
                            PositionId = positionChangeLastRequest.PositionId,
                            RequestedChangeDate = positionChangeLastRequest.RequestedChangeDate,
                            PositionChangeRequestStatus = PositionChangeRequestStatus.Cancel,
                            CreationDate = DateTime.Now,
                            CreatedBy = userid
                        };
                        unitOfWork.PositionChangeRequestsRepo.Add(newlog);
                    }
                    else
                    {
                        if (positionChangeLastRequest.PositionChangeRequestStatus == PositionChangeRequestStatus.Pending && positionChangeLastRequest.RequestedChangeDate != PositionAssignFromDate) // if this date is not the previous date, so user did changed the date again, so update log table
                        {
                            var newlog = new PositionChangeRequest
                            {
                                EmpHeaderId = employee.EmpHeaderId,
                                PositionId = positionChangeLastRequest.PositionId,
                                RequestedChangeDate = PositionAssignFromDate,
                                PositionChangeRequestStatus = PositionChangeRequestStatus.Pending,
                                CreationDate = DateTime.Now,
                                CreatedBy = userid
                            };
                            unitOfWork.PositionChangeRequestsRepo.Add(newlog);
                        }
                    }
                }
                else
                {
                    var newlog = new PositionChangeRequest
                    {
                        EmpHeaderId = employee.EmpHeaderId,
                        PositionId = employee.PositionId ?? 0,
                        RequestedChangeDate = PositionAssignFromDate,
                        PositionChangeRequestStatus = PositionChangeRequestStatus.Pending,
                        CreationDate = DateTime.Now,
                        CreatedBy = userid
                    };
                    unitOfWork.PositionChangeRequestsRepo.Add(newlog);
                }
                unitOfWork.SaveChanges();
            }
        }
    }
}
