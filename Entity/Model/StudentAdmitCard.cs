using System;
using System.ComponentModel.DataAnnotations;

namespace Entity.Model
{
    public class StudentAdmitCard
    {
        [Key]
        public int TableId { get; set; }

        [StringLength(50)]
        public string StudentId { get; set; }

        [StringLength(50)]
        public string StudentName { get; set; }

        [StringLength(50)]
        public string ClassName { get; set; }

        [StringLength(50)]
        public string SectionName { get; set; }

        [StringLength(50)]
        public string RoomNo { get; set; }

        [StringLength(50)]
        public string FloorNo { get; set; }

        [StringLength(50)]
        public string Semester { get; set; }

        [StringLength(50)]
        public string SessionId { get; set; }

        [StringLength(250)]
        public string Attribute1 { get; set; }
        [StringLength(250)]
        public string Attribute2 { get; set; }
        [StringLength(250)]
        public string Attribute3 { get; set; }
        [StringLength(250)]
        public string Attribute4 { get; set; }
        [StringLength(250)]
        public string Attribute5 { get; set; }

        public long? CreatedBy { get; set; }


        /// <summary>
        /// WHO Column
        /// </summary>
        [DataType(DataType.Date, ErrorMessage = "Date format should be MM/DD/YYYY")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? CreationDate { get; set; }


        public long? LastUpdateBy { get; set; }

        [DataType(DataType.Date, ErrorMessage = "Date format should be MM/DD/YYYY")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? LastUpdateDate { get; set; }
    }
}