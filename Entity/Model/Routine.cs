using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entity.Model
{
    public class Routine : BaseEntity
    {
        [Key]
        public int RoutineId { get; set; }

        public int? DayId { get; set; }

        [Column(Order = 3)]
        public DateTime RoutineDate { get; set; }

        public int? Period { get; set; }

        [Required]
        [StringLength(30)]
        public string StartTime { get; set; }

        [Required]
        [StringLength(30)]
        public string EndTime { get; set; }

        [Column(Order = 7)]
        public int ClassCountNo { get; set; }

        public int? CampusId { get; set; }

        public int? SubjectId { get; set; }

        public int? BookId { get; set; }

        public int? ClassId { get; set; }

        public int? SECTION_ID { get; set; }

        public int? Shift { get; set; }

        public int? SectionId { get; set; }

        public int? TeacherId { get; set; }

        [StringLength(20)]
        public string GroupName { get; set; }
    }
}
