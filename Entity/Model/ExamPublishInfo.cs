using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entity.Model
{
    public class ExamPublishInfo : BaseEntity
    {
        [Key]
        public int PublishInfoId { get; set; }
        public int SessionId { get; set; }
        public int ClassId { get; set; }
        public int SubjectId { get; set; }
        public int SectionId { get; set; }
        public int? PublishBy { get; set; }
        public int ExamId { get; set; }
        public bool IsPublished { get; set; }
    }

}
