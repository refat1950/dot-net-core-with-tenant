﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entity.Model
{
    public class StudentCategoryLog : BaseEntity
    {
        [Key]
        public int CategoryLogHeaderId { get; set; }
        public int FromSessionId { get; set; }
        public int ToSessionId { get; set; }
        public int StudentHeaderId { get; set; }
        public int StudentCategoryId { get; set; }
    }
}
