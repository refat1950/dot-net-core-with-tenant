using System;
using System.ComponentModel.DataAnnotations;

namespace Entity.Model
{
    public partial class EventRoutine : BaseEntity
    {
        [Key]
        public int RoutineId { get; set; }

        public int? SessionId { get; set; }

        public int? EventDtlsId { get; set; }

        public int ClassId { get; set; }

        public int? SectionId { get; set; }

        public int SubjectId { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public virtual Subject BssSubject { get; set; }
        public virtual Class BssClass { get; set; }
    }
}
