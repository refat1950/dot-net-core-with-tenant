﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class HrPayHead : BaseEntity
    {
        [Key]
        public int PayHeadId { get; set; }
        public string PayHeadName { get; set; }
        public string PayHeadShortValue { get; set; }
        public PayHeadType PayHeadType { get; set; }
        public string Description { get; set; }
    }
    public enum PayHeadType
    {
        Addition = 1,
        Deduction = 2
    }
}
