﻿using data.UnitOfWork;
using Entity.Model;
using Repository.Context;
using Service.View_Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Services
{
	public class FlexValueService
	{
		private IUnitOfWork _unitOfWork;
	

        public FlexValueService(IUnitOfWork unitOfWork)
		{
			_unitOfWork = unitOfWork;
        }

		public List<VmSelectListService> GetFlexValueListFromFlexSetShortValue(string shortValue)
		{
			var Set = _unitOfWork.FlexSetRepo.FirstOrDefault(o => o.FlexValueShortName == shortValue);
			return Set.FndFlexValue.OrderBy(l => l.FlexValueId).Select(s => new VmSelectListService
			{
				Id = s.FlexValueId,
				Name = s.FlexValue
			}).ToList();
		}

		public int GetFlexValueSetIdByFlexValueShortName(string shortValue)
		{
			var Set = _unitOfWork.FlexSetRepo.FirstOrDefault(o => o.FlexValueShortName == shortValue)!= null ? _unitOfWork.FlexSetRepo.FirstOrDefault(o => o.FlexValueShortName == shortValue).FlexValueSetId : 0;
			return Set;
		}
		public string GetFlexValueSetNameByFlexValueSetId(int FLexSetValueId)
		{
			var FlexValueSet = _unitOfWork.FlexSetRepo.FirstOrDefault(o => o.FlexValueSetId == FLexSetValueId) != null ? _unitOfWork.FlexSetRepo.FirstOrDefault(o => o.FlexValueSetId == FLexSetValueId).FlexValueSetName : "";
			return FlexValueSet;
		}


		public int GetFlexValueIdByFlexShortValue(string shortValue)
		{
			var Set = _unitOfWork.FlexValueRepo.FirstOrDefault(o => o.FlexShortValue == shortValue) != null ? _unitOfWork.FlexValueRepo.FirstOrDefault(o => o.FlexShortValue == shortValue).FlexValueId : 0;
			return Set;
		}

		public string GetFlexShortValueByFlexValueId(int flexValueId)
		{
			var Set = _unitOfWork.FlexValueRepo.FirstOrDefault(o => o.FlexValueId == flexValueId);

			return Set != null ? Set.FlexShortValue : "";
		}

		public string GetFlexValueByFlexValueId(int FlexValueId)
		{
            string flexValueSet = String.Empty;
            try
            {
                flexValueSet = _unitOfWork.FlexValueRepo.FirstOrDefault(o => o.FlexValueId == FlexValueId) != null? _unitOfWork.FlexValueRepo.FirstOrDefault(o => o.FlexValueId == FlexValueId).FlexValue : "";
            }
            catch (Exception)
            {
                flexValueSet = "";
            }
			return flexValueSet;
		}
		public int DeleteFlex(int FlexValueId)
		{
			var flex = _unitOfWork.FlexValueRepo.FirstOrDefault(f => f.FlexValueId == FlexValueId);
			_unitOfWork.FlexValueRepo.Remove(flex);
			return _unitOfWork.SaveChanges();
		}

	}
}
