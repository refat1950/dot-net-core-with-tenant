﻿using Entity.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class TeacherLeaveCount : BaseEntity
    {
        [Key]
        public int LeaveCountId { get; set; }
        public int ApplicationId { get; set; }
        public string LeaveQuantity { get; set; }
        public int TeacherTotalLeave { get; set; }
        public int? TeacherLeaveRemain { get; set; }
    }
}
