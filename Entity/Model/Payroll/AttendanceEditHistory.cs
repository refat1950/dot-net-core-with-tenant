using Entity.Model;
using System;
using System.ComponentModel.DataAnnotations;

namespace Entity.Model
{
    public class AttendanceEditHistory : BaseEntity
    {
        [Key]
        public int MenualAttendanceId { get; set; }
        public int EmployeeHeaderId { get; set; }
        public DateTime Date { get; set; }
        public string InTime { get; set; }
        public string OutTime { get; set; }
        public bool isActive { get; set; }
    }
}
