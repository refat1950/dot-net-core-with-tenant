﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class AttendanceGrade : BaseEntity
    {
        [Key]
        public int AttGradeId { get; set; }

        public string Description { get; set; }
        public string ShortDesc { get; set; }
        public string AltName { get; set; }

        public decimal FromScore { get; set; }
        public decimal ToScore { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "Active To")]
        public DateTime? StartDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "Active To")]
        public DateTime? EndDate { get; set; }
    }
}
