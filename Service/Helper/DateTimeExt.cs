﻿using System;

namespace Business
{
    public static class DateTimeExt
    {
        public static string GetAdminDateTimeFormate()
        {
            return "MM/dd/yyyy";
        }

        public static DateTime DateTimeNow()
        {
            return DateTime.UtcNow.AddHours(6);
        }

        public static string MinuteTohour(int minutes)
        {
            var span = TimeSpan.FromMinutes(minutes);
            var label = span.ToString("h'h 'm'm'");

            if (label.StartsWith("0h"))
            {
                label = StringExt.StringRemoveString(label, "0h ");
            }

            return label;
        }

        public static DateTime FirstDayOfMonth(DateTime dateTime)
        {
            return new DateTime(dateTime.Year, dateTime.Month, 1);
        }
    }
}