﻿using Entity.Model;
using Service.ViewModel;
using Service.ViewModel.Payroll;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Web.Models;

namespace Service.DataService
{
    public interface IEmployeeService
    {
        int Add(VmEmployee vmEmployee, int userId);
        int Edit(VmEmployee employee, int userId);
        void AddEmployeeTransaction(int HeadId);
        HrEmployee Get(int id);
        int SaveChanges();
        EmployeeJoiningStatus GetEmployeeCurrentStatus(int EmpHeaderId);
        IEnumerable<HrEmployee> GetActiveEmployees();
        IEnumerable<HrEmployee> GetLeftEmployees();
        string GenerateEmpId();
    }
}
