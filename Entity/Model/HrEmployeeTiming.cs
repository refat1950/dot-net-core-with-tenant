using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entity.Model
{
    public partial class HrEmployeeTiming : BaseEntity
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int EmpHeaderId { get; set; }

        [StringLength(15)]
        public string WorkingDay { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }


    }
}
