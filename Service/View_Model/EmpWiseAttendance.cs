﻿using System;

namespace Services.View_Model
{
    public class EmpWiseAttendance
    {
        public DateTime OFFICE_TIME { get; set; }
        public string SHIFT_NAME { get; set; }
        public string SHIFT_TIME { get; set; }
        public string IN_TIME { get; set; }
        public string OUT_TIME { get; set; }
        public string LATE_TIME { get; set; }
        public int? EARLY_LEAVE { get; set; }
        public string ATTENDANCE_CODE { get; set; }
    }
}