using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Entity.Model
{
    public partial class School : BaseEntity
    {
        public School()
        {
            BssLogo = new HashSet<Logo>();
            SchoolBranch = new HashSet<SchoolBranch>();
            GeneralInfo = new HashSet<GeneralInfo>();
            SocialLinks = new HashSet<SchoolLinks>();
        }

        [Key]
        public int SchoolId { get; set; }

        [Required]
        [StringLength(50)]
        public string SchoolName { get; set; }


        [Required]
        [StringLength(50)]
        public string SchoolEMail { get; set; }

        public string SchoolInquiryMail { get; set; }


        public string SchoolInquiryMailPass { get; set; }


        public virtual ICollection<Logo> BssLogo { get; set; }

        public string SchoolTagLine { get; set; }
        public string Mobile1 { get; set; }
        public string Mobile2 { get; set; }
        public int SmsProviderId { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string SmsUserName { get; set; }
        public string SmsPassword { get; set; }
        public string SmsSender { get; set; }
        public int YearStartMonth { get; set; }

        public string CustomerUserName { get; set; }
        public string CustomerUserPasword { get; set; }

        public int? AttendenceTypeId { get; set; }
        public int? PositionTypeId { get; set; }

        public bool? IsAllowedEditAfterOvertimeAssigned { get; set; }
        public bool? IsAllowedEditAfterIntimeOuttimeApproved { get; set; }
        public bool? IsAllowedEditAfterIntimeOuttimeCanceled { get; set; }
        public bool? IsAllowedEditIfIntimeOuttimeRequestPending { get; set; }

        public int? LateCountAfterMinutesForStu { get; set; }
        public int? EarlyOutCountAfterMinutesForStu { get; set; }

        public bool? SendSmstoStudentAfterBillRecived { get; set; }
        public bool? SendSmstoStudentAfterStudentAdd { get; set; }

        public virtual ICollection<SchoolBranch> SchoolBranch { get; set; }
        public virtual ICollection<GeneralInfo> GeneralInfo { get; set; }
        public virtual ICollection<SchoolLinks> SocialLinks { get; set; }
    }
}
