﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class StudentAdmission : BaseEntity
    {
        [Key]
        public int StudentHeaderId { get; set; }

        [StringLength(30)]
        [Display(Name = "Id #:")]
        public string StudentId { get; set; }
        [Display(Name = "First Name:")]

        [StringLength(30)]
        public string FirstName { get; set; }
        [Display(Name = "Middle Name:")]
        [StringLength(30)]
        public string MiddleName { get; set; }
        [Display(Name = "Last Name:")]
        [StringLength(30)]
        public string LastName { get; set; }
        [Display(Name = "Nick Name:")]
        [StringLength(50)]
        public string NickName { get; set; }
        [Display(Name = "Holding No:")]
        [StringLength(30)]
        public string PresHoldingNo { get; set; }
        [Display(Name = "Street No:")]
        [StringLength(50)]
        public string PresStreet { get; set; }
        [Display(Name = "Area:")]
        public int PresArea { get; set; }
        [Display(Name = "Police Station:")]
        public int? PresThanaId { get; set; }
        [Display(Name = "Post Code:")]
        [StringLength(10)]
        public string PresPostCode { get; set; }
        [Display(Name = "City:")]
        public int? PresCityId { get; set; }
        [Display(Name = "Division:")]
        public int? PresDivisionId { get; set; }
        [Display(Name = "Country:")]
        public int? PresCountryId { get; set; }
        [Display(Name = "Holding No:")]
        [StringLength(30)]
        public string PeamHoldingNo { get; set; }
        [Display(Name = "Street:")]
        [StringLength(50)]
        public string PermStreet { get; set; }
        [Display(Name = "Area:")]
        public int? PermArea { get; set; }
        [Display(Name = "Post Code:")]
        [StringLength(10)]
        public string PermPostCode { get; set; }
        [Display(Name = "Police Station:")]
        public int? PermThanaId { get; set; }
        [Display(Name = "City:")]
        public int? PermCityId { get; set; }
        [Display(Name = "Division:")]
        public int? PermDivisionId { get; set; }
        [Display(Name = "Country:")]
        public int? PermCountryId { get; set; }
        [Display(Name = "Date Of Birth:")]
        [Required]
        public DateTime? Dob { get; set; }
        [Display(Name = "Country:")]
        public int? BirthCountry { get; set; }
        [Display(Name = "Place Of Birth:")]
        public int? PlaceOfBirth { get; set; }
        [Display(Name = "Gender:")]
        public int? Gender { get; set; }
        [Display(Name = "Religion:")]
        public int? Religion { get; set; }
        [Display(Name = "Residential Phone/Mobile:")]
        [StringLength(30)]
        public string ResPhone { get; set; }
        [Display(Name = "Mobile:")]
        [StringLength(30)]
        public string Mobile { get; set; }
        [Display(Name = "Email:")]
        [StringLength(100)]
        public string Email { get; set; }
        [Display(Name = "Blood Group:")]
        public int? BloodGroup { get; set; }
        [Display(Name = "Nationality:")]
        public int? Nationality { get; set; }
        [Display(Name = "Birth Certificate # :")]
        [StringLength(30)]
        public string BirthCertificateNo { get; set; }
        [Display(Name = "Passport #:")]
        [StringLength(30)]
        public string PassportNo { get; set; }
        [Display(Name = "Medical History:")]
        [StringLength(200)]
        public string MedicalHistory { get; set; }
        [Display(Name = "Emergency Medical Action:")]
        [StringLength(200)]
        public string EmergencyMedicalAction { get; set; }
        [Display(Name = "Father First Name:")]
        [StringLength(30)]
        public string FatherFirstName { get; set; }
        [Display(Name = "Middle Name:")]
        [StringLength(30)]
        public string FatherMiddleName { get; set; }
        [Display(Name = "Last Name:")]
        [StringLength(30)]
        public string FatherLastName { get; set; }
        [Display(Name = "Education:")]
        public int? FatherEducation { get; set; }
        [Display(Name = "Occupation:")]
        public int? FatherOccupation { get; set; }
        [Display(Name = "Mobile:")]
        [StringLength(30)]
        public string FatherMobile { get; set; }
        [Display(Name = "National Id #:")]
        [StringLength(30)]
        public string FatherNationalId { get; set; }
        [Display(Name = "National Id #:")]
        [StringLength(30)]
        public string FatherPassportNo { get; set; }
        [Display(Name = "First Name:")]
        [StringLength(30)]
        public string MotherFirstName { get; set; }
        [Display(Name = "Middle Name:")]
        [StringLength(30)]
        public string MotherMiddleName { get; set; }
        [Display(Name = "Last Name:")]
        [StringLength(30)]
        public string MotherLastName { get; set; }
        [Display(Name = "Education:")]
        public int? MotherEducation { get; set; }
        [Display(Name = "Occupation:")]
        public int? MotherOccupation { get; set; }
        [Display(Name = "Mobile:")]
        [StringLength(30)]
        public string MotherMobile { get; set; }
        [Display(Name = "National Id #:")]
        [StringLength(30)]
        public string MotherNationalId { get; set; }
        [Display(Name = "Passport No:")]
        [StringLength(30)]
        public string MoherPassportNo { get; set; }
        [Display(Name = "First Name")]
        [StringLength(30)]
        public string LocGuardianFirstName { get; set; }
        [Display(Name = "Middle Name")]
        [StringLength(30)]
        public string LocGuardianMiddleName { get; set; }
        [Display(Name = "Last Name")]
        [StringLength(30)]
        public string LocGuardianLastName { get; set; }

        [Display(Name = "Relationship")]
        [StringLength(30)]
        public string LocGuardianRelationship { get; set; }

        [Display(Name = " Mobile")]
        [StringLength(30)]
        public string LocGuardianMobile { get; set; }

        [Display(Name = "Local Gurdian national Id")]
        [StringLength(30)]
        public string LocalGurdianNationalId { get; set; }

        [Display(Name = "Passport")]
        [StringLength(30)]
        public string LocalGurdianPassportNo { get; set; }

        [StringLength(50)]
        public string StudentPhotoPath { get; set; }
    }
}
