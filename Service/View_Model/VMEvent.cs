﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.View_Model
{
    public class VMEvent
    {
        public int EventId { get; set; }
        public string EventName { get; set; }
    }
}
