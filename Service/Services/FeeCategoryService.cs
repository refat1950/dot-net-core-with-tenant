﻿using data.UnitOfWork;
using Entity.Model;
using Repository.Context;
using Service.Services.Interfaces;
using Service.View_Model;
using System.Collections.Generic;
using System.Linq;

namespace Service.Services
{
    public class FeeCategoryService : IFeeCategoryService
    {
        private IUnitOfWork _unitOfWork;
    

        public FeeCategoryService( IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public int Add(FeeCategory model)
        {
            if (model.FeeCategoryId == 0)
            {
                FeeCategory newData = new FeeCategory();
                newData.FeeCategoryId = 0;
                newData.FeeCategoryName = model.FeeCategoryName;
                newData.ReceiptPrefix = model.ReceiptPrefix;
                newData.Description = model.Description;
                _unitOfWork.FeeCategoryRepo.Add(newData);
            }
            else
            {
                var existingData = _unitOfWork.FeeCategoryRepo.Get(model.FeeCategoryId);
                if (existingData != null)
                {
                    existingData.FeeCategoryName = model.FeeCategoryName;
                    existingData.ReceiptPrefix = model.ReceiptPrefix;
                    existingData.Description = model.Description;
                }
            }
            return _unitOfWork.SaveChanges();
        }

        public List<VmFeeCategoryList> GetBetween(int start, int displayLength, string searchValue, out int totalLength)
        {
            var allValues = _unitOfWork.FeeCategoryRepo.GetAllQueryable();

            if (!string.IsNullOrEmpty(searchValue))
            {
                allValues = allValues.Where(w => (!string.IsNullOrEmpty(w.FeeCategoryName) && w.FeeCategoryName.ToLower().Contains(searchValue.ToLower())) || (!string.IsNullOrEmpty(w.ReceiptPrefix) && w.ReceiptPrefix.ToLower().Contains(searchValue.ToLower())) || (!string.IsNullOrEmpty(w.Description) && w.Description.ToLower().Contains(searchValue.ToLower())));
            }
            var result = allValues.Select(s => new VmFeeCategoryList
            {
                FeeCategoryId = s.FeeCategoryId,
                FeeCategoryName = s.FeeCategoryName,
                ReceiptPrefix = s.ReceiptPrefix,
                Description = s.Description,
            });
            var displayedValues = displayLength == -1 ? result
                .OrderBy(o => o.FeeCategoryId).Skip(start).OrderBy(o=>o.FeeCategoryId).ToList() : result
                .OrderBy(o => o.FeeCategoryId).Skip(start)
                .Take(displayLength).ToList();
            totalLength = allValues.Count();
            return displayedValues;
        }
    }
}
