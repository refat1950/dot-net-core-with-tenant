using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entity.Model
{
    public partial class SchoolBranch : BaseEntity
    {
        public int SchoolId { get; set; }
        public int BranchId { get; set; }

        [Required]
        [StringLength(100)]
        public string BranchName { get; set; }


        public virtual School School { get; set; }
    }
}
