using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entity.Model
{
    public partial class HrLeaveInfo : BaseEntity
    {
        [Key]
        public int EmployeeTypeId { get; set; }

        public int? LeaveTypeId { get; set; }

        public int? NoOfLeave { get; set; }

        public bool? Incashable { get; set; }

        [StringLength(10)]
        public string CashAgainst { get; set; }
    }
}
