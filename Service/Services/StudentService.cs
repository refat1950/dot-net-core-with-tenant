﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics.Eventing.Reader;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using Entity.Model;
using Repository.Context;
using Repository.Provider;
using Services.Interfaces;
using Services.View_Model;
using Services.Common;
using Service.Services;
using Entity.Model.Transaction;
using Web.Models.View_Model;
using Service.Helper;
using Service.DataService;
using Service.View_Model;
using data.UnitOfWork;
using Services.CoreServices;
namespace Services
{
    public class StudentService : IStudentService
    {
        private IUnitOfWork _unitOfWork;
     

        readonly DateTime _dateNow = DateTime.UtcNow.AddHours(6);

        const string ExamPaperShortCode = "EP";
        const string ClassWorkShortCode = "CW";
        readonly string[] _rcvTypes = { "360", "361" };
        private const int Semester1StEventId = 1;
        private const int Semester2NdEventId = 2;
        private const int NoticeForAllId = 354;
        private const int NoticeFor = 355;
        private const int NoticeType = 353;
        private const int NewsType = 5079;
        private const int PlayGroup = 2;
        private const int Nursery = 3;
        private const int CurriculumFlexId = 64;
        private int AchivementFlexValueId = 5037;
        private readonly IValidationDictionary _validationDictionary;
        private readonly ITeacherService teacherService;
        private StudentAttendanceService studentAttendanceService;
        FlexValueService _flexService;

        public void AddStudentTransaction(int studentheaderid)
        {
            var tstudent = _unitOfWork.StudentRepo.FirstOrDefault(s => s.StudentHeaderId == studentheaderid);
            StudentTransaction tobj = new StudentTransaction();
            tobj.StudentHeaderId = tstudent.StudentHeaderId;
            tobj.StudentPhotoPath = tstudent.StudentPhotoPath;
            tobj.FirstName = tstudent.FirstName;
            tobj.MiddleName = tstudent.MiddleName;
            tobj.LastName = tstudent.LastName;
            tobj.NickName = tstudent.NickName;
            tobj.StudentId = tstudent.StudentId;
            tobj.GenderId = tstudent.GenderId;
            tobj.BirthDate = tstudent.BirthDate;

            tobj.Email = tstudent.Email;
            tobj.Mobile = tstudent.Mobile;
            tobj.ResPhone = tstudent.ResPhone;
            tobj.PunchCardNo = tstudent.PunchCardNo;
            tobj.AdmissionDate = tstudent.AdmissionDate;

            tobj.AdmissionSessionId = tstudent.AdmissionSessionId;
            tobj.AdmissionClassId = tstudent.AdmissionClassId;
            tobj.AdmissionSectionId = tstudent.AdmissionSectionId;
            tobj.AdmissionShift = tstudent.AdmissionShift;
            tobj.SessionId = tstudent.SessionId;
            tobj.SectionId = tstudent.SectionId;
            tobj.Shift = tstudent.Shift;
            tobj.DeviceNumber = tstudent.DeviceNumber;
            tobj.RollNumber = tstudent.RollNumber;
            tobj.EndDate = tstudent.EndDate;

            tobj.ClassId = tstudent.ClassId;
            tobj.CampusId = tstudent.CampusId;
            tobj.StatusId = tstudent.StatusId;
            tobj.ExitReasons = tstudent.ExitReasons;
            tobj.ProximityNum = tstudent.ProximityNum;

            tobj.Age = tstudent.Age;
            tobj.PresHoldingNo = tstudent.PresHoldingNo;
            tobj.PresStreet = tstudent.PresStreet;
            tobj.PresArea = tstudent.PresArea;
            tobj.PresPostCode = tstudent.PresPostCode;
            tobj.PresCountryId = tstudent.PresCountryId;
            tobj.PresDivisionId = tstudent.PresDivisionId;
            tobj.PresCityId = tstudent.PresCityId;
            tobj.PresThanaId = tstudent.PresThanaId;

            tobj.PermHoldingNo = tstudent.PermHoldingNo;
            tobj.PermStreet = tstudent.PermStreet;
            tobj.PermArea = tstudent.PermArea;
            tobj.PermPostCode = tstudent.PermPostCode;
            tobj.PermCountryId = tstudent.PermCountryId;
            tobj.PermDivisionId = tstudent.PermDivisionId;
            tobj.PermCityId = tstudent.PermCityId;
            tobj.PermThanaId = tstudent.PermThanaId;

            tobj.FatherFirstName = tstudent.FatherFirstName;
            tobj.FatherMiddleName = tstudent.FatherMiddleName;
            tobj.FatherLastName = tstudent.FatherLastName;
            tobj.FatherMobile = tstudent.FatherMobile;
            tobj.FatherOfficePhone = tstudent.FatherOfficePhone;
            tobj.FatherNationalId = tstudent.FatherNationalId;
            tobj.FatherPassportNo = tstudent.FatherPassportNo;
            tobj.FatherEducation = tstudent.FatherEducation;
            tobj.FatherEducationDetails = tstudent.FatherEducationDetails;
            tobj.FatherOccupation = tstudent.FatherOccupation;
            tobj.FatherOccupationDetails = tstudent.FatherOccupationDetails;
            tobj.FatherOfficeAddress = tstudent.FatherOfficeAddress;

            tobj.MotherFirstName = tstudent.MotherFirstName;
            tobj.MotherMiddleName = tstudent.MotherMiddleName;
            tobj.MotherLastName = tstudent.MotherLastName;
            tobj.MotherMobile = tstudent.MotherMobile;
            tobj.MotherOfficePhone = tstudent.MotherOfficePhone;
            tobj.MotherNationalId = tstudent.MotherNationalId;
            tobj.MoherPassportNo = tstudent.MoherPassportNo;
            tobj.MotherEducation = tstudent.MotherEducation;
            tobj.MotherEducationDetails = tstudent.MotherEducationDetails;
            tobj.MotherOccupation = tstudent.MotherOccupation;
            tobj.MotherOccupationDetails = tstudent.MotherOccupationDetails;
            tobj.MotherOfficeAddress = tstudent.MotherOfficeAddress;

            tobj.LocGuardianFirstName = tstudent.LocGuardianFirstName;
            tobj.LocGuardianMiddleName = tstudent.LocGuardianMiddleName;
            tobj.LocGuardianLastName = tstudent.LocGuardianLastName;
            tobj.LocalGurdianNationalId = tstudent.LocalGurdianNationalId;
            tobj.LocalGurdianPassportNo = tstudent.LocalGurdianPassportNo;
            tobj.LocGuardianOfficeAddress = tstudent.LocGuardianOfficeAddress;
            tobj.LocGuardianMobile = tstudent.LocGuardianMobile;
            tobj.LocGuardianRelationship = tstudent.LocGuardianRelationship;

            tobj.BloodId = tstudent.BloodId;
            tobj.ReligionId = tstudent.ReligionId;
            tobj.NationalityId = tstudent.NationalityId;
            tobj.PassportNo = tstudent.PassportNo;
            tobj.BirthCertificateNo = tstudent.BirthCertificateNo;
            tobj.EmergencyMedicalAction = tstudent.EmergencyMedicalAction;
            tobj.MedicalHistory = tstudent.MedicalHistory;

            tobj.LastUpdateBy = tstudent.LastUpdateBy;
            tobj.CreationDate = tstudent.CreationDate;
            tobj.LastUpdateDate = tstudent.LastUpdateDate;
            _unitOfWork.StudentTransactionRepo.Add(tobj);
            _unitOfWork.SaveChanges();
        }
        public List<VMSelectList> GetAllCurrentSessionActiveStudents()
        {
            var activeId = _flexService.GetFlexValueIdByFlexShortValue("active");
            var currentSessionId = _unitOfWork.SessionRepo.FirstOrDefault(s => s.Status == true).SessionId;
            return _unitOfWork.StudentRepo.Find(o => o.StatusId == activeId && o.SessionId == currentSessionId).Select(s => new VMSelectList
            {
                Id = s.StudentHeaderId,
                Name = s.FirstName + " " + s.MiddleName + " " + s.LastName + " (" + s.StudentId + ")"
            }).ToList();
        }
        public StudentService(IValidationDictionary validationDictionary, FlexValueService _flexService, ITeacherService teacherService , StudentAttendanceService studentAttendanceService, IUnitOfWork unitOfWork)
        {
            _validationDictionary = validationDictionary;
            this._flexService = _flexService;
            this.teacherService = teacherService;
            this.studentAttendanceService = studentAttendanceService;
            _unitOfWork = unitOfWork;
        }
        protected bool ValidateProduct(Student productToValidate)
        {
            if (productToValidate.Email.Trim().Length == 0)
                _validationDictionary.AddError("Email", "Email is required.");
            if (productToValidate.PermPostCode.Trim().Length == 0)
                _validationDictionary.AddError("Post code", "Post code is required");
            return _validationDictionary.IsValid;
        }
      
        public void PopulateStudentMarks(List<StudentMarks> marks, bool isNew, int referenceId)
        {
            if (isNew)
            {
                foreach (StudentMarks t in marks)
                {
                    t.CreatedBy = referenceId;
                    t.CreationDate = _dateNow;
                    t.Attribute1 = "N";
                }
            }
            else
            {
                foreach (StudentMarks t in marks)
                {
                    t.LastUpdateBy = referenceId;
                    t.LastUpdateDate = _dateNow;
                    t.Attribute1 = "O";
                }
            }

        }
        
        public AttendenceViewModel StudentAttendance(int studentId, int sessionId)
        {
            var sem1St = _unitOfWork.PlannerEventDetailsRepo.FirstOrDefault(w => w.SessionId == sessionId && w.EventId == Semester1StEventId);
            var sem2Nd = _unitOfWork.PlannerEventDetailsRepo.FirstOrDefault(w => w.SessionId == sessionId && w.EventId == Semester2NdEventId);
            if (sem1St == null && sem2Nd == null)
            {
                return new AttendenceViewModel { Atten1StSeme = "0%", Atten2NdSeme = "0%", AttenAverage = "0%", AttenTotalDay = "0" };
            }

            var startDate = sem1St != null ? sem1St.StartDate : DateTime.Today;
            var sem1StEndDate = sem1St != null ? sem1St.EndDate : DateTime.Today;
            var sem2NdStartDate = sem2Nd != null ? sem2Nd.StartDate : DateTime.Today;
            var endDate = sem2Nd != null ? sem2Nd.EndDate : DateTime.Today;


            //var student = _unitOfWork.Student.FirstOrDefault(f => f.StudentHeaderId == studentId);
            //var studentAtten = StudentAttendenceList(student.ProximityNum, startDate.ToString(),endDate.ToString(), 10000, 0);
            var studentAtten = _unitOfWork.AttendanceStudentRepo.Find(w => w.StudentHeaderId == studentId && w.PunchDate >= startDate && w.PunchDate <= endDate).ToList();

            var attenPresentOnTime = studentAtten.Count(c => c.TimeLate <= 0);
            var attenLate = studentAtten.Count(c => c.TimeLate > 0);
            var attenAbsent = studentAtten.Count(c => c.InTime == null);
            var attenTotalDay = studentAtten.Count().ToString();
            var attenTotalDayInt = studentAtten.Count();

            var sem1StTotal = (decimal)studentAtten.Count(w => w.PunchDate <= sem1StEndDate);
            var atten1StSeme = (decimal)studentAtten.Count(w => w.PunchDate <= sem1StEndDate && w.InTime != null);

            decimal atten1StSemeAvg = 0;
            if (sem1StTotal != 0 && atten1StSeme != 0)
                atten1StSemeAvg = decimal.Round((atten1StSeme / sem1StTotal) * 100, 2, MidpointRounding.AwayFromZero);


            var sem2NdTotal = (decimal)studentAtten.Count(w => w.PunchDate >= sem2NdStartDate && w.PunchDate <= endDate);
            var atten2NdSeme = (decimal)studentAtten.Count(w => w.PunchDate >= sem2NdStartDate && w.PunchDate <= endDate && w.InTime != null);

            decimal atten2NdSemeAvg = 0;
            if (sem2NdTotal != 0 && atten2NdSeme != 0)
                atten2NdSemeAvg = decimal.Round((atten2NdSeme / sem2NdTotal) * 100, 2, MidpointRounding.AwayFromZero);

            var semTotal = (decimal)studentAtten.Count();
            var attenSeme = (decimal)studentAtten.Count(w => w.InTime != null);
            decimal attenSemeAvg = 0;

            if (semTotal != 0 && attenSeme != 0)
                attenSemeAvg = decimal.Round(((attenSeme / semTotal) * 100), 2, MidpointRounding.AwayFromZero);

            var comment = _unitOfWork.AttendanceGradeRepo.FirstOrDefault(f => f.FromScore <= attenSemeAvg && f.ToScore >= attenSemeAvg);
            return new AttendenceViewModel
            {
                AttenPresentOnTime = attenPresentOnTime,
                AttenLate = attenLate,
                AttenAbsent = attenAbsent,
                AttenTotalDay = attenTotalDay,
                Atten1StSeme = atten1StSemeAvg + "%",
                Atten2NdSeme = atten2NdSemeAvg + "%",
                AttenAverage = attenSemeAvg + "%",
                AttenComment = comment != null && attenTotalDayInt != 0 ? comment.Description : ""
            };
        }
        public List<StudentClassRoutineViewModel> GetStudentClassRoutine(DateTime date, int sessionId, int classId, int sectionId)
        {
            try
            {
                return _unitOfWork.ClassRoutineRepo.Find(s => s.RoutineDate == date.Date && s.ClassId == classId && s.SectionId == sectionId && s.SessionId == sessionId)
                                    .Select(s => new StudentClassRoutineViewModel
                                    {
                                        Period = _unitOfWork.FlexValueRepo.FirstOrDefault(f => f.FlexValueId == s.Period).FlexValue + " Preiod",
                                        Subject = _unitOfWork.SubjectRepo.FirstOrDefault(f => f.SubjectId == s.SubjectId).ShortName
                                    })
                                    .OrderBy(o => o.Period)
                        .ToList();
            }
            catch (Exception ex)
            {
                return new List<StudentClassRoutineViewModel>();
            }
        }
        public List<StudentNoticeBoardViewModel> GetStudentNoteces(int studentId, int classId, int sectionId, int campusId)
        {
            try
            {
                return GetNotice(NoticeType, classId, sectionId, campusId)
                    .Select(s => new StudentNoticeBoardViewModel
                    {
                        Text = s.Header,
                        Link = "/Student/Notice"
                    })
                    .ToList();
            }
            catch (Exception)
            {
                return new List<StudentNoticeBoardViewModel>();
            }
        }
        public List<string> GetNews(int classId, int sectionId, int campusId)
        {
            try
            {
                return GetNotice(NewsType, classId, sectionId, campusId).Select(s => s.Body).ToList();
            }
            catch (Exception)
            {
                return new List<string>();
            }
        }
        private IQueryable<Notice> GetNotice(int noticeType, int classId, int sectionId, int campusId)
        {
            var today = _dateNow.Date;//CoreServices.TruncateTime(_dateNow);

            return _unitOfWork.NoticeRepo.Find(w => (w.NoticeFor == NoticeFor || w.NoticeFor == NoticeForAllId) &&
                             w.NoticeType == noticeType &&
                             (w.EffectiveFrom <= today || w.EffectiveFrom == null) &&
                             (w.EffectiveTo >= today || w.EffectiveTo == null) &&
                             (w.ClassId == null || w.ClassId == 0 || w.ClassId == classId) &&
                             (w.SectionId == null || w.SectionId == 0 || w.SectionId == sectionId) &&
                             (w.CampusId == null || w.CampusId == 0 || w.CampusId == campusId) &&
                             (NewsType == noticeType || w.OrderBy != null)).OrderByDescending(o=>o.CreationDate).AsQueryable();
        }
        public List<VMNotice> GetFullNotice(int classId, int sectionId, int campusId)
        {
            var today = _dateNow.Date;//CoreServices.TruncateTime(_dateNow);
            try
            {
                var notice = _unitOfWork.NoticeRepo.Find(w => (w.NoticeFor == NoticeFor || w.NoticeFor == NoticeForAllId) &&
                                 w.NoticeType == NoticeType &&
                                 (w.EffectiveFrom <= today || w.EffectiveFrom == null) &&
                                 (w.EffectiveTo >= today || w.EffectiveTo == null) &&
                                 (w.ClassId == null || w.ClassId == 0 || w.ClassId == classId) &&
                                 (w.SectionId == null || w.SectionId == 0 || w.SectionId == sectionId) &&
                                 (w.CampusId == null || w.CampusId == 0 || w.CampusId == campusId) &&
                                  w.OrderBy != null)
                                .AsEnumerable()
                                .Select(s =>
                                {
                                    var noticeTypes = _unitOfWork.FlexValueRepo.FirstOrDefault(f => f.FlexValueId == s.NoticeType);
                                    var noticeFors = _unitOfWork.FlexValueRepo.FirstOrDefault(f => f.FlexValueId == s.NoticeFor);

                                    var noticeForText = noticeFors != null ? noticeFors.FlexValue : "";

                                    var orderBy = _unitOfWork.EmployeeRepo.FirstOrDefault(f => f.EmpHeaderId == s.OrderBy);
                                    var designation = string.Empty;
                                    var orderByName = string.Empty;

                                    if (orderBy != null)
                                    {
                                        orderByName = (orderBy.EmpFirstName + " " +
                                                       orderBy.EmpMiddleName + " " +
                                                       orderBy.EmpLastName).Replace("  ", " ");

                                        var designationFlex = _unitOfWork.FlexValueRepo.FirstOrDefault(x => x.FlexValueId == orderBy.DesignationId);

                                        designation = designationFlex != null ? designationFlex.FlexValue : "";
                                    }
                                    return noticeTypes != null ? new VMNotice
                                    {
                                        ID = s.NoticeId,
                                        Header = s.Header,
                                        Notice = s.HtmlBody,
                                        NoticeType = noticeTypes.FlexValue,
                                        NoticeFor = noticeForText,
                                        NoticeDate = s.NoticeDate,
                                        Tailer = s.Tailer,
                                        OrderBy = orderByName,
                                        Designation = designation
                                    } : null;
                                }).OrderByDescending(o => o.ID).ToList();
                return notice;
            }
            catch (Exception)
            {
                return new List<VMNotice>();
            }
        }
        public List<StudentDocumentViewModel> GetStudentDocuments(int classId, int sectionId, DateTime date)
        {
            try
            {
                return _unitOfWork.StuDocumentRepo.Find(w => w.ClassId == classId && (w.SectionId == sectionId || w.SectionId == 0 || w.SectionId == null))
                        .OrderByDescending(o => o.ActivateDate)
                        .Take(5)
                        .AsEnumerable()
                        .Select(s =>
                        {
                            var fndFlexValue = _unitOfWork.FlexValueRepo.FirstOrDefault(f => f.FlexValueId == s.TypeId);
                            return new StudentDocumentViewModel
                            {
                                Date = s.ActivateDate,
                                DocumentType = fndFlexValue != null ? fndFlexValue.FlexValue : "",
                                Link = s.Path.Replace("~", "")
                            };
                        })
                        .ToList();
            }
            catch (Exception)
            {
                return new List<StudentDocumentViewModel>();
            }
        }
        public List<StudentSubjectMarksViewModel> StudentSubjectMarks(int classId, int studentId, int sessionId)
        {
            /*   try
               {
                   var firstLog = _unitOfWork.ProcessLogRepo.Find(w => w.ClassId == classId && w.SessionId == sessionId)
                       .OrderByDescending(o => o.ProcessLogId)
                       .FirstOrDefault();

                   #region Null Check

                   if (firstLog == null)
                   {
                       return new List<StudentSubjectMarksViewModel>();
                   }
                   #endregion


                   var publishedMarks = _unitOfWork.StudentMarksRepo
                                           .Find(w => w.SessionId == sessionId && w.ClassId == classId && w.Attribute1.ToUpper() == "P")
                                           .Select(s => s.SubjectId)
                                           .ToArray();

                   var majorSubject = _unitOfWork.ClassSubjectRepo.Find(w => w.ResultSerial > 0).Select(s => s.SubjectId).ToArray();

                   var studentSubjectMarks = _unitOfWork.ProcessAssessmentMarksRepo
                                                       .Find(w => w.ProcessLogId == firstLog.ProcessLogId &&
                                                                   w.StudentHeaderId == studentId &&
                                                                   majorSubject.Contains(w.SubjectId.Value) &&
                                                                   publishedMarks.Contains(w.SubjectId.Value))
                                                       .Select(s => new StudentSubjectMarksViewModel
                                                       {
                                                           SubjectId = s.SubjectId.Value,
                                                           SubjectName = s.SubjectName,
                                                           Mark100 = s.SemesterMark.Value,
                                                           Mark150 = s.TotalObtainedMark.Value,
                                                           Marks50 = s.Total.Value
                                                       })
                                                   .Distinct()
                                                   .ToList();

                   var classExamGrade = _unitOfWork.ClassExamGradeRepo
                      .Find(w => w.ClassId == classId)
                      .Join(_unitOfWork.ExamGradeRepo, ur => ur.GradeId, r => r.GradeId, (ur, r) => r)
                      .Select(s => new
                      {
                          s.ToScore,
                          s.FromScore,
                          s.ShortDesc,
                          s.Description
                      });


                   var colors = new List<MarkColore>
               {
                   new MarkColore {Garde = "A+" , Color = "57499E"},
                   new MarkColore {Garde = "A" , Color = "3A53A4"},
                   new MarkColore {Garde = "B+" , Color = "43BFEF"},
                   new MarkColore {Garde = "B" , Color = "54B847"},
                   new MarkColore {Garde = "C" , Color = "F9EC26"},
                   new MarkColore {Garde = "D" , Color = "F7921D"},
                   new MarkColore {Garde = "U" , Color = "ED2024"}
               };

                   foreach (var g in studentSubjectMarks)
                   {
                       var avg = decimal.Round(((g.Mark150 / 150) * 100), 2, MidpointRounding.AwayFromZero);
                       g.Avg = avg;

                       var bssSubjects = _unitOfWork.SubjectRepo.FirstOrDefault(f => f.SubjectId == g.SubjectId);
                       if (bssSubjects != null) g.SubjectCode = bssSubjects.ShortName;

                       var grade = classExamGrade.FirstOrDefault(f => f.FromScore <= avg && f.ToScore >= avg);
                       if (grade != null)
                       {
                           var gradeText = grade.ShortDesc.ToUpper();
                           var color = colors.FirstOrDefault(f => f.Garde == gradeText);

                           g.Grade = gradeText;
                           if (color != null) g.color = color.Color;
                       }
                   }
                   return studentSubjectMarks;
               }
               catch
               {
                   return new List<StudentSubjectMarksViewModel>();
               }*/
            return new List<StudentSubjectMarksViewModel>();
        }
        public string StudentSubjectBarChart(List<StudentSubjectMarksViewModel> subjects)
        {
            try
            {
                var subjectsChart = "[";

                foreach (var subject in subjects)
                {
                    subjectsChart += "['" + subject.SubjectCode.Replace(Environment.NewLine, "") + "', " + subject.Avg +
                                     ", '" + "color: #" + subject.color + "'],";
                }

                if (subjectsChart != "[")
                {
                    subjectsChart = subjectsChart.Remove(subjectsChart.Length - 1);
                }
                subjectsChart += "]";

                return subjectsChart;
            }
            catch
            {
                return string.Empty;
            }
        }
        public StudentBillInfoViewModel GetStudentBillInfo(int studentId, int sessionId)
        {
            try
            {
                var bills = GetStudentBill(studentId, sessionId).Where(w=> w != null).ToList();
                var dueBill = bills.Where(w => w.RcvAmount == -1).ToList();

                var paidBill = bills.Where(w => w.RcvAmount != -1).ToList();
                var lastBillDate = string.Empty;
                if (paidBill.Any())
                {
                    var studentBillViewModel = paidBill.OrderByDescending(o => o.BillDate).FirstOrDefault();
                    if (studentBillViewModel != null)
                        if (studentBillViewModel.BillDate != null)
                        {
                            lastBillDate = studentBillViewModel.PeriodName;
                            //studentBillViewModel.BillDate.Value.ToString("MMMM yyyy");
                        }
                }

                var notice = dueBill.Any()
                    ? "<p class= \"dueAlert\">* Need to pay the due ammount or Id will be inactive</p>"
                    : "<p class= \"PaidAlert\">* All Paid</p>";

                return new StudentBillInfoViewModel
                {
                    CurrentStatus = dueBill.Count() + " Month's Dues",
                    DueAmmount = dueBill.Sum(s => s.PayableAmount ?? 0).ToString("0.##"),
                    LastPaidMonth = lastBillDate, //"December 2011",
                    Notice = notice
                };
            }
            catch
            {
                return new StudentBillInfoViewModel();
            }
        }
        public List<StudentBillViewModel> GetStudentBill(int studentId, int sessionId)
        {
            var billMaster = _unitOfWork.StudentBillMasterRepo.Find(w => w.StudentHeaderId == studentId && w.SessionId == sessionId);

            var billMasterIds = billMaster.Select(s => s.BillHeaderId).ToArray();

            var bilLine = _unitOfWork.StudentBillLineRepo.Find(w => billMasterIds.Contains(w.BillHeaderId) && _rcvTypes.Contains(w.RcvType))
                .AsEnumerable()
                .Select(s =>
                {
                    var period = _unitOfWork.FndPeriodsRepo.FirstOrDefault(ww => ww.PeriodId == s.PeriodId);
                    var master = billMaster.FirstOrDefault(f => f.BillHeaderId == s.BillHeaderId);

                    var rcvTypeFlex = _unitOfWork.FlexValueRepo.FirstOrDefault(www => www.FlexValueId.ToString() == s.RcvType);
                    var rcvType = rcvTypeFlex != null ? rcvTypeFlex.FlexValue : string.Empty;

                    var rcvState = string.Empty;
                    decimal? rcvAmount = 0;

                    if (master != null && period != null && (((s.ActualRcvAmount + s.DiscAmount) < s.BillAmount) && period.BillingLastDate <= master.BillDate))
                    {
                        rcvState = "DUE";
                    }
                    else
                    {
                        rcvAmount = s.ActualRcvAmount;
                    }

                    return master != null && period != null ? new StudentBillViewModel
                    {
                        BillDate = master.BillDate,
                        PeriodName = period.PeriodName,
                        PayableAmount = s.BillAmount,
                        RcvType = rcvType,
                        RcvAmount = rcvAmount,
                        RcvState = rcvState,
                        BillLastDate = period.BillingLastDate
                    } : null;
                })
                .ToList();

            return bilLine;
        }
        public IQueryable<AttendenceInfoViewMOdel> GetStudentAttendence(int studentId, int sessionId, string sSearch1, string sSearch2, int skip, int take)
        {
            var sem1St = _unitOfWork.PlannerEventDetailsRepo.FirstOrDefault(w => w.SessionId == sessionId && w.EventId == Semester1StEventId);
            var sem2Nd = _unitOfWork.PlannerEventDetailsRepo.FirstOrDefault(w => w.SessionId == sessionId && w.EventId == Semester2NdEventId);

            var startDate = sem1St != null ? sem1St.StartDate : _dateNow.Date;
            var endDate = sem2Nd != null ? sem2Nd.EndDate : _dateNow.Date;

            var studentAttendance = _unitOfWork.AttendanceStudentRepo.Find(w => w.StudentHeaderId == studentId && w.PunchDate >= startDate && w.PunchDate <= endDate).AsQueryable();
            if (skip != 0)
            {
                studentAttendance = studentAttendance.Skip(skip);
            }
            if (take != 0)
            {
                studentAttendance = studentAttendance.Take(take);
            }

            var attenList = studentAttendance.Select(s => new AttendenceInfoViewMOdel
            {
                Date = s.PunchDate,
                InTime = s.InTime,
                OutTime = s.OutTime,
                DoorNo = s.DoorNumber != null ? s.DoorNumber.Value : 0,
                Type = s.TimeLate == null && s.EarlyLeave == null ? "Absent" :
                            (s.TimeLate >= 0 && s.EarlyLeave > 0 ?
                                    "Present" :
                                    (s.TimeLate < 0 ? "Late" :
                                        (s.EarlyLeave < 0 ? "Early Leave" : "")))
            });

            var serial = skip + 1;
            foreach (var odel in attenList)
            {
                odel.Serial = serial;
            }

            return attenList;
        }
        public TimeSpan StringToTimeSpan(string timeString)
        {
            string[] words = timeString.Split(new char[] { ' ', '.', ':', ',', ';' });
            int hr = Int32.Parse(Regex.Match(words[0], @"\d+").Value);
            int min = Int32.Parse(Regex.Match(words[1], @"\d+").Value);

            TimeSpan span = new TimeSpan(hr, min, 0);
            return span;
        }
        public DateTime ToSystemDateTime(string dateString)
        {
            var arr = dateString.Split('-', '/');
            var value = new DateTime(Convert.ToInt16(arr[2]), Convert.ToInt16(arr[1]), Convert.ToInt16(arr[0]));
            return value;
        }
        public long GetStudentRoutinCount(int studentId, string startDate, string endDate, string routineType, int semesters, int subjectId)
        {
            var count = GetStudentRoutin(studentId, 0, 0, startDate, endDate, routineType, semesters, subjectId, true).GroupBy(g => g.Date).Count();
            if (routineType == "class")
            {
                return count * 10;
            }
            else
            {
                return count;
            }
        }
        public IEnumerable<StudentClassExamRutine> GetStudentRoutin(int studentId, int skip, int take, string startDate, string endDate, string routineType, int semesters, int subjectId, bool isCount)
        {
            var currentSession = _unitOfWork.SessionRepo.FirstOrDefault(f => f.Status == true);

            var student = _unitOfWork.StudentRepo.FirstOrDefault(f => f.StudentHeaderId == studentId);

            if (student == null || student.ClassId == null || student.SectionId == null || student.CampusId == null || currentSession == null)
            {
                return new List<StudentClassExamRutine>();
            }

            int sessionId = currentSession.SessionId;
            int classId = student.ClassId.Value;
            int sectionId = student.SectionId.Value;
            int campusId = student.CampusId.Value;

            DateTime sstartDate;
            DateTime.TryParseExact(startDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out sstartDate);

            DateTime sendDate;
            DateTime.TryParseExact(endDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out sendDate);


            if (sstartDate.Year == 1)
            {
                sstartDate = DateTime.UtcNow.AddHours(6);
            }

            return routineType == "class" ?
                GetStudentClassRoutin(sessionId, classId, sectionId, campusId, skip, take, sstartDate, sendDate, semesters, subjectId, isCount) :
                GetStudentExamRoutin(sessionId, classId, sectionId, campusId, skip, take, sstartDate, sendDate, semesters, subjectId);
        }
        public IEnumerable<StudentClassExamRutine> GetStudentClassRoutin(int sessionId, int classId, int sectionId, int campusId, int skip, int take, DateTime startDate, DateTime endDate, int semesters, int subjectId, bool isCount)
        {
            try
            {
                int?[] campusIds = { campusId, 0, null };
                var routine = _unitOfWork.ClassRoutineRepo.Find(w => w.SessionId == sessionId &&
                                                                w.ClassId == classId &&
                                                                w.SectionId == sectionId &&
                                                                campusIds.Contains(w.CampusId) &&
                                                                w.IsActive);

                var periods = _unitOfWork.ClassSectionRepo
                                        .FirstOrDefault(w => w.ClassId == classId && w.SectionId == sectionId && w.CampusId == campusId);


                if (subjectId != 0)
                {
                    routine = routine.Where(w => w.SubjectId == subjectId);
                }

                if (semesters != 0)
                {
                    routine = routine.Where(w => w.EventId == semesters);
                }

                routine = routine.Where(w => w.RoutineDate >= startDate.Date);

                if (endDate.Year != 1)
                {
                    routine = routine.Where(w => w.RoutineDate <= endDate.Date);
                }

                routine = routine.OrderBy(o => new { o.RoutineDate, o.Period });

                if (take != 0 && subjectId == 0 && !isCount)
                {
                    var index = (skip + take) / take;

                    var allDates = routine.GroupBy(g => g.RoutineDate).Select(s => s.FirstOrDefault().RoutineDate).ToArray();

                    var routineDate = allDates.Count() > 0 ? allDates[index - 1] : DateTime.Today;

                    routine = routine.Where(w => w.RoutineDate == routineDate);
                }
                else
                {
                    if (skip != 0)
                    {
                        routine = routine.Skip(skip);
                    }
                    if (take != 0)
                    {
                        routine = routine.Take(take);
                    }
                }

                var newroutine = routine
                    .AsEnumerable()
                    .Select(s =>
                    {
                        var bssSubject = _unitOfWork.SubjectRepo.FirstOrDefault(f => f.SubjectId == s.SubjectId);
                        var day = s.RoutineDate.ToString("dddd");
                        var date = s.RoutineDate.ToString("dd-MMM-yy");
                        var period = _unitOfWork.FlexValueRepo.FirstOrDefault(f => f.FlexValueId == s.Period);


                        var documentType = string.Empty;
                        var documentUrl = "#";

                        int?[] sectionids = { s.SectionId, 0, null };
                        if (s.RoutineDate <= _dateNow)
                        {
                            var docType =
                                _unitOfWork.FlexValueRepo.FirstOrDefault(f => f.FlexShortValue == ClassWorkShortCode);
                            if (docType != null)
                            {
                                documentType = docType.FlexValue;
                                var docu = _unitOfWork.StuDocumentRepo
                                    .FirstOrDefault(f => f.SessionId == sessionId &&
                                                         f.ClassId == classId &&
                                                         sectionids.Contains(f.SectionId) &&
                                                         f.ActivateDate == s.RoutineDate &&
                                                         f.TypeId == docType.FlexValueId);

                                if (docu != null) documentUrl = docu.Path;
                            }
                        }

                        var startDateTime = DateTime.Today;//DateTime.Today.Add(s.StartTime);
                        var endDateTime = DateTime.Today;//DateTime.Today.Add(s.EndTime);
                        if (periods != null)
                        {
                            var a = TimeSpan.Parse(periods.StartTime);
                            var b = TimeSpan.Parse(periods.EndTime);

                            startDateTime = DateTime.Today.Add(a);
                            endDateTime = DateTime.Today.Add(b);
                        }

                        var startTime = startDateTime.ToString("hh:mm tt");
                        var endTime = endDateTime.ToString("hh:mm tt");

                        var time = startDateTime != DateTime.Today ? startTime + " - " + endTime : "";

                        return bssSubject != null && period != null
                            ? new StudentClassExamRutine
                            {
                                Serial = period.FlexValue,
                                Date = date,
                                Day = day,
                                ClassNo = s.ClassCount.ToString(),//s.ClassCountNo.ToString(),
                                SubjectName = bssSubject.SubjectName,
                                Time = time,
                                DocumentType = documentType,
                                DocumentUrl = documentUrl
                            }
                            : period != null
                                ? new StudentClassExamRutine
                                {
                                    Serial = period.FlexValue,
                                    Date = date,
                                    Day = day,
                                    Time = time,
                                    DocumentType = "",
                                    DocumentUrl = "#"
                                }
                                : null;
                    });

                return newroutine.Where(w => w != null);
            }
            catch
            {
                return new List<StudentClassExamRutine>();
            }
        }
        public IEnumerable<StudentClassExamRutine> GetStudentExamRoutin(int sessionId, int classId, int sectionId, int campusId, int skip, int take, DateTime startDate, DateTime endDate, int semesters, int subjectId)
        {
            var currentSession = _unitOfWork.SessionRepo.FirstOrDefault(f => f.Status == true);

            var examType = _unitOfWork.PlannerEventTypeRepo.FirstOrDefault(f => f.ShortName == "EXAM");

            var examPlannerEvenTs = semesters != 0 ?

                _unitOfWork.PlannerEventRepo.Find(w => w.MasterEventId == semesters && w.EventTypeId == examType.EventTypeId).ToList() :

                _unitOfWork.PlannerEventRepo.Find(w =>
                            w.EventTypeId == examType.EventTypeId &&
                            (w.MasterEventId == Semester1StEventId || w.MasterEventId == Semester2NdEventId))
                        .ToList();

            var i = examPlannerEvenTs.Select(s => s.EventId).ToArray();

            //var eventIds = _unitOfWork.PlannerEvent.Where(w => w.EventTypeId == examType.EventTypeId && i.Contains(w.MasterEventId.Value)).Select(s => s.EventId).ToArray();

            var eventDetls = _unitOfWork.PlannerEventDetailsRepo.Find(w => w.SessionId == sessionId && i.Contains(w.EventId)).Select(s => s.EventDtlsId).ToArray();


            var routine = _unitOfWork.EventRoutineRepo.Find(w => w.ClassId == classId &&
                                                            w.SectionId == sectionId &&
                                                            w.SessionId == currentSession.SessionId &&
                                                            eventDetls.Contains(w.EventDtlsId.Value));
            if (subjectId != 0)
            {
                routine = routine.Where(w => w.SubjectId == subjectId);
            }

            routine = routine.Where(w => w.StartDate.Value.Date >= startDate.Date);

            if (endDate.Year != 1)
            {
                routine = routine.Where(w => w.StartDate.Value.Date <= endDate.Date);
            }

            routine = routine.OrderBy(o => new { o.StartDate });

            if (skip != 0)
            {
                routine = routine.Skip(skip);
            }
            if (take != 0)
            {
                routine = routine.Take(take);
            }


            var newroutine = routine
                           .AsEnumerable()
                           .Select(s =>
                           {
                               var bssSubject = _unitOfWork.SubjectRepo.FirstOrDefault(f => f.SubjectId == s.SubjectId);

                               var eventStartDate = s.StartDate;
                               var eventEndDate = s.EndDate;

                               var documentType = "Exam Paper";
                               var documentUrl = "#";

                               var day = eventStartDate != null ? eventStartDate.Value.ToString("dddd") : string.Empty;
                               var date = eventStartDate != null ? eventStartDate.Value.ToString("dd-MMM-yy") : string.Empty;
                               var startTime = eventStartDate != null ? eventStartDate.Value.ToString("hh:mm tt") : string.Empty;

                               if (eventStartDate != null && eventEndDate <= _dateNow)
                               {
                                   var docDate = DateTime.Parse(date);

                                   var docType = _unitOfWork.FlexValueRepo.FirstOrDefault(f => f.FlexShortValue == ExamPaperShortCode);
                                   if (docType != null)
                                   {
                                       documentType = docType.FlexValue;
                                       var docu = _unitOfWork.StuDocumentRepo
                                                        .FirstOrDefault(f => f.SessionId == sessionId &&
                                                                                f.ClassId == classId &&
                                                                                f.SectionId == sectionId &&
                                                                                f.ActivateDate == docDate &&
                                                                                f.TypeId == docType.FlexValueId);

                                       if (docu != null) documentUrl = docu.Path;
                                   }
                               }
                               var endTime = string.Empty;

                               if (eventEndDate != null)
                               {
                                   endTime = eventEndDate.Value.ToString("hh:mm tt");
                               }

                               return bssSubject != null ? new StudentClassExamRutine
                               {
                                   Serial = "",
                                   Date = date,
                                   Day = day,
                                   SubjectName = bssSubject.SubjectName,
                                   Time = startTime + " - " + endTime,
                                   DocumentType = documentType,
                                   DocumentUrl = documentUrl
                               } : null;
                           });

            return newroutine.Where(w => w != null);
        }
        public List<StudentCurriculumVm> GetStudentCuriculamByClassSection(int classId, int sectionId, int examId)
        {
            var currentSession = _unitOfWork.SessionRepo.FirstOrDefault(f => f.Status == true);

            if (currentSession != null)
            {
                var sessionId = currentSession.SessionId;

                var allClassSectionStudent = _unitOfWork.StudentRepo.Find(w => w.ClassId == classId && w.SectionId == sectionId && w.Status == true);

                var cur = _unitOfWork.StudentCurriculumResultRepo.Find(w => w.SessionId == sessionId && w.ExamId == examId).ToList();

                var curriculum =_unitOfWork.FlexValueRepo 
                    .Find(w => w.FlexValueSetId == CurriculumFlexId && w.FlexValueId!=AchivementFlexValueId)
                    .Select(s => new
                    {
                        Id = s.FlexValueId,
                        Name = s.FlexValue
                    })
                    .ToList();

                var curiculam = allClassSectionStudent.AsEnumerable().Select(s => new StudentCurriculumVm
                {
                    StudentHeaderId = s.StudentHeaderId,
                    StudentId = s.StudentId,
                    Name = s.FirstName + " " + s.MiddleName + " " + s.LastName,
                    CurriculumVms = curriculum.Select(ss =>
                    {
                        var bssStudentCurriculumResult = cur.FirstOrDefault(f => f.StudentHeaderId == s.StudentHeaderId && f.CurriculumId == ss.Id);
                        return new CurriculumVm
                        {
                            CurriculumId = ss.Id,
                            GradeId = bssStudentCurriculumResult != null ? bssStudentCurriculumResult.GradeId : 0
                        };
                    }).ToList()
                }
                    )
                    .ToList();

                return curiculam;
            }
            return new List<StudentCurriculumVm>();
        }
        public string StudentCuriculamAddOrUpdate(VmCurricullamSave postCurricullam, int userId)
        {
            string msg;
            var currentSession = _unitOfWork.SessionRepo.FirstOrDefault(f => f.Status == true);

            var examId = postCurricullam.EventId;

            if (currentSession != null)
            {
                var sessionId = currentSession.SessionId;

                foreach (var vmCurricullamSave in postCurricullam.StudentCurricullams)
                {
                    var studentId = vmCurricullamSave.StudentId;

                    var studentCurriculumns = _unitOfWork.StudentCurriculumResultRepo.Find(w => w.SessionId == sessionId && w.ExamId == examId && w.StudentHeaderId == studentId);

                    foreach (var vmCurricullamList in vmCurricullamSave.CurricullamList)
                    {
                        var curriculumId = vmCurricullamList.Id;
                        var gardeId = vmCurricullamList.val;

                        var studentCurriculumn = studentCurriculumns.FirstOrDefault(a => a.CurriculumId == curriculumId);

                        if (gardeId != 0)
                        {
                            if (studentCurriculumn != null)
                            {
                                studentCurriculumn.GradeId = gardeId;
                                studentCurriculumn.LastUpdateBy = userId;
                                studentCurriculumn.LastUpdateDate = _dateNow;
                            }
                            else
                            {
                                _unitOfWork.StudentCurriculumResultRepo.Add(new StudentCurriculumResult
                                {
                                    StudentHeaderId = studentId,
                                    SessionId = sessionId,
                                    CurriculumId = curriculumId,
                                    GradeId = gardeId,
                                    ExamId = examId,
                                    CreatedBy = userId,
                                    CreationDate = _dateNow
                                });
                            }
                        }
                    }
                }
                msg = "Curriculum Saved";
                _unitOfWork.SaveChanges();
            }
            else
            {
                msg = "session not found";
            }


            return msg;
        }
        public List<VmAttendanceStudent> StudentAttendenceList(string cardNumber, string startDate, string endDate, int take, int skip)
        {/*
            var cardName = new SqlParameter("@CardNumber", cardNumber);
            var paramStartDate = new SqlParameter("@StartDate", startDate);
            var paramEndDate = new SqlParameter("@EndDate", endDate);
            var paramTake = new SqlParameter("@TAKE", (object)take);
            var paramskip = new SqlParameter("@SKIP", (object)skip);

            var attendanceStudent = _db.Database.SqlQuery<VmAttendanceStudent>("exec dbo.[GetStudentAttendanceByCard] @CardNumber, @StartDate, @EndDate, @TAKE, @SKIP",
                cardName, paramStartDate, paramEndDate, paramTake, paramskip).ToList();
            return attendanceStudent;*/
            return new List<VmAttendanceStudent>();
        }
        public List<StudentOtherInfo> GetStudentOtherInfo(int classId, int sectionId, int examId)
        {
            var currentSession = _unitOfWork.SessionRepo.FirstOrDefault(f => f.Status == true);

            if (currentSession != null)
            {
                var sessionId = currentSession.SessionId;
                var studentIds =
                    _unitOfWork.StudentRepo.Find(w => w.Status == true && w.ClassId == classId && w.SectionId == sectionId)
                        .Select(s => s.StudentHeaderId)
                        .ToArray();

                var entryPoint = (from s in _unitOfWork.StudentRepo.Find(w=>w.Status == true && studentIds.Contains(w.StudentHeaderId))
                                  join si in _unitOfWork.StudentOtherInfoRepo.Find(w => w.SessionId == sessionId && w.EventId == examId && studentIds.Contains(w.StudentHeaderId))
                                  on s.StudentHeaderId equals si.StudentHeaderId into ps
                                  from si in ps.DefaultIfEmpty()
                                  select new StudentOtherInfo
                                  {
                                      Name = s.FirstName + " " + s.MiddleName + " " + s.LastName,
                                      StudentHeaderId = s.StudentHeaderId,
                                      StudentId = s.StudentId,
                                      WorkDay = si != null ? si.TotalWorkingDay : 0,
                                      Absent = si != null ? si.TotalAbsent : 0,
                                      Late = si != null ? si.TotalLate : 0,
                                      Height = si != null ? si.Height : 0,
                                      Weight = si != null ? si.Weight : 0,
                                      Remarks = si != null ? si.Attribute1 : "",
                                  }).ToList();


                //var i = _unitOfWork.Student.



                return entryPoint;
            }
            return new List<StudentOtherInfo>();
        }
        public string StudentInfoAddOrUpdate(StudentOtherInfoSave postCurricullam, int userId)
        {
            string msg;
            var currentSession = _unitOfWork.SessionRepo.FirstOrDefault(f => f.Status == true);

            if (currentSession != null)
            {
                var sessionId = currentSession.SessionId;
                var examId = postCurricullam.ExamId;
                var studentIds = postCurricullam.StudentOtherInfo.Select(s => s.StudentHeaderId).ToArray();
                var oldInfo = _unitOfWork.StudentOtherInfoRepo.Find(w => w.SessionId == sessionId && w.EventId == examId && studentIds.Contains(w.StudentHeaderId));

                foreach (var studentOtherInfo in postCurricullam.StudentOtherInfo)
                {
                    var studentOldInfo = oldInfo.FirstOrDefault(f => f.StudentHeaderId == studentOtherInfo.StudentHeaderId);
                    if (studentOldInfo != null)
                    {
                        studentOldInfo.TotalWorkingDay = studentOtherInfo.WorkDay;
                        studentOldInfo.TotalAbsent = studentOtherInfo.Absent;
                        studentOldInfo.TotalLate = studentOtherInfo.Late;
                        studentOldInfo.Height = studentOtherInfo.Height;
                        studentOldInfo.Weight = studentOtherInfo.Weight;
                        studentOldInfo.Attribute1 = studentOtherInfo.Remarks != null
                            ? studentOtherInfo.Remarks
                            : string.Empty;
                    }
                    else
                    {
                        if (studentOtherInfo.WorkDay != 0)
                        {
                            _unitOfWork.StudentOtherInfoRepo.Add(new Entity.Model.StudentOtherInfo
                            {
                                SessionId = sessionId,
                                EventId = examId,
                                StudentHeaderId = studentOtherInfo.StudentHeaderId,
                                TotalWorkingDay = studentOtherInfo.WorkDay,
                                TotalAbsent = studentOtherInfo.Absent,
                                TotalLate = studentOtherInfo.Late,
                                Height = studentOtherInfo.Height,
                                Weight = studentOtherInfo.Weight,
                                Attribute1 = studentOtherInfo.Remarks != null ? studentOtherInfo.Remarks : string.Empty
                            });
                        }
                    }
                }

                _unitOfWork.SaveChanges();
            }
                return "Saved";
        }
        public List<VmStudentData> GetStudents(string search, int start, int displaylength, int? ChangeSessionId, int? ChangeClassId, int? ChangeSectionId, out int totalLength)
        {
            if (displaylength == -1) { displaylength = Int32.MaxValue; }
            var student = _unitOfWork.StudentRepo.GetAll().Select(s=> new {
                s.StudentHeaderId,
                s.StudentId,
                s.RollNumber,
                s.FirstName,
                s.MiddleName,
                s.LastName,
                s.StudentPhotoPath,
                s.ClassId,
                s.SessionId,
                s.SectionId,
                s.StatusId,
                s.Shift,
                s.Gender,
                s.FatherFirstName,
                s.FatherMiddleName,
                s.FatherLastName,
                s.MotherFirstName,
                s.MotherMiddleName,
                s.MotherLastName,
                s.PresHoldingNo,
                s.PresArea,
                s.PresStreet,
                s.PresThanaId,
                s.PresCityId,
                s.PermHoldingNo,
                s.PermArea,
                s.PermStreet,
                s.PermThanaId,
                s.PermCityId,
                s.Mobile,
                s.BirthDate,
                s.Religion,
                s.Nationality,
                s.BloodGroup
            }).AsQueryable();
            
            if (!string.IsNullOrEmpty(search))
            {
                student = student.Where(w =>
                (!string.IsNullOrEmpty(w.FirstName) && w.FirstName.ToLower().Contains(search.ToLower())) ||
                (!string.IsNullOrEmpty(w.StudentId) && w.StudentId.ToLower().Contains(search.ToLower()))).AsQueryable();
            }

            if (ChangeSessionId > 0 && ChangeClassId == 0 && (ChangeSectionId == 0 || ChangeSectionId == null))
            {
                student = student.Where(s => s.SessionId == ChangeSessionId).AsQueryable();
            }

            else if (ChangeSessionId == 0 && ChangeClassId > 0 && (ChangeSectionId == 0 || ChangeSectionId == null))
            {
                student = student.Where(s => s.ClassId == ChangeClassId).AsQueryable();
            }

            else if (ChangeSessionId > 0 && ChangeClassId > 0 && (ChangeSectionId == 0 || ChangeSectionId == null))
            {
                student = student.Where(s => s.ClassId == ChangeClassId && s.SessionId == ChangeSessionId).AsQueryable();
            }

            else if (ChangeSessionId > 0 && ChangeClassId > 0 && ChangeSectionId > 0)
            {
                student = student.Where(s => s.SessionId == ChangeSessionId && s.ClassId == ChangeClassId && s.SectionId == ChangeSectionId).AsQueryable();
            }

            else if (ChangeSessionId == 0 && ChangeClassId > 0 && ChangeSectionId > 0)
            {
                student = student.Where(s => s.ClassId == ChangeClassId && s.SectionId == ChangeSectionId).AsQueryable();
            }

            var sessionRepo = _unitOfWork.SessionRepo.GetAll().Select(s => new { s.SessionId, s.SessionName }).ToList();
            var classRepo = _unitOfWork.ClassRepo.GetAll().Select(s => new { s.ClassId, s.ClassName }).ToList();
            var sectionRepo = _unitOfWork.SectionRepo.GetAll().Select(s => new { s.SectionId, s.SectionName }).ToList();
            var flexvalueRepo = _unitOfWork.FlexValueRepo.GetAll().Select(s => new { s.FlexValueId, s.FlexValue }).ToList();
            var sl = 1;
            var result = student.OrderBy(o => o.ClassId).OrderBy(o => o.SectionId).OrderBy(o => o.RollNumber).AsEnumerable().Skip(start).Take(displaylength)
                .Select(s=> new { 
                    data = s,
                    sessionRepo = sessionRepo.FirstOrDefault(c => c.SessionId == s.SessionId),
                    classeRepo = classRepo.FirstOrDefault(c => c.ClassId == s.ClassId),
                    sectionRepo = sectionRepo.FirstOrDefault(c=> c.SectionId == s.SectionId),
                    presThana = flexvalueRepo.FirstOrDefault(f => f.FlexValueId == s.PresThanaId),
                    presCity = flexvalueRepo.FirstOrDefault(f => f.FlexValueId == s.PresCityId),
                    permThana = flexvalueRepo.FirstOrDefault(f => f.FlexValueId == s.PermThanaId),
                    permCity = flexvalueRepo.FirstOrDefault(f => f.FlexValueId == s.PermCityId)
                })
                .Select(s => new VmStudentData
            {
                SerialNo = sl++,
                SassionName = s.sessionRepo != null ? s.sessionRepo.SessionName : "",
                ClassName = s.classeRepo != null ? s.classeRepo.ClassName : "",
                Section = s.sectionRepo != null ? s.sectionRepo.SectionName : "",
                StudentHeaderId = s.data.StudentHeaderId,
                StudentId = s.data.StudentId,
                RollNumber = s.data.RollNumber,
                FirstName = s.data.FirstName + " " + s.data.MiddleName + " " + s.data.LastName,
                StudentPhotoPath = s.data.StudentPhotoPath,
                StudentStatus = flexvalueRepo.FirstOrDefault(f=>f.FlexValueId == s.data.StatusId) != null ? flexvalueRepo.FirstOrDefault(f => f.FlexValueId == s.data.StatusId).FlexValue: "",
                ShiftName = flexvalueRepo.FirstOrDefault(f => f.FlexValueId == s.data.Shift) != null ? flexvalueRepo.FirstOrDefault(f => f.FlexValueId == s.data.Shift).FlexValue : "",
                Gender = s.data.Gender,
                FatherFirstName = s.data.FatherFirstName + " " + s.data.FatherMiddleName + " " + s.data.FatherLastName,
                MotherFirstName = s.data.MotherFirstName + " " + s.data.MotherMiddleName + " " + s.data.MotherLastName,
                PresArea = s.data.PresHoldingNo + "," 
                    + s.data.PresArea + "," 
                    + s.data.PresStreet + "," 
                    + (s.presThana != null ? s.presThana.FlexValue: "") + "," 
                    + (s.presCity != null ? s.presCity.FlexValue:""),
                PermArea = s.data.PermHoldingNo + "," 
                    + s.data.PermArea + "," 
                    + s.data.PermStreet + "," 
                    + (s.permThana != null ? s.permThana.FlexValue : "") + "," 
                    + (s.permCity != null ? s.permCity.FlexValue : ""),
                Mobile = s.data.Mobile,
                BirthDate = s.data.BirthDate.Value.ToString("dd/MM/yyyy"),
                Religion = s.data.Religion,
                Nationality = s.data.Nationality,
                BloodGroup = s.data.BloodGroup,
            }).ToList();

            totalLength = student.Count();
            return result;
        }
        public VmStudentData StudentInfo(int studentheaderid)
        {
            var student = _unitOfWork.StudentRepo.FirstOrDefault(f => f.StudentHeaderId == studentheaderid);
            var sessionRepo = _unitOfWork.SessionRepo.GetAll().Select(s => new { s.SessionId, s.SessionName }).ToList();
            var classRepo = _unitOfWork.ClassRepo.GetAll().Select(s => new { s.ClassId, s.ClassName }).ToList();
            var sectionRepo = _unitOfWork.SectionRepo.GetAll().Select(s => new { s.SectionId, s.SectionName }).ToList();
            var flexvalueRepo = _unitOfWork.FlexValueRepo.GetAll().Select(s => new { s.FlexValueId, s.FlexValue }).ToList();
            var studentdata = new VmStudentData();
            studentdata.StudentHeaderId = student.StudentHeaderId;
            studentdata.StudentId = student.StudentId;
            studentdata.FirstName = student.FirstName;
            studentdata.MiddleName = student.MiddleName;
            studentdata.LastName = student.LastName;
            studentdata.SassionName = sessionRepo.FirstOrDefault(f => f.SessionId == student.SessionId) != null ? sessionRepo.FirstOrDefault(f => f.SessionId == student.SessionId).SessionName : "";
            studentdata.NickName = student.NickName;
            studentdata.PunchCardNo = student.PunchCardNo ;
            studentdata.PresHoldingNo = student.PresHoldingNo ;
            studentdata.PresStreet = student.PresStreet;
            studentdata.ClassName= classRepo.FirstOrDefault(f => f.ClassId == student.ClassId) != null ? classRepo.FirstOrDefault(f => f.ClassId == student.ClassId).ClassName : "";
            studentdata.ShiftName = flexvalueRepo.FirstOrDefault(f => f.FlexValueId == student.Shift) != null ? flexvalueRepo.FirstOrDefault(f => f.FlexValueId == student.Shift).FlexValue : "";
            studentdata.Section= sectionRepo.FirstOrDefault(f => f.SectionId == student.SectionId) != null ? sectionRepo.FirstOrDefault(f => f.SectionId == student.SectionId).SectionName : "";
            studentdata.StudentStatus = flexvalueRepo.FirstOrDefault(f => f.FlexValueId == student.StatusId) != null ? flexvalueRepo.FirstOrDefault(f => f.FlexValueId == student.StatusId).FlexValue : "";
            studentdata.PresArea = student.PresArea;
            studentdata.Age = student.Age;
            studentdata.PresThanaId = student.PresThanaId;
            studentdata.PresPostCode = student.PresPostCode;
            studentdata.PresCityId = student.PresCityId;
            studentdata.DeviceNumber = student.DeviceNumber;
            studentdata.RollNumber = student.RollNumber;
            studentdata.PresDivisionId = student.PresDivisionId;
            studentdata.PresCountryId = student.PresCountryId;
            studentdata.PermHoldingNo = student.PermHoldingNo;
            studentdata.PermStreet = student.PermStreet;
            studentdata.PermArea= student.PermArea;
            studentdata.PermPostCode= student.PermPostCode;
            studentdata.PermThanaId= student.PermThanaId;
            studentdata.PermCityId= student.PermCityId;
            studentdata.PermDivisionId= student.PermDivisionId;
            studentdata.PermCountryId= student.PermCountryId;
            studentdata.Dob= student.Dob;
            studentdata.BirthCity= student.BirthCity;
            studentdata.BirthCountry= student.BirthCountry;
            studentdata.PlaceOfBirth= student.PlaceOfBirth;
            studentdata.AdmissionDate = student.AdmissionDate != null ? student.AdmissionDate.Value.ToString("dd/MM/yyyy") : "";
            studentdata.ReadmissionDate = student.ReadmissionDate != null ? student.ReadmissionDate.Value.ToString("dd/MM/yyyy") : "";
            studentdata.AdmissionSessionId= student.AdmissionSessionId;
            studentdata.ReadmissionSessionId= student.ReadmissionSessionId;
            studentdata.AdmissionClassId= student.AdmissionClassId;
            studentdata.AdmissionSectionId= student.AdmissionSectionId;
            studentdata.AdmissionShift= student.AdmissionShift;
            studentdata.SessionId= student.SessionId;
            studentdata.ClassId= student.ClassId;
            studentdata.SectionId= student.SectionId;
            studentdata.Shift= student.Shift;
            studentdata.CampusId= student.CampusId;
            studentdata.EndDate= student.EndDate != null ? student.EndDate.Value.ToString("dd/MM/yyyy") : "";
            studentdata.BirthDate= student.BirthDate != null ? student.BirthDate.Value.ToString("dd/MM/yyyy") : "";
            studentdata.Status= student.Status;
            studentdata.StatusId= student.StatusId ?? 0;
            studentdata.Gender= student.Gender;
            studentdata.GenderId= student.GenderId ?? 0;
            studentdata.Religion= student.Religion;
            studentdata.ReligionId= student.ReligionId;
            studentdata.ResPhone= student.ResPhone;
            studentdata.Mobile= student.Mobile;
            studentdata.Email= student.Email;
            studentdata.LocGuardianOfficeAddress= student.LocGuardianOfficeAddress;
            studentdata.BloodGroup= student.BloodGroup;
            studentdata.BloodId= student.BloodId;
            studentdata.Nationality= student.Nationality;
            studentdata.NationalityId= student.NationalityId;
            studentdata.BirthCertificateNo= student.BirthCertificateNo;
            studentdata.PassportNo= student.PassportNo;
            studentdata.MedicalHistory= student.MedicalHistory;
            studentdata.EmergencyMedicalAction= student.EmergencyMedicalAction;
            studentdata.FatherFirstName= student.FatherFirstName;
            studentdata.FatherMiddleName= student.FatherMiddleName;
            studentdata.FatherLastName= student.FatherLastName;
            studentdata.FatherEducation= student.FatherEducation;
            studentdata.FatherEducationDetails= student.FatherEducationDetails;
            studentdata.FatherOccupation= student.FatherOccupation;
            studentdata.FatherOccupationDetails= student.FatherOccupationDetails;
            studentdata.FatherOfficeAddress= student.FatherOfficeAddress;
            studentdata.FatherOfficePhone= student.FatherOfficePhone;
            studentdata.FatherMobile= student.FatherMobile;
            studentdata.FatherNationalId= student.FatherNationalId;
            studentdata.FatherPassportNo= student.FatherPassportNo;
            studentdata.MotherFirstName= student.MotherFirstName;
            studentdata.MotherMiddleName= student.MotherMiddleName;
            studentdata.MotherLastName= student.MotherLastName;
            studentdata.MotherEducation= student.MotherEducation;
            studentdata.MotherEducationDetails= student.MotherEducationDetails;
            studentdata.MotherOccupation= student.MotherOccupation;
            studentdata.MotherOccupationDetails= student.MotherOccupationDetails;
            studentdata.MotherOfficeAddress= student.MotherOfficeAddress;
            studentdata.MotherOfficePhone= student.MotherOfficePhone;
            studentdata.MotherMobile= student.MotherMobile;
            studentdata.MotherNationalId= student.MotherNationalId;
            studentdata.MoherPassportNo= student.MoherPassportNo;
            studentdata.LocGuardianFirstName= student.LocGuardianFirstName;
            studentdata.LocGuardianMiddleName= student.LocGuardianMiddleName;
            studentdata.LocGuardianLastName= student.LocGuardianLastName;
            studentdata.LocGuardianRelationship= student.LocGuardianRelationship;
            studentdata.LocGuardianMobile= student.LocGuardianMobile;
            studentdata.LocalGurdianNationalId= student.LocalGurdianNationalId;
            studentdata.LocalGurdianPassportNo= student.LocalGurdianPassportNo;
            studentdata.StudentPhotoPath= student.StudentPhotoPath;
            studentdata.ProximityNum= student.ProximityNum;
            studentdata.ExitReasons= student.ExitReasons;

            return studentdata;

        }
        public List<VmStuCurrentNoticeList> StudentNoticelist(int studentheaderid)
        {
            var student = _unitOfWork.StudentRepo.FirstOrDefault(f => f.StudentHeaderId == studentheaderid);
            var notices = _unitOfWork.NoticeRepo.GetAll().OrderByDescending(s => s.NoticeId).ToList();
            var vmStuCurrentNoticeLists = new List<VmStuCurrentNoticeList>();
            foreach (var v in notices)
            {
                var currentTime = DateTime.Now;
                var currentDate = DateTime.Now.Date;
                if ((v.EffectiveFrom != null && v.EffectiveFrom.Value.Date <= currentDate) && (v.EffectiveTo == null || (v.EffectiveTo.Value.Date >= currentDate)))
                {

                    if ((v.CampusId != 0 && v.CampusId == student.CampusId) || v.CampusId == 0)
                    {
                        if ((v.ShiftId != 0 && v.ShiftId == student.Shift) || v.ShiftId == 0)
                        {
                            if ((v.ClassId != 0 && v.ClassId == student.ClassId) || v.ClassId == 0)
                            {
                                if ((v.SectionId != 0 && v.ClassId == student.ClassId && v.SectionId == student.SectionId) || v.SectionId == 0)
                                {
                                    VmStuCurrentNoticeList vmStuCurrentNoticeList = new VmStuCurrentNoticeList();
                                    vmStuCurrentNoticeList.NoticeId = v.NoticeId;
                                    vmStuCurrentNoticeList.NoticeFor = _flexService.GetFlexValueByFlexValueId(v.NoticeFor ?? 0);
                                    vmStuCurrentNoticeList.NoticeType = _flexService.GetFlexValueByFlexValueId(v.NoticeType ?? 0);
                                    vmStuCurrentNoticeList.Header = v.Header;
                                    vmStuCurrentNoticeList.Body = v.Body;
                                    vmStuCurrentNoticeList.EffectiveFrom = v.EffectiveFrom != null ? v.EffectiveFrom.Value.ToString("dd/MM/yyyy") : "";
                                    vmStuCurrentNoticeList.EffectiveTo = v.EffectiveTo != null ? v.EffectiveTo.Value.ToString("dd/MM/yyyy") : "";
                                    vmStuCurrentNoticeLists.Add(vmStuCurrentNoticeList);
                                }
                            }
                        }
                    }

                }

            }
            return vmStuCurrentNoticeLists;
        }
        public List<VmDocumentList> StudentDocumentlist(int studentheaderid)
        {
            var student = _unitOfWork.StudentRepo.FirstOrDefault(f => f.StudentHeaderId == studentheaderid);
            var documents = _unitOfWork.StuDocumentRepo.GetAll().OrderByDescending(s => s.DocId).ToList();
            var vmDocumentLists = new List<VmDocumentList>();
            var currentSession = _unitOfWork.SessionRepo.FirstOrDefault(s => s.Status == true);
            var currentDate = DateTime.Now.Date;

            foreach (var s in documents)
            {
                if ((s.ActivateDate != null && s.ActivateDate <= currentDate) && (s.EffectiveTo == null || (s.EffectiveTo >= currentDate)))
                {
                    if (s.ClassId != 0 && s.ClassId == student.ClassId)
                    {
                        if (s.ShiftId == 0 || (s.ShiftId != 0 && s.ShiftId == student.Shift))
                        {
                            if (s.SectionId == 0 || (s.SectionId != 0 && s.SectionId == student.SectionId && s.ClassId == student.ClassId))
                            {
                                VmDocumentList vmDocumentList = new VmDocumentList();
                                vmDocumentList.DocId = s.DocId;
                                vmDocumentList.DocumentName = _flexService.GetFlexValueByFlexValueId(s.TypeId ?? 0);
                                vmDocumentList.PublishDate = s.ActivateDate != null ? s.ActivateDate.Value.ToString("dd/MM/yyyy") : "";
                                vmDocumentList.EffectiveTo = s.EffectiveTo != null ? s.EffectiveTo.Value.ToString("dd/MM/yyyy") : "";
                                vmDocumentList.SubjectName = _unitOfWork.SubjectRepo.FirstOrDefault(a => a.SubjectId == s.SubjectId) != null ? _unitOfWork.SubjectRepo.FirstOrDefault(a => a.SubjectId == s.SubjectId).SubjectName : "";
                                vmDocumentList.FileName = s.Attribute1 != null ? s.Attribute1 : "";
                                vmDocumentList.Path = s.Path;
                                vmDocumentLists.Add(vmDocumentList);
                            }
                        }
                    }
                }
            }
            return vmDocumentLists;
        }
        public List<VmRoutineList> GetRoutineData(int? studentheaderid)
        {
            var student = _unitOfWork.StudentRepo.FirstOrDefault(s => s.StudentHeaderId == studentheaderid);
            if(student != null)
            {
                var routine = _unitOfWork.ClassSectionRoutineRepo.Find(w => w.ClassId == student.ClassId && w.SectionId == student.SectionId && w.SessionId == student.SessionId).ToList()
               .GroupBy(g => g.DayInTheWeek)
               .Select(s => new VmRoutineList
               {

                   DayOfWeek = s.Key,
                   Day = Enum.GetName(typeof(DayOfWeek), s.Key),
                   Data = s.Select(ss => new VmRoutineData
                   {
                       RoutineId = ss.RoutineId,
                       SubjectId = ss.SubjectId,
                       SubjectName = ss.Subject.SubjectName,
                       StartTime = ss.StartTime,
                       EndTime = ss.EndTime,
                       Teachernames = teacherService.GetTeacherName(ss.SubjectId ?? 0)

                   }).ToList()
               }).OrderBy(o => o.DayOfWeek).ToList();
              return routine;
            }
            return new List<VmRoutineList>();
        }
        public List<VmStudentAttendance> StudentAtendance(int studentheaderid ,string FromDate , string ToDate)
        {
            var schooldata = _unitOfWork.SchoolRepo.FirstOrDefault();
            var studentinfo = _unitOfWork.StudentRepo.FirstOrDefault(s => s.StudentHeaderId == studentheaderid);
            var stuAttendanceList = new List<VmStudentAttendance>();
            if (studentinfo != null)
            {
                var stuAttendanceData = _unitOfWork.AttendancesRepo.Find(s => s.CardNumber == studentinfo.PunchCardNo && s.DoorNumber == studentinfo.DeviceNumber).ToList();

                //var studentHeaderId = _user.ReferrenceId.Value;
                //var student = _unitOfWork.Student.FirstOrDefault(x => x.StudentHeaderId == studentHeaderId);
                //var attendance = _unitOfWork.Attendance.Where(s=>s.CardNumber == student.StudentId).ToList();


                var startdate = DateTime.Now;
                var enddate = DateTime.Now.Date;

                if (!string.IsNullOrEmpty(FromDate) && string.IsNullOrEmpty(ToDate))
                    startdate = Convert.ToDateTime(DateTimeHelperService.ConvertBDDateStringToDateTimeObject(FromDate)).Date;

                if (string.IsNullOrEmpty(FromDate) && !string.IsNullOrEmpty(ToDate))
                    enddate = Convert.ToDateTime(DateTimeHelperService.ConvertBDDateStringToDateTimeObject(ToDate)).Date;

                if (!string.IsNullOrEmpty(FromDate) && !string.IsNullOrEmpty(ToDate))
                {
                    startdate = Convert.ToDateTime(DateTimeHelperService.ConvertBDDateStringToDateTimeObject(FromDate)).Date;
                    enddate = Convert.ToDateTime(DateTimeHelperService.ConvertBDDateStringToDateTimeObject(ToDate)).Date;
                }

                var currentMonth = DateTime.Now.Month;
                var daydistance = (enddate - startdate).TotalDays;
                var atndata = _unitOfWork.AttendanceChangeRequistsRepo.GetAll().OrderByDescending(s => s.AttendanceChangeRequistId);

                for (var i = 0; i <= daydistance; i++)
                {
                    var date = startdate.AddDays(i);
                    DateTime PunchDate = Convert.ToDateTime(date.ToString("yyyy-MM-dd HH:mm:ss.fff"));

                    VmStudentAttendance atnDetails = new VmStudentAttendance();

                    atnDetails.PunchDate = PunchDate.ToString("dd/MM/yyyy");
                    atnDetails.DateFormateForToday = PunchDate.ToString("MM/dd/yyyy");
                    atnDetails.PunchDateWithoutConvert = PunchDate;

                    var atnData = studentAttendanceService.GetDayAttendanceData(date, studentinfo.StudentHeaderId);

                    atnDetails.InTime = atnData.InTime;
                    atnDetails.OutTime = atnData.OutTime;
                    atnDetails.ClassInTime = atnData.StartTime;
                    atnDetails.ClassOutTime = atnData.EndTime;
                    atnDetails.PreviousClassInTime = stuAttendanceData.Any() ? stuAttendanceData.FirstOrDefault().PunchTime : "";
                    atnDetails.PreviousClassOutTime = stuAttendanceData.Any() ? stuAttendanceData.OrderByDescending(s => s.AttendanceId).FirstOrDefault().PunchTime : "";

                    atnDetails.CurrentStatusInTime = (int)atnData.RequestedInTimeStatus;
                    atnDetails.CurrentStatusOutTime = (int)atnData.RequestedOutTimeStatus;

                    atnDetails.RequistedInTime = atnData.RequestedInTime;
                    atnDetails.RequistedOutTime = atnData.RequestedOutTime;

                    atnDetails.RequestedInTimeHeaderId = atnData.RequestedInTimeHeaderId;
                    atnDetails.RequestedOutTimeHeaderId = atnData.RequestedOutTimeHeaderId;

                    atnDetails.ApprovedInTime = atnData.ApprovedInTime;
                    atnDetails.ApprovedOutTime = atnData.ApprovedOutTime;

                    atnDetails.LateTime = atnData.LateTime;
                    atnDetails.EarlyOut = atnData.EarlyOut;
                    atnDetails.OverTime = atnData.OverTime;
                    atnDetails.StudyHourDistance = atnData.StudyHourDistance;

                    atnDetails.IsAllowedEditIfIntimeOuttimeRequestPending = schooldata != null ? schooldata.IsAllowedEditIfIntimeOuttimeRequestPending != null ? schooldata.IsAllowedEditIfIntimeOuttimeRequestPending : false : false;

                    stuAttendanceList.Add(atnDetails);
                }
                return stuAttendanceList;
            }
            return new List<VmStudentAttendance>();
        }
        public List<VmBillLinelist> StudentBillinglist(int studentheaderid ,int feeSubCategoryId)
        {
            var student = _unitOfWork.StudentRepo.FirstOrDefault(s => s.StudentHeaderId == studentheaderid);
            var classlist = _unitOfWork.ClassRepo.GetAllQueryable();
            var secionlist = _unitOfWork.SectionRepo.GetAllQueryable();
            var sessionList = _unitOfWork.SessionRepo.GetAllEnumerable();
            var billPeriod = _unitOfWork.HrBillPeriodWisePayRollIssueDatesRepo.GetAllQueryable();
            var billmaster = _unitOfWork.StudentBillMasterRepo.GetAllEnumerable();
            var feeSubCategory = _unitOfWork.FeeSubCategoryRepo.GetAllQueryable();
            var billine = _unitOfWork.StudentBillLineRepo.GetAllEnumerable();
            
            if (student != null)
            {
                var studentExistsOrNot = billmaster.Where(s => s.StudentHeaderId == student.StudentHeaderId).ToList();

                if (studentExistsOrNot.Any())
                {
                    List<VmBillLinelist> BillLinelist = new List<VmBillLinelist>();

                    foreach (var item in studentExistsOrNot)
                    {
                        var BillList = billine.Where(d => d.BillDtlId == item.BillHeaderId && d.FeeSubCategoryId == feeSubCategoryId).Select(ss => new VmBillLinelist
                        {
                            BillDate = GetBillDate(ss.BillHeaderId),
                            Period = GetBillPeriod(ss.BillHeaderId),
                            Category = feeSubCategory.FirstOrDefault(dd => dd.FeeSubCategoryId == ss.FeeSubCategoryId) != null ? feeSubCategory.FirstOrDefault(dd => dd.FeeSubCategoryId == ss.FeeSubCategoryId).FeeSubCategoryName : "",
                            Amount = ss.ActualRcvAmount,
                            Discount = ss.DiscAmount
                        }).ToList();

                        BillLinelist.AddRange(BillList);
                    }
                  return BillLinelist;

                }

            }
            return new List<VmBillLinelist>();
        }
        public List<VmStuLeaveApplyList> StudentLeaveApplyList(int studentheaderid)
        {
            var student = _unitOfWork.StudentRepo.FirstOrDefault(f => f.StudentHeaderId == studentheaderid);
        
            var leaveApply = _unitOfWork.StudentLeaveApplicationsRepo.Find(s => s.StudentHeaderId == student.StudentHeaderId).OrderByDescending(o => o.ApplicationId).ToList();

            var allValues = leaveApply.Select(s => new VmStuLeaveApplyList()
            {
                ApplicationId = s.ApplicationId,
                LeaveCategory = _unitOfWork.FlexValueRepo.FirstOrDefault(f => f.FlexValueId == s.LeaveCategoryId) != null ? _unitOfWork.FlexValueRepo.FirstOrDefault(f => f.FlexValueId == s.LeaveCategoryId).FlexValue : "",
                LeaveCategoryId = s.LeaveCategoryId,
                Subject = s.Subject,
                LeaveStartDate = s.LeaveStartDate.ToString("dd/MM/yyyy hh:mm tt"),
                LeaveEndDate = s.LeaveEndDate.ToString("dd/MM/yyyy hh:mm tt"),
                LeaveApplyDate = s.LeaveApplyDate.ToString("dd/MM/yyyy hh:mm tt"),
                LeaveStatus = s.LeaveStatus.ToString(),
                Comment = s.Comment,
            }).ToList();

            return allValues;
        }
        public List<VmStudentDocument> StudentdatewiseDocumentlist(int studentheaderid, string SelectDate)
        {
            var student = _unitOfWork.StudentRepo.FirstOrDefault(s => s.StudentHeaderId == studentheaderid);

            var currentSelectDate = DateTimeHelperService.ConvertBDDateStringToDateTimeObject(SelectDate);
            var selectDate = Convert.ToDateTime(currentSelectDate).Date;

            var currentDate = DateTime.Now.Date;

            var documentTypeList = _unitOfWork.StuDocumentRepo.Find(s => (s.ClassId == student.ClassId) && (s.SectionId == student.SectionId || s.SessionId == student.SessionId || s.ShiftId == student.Shift) && s.ActivateDate.Value.Date <= currentDate && s.EffectiveTo.Value.Date >= currentDate).ToList();

            if (!string.IsNullOrEmpty(SelectDate))
            {
                documentTypeList = documentTypeList.AsEnumerable().Where(ss => ss.ActivateDate.Value.Date <= selectDate && ss.EffectiveTo.Value.Date >= selectDate).ToList();
            }

            var studentAllDocuments = documentTypeList.Select(s => new VmStudentDocument
            {
                ClassName = _unitOfWork.ClassRepo.FirstOrDefault(sa => sa.ClassId == s.ClassId) != null ? _unitOfWork.ClassRepo.FirstOrDefault(sa => sa.ClassId == s.ClassId).ClassName : "",
                SectionName = _unitOfWork.SectionRepo.FirstOrDefault(sa => sa.SectionId == s.SectionId) != null ?
                _unitOfWork.SectionRepo.FirstOrDefault(sa => sa.SectionId == s.SectionId).SectionName : "",
                SubjectName = _unitOfWork.SubjectRepo.FirstOrDefault(sa => sa.SubjectId == s.SubjectId) != null ?
                _unitOfWork.SubjectRepo.FirstOrDefault(sa => sa.SubjectId == s.SubjectId).SubjectName : "",
                DocumentType = _flexService.GetFlexValueByFlexValueId(s.TypeId ?? 0),
                PublishDate = s.ActivateDate != null ? s.ActivateDate.Value.ToString("dd/MM/yyyy") : "",
                EffectiveTo = s.EffectiveTo != null ? s.EffectiveTo.Value.ToString("dd/MM/yyyy") : "",
                Path = s.Path,
            }).ToList();

            return studentAllDocuments;
        }
        public string GetBillDate(int billId)
        {
            var billDate = _unitOfWork.StudentBillLineRepo.FirstOrDefault(s => s.BillHeaderId == billId).CreationDate;
            return billDate != null ? billDate.Value.ToString("dd/MM/yyyy") : "";
        }
        public string GetBillPeriod(int billId)
        {
            var billPeriodId = _unitOfWork.StudentBillLineRepo.FirstOrDefault(s => s.BillHeaderId == billId).PeriodId;
            return _unitOfWork.SubCategoryFeePeriodsRepo.FirstOrDefault(s => s.BillPeriodId == billPeriodId).PeriodName;
        }

        public List<VmRoomSeatPlan> GetExamSeatView(int ExamScheduleId, List<int> RoomIds)
        {
            var roomrepo = _unitOfWork.ClassRoomsRepo;
            var RoomSetupRepo = _unitOfWork.RoomSetupsRepo;
            var dateWiseSubjectRepo = _unitOfWork.DateWiseSubjectsRepo;
            var examScheduleLineRepo = _unitOfWork.ExamScheduleLinesRepo;
            var classRepo = _unitOfWork.ClassRepo.GetAll();
            var sectionRepo = _unitOfWork.SectionRepo.GetAll();
            var examSeatPlanRepo = _unitOfWork.ExamSeatPlansRepo.GetAll();
            var examschedulelineids = _unitOfWork.ExamScheduleLinesRepo.Find(ss => ss.ExamScheduleId == ExamScheduleId).Select(w => w.ExamScheduleLineId).ToList();
            var studentTransactionrepo = _unitOfWork.StudentTransactionRepo.GetAll().Select(w => new { w.TransactionId, w.ClassId, w.SectionId, w.RollNumber }).ToList();
            var exdata = examSeatPlanRepo.Where(sss => examschedulelineids.Contains(sss.ExamScheduleLineId)).Any();
            if (exdata)
            {
                var vmdata = new List<VmRoomSeatPlan>();
                foreach (var roomid in RoomIds)
                {
                    var roomname = roomrepo.FirstOrDefault(s => s.ClassRoomHeaderId == roomid) != null ? roomrepo.FirstOrDefault(s => s.ClassRoomHeaderId == roomid).ClassRoomName : "";
                    var RoomLayoutId = _unitOfWork.RoomSetupsRepo.Find(w => w.ClassRoomHeaderId == roomid && w.SetupFor == SetupFor.ClassRoom).Select(ss => ss.RoomSetupHeaderId).ToList();
                    var thisRoomLayouts = examSeatPlanRepo.Where(s => examschedulelineids.Contains(s.ExamScheduleLineId) && RoomLayoutId.Contains(s.RoomSetupHeaderId)).Select(d => d.RoomSetupHeaderId).Distinct().ToList();

                    var vmcol = new List<VmRoomColumn>();
                    var vmcolrow = new List<VmRoomColumnRow>();

                    foreach (var thisRoomLayoutId in thisRoomLayouts)
                    {
                        var roomsetup = RoomSetupRepo.FirstOrDefault(c => c.RoomSetupHeaderId == thisRoomLayoutId);
                        if (roomsetup.ClassRoomColumns.Count() > 0)
                        {
                            foreach (var item in roomsetup.ClassRoomColumns)
                            {
                                var d = new VmRoomColumn();
                                d.RoomSetupHeaderId = item.RoomSetupHeaderId;
                                d.ColumnId = item.ClassRoomColumnId;
                                d.ColumnSerial = item.ColumnSerial;
                                d.NoOfRow = item.NoOfRow;
                                d.RoomId = item.ClassRoomColumnId;
                                d.DefaultLayout = roomsetup.DefaultLayout ?? false;
                                vmcol.Add(d);

                                foreach (var colrow in item.ClassRoomColumnRows)
                                {
                                    if (colrow.Status == SeatStatus.Added)
                                    {
                                        var Datewiseids = examSeatPlanRepo.Where(s => s.ClassRoomColumnRowId == colrow.ClassRoomColumnRowId).ToList();
                                        var DateWiseId = Datewiseids.FirstOrDefault() != null ? Datewiseids.FirstOrDefault().DateWiseSubjectId : 0;
                                        var scheduleLineid = dateWiseSubjectRepo.FirstOrDefault(f => f.DateWiseSubjectId == DateWiseId) != null ? dateWiseSubjectRepo.FirstOrDefault(f => f.DateWiseSubjectId == DateWiseId).ExamScheduleLineId : 0;

                                        var DateWiseSubjectIds = "";
                                        foreach (var id in Datewiseids.Select(s => s.DateWiseSubjectId).ToList())
                                        {
                                            DateWiseSubjectIds += id + ",";
                                        }

                                        var dd = new VmRoomColumnRow();
                                        dd.ColumnId = colrow.ClassRoomColumnId;
                                        dd.ClassRoomColumnRowId = colrow.ClassRoomColumnRowId;
                                        dd.RowSerial = colrow.RowSerial;
                                        dd.NoOfSeat = colrow.NoofSeat;
                                        dd.SeatSerial = colrow.SeatSerial;
                                        dd.Status = colrow.Status;
                                        dd.ExamScheduleLineId = scheduleLineid;
                                        dd.DateWiseSubjectIds = DateWiseSubjectIds;
                                        dd.ExamScheduleId = ExamScheduleId;

                                        var ScheduleLine = examScheduleLineRepo.FirstOrDefault(s => s.ExamScheduleId == ExamScheduleId && s.ExamScheduleLineId == scheduleLineid);

                                        if (ScheduleLine != null)
                                        {
                                            dd.ClassId = ScheduleLine.ClassId;
                                            dd.ClassName = classRepo.FirstOrDefault(s => s.ClassId == dd.ClassId) != null ? classRepo.FirstOrDefault(s => s.ClassId == dd.ClassId).ClassName : "";
                                            dd.SectionId = ScheduleLine.SectionId;
                                            dd.SectionName = sectionRepo.FirstOrDefault(s => s.SectionId == dd.SectionId) != null ? sectionRepo.FirstOrDefault(s => s.SectionId == dd.SectionId).SectionName : "";
                                            var givenrollnumber = examSeatPlanRepo.Where(s => s.ClassRoomColumnRowId == dd.ClassRoomColumnRowId).Select(w => w.RollNumber).FirstOrDefault();
                                            var StudentTransactionRepo = studentTransactionrepo.OrderByDescending(w => w.TransactionId).FirstOrDefault(z => z.ClassId == dd.ClassId && z.SectionId == dd.SectionId && z.RollNumber == givenrollnumber);
                                            dd.RollNumber = StudentTransactionRepo != null ? StudentTransactionRepo.RollNumber ?? 0 : 0;
                                        }
                                        else
                                        {
                                            dd.ClassId = 0;
                                            dd.ClassName = "";
                                            dd.SectionId = 0;
                                            dd.SectionName = "";
                                            dd.RollNumber = 0;
                                        }
                                        vmcolrow.Add(dd);
                                    }
                                    else
                                    {
                                        var dd = new VmRoomColumnRow();
                                        dd.ColumnId = colrow.ClassRoomColumnId;
                                        dd.ClassRoomColumnRowId = colrow.ClassRoomColumnRowId;
                                        dd.RowSerial = colrow.RowSerial;
                                        dd.NoOfSeat = colrow.NoofSeat;
                                        dd.SeatSerial = colrow.SeatSerial;
                                        dd.Status = colrow.Status;
                                        vmcolrow.Add(dd);

                                    }
                                }
                            }
                        }
                    }

                    var data = new VmRoomSeatPlan();
                    data.RoomName = roomname;
                    data.RoomId = roomid;
                    data.vmRoomColumns = vmcol;
                    data.vmRoomColumnRows = vmcolrow;
                    vmdata.Add(data);
                }
                return vmdata;
            }
            return new List<VmRoomSeatPlan>();
        }
    }

    public class StudentCurriculumVm
    {
        public int StudentHeaderId { get; set; }

        public string StudentId { get; set; }

        public string Name { get; set; }

        public List<CurriculumVm> CurriculumVms { get; set; }
    }

    public class CurriculumVm
    {
        public int CurriculumId { get; set; }

        public int GradeId { get; set; }
    }

    public class StudentBillViewModel
    {
        public DateTime? BillDate { get; set; }
        public string PeriodName { get; set; }
        public string RcvType { get; set; }
        public decimal? RcvAmount { get; set; }
        public decimal? PayableAmount { get; set; }
        public string RcvState { get; set; }
        public DateTime? BillLastDate { get; set; }
    }

    public class MarkColore
    {
        public string Garde { get; set; }
        public string Color { get; set; }
    }

    public class StudentOtherInfo
    {
        public int StudentHeaderId { get; set; }

        public string StudentId { get; set; }

        public string Name { get; set; }

        public int WorkDay { get; set; }

        public int Absent { get; set; }

        public int Late { get; set; }

        public decimal Height { get; set; }

        public decimal Weight { get; set; }

        public string Remarks { get; set; }
    }

    public class StudentOtherInfoSave
    {
        public int ExamId { get; set; }
        public List<StudentOtherInfo> StudentOtherInfo { get; set; }
    }
}