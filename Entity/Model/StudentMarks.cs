using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entity.Model
{
    public partial class StudentMarks //: BaseEntity
    {
        [Key]
        public int MarksId { get; set; }

        public int SessionId { get; set; }

        //[ForeignKey("Student")]
        public int StudentHeaderId { get; set; }

        [ForeignKey("Class")]
        public int ClassId { get; set; }

        [ForeignKey("Section")]
        public int SectionId { get; set; }

        public int Shift { get; set; }

        public int TeacherId { get; set; }

        public int SubjectId { get; set; }

        public int? ExamId { get; set; }

        public int ExamTypeId { get; set; }

        public int? EventId { get; set; }

        public decimal OutOfMarks { get; set; }

        public decimal? ObtainedMarks { get; set; }

        public AttendanceType? AttendanceType { get; set; }

        //public virtual Student Student { get; set; }
        public virtual Class Class { get; set; }
        public virtual Section Section { get; set; }

        [ForeignKey("EventId")]
        public virtual PlannerEvent Plannerevent { get; set; }




        [StringLength(250)]
        public string Attribute1 { get; set; }

        [StringLength(250)]
        public string Attribute2 { get; set; }

        [StringLength(250)]
        public string Attribute3 { get; set; }

        [StringLength(250)]
        public string Attribute4 { get; set; }

        [StringLength(250)]
        public string Attribute5 { get; set; }

        public long? CreatedBy { get; set; }

        [DataType(DataType.Date, ErrorMessage = "Date format should be MM/DD/YYYY")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? CreationDate { get; set; }


        public long? LastUpdateBy { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? LastUpdateDate { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "Active From")]
        public DateTime? EffectiveFrom { get; set; }


        [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "Active To")]
        public DateTime? EffectiveTo { get; set; }
    }

    public enum AttendanceType
    {
        Absent = 1,
        Present = 2
    }
}
