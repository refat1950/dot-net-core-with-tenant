using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entity.Model
{
    public partial class FndMenuPage : BaseEntity
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int MenuId { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int PageId { get; set; }
    }
}
