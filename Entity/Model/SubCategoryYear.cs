﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class SubCategoryYear : BaseEntity
    {
        public int BillPeriodId { get; set; }
        public int YearId { get; set; }
        public virtual Year Year { get; set; }
        public virtual SubCategoryFeePeriod SubCategoryFeePeriod { get; set; }

    }
}
