using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entity.Model
{
    public partial class Calender : BaseEntity
    {
        [Key]
        [Column(TypeName = "date")]
        public DateTime CalendarDate { get; set; }

        public bool? IsHoliday { get; set; }

        public int? EventDtlsId { get; set; }


        //public virtual PlannerEventDetails PlannerEventDetails { get; set; }
    }
}
