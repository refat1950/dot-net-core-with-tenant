using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entity.Model
{
    public partial class ScoolAlert : BaseEntity
    {
        public ScoolAlert()
        {
            AlertHistory = new List<AlertHistory>();
        }

        [Key]
        public int AlertId { get; set; }

        [StringLength(200)]
        public string AlertName { get; set; }

        [StringLength(500)]
        public string AlertDescription { get; set; }

        public DateTime? AlertDate { get; set; }

        public int? ModuleId { get; set; }

        //[Column(TypeName = "ntext")]
        public string AlertSql { get; set; }

        [StringLength(1024)]
        public string AlertMessage { get; set; }

        [StringLength(5)]
        public string AlertFrequency { get; set; }

        [StringLength(50)]
        public string AlertStartTime { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public bool? Status { get; set; }
        public int? AlertHistoryId { get; set; }
        public virtual ICollection<AlertHistory> AlertHistory { get; set; }

    }
}
