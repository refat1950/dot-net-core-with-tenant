﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.View_Model
{
   public class VMSubject
    {
       public int SubjectId { get; set; }
       public string SubjectName { get; set; }
    }
}
