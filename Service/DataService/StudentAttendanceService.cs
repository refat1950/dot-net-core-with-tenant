﻿using Entity.Model;
using Entity.Model.Transaction;
using Repository.Context;
using Service.Helper;
using Service.View_Model;
using System;
using System.Collections.Generic;
using System.Linq;
using Repository.Utility;
using Services.View_Model;
using Service.Services;
using Services.CoreServices;

namespace Service.DataService
{
    public class StudentAttendanceService
    {
        private ProjectDbContext db;
        private FlexValueService flexValueService;
        public StudentAttendanceService(ProjectDbContext db, FlexValueService flexValueService)
        {
            this.db = db;
            this.flexValueService = flexValueService;
        }
        public List<VmStudentDateWiseAttendance> GetBetween(int start, int displayLength, string searchValue, string selectedDate, out int totalLength)
        {
            var result = new List<VmStudentDateWiseAttendance>();
            var date = Convert.ToDateTime(DateTimeHelperService.ConvertBDDateStringToDateTimeObject(selectedDate));

            var schoolInfo = db.School.FirstOrDefault();
            var stuLeaveList = db.StudentLeaveApplications.AsEnumerable();
            var isHoliday = db.EventAndHolidayPlanners.AsEnumerable().Where(w => w.EventTypeId == flexValueService.GetFlexValueIdByFlexShortValue("holiday") && ((w.EndDate == null && w.StartDate == date) || (w.EndDate != null && w.StartDate <= date && w.EndDate >= date))).Any();
            var studentList = db.Student.Select(i => new VmSelectStudent
            {
                PunchCardId = i.PunchCardNo,
                DeviceId = i.DeviceNumber,
                StudentHeaderId = i.StudentHeaderId,
                StudentId = i.StudentId,
                StuFirstName = i.FirstName,
                StuMiddleName = i.MiddleName,
                StuLastName = i.LastName,
                StudentTransactions = i.StudentTransaction
            }).AsQueryable();
            var attendanceList = db.Attendance.Select(i => new VmAttendanceSelectStudent
            {
                PunchDate = i.PunchDate,
                CardNumber = i.CardNumber,
                DoorNumber = i.DoorNumber,
                PunchTime = i.PunchTime,
                AttendanceId = i.AttendanceId
            }).Where(w => CoreServices.TruncateTime(w.PunchDate) == date).ToList();

            var dayFromDate = date.DayOfWeek.ToString();
            var allweakdata = from Weakdays e in Enum.GetValues(typeof(Weakdays))
                              select new VMSelectList { Id = (int)e, Name = e.ToString() };

            var weakday = allweakdata.Where(w => w.Name == dayFromDate).FirstOrDefault();
            var routineTransaction = db.ClassSectionRoutineTransacitons.Select(i => new { i.ClassRoutineTransacitonId, i.ClassId, i.SessionId, i.SectionId, i.DayInTheWeek, i.StartTime, i.EndTime, i.EffectiveFrom, i.EffectiveTo });
            var atndata = db.AttendanceChangeRequists.OrderByDescending(s => s.AttendanceChangeRequistId);

            foreach (var student in studentList)
            {
                var thisStudentAtndata = atndata.Where(s => s.StudentHeaderId == student.StudentHeaderId && s.PunchDate == date).ToList();
                var RequistedInTime = thisStudentAtndata.FirstOrDefault(s => s.RequistedInTime != null);
                var RequistedOutTime = thisStudentAtndata.FirstOrDefault(s => s.RequistedOutTime != null);

                var LateCountAfterMinutes = Convert.ToDouble(schoolInfo != null ? schoolInfo.LateCountAfterMinutesForStu != null ? schoolInfo.LateCountAfterMinutesForStu : 0 : 0);
                var EarlyOutCountAfterMinutes = Convert.ToDouble(schoolInfo != null ? schoolInfo.EarlyOutCountAfterMinutesForStu != null ? schoolInfo.EarlyOutCountAfterMinutesForStu : 0 : 0);

                var studentAttendance = attendanceList.Where(w => w.CardNumber == student.PunchCardId && w.DoorNumber == student.DeviceId);
                var data = new VmStudentDateWiseAttendance();
                data.StudentHeaderId = student.StudentHeaderId;
                data.StudentId = student.StudentId;
                data.StudentName = student.StuFirstName + " " + student.StuMiddleName + " " + student.StuLastName;

                var thisStudentCurrentSession = db.Session.FirstOrDefault(s => date >= CoreServices.TruncateTime(s.StartDate) && date <= CoreServices.TruncateTime(s.EndDate));
                if (thisStudentCurrentSession != null)
                {
                    var studendLastUpdatedLog = student.StudentTransactions.OrderByDescending(w => w.TransactionId).Where(w => w.StudentHeaderId == student.StudentHeaderId).FirstOrDefault();
                    var routineTrasaction = routineTransaction.Where(w => studendLastUpdatedLog != null && w.SessionId == studendLastUpdatedLog.SessionId && w.ClassId == studendLastUpdatedLog.ClassId && w.SectionId == studendLastUpdatedLog.SectionId && w.DayInTheWeek == weakday.Id && w.EffectiveFrom <= date && (w.EffectiveTo >= date || w.EffectiveTo == null)).ToList();

                    data.StartTime = routineTrasaction.Any() ? routineTrasaction.FirstOrDefault().StartTime : "Not Set";
                    data.EndTime = (routineTrasaction.Any() && routineTrasaction.OrderByDescending(w => w.ClassRoutineTransacitonId).FirstOrDefault() != null) ? routineTrasaction.OrderByDescending(w => w.ClassRoutineTransacitonId).FirstOrDefault().EndTime : "Not Set";

                    data.InTime = (thisStudentAtndata.FirstOrDefault(s => s.RequistedInTime != null) != null
                        && thisStudentAtndata.FirstOrDefault(s => s.RequistedInTime != null).Status == AttendanceApprovedStatus.Approved)
                        ? thisStudentAtndata.FirstOrDefault(s => s.ApprovedInTime != null).ApprovedInTime
                            : studentAttendance.Any() ? studentAttendance.FirstOrDefault().PunchTime : "";

                    data.OutTime = (thisStudentAtndata.FirstOrDefault(s => s.RequistedOutTime != null) != null
                        && thisStudentAtndata.FirstOrDefault(s => s.RequistedOutTime != null).Status == AttendanceApprovedStatus.Approved) ? thisStudentAtndata.FirstOrDefault(s => s.ApprovedOutTime != null).ApprovedOutTime
                            : studentAttendance.Any() ? studentAttendance.OrderByDescending(o => o.AttendanceId).FirstOrDefault().PunchTime : "";

                    if (!String.IsNullOrEmpty(data.InTime) && !String.IsNullOrEmpty(data.OutTime))
                        data.StudyHour = TimeSpan.FromMinutes(DateTime.Parse(data.OutTime).Subtract(DateTime.Parse(data.InTime)).TotalMinutes).ToString(@"hh\:mm");
                    else
                        data.StudyHour = "";
                    data.AttendanceType = GetAttendanceType(schoolInfo, isHoliday, stuLeaveList, date, student, studentAttendance, data.StartTime, data.EndTime);
                    data.TotalPaunch = studentAttendance.Select(s => s.PunchTime).Count();

                    data.RequestedInTimeStatus = RequistedInTime != null ? RequistedInTime.Status : 0;
                    data.RequestedOutTimeStatus = RequistedOutTime != null ? RequistedOutTime.Status : 0;

                    data.RequestedInTime = RequistedInTime != null ? RequistedInTime.RequistedInTime : "";
                    data.RequestedOutTime = RequistedOutTime != null ? RequistedOutTime.RequistedOutTime : "";

                    data.RequestedInTimeHeaderId = RequistedInTime != null ? RequistedInTime.AttendanceChangeRequistId : 0;
                    data.RequestedOutTimeHeaderId = RequistedOutTime != null ? RequistedOutTime.AttendanceChangeRequistId : 0;

                    data.ApprovedInTime = (RequistedInTime != null && RequistedInTime.Status == AttendanceApprovedStatus.Approved)
                        ? thisStudentAtndata.FirstOrDefault(s => s.ApprovedInTime != null).ApprovedInTime : "";
                    data.ApprovedOutTime = (RequistedOutTime != null && RequistedOutTime.Status == AttendanceApprovedStatus.Approved)
                        ? thisStudentAtndata.FirstOrDefault(s => s.ApprovedOutTime != null).ApprovedOutTime : "";

                    DateTime? starttimeworking = null;
                    DateTime? endtimeworking = null;

                    //if time any approved
                    if (!string.IsNullOrEmpty(data.ApprovedInTime) && !string.IsNullOrEmpty(data.StartTime) && data.StartTime != "Not Set")
                    {
                        DateTime inTime = DateTime.Parse(data.ApprovedInTime);
                        DateTime startTime = DateTime.Parse(data.StartTime);
                        starttimeworking = inTime;
                        if (inTime > startTime.AddMinutes(LateCountAfterMinutes)) // if comes after startTime
                            data.LateTime = TimeSpan.FromMinutes(inTime.Subtract(startTime).TotalMinutes).ToString(@"hh\:mm");
                    }

                    // if no time approved, calculate from previous time
                    else if (!string.IsNullOrEmpty(data.InTime) && !string.IsNullOrEmpty(data.StartTime) && data.StartTime != "Not Set")
                    {
                        DateTime inTime = DateTime.Parse(data.InTime);
                        DateTime startTime = DateTime.Parse(data.StartTime);
                        starttimeworking = inTime;
                        if (inTime > startTime.AddMinutes(LateCountAfterMinutes)) // if comes after startTime
                            data.LateTime = TimeSpan.FromMinutes(inTime.Subtract(startTime).TotalMinutes).ToString(@"hh\:mm");
                    }
                    else data.LateTime = "";

                    //if time any approved
                    if (!string.IsNullOrEmpty(data.ApprovedOutTime) && !string.IsNullOrEmpty(data.EndTime) && data.EndTime != "Not Set")
                    {
                        DateTime outTime = DateTime.Parse(data.ApprovedOutTime);
                        DateTime endTime = DateTime.Parse(data.EndTime);
                        endtimeworking = outTime;
                        if (outTime < endTime.AddMinutes(-EarlyOutCountAfterMinutes)) // if go before endTime
                            data.EarlyOut = TimeSpan.FromMinutes(endTime.Subtract(outTime).TotalMinutes).ToString(@"hh\:mm");
                        if (outTime > endTime) // if do overtime
                            data.OverTime = TimeSpan.FromMinutes(endTime.Subtract(outTime).TotalMinutes).ToString(@"hh\:mm");
                    }

                    // if no time approved, calculate from previous time
                    else if (!string.IsNullOrEmpty(data.OutTime) && !string.IsNullOrEmpty(data.EndTime) && data.EndTime != "Not Set")
                    {
                        DateTime outTime = DateTime.Parse(data.OutTime);
                        DateTime endTime = DateTime.Parse(data.EndTime);
                        endtimeworking = outTime;
                        if (outTime < endTime.AddMinutes(-EarlyOutCountAfterMinutes)) // if go before outtime
                            data.EarlyOut = TimeSpan.FromMinutes(endTime.Subtract(outTime).TotalMinutes).ToString(@"hh\:mm");
                        if (outTime > endTime) // if do overtime
                            data.OverTime = TimeSpan.FromMinutes(endTime.Subtract(outTime).TotalMinutes).ToString(@"hh\:mm");
                    }
                    else data.EarlyOut = "";

                    if (starttimeworking != null && endtimeworking != null)
                        data.StudyHourDistance = TimeSpan.FromMinutes(endtimeworking.Value.Subtract(starttimeworking ?? DateTime.Now).TotalMinutes).ToString(@"hh\:mm");
                    else { data.OverTime = ""; data.StudyHourDistance = ""; }
                    result.Add(data);
                }
            }
            if (!string.IsNullOrEmpty(searchValue))
            {
                result = result.Where(w => (!string.IsNullOrEmpty(w.StudentName) && w.StudentName.ToLower().Contains(searchValue.ToLower())) || (!string.IsNullOrEmpty(w.Designation) && w.Designation.ToLower().Contains(searchValue.ToLower())) || (!string.IsNullOrEmpty(w.InTime) && w.InTime.ToLower().Contains(searchValue.ToLower())) || (!string.IsNullOrEmpty(w.OutTime) && w.OutTime.ToLower().Contains(searchValue.ToLower())) || (!string.IsNullOrEmpty(w.AttendanceType) && w.AttendanceType.ToLower().Contains(searchValue.ToLower()))).ToList();
            }
            var displayedValues = displayLength == -1 ? result
                .OrderByDescending(o => o.StudentHeaderId).Skip(start).ToList() : result
                .OrderByDescending(o => o.StudentHeaderId).Skip(start)
                .Take(displayLength).ToList();
            totalLength = result.Count;
            return displayedValues;
        }

        public List<VmStudentDateWiseAttendance> GetDayAttendance(string selectedDate, List<int?>StudentHeaderIds)
        {
            var result = new List<VmStudentDateWiseAttendance>();
            var date = Convert.ToDateTime(DateTimeHelperService.ConvertBDDateStringToDateTimeObject(selectedDate));

            var schoolInfo = db.School.FirstOrDefault();
            var stuLeaveList = db.StudentLeaveApplications.AsEnumerable();
            var isHoliday = db.EventAndHolidayPlanners.AsEnumerable().Where(w => w.EventTypeId == flexValueService.GetFlexValueIdByFlexShortValue("holiday") && ((w.EndDate == null && w.StartDate == date) || (w.EndDate != null && w.StartDate <= date && w.EndDate >= date))).Any();
            var studentRepo = db.Student.AsQueryable();
            if (StudentHeaderIds != null)
            {
                studentRepo = studentRepo.Where(w => StudentHeaderIds.Contains(w.StudentHeaderId)).AsQueryable();
            }

            var studentList = studentRepo.Select(i => new VmSelectStudent
            {
                PunchCardId = i.PunchCardNo,
                DeviceId = i.DeviceNumber,
                StudentHeaderId = i.StudentHeaderId,
                StudentId = i.StudentId,
                StuFirstName = i.FirstName,
                StuMiddleName = i.MiddleName,
                StuLastName = i.LastName,
                StudentTransactions = i.StudentTransaction
            }).ToList();


            var attendanceList = db.Attendance.Select(i => new VmAttendanceSelectStudent
            {
                PunchDate = i.PunchDate,
                CardNumber = i.CardNumber,
                DoorNumber = i.DoorNumber,
                PunchTime = i.PunchTime,
                AttendanceId = i.AttendanceId
            }).Where(w => CoreServices.TruncateTime(w.PunchDate) == date).ToList();

            var dayFromDate = date.DayOfWeek.ToString();
            var allweakdata = from Weakdays e in Enum.GetValues(typeof(Weakdays))
                              select new VMSelectList { Id = (int)e, Name = e.ToString() };

            var weakday = allweakdata.Where(w => w.Name == dayFromDate).FirstOrDefault();
            var routineTransaction = db.ClassSectionRoutineTransacitons.Select(i => new { i.ClassRoutineTransacitonId, i.ClassId, i.SessionId, i.SectionId, i.DayInTheWeek, i.StartTime, i.EndTime, i.EffectiveFrom, i.EffectiveTo }).ToList();
            var atndata = db.AttendanceChangeRequists.OrderByDescending(s => s.AttendanceChangeRequistId).ToList();
            foreach (var student in studentList)
            {
                var studentAttendance = attendanceList.Where(w => w.CardNumber == student.PunchCardId && w.DoorNumber == student.DeviceId);
                var thisStudentAtndata = atndata.Where(s => s.StudentHeaderId == student.StudentHeaderId && s.PunchDate == date).ToList();
                var RequistedInTime = thisStudentAtndata.FirstOrDefault(s => s.RequistedInTime != null);
                var RequistedOutTime = thisStudentAtndata.FirstOrDefault(s => s.RequistedOutTime != null);

                var LateCountAfterMinutes = Convert.ToDouble(schoolInfo != null ? schoolInfo.LateCountAfterMinutesForStu != null ? schoolInfo.LateCountAfterMinutesForStu : 0 : 0);
                var EarlyOutCountAfterMinutes = Convert.ToDouble(schoolInfo != null ? schoolInfo.EarlyOutCountAfterMinutesForStu != null ? schoolInfo.EarlyOutCountAfterMinutesForStu : 0 : 0);

                var data = new VmStudentDateWiseAttendance();
                data.StudentHeaderId = student.StudentHeaderId;
                data.StudentId = student.StudentId;
                data.StudentName = student.StuFirstName + " " + student.StuMiddleName + " " + student.StuLastName;

                var thisStudentCurrentSession = db.Session.FirstOrDefault(s => date >= CoreServices.TruncateTime(s.StartDate) && date <= CoreServices.TruncateTime(s.EndDate));
                if (thisStudentCurrentSession != null)
                {
                    var studendLastUpdatedLog = student.StudentTransactions.OrderByDescending(w => w.TransactionId).Where(w => w.StudentHeaderId == student.StudentHeaderId).FirstOrDefault();
                    var routineTrasaction = routineTransaction.Where(w => studendLastUpdatedLog != null && w.SessionId == studendLastUpdatedLog.SessionId && w.ClassId == studendLastUpdatedLog.ClassId && w.SectionId == studendLastUpdatedLog.SectionId && w.DayInTheWeek == weakday.Id && w.EffectiveFrom <= date && (w.EffectiveTo >= date || w.EffectiveTo == null)).ToList();

                    data.StartTime = routineTrasaction.Any() ? routineTrasaction.FirstOrDefault().StartTime : "Not Set";
                    data.EndTime = (routineTrasaction.Any() && routineTrasaction.OrderByDescending(w => w.ClassRoutineTransacitonId).FirstOrDefault() != null) ? routineTrasaction.OrderByDescending(w => w.ClassRoutineTransacitonId).FirstOrDefault().EndTime : "Not Set";

                    data.InTime = (thisStudentAtndata.FirstOrDefault(s => s.RequistedInTime != null) != null 
                        && thisStudentAtndata.FirstOrDefault(s =>  s.RequistedInTime != null).Status == AttendanceApprovedStatus.Approved)
                        ? thisStudentAtndata.FirstOrDefault(s => s.ApprovedInTime != null).ApprovedInTime 
                            : studentAttendance.Any() ? studentAttendance.FirstOrDefault().PunchTime : "" ;

                    data.OutTime = (thisStudentAtndata.FirstOrDefault(s => s.RequistedOutTime != null) != null 
                        && thisStudentAtndata.FirstOrDefault(s =>  s.RequistedOutTime != null).Status == AttendanceApprovedStatus.Approved) ? thisStudentAtndata.FirstOrDefault(s => s.ApprovedOutTime != null).ApprovedOutTime 
                            : studentAttendance.Any() ? studentAttendance.OrderByDescending(o => o.AttendanceId).FirstOrDefault().PunchTime : "";

                    if (!String.IsNullOrEmpty(data.InTime) && !String.IsNullOrEmpty(data.OutTime))
                        data.StudyHour = TimeSpan.FromMinutes(DateTime.Parse(data.OutTime).Subtract(DateTime.Parse(data.InTime)).TotalMinutes).ToString(@"hh\:mm");
                    else
                        data.StudyHour = "";
                    data.AttendanceType = GetAttendanceType(schoolInfo, isHoliday, stuLeaveList, date, student, studentAttendance, data.StartTime, data.EndTime);
                    data.TotalPaunch = studentAttendance.Select(s => s.PunchTime).Count();

                    data.RequestedInTimeStatus = RequistedInTime != null ? RequistedInTime.Status : 0;
                    data.RequestedOutTimeStatus = RequistedOutTime != null ? RequistedOutTime.Status : 0;

                    data.RequestedInTime = RequistedInTime != null ? RequistedInTime.RequistedInTime : "";
                    data.RequestedOutTime = RequistedOutTime != null ? RequistedOutTime.RequistedOutTime : "";

                    data.RequestedInTimeHeaderId = RequistedInTime != null ? RequistedInTime.AttendanceChangeRequistId : 0;
                    data.RequestedOutTimeHeaderId = RequistedOutTime != null ? RequistedOutTime.AttendanceChangeRequistId : 0;

                    data.ApprovedInTime = (RequistedInTime != null && RequistedInTime.Status == AttendanceApprovedStatus.Approved)
                        ? thisStudentAtndata.FirstOrDefault(s => s.ApprovedInTime != null).ApprovedInTime : "";
                    data.ApprovedOutTime = (RequistedOutTime != null && RequistedOutTime.Status == AttendanceApprovedStatus.Approved)
                        ? thisStudentAtndata.FirstOrDefault(s => s.ApprovedOutTime != null).ApprovedOutTime : "";

                    DateTime? starttimeworking = null;
                    DateTime? endtimeworking = null;

                    //if time any approved
                    if (!string.IsNullOrEmpty(data.ApprovedInTime) && !string.IsNullOrEmpty(data.StartTime) && data.StartTime != "Not Set")
                    {
                        DateTime inTime = DateTime.Parse(data.ApprovedInTime);
                        DateTime startTime = DateTime.Parse(data.StartTime);
                        starttimeworking = inTime;
                        if (inTime > startTime.AddMinutes(LateCountAfterMinutes)) // if comes after startTime
                            data.LateTime = TimeSpan.FromMinutes(inTime.Subtract(startTime).TotalMinutes).ToString(@"hh\:mm");
                    }

                    // if no time approved, calculate from previous time
                    else if (!string.IsNullOrEmpty(data.InTime) && !string.IsNullOrEmpty(data.StartTime) && data.StartTime != "Not Set")
                    {
                        DateTime inTime = DateTime.Parse(data.InTime);
                        DateTime startTime = DateTime.Parse(data.StartTime);
                        starttimeworking = inTime;
                        if (inTime > startTime.AddMinutes(LateCountAfterMinutes)) // if comes after startTime
                            data.LateTime = TimeSpan.FromMinutes(inTime.Subtract(startTime).TotalMinutes).ToString(@"hh\:mm");
                    }
                    else data.LateTime = "";

                    //if time any approved
                    if (!string.IsNullOrEmpty(data.ApprovedOutTime) && !string.IsNullOrEmpty(data.EndTime) && data.EndTime != "Not Set")
                    {
                        DateTime outTime = DateTime.Parse(data.ApprovedOutTime);
                        DateTime endTime = DateTime.Parse(data.EndTime);
                        endtimeworking = outTime;
                        if (outTime < endTime.AddMinutes(-EarlyOutCountAfterMinutes)) // if go before endTime
                            data.EarlyOut = TimeSpan.FromMinutes(endTime.Subtract(outTime).TotalMinutes).ToString(@"hh\:mm");
                        if (outTime > endTime) // if do overtime
                            data.OverTime = TimeSpan.FromMinutes(endTime.Subtract(outTime).TotalMinutes).ToString(@"hh\:mm");
                    }

                    // if no time approved, calculate from previous time
                    else if (!string.IsNullOrEmpty(data.OutTime) && !string.IsNullOrEmpty(data.EndTime) && data.EndTime != "Not Set")
                    {
                        DateTime outTime = DateTime.Parse(data.OutTime);
                        DateTime endTime = DateTime.Parse(data.EndTime);
                        endtimeworking = outTime;
                        if (outTime < endTime.AddMinutes(-EarlyOutCountAfterMinutes)) // if go before outtime
                            data.EarlyOut = TimeSpan.FromMinutes(endTime.Subtract(outTime).TotalMinutes).ToString(@"hh\:mm");
                        if (outTime > endTime) // if do overtime
                            data.OverTime = TimeSpan.FromMinutes(endTime.Subtract(outTime).TotalMinutes).ToString(@"hh\:mm");
                    }
                    else data.EarlyOut = "";

                    if (starttimeworking != null && endtimeworking != null)
                        data.StudyHourDistance = TimeSpan.FromMinutes(endtimeworking.Value.Subtract(starttimeworking ?? DateTime.Now).TotalMinutes).ToString(@"hh\:mm");
                    else { data.OverTime = ""; data.StudyHourDistance = ""; }
                    result.Add(data);
                }
            }
            return result;
        }

        public VmStudentDateWiseAttendance GetDayAttendanceData(DateTime date, int StudentHeaderId)
        {
            var schoolInfo = db.School.FirstOrDefault();
            var stuLeaveList = db.StudentLeaveApplications.AsEnumerable();
            var isHoliday = db.EventAndHolidayPlanners.AsEnumerable().Where(w => w.EventTypeId == flexValueService.GetFlexValueIdByFlexShortValue("holiday") && ((w.EndDate == null && w.StartDate == date) || (w.EndDate != null && w.StartDate <= date && w.EndDate >= date))).Any();
            var studentdata = db.Student.Where(s => s.StudentHeaderId == StudentHeaderId).Select(i => new VmSelectStudent
            {
                PunchCardId = i.PunchCardNo,
                DeviceId = i.DeviceNumber,
                StudentHeaderId = i.StudentHeaderId,
                StudentId = i.StudentId,
                StuFirstName = i.FirstName,
                StuMiddleName = i.MiddleName,
                StuLastName = i.LastName,
                StudentTransactions = i.StudentTransaction
            }).FirstOrDefault();

            var attendanceList = db.Attendance.Select(i => new VmAttendanceSelectStudent
            {
                PunchDate = i.PunchDate,
                CardNumber = i.CardNumber,
                DoorNumber = i.DoorNumber,
                PunchTime = i.PunchTime,
                AttendanceId = i.AttendanceId
            }).Where(w => CoreServices.TruncateTime(w.PunchDate) == date && w.CardNumber == studentdata.PunchCardId && w.DoorNumber == studentdata.DeviceId).ToList();

            var dayFromDate = date.DayOfWeek.ToString();
            var allweakdata = from Weakdays e in Enum.GetValues(typeof(Weakdays))
                              select new VMSelectList { Id = (int)e, Name = e.ToString() };
            var weakday = allweakdata.Where(w => w.Name == dayFromDate).FirstOrDefault();
            var routineTransaction = db.ClassSectionRoutineTransacitons.Select(i => new { i.ClassRoutineTransacitonId, i.ClassId, i.SessionId, i.SectionId, i.DayInTheWeek, i.StartTime, i.EndTime, i.EffectiveFrom, i.EffectiveTo }).ToList();

            var studentAttendance = attendanceList.Where(w => w.CardNumber == studentdata.PunchCardId && w.DoorNumber == studentdata.DeviceId);
            var atnData = db.AttendanceChangeRequists.OrderByDescending(s => s.AttendanceChangeRequistId);

            var data = new VmStudentDateWiseAttendance();
            data.StudentHeaderId = studentdata.StudentHeaderId;
            data.StudentId = studentdata.StudentId;
            data.StudentName = studentdata.StuFirstName + " " + studentdata.StuMiddleName + " " + studentdata.StuLastName;
            var thisStudentAtndata = atnData.Where(s => s.StudentHeaderId == studentdata.StudentHeaderId && s.PunchDate == date).ToList();
            var RequistedInTime = thisStudentAtndata.FirstOrDefault(s => s.RequistedInTime != null);
            var RequistedOutTime = thisStudentAtndata.FirstOrDefault(s => s.RequistedOutTime != null);

            var LateCountAfterMinutes = Convert.ToDouble(schoolInfo != null ? schoolInfo.LateCountAfterMinutesForStu != null ? schoolInfo.LateCountAfterMinutesForStu : 0 : 0);
            var EarlyOutCountAfterMinutes = Convert.ToDouble(schoolInfo != null ? schoolInfo.EarlyOutCountAfterMinutesForStu != null ? schoolInfo.EarlyOutCountAfterMinutesForStu : 0 : 0);

            var studendLastUpdatedLog = studentdata.StudentTransactions.OrderByDescending(w => w.TransactionId).Where(w => w.StudentHeaderId == studentdata.StudentHeaderId).FirstOrDefault();

            var routineTrasaction = routineTransaction.Where(w => studendLastUpdatedLog != null && w.SessionId == studendLastUpdatedLog.SessionId && w.ClassId == studendLastUpdatedLog.ClassId && w.SectionId == studendLastUpdatedLog.SectionId && w.DayInTheWeek == weakday.Id && w.EffectiveFrom <= date && (w.EffectiveTo >= date || w.EffectiveTo == null)).ToList();

            data.StartTime = routineTrasaction.Any() ? routineTrasaction.FirstOrDefault().StartTime : "Not Set";
            data.EndTime = (routineTrasaction.Any() && routineTrasaction.OrderByDescending(w => w.ClassRoutineTransacitonId).FirstOrDefault() != null) ? routineTrasaction.OrderByDescending(w => w.ClassRoutineTransacitonId).FirstOrDefault().EndTime : "Not Set";

            data.InTime = (RequistedInTime != null && RequistedInTime.Status == AttendanceApprovedStatus.Approved)
                ? thisStudentAtndata.FirstOrDefault(s => s.ApprovedInTime != null).ApprovedInTime
                    : studentAttendance.Any() ? studentAttendance.FirstOrDefault().PunchTime : "";

            data.OutTime = (RequistedOutTime != null && RequistedOutTime.Status == AttendanceApprovedStatus.Approved) 
                ? thisStudentAtndata.FirstOrDefault(s => s.ApprovedOutTime != null).ApprovedOutTime
                    : studentAttendance.Any() ? studentAttendance.OrderByDescending(o => o.AttendanceId).FirstOrDefault().PunchTime : "";

            if (!String.IsNullOrEmpty(data.InTime) && !String.IsNullOrEmpty(data.OutTime))
                data.StudyHour = TimeSpan.FromMinutes(DateTime.Parse(data.OutTime).Subtract(DateTime.Parse(data.InTime)).TotalMinutes).ToString(@"hh\:mm");
            else
                data.StudyHour = "";
            data.AttendanceType = GetAttendanceType(schoolInfo, isHoliday, stuLeaveList, date, studentdata, studentAttendance, data.StartTime, data.EndTime);
            data.TotalPaunch = studentAttendance.Select(s => s.PunchTime).Count();

            data.RequestedInTimeStatus = RequistedInTime != null ? RequistedInTime.Status : 0;
            data.RequestedOutTimeStatus = RequistedOutTime != null ? RequistedOutTime.Status : 0;

            data.RequestedInTime = RequistedInTime != null ? RequistedInTime.RequistedInTime : "";
            data.RequestedOutTime = RequistedOutTime != null ? RequistedOutTime.RequistedOutTime : "";

            data.RequestedInTimeHeaderId = RequistedInTime != null ? RequistedInTime.AttendanceChangeRequistId : 0;
            data.RequestedOutTimeHeaderId = RequistedOutTime != null ? RequistedOutTime.AttendanceChangeRequistId : 0;

            data.ApprovedInTime = (RequistedInTime != null && RequistedInTime.Status == AttendanceApprovedStatus.Approved)
                ? thisStudentAtndata.FirstOrDefault(s => s.ApprovedInTime != null).ApprovedInTime : "";
            data.ApprovedOutTime = (RequistedOutTime != null && RequistedOutTime.Status == AttendanceApprovedStatus.Approved)
                ? thisStudentAtndata.FirstOrDefault(s => s.ApprovedOutTime != null).ApprovedOutTime : "";

            DateTime? starttimeworking = null;
            DateTime? endtimeworking = null;

            //if time any approved
            if (!string.IsNullOrEmpty(data.ApprovedInTime) && !string.IsNullOrEmpty(data.StartTime) && data.StartTime != "Not Set")
            {
                DateTime inTime = DateTime.Parse(data.ApprovedInTime);
                DateTime startTime = DateTime.Parse(data.StartTime);
                starttimeworking = inTime;
                if (inTime > startTime.AddMinutes(LateCountAfterMinutes)) // if comes after startTime
                    data.LateTime = TimeSpan.FromMinutes(inTime.Subtract(startTime).TotalMinutes).ToString(@"hh\:mm");
            }

            // if no time approved, calculate from previous time
            else if (!string.IsNullOrEmpty(data.InTime) && !string.IsNullOrEmpty(data.StartTime) && data.StartTime != "Not Set")
            {
                DateTime inTime = DateTime.Parse(data.InTime);
                DateTime startTime = DateTime.Parse(data.StartTime);
                starttimeworking = inTime;
                if (inTime > startTime.AddMinutes(LateCountAfterMinutes)) // if comes after startTime
                    data.LateTime = TimeSpan.FromMinutes(inTime.Subtract(startTime).TotalMinutes).ToString(@"hh\:mm");
            }
            else data.LateTime = "";

            //if time any approved
            if (!string.IsNullOrEmpty(data.ApprovedOutTime) && !string.IsNullOrEmpty(data.EndTime) && data.EndTime != "Not Set")
            {
                DateTime outTime = DateTime.Parse(data.ApprovedOutTime);
                DateTime endTime = DateTime.Parse(data.EndTime);
                endtimeworking = outTime;
                if (outTime < endTime.AddMinutes(-EarlyOutCountAfterMinutes)) // if go before endTime
                    data.EarlyOut = TimeSpan.FromMinutes(endTime.Subtract(outTime).TotalMinutes).ToString(@"hh\:mm");
                if (outTime > endTime) // if do overtime
                    data.OverTime = TimeSpan.FromMinutes(endTime.Subtract(outTime).TotalMinutes).ToString(@"hh\:mm");
            }

            // if no time approved, calculate from previous time
            else if (!string.IsNullOrEmpty(data.OutTime) && !string.IsNullOrEmpty(data.EndTime) && data.EndTime != "Not Set")
            {
                DateTime outTime = DateTime.Parse(data.OutTime);
                DateTime endTime = DateTime.Parse(data.EndTime);
                endtimeworking = outTime;
                if (outTime < endTime.AddMinutes(-EarlyOutCountAfterMinutes)) // if go before outtime
                    data.EarlyOut = TimeSpan.FromMinutes(endTime.Subtract(outTime).TotalMinutes).ToString(@"hh\:mm");
                if (outTime > endTime) // if do overtime
                    data.OverTime = TimeSpan.FromMinutes(endTime.Subtract(outTime).TotalMinutes).ToString(@"hh\:mm");
            }
            else data.EarlyOut = "";

            if (starttimeworking != null && endtimeworking != null)
                data.StudyHourDistance = TimeSpan.FromMinutes(endtimeworking.Value.Subtract(starttimeworking ?? DateTime.Now).TotalMinutes).ToString(@"hh\:mm");
            else { data.OverTime = ""; data.StudyHourDistance = ""; }
            return data;
        }

        public string GetAttendanceType(School schoolInfo,bool isHoliday,IEnumerable<StudentLeaveApplication> stuLeaveList, DateTime date, VmSelectStudent student, IEnumerable<VmAttendanceSelectStudent> studentAttendance, string StartTime
, string EndTime)
        {
            TimeSpan difForLateCheck = new TimeSpan();
            TimeSpan difForEarlyOutCheck = new TimeSpan();
            var LateCountAfterMinutes = schoolInfo != null ? schoolInfo.LateCountAfterMinutesForStu != null ?
                schoolInfo.LateCountAfterMinutesForStu : 0: 0;

            var EarlyOutCountAfterMinutes = schoolInfo != null ? schoolInfo.EarlyOutCountAfterMinutesForStu != null ? schoolInfo.EarlyOutCountAfterMinutesForStu : 0 : 0;
            
            var firstPunch = studentAttendance.FirstOrDefault();
            var lastPunch = studentAttendance.LastOrDefault();

            if (firstPunch != null && StartTime != "Not Set")
            {
                difForLateCheck = DateTime.Parse(firstPunch.PunchTime).Subtract(DateTime.Parse(StartTime));
            }
            if (lastPunch != null && EndTime != "Not Set")
            {
                difForEarlyOutCheck = DateTime.Parse(EndTime).Subtract(DateTime.Parse(lastPunch.PunchTime));
            }
            var isWeekend = date.DayOfWeek == DayOfWeek.Friday ? true : false;

            string type = "";
            if (student == null || student.DeviceId == null || student.DeviceId == 0 || student.PunchCardId == null && StartTime == "Not Set" || EndTime == "Not Set")
                type = "Not Found";
            else if (isWeekend)
                type = "Weekend";
            else if (isHoliday)
                type = "Holiday";
            else if (stuLeaveList.FirstOrDefault(f => f.StudentHeaderId == student.StudentHeaderId && f.LeaveStartDate <= date && f.LeaveEndDate >= date && f.LeaveStatus == LeaveStatus.Approved) != null)
                type = "Leave";
            else if (firstPunch == null) // if not a single punch
                type = "Absent";
            else if (difForLateCheck.TotalMinutes > LateCountAfterMinutes)
                type = "Late";
            else if (difForEarlyOutCheck.TotalMinutes > EarlyOutCountAfterMinutes)
                type = "Early Out";
            else
                type = "Present";
            return type;
        }
    }
    public class VmSelectStudent
    {

        public string PunchCardId { get; set; }
        public int? DeviceId { get; set; }
        public int StudentHeaderId { get; set; }
        public string StudentId { get; set; }
        public int? DesignationId { get; set; }
        public string StuFirstName { get; set; }
        public string StuMiddleName { get; set; }
        public string StuLastName { get; set; }
        public virtual ICollection<StudentTransaction> StudentTransactions { get; set; }
    }
    public class VmAttendanceSelectStudent
    {
        public int AttendanceId { get; set; }

        public DateTime PunchDate { get; set; }
        public string PunchTime { get; set; }

        //Device Id
        public int? DoorNumber { get; set; }

        public int? GateNumber { get; set; }
        public string CardNumber { get; set; }

        public int? PermissionTypeId { get; set; }

        public bool? AttendanceStatus { get; set; }
        public int? DevicePort { get; set; }
        public string DeviceIP { get; set; }
    }
}
