using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Entity.Model
{
    public partial class PlannerEventType : BaseEntity
    {
        public PlannerEventType()
        {
            PlannerEvent = new HashSet<PlannerEvent>();
        }

        [Key]
        public int EventTypeId { get; set; }

        [Required]
        [StringLength(50)]
        public string EventTypeName { get; set; }

        [StringLength(30)]
        public string ShortName { get; set; }

        public virtual ICollection<PlannerEvent> PlannerEvent { get; set; }
    }
}
