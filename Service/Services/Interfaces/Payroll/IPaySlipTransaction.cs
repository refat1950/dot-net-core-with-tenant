﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Services.Interfaces
{
    public interface IPaySlipTransaction
    {
        bool GeneratePayslip(int empHeaderId, int periodId, int? userid, int poisitionid, int payrollid);
        bool ReGeneratePayslip(int empHeaderId, int periodId, int? userid, int poisitionid, int payrollid);
        bool CancelPayslip(int empHeaderId, int periodId, int? userid, int poisitionid, int payrollid);
        bool UpdatePayslip(int empHeaderId, int periodId, int? userid, int poisitionid, int payrollid);
    }
}
