﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Entity.Model
{
    public class HrShift : BaseEntity
    {
        [Key]
        public int ShiftId { get; set; }
        public string ShiftName { get; set; }
        public string ShiftShortName { get; set; }
        public string ShiftColor { get; set; }
        public string DayOfWeek { get; set; }
        public string DayOfMonth { get; set; }
        public string WeekOfMonth { get; set; }
        public string ShiftStartTime { get; set; }
        public string ShiftEndTime { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool Status { get; set; }
        public List<HrEmployeeShift> HR_EMPLOYEE_SHIFT { get; set; }
    }
}
