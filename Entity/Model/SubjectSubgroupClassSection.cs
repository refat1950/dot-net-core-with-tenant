﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class SubjectSubgroupClassSection
    {
        [Key]
        public int TableId { get; set; }
        public int SubgroupId { get; set; }
        public int ClassId { get; set; }
        public int? SectionId { get; set; }
        public int TeacherId { get; set; }
        public int SessionId { get; set; }
        public int MasterEventId { get; set; }
        public int EventId { get; set; }
        public int? NoOfExam { get; set; }
        public int? BestCount { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Attribute1 { get; set; }
        public string Attribute2 { get; set; }
        public string Attribute3 { get; set; }
        public string Attribute4 { get; set; }
        public string Attribute5 { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreationDate { get; set; }
        public int? LastUpdateBy { get; set; }
        public DateTime? LastUpdateDate { get; set; }
        //[DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Date, ErrorMessage = "Date format should be MM/DD/YYYY")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "Active From")]
        public DateTime? EffectiveFrom { get; set; }


        // [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}", ApplyFormatInEditMode = true)]
        //[Display(Name = "Active To")]

        [DataType(DataType.Date, ErrorMessage = "Date format should be MM/DD/YYYY")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "Active To")]
        public DateTime? EffectiveTo { get; set; }
        public virtual Class Class { get; set; }
        public virtual Section Section { get; set; }
        [ForeignKey("SubgroupId")]
        public virtual SubjectSubgroup SubjectSubgroup { get; set; }
        [ForeignKey("EventId")]
        public virtual PlannerEvent PlannerEvent { get; set; }

    }
}
