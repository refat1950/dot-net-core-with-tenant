﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model.Alert
{
    public class MessageAlertMaster : BaseEntity
    {
        [Key]
        public int AlertId { get; set; }
        public string AlertName { get; set; }
        public string GroupIds { get; set; }
        public string Mesage { get; set; }
        public string MessageId { get; set; }
        public string ResponseMessage { get; set; }
        public bool IsError { get; set; }
        public int? ErrorCode { get; set; }
        public DateTime SendingDateTime { get; set; }
    }
}
