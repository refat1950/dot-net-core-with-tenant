using System;
using System.ComponentModel.DataAnnotations;

namespace Entity.Model
{
    public partial class MedicalHistory : BaseEntity
    {
        [Key]
        public int MedicalHistroyId { get; set; }

        [StringLength(256)]
        public string HistoryName { get; set; }

        [StringLength(20)]
        public string UserType { get; set; }

        public int? ReferenceId { get; set; }


    }
}
