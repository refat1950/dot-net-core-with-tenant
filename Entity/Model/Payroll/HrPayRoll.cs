﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class HrPayRoll : BaseEntity
    {
        [Key]
        public int PayRollId { get; set; }
        public string PayRollName { get; set; }
        public int? PaymentTypeId { get; set; }
        public DateTime? IssueDate { get; set; }
        ICollection<HrPayRollPayHead> PayHeadList { get; set; }
    }
}
