﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.View_Model
{
    public class VmFeeSubCategoryAdd
    {
        public int FeeSubCategoryId { get; set; }
        public string FeeSubCategoryName { get; set; }
        public int FeeCategoryId { get; set; }
        public int SessionId { get; set; }
        public decimal Amount { get; set; }
        public int FeeType { get; set; }
        public List<VmSubCategoryFeeTypeList> FeeTypeList { get; set; }
    }

    public class VmSubCategoryFeeTypeList
    {
        public int? SubCategoryFeeTypeId { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string DueDate { get; set; }
        public int? Year { get; set; }
        public int? Month { get; set; }
        public int? Day { get; set; }

        public string YearName { get; set; }
        public string MonthName { get; set; }
        public string Dayname { get; set; }
    }
}
