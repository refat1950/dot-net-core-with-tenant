﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.ViewModel
{
    public class VmPeriodYear
    {
        public int Year { get; set; }
        public bool IsVisible { get; set; }
    }
}
