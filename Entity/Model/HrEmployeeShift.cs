﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entity.Model
{
    public class HrEmployeeShift : BaseEntity
    {
        public int EmpHeaderId { get; set; }
        public int ShiftId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? ShiftType { get; set; }
        public bool Status { get; set; }

        public HrEmployee HrEmployee { get; set; }
        public HrShift HrShift { get; set; }
    }
}