﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class HrEmployeePayHead : BaseEntity
    {
        [Key]
        public int EmployeePayHeadId { get; set; }
        public int EmpHeaderId { get; set; }
        public int PayHeadId { get; set; }
        public decimal Unit { get; set; }
        public bool IsPercentage { get; set; }
    }
}
