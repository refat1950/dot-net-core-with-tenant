using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entity.Model
{
    public partial class SchoolTask : BaseEntity
    {
        [Key]
        public int TaskId { get; set; }

        public int? RefId { get; set; }

        public int? RefTypeId { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public int? GenerateTypeId { get; set; }

        public bool? IsActive { get; set; }

        [StringLength(256)]
        public string TaskTitle { get; set; }

        //[Column(TypeName = "ntext")]
        public string TaskDescription { get; set; }

        public int? TaskStatusId { get; set; }
    }
}
