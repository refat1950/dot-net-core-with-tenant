﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class ExamScheduleLine : BaseEntity
    {
        [Key]
        public int ExamScheduleLineId { get; set; }
        public int ClassId { get; set; }
        public int SectionId { get; set; }
        public DateTime ExamStartdate { get; set; }
        public DateTime ExamEnddate { get; set; }
        public int ExamScheduleId { get; set; }
        public virtual ExamSchedule ExamSchedule { get; set; }
        public virtual List<DateWiseSubject> DateWiseSubjects { get; set; }
        public virtual List<ExamSeatPlan> ExamSeatPlans { get; set; }
    }
}
