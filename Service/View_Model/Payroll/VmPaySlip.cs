﻿using Entity.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.ViewModel.Payroll
{
    public class VmEmpStatusLog
    {
        public int EmpHeaderId { get; set; }
        public EmployeeJoiningStatus EmployeeJoiningStatus { get; set; }
    }
    public class VmEmpResignData
    {
        public int ResignationId { get; set; }
        public int EmployeeHeaderId { get; set; }
        public DateTime ResignDate { get; set; }
    }
    public class VmEmpRejoinData
    {
        public int RejoinId { get; set; }
        public int EmpHeaderId { get; set; }
        public DateTime RejoinDate { get; set; }
    }
    public class VmEmployeeData
    {
        public int EmpHeaderId { get; set; }
        public EmployeeJoiningStatus EmployeeJoiningStatus { get; set; }
        public string EmpName { get; set; }
        public int? PositionId { get; set; }
        public DateTime JoiningDate { get; set; }
    }
    public class VmHrPositionData
    {
        public int PostionId { get; set; }
        public string PostionName { get; set; }
        public int PayrollId { get; set; }
    }

    public class VmBillPeriodsData
    {
        public int BillPeriodId { get; set; }
        public string MonthName { get; set; }
        public BillPeriodType? PeriodTypeId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool IsVisible { get; set; }
    }

    public class VmEmpoyeePeriod
    {
        public int EmpId { get; set; }
        public string EmpName { get; set; }
        public int PeriodId { get; set; }
        public string PeriodName { get; set; }
        public int PositionId { get; set; }
        public string PositionName { get; set; }
        public int PayrollId { get; set; }
        public string PayrollName { get; set; }
        public int PayslipPeriodSettings { get; set; }
        public List<VmPeriodList> PeriodList { get; set; }
    }
    public class VmPaySlip
    {
        public int PeriodId { get; set; }
        public string PeriodName { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public int EmpHeaderId { get; set; }
        public string EmpId { get; set; }
        public string EmpName { get; set; }
        public string EmpMobileNumber { get; set; }
        public string Email { get; set; }
        public int PositionId { get; set; }
        public string PositionName { get; set; }
        public string JoinningDate { get; set; }
        public string PaySlipId { get; set; }
        public bool IsPaid { get; set; }
        public int PayrollId { get; set; }
        public string PayrollName { get; set; }
        public PaidStatus PaidStatus { get; set; }
        public decimal TotalAmountPayable { get; set; }
        public decimal TotalAdditionAmount { get; set; }
        public decimal TotalDeductionAmount { get; set; }
        public List<VmPayHeadDataList> vmPayHeadDataList { get; set; }
    }

    public class VmPayHeadDataList
    {
        public int Masterlogid { get; set; }
        public int PayHeadId { get; set; }
        public string PayHeadName { get; set; }
        public string PayHeadType { get; set; }
        public bool IsPayHeadTypeEditable { get; set; }
        public decimal Unit { get; set; }
        public decimal CalculatedValue { get; set; }
        public bool IsPercentage { get; set; }
        public bool IsAmountEditable { get; set; }
        public bool IsTimingFees { get; set; }
        public string UnitType { get; set; }
        public decimal Amount { get; set; }
        public string AdditionType { get; set; }
        public decimal Additionamount { get; set; }
        public string DeductionType { get; set; }
        public decimal Deductionamount { get; set; }
    }
    public class VmPaySlipExtend
    {
        public int EmployeeId { get; set; }
        public int PeriodId { get; set; }
        public int PositionId { get; set; }
        public int PayrollId { get; set; }
    }

    public class VmSalaryStatement
    {
        public int EmpHeaderId { get; set; }
        public int PeriodIdStart { get; set; }
        public int PeriodIdEnd { get; set; }
    }
    public class VmPayrollPositionWisePeriodList
    {
        public int Serial { get; set; }
        public int PeriodId { get; set; }
        public int PositionId { get; set; }
        public int PayrollId { get; set; }
    }
    public class VmPeriodList
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public PaidStatus PaidStatus { get; set; }
        public string PaidDate { get; set; }
        public PayslipCurrentStatus GenerateStatus { get; set; }
        public string GenerateStatusName { get; set; }
        public decimal TotalPaidAmount { get; set; }
        public decimal TotalAmountPayable { get; set; }
        public string GenerationDate { get; set; }
        public int PositionId { get; set; }
        public int PayrollId { get; set; }
        public bool IsUpdateable { get; set; }
        public string LastUpdateDate { get; set; }
        public int Serial { get; set; }
        public int SerialReverse { get; set; }
        public List<VmParciallyPaidList> vmParciallyPaidLists { get; set; }
    }
    public class VmParciallyPaidList
    {
        public decimal Amount { get; set; }
        public string PaidDate { get; set; }
    }

    public class VmSalaryTotal
    {
        public decimal Netsalary { get; set; }
        public decimal Grosstotal { get; set; }
        public decimal Deductiontotal { get; set; }
    }

    public class VmDateWiseAttendance
    {
        public int EmpHeaderId { get; set; }
        public string EmpId { get; set; }
        public string TeacherName { get; set; }
        public string Designation { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string InTime { get; set; }
        public string OutTime { get; set; }
        public string WorkingHour { get; set; }
        public string AttendanceType { get; set; }
        public int TotalPaunch { get; set; }

        public string LateTime { get; set; }
        public string OverTime { get; set; }
        public string RealOverTime { get; set; }
        public string ApprovedOverTime { get; set; }
        public string EarlyOut { get; set; }
    }

    public class CalculatedFee {
        public decimal Fee { get; set; }
        public string Hours { get; set; }
        public string Minutes { get; set; }
    }
    public class VmBindSelectList
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
    public class VmBillPeriodList
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public BillPeriodType? billPeriodType { get; set; }
    }
}
