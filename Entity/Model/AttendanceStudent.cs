﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entity.Model
{    /// <summary>
     /// This Entity is required for Attendance Data processing. If Need to Change Pls Check with corresponding person.
     /// </summary>
    public class AttendanceStudent
    {
        [Key]
        public long AttendanceId { get; set; }

        public int StudentHeaderId { get; set; }

        [StringLength(50)]
        public string CardNumber { get; set; }

        public DateTime PunchDate { get; set; }

        [StringLength(50)]
        public string StartTime { get; set; }

        [StringLength(50)]
        public string EndTime { get; set; }

        [StringLength(50)]
        public string InTime { get; set; }

        [StringLength(50)]
        public string OutTime { get; set; }

        public int? TimeLate { get; set; }

        public int? EarlyLeave { get; set; }

        public int? DoorNumber { get; set; }
    }
}