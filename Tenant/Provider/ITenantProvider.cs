﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tenant.Provider
{
    public interface ITenantProvider
    {
        int GetTenantId();
        string GetConnectionStringName();
        string GetConnectionString();
    }
}
