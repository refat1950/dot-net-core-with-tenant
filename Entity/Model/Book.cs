using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entity.Model
{
    public partial class Book : BaseEntity
    {
        [Key]
        public int BookId { get; set; }

        [Required]
        [StringLength(50)]
        public string BookName { get; set; }
        public DateTime? PurchaseDate { get; set; }
        public string BillNo { get; set; }
        public string ISBNNo { get; set; }
        public string BookNo { get; set; }
        public int AuthorId { get; set; }
        //public string AuthorName { get; set; }
        [StringLength(20)]
        public string Edition { get; set; }

        public int BookCategoryId { get; set; }

        public string PublisherName { get; set; }
        public int? NoOfCopies { get; set; }

        public int ShelfId { get; set; }

        public int? BookPosition { get; set; }

        //[Column(TypeName = "money")]
        public decimal? BookCost { get; set; }
        public BookLanguage Language { get; set; }

        public BookCondition BookCondition { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? PublishYear { get; set; }

        public bool IsActive { get; set; }

        public virtual BookCategory BookCategory { get; set; }
        public virtual BookShelfs BookShelfs { get; set; }
        public virtual List<IssuedBook> IssuedBook { get; set; }
        public virtual BookAuthor BookAuthor { get; set; }
    }

    public enum BookCondition
    {
        BrandNew = 1,
        MeddiumOld = 2,
        Old = 3,
        Destroyed = 4
    }
    public enum BookLanguage
    {
        Bangla = 1,
        English = 2,
        Arabic = 3,
        Hindi = 4
    }
}
