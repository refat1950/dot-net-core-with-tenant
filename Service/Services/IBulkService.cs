﻿using Service.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Services
{
    public interface IBulkService
    {
        ISessionService sessionService { get; set; }
        FlexValueService flexValueService { get; set; }
    }

    public class BulkService : IBulkService
    {
        public ISessionService sessionService { get; set; }
        public FlexValueService flexValueService { get; set; }
        public BulkService(ISessionService sessionService, FlexValueService flexValueService)
        {
            this.flexValueService = flexValueService;
            this.sessionService = sessionService;
        }
    }
}
