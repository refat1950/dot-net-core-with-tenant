﻿using System.ComponentModel.DataAnnotations;

namespace Entity.Model
{
    public class TempLeave : BaseEntity
    {
        [Key]
        public int Tempid { get; set; }

        public int Empheaderid { get; set; }
        public int NoOfLeave { get; set; }
        public string LeaveDays { get; set; }
        public int PeriodId { get; set; }
    }
}