﻿using System.Collections.Generic;

namespace Services.View_Model
{
    public class VmTeacherDashbord
    {
        public string FullName { get; set; }
        public string TeacherId { get; set; }
        public string ImageUrl { get; set; }
        public string Department { get; set; }
        public string Designation { get; set; }
        public string Section { get; set; }
        public string Campus { get; set; }
        public string Email { get; set; }
        public List<VmTeacherClassRoutine> ClassRoutine { get; set; }
        public List<TeacherNews> TeacherNews { get; set; }
        public AttendanceVm Attendance { get; set; }
        public List<string> News { get; set; }
    }

    public class VmTeacherClassRoutine
    {
        public string Period { get; set; }
        public string Subject { get; set; }
        public string Class { get; set; }
        public string Section { get; set; }
    }

    public class TeacherNews
    {
        public string NewsHead { get; set; }
        public string NewsUrl { get; set; }
    }

    public class AttendanceVm
    {
        public int TotalWorkingDay { get; set; }
        public int Present { get; set; }
        public int Late { get; set; }
        public int EarlyLeave { get; set; }
        public int Absent { get; set; }
        public int DeductionLate { get; set; }
        public int DeductionEarly { get; set; }
        public int LeaveWithoutPay { get; set; }
        public int TotalDeduction { get; set; }
        public int Payable { get; set; }
    }
}

