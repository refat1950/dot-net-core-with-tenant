﻿using data.UnitOfWork;
using Entity.Model;
using Repository.Context;
using Service.Helper;
using Service.Service.Interfaces.Payroll;
using Service.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Services.Payroll
{
    public class MonthlyPeriodGeneration : IPeriodGeneration
    {
        private IUnitOfWork unitOfWork;
        private IMigratedPayrollDataProvider migratedPayrollDataProvider;

        public MonthlyPeriodGeneration(IUnitOfWork unitOfWork, IMigratedPayrollDataProvider migratedPayrollDataProvider)
        {
            this.unitOfWork = unitOfWork;
            this.migratedPayrollDataProvider = migratedPayrollDataProvider;
        }

        public void Generate(bool autoActiveCurrentYear)
        {
            // fix all existing period if need
            migratedPayrollDataProvider.FixAllPeriodStartEndDate();

            var bdDateNow = BdDateTime.Now();
            var currentYear = bdDateNow.Year;
            for (int year = currentYear-1; year <= currentYear+1; year++)
            {
                string periodName = "";
                var thisYear = year.ToString();
                var year_lastTwoDigit = thisYear.Substring(thisYear.Length - 2, 2);
                int counterMonthInitial = 1;
                int counterMonthRange = 1;
                for (int i = 0; i < 12; i++)
                {
                    var lastDateOfMonth = new DateTime(year, counterMonthRange, 1).AddMonths(1).AddDays(-1).AddHours(23).AddMinutes(59).AddSeconds(59);
                    var lastDayOfMonth = lastDateOfMonth.Day;

                    var periodStartDate = new DateTime(year, counterMonthInitial, 1);
                    var periodEndDate = new DateTime(year, counterMonthRange, lastDayOfMonth);
                    var monthNameStart = periodStartDate.ToString("MMM");
                    var monthNameEnd = periodEndDate.ToString("MMM");

                    periodName = monthNameStart + "-" + year_lastTwoDigit;
                    var periodtypeId = BillPeriodType.Monthly;

                    // update PeriodTypeId for this type of monthly period
                    var thisPeriod = unitOfWork.PeriodRepo.FirstOrDefault(w => w.StartDate == periodStartDate && w.EndDate == periodEndDate);
                    if (thisPeriod != null)
                    {
                        if (thisPeriod.PeriodTypeId == null)
                        {
                            thisPeriod.PeriodTypeId = periodtypeId;
                        }
                    }
                    else
                    {
                        BillPeriod billPeriod = new BillPeriod();
                        billPeriod.MonthId = counterMonthInitial;
                        billPeriod.PeriodName = periodName;
                        billPeriod.Year = year;
                        if (autoActiveCurrentYear && currentYear == year) // if this is current year
                            billPeriod.IsVisible = true;
                        else
                            billPeriod.IsVisible = false;
                        billPeriod.PeriodTypeId = periodtypeId;
                        billPeriod.StartDate = periodStartDate;
                        billPeriod.EndDate = periodEndDate;
                        unitOfWork.PeriodRepo.Add(billPeriod);
                    }
                    counterMonthInitial = counterMonthInitial + 1;
                    counterMonthRange = counterMonthRange + 1;
                }
            }
            unitOfWork.SaveChanges();
        }
    }
}
