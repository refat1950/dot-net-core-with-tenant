﻿using data.UnitOfWork;
using Entity.Model;
using Repository.Context;
using Service.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Services
{
    public class SessionService:ISessionService
    {
        private readonly IUnitOfWork unitOfWork;

        public SessionService(IUnitOfWork unitOfWork) {
            this.unitOfWork = unitOfWork;
        }

        public Session GetActiveSession()
        {
            var session = unitOfWork.SessionRepo.Find(f => f.Status == true).FirstOrDefault();
            if (session != null)
            {
                return session;
            }
            else{
                return new Session();
            }
        }

        public bool isExists(int year)
        {
            var isEx = unitOfWork.SessionRepo.Find(f => (f.StartDate != null && f.StartDate.Value.Year == year) || (f.EndDate != null && f.EndDate.Value.Year == year)).Any();
            return isEx;
        }

        public List<VmSelectLists> GetAllIsVisibleSessions()
        {
            var session = unitOfWork.SessionRepo.Find(s => s.IsVisible == true).Select(s => new VmSelectLists
            {
                Id = s.SessionId,
                Name = s.SessionName
            }).ToList();
            return session;
        }
    }

    public class VmSelectLists
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
