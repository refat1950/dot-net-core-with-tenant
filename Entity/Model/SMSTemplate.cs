﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class SMSTemplate : BaseEntity
    {
        [Key]
        public int TemplateId { get; set; }
        public string Name { get; set; }
        public string Template { get; set; }
        public string Vairables { get; set; }
        public SmsTemplateType Type { get; set; }
        public SmsTemplateSetupFor? SmsFor { get; set; }
    }
    public enum SmsTemplateType
    {
        AftertBillRecieve = 1,
        AfterAdmission = 2,
        StudentLoginCredential = 3,
        TeacherOrEmployeeLoginCredential = 4,
        StudentPasswordReset = 5,
        EmployeePasswordReset = 6,
        Others = 7
    }
    public enum SmsTemplateSetupFor
    {
        TeacherOrEmployee = 1,
        Student = 2
    }
}
