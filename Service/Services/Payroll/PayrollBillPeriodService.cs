﻿using data.UnitOfWork;
using Entity.Model;
using Repository.Context;
using Service.Helper;
using Service.Services;
using Service.ViewModel;
using Service.ViewModel.Payroll;
using Services.CoreServices;
using System;
using System.Collections.Generic;
using System.Linq;
namespace Service.Services.Payroll
{
    public class PayrollBillPeriodService
    {
        private IUnitOfWork unitOfWork;
        public PayrollBillPeriodService(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public BillPeriod GetBiggestPeriodByPeriodId(List<int> PeriodIds)
        {
            if (PeriodIds.Any() && PeriodIds != null)
            {
                var allPeriods = unitOfWork.PeriodRepo.Find(f => PeriodIds.Contains(f.PeriodId)).ToList();
                // let the 1st period as biggest period
                var BiggestPeriodId = PeriodIds[0];
                var AssumingBigPeriod = allPeriods.FirstOrDefault(s => s.PeriodId == BiggestPeriodId);
                if (AssumingBigPeriod != null)
                {
                    DateTime AssumingBigDate = AssumingBigPeriod.EndDate.Value.Date;
                    foreach (var pid in PeriodIds)
                    {
                        var endDate_j = allPeriods.FirstOrDefault(s => s.PeriodId == pid);
                        if (AssumingBigDate < endDate_j.EndDate.Value.Date)
                        {
                            AssumingBigDate = endDate_j.EndDate.Value.Date;
                            BiggestPeriodId = pid;
                        }
                    }
                    return allPeriods.FirstOrDefault(s => s.PeriodId == BiggestPeriodId);
                }
                else
                    return new BillPeriod();
            }
            else
                return new BillPeriod();
        }
        public VmBillPeriodsData GetBiggestPeriodByPeriodId(List<VmBillPeriodsData> billPeriods, List<int> PeriodIds)
        {
            if (PeriodIds.Any() && PeriodIds != null)
            {
                var allPeriods = billPeriods.Where(f => PeriodIds.Contains(f.BillPeriodId)).ToList();
                // let the 1st period as biggest period
                var BiggestPeriodId = PeriodIds[0];
                var AssumingBigPeriod = allPeriods.FirstOrDefault(s => s.BillPeriodId == BiggestPeriodId);
                if (AssumingBigPeriod != null)
                {
                    DateTime AssumingBigDate = AssumingBigPeriod.EndDate.Value.Date;
                    foreach (var pid in PeriodIds)
                    {
                        var endDate_j = allPeriods.FirstOrDefault(s => s.BillPeriodId == pid);
                        if (AssumingBigDate < endDate_j.EndDate.Value.Date)
                        {
                            AssumingBigDate = endDate_j.EndDate.Value.Date;
                            BiggestPeriodId = pid;
                        }
                    }
                    return allPeriods.FirstOrDefault(s => s.BillPeriodId == BiggestPeriodId);
                }
                else
                    return new VmBillPeriodsData();
            }
            else
                return new VmBillPeriodsData();
        }

        public BillPeriod GetSmallestPeriodByPeriodId(List<int> PeriodIds)
        {
            if (PeriodIds.Any() && PeriodIds != null)
            {
                var allPeriods = unitOfWork.PeriodRepo.Find(f => PeriodIds.Contains(f.PeriodId)).ToList();
                // let the 1st period as biggest period
                var BiggestPeriodId = PeriodIds[0];
                var AssumingBigPeriod = allPeriods.FirstOrDefault(s => s.PeriodId == BiggestPeriodId);
                if (AssumingBigPeriod != null)
                {
                    DateTime AssumingBigDate = AssumingBigPeriod.EndDate.Value.Date;
                    foreach (var pid in PeriodIds)
                    {
                        var endDate_j = allPeriods.FirstOrDefault(s => s.PeriodId == pid);
                        if (AssumingBigDate > endDate_j.EndDate.Value.Date)
                        {
                            AssumingBigDate = endDate_j.EndDate.Value.Date;
                            BiggestPeriodId = pid;
                        }
                    }
                    return allPeriods.FirstOrDefault(s => s.PeriodId == BiggestPeriodId);
                }
                else
                    return new BillPeriod();
            }
            else
                return new BillPeriod();
        }

        public BillPeriod GetFirstPeriod(BillPeriodType PeriodTypeId)
        {
            var GetPeriods = unitOfWork.PeriodRepo.Find(s => s.PeriodTypeId == PeriodTypeId).Select(s => new { s.PeriodId, s.StartDate, s.EndDate }).ToList();
            var CuurentDate = DateTime.Now.Date;
            var CuurentPeriodId = 0;
            var FirstPeriodId = 0;

            if (GetPeriods.Any())
            {
                foreach (var item in GetPeriods)
                {
                    var startDate = item.StartDate.Value.Date;
                    var EndDate = item.EndDate.Value.Date;
                    // Assuming you know EndDate > startDate
                    if (CuurentDate >= startDate && CuurentDate <= EndDate)
                    {
                        CuurentPeriodId = item.PeriodId;
                        break;
                    }
                }
                var PreviousDate = unitOfWork.PeriodRepo.FirstOrDefault(s => s.PeriodId == CuurentPeriodId).StartDate.Value.AddDays(-1);

                for (int i = 0; i < GetPeriods.Count; i++)
                {
                    foreach (var items in GetPeriods)
                    {

                        var startDate = items.StartDate.Value.Date;
                        var EndDate = items.EndDate.Value.Date;
                        // Assuming you know EndDate > startDate
                        if (PreviousDate >= startDate && PreviousDate <= EndDate)
                        {
                            FirstPeriodId = items.PeriodId;
                            PreviousDate = startDate.AddDays(-1);
                        }
                    }
                }

                if (FirstPeriodId == 0) //if its happens one time in a year, its the 1st and last period
                {
                    FirstPeriodId = CuurentPeriodId;
                }
            }
            else
                FirstPeriodId = 0;

            return unitOfWork.PeriodRepo.FirstOrDefault(s => s.PeriodId == FirstPeriodId);
        }

        public BillPeriod GetNextPeriod(int PeriodId)
        {
            var period = unitOfWork.PeriodRepo.FirstOrDefault(s => s.PeriodId == PeriodId);
            if (period != null)
            {
                var NextDate = period.EndDate.Value.Date.AddDays(1);
                var NextPeriod = unitOfWork.PeriodRepo.FirstOrDefault(w => w.PeriodTypeId == period.PeriodTypeId && NextDate >= CoreServices.TruncateTime(w.StartDate.Value) && NextDate <= CoreServices.TruncateTime(w.EndDate.Value));
                //var nid = NextPeriod != null ? NextPeriod.BillPeriodId : 0;
                return NextPeriod;
            }
            else
                return new BillPeriod();
        }
        public VmBillPeriodsData GetNextPeriod(List<VmBillPeriodsData> billPeriods,int PeriodId)
        {
            var period = billPeriods.FirstOrDefault(s => s.BillPeriodId == PeriodId);
            if (period != null)
            {
                var NextDate = period.EndDate.Value.Date.AddDays(1);
                var NextPeriod = billPeriods.FirstOrDefault(w => w.PeriodTypeId == period.PeriodTypeId && NextDate >= w.StartDate.Value.Date && NextDate <= w.EndDate.Value.Date);
                //var nid = NextPeriod != null ? NextPeriod.BillPeriodId : 0;
                return NextPeriod;
            }
            else
                return new VmBillPeriodsData();
        }
        public List<VmBillPeriodList> GetSynchronizedBillPeriodList()
        {
            var dList = new List<VmBillPeriodList>();
            var periodGroups = unitOfWork.PeriodRepo.Find(f => f.Year > 2017).OrderBy(o => o.Year).GroupBy(g => g.Year).ToList();
            foreach (var pGroup in periodGroups)
            {
                foreach (var ptGroup in pGroup.GroupBy(g=>g.PeriodTypeId).ToList())
                {
                    var smallestPeriod = GetSmallestPeriodByPeriodId(ptGroup.Select(s => s.PeriodId).ToList());
                    var biggestPeriod = GetBiggestPeriodByPeriodId(ptGroup.Select(s => s.PeriodId).ToList());
                    if (smallestPeriod != null && smallestPeriod.EndDate != null && biggestPeriod != null && biggestPeriod.EndDate != null)
                    {
                        while (smallestPeriod != null && smallestPeriod.EndDate.Value.Date <= biggestPeriod.EndDate.Value.Date)
                        {
                            var d = new VmBillPeriodList();
                            d.Id = smallestPeriod.PeriodId;
                            d.Name = smallestPeriod.PeriodName;
                            d.billPeriodType = smallestPeriod.PeriodTypeId;
                            dList.Add(d);
                            smallestPeriod = GetNextPeriod(smallestPeriod.PeriodId);
                        }
                    }
                }
            }
            return dList;
        }

        public BillPeriodType? GetPayrollTypeIdByPayrollId(int payrollId)
        {
            //every periodIds of this payroll is assigend to this table
            BillPeriodType? payrollTypeId = BillPeriodType.Monthly;
            var HrBillPeriodWisePayRollIssueDatesRepo = unitOfWork.HrBillPeriodWisePayRollIssueDatesRepo.FirstOrDefault(w => w.PayRollId == payrollId);
            var periodId = HrBillPeriodWisePayRollIssueDatesRepo != null ? HrBillPeriodWisePayRollIssueDatesRepo.PeriodId : 0;
            if (periodId != 0)
                payrollTypeId = unitOfWork.PeriodRepo.FirstOrDefault(s => s.PeriodId == periodId).PeriodTypeId;
            return payrollTypeId;
        }
        public BillPeriodType? GetPayrollTypeIdByPayrollId(List<VmBillPeriodsData> billPeriods, int payrollId)
        {
            //every periodIds of this payroll is assigend to this table
            BillPeriodType? payrollTypeId = BillPeriodType.Monthly;
            var HrBillPeriodWisePayRollIssueDatesRepo = unitOfWork.HrBillPeriodWisePayRollIssueDatesRepo.FirstOrDefault(w => w.PayRollId == payrollId);
            var periodId = HrBillPeriodWisePayRollIssueDatesRepo != null ? HrBillPeriodWisePayRollIssueDatesRepo.PeriodId : 0;
            if (periodId != 0)
                payrollTypeId = billPeriods.FirstOrDefault(s => s.BillPeriodId == periodId).PeriodTypeId;
            return payrollTypeId;
        }

        public BillPeriod GetPeriodById(int periodId)
        {
            var period = unitOfWork.PeriodRepo.FirstOrDefault(s => s.PeriodId == periodId);
            return period != null ? period : new BillPeriod();
        }

        public void FixCurrentlyActivePeriodsForAllPeriodtype()
        {
            //fix currently active periods for all period type
            var allYearGroup = unitOfWork.PeriodRepo.GetAll().GroupBy(g => g.Year).ToList();
            foreach (var yearGroup in allYearGroup)
            {
                if (yearGroup != null && yearGroup.Where(w => w.IsVisible == true).Any())
                {
                    foreach (var y in yearGroup)
                        y.IsVisible = true;
                    unitOfWork.SaveChanges();
                }
            }
        }
    }
}
