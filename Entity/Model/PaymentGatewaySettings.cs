﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class PaymentGatewaySetting : BaseEntity
    {
        [Key]
        public int GatewayHeaderId { get; set; }
        public string MerchantNumber { get; set; }
        public string APIorURL { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string AccessOrAppkey { get; set; }
        public string SecretkeyOrToken { get; set; }
        public string CustomerOrClientCode { get; set; }
        public string PartnerCode { get; set; }
        public string BankName { get; set; }
        public string AccountNumber { get; set; }
        public int? GatewayProvider { get; set; }
        public string Attribute7 { get; set; }
        public string Attribute8 { get; set; }
        public string Attribute9 { get; set; }
        public string Attribute10 { get; set; }
        public bool? PaymentStatus { get; set; }
        public DateTime? ActivationDate { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public int? CompanyId { get; set; }
    }
}
