﻿using System.ComponentModel.DataAnnotations;

namespace Entity.Model
{
    public class FeeSubCategoryWaiver : BaseEntity
    {
        [Key]
        public int FeeSubCategoryWaiverId { get; set; }
        public int FeeSubCategoryId { get; set; }
        public int Category_GenderID { get; set; }
        public WaiverType WaiverType { get; set; }
        public decimal Percentage { get; set; }
        public virtual FeeSubCategory FeeSubCategory { get; set; }
    }

    public enum WaiverType
    {
        Gender = 1,
        Category = 2
    }
}
