using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entity.Model
{
    public partial class HrEmployeeRefrence : BaseEntity
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int EmpHeaderId { get; set; }

        public int ReferenceId { get; set; }

        [StringLength(50)]
        public string ReferenceName { get; set; }

        [StringLength(100)]
        public string Designation { get; set; }

        [StringLength(100)]
        public string Address1 { get; set; }

        [StringLength(100)]
        public string Address2 { get; set; }

        [StringLength(100)]
        public string Address3 { get; set; }

        [StringLength(30)]
        public string ResPhone { get; set; }

        [StringLength(30)]
        public string OffPhone { get; set; }

        [StringLength(30)]
        public string Mobile1 { get; set; }

        [StringLength(30)]
        public string Mobile2 { get; set; }

        [StringLength(50)]
        public string Email { get; set; }

        [StringLength(30)]
        public string Relation { get; set; }

    }
}
