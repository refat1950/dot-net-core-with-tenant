﻿using Repository.Utility;
using Service.ViewModel.Payroll;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Services.Interfaces
{
    public interface IPaySlipDataProvider
    {
        VmPaySlip GetPaySlipData(int empHeaderId, int periodId, int positionid, int payrollid);
        List<VmEmpoyeePeriod> GetEmployesAllPeriodsPayslipData(List<int> EmpIds, EmpStatus status);
        VmEmpoyeePeriod GetEmployeeBillPeriods(int EmpHeaderId);
    }
}
