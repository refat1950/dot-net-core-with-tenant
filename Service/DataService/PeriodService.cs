﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using data.UnitOfWork;
using Entity.Model;
using Service.Helper;
using Service.ViewModel;

namespace Service.DataService
{
    public class PeriodService : IPeriodService
    {
        private IUnitOfWork _unitOfWork;
        public PeriodService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public void AddRange(IEnumerable<BillPeriod> periods)
        {
            _unitOfWork.PeriodRepo.AddRange(periods);
            SaveChanges();
        }

        public bool AlreadyExists(int year)
        {
            return _unitOfWork.PeriodRepo.Find(f => f.Year == year).Any();
        }

        public IEnumerable<BillPeriod> GetActivePeriods()
        {
            return _unitOfWork.PeriodRepo.Find(w => w.IsVisible == true);
        }

        public IEnumerable<BillPeriod> Search(Expression<Func<BillPeriod, bool>> predicate)
        {
            return _unitOfWork.PeriodRepo.Find(predicate);
        }

        public IEnumerable<VmPeriodYear> GetYears()
        {
            return _unitOfWork.PeriodRepo.GetAll().GroupBy(g=>g.Year).Select(s=> new VmPeriodYear { Year=s.Key,IsVisible=s.FirstOrDefault()!=null?s.FirstOrDefault().IsVisible:false});
        }

        public int SaveChanges()
        {
           return _unitOfWork.SaveChanges();
        }

        public BillPeriod GetCurrentPeriod()
        {
            int year = BdDateTime.Today().Year;
            int month = BdDateTime.Today().Month;
            return _unitOfWork.PeriodRepo.Find(w => w.MonthId == month && w.Year == year).FirstOrDefault();
        }

        public BillPeriod GetNextPeriod(int PeriodId)
        {
            var period = _unitOfWork.PeriodRepo.Get(PeriodId);
            var month = period.MonthId;
            var year = period.Year;
            if (month == 12)
            {
                return _unitOfWork.PeriodRepo.Find(w => w.MonthId == 1 && w.Year == year+1).FirstOrDefault();
            }
            return _unitOfWork.PeriodRepo.Find(w => w.MonthId == month+1 && w.Year == year).FirstOrDefault();
        }


        public BillPeriod GetPreviousPeriod(int PeriodId)
        {
            var period = _unitOfWork.PeriodRepo.Get(PeriodId);
            var month = period.MonthId;
            var year = period.Year;
            if(month == 1)
            {
                return _unitOfWork.PeriodRepo.Find(w => w.MonthId == 12 && w.Year == year - 1).FirstOrDefault();
            }
        
            return _unitOfWork.PeriodRepo.Find(w => w.MonthId == month - 1 && w.Year == year).FirstOrDefault();
        }
        /// <summary>
        /// here fromPeriodId is base if toPeriod is greater then it will return true
        /// </summary>
        /// <param name="fromPeriodId"></param>
        /// <param name="toPeriodId"></param>
        /// <returns>here fromPeriodId is base if toPeriod is greater then it will return true</returns>
        public bool IsCurrentPeriodGreaterOrNot(int fromPeriodId,int toPeriodId)
        {
            var fromPeriod= _unitOfWork.PeriodRepo.Get(fromPeriodId);
            var toPeriod= _unitOfWork.PeriodRepo.Get(toPeriodId);
            if (toPeriod.Year > fromPeriod.Year) return true;
            else if (toPeriod.Year == fromPeriod.Year && toPeriod.MonthId > fromPeriod.MonthId) return true;
            return false;
        }

        public int GetPeriodDistance(int fromMonth,int toMonth)
        {
            int cnt = 1;
            while (fromMonth != toMonth)
            {
                fromMonth = GetNextPeriod(fromMonth).PeriodId;
                cnt++;
            }
            return cnt;//_unitOfWork.PeriodRepo.Find(w => w.BillPeriodId >= fromMonth && w.BillPeriodId <= toMonth).Count();
        }

        
        public BillPeriod GetPeriodByDate(DateTime date)
        {
            int year = date.Year;
            int month = date.Month;
            if (!_unitOfWork.PeriodRepo.Find(w => w.MonthId == month && w.Year == year).Any())
            {
                List<BillPeriod> periods = new List<BillPeriod>();
                    if (!AlreadyExists(year))
                    {
                        for (int j = 1; j <= 12; j++)
                        {
                            string monthName = new DateTime(year, j, 1).ToString("MMM-yy");
                            BillPeriod period = new BillPeriod();
                            period.MonthId = j;
                            period.PeriodName = monthName;
                            period.Year = year;
                            period.IsVisible = false;
                            periods.Add(period);
                        }
                    }
                AddRange(periods);
            }
            return _unitOfWork.PeriodRepo.Find(w => w.MonthId == month && w.Year == year).FirstOrDefault();
        }

        public BillPeriod GetPeriodById(int id)
        {
            return _unitOfWork.PeriodRepo.Find(w => w.PeriodId == id).FirstOrDefault();
        }
    }
}
