﻿using Entity.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class StudentHistory : BaseEntitiesMax
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int StudentHeaderId { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int? BkupSessionId { get; set; }
        [Required]
        [StringLength(30)]
        public string StudentId { get; set; }

        [Required]
        [StringLength(30)]
        public string FirstName { get; set; }

        [StringLength(30)]
        public string MiddleName { get; set; }

        [StringLength(30)]
        public string LastName { get; set; }

        [StringLength(50)]
        public string NickName { get; set; }

        [StringLength(30)]
        public string PresHoldingNo { get; set; }

        [StringLength(50)]
        public string PresStreet { get; set; }

        [StringLength(50)]
        public string PresArea { get; set; }

        public int? PresThanaId { get; set; }

        [StringLength(10)]
        public string PresPostCode { get; set; }

        public int? PresCityId { get; set; }

        public int? PresDivisionId { get; set; }

        public int? PresCountryId { get; set; }

        [StringLength(30)]
        public string PermHoldingNo { get; set; }

        [StringLength(50)]
        public string PermStreet { get; set; }

        [StringLength(50)]
        public string PermArea { get; set; }

        [StringLength(10)]
        public string PermPostCode { get; set; }

        public int? PermThanaId { get; set; }

        public int? PermCityId { get; set; }

        public int? PermDivisionId { get; set; }

        public int? PermCountryId { get; set; }

        public DateTime? Dob { get; set; }

        public int? BirthCity { get; set; }

        public int? BirthCountry { get; set; }

        [StringLength(60)]
        public string PlaceOfBirth { get; set; }

        public DateTime? AdmissionDate { get; set; }

        public int? AdmissionSessionId { get; set; }

        public int? AdmissionClassId { get; set; }

        public int? AdmissionSectionId { get; set; }

        public int? AdmissionShift { get; set; }


        public int? SessionId { get; set; }

        public int? ClassId { get; set; }

        public int? SectionId { get; set; }

        public int? Shift { get; set; }

        public int? CampusId { get; set; }

        public DateTime? EndDate { get; set; }

        public bool? Status { get; set; }

        [StringLength(10)]
        public string Gender { get; set; }

        [StringLength(30)]
        public string Religion { get; set; }

        [StringLength(30)]
        public string ResPhone { get; set; }

        [StringLength(30)]
        public string Mobile { get; set; }

        [StringLength(100)]
        public string Email { get; set; }

        [StringLength(10)]
        public string BloodGroup { get; set; }

        [StringLength(60)]
        public string Nationality { get; set; }

        [StringLength(30)]
        public string BirthCertificateNo { get; set; }

        [StringLength(30)]
        public string PassportNo { get; set; }

        [StringLength(200)]
        public string MedicalHistory { get; set; }

        [StringLength(200)]
        public string EmergencyMedicalAction { get; set; }

        [StringLength(30)]
        public string FatherFirstName { get; set; }

        [StringLength(30)]
        public string FatherMiddleName { get; set; }

        [StringLength(30)]
        public string FatherLastName { get; set; }

        [StringLength(50)]
        public string FatherEducation { get; set; }

        [StringLength(200)]
        public string FatherEducationDetails { get; set; }

        [StringLength(30)]
        public string FatherOccupation { get; set; }

        [StringLength(150)]
        public string FatherOccupationDetails { get; set; }

        [StringLength(200)]
        public string FatherOfficeAddress { get; set; }

        [StringLength(30)]
        public string FatherOfficePhone { get; set; }

        [StringLength(30)]
        public string FatherMobile { get; set; }

        [StringLength(30)]
        public string FatherNationalId { get; set; }

        [StringLength(30)]
        public string FatherPassportNo { get; set; }

        [StringLength(30)]
        public string MotherFirstName { get; set; }

        [StringLength(30)]
        public string MotherMiddleName { get; set; }

        [StringLength(30)]
        public string MotherLastName { get; set; }

        [StringLength(50)]
        public string MotherEducation { get; set; }

        [StringLength(200)]
        public string MotherEducationDetails { get; set; }

        [StringLength(30)]
        public string MotherOccupation { get; set; }

        [StringLength(150)]
        public string MotherOccupationDetails { get; set; }

        [StringLength(200)]
        public string MotherOfficeAddress { get; set; }

        [StringLength(30)]
        public string MotherOfficePhone { get; set; }

        [StringLength(30)]
        public string MotherMobile { get; set; }

        [StringLength(30)]
        public string MotherNationalId { get; set; }

        [StringLength(30)]
        public string MoherPassportNo { get; set; }

        [StringLength(30)]
        public string LocGuardianFirstName { get; set; }

        [StringLength(30)]
        public string LocGuardianMiddleName { get; set; }

        [StringLength(30)]
        public string LocGuardianLastName { get; set; }

        [StringLength(30)]
        public string LocGuardianRelationship { get; set; }

        [StringLength(30)]
        public string LocGuardianMobile { get; set; }

        [StringLength(30)]
        public string LocalGurdianNationalId { get; set; }

        [StringLength(30)]
        public string LocalGurdianPassportNo { get; set; }

        [StringLength(50)]
        public string StudentPhotoPath { get; set; }

        [StringLength(15)]
        public string ProximityNum { get; set; }

        [StringLength(15)]
        public string ExitReasons { get; set; }

    }
}
