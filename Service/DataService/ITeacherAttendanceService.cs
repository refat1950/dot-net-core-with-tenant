﻿using Service.View_Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.DataService
{
    public interface ITeacherAttendanceService
    {
        List<VmDateWiseAttendance> GetBetween(int start, int displayLength, string searchValue, string selectedDate, out int totalLength);
        List<VmDateWiseAttendance> GetDayAttendance(string selectedDate);
    }
}
