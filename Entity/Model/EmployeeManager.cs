﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class EmployeeManager : BaseEntity
    {
        public int EmpHeaderId { get; set; }
        public int ManagerId { get; set; }
        public int ManagerType { get; set; }
    }
}
