using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Entity.Model
{
    public partial class Session : BaseEntity
    {
        [Key]
        public int SessionId { get; set; }

        [Required]
        [StringLength(50)]
        public string SessionName { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public bool? Status { get; set; }
        public int? SessionType { get; set; }
        public bool? IsVisible { get; set; }
        public virtual ICollection<BillInfo> BillInfo { get; set; }
    }
}
