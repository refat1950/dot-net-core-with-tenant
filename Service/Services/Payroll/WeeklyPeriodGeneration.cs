﻿using data.UnitOfWork;
using Entity.Model;
using Repository.Context;
using Repository.Utility;
using Service.Helper;
using Service.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Services.Payroll
{
    public class WeeklyPeriodGeneration : IPeriodGeneration
    {
        private IUnitOfWork unitOfWork;

        public WeeklyPeriodGeneration(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public void Generate(bool autoActiveCurrentYear)
        {
            string periodName = "";
            var bdDateNow = BdDateTime.Now();
            var currentYear = bdDateNow.Year;
            var billPeriodList = new List<BillPeriod>();
           
            for (int year = currentYear - 1; year <= currentYear + 1; year++)
            {
                var StartDate = new DateTime(year, 1, 1).Date;
                var endDate = new DateTime(year, 12, 31).Date;
                
                var asas = new List<string>();

                var vmWeekPeriods = new List<VmWeekPeriod>();

                for (int month = 1; month <= 12; month++)
                {
                    var periodStartDate = new DateTime(year, month, 1);
                    var lastDateOfMonth = new DateTime(year, month, 1).AddMonths(1).AddDays(-1).AddHours(23).AddMinutes(59).AddSeconds(59).Date;
                    
                    var week = 1;
                    while (periodStartDate <= lastDateOfMonth)
                    {
                        var d = new VmWeekPeriod();
                        if (week == 4) //last week
                        {
                            var distanceTillWeekEnd = (int)(lastDateOfMonth - periodStartDate).TotalDays + 1;
                            d.StartDate = periodStartDate;
                            d.EndDate = periodStartDate.AddDays(distanceTillWeekEnd - 1);
                            d.NumOfDays = distanceTillWeekEnd;
                        }
                        else
                        {
                            d.StartDate = periodStartDate;
                            d.EndDate = periodStartDate.AddDays(6);
                            d.NumOfDays = 7;
                            periodStartDate = d.EndDate.AddDays(1);
                            
                        }
                        vmWeekPeriods.Add(d);
                        if (week == 4)
                            break;
                        week++;
                    }
                }

                //var dateDistance = (int)(endDate - StartDate).TotalDays + 1;
                //for (var i = 1; i <= dateDistance; i = i + 7)
                //{
                //    for (var j = i; j < (i + 7); j++)
                //    {
                //        var thisDay = StartDate.AddDays(j - 1);
                //        var thisDate_Day = (int)thisDay.DayOfWeek;
                //        if ((int)StartDate.DayOfWeek == thisDate_Day && thisDay.Date <= endDate)
                //        {
                //            var d = new VmWeekPeriod();
                //            if (count == 4) // last week
                //            {
                //                var lastDateOfMonth = new DateTime(thisDay.Year, thisDay.Month, 1).AddMonths(1).AddDays(-1).AddHours(23).AddMinutes(59).AddSeconds(59);
                //                var distanceTillWeekEnd = (int)(lastDateOfMonth - thisDay).TotalDays + 1;
                //                d.StartDate = thisDay;
                //                d.EndDate = thisDay.AddDays(distanceTillWeekEnd - 1);
                //                d.NumOfDays = distanceTillWeekEnd;
                //                count = 1;
                //            }
                //            else
                //            {
                //                d.StartDate = thisDay;
                //                d.EndDate = thisDay.AddDays(6);
                //                d.NumOfDays = 7;
                                
                //            }
                //            vmWeekPeriods.Add(d);
                //            count++;
                            
                //            //if (c < 52)
                //            //{
                //            //    if (c == 51) //last weeek of the year
                //            //    {
                //            //        var distanceTillYearEnd = (int)(endDate - thisDay).TotalDays + 1;
                //            //        d.StartDate = thisDay;
                //            //        d.EndDate = thisDay.AddDays(distanceTillYearEnd - 1);
                //            //        d.NumOfDays = distanceTillYearEnd;
                //            //        //asas.Add(thisDay.ToString("dd MMM yy") + " - " + thisDay.AddDays(distanceTillYearEnd).ToString("dd MMM yy") + " (" + distanceTillYearEnd + " days)");
                //            //    }
                //            //    else
                //            //    {
                //            //        d.StartDate = thisDay;
                //            //        d.EndDate = thisDay.AddDays(6);
                //            //        d.NumOfDays = 7;

                //            //        //asas.Add(thisDay.ToString("dd MMM yy") + " - " + thisDay.AddDays(6).ToString("dd MMM yy") + " (7 days)");
                //            //    }
                //            //    c++;
                //            //    vmWeekPeriods.Add(d);
                //            //}
                //        }
                //    }
                //}
                
                foreach (var week in vmWeekPeriods)
                {
                    var periodStartDate = week.StartDate;
                    var periodEndDate = week.EndDate;
                    var periodtypeId = BillPeriodType.Weekly;
                    periodName = "" + periodStartDate.ToString("dd") + " - " + periodEndDate.ToString("dd") + " "+ periodStartDate.ToString("MMM") + " "+ periodEndDate.ToString("yyyy") + " (" + week.NumOfDays + " days)";
                    asas.Add(periodName);
                    // update PeriodTypeId for this type of Tri_Annual period
                    var thisPeriod = unitOfWork.PeriodRepo.FirstOrDefault(w => w.StartDate == periodStartDate && w.EndDate == periodEndDate);
                    if (thisPeriod != null)
                    {
                        if (thisPeriod.PeriodTypeId == null)
                        {
                            thisPeriod.PeriodTypeId = periodtypeId;
                        }
                    }
                    else
                    {
                        BillPeriod billPeriod = new BillPeriod();
                        billPeriod.MonthId = 0;
                        billPeriod.PeriodName = periodName;
                        billPeriod.Year = year;
                        if (autoActiveCurrentYear && currentYear == year) // if this is current year
                            billPeriod.IsVisible = true;
                        else
                            billPeriod.IsVisible = false;
                        billPeriod.PeriodTypeId = periodtypeId;
                        billPeriod.StartDate = periodStartDate;
                        billPeriod.EndDate = periodEndDate;
                        billPeriod.Attribute1 = week.NumOfDays.ToString();
                        billPeriodList.Add(billPeriod);
                    }
                }
            }

            if (billPeriodList.Any())
            {
                unitOfWork.PeriodRepo.AddRange(billPeriodList);
            }
            unitOfWork.SaveChanges();
        }

        public class VmWeekPeriod
        {
            public DateTime StartDate { get; set; }
            public DateTime EndDate { get; set; }
            public int NumOfDays { get; set; }
        }
    }
}
