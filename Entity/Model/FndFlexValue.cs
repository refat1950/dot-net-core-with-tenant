using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Entity.Model
{
    public partial class FndFlexValue : BaseEntity
    {
        [Key]
        public int FlexValueId { get; set; }

        [Required]
        [StringLength(100)]
        public string FlexValue { get; set; }

        [StringLength(50)]
        public string FlexShortValue { get; set; }

        public int FlexValueSetId { get; set; }

        public int? ParentFlexValueId { get; set; }

        public int? ParentFlexValueSetId { get; set; }

        public virtual FndFlexValueSet FndFlexValueSets { get; set; }
    }
}
