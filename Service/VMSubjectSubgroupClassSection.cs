﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class VMSubjectSubgroupClassSection
    {
        public int SUBGROUP_ID { get; set; }
        public int CLASS_ID { get; set; }
        public int? SECTION_ID { get; set; }
        public int TEACHER_ID { get; set; }
        public int SESSION_ID { get; set; }
        public int MASTER_EVENT_ID { get; set; }
        public int EVENT_ID { get; set; }
        public string SUBGROUP_NAME { get; set; }
        public int? NO_OF_EXAM { get; set; }
        public int? BEST_COUNT { get; set; }

    }
}
