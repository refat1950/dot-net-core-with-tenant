﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.View_Model
{
    public class ReturnGenerateRoutineView
    {
        public List<Routinedates> Routinedates { get; set; }
        public List<ExcludeRoutineDates> ExcludeDate { get; set; }
        public int ValidDateCount { get; set; }
    }
    public class GenerateRoutineView
    {
        public int SessionId { get; set; }
        public int SemesterId { get; set; }
        public int Shift { get; set; }

        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public int DayCount { get; set; }
        public int ClassId { get; set; }
        public int SectionId { get; set; }
        public int PeriodId { get; set; }
        public int SubjectId { get; set; }
        public int? SubjectSubgroupId { get; set; }
        public int TeacherId { get; set; }
        public int? BookId { get; set; }
        public List<int> Day { get; set; }
    }

    public class Routinedates
    {
        public string Date { get; set; }
        public string DateName { get; set; }
        public string Period { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string Class { get; set; }
        public string Section { get; set; }
    }
    public class ExcludeRoutineDates
    {
        public string Date { get; set; }
        public string DateName { get; set; }
        public string Reason { get; set; }
    }

    public class ExamRoutineGen
    {
        public int SessionId { get; set; }
        public int ClassId { get; set; }
        public int SectionId { get; set; }
        public int ExamId { get; set; }
    }

    public class ExamRoutineSave
    {
        public int SessionId { get; set; }
        public int ClassId { get; set; }
        public int SectionId { get; set; }
        public int ExamId { get; set; }
        public  List<ExamClassSubjects> ExamClassSubject { get; set; }
    }

    public class ExamClassSubjects
    {
        public int SubjectId { get; set; }

        public string SubjectName { get; set; }

        public string ExamDate { get; set; }

        public string StartTime { get; set; }

        public string EndTime { get; set; }

        public string Particulars { get; set; }
    }
}
