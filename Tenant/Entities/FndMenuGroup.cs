﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Tenant.Entities
{
    public partial class FndMenuGroup : BaseEntity
    {
        [Key]
        public int MenuGroupId { get; set; }
        public int GroupSerial { get; set; }
        public string GroupName { get; set; }
        public int? Status { get; set; }
    }
}
