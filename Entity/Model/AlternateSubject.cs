﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class AlternateSubject : BaseEntity
    {
        [Key]
        public int AlternateSubjectId { get; set; }
        public int SubjectId { get; set; }
        public string ReferenceType { get; set; }
        public int ReferenceId { get; set; }
        public Subject Subject { get; set; }


    }
}