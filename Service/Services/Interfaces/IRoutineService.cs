﻿using System.Collections.Generic;
using Services.View_Model;

namespace Services.Interfaces
{
    public interface IRoutineService
    {
        ReturnGenerateRoutineView GenerateRoutine(GenerateRoutineView formdata);
        ReturnGenerateRoutineView SaveRoutine(GenerateRoutineView formdata, int userId);

        List<ExamClassSubjects> GenerateExamRoutine(ExamRoutineGen formdata);
        string ExamRoutineSave(ExamRoutineSave examRoutine, int userId);

        long GetRoutineAjaxCount(string sSearch, string startDate, string endDate, int eventId,
            int shiftId, int campusId, int sectionId, int classId, int subjectId);

        IEnumerable<string[]> GetRoutineAjax(string sSearch, int iDisplayStart, int iDisplayLength, string startDate, string endDate,
            int eventId, int shiftId, int campusId, int sectionId, int classId, int subjectId);
    }
}