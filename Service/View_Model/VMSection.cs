﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.View_Model
{
    public class VMSection
    {
        public int SectionId { get; set; }
        public string SectionName { get; set; }
    }
    public class VMSelectList
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
