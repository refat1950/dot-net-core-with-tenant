using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entity.Model
{
    public partial class FndUsers : BaseEntity
    {
        [Key]
        public int UserId { get; set; }

        [Required]
        [StringLength(50)]
        public string UserName { get; set; }

        [Required]
        [StringLength(10)]
        public string Pwd { get; set; }

        public int? ReferenceId { get; set; }

        [StringLength(3)]
        public string UserType { get; set; }

        [StringLength(20)]
        public string PasswdHints { get; set; }

        [StringLength(100)]
        public string SecurityQuestion { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        [StringLength(20)]
        public string Status { get; set; }

        [StringLength(100)]
        public string SecurityAnswer { get; set; }
    }
}
