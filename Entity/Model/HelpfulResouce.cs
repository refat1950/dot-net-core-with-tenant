using System;
using System.ComponentModel.DataAnnotations;

namespace Entity.Model
{
    public partial class HelpfulResouce : BaseEntity
    {
        [Key]
        public int HelpfulResourceId { get; set; }

        [StringLength(100)]
        public string Name { get; set; }

        [StringLength(100)]
        public string Url { get; set; }

        public int? ParentId { get; set; }


    }
}
