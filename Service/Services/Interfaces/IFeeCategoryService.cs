﻿using Entity.Model;
using Service.View_Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Services.Interfaces
{
    public interface IFeeCategoryService
    {
        int Add(FeeCategory model);
        List<VmFeeCategoryList> GetBetween(int start, int displayLength, string searchValue, out int totalLength);
    }
}
