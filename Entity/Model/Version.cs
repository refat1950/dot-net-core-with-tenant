﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class Version : BaseEntity
    {
        [Key]
        public int VersionId { get; set; }
        public int AppId { get; set; }
        public DateTime VersionDate { get; set; }
        public string VersionNo { get; set; }
        public string VersionName { get; set; }
        public string Segment1 { get; set; }
        public string Segment2 { get; set; }
        public string Segment3 { get; set; }
        public string Segment4 { get; set; }
    }
}
