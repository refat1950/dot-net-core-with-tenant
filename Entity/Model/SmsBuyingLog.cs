﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class SmsBuyingLog : BaseEntity
    {
        [Key]
        public int SmsBuyingLogId { get; set; }
        public string CompanyName { get; set; }
        public string CustomerName { get; set; }
        public string MobileNumber { get; set; }
        public string PhoneNumber { get; set; }
        public string EmailAddress { get; set; }
        public int? CountryId { get; set; }
        public int? DivisionOrStateId { get; set; }
        public int? DistrictId { get; set; }
        public int? ThanaOrUpazilaId { get; set; }
        public string AddressOne { get; set; }
        public string AddressTwo { get; set; }

        public string CustomerUserName { get; set; }
        public string CustomerUserPasword { get; set; }

        public decimal balance { get; set; }
        public string transactionId { get; set; }

        public string TransactionStatus { get; set; }
    }
}
