using Entity.Model;
using System;
using System.ComponentModel.DataAnnotations;

namespace Entity.Model
{
    public class ActionHistory : BaseEntity
    {
        [Key]
        public int ActionId { get; set; }

        [StringLength(10)]
        public string ObjectTypeCode { get; set; }

        [StringLength(10)]
        public string ObjectSubTypeCode { get; set; }

        [StringLength(10)]
        public string ActionCode { get; set; }

        public DateTime? ActionDate { get; set; }

        [StringLength(200)]
        public string Note { get; set; }

        public int? RequestId { get; set; }

        public int? ApprovalPathId { get; set; }

        [StringLength(10)]
        public string OfflineCode { get; set; }

    }
}
