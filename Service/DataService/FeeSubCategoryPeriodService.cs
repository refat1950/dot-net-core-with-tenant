﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using data.UnitOfWork;
using Entity.Model;
using Repository.Context;
using Repository.DefaultValues;
using Service.Helper;
using Service.ViewModel;
using Services.View_Model;

namespace Service.DataService
{
    public class FeeSubCategoryPeriodService
    {
        private IUnitOfWork _unitOfWork;
        private ProjectDbContext db;
        private DateTimeDistinctionCalculation dateTimeDistinctionCalculation;
        public FeeSubCategoryPeriodService(IUnitOfWork unitOfWork, ProjectDbContext db, DateTimeDistinctionCalculation dateTimeDistinctionCalculation)
        {
            _unitOfWork = unitOfWork;
            this.db = db;
            this.dateTimeDistinctionCalculation = dateTimeDistinctionCalculation;
        }

        #region old code
        //public SubCategoryFeePeriod GetNextPeriod(int PeriodId)
        //{
        //    var period = db.SubCategoryFeePeriods.Find(PeriodId);
        //    var allPeriodOfThisType = db.SubCategoryFeePeriods.Where(w => w.FeeSubCategoryId == period.FeeSubCategoryId).Select(s => new { s.BillPeriodId, s.StartDate, s.EndDate });

        //    var NextPeriodId = 0;
        //    var NextDate = period.EndDate.AddDays(1);

        //    foreach (var item in allPeriodOfThisType)
        //    {
        //        var startDate = item.StartDate;
        //        var EndDate = item.EndDate;
        //        // Assuming you know EndDate > startDate
        //        if (NextDate >= startDate && NextDate <= EndDate)
        //        {
        //            NextPeriodId = item.BillPeriodId;
        //            break;
        //        }
        //    }

        //    return db.SubCategoryFeePeriods.FirstOrDefault(s => s.BillPeriodId == NextPeriodId);
        //}

        //public SubCategoryFeePeriod GetPreviousPeriod(int PeriodId)
        //{
        //    var period = db.SubCategoryFeePeriods.Find(PeriodId);
        //    var allPeriodOfThisType = db.SubCategoryFeePeriods.Where(w => w.FeeSubCategoryId == period.FeeSubCategoryId).Select(s => new { s.BillPeriodId, s.StartDate, s.EndDate });

        //    var PreviousPeriodId = 0;
        //    var PreviousDate = period.StartDate.AddDays(-1);

        //    foreach (var item in allPeriodOfThisType)
        //    {
        //        var startDate = item.StartDate;
        //        var EndDate = item.EndDate;
        //        // Assuming you know EndDate > startDate
        //        if (PreviousDate >= startDate && PreviousDate <= EndDate)
        //        {
        //            PreviousPeriodId = item.BillPeriodId;
        //            break;
        //        }
        //    }

        //    return db.SubCategoryFeePeriods.FirstOrDefault(s => s.BillPeriodId == PreviousPeriodId);
        //}

        //public SubCategoryFeePeriod GetFirstPeriod(int FeeSubCategoryId)
        //{
        //    var GetPeriods = db.SubCategoryFeePeriods.Where(s => s.FeeSubCategoryId == FeeSubCategoryId).Select(s => new { s.BillPeriodId, s.StartDate, s.EndDate }).ToList();
        //    var CuurentDate = DateTime.Now.Date;
        //    var CuurentPeriodId = 0;
        //    var FirstPeriodId = 0;

        //    if (GetPeriods.Count > 0) //if cateogry exists
        //    {
        //        foreach (var item in GetPeriods)
        //        {
        //            var startDate = item.StartDate;
        //            var EndDate = item.EndDate;
        //            // Assuming you know EndDate > startDate
        //            if (CuurentDate >= startDate && CuurentDate <= EndDate)
        //            {
        //                CuurentPeriodId = item.BillPeriodId;
        //                break;
        //            }
        //        }
        //        var CuurentPeriod = db.SubCategoryFeePeriods.FirstOrDefault(s => s.BillPeriodId == CuurentPeriodId);
        //        if (CuurentPeriod != null)
        //        {
        //            DateTime PreviousDate = CuurentPeriod.StartDate.AddDays(-1);

        //            for (int i = 0; i < GetPeriods.Count; i++)
        //            {
        //                foreach (var items in GetPeriods)
        //                {

        //                    var startDate = items.StartDate;
        //                    var EndDate = items.EndDate;
        //                    // Assuming you know EndDate > startDate
        //                    if (PreviousDate >= startDate && PreviousDate <= EndDate)
        //                    {
        //                        FirstPeriodId = items.BillPeriodId;
        //                        PreviousDate = startDate.AddDays(-1);
        //                    }
        //                }
        //            }

        //            if (FirstPeriodId == 0) //if its happens one time in a year, its the 1st and last period
        //            {
        //                FirstPeriodId = CuurentPeriodId;
        //            }
        //        }
        //        else
        //        {
        //            FirstPeriodId = 0;
        //        }

        //    }
        //    else
        //    {
        //        FirstPeriodId = 0;
        //    }

        //    return db.SubCategoryFeePeriods.FirstOrDefault(s => s.BillPeriodId == FirstPeriodId);

        //}

        //public SubCategoryFeePeriod GetLastPeriod(int FeeSubCategoryId)
        //{

        //    var GetPeriods = db.SubCategoryFeePeriods.Where(s => s.FeeSubCategoryId == FeeSubCategoryId).ToList();

        //    var CuurentDate = DateTime.Now.Date;
        //    var CuurentPeriodId = 0;
        //    var LastPeriodId = 0;

        //    if (GetPeriods.Count > 0) //if cateogry exists
        //    {

        //        foreach (var item in GetPeriods)
        //        {
        //            var startDate = item.StartDate;
        //            var EndDate = item.EndDate;
        //            // Assuming you know EndDate > startDate
        //            if (CuurentDate >= startDate && CuurentDate <= EndDate)
        //            {
        //                CuurentPeriodId = item.BillPeriodId;
        //                break;
        //            }
        //        }

        //        var CuurentPeriod = db.SubCategoryFeePeriods.FirstOrDefault(s => s.BillPeriodId == CuurentPeriodId);

        //        if (CuurentPeriod != null)
        //        {
        //            DateTime NextDate = CuurentPeriod.EndDate.AddDays(1);

        //            for (int i = 0; i < GetPeriods.Count; i++)
        //            {
        //                foreach (var items in GetPeriods)
        //                {

        //                    var startDate = items.StartDate;
        //                    var EndDate = items.EndDate;
        //                    // Assuming you know EndDate > startDate
        //                    if (NextDate >= startDate && NextDate <= EndDate)
        //                    {
        //                        LastPeriodId = items.BillPeriodId;
        //                        NextDate = EndDate.AddDays(1);
        //                    }
        //                }
        //            }

        //            if (LastPeriodId == 0) //if its happens one time in a year, its the 1st and last period
        //            {
        //                LastPeriodId = CuurentPeriodId;
        //            }
        //        }
        //        else
        //        {
        //            LastPeriodId = 0;
        //        }
        //    }
        //    else
        //    {
        //        LastPeriodId = 0;
        //    }

        //    return db.SubCategoryFeePeriods.FirstOrDefault(s => s.BillPeriodId == LastPeriodId);
        //}

        //public int GetPeriodDistance(int fromPeriodId, int toPeriodId)
        //{
        //    var fromStartDate = db.SubCategoryFeePeriods.FirstOrDefault(s => s.BillPeriodId == fromPeriodId).StartDate;
        //    var toStartDate = db.SubCategoryFeePeriods.FirstOrDefault(s => s.BillPeriodId == toPeriodId).StartDate;

        //    if (fromStartDate != null || toStartDate != null)
        //    {
        //        if (fromPeriodId == toPeriodId)
        //        {
        //            var cnt = 0; //same period
        //            return cnt;
        //        }
        //        else if (fromStartDate > toStartDate)
        //        {
        //            var cnt = 0;
        //            var toPeriod = GetNextPeriod(toPeriodId).BillPeriodId;
        //            while (true)
        //            {
        //                if (toPeriod == fromPeriodId)
        //                {
        //                    break;
        //                }
        //                else
        //                {
        //                    toPeriod = GetNextPeriod(toPeriod).BillPeriodId;
        //                    cnt--;
        //                }
        //            }
        //            cnt--;
        //            return cnt;
        //        }
        //        else
        //        {
        //            var cnt = 1;
        //            var fromPeriod = GetNextPeriod(fromPeriodId).BillPeriodId;
        //            while (true)
        //            {
        //                if (fromPeriod == toPeriodId)
        //                {
        //                    break;
        //                }
        //                else
        //                {
        //                    fromPeriod = GetNextPeriod(fromPeriod).BillPeriodId;
        //                    cnt++;
        //                }
        //            }
        //            return cnt + 1;
        //        }

        //    }
        //    else
        //    {
        //        return 0;
        //    }
        //}

        //public SubCategoryFeePeriod GetBigestPeriodByPeriodIds(List<int?> PeriodIds, int FeeSubCategoryId)
        //{
        //    //var BiggestPeriodId = 0;
        //    // DateTime BiggestDate = DateTime.Now;
        //    //var AssumingBigDate = PeriodIds[0];
        //    return db.SubCategoryFeePeriods.Where(w => PeriodIds.Contains(w.BillPeriodId)).OrderByDescending(w => w.EndDate).FirstOrDefault();

        //    //DateTime BiggestDate = db.SubCategoryFeePeriods.FirstOrDefault(s => s.BillPeriodId == AssumingBigDate).EndDate;
        //    //foreach (var j in PeriodIds)
        //    //{
        //    //    var endDate_j = db.SubCategoryFeePeriods.FirstOrDefault(s => s.BillPeriodId == j);
        //    //    if (BiggestDate < endDate_j.EndDate)
        //    //    {
        //    //        BiggestDate = endDate_j.EndDate;
        //    //    }
        //    //}

        //    //var GetPeriods = db.SubCategoryFeePeriods.Where(s => s.FeeSubCategoryId == FeeSubCategoryId).ToList();
        //    //foreach (var item in GetPeriods)
        //    //{
        //    //    var startDate = item.StartDate;
        //    //    var EndDate = item.EndDate;
        //    //    // Assuming you know EndDate > startDate
        //    //    if (BiggestDate.Ticks >= startDate.Ticks && BiggestDate.Ticks <= EndDate.Ticks)
        //    //    {
        //    //        BiggestPeriodId = item.BillPeriodId;
        //    //    }
        //    //}


        //    //return db.SubCategoryFeePeriods.FirstOrDefault(s => s.BillPeriodId == BiggestPeriodId);
        //}

        //public int GetTotalAvailablePeriods(SubCategoryFeePeriod Period)
        //{
        //    var TotalAvailablePeriods = 0;
        //    var FindNextPeriod = GetNextPeriod(Period.BillPeriodId);
        //    while (true)
        //    {

        //        if (FindNextPeriod != null)
        //        {
        //            TotalAvailablePeriods = TotalAvailablePeriods + 1;
        //            FindNextPeriod = GetNextPeriod(FindNextPeriod.BillPeriodId);
        //        }
        //        else
        //        {
        //            break;
        //        }
        //    }

        //    return TotalAvailablePeriods;
        //}

        //public VmBillInformation GetDueWithWaibers(decimal MaximumWaiberPercentage, DateTime DueDate, FeeSubCategoryFine feeSubCategoryFine, decimal feeSubCategoryAmount)
        //{
        //    VmBillInformation vmBillInformation = new VmBillInformation();
        //    decimal Bill_Amount = 0;
        //    decimal student_due_amount = 0;
        //    var currentDate = DateTime.Now;
        //    //check if fine in amount or percentage
        //    //if fine in percentage, get the amount in decmal
        //    if (feeSubCategoryFine.IsPercentage == true)
        //    {
        //        student_due_amount = (feeSubCategoryFine.Amount / 100) * feeSubCategoryAmount;
        //    }
        //    //if fine in amount
        //    else if (feeSubCategoryFine.IsPercentage == false)
        //    {
        //        student_due_amount = feeSubCategoryFine.Amount;
        //    }

        //    // checking if the fine type is fixed or incremental
        //    //if fine type is fixed, adding due with main fee
        //    if (feeSubCategoryFine.IsIncremental == false)
        //    {
        //        //add weaber with main amount
        //        var AfterWaiberAmount = (feeSubCategoryAmount / 100) * MaximumWaiberPercentage;
        //        Bill_Amount = (feeSubCategoryAmount - AfterWaiberAmount) + student_due_amount;

        //        vmBillInformation.BillAmount = feeSubCategoryAmount;
        //        vmBillInformation.WaiberPercentage = Convert.ToString(MaximumWaiberPercentage) + "%";
        //        vmBillInformation.WaiberAmount = AfterWaiberAmount;
        //        vmBillInformation.LateFee = student_due_amount;
        //        vmBillInformation.ActualAmount = Bill_Amount;
        //    }
        //    //if fine type is incremental
        //    else if (feeSubCategoryFine.IsIncremental == true)
        //    {
        //        string currentMonth = DateTime.Now.Month.ToString();
        //        string currentYear = DateTime.Now.Year.ToString();

        //        var TotalDayInThisMonth = DateTime.DaysInMonth(Convert.ToInt16(currentYear), Convert.ToInt16(currentMonth));
        //        var DayDistance = (currentDate - DueDate).TotalDays;
        //        vmBillInformation.DateDistance = dateTimeDistinctionCalculation.CalculateDistinctionOfTwoDateTime(currentDate, DueDate).Day;

        //        //if increment type is monthly
        //        if (feeSubCategoryFine.IncrementType == (int)IncrementType.Monthly)
        //        {
        //            // get due with number of month late
        //            var TotalMonthPayable = DayDistance / TotalDayInThisMonth;
        //            TotalMonthPayable = Math.Ceiling(TotalMonthPayable);

        //            //add weaber with main amount
        //            var AfterWaiberAmount = (feeSubCategoryAmount / 100) * MaximumWaiberPercentage;
        //            Bill_Amount = (feeSubCategoryAmount - AfterWaiberAmount) + (student_due_amount * Convert.ToInt16(TotalMonthPayable));

        //            vmBillInformation.BillAmount = feeSubCategoryAmount;
        //            vmBillInformation.WaiberPercentage = Convert.ToString(MaximumWaiberPercentage) + "%";
        //            vmBillInformation.WaiberAmount = AfterWaiberAmount;
        //            vmBillInformation.LateFee = student_due_amount * Convert.ToInt16(TotalMonthPayable);
        //            vmBillInformation.ActualAmount = Bill_Amount;
        //        }
        //        else if (feeSubCategoryFine.IncrementType == (int)IncrementType.Daily)
        //        {
        //            // get due with number of month late
        //            var TotalDayPayable = DayDistance / feeSubCategoryFine.DaysForIncrement;
        //            TotalDayPayable = Math.Ceiling(TotalDayPayable ?? 0.0);

        //            //add weaber with main amount
        //            var AfterWaiberAmount = (feeSubCategoryAmount / 100) * MaximumWaiberPercentage;

        //            Bill_Amount = (feeSubCategoryAmount - AfterWaiberAmount) + (student_due_amount * Convert.ToInt16(TotalDayPayable));

        //            if (Bill_Amount > feeSubCategoryFine.MaximumIncrementAmount)
        //            {
        //                Bill_Amount = feeSubCategoryFine.MaximumIncrementAmount ?? 0;
        //            }

        //            vmBillInformation.BillAmount = feeSubCategoryAmount;
        //            vmBillInformation.WaiberPercentage = Convert.ToString(MaximumWaiberPercentage) + "%";
        //            vmBillInformation.WaiberAmount = AfterWaiberAmount;
        //            vmBillInformation.LateFee = student_due_amount * Convert.ToInt16(TotalDayPayable);
        //            vmBillInformation.ActualAmount = Bill_Amount;
        //        }
        //    }

        //    return vmBillInformation;
        //}

        //public VmBillInformation GetDueWithoutWaibers(DateTime DueDate, FeeSubCategoryFine feeSubCategoryFine, decimal feeSubCategoryAmount)
        //{

        //    VmBillInformation vmBillInformation = new VmBillInformation();
        //    decimal Bill_Amount = 0;
        //    decimal student_due_amount = 0;
        //    var currentDate = DateTime.Now;
        //    //check if fine in amount or percentage
        //    //if fine in percentage, get the amount in decmal
        //    if (feeSubCategoryFine.IsPercentage == true)
        //    {
        //        student_due_amount = (feeSubCategoryFine.Amount / 100) * feeSubCategoryAmount;
        //    }
        //    //if fine in amount
        //    else if (feeSubCategoryFine.IsPercentage == false)
        //    {
        //        student_due_amount = feeSubCategoryFine.Amount;
        //    }

        //    // checking if the fine type is fixed or incremental
        //    //if fine type is fixed, adding due with main fee
        //    if (feeSubCategoryFine.IsIncremental == false)
        //    {
        //        Bill_Amount = feeSubCategoryAmount + student_due_amount;

        //        vmBillInformation.BillAmount = feeSubCategoryAmount;
        //        vmBillInformation.LateFee = student_due_amount;
        //        vmBillInformation.ActualAmount = Bill_Amount;
        //    }
        //    //if fine type is incremental
        //    else if (feeSubCategoryFine.IsIncremental == true)
        //    {
        //        string currentMonth = DateTime.Now.Month.ToString();
        //        string currentYear = DateTime.Now.Year.ToString();

        //        var TotalDayInThisMonth = DateTime.DaysInMonth(Convert.ToInt16(currentYear), Convert.ToInt16(currentMonth));
        //        var DayDistance = (currentDate - DueDate).TotalDays;
        //        vmBillInformation.DateDistance = dateTimeDistinctionCalculation.CalculateDistinctionOfTwoDateTime(currentDate, DueDate).Day;

        //        //if increment type is monthly
        //        if (feeSubCategoryFine.IncrementType == (int)IncrementType.Monthly)
        //        {
        //            // get due with number of month late
        //            var TotalMonthPayable = DayDistance / TotalDayInThisMonth;
        //            TotalMonthPayable = Math.Ceiling(TotalMonthPayable);

        //            Bill_Amount = feeSubCategoryAmount + (student_due_amount * Convert.ToInt16(TotalMonthPayable));

        //            vmBillInformation.BillAmount = feeSubCategoryAmount;
        //            vmBillInformation.LateFee = student_due_amount * Convert.ToInt16(TotalMonthPayable);
        //            vmBillInformation.ActualAmount = Bill_Amount;
        //        }
        //        else if (feeSubCategoryFine.IncrementType == (int)IncrementType.Daily)
        //        {
        //            // get due with number of month late
        //            var TotalDayPayable = DayDistance / feeSubCategoryFine.DaysForIncrement;
        //            TotalDayPayable = Math.Ceiling(TotalDayPayable ?? 0.0);

        //            Bill_Amount = feeSubCategoryAmount + (student_due_amount * Convert.ToInt16(TotalDayPayable));

        //            if (Bill_Amount > feeSubCategoryFine.MaximumIncrementAmount)
        //            {
        //                Bill_Amount = feeSubCategoryFine.MaximumIncrementAmount ?? 0;
        //            }

        //            vmBillInformation.BillAmount = feeSubCategoryAmount;
        //            vmBillInformation.LateFee = student_due_amount * Convert.ToInt16(TotalDayPayable);
        //            vmBillInformation.ActualAmount = Bill_Amount;

        //        }
        //    }

        //    return vmBillInformation;
        //}
        #endregion

        public SubCategoryFeePeriod GetCurrentPeriod(int FeeSubCategoryId)
        {
            var CurrentDate = DateTime.Now.Date;
            var CurrentPeriod = db.SubCategoryFeePeriods.Where(s => s.FeeSubCategoryId == FeeSubCategoryId && CurrentDate.Month >= s.StartDate.Month && CurrentDate.Month <= s.EndDate.Month).FirstOrDefault();
            return CurrentPeriod;
        }

        public Year GetCurrentYear()
        {
            var CurrentDate = DateTime.Now.Date;
            var year = db.Years.FirstOrDefault(f => f.YearName == CurrentDate.Year);
            return year;
        }

        public SubCategoryFeePeriod GetPeriodById(int PeriodId)
        {
            return db.SubCategoryFeePeriods.FirstOrDefault(s => s.BillPeriodId == PeriodId);
        }

        
        public SubCategoryYear GetNextPeriodByPeriodIdAndYearId(int periodId, int yearId)
        {
            var period = db.SubCategoryFeePeriods.Find(periodId);
            var periodMappers = db.SubCategoryYears.Where(w => w.SubCategoryFeePeriod.FeeSubCategoryId == period.FeeSubCategoryId)
            .OrderBy(o => new { o.SubCategoryFeePeriod.FeeSubCategoryId,  o.YearId,o.BillPeriodId }).ToList()
            .Select((s, index) => new
            {
                Id = index+1,
                BillPeriodId = s.BillPeriodId,
                YearId = s.YearId
            }).ToList();
            var currentPeriodId = periodMappers.FirstOrDefault(f=>f.BillPeriodId == periodId && f.YearId == yearId);
            if (currentPeriodId != null)
            {
                var nextPeriod = periodMappers.Where(w => w.Id > currentPeriodId.Id).FirstOrDefault();
                if (nextPeriod != null)
                {
                    return db.SubCategoryYears.Where(w => w.YearId == nextPeriod.YearId && w.BillPeriodId == nextPeriod.BillPeriodId).FirstOrDefault();
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }

        }

        public SubCategoryYear GetPreviousPeriodByPeriodIdAndYearId(int periodId, int yearId)
        {
            var period = db.SubCategoryFeePeriods.Find(periodId);
            var periodMappers = db.SubCategoryYears.Where(w => w.SubCategoryFeePeriod.FeeSubCategoryId == period.FeeSubCategoryId)
            .OrderBy(o => new { o.SubCategoryFeePeriod.FeeSubCategoryId, o.YearId, o.BillPeriodId }).ToList()
            .Select((s, index) => new
            {
                Id = index + 1,
                BillPeriodId = s.BillPeriodId,
                YearId = s.YearId
            }).ToList();
            var currentPeriodId = periodMappers.FirstOrDefault(f => f.BillPeriodId == periodId && f.YearId == yearId);
            if (currentPeriodId != null)
            {
                var prevPeriod = periodMappers.Where(w => w.Id < currentPeriodId.Id).LastOrDefault();
                if (prevPeriod != null)
                {
                    return db.SubCategoryYears.Where(w => w.YearId == prevPeriod.YearId && w.BillPeriodId == prevPeriod.BillPeriodId).FirstOrDefault();
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }

        }

        public bool IsFirstPeriodGreaterthanCurrent(int firstPeriodId,int firstYearId, int currentPeriodId, int subCategoryId)
        {
            var isGreater = true;
            var periodMappers = db.SubCategoryYears.Where(w => w.SubCategoryFeePeriod.FeeSubCategoryId == subCategoryId)
            .OrderBy(o => new { o.YearId, o.BillPeriodId }).ToList()
            .Select((s, index) => new
            {
                Id = index + 1,
                BillPeriodId = s.BillPeriodId,
                YearId = s.YearId
            }).ToList();
            var currentYear = GetCurrentYear();
            var firstPeriod = periodMappers.FirstOrDefault(f => f.BillPeriodId == firstPeriodId && f.YearId == firstYearId);

            if (currentYear != null)
            {
                var currentPeriod = periodMappers.FirstOrDefault(f => f.BillPeriodId == currentPeriodId && f.YearId == currentYear.YearId);
                isGreater = firstPeriod.Id > currentPeriod.Id;
                return isGreater;
            }
            else{
                return false;
            }
            
        }

        public SubCategoryYear GetFirstPeriodOfYearByDateAndSubCategory(DateTime date, int subCategoryId)
        {
            var periodMappers = db.SubCategoryYears.Where(w => w.SubCategoryFeePeriod.FeeSubCategoryId == subCategoryId)
           .OrderBy(o => new { o.YearId, o.BillPeriodId }).ToList();
            return periodMappers.Where(f => f.Year.YearName == date.Year).FirstOrDefault();
        }

        public decimal GetFineAmount(DateTime DueDate, FeeSubCategoryFine feeSubCategoryFine, decimal feeSubCategoryAmount)
        {
            decimal fineAmount = 0;
            var currentDate = DateTime.Now.Date;

            if (currentDate > DueDate)
            {
                //check if fine in amount or percentage
                //if fine in percentage, get the amount in decmal
                if (feeSubCategoryFine.IsPercentage == true)
                {
                    fineAmount = (feeSubCategoryFine.Amount / 100) * feeSubCategoryAmount;
                }
                //if fine in amount
                else if (feeSubCategoryFine.IsPercentage == false)
                {
                    fineAmount = feeSubCategoryFine.Amount;
                }
                // no need to check fixed.....

                if (feeSubCategoryFine.IsIncremental == true)
                {
                    string currentMonth = DateTime.Now.Month.ToString();
                    string currentYear = DateTime.Now.Year.ToString();

                    var TotalDayInThisMonth = DateTime.DaysInMonth(Convert.ToInt16(currentYear), Convert.ToInt16(currentMonth));
                    var DayDistance = (currentDate - DueDate).TotalDays;

                    //if increment type is monthly
                    if (feeSubCategoryFine.IncrementType == (int)IncrementType.Monthly)
                    {
                        // get due with number of month late
                        var TotalMonthPayable = Math.Abs(DayDistance) / TotalDayInThisMonth;
                        TotalMonthPayable = Math.Ceiling(TotalMonthPayable);

                        fineAmount = fineAmount * Convert.ToInt16(TotalMonthPayable);
                        //vmBillInformation.ActualAmount = Bill_Amount;
                    }
                    else if (feeSubCategoryFine.IncrementType == (int)IncrementType.Daily)
                    {
                        // get due with number of month late
                        var TotalDayPayable = Math.Abs(DayDistance) / feeSubCategoryFine.DaysForIncrement;
                        TotalDayPayable = Math.Ceiling(TotalDayPayable ?? 0.0);
                        fineAmount = fineAmount * Convert.ToInt16(TotalDayPayable);
                    }
                }
            }
            else{
                fineAmount = 0;
            }
            return fineAmount;
        }

        public string GetFormatedPeriod(int PeriodId, int YearName)
        {
            var subCatPeriod = GetPeriodById(PeriodId);
            String[] tag = subCatPeriod.PeriodName.Split('-');
            var PeriodName = tag[1] + " " + tag[0] + " " + YearName.ToString().Substring(2, 2) + " to " + tag[3] + " " + tag[2] + " " + YearName.ToString().Substring(2, 2);
            return PeriodName;
        }

        public VmWaiverApplicable WaiverApplicable(int SubCateGoryId, int StudentHeaderId,DateTime startDate, DateTime endDate)
        {
            var waiversession = db.Session.FirstOrDefault(s => (startDate >= s.StartDate && startDate <= s.EndDate) && (endDate >= s.StartDate && endDate <= s.EndDate));
            if (waiversession !=null)
            {
                var waiverSession = db.Session.FirstOrDefault(s => s.SessionId == waiversession.SessionId);
                var waiverSesssionStartDate = waiverSession.StartDate.Value.Date;
                var waiverSesssionEndDate = waiverSession.EndDate.Value.Date;

                List<int> StudentCategoryIds = new List<int>();
                var StudentCategory = db.StudentCategories.Where(w => w.StudentHeaderId == StudentHeaderId).ToList();

                foreach (var item in StudentCategory)
                {
                    var FromSessionstartDate = db.Session.FirstOrDefault(s => s.SessionId == item.FromSessionId).StartDate.Value.Date;
                    var FromSessionendDate = db.Session.FirstOrDefault(s => s.SessionId == item.FromSessionId).EndDate.Value.Date;

                    var ToSessionstartDate = db.Session.FirstOrDefault(s => s.SessionId == item.ToSessionId).StartDate.Value.Date;
                    var ToSessionIdendDate = db.Session.FirstOrDefault(s => s.SessionId == item.ToSessionId).EndDate.Value.Date;

                    if ((waiverSesssionStartDate >= FromSessionstartDate && waiverSesssionEndDate >= FromSessionendDate) && (waiverSesssionStartDate <= ToSessionstartDate && waiverSesssionEndDate <= ToSessionIdendDate))
                    {
                        StudentCategoryIds.Add(item.StudentCategoryId);
                    }
                }

                //check if any waibers exists for this sub-category
                var feeSubCategoryWaiver = db.FeeSubCategoryWaiver.Where(s => s.FeeSubCategoryId == SubCateGoryId).ToList();
                var WaiversExists = feeSubCategoryWaiver.Where(s => StudentCategoryIds.Contains(s.Category_GenderID)).ToList();

                //waiver calculation
                bool waiverappicable = false;
                decimal MaximumWaiberPercentage = 0;
                FeeSubCategoryWaiver maxWaiver = new FeeSubCategoryWaiver();
                var maximumWeaversCategory = new StudentCategory();
                if (WaiversExists.Any())
                {
                    // find maximum Weaber
                    maxWaiver = WaiversExists.OrderByDescending(o => o.Percentage).FirstOrDefault();
                    MaximumWaiberPercentage = maxWaiver.Percentage;
                    int MaximumPercentageWeaberId = maxWaiver.FeeSubCategoryWaiverId;

                    maximumWeaversCategory = db.StudentCategories.FirstOrDefault(s => s.StudentHeaderId == StudentHeaderId && s.StudentCategoryId == maxWaiver.Category_GenderID);

                    //if effective date not ends and waiber aplicable
                    if (MaximumWaiberPercentage != 0 && MaximumWaiberPercentage < 100)
                    {
                        waiverappicable = true;
                    }
                }
                // end of waiver calculation

                if (startDate.Date >= waiverSesssionStartDate && endDate.Date <= waiverSesssionEndDate)
                {
                    //if effective to not defined
                    if (maxWaiver != null && maximumWeaversCategory.EffectiveTo == null)
                    {
                        waiverappicable = true;
                    }
                    //if effective to defined
                    else if (maxWaiver != null && maximumWeaversCategory.EffectiveTo != null)
                    {
                        //if effective to ends after start end date or effective to is between start end date
                        if ((maximumWeaversCategory.EffectiveTo.Value.Date >= startDate.Date && maximumWeaversCategory.EffectiveTo.Value.Date >= endDate.Date) || (maximumWeaversCategory.EffectiveTo.Value.Date >= startDate.Date && maximumWeaversCategory.EffectiveTo.Value.Date <= endDate.Date))
                        {
                            waiverappicable = true;
                        }
                        //if effective to ends before start end date
                        else if (maximumWeaversCategory.EffectiveTo.Value.Date <= startDate.Date && maximumWeaversCategory.EffectiveTo.Value.Date <= endDate.Date)
                        {
                            waiverappicable = false;
                        }
                        // effective to not between and after for start end date
                        else
                        {
                            waiverappicable = false;
                        }
                    }
                    else
                    {
                        waiverappicable = false;
                    }
                }
                else
                {
                    waiverappicable = false;
                }

                VmWaiverApplicable data = new VmWaiverApplicable();
                data.waiverappicable = waiverappicable;
                data.MaximumWaiberPercentage = MaximumWaiberPercentage;
                return data;
            }

            VmWaiverApplicable vmWaiverApplicable = new VmWaiverApplicable();
            vmWaiverApplicable.waiverappicable = false;
            vmWaiverApplicable.MaximumWaiberPercentage = 0;
            return vmWaiverApplicable;
        }

        public class VmWaiverApplicable
        {
            public bool waiverappicable { get; set; }
            public decimal MaximumWaiberPercentage { get; set; }
        }
    }
}
