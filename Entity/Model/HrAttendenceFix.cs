﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class HrAttendenceFix : BaseEntity
    {
        [Key]
        public int FixId { get; set; }

        public int EmpHeaderId { get; set; }
        public DateTime PunchDate { get; set; }

        [Required]
        [StringLength(50)]
        public string PunchTime { get; set; }

        [Required]
        [StringLength(50)]
        public string OutTime { get; set; }

        public int DoorNumber { get; set; }

        public int? GateNumber { get; set; }

        [Required]
        [StringLength(50)]
        public string CardNumber { get; set; }

        public int Status { get; set; }

        public int? ApprovedBy { get; set; }

        public int? RequestBy { get; set; }

        public bool? IsProcessed { get; set; }
    }
}
