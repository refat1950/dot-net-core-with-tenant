﻿using Entity.Model.Process;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class MailGroup
    {
        [Key]
        public int MailGroupTableId { get; set; }
        public int EmpHeaderId { get; set; }
        public int MailGroupId { get; set; }

        public virtual HrEmployee HrEmployee { get; set; }
    }
}
