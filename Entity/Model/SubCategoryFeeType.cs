﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Entity.Model
{
    public class SubCategoryFeeType : BaseEntity
    {
        [Key]
        public int SubCategoryFeeTypeId { get; set; }
        public int FeeSubCategoryId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime DueDate { get; set; }
        public virtual FeeSubCategory FeeSubCategory { get; set; }
    }


}
