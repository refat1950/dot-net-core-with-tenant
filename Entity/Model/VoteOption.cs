using System;
using System.ComponentModel.DataAnnotations;

namespace Entity.Model
{
    public partial class VoteOption : BaseEntity
    {
        [Key]
        public int VoteOptionId { get; set; }

        [StringLength(100)]
        public string VoteOptionDesc { get; set; }

        public int? VoteId { get; set; }

        public int? Status { get; set; }


    }
}
