using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entity.Model
{
    public partial class GeneralInfoType : BaseEntity
    {
        public GeneralInfoType()
        {
            GeneralInfo = new HashSet<GeneralInfo>();
        }

        [Key]
        public int InfoTypeId { get; set; }

        [Required]
        [StringLength(100)]
        public string InfoType { get; set; }

        public virtual ICollection<GeneralInfo> GeneralInfo { get; set; }
    }
}
