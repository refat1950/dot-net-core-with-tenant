using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entity.Model
{
    public partial class JobApply : BaseEntity
    {
        [Key]
        public int JobApplyId { get; set; }

        public DateTime ApplyDate { get; set; }

        public int BssJobId { get; set; }

        [ForeignKey("JobApplicant")]
        public long JobApplicantId { get; set; }

        public virtual JobApplicant JobApplicant { get; set; }
        public virtual Job Job { get; set; }

    }
}
