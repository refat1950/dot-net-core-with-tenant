using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entity.Model
{
    public partial class ClassCourseSubject : BaseEntity
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CourseMasterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int SubjectId { get; set; }

    }
}
