﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entity.Model
{
    public class HrExcepShift : BaseEntity
    {
        public int EmpHeaderId { get; set; }
        public DateTime ShiftDate { get; set; }

        /// <summary>
        /// From Flex
        /// </summary>
        public int ExcepType { get; set; }

        [StringLength(50)]
        public string ShiftStartTime { get; set; }

        [StringLength(50)]
        public string ShiftEndTime { get; set; }

        [StringLength(200)]
        public string Remarks { get; set; }

        public HrEmployee HrEmployee { get; set; }
    }
}