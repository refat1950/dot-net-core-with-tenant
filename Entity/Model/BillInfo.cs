using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entity.Model
{
    public partial class BillInfo : BaseEntity
    {
        [Key]
        public int BillInfoId { get; set; }

        public int? SchoolId { get; set; }

        public int? CampusId { get; set; }

        public int SessionId { get; set; }

        //[ForeignKey("FndFlexValue")]
        public int FeeSubCategoryId { get; set; }

        //public int? FlexValueId { get; set; }

        public int ClassId { get; set; }

        //[Column(TypeName = "money")]
        public decimal? BillAtm { get; set; }

        [DisplayFormat(DataFormatString = "{0:n2}")]
        public decimal? MaxDiscPct { get; set; }


        public virtual School School { get; set; }
        public virtual Class Class { get; set; }
        public virtual Session Session { get; set; }
        //public virtual FndFlexValue FndFlexValue { get; set; }
    }
}
