using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entity.Model
{
    [Table("SchoolCalender")]
    public partial class SchoolCalender : BaseEntity
    {
        [Key]
        public int CalenderId { get; set; }

        public DateTime CalDate { get; set; }

        public int? EventId { get; set; }

        [StringLength(100)]
        public string EventName { get; set; }


    }
}
