﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Entity.Model;
using System.Text;
using System.Threading.Tasks;
using Services.Interfaces;
using Services;
using Repository.Context;
using data.UnitOfWork;

namespace Web.GlobalService
{
   public class GlobalService
    {
        private IUnitOfWork _unitOfWork;
       

        public GlobalService( IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public int GetCurrentSessionId()
        {
            var sessionId = _unitOfWork.SessionRepo.FirstOrDefault(p => p.Status == true).SessionId;
            return sessionId;
        }
    }
}
