﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Tenant.Context;

namespace Tenant.Provider
{
    public class DatabaseTenantProvider : ITenantProvider
    {
        private int tenantId = 0;
        private string connectionStringName;
        private string connectionString;
        private IConfiguration configuration;
        public DatabaseTenantProvider(TenantContext context, IHttpContextAccessor accessor, IConfiguration configuration)
        {
            this.configuration = configuration;
            var host = accessor.HttpContext.Request.Host.Host; //.Url.Host;
            connectionString = context.TenantInfo.FirstOrDefault(t => t.Url == host) != null ? context.TenantInfo.FirstOrDefault(t => t.Url == host).ConnectionString : "";
            connectionStringName = context.TenantInfo.FirstOrDefault(t => t.Url == host) != null ? context.TenantInfo.FirstOrDefault(t => t.Url == host).ConnectionStringName : "";
            tenantId = context.TenantInfo.FirstOrDefault(t => t.Url == host) != null ? context.TenantInfo.FirstOrDefault(t => t.Url == host).TenantId : 0;
        }
        public int GetTenantId()
        {
            return tenantId;
        }
        public string GetConnectionStringName()
        {
            return connectionStringName;
        }
        public string GetConnectionString()
        {
            return connectionString;
        }
    }

}
