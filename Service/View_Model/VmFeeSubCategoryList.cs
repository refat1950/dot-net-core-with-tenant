﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.View_Model
{
    public class VmFeeSubCategoryList
    {
        public int FeeSubCategoryId { get; set; }
        public string FeeSubCategoryName { get; set; }
        public int FeeCategoryId { get; set; }
        public string FeeCategoryName { get; set; }
        public int SessionId { get; set; }
        public string SessionName { get; set; }
        public decimal Amount { get; set; }
        public int FeeTypeId { get; set; }
        public string FeeTypeName { get; set; }
    }
}
