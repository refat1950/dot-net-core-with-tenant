﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class RoomSetup : BaseEntity
    {
        [Key]
        public int RoomSetupHeaderId { get; set; }
        public int ClassRoomHeaderId { get; set; }
        public int? NoofColumn { get; set; }
        public SetupFor SetupFor { get; set; }
        public int? SessionId { get; set; }
        public int? ClassId { get; set; }
        public int? SectionId { get; set; }
        public int? ExamId { get; set; }
        public int? ExamTypeId { get; set; }
        public int? SubjectId { get; set; }
        public bool? DefaultLayout { get; set; }
        public virtual List<ClassRoomColumn> ClassRoomColumns { get; set; }
    }
    public enum SetupFor
    {
        ClassRoom = 1,
        Exam = 2,
        Conference = 3
    }
}
