﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class HrEmployeeLeaveApplication : BaseEntity
    {
        public HrEmployeeLeaveApplication()
        {
            LeaveApplicationFiles = new List<LeaveApplicationFile>();
        }
        [Key]
        public int ApplicationId { get; set; }
        public int EmpHeaderId { get; set; }
        public int LeaveCategoryId { get; set; }
        public DateTime LeaveStartDate { get; set; }
        public DateTime LeaveEndDate { get; set; }
        public DateTime LeaveApplyDate { get; set; }
        public string Subject { get; set; }
        public string Comment { get; set; }
        public LeaveStatus LeaveStatus { get; set; }
        public bool? IsOnDuty { get; set; }
        public string Remarks { get; set; }
        public string Attachment1 { get; set; }
        public string Attachment2 { get; set; }
        public string Attachment3 { get; set; }
        public virtual ICollection<LeaveApplicationFile> LeaveApplicationFiles { get; set; }

    }
    public class LeaveApplicationFile : BaseEntity
    {
        [Key]
        public int FileId { get; set; }
        public string FileName { get; set; }
        public string FileUrl { get; set; }
        public int ApplicationId { get; set; }
        public virtual HrEmployeeLeaveApplication HrEmployeeLeaveApplication { get; set; }
    }
    public enum LeaveStatus
    {
        Pending = 0,
        Approved = 1,
        Declined = 2
    }
}
