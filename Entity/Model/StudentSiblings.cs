using System;
using System.ComponentModel.DataAnnotations;

namespace Entity.Model
{
    public partial class StudentSiblings : BaseEntity
    {
        [Key]
        public int SiblingsId { get; set; }

        public int StudentHeaderId { get; set; }

        public int? GurdianId { get; set; }

        public int? SiblingsStudentId { get; set; }

        [StringLength(100)]
        public string SiblingsName { get; set; }

        [StringLength(10)]
        public string SiblingsGender { get; set; }

        public DateTime? SiblingsDob { get; set; }

        public int? SiblingsSchoolId { get; set; }

        public int? SiblingsClassId { get; set; }


    }
}
