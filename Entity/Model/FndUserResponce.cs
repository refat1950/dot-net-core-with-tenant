using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entity.Model
{
    public partial class FndUserResponce : BaseEntity
    {
        public int UserId { get; set; }
        public int ResponId { get; set; }
    }
}
