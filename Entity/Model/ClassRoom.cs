﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class ClassRoom : BaseEntity
    {
        [Key]
        public int ClassRoomHeaderId { get; set; }
        public int? Roomcapacity { get; set; }
        public string ClassRoomName { get; set; }
        public bool Status { get; set; }
    }
}
