using Entity.Model;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entity.Model
{
    public partial class GeneralInfo : BaseEntity
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int InfoId { get; set; }

        public int InfoTypeId { get; set; }

        public int SchoolId { get; set; }

        public int? Sequnece { get; set; }

        [Required]
        [StringLength(100)]
        public string InfoTitle { get; set; }

        public string InfoDescription { get; set; }

        public virtual School BssSchool { get; set; }

        public virtual GeneralInfoType GeneralInfoType { get; set; }
    }
}
