﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class ExamSchedule : BaseEntity
    {
        [Key]
        public int ExamScheduleId { get; set; }
        public int SessionId { get; set; }
        public int ExamId { get; set; }
        public string RoomIds { get; set; }
        public bool? IsExamSeatPlanPublished { get; set; }
        public bool? IsExamRoutinePublished { get; set; }
        public virtual List<ExamScheduleLine> ExamScheduleLines { get; set; }
    }
}
