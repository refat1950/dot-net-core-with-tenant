﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class Menu : BaseEntity
    {
        [Key]
        public int MenuId { get; set; }

        [Required(ErrorMessage = "Menu Name is required")]
        [Display(Name = "Menu Name")]
        public string MenuName { get; set; }

        [Required(ErrorMessage = "Menu Url is required")]
        [Display(Name = "Url")]
        public string Url { get; set; }

        [Range(0, int.MaxValue, ErrorMessage = "Sequence must be number")]
        [Required(ErrorMessage = "Menu Sequence Required")]
        [Display(Name = "Sequence")]
        public int Sequence { get; set; }

        [Display(Name = "Is Active")]
        public bool IsActive { get; set; }

        [Display(Name = "Parent Menu")]
        public int? ParentMenuId { get; set; }

        public virtual Menu ParentMenu { get; set; }
    }
}
