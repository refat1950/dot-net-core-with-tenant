﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    /// <summary>
    ///     Mimimal set of data needed to persist role information
    /// </summary>
    //public interface IRole : IRole<int>
    //{
    //}

    /// <summary>
    ///     Mimimal set of data needed to persist role information
    /// </summary>
    /// <typeparam name="TKey"></typeparam>
    public interface IRole
    {
        /// <summary>
        ///     Id of the role
        /// </summary>
        int RoleId { get; }

        /// <summary>
        ///     Name of the role
        /// </summary>
        string RoleName { get; set; }
    }
}
