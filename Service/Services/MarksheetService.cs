﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics.Eventing.Reader;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using Entity.Model;
using Repository.Context;
using Repository.Provider;
using Services.Interfaces;
using Services.View_Model;
using Services.Common;
using Service.Services;
using Entity.Model.Transaction;
using Web.Models.View_Model;
using data.UnitOfWork;

namespace Services
{
    public class MarksheetService : IMarksheetService
    {
        private IUnitOfWork _unitOfWork;
        

        public MarksheetService( IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public VmMarkSheet ProcesseMarksData(int StudentHeaderId, int sessionId, int examid, int ClassId, int SectionId,List<ExamGrade> examGrades)
        {
            var subjectCount = 0;
            decimal allsub = 0;
            decimal allsubTotal = 0;
            string resultGrade = "";
            string point = "";
            decimal totalBaseMark = 0;
            decimal totalObtainmarks = 0;
            decimal totalPoints = 0;
            var grades = new List<string>();

            var examtypeRepo = _unitOfWork.ExamTypeRepo.GetAllEnumerable();
            var campus = _unitOfWork.CampusRepo.GetAllQueryable();
            var classdata = _unitOfWork.ClassRepo.FirstOrDefault(f => f.ClassId == ClassId);
            var section = _unitOfWork.SectionRepo.FirstOrDefault(f => f.SectionId == SectionId);
            var exam = _unitOfWork.ExamRepo.FirstOrDefault(s => s.ExamId == examid);
            var student = _unitOfWork.StudentRepo.FirstOrDefault(w => w.StudentHeaderId == StudentHeaderId);
            var ClassSubjectWiseExamTypeRepo = _unitOfWork.ClassSubjectWiseExamTypeRepo.GetAllEnumerable();
            var examMarks = _unitOfWork.StudentMarksRepo.Find(w => w.SessionId == sessionId && w.ExamId == examid && w.ClassId == ClassId && w.SectionId == SectionId && w.StudentHeaderId == StudentHeaderId).ToList();

            VmMarkSheet vmMarkSheet = new VmMarkSheet();
            vmMarkSheet.SessionId = sessionId;
            if (student != null)
            {
                // student info
                vmMarkSheet.StudentHeaderId = student.StudentHeaderId;
                vmMarkSheet.StudentName = student.FirstName + " " + student.MiddleName + " " + student.LastName;
                vmMarkSheet.StudentRoll = student.RollNumber;
                vmMarkSheet.ClassId = ClassId;
                vmMarkSheet.ClassName = classdata != null ? classdata.ClassName : "";
                vmMarkSheet.SectionId = SectionId;
                vmMarkSheet.SectionName = section != null ? section.SectionName : "";
                vmMarkSheet.CampusId = student.CampusId;
                vmMarkSheet.CampusName = campus.FirstOrDefault(f => f.CampusId == student.CampusId) != null ? campus.FirstOrDefault(f => f.CampusId == student.CampusId).CampusName : "";
                vmMarkSheet.ExamId = examid;
                vmMarkSheet.ExamName = exam != null ? exam.ExamName : "";

                var ClassWiseSubjectlineid = _unitOfWork.StudentSubjectLinesRepo.FirstOrDefault(w => w.StudentHeaderId == student.StudentHeaderId) != null ? _unitOfWork.StudentSubjectLinesRepo.FirstOrDefault(w => w.StudentHeaderId == student.StudentHeaderId).StudentSubjectLineId : 0;
                //subject list of student
                /* var ClassWiseSubjectList = _unitOfWork.ClassSubject.Where(w => w.ClassId == student.ClassId).OrderBy(o => o.ResultSerial).ToList();*/
                var ClassWiseSubjectList = _unitOfWork.StudentWiseSubjectsRepo.Find(w => w.StudentSubjectLineId == ClassWiseSubjectlineid && w.IsSubjectAssigned == true).ToList();
                if (ClassWiseSubjectList.Any())
                {
                    List<Subject> subjectList = ClassWiseSubjectList.Select(w => w.Subject).ToList();// new List<Subject>();

                    vmMarkSheet.vmSubjectWiseMarks = new List<VmMarkSheetSubject>();
                    foreach (var subject in subjectList)
                    {
                        VmMarkSheetSubject vmMarkSheetSubject = new VmMarkSheetSubject();
                        vmMarkSheetSubject.SubjectId = subject.SubjectId;
                        vmMarkSheetSubject.SubjectName = subject.SubjectName;

                        ////:strat: examtype wise all marks
                        vmMarkSheetSubject.vmMarkSheetExamTypeWiseMarks = new List<VmMarkSheetExamTypeWiseMarks>();
                        var examMarksList = examMarks.Where(w => w.SubjectId == subject.SubjectId).ToList();

                        var thisSubjectAllExamType = ClassSubjectWiseExamTypeRepo.Where(w => w.ClassId == student.ClassId && w.SubjectId == subject.SubjectId && w.ExamId == examid).ToList();

                        foreach (var pid in thisSubjectAllExamType)
                        {
                            var parentId = pid.ExamTypeId;
                            var examType = examtypeRepo.FirstOrDefault(f => f.ExamTypeId == parentId);

                            var item = new VmMarkSheetExamTypeWiseMarks();
                            item.ExamTypeId = parentId;
                            item.ExamTypeName = examType != null ? examType.ExamTypeName : "";

                            var allChieldsOfthoseParents = examtypeRepo.Where(w => w.ParentExamTypeId == parentId).Select(s => s.ExamTypeId).ToList();
                            var allAssignedChield = examMarksList.Where(w => allChieldsOfthoseParents.Contains(w.ExamTypeId)).ToList();

                            if (allChieldsOfthoseParents.Any())
                            {
                                // get Base mark from one of the first child from assigned chields, else get it from original parent list
                                item.BaseMarks = allAssignedChield.Any() ? allAssignedChield.FirstOrDefault().OutOfMarks
                                    : thisSubjectAllExamType.FirstOrDefault(s => s.ExamTypeId == parentId) != null ? thisSubjectAllExamType.FirstOrDefault(s => s.ExamTypeId == parentId).BaseMark : 0;

                                var total_ExamType = allChieldsOfthoseParents.Count();
                                var total_ObtainedMarks = allAssignedChield.Select(s => s.ObtainedMarks).Sum() ?? 0;

                                var wasAbsentInAllExamInThisExamType = allChieldsOfthoseParents.Count() == allAssignedChield.Count() && allAssignedChield.Count() == allAssignedChield.Where(w => w.AttendanceType == AttendanceType.Absent).Count();

                                if (examType.SubExamMarkBase == SubExamMarkBase.Average)
                                    item.ObtainedMarks = total_ObtainedMarks / total_ExamType;
                                else if (examType.SubExamMarkBase == SubExamMarkBase.Highest)
                                    item.ObtainedMarks = allAssignedChield.OrderByDescending(o => o.ObtainedMarks).Any()
                                        ? allAssignedChield.OrderByDescending(o => o.ObtainedMarks).FirstOrDefault().ObtainedMarks ?? 0 : 0;

                                if (allAssignedChield.Any()) // if any chield assigned
                                {
                                    if (wasAbsentInAllExamInThisExamType)
                                        item.ObtainedMarksText = "A";
                                    else
                                        item.ObtainedMarksText = Round(item.ObtainedMarks).ToString();
                                }
                                else // no marks assigned to all chields
                                    item.ObtainedMarksText = "NA";
                            }
                            else
                            {
                                var AssignedExamType = examMarksList.FirstOrDefault(f => f.ExamTypeId == parentId);
                                item.BaseMarks = AssignedExamType != null ? AssignedExamType.OutOfMarks : thisSubjectAllExamType.FirstOrDefault(s => s.ExamTypeId == parentId) != null ? thisSubjectAllExamType.FirstOrDefault(s => s.ExamTypeId == parentId).BaseMark : 0;

                                item.ObtainedMarks = AssignedExamType != null ? AssignedExamType.ObtainedMarks ?? 0 : 0;

                                if (AssignedExamType != null)
                                {
                                    if (AssignedExamType.AttendanceType == AttendanceType.Absent)
                                        item.ObtainedMarksText = "A";
                                    else
                                        item.ObtainedMarksText = Round(AssignedExamType.ObtainedMarks ?? 0).ToString();
                                }
                                else
                                    item.ObtainedMarksText = "NA";
                            }
                            item.ObtainedMarks = (decimal)(float)System.Math.Round(item.ObtainedMarks, 2);
                            vmMarkSheetSubject.vmMarkSheetExamTypeWiseMarks.Add(item);
                        }
                        vmMarkSheet.vmSubjectWiseMarks.Add(vmMarkSheetSubject);


                        // *********** marks calculation
                        // this subjects all marks
                        if (vmMarkSheetSubject.vmMarkSheetExamTypeWiseMarks != null)
                        {
                            foreach (var subjectMark in vmMarkSheetSubject.vmMarkSheetExamTypeWiseMarks)
                            {
                                totalBaseMark += subjectMark.BaseMarks;
                                allsub += subjectMark.ObtainedMarks;
                                totalObtainmarks += subjectMark.ObtainedMarks;
                            }
                        }

                        allsubTotal += allsub;
                        var obtainedResult = examGrades.Where(w => allsubTotal >= (int)w.FromScore && allsubTotal <= (int)w.ToScore).FirstOrDefault();
                        resultGrade = obtainedResult != null ? obtainedResult.AltName : "";
                        point = obtainedResult != null ? obtainedResult.Point : "0";
                        //:end:

                        vmMarkSheetSubject.SubjectTotalMarks = allsubTotal;
                        vmMarkSheetSubject.SubjectTotalGrade = resultGrade;
                        vmMarkSheetSubject.SubjectTotalPoint = point;

                        //reset variables
                        grades.Add(resultGrade);
                        resultGrade = "";
                        totalPoints += Convert.ToDecimal(point);
                        point = "";
                        allsub = 0;
                        allsubTotal = 0;
                        subjectCount++;
                        // *********** end of marks calculation
                    }


                    // ************* all Total/avarage masks calculation
                    var average_point = "0";
                    var average_grade = "0";
                    if (grades.Contains("F"))
                    {
                        average_point = "";
                        average_grade = "F";
                    }
                    else
                    {
                        var a_point = (decimal)(float)System.Math.Round((totalPoints / subjectCount), 2);
                        average_point = a_point.ToString();
                        var a_obtainMark = (decimal)(float)System.Math.Round((totalObtainmarks / subjectCount), 2);
                        var Result = examGrades.Where(w => a_obtainMark >= (int)w.FromScore && a_obtainMark <= (int)w.ToScore).FirstOrDefault();
                        average_grade = Result != null ? Result.AltName : "";
                    }
                    vmMarkSheet.TotalBaseMark = totalBaseMark;
                    vmMarkSheet.TotalObtainmarks = totalObtainmarks;
                    vmMarkSheet.AvaragePoint = average_point;
                    vmMarkSheet.AvarageGrade = average_grade;
                    // ************* end of all Total/avarage masks calculation
                }
            }
            return vmMarkSheet;
        }

        public VmMarkSheet GetProcessedMarkSheetData(int studentHeaderId, int sessionId, int examid, int classId, int sectionId, List<ExamGrade> examGrades)
        {
            var subjectCount = 0;
            decimal allsub = 0;
            decimal allsubTotal = 0;
            string resultGrade = "";
            string point = "";
            decimal totalBaseMark = 0;
            decimal totalObtainmarks = 0;
            decimal totalPoints = 0;
            var grades = new List<string>();

            var getCurrentlyCalculatedMarksheet = ProcesseMarksData(studentHeaderId, sessionId, examid, classId, sectionId, examGrades);
            var examtypeRepo = _unitOfWork.ExamTypeRepo.GetAllEnumerable();

            var vmMarkSheet = new VmMarkSheet();
            var marksData = _unitOfWork.ProcessedMarksheetsRepo.Find(w => w.StudentHeaderId == studentHeaderId && w.SessionId == sessionId && w.ExamId == examid && w.ClassId == classId && w.SectionId == sectionId).ToList();
            if (marksData.Any())
            {
                vmMarkSheet.StudentHeaderId = getCurrentlyCalculatedMarksheet.StudentHeaderId;
                vmMarkSheet.StudentName = getCurrentlyCalculatedMarksheet.StudentName;
                vmMarkSheet.StudentRoll = getCurrentlyCalculatedMarksheet.StudentRoll;
                vmMarkSheet.ClassId = getCurrentlyCalculatedMarksheet.ClassId;
                vmMarkSheet.ClassName = getCurrentlyCalculatedMarksheet.ClassName;
                vmMarkSheet.SectionId = getCurrentlyCalculatedMarksheet.SectionId;
                vmMarkSheet.SectionName = getCurrentlyCalculatedMarksheet.SectionName;
                vmMarkSheet.CampusId = getCurrentlyCalculatedMarksheet.CampusId;
                vmMarkSheet.CampusName = getCurrentlyCalculatedMarksheet.CampusName;
                vmMarkSheet.ExamId = getCurrentlyCalculatedMarksheet.ExamId;
                vmMarkSheet.ExamName = getCurrentlyCalculatedMarksheet.ExamName;
                vmMarkSheet.vmSubjectWiseMarks = new List<VmMarkSheetSubject>();

                var allSubjectsOfThisMarksheet = getCurrentlyCalculatedMarksheet.vmSubjectWiseMarks.Select(s => new { s.SubjectId,s.SubjectName }).ToList();
                foreach (var subject in allSubjectsOfThisMarksheet)
                {
                    VmMarkSheetSubject vmMarkSheetSubject = new VmMarkSheetSubject();
                    vmMarkSheetSubject.SubjectId = subject.SubjectId;
                    vmMarkSheetSubject.SubjectName = subject.SubjectName;

                    ////:strat: examtype wise all marks
                    vmMarkSheetSubject.vmMarkSheetExamTypeWiseMarks = new List<VmMarkSheetExamTypeWiseMarks>();
                    var examMarksList = marksData.Where(w => w.SubjectId == subject.SubjectId).ToList();

                    foreach (var e in examMarksList)
                    {
                        var examType = examtypeRepo.FirstOrDefault(f => f.ExamTypeId == e.ExamTypeId);
                        var item = new VmMarkSheetExamTypeWiseMarks();
                        item.ExamTypeId = e.ExamTypeId;
                        item.ExamTypeName = examType != null ? examType.ExamTypeName : "";
                        item.BaseMarks = e.BaseMarks;
                        item.ObtainedMarks = e.ObtainedMarks;
                        item.ObtainedMarksText = e.ObtainedMarksText;
                        vmMarkSheetSubject.vmMarkSheetExamTypeWiseMarks.Add(item);
                    }
                    vmMarkSheet.vmSubjectWiseMarks.Add(vmMarkSheetSubject);

                    // *********** marks calculation
                    // this subjects all marks
                    if (vmMarkSheetSubject.vmMarkSheetExamTypeWiseMarks != null)
                    {
                        foreach (var subjectMark in vmMarkSheetSubject.vmMarkSheetExamTypeWiseMarks)
                        {
                            totalBaseMark += subjectMark.BaseMarks;
                            allsub += subjectMark.ObtainedMarks;
                            totalObtainmarks += subjectMark.ObtainedMarks;
                        }
                    }

                    allsubTotal += allsub;
                    var obtainedResult = examGrades.Where(w => allsubTotal >= (int)w.FromScore && allsubTotal <= (int)w.ToScore).FirstOrDefault();
                    resultGrade = obtainedResult != null ? obtainedResult.AltName : "";
                    point = obtainedResult != null ? obtainedResult.Point : "0";
                    //:end:

                    vmMarkSheetSubject.SubjectTotalMarks = allsubTotal;
                    vmMarkSheetSubject.SubjectTotalGrade = resultGrade;
                    vmMarkSheetSubject.SubjectTotalPoint = point;

                    //reset variables
                    grades.Add(resultGrade);
                    resultGrade = "";
                    totalPoints += Convert.ToDecimal(point);
                    point = "";
                    allsub = 0;
                    allsubTotal = 0;
                    subjectCount++;
                    // *********** end of marks calculation
                }

                // ************* all Total/avarage masks calculation
                var average_point = "0";
                var average_grade = "0";
                if (grades.Contains("F"))
                {
                    average_point = "";
                    average_grade = "F";
                }
                else
                {
                    var a_point = (decimal)(float)System.Math.Round((totalPoints / subjectCount), 2);
                    average_point = a_point.ToString();
                    var a_obtainMark = (decimal)(float)System.Math.Round((totalObtainmarks / subjectCount), 2);
                    var Result = examGrades.Where(w => a_obtainMark >= (int)w.FromScore && a_obtainMark <= (int)w.ToScore).FirstOrDefault();
                    average_grade = Result != null ? Result.AltName : "";
                }
                vmMarkSheet.TotalBaseMark = totalBaseMark;
                vmMarkSheet.TotalObtainmarks = totalObtainmarks;
                vmMarkSheet.AvaragePoint = average_point;
                vmMarkSheet.AvarageGrade = average_grade;
                // ************* end of all Total/avarage masks calculation
            }
            return vmMarkSheet;
        }

        public void StoreProcessedMarkSheet(VmMarkSheet vmMarkSheet ,int userid)
        {
            var pDataList = new List<ProcessedMarksheet>();
            if (vmMarkSheet != null)
            {
                foreach (var subject in vmMarkSheet.vmSubjectWiseMarks)
                {
                    foreach (var exam in subject.vmMarkSheetExamTypeWiseMarks)
                    {
                        var newMarkSheet = new ProcessedMarksheet
                        {
                            StudentHeaderId = vmMarkSheet.StudentHeaderId,
                            SessionId = vmMarkSheet.SessionId,
                            ClassId = vmMarkSheet.ClassId ?? 0,
                            SectionId = vmMarkSheet.SectionId ?? 0,
                            CampusId = vmMarkSheet.CampusId,
                            ExamId = vmMarkSheet.ExamId,
                            SubjectId = subject.SubjectId,
                            ExamTypeId = exam.ExamTypeId,
                            BaseMarks = exam.BaseMarks,
                            ObtainedMarks = exam.ObtainedMarks,
                            ObtainedMarksText = exam.ObtainedMarksText,
                            CreatedBy = userid,
                            CreationDate = DateTime.Now
                        };
                        pDataList.Add(newMarkSheet);
                    }
                }
            }
            if (pDataList.Any())
            {
                _unitOfWork.ProcessedMarksheetsRepo.AddRange(pDataList);
                _unitOfWork.SaveChanges();
            }
        }
        public bool IsMarkSeetProcessed(int studentHeaderId, int sessionId, int examid, int classId, int sectionId)
        {
            return _unitOfWork.ProcessedMarksheetsRepo.Find(w => w.StudentHeaderId == studentHeaderId && w.SessionId == sessionId && w.ExamId == examid && w.ClassId == classId && w.SectionId == sectionId).Any();
        }

        public decimal Round(decimal mark)
        {
            return (decimal)(float)System.Math.Round(mark, 2);
        }
    }
}