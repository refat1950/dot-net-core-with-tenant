using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entity.Model
{
    public partial class VoteMember : BaseEntity
    {
        public int VotePersonId { get; set; }
        public int ReferenceId { get; set; }
        public int VoteId { get; set; }

        [StringLength(50)]
        public string ReferenceType { get; set; }

        public int? VoteOptionId { get; set; }

        public int? Status { get; set; }


    }
}
