using System;
using System.ComponentModel.DataAnnotations;

namespace Entity.Model
{
    public partial class FndResponsibility : BaseEntity
    {
        [Key]
        public int RESPON_ID { get; set; }

        [Required]
        [StringLength(100)]
        public string ResponName { get; set; }


    }
}
