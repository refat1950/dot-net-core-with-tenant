using Entity.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Entity.Model
{
    public partial class FndFlexValueSet : BaseEntity
    {

        [Key]
        public int FlexValueSetId { get; set; }

        [Required]
        [StringLength(100)]
        public string FlexValueSetName { get; set; }
        [Required]
        [StringLength(30)]
        public string FlexValueShortName { get; set; }

        public int? ModuleId { get; set; }

        public virtual ICollection<FndFlexValue> FndFlexValue { get; set; }
    }
}
