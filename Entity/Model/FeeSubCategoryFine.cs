﻿using System.ComponentModel.DataAnnotations;

namespace Entity.Model
{
    public class FeeSubCategoryFine : BaseEntity
    {
        [Key]
        public int SubCategoryFineId { get; set; }
        public int FeeSubCategoryId { get; set; }
        public bool IsPercentage { get; set; }
        public decimal Amount { get; set; }
        public bool IsIncremental { get; set; }
        public int? IncrementType { get; set; }
        public int? DaysForIncrement { get; set; }
        public decimal? MaximumIncrementAmount { get; set; }
        public virtual FeeSubCategory FeeSubCategory { get; set; }
    }
}
