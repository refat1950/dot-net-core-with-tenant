﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class AttendanceProcess : BaseEntity
    {
        [Key]
        public int Id { get; set; }
        public int EmpHeaderId { get; set; }
        public DateTime PunchDate { get; set; }
        [Required]
        [StringLength(50)]
        public string PunchTime { get; set; }
        public string CreatedIp { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string LocationException { get; set; }
    }
}
