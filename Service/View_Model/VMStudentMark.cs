﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using System.Threading.Tasks;

namespace Services.View_Model
{
    public class VMStudentMark
    {
        public int  MarksId { get; set; }
        public int  StudentHeaderId { get; set; }
        public string StudentName { get; set; }
        public string StudentId { get; set; }
        public int ClassId { get; set; }
        public int SessionId { get; set; }
        public int SectionId { get; set; }
        public int? MasterEventId { get; set; }
        public int? EventId { get; set; }
        public decimal? ObtainMarks { get; set; }
        public decimal? OutOfMarks { get; set; }
        public string IsPublish { get; set; }
    }

    public class VMStudentMarkList
    {
        public int StudentHeaderId { get; set; }
        public string StudentName { get; set; }
        public string StudentId { get; set; }
        public string IsNew { get; set; }
        public decimal? TotalMarks { get; set; }
        public List<VMStudentMark> VmStudentMarks { get; set; }
    }
}
