using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entity.Model
{
    public class StudentBillLineTemporary
    {
        [Key]
        public int BillHeaderId { get; set; }

        public int? BillDtlId { get; set; }

        [StringLength(10)]
        public string RcvType { get; set; }

        public int? FeeSubCategoryId { get; set; }

        //[Column(TypeName = "money")]
        public decimal? BillAmount { get; set; }

        public decimal? ActualRcvAmount { get; set; }

        public decimal? DiscAmount { get; set; }

        public int? PeriodId { get; set; }

        [StringLength(200)]
        public string Remarks { get; set; }

        [StringLength(100)]
        public string VatAmt { get; set; }
        public decimal? WaiberPercentage { get; set; }
        public decimal? WaiberAmount { get; set; }
        public decimal? LateFee { get; set; }
        public int YearId { get; set; }

        [StringLength(10)]
        public string Segment1 { get; set; }

        [StringLength(10)]
        public string Segment2 { get; set; }

        [StringLength(10)]
        public string Segment3 { get; set; }

        [StringLength(10)]
        public string Segment4 { get; set; }

        [StringLength(10)]
        public string Segment5 { get; set; }

        [StringLength(10)]
        public string Segment6 { get; set; }

        [StringLength(10)]
        public string Segment7 { get; set; }

        [StringLength(10)]
        public string DiscAcc { get; set; }

        [StringLength(250)]
        public string Attribute1 { get; set; }
        [StringLength(250)]
        public string Attribute2 { get; set; }
        [StringLength(250)]
        public string Attribute3 { get; set; }
        [StringLength(250)]
        public string Attribute4 { get; set; }
        [StringLength(250)]
        public string Attribute5 { get; set; }

        public long? CreatedBy { get; set; }


        /// <summary>
        /// WHO Column
        /// </summary>
        [DataType(DataType.Date, ErrorMessage = "Date format should be MM/DD/YYYY")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? CreationDate { get; set; }


        public long? LastUpdateBy { get; set; }

        [DataType(DataType.Date, ErrorMessage = "Date format should be MM/DD/YYYY")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? LastUpdateDate { get; set; }

        [ForeignKey("BillDtlId")]
        public virtual StudentBillMasterTemporary StudentBillMasterTemporary { get; set; }

    }
}
