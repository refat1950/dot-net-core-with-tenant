using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entity.Model
{
    public partial class News : BaseEntity
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }
        [StringLength(150)]
        public string NewsTitle { get; set; }
        [StringLength(250)]
        public string Description { get; set; }

        public DateTime? NewsDate { get; set; }
    }
}
