﻿using System;
using System.Collections.Generic;
using System.Linq;
using Entity.Model;
using Service.View_Model;
using Services.View_Model;
using Web.Models;
using Web.Models.View_Model;

namespace Services.Interfaces
{
    public interface IStudentService
    {
        List<StudentClassRoutineViewModel> GetStudentClassRoutine(DateTime date, int sessionId, int classId, int sectionId);

        List<StudentDocumentViewModel> GetStudentDocuments(int classId, int sectionId, DateTime date);
        List<StudentSubjectMarksViewModel> StudentSubjectMarks(int classId, int studentId, int sessionId);
        string StudentSubjectBarChart(List<StudentSubjectMarksViewModel> subjects);
        StudentBillInfoViewModel GetStudentBillInfo(int studentId, int sessionId);
        List<StudentBillViewModel> GetStudentBill(int studentId, int sessionId);

        List<StudentNoticeBoardViewModel> GetStudentNoteces(int studentId, int classId, int sectionId, int campusId);

        List<VMNotice> GetFullNotice(int classId, int sectionId, int campusId);
        List<string> GetNews(int classId, int sectionId, int campusId);

        long GetStudentRoutinCount(int studentId, string startDate, string endDate, string routineType, int semesters, int subjectId);
        IEnumerable<StudentClassExamRutine> GetStudentRoutin(int studentId, int skip, int take, string startDate, string endDate, string routineType, int semesters, int subjectId, bool isCount);
        IEnumerable<StudentClassExamRutine> GetStudentClassRoutin(int sessionId, int classId, int sectionId, int campusId, int skip, int take, DateTime startDate, DateTime endDate, int semesters, int subjectId, bool isCount);
        IEnumerable<StudentClassExamRutine> GetStudentExamRoutin(int sessionId, int classId, int sectionId, int campusId, int skip, int take, DateTime startDate, DateTime endDate, int semesters, int subjectId);

        List<StudentCurriculumVm> GetStudentCuriculamByClassSection(int classId, int sectionId, int examId);
        List<StudentOtherInfo> GetStudentOtherInfo(int classId, int sectionId, int examId);

        string StudentCuriculamAddOrUpdate(VmCurricullamSave postCurricullam, int userId);
        string StudentInfoAddOrUpdate(StudentOtherInfoSave postCurricullam, int userId);
        void AddStudentTransaction(int studentheaderid);

        List<VmStudentData> GetStudents(string search, int start, int displaylength,  int? ChangeSessionId, int? ChangeClassId, int? ChangeSectionId, out int totalLength);
        VmStudentData StudentInfo(int studentheaderid);

        List<VmStuCurrentNoticeList> StudentNoticelist(int studentheaderid);

        List<VmDocumentList> StudentDocumentlist(int studentheaderid);
        List<VmRoutineList> GetRoutineData(int? studentheaderid);
        List<VmStudentAttendance> StudentAtendance(int studentheaderid, string FromDate, string ToDate);
        List<VmBillLinelist> StudentBillinglist(int studentheaderid, int feeSubCategoryId);
        List<VmStuLeaveApplyList> StudentLeaveApplyList(int studentheaderid);
        List<VmStudentDocument> StudentdatewiseDocumentlist(int studentheaderid , string SelectDate);
        string GetBillDate(int billId);
        string GetBillPeriod(int billId);

        List<VmRoomSeatPlan> GetExamSeatView(int ExamScheduleId, List<int> RoomIds);


    }
}
