﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class LeaveMaster : BaseEntity
    {
        [Key]
        public int LeaveMasterId { get; set; }
        public int EmployeeHeaderId { get; set; }
        public int LeaveTypeId { get; set; }
        public int Status { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string FromTime { get; set; }
        public string ToTime { get; set; }
        public string Reason { get; set; }
        public string AttachmentUrl { get; set; }
        public int LeaveApprover { get; set; }
        //public string ATTACHMENT_URL2 { get; set; }
    }
}
