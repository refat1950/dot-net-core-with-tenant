﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class StudentOtherInfo
    {
        [Key]
        public int InfoId { get; set; }
        public int SessionId { get; set; }
        public int EventId { get; set; }
        public int StudentHeaderId { get; set; }

        public int TotalWorkingDay { get; set; }
        public int TotalAbsent { get; set; }
        public int TotalLate { get; set; }

        public decimal Height { get; set; }
        public decimal Weight { get; set; }


        //public string Remarks { get; set; }

        public string Attribute1 { get; set; }
        public string Attribute2 { get; set; }
        public string Attribute3 { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? CreationDate { get; set; }
        public int? LastUpdateBy { get; set; }
        public DateTime? LasteUpdateDate { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "Active From")]
        public DateTime? EffectiveFrom { get; set; }


        [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "Active To")]
        public DateTime? EffectiveTo { get; set; }


    }
}