﻿using data.UnitOfWork;
using Repository.Context;
using Service.DataService;
using Service.Helper;
using Service.Services.Interfaces;
using Service.ViewModel.Payroll;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Service.Services.Payroll
{
    public class EarlyOutFeeFullMonth : IPaySlipEarlyOutFee
    {
        private IUnitOfWork unitOfWork;
        private IEmployeeAttendanceService employeeAttendanceService;
        public EarlyOutFeeFullMonth(IUnitOfWork unitOfWork, IEmployeeAttendanceService employeeAttendanceService)
        {
            this.unitOfWork = unitOfWork;
            this.employeeAttendanceService = employeeAttendanceService;
        }

        public CalculatedFee GetEarlyOutFee(int empHeaderId, int periodId)
        {
            var period = unitOfWork.PeriodRepo.FirstOrDefault(s => s.PeriodId == periodId);
            if (period !=null)
            {
                var earlyOutPayheadId = unitOfWork.HrPayHeadsRepo.FirstOrDefault(s => s.PayHeadShortValue == "eo");
                var thisEmpFeePerHour = earlyOutPayheadId != null ? unitOfWork.HrEmployeePayHeadsRepo.FirstOrDefault(s => s.EmpHeaderId == empHeaderId && s.PayHeadId == earlyOutPayheadId.PayHeadId) != null ? unitOfWork.HrEmployeePayHeadsRepo.FirstOrDefault(s => s.EmpHeaderId == empHeaderId && s.PayHeadId == earlyOutPayheadId.PayHeadId).Unit : 0 : 0;
                var thisEmpFeePerMinute = (decimal)(Convert.ToDouble(thisEmpFeePerHour) / 60.0);
                var totalMinutes = 0;

                var startDate = period.StartDate.Value.Date;
                var endDate = period.EndDate.Value.Date;
                var dayDistance = (endDate - startDate).TotalDays;
                for (var i = 0; i <= dayDistance; i++)
                {
                    var date = startDate.AddDays(i);
                    var atnData = employeeAttendanceService.GetDayAttendanceData(date, empHeaderId);
                    if ( !String.IsNullOrEmpty(atnData.EarlyOut) && atnData.EarlyOut != "00:00")
                    {
                        var time = DateTime.Parse(atnData.EarlyOut);
                        var totalMinute = (time.Hour > 0 ? time.Hour * 60 : 0) + time.Minute;
                        totalMinutes = totalMinutes + totalMinute;
                    }
                }
                var d = new CalculatedFee();
                d.Fee = Math.Round(thisEmpFeePerMinute * totalMinutes, 0);
                d.Hours = Math.Round((decimal)(totalMinutes / 60.0), 2).ToString();
                d.Minutes = totalMinutes.ToString();
                return d;
            }
            else
                return new CalculatedFee();
        }
    }
}
