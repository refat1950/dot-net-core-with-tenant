﻿using data.UnitOfWork;
using Entity.Model;
using Repository.Context;
using Repository.Utility;
using Service.Helper;
using Service.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Service.Services.Payroll
{
    public class PaySlipTransaction : IPaySlipTransaction
    {
        private IPaySlipOverTime paySlipOverTime;
        private IPaySlipEarlyOutFee paySlipEarlyOutFee;
        private IPaySlipLateFee paySlipLateFee;
        private IUnitOfWork unitOfWork;
        public PaySlipTransaction(IUnitOfWork unitOfWork, IPaySlipOverTime paySlipOverTime, IPaySlipEarlyOutFee paySlipEarlyOutFee, IPaySlipLateFee paySlipLateFee)
        {
            this.unitOfWork = unitOfWork;
            this.paySlipOverTime = paySlipOverTime;
            this.paySlipEarlyOutFee = paySlipEarlyOutFee;
            this.paySlipLateFee = paySlipLateFee;
        }

        // generate with current employee payheads
        public bool GeneratePayslip(int empHeaderId, int periodId, int? userid, int poisitionid, int payrollid)
        {
            var checkIfPeriodWisePaySlipExist = unitOfWork.HrPaySlipTransactionMastersRepo.FirstOrDefault(w => w.PeriodId == periodId && w.EmpHeaderId == empHeaderId && w.PositionId == poisitionid && w.PayrollId == payrollid);

            if (checkIfPeriodWisePaySlipExist == null)
            {
                GeneratePayslipWith(PayslipGenerationType.New, empHeaderId, periodId,userid,poisitionid,payrollid, checkIfPeriodWisePaySlipExist);
                return true;
            }
            else
            {
                if (checkIfPeriodWisePaySlipExist.PayslipCurrentStatus == PayslipCurrentStatus.Canceled)
                {
                    GeneratePayslipWith(PayslipGenerationType.Canceled, empHeaderId, periodId, userid, poisitionid, payrollid, checkIfPeriodWisePaySlipExist);
                    return true;
                }
                else
                    return false;
            }
        }

        public void GeneratePayslipWith(PayslipGenerationType type, int empHeaderId, int periodId, int? userid, int poisitionid, int payrollid, HrPaySlipTransactionMaster existingmasterdata)
        {
            var generateWithLateFee = unitOfWork.FlexValueRepo.Find(f => f.FlexShortValue == "GenerateWithLateFee").FirstOrDefault() != null ? unitOfWork.FlexValueRepo.Find(f => f.FlexShortValue == "GenerateWithLateFee").FirstOrDefault().Attribute1 == "True" ? true : false : true;
            var generateWithEarlyOutFee = unitOfWork.FlexValueRepo.Find(f => f.FlexShortValue == "GenerateWithEarlyOutFee").FirstOrDefault() != null ? unitOfWork.FlexValueRepo.Find(f => f.FlexShortValue == "GenerateWithEarlyOutFee").FirstOrDefault().Attribute1 == "True" ? true : false : true;
            var generateWithOvertime = unitOfWork.FlexValueRepo.Find(f => f.FlexShortValue == "GenerateWithOvertime").FirstOrDefault() != null ? unitOfWork.FlexValueRepo.Find(f => f.FlexShortValue == "GenerateWithOvertime").FirstOrDefault().Attribute1 == "True" ? true : false : false;
           
            var latefeePayhead = unitOfWork.HrPayHeadsRepo.FirstOrDefault(s => s.PayHeadShortValue == "lf");
            var earlyOutPayhead = unitOfWork.HrPayHeadsRepo.FirstOrDefault(s => s.PayHeadShortValue == "eo");
            var overTimePayhead = unitOfWork.HrPayHeadsRepo.FirstOrDefault(s => s.PayHeadShortValue == "ot");
            var EmpPayHeads = unitOfWork.HrEmployeePayHeadsRepo.Find(w => w.EmpHeaderId == empHeaderId).ToList();

            //Generate PaySlidId
            var paysilpId = GetNewPayslipId(empHeaderId);

            HrPaySlipTransactionMaster hrPayMaster = new HrPaySlipTransactionMaster();
            
            if (type == PayslipGenerationType.New)
            {
                hrPayMaster.PaySlipMasterHeaderId = 0;
                hrPayMaster.PeriodId = periodId;
                hrPayMaster.EmpHeaderId = empHeaderId;
                hrPayMaster.PositionId = poisitionid;
                hrPayMaster.PayrollId = payrollid;
                hrPayMaster.TotalAmountPayable = 0; // the total amount will be update when a payslip generate is complete
                hrPayMaster.TotalAdditionAmount = 0;
                hrPayMaster.TotalDeductionAmount = 0;
                hrPayMaster.PaidStatus = PaidStatus.UnPaid;
                hrPayMaster.PayslipCurrentStatus = PayslipCurrentStatus.Generated;
                hrPayMaster.PayslipId = paysilpId;
                hrPayMaster.CreatedBy = userid;
                hrPayMaster.CreationDate = DateTime.Now;
                hrPayMaster.LastUpdateBy = userid;
                hrPayMaster.LastUpdateDate = DateTime.Now;
                hrPayMaster.GenerationDate = DateTime.Now;
                unitOfWork.HrPaySlipTransactionMastersRepo.Add(hrPayMaster);
                unitOfWork.SaveChanges();
            }
            else if (type == PayslipGenerationType.Canceled)
            {
                hrPayMaster = existingmasterdata;
                hrPayMaster.TotalAmountPayable = 0; // the total amount will be update when a payslip cancel is complete
                hrPayMaster.TotalAdditionAmount = 0;
                hrPayMaster.TotalDeductionAmount = 0;
                hrPayMaster.PaidStatus = PaidStatus.UnPaid;
                hrPayMaster.PayslipCurrentStatus = PayslipCurrentStatus.Generated;
                hrPayMaster.PayslipId = paysilpId;
                hrPayMaster.LastUpdateBy = userid;
                hrPayMaster.LastUpdateDate = DateTime.Now;
                hrPayMaster.GenerationDate = DateTime.Now;
                unitOfWork.SaveChanges();
            }
            
            // add master transaction log
            HrPaySlipTransactionMasterLog hrPayMasterLog = new HrPaySlipTransactionMasterLog();
            hrPayMasterLog.HrPaySlipTransactionMasterLogId = 0;
            hrPayMasterLog.PaySlipMasterHeaderId = hrPayMaster.PaySlipMasterHeaderId; // relational insert
            hrPayMasterLog.PeriodId = periodId;
            hrPayMasterLog.EmpHeaderId = empHeaderId;
            hrPayMasterLog.PositionId = poisitionid;
            hrPayMasterLog.PayrollId = payrollid;
            hrPayMasterLog.TotalAmountPayable = 0; // the total amount will be update when a payslip generate is complete
            hrPayMasterLog.TotalAdditionAmount = 0;
            hrPayMasterLog.TotalDeductionAmount = 0;
            hrPayMasterLog.PaidStatus = PaidStatus.UnPaid;
            hrPayMasterLog.PayslipCurrentStatus = PayslipCurrentStatus.Generated;
            hrPayMasterLog.PayslipId = paysilpId;
            hrPayMasterLog.CreatedBy = userid;
            hrPayMasterLog.CreationDate = DateTime.Now;
            hrPayMasterLog.LastUpdateBy = userid;
            hrPayMasterLog.LastUpdateDate = DateTime.Now;
            hrPayMasterLog.GenerationDate = DateTime.Now;
            unitOfWork.HrPaySlipTransactionMasterLogsRepo.Add(hrPayMasterLog);
            unitOfWork.SaveChanges();

            // add master transaction log history
            HrPaySlipTransactionMasterLogHistory hrPayMasterLogHistory = new HrPaySlipTransactionMasterLogHistory();
            hrPayMasterLogHistory.HrPaySlipTransactionMasterLogId = 0;
            hrPayMasterLogHistory.PaySlipMasterHeaderId = hrPayMaster.PaySlipMasterHeaderId; // relational insert
            hrPayMasterLogHistory.PeriodId = periodId;
            hrPayMasterLogHistory.EmpHeaderId = empHeaderId;
            hrPayMasterLogHistory.PositionId = poisitionid;
            hrPayMasterLogHistory.PayrollId = payrollid;
            hrPayMasterLogHistory.TotalAmountPayable = 0; // the total amount will be update when a payslip generate is complete
            hrPayMasterLogHistory.TotalAdditionAmount = 0;
            hrPayMasterLogHistory.TotalDeductionAmount = 0;
            hrPayMasterLogHistory.PaidStatus = PaidStatus.UnPaid;
            hrPayMasterLogHistory.PayslipCurrentStatus = PayslipCurrentStatus.Generated;
            hrPayMasterLogHistory.PayslipId = paysilpId;
            hrPayMasterLogHistory.CreatedBy = userid;
            hrPayMasterLogHistory.CreationDate = DateTime.Now;
            hrPayMasterLogHistory.LastUpdateBy = userid;
            hrPayMasterLogHistory.LastUpdateDate = DateTime.Now;
            hrPayMasterLogHistory.GenerationDate = DateTime.Now;
            unitOfWork.HrPaySlipTransactionMasterLogHistorysRepo.Add(hrPayMasterLogHistory);
            unitOfWork.SaveChanges();

            var basicSalaryId = unitOfWork.HrPayHeadsRepo.FirstOrDefault(s => s.PayHeadShortValue == "bs").PayHeadId;
            var thisEmpBasicSalary = EmpPayHeads.FirstOrDefault(s => s.PayHeadId == basicSalaryId).Unit;
            var payheadData = unitOfWork.HrPayHeadsRepo.GetAll();
            
            decimal totalAddition = 0;
            decimal totalDeduction = 0;
            foreach (var item in EmpPayHeads)
            {
                var thisPayhead = payheadData.FirstOrDefault(f => f.PayHeadId == item.PayHeadId);
                var payheadType = thisPayhead != null ? (int)thisPayhead.PayHeadType : 0;
                var addAllowed = true; // check if this employee contains any timing fee like late fee, early out or overtime fee, or has permission to generate these timing fees..
                var calculatedValue = item.Unit;
                var hours = "";
                var minutes = "";
                if (latefeePayhead != null && item.PayHeadId == latefeePayhead.PayHeadId) //if its late fee
                {
                    //check generate permission
                    if (generateWithLateFee)
                    {
                        var getfeedata = paySlipLateFee.GetLateFee(empHeaderId, periodId);
                        calculatedValue = Convert.ToInt16(getfeedata.Fee);
                        hours = getfeedata.Hours;
                        minutes = getfeedata.Minutes;
                    }
                    else addAllowed = false;
                }
                else if (earlyOutPayhead != null && item.PayHeadId == earlyOutPayhead.PayHeadId) // if its early out
                {
                    //check generate permission
                    if (generateWithEarlyOutFee)
                    {
                        var getfeedata = paySlipEarlyOutFee.GetEarlyOutFee(empHeaderId, periodId);
                        calculatedValue = Convert.ToInt16(getfeedata.Fee);
                        hours = getfeedata.Hours;
                        minutes = getfeedata.Minutes;
                    }
                    else addAllowed = false;
                }
                else if (overTimePayhead != null && item.PayHeadId == overTimePayhead.PayHeadId)// if its overtime
                {
                    //check generate permission
                    if (generateWithOvertime)
                    {
                        var getfeedata = paySlipOverTime.GetOvertimeFee(empHeaderId, periodId);
                        calculatedValue = Convert.ToInt16(getfeedata.Fee);
                        hours = getfeedata.Hours;
                        minutes = getfeedata.Minutes;
                    }
                    else addAllowed = false;
                }
                if (addAllowed)
                {
                    if (item.IsPercentage)
                        calculatedValue = (thisEmpBasicSalary / 100) * item.Unit;

                    // transaction table payhead insert
                    HrPaySlipTransaction hrPaySlipTransaction = new HrPaySlipTransaction();
                    hrPaySlipTransaction.PaySlipMasterHeaderId = hrPayMaster.PaySlipMasterHeaderId; // master table child insert
                    hrPaySlipTransaction.PayHeadId = item.PayHeadId;
                    hrPaySlipTransaction.Unit = item.Unit;
                    hrPaySlipTransaction.CalculatedValue = calculatedValue;
                    hrPaySlipTransaction.IsPercentage = item.IsPercentage;
                    hrPaySlipTransaction.CreatedBy = userid;
                    hrPaySlipTransaction.CreationDate = DateTime.Now;
                    hrPaySlipTransaction.Attribute1 = hours;
                    hrPaySlipTransaction.Attribute2 = minutes;
                    hrPaySlipTransaction.Attribute3 = payheadType.ToString();
                    unitOfWork.HrPaySlipTransactionsRepo.Add(hrPaySlipTransaction);
                    unitOfWork.SaveChanges();

                    // transaction log table payhead insert
                    HrPaySlipTransactionLog hrPaySlipTransactionLog = new HrPaySlipTransactionLog();
                    hrPaySlipTransactionLog.HrPaySlipTransactionMasterLogId = hrPayMasterLog.HrPaySlipTransactionMasterLogId; // master log table child insert
                    hrPaySlipTransactionLog.PayHeadId = item.PayHeadId;
                    hrPaySlipTransactionLog.Unit = item.Unit;
                    hrPaySlipTransactionLog.CalculatedValue = calculatedValue;
                    hrPaySlipTransactionLog.IsPercentage = item.IsPercentage;
                    hrPaySlipTransactionLog.CreatedBy = userid;
                    hrPaySlipTransactionLog.CreationDate = DateTime.Now;
                    hrPaySlipTransactionLog.Attribute1 = hours;
                    hrPaySlipTransactionLog.Attribute2 = minutes;
                    hrPaySlipTransactionLog.Attribute3 = payheadType.ToString();
                    unitOfWork.HrPaySlipTransactionLogsRepo.Add(hrPaySlipTransactionLog);
                    unitOfWork.SaveChanges();

                    // transaction log history table payhead insert
                    HrPaySlipTransactionLogHistory hrPaySlipTransactionLogHistory = new HrPaySlipTransactionLogHistory();
                    hrPaySlipTransactionLogHistory.HrPaySlipTransactionMasterLogId = hrPayMasterLogHistory.HrPaySlipTransactionMasterLogId; // master log table child insert
                    hrPaySlipTransactionLogHistory.PayHeadId = item.PayHeadId;
                    hrPaySlipTransactionLogHistory.Unit = item.Unit;
                    hrPaySlipTransactionLogHistory.CalculatedValue = calculatedValue;
                    hrPaySlipTransactionLogHistory.IsPercentage = item.IsPercentage;
                    hrPaySlipTransactionLogHistory.CreatedBy = userid;
                    hrPaySlipTransactionLogHistory.CreationDate = DateTime.Now;
                    hrPaySlipTransactionLogHistory.Attribute1 = hours;
                    hrPaySlipTransactionLogHistory.Attribute2 = minutes;
                    hrPaySlipTransactionLogHistory.Attribute3 = payheadType.ToString();
                    unitOfWork.HrPaySlipTransactionLogHistorysRepo.Add(hrPaySlipTransactionLogHistory);
                    unitOfWork.SaveChanges();

                    // master log table payhead change amount insert, for temporary use
                    HrPaySlipTransactionLogAfterPayheadChange hrPaySlipChangeLog = new HrPaySlipTransactionLogAfterPayheadChange();
                    hrPaySlipChangeLog.HrPaySlipTransactionMasterLogId = hrPayMasterLog.HrPaySlipTransactionMasterLogId; // master log table child insert
                    hrPaySlipChangeLog.PayHeadId = item.PayHeadId;
                    hrPaySlipChangeLog.Unit = item.Unit;
                    hrPaySlipChangeLog.CalculatedValue = calculatedValue;
                    hrPaySlipChangeLog.IsPercentage = item.IsPercentage;
                    hrPaySlipChangeLog.CreatedBy = userid;
                    hrPaySlipChangeLog.CreationDate = DateTime.Now;
                    hrPaySlipChangeLog.Attribute1 = hours;
                    hrPaySlipChangeLog.Attribute2 = minutes;
                    hrPaySlipChangeLog.Attribute3 = payheadType.ToString();
                    unitOfWork.HrPaySlipTransactionLogAfterPayheadChangesRepo.Add(hrPaySlipChangeLog);
                    unitOfWork.SaveChanges();

                    //calculate total addition & total deduction
                    if (thisPayhead != null)
                    {
                        if (thisPayhead.PayHeadType == PayHeadType.Addition)
                        {
                            totalAddition = totalAddition + calculatedValue;
                        }
                        else if (thisPayhead.PayHeadType == PayHeadType.Deduction)
                        {
                            totalDeduction = totalDeduction + calculatedValue;
                        }
                    }
                }
            }

            //update mastertables payable amounts
            var netSalary = totalAddition - totalDeduction;
            UpdateMasterPayslipTotalAmount(totalAddition, totalDeduction, empHeaderId, periodId, poisitionid, payrollid);
        }
        
        // generate again with employee's previous payheads from temporary log table, during regenerate the previous payheads amount can be change only, no new payhead cannot be add.. the timing fees will also remain the same as edited last time
        public bool ReGeneratePayslip(int empHeaderId, int periodId, int? userid, int poisitionid, int payrollid)
        {
            var PrevMasterData = unitOfWork.HrPaySlipTransactionMastersRepo.FirstOrDefault(w => w.PeriodId == periodId && w.EmpHeaderId == empHeaderId && w.PositionId == poisitionid && w.PayrollId == payrollid);

            if (PrevMasterData != null)
            {
                var latefeePayhead = unitOfWork.HrPayHeadsRepo.FirstOrDefault(s => s.PayHeadShortValue == "lf");
                var earlyOutPayhead = unitOfWork.HrPayHeadsRepo.FirstOrDefault(s => s.PayHeadShortValue == "eo");
                var overTimePayhead = unitOfWork.HrPayHeadsRepo.FirstOrDefault(s => s.PayHeadShortValue == "ot");

                // ********** update existing payslip and remove its previous child

                //Generate PaySlidId
                var paysilpId = GetNewPayslipId(empHeaderId);

                // update existing master payslip to regenerate
                PrevMasterData.TotalAmountPayable = 0; // the total amount will be update when a payslip regenerate is complete
                PrevMasterData.TotalAdditionAmount = 0;
                PrevMasterData.TotalDeductionAmount = 0;
                PrevMasterData.PaidStatus = PaidStatus.UnPaid;
                PrevMasterData.PayslipCurrentStatus = PayslipCurrentStatus.ReGenerated;
                PrevMasterData.PayslipId = paysilpId;
                PrevMasterData.LastUpdateBy = userid;
                PrevMasterData.LastUpdateDate = DateTime.Now;
                PrevMasterData.GenerationDate = DateTime.Now;
                unitOfWork.SaveChanges();

                // remove privious transaction payheads
                var payslipTransationChilds = PrevMasterData.HrPaySlipTransactions;
                if (payslipTransationChilds.Any())
                {
                    unitOfWork.HrPaySlipTransactionsRepo.RemoveRange(payslipTransationChilds);
                    unitOfWork.SaveChanges();
                }

                var FindPeriviousPayHeads = PrevMasterData.HrPaySlipTransactionMasterLog.OrderByDescending(s => s.HrPaySlipTransactionMasterLogId).FirstOrDefault();

                // add master transaction log, new master data copy
                HrPaySlipTransactionMasterLog hrPayMasterLog = new HrPaySlipTransactionMasterLog();
                hrPayMasterLog.HrPaySlipTransactionMasterLogId = 0;
                hrPayMasterLog.PaySlipMasterHeaderId = PrevMasterData.PaySlipMasterHeaderId; // relational insert
                hrPayMasterLog.PeriodId = PrevMasterData.PeriodId;
                hrPayMasterLog.EmpHeaderId = PrevMasterData.EmpHeaderId;
                hrPayMasterLog.PositionId = poisitionid;
                hrPayMasterLog.PayrollId = payrollid;
                hrPayMasterLog.TotalAmountPayable = 0; // the total amount will be update when a payslip regenerate is complete
                hrPayMasterLog.TotalAdditionAmount = 0;
                hrPayMasterLog.TotalDeductionAmount = 0;
                hrPayMasterLog.PaidStatus = PaidStatus.UnPaid;
                hrPayMasterLog.PayslipCurrentStatus = PayslipCurrentStatus.ReGenerated;
                hrPayMasterLog.PayslipId = paysilpId;
                hrPayMasterLog.CreatedBy = userid;
                hrPayMasterLog.CreationDate = DateTime.Now;
                hrPayMasterLog.LastUpdateBy = userid;
                hrPayMasterLog.LastUpdateDate = DateTime.Now;
                hrPayMasterLog.GenerationDate = DateTime.Now;
                unitOfWork.HrPaySlipTransactionMasterLogsRepo.Add(hrPayMasterLog);
                unitOfWork.SaveChanges();

                // add master transaction log history
                HrPaySlipTransactionMasterLogHistory hrPayMasterLogHistory = new HrPaySlipTransactionMasterLogHistory();
                hrPayMasterLogHistory.HrPaySlipTransactionMasterLogId = 0;
                hrPayMasterLogHistory.PaySlipMasterHeaderId = PrevMasterData.PaySlipMasterHeaderId; // relational insert
                hrPayMasterLogHistory.PeriodId = periodId;
                hrPayMasterLogHistory.EmpHeaderId = empHeaderId;
                hrPayMasterLogHistory.PositionId = poisitionid;
                hrPayMasterLogHistory.PayrollId = payrollid;
                hrPayMasterLogHistory.TotalAmountPayable = 0; // the total amount will be update when a payslip generate is complete
                hrPayMasterLogHistory.TotalAdditionAmount = 0;
                hrPayMasterLogHistory.TotalDeductionAmount = 0;
                hrPayMasterLogHistory.PaidStatus = PaidStatus.UnPaid;
                hrPayMasterLogHistory.PayslipCurrentStatus = PayslipCurrentStatus.Generated;
                hrPayMasterLogHistory.PayslipId = paysilpId;
                hrPayMasterLogHistory.CreatedBy = userid;
                hrPayMasterLogHistory.CreationDate = DateTime.Now;
                hrPayMasterLogHistory.LastUpdateBy = userid;
                hrPayMasterLogHistory.LastUpdateDate = DateTime.Now;
                hrPayMasterLogHistory.GenerationDate = DateTime.Now;
                unitOfWork.HrPaySlipTransactionMasterLogHistorysRepo.Add(hrPayMasterLogHistory);
                unitOfWork.SaveChanges();
                
                var basicSalaryId = unitOfWork.HrPayHeadsRepo.FirstOrDefault(s => s.PayHeadShortValue == "bs").PayHeadId;
                var thisEmpBasicSalary = FindPeriviousPayHeads.HrPaySlipTransactionLogAfterPayheadChanges.FirstOrDefault(s => s.PayHeadId == basicSalaryId).Unit;
                var payheadData = unitOfWork.HrPayHeadsRepo.GetAll();

                decimal totalAddition = 0;
                decimal totalDeduction = 0;
                // add new transaction payheads from log table, as the previous payhead amount can be updated during regenerate
                foreach (var item in FindPeriviousPayHeads.HrPaySlipTransactionLogAfterPayheadChanges)
                {
                    var thisPayhead = payheadData.FirstOrDefault(f => f.PayHeadId == item.PayHeadId);
                    var calculatedValue = item.Unit;
                    if (item.IsPercentage)
                        calculatedValue = (thisEmpBasicSalary / 100) * item.Unit;

                    HrPaySlipTransaction hrPaySlipTransaction = new HrPaySlipTransaction();
                    hrPaySlipTransaction.PayHeadId = item.PayHeadId;
                    hrPaySlipTransaction.Unit = item.Unit;
                    hrPaySlipTransaction.CalculatedValue = item.CalculatedValue;
                    hrPaySlipTransaction.IsPercentage = item.IsPercentage;
                    hrPaySlipTransaction.CreatedBy = userid;
                    hrPaySlipTransaction.CreationDate = DateTime.Now;
                    hrPaySlipTransaction.Attribute1 = item.Attribute1;
                    hrPaySlipTransaction.Attribute2 = item.Attribute2;
                    hrPaySlipTransaction.Attribute3 = item.Attribute3; // payheadType
                    hrPaySlipTransaction.PaySlipMasterHeaderId = PrevMasterData.PaySlipMasterHeaderId;
                    unitOfWork.HrPaySlipTransactionsRepo.Add(hrPaySlipTransaction);
                    unitOfWork.SaveChanges();

                    // transaction log table payhead insert
                    HrPaySlipTransactionLog hrPaySlipTransactionLog = new HrPaySlipTransactionLog();
                    hrPaySlipTransactionLog.HrPaySlipTransactionMasterLogId = hrPayMasterLog.HrPaySlipTransactionMasterLogId; // master log table child insert
                    hrPaySlipTransactionLog.PayHeadId = item.PayHeadId;
                    hrPaySlipTransactionLog.Unit = item.Unit;
                    hrPaySlipTransactionLog.CalculatedValue = calculatedValue;
                    hrPaySlipTransactionLog.IsPercentage = item.IsPercentage;
                    hrPaySlipTransactionLog.CreatedBy = userid;
                    hrPaySlipTransactionLog.CreationDate = DateTime.Now;
                    hrPaySlipTransactionLog.Attribute1 = item.Attribute1;
                    hrPaySlipTransactionLog.Attribute2 = item.Attribute2;
                    hrPaySlipTransactionLog.Attribute3 = item.Attribute3; // payheadType
                    unitOfWork.HrPaySlipTransactionLogsRepo.Add(hrPaySlipTransactionLog);
                    unitOfWork.SaveChanges();

                    // transaction log history table payhead insert
                    HrPaySlipTransactionLogHistory hrPaySlipTransactionLogHistory = new HrPaySlipTransactionLogHistory();
                    hrPaySlipTransactionLogHistory.HrPaySlipTransactionMasterLogId = hrPayMasterLogHistory.HrPaySlipTransactionMasterLogId; // master log table child insert
                    hrPaySlipTransactionLogHistory.PayHeadId = item.PayHeadId;
                    hrPaySlipTransactionLogHistory.Unit = item.Unit;
                    hrPaySlipTransactionLogHistory.CalculatedValue = calculatedValue;
                    hrPaySlipTransactionLogHistory.IsPercentage = item.IsPercentage;
                    hrPaySlipTransactionLogHistory.CreatedBy = userid;
                    hrPaySlipTransactionLogHistory.CreationDate = DateTime.Now;
                    hrPaySlipTransactionLogHistory.Attribute1 = item.Attribute1;
                    hrPaySlipTransactionLogHistory.Attribute2 = item.Attribute2;
                    hrPaySlipTransactionLogHistory.Attribute3 = item.Attribute3; // payheadType
                    unitOfWork.HrPaySlipTransactionLogHistorysRepo.Add(hrPaySlipTransactionLogHistory);
                    unitOfWork.SaveChanges();

                    // transaction log table payhead change amount insert, for temporary use
                    HrPaySlipTransactionLogAfterPayheadChange hrPaySlipChangeLog = new HrPaySlipTransactionLogAfterPayheadChange();
                    hrPaySlipChangeLog.HrPaySlipTransactionMasterLogId = hrPayMasterLog.HrPaySlipTransactionMasterLogId; // master log table child insert
                    hrPaySlipChangeLog.PayHeadId = item.PayHeadId;
                    hrPaySlipChangeLog.Unit = item.Unit;
                    hrPaySlipChangeLog.CalculatedValue = calculatedValue;
                    hrPaySlipChangeLog.IsPercentage = item.IsPercentage;
                    hrPaySlipChangeLog.CreatedBy = userid;
                    hrPaySlipChangeLog.CreationDate = DateTime.Now;
                    hrPaySlipChangeLog.Attribute1 = item.Attribute1;
                    hrPaySlipChangeLog.Attribute2 = item.Attribute2;
                    hrPaySlipChangeLog.Attribute3 = item.Attribute3; // payheadType
                    unitOfWork.HrPaySlipTransactionLogAfterPayheadChangesRepo.Add(hrPaySlipChangeLog);
                    unitOfWork.SaveChanges();

                    //calculate total addition & total deduction
                    if (thisPayhead != null)
                    {
                        if (thisPayhead.PayHeadType == PayHeadType.Addition)
                        {
                            totalAddition = totalAddition + calculatedValue;
                        }
                        else if (thisPayhead.PayHeadType == PayHeadType.Deduction)
                        {
                            totalDeduction = totalDeduction + calculatedValue;
                        }
                    }
                }
                //update mastertables payable amounts
                var netSalary = totalAddition - totalDeduction;
                UpdateMasterPayslipTotalAmount(totalAddition, totalDeduction, empHeaderId, periodId, poisitionid, payrollid);
                return true;
            }
            else
            {
                return false;
            }
        }

        // only recalculate late fee,early out, overtime fee if permission enabled and leave existing data as it was, update master tables datas
        public bool UpdatePayslip(int empHeaderId, int periodId, int? userid, int poisitionid, int payrollid)
        {
            var PrevMasterData = unitOfWork.HrPaySlipTransactionMastersRepo.FirstOrDefault(w => w.PeriodId == periodId && w.EmpHeaderId == empHeaderId && w.PositionId == poisitionid && w.PayrollId == payrollid);

            if (PrevMasterData != null)
            {
                var latefeePayhead = unitOfWork.HrPayHeadsRepo.FirstOrDefault(s => s.PayHeadShortValue == "lf");
                var earlyOutPayhead = unitOfWork.HrPayHeadsRepo.FirstOrDefault(s => s.PayHeadShortValue == "eo");
                var overTimePayhead = unitOfWork.HrPayHeadsRepo.FirstOrDefault(s => s.PayHeadShortValue == "ot");
                
                // ********** update existing payslip data
                
                // update existing master payslip to regenerate
                PrevMasterData.TotalAmountPayable = 0; // the total amount will be update when a payslip update is complete
                PrevMasterData.TotalAdditionAmount = 0;
                PrevMasterData.TotalDeductionAmount = 0;
                PrevMasterData.PayslipCurrentStatus = PayslipCurrentStatus.Updated;
                PrevMasterData.LastUpdateBy = userid;
                PrevMasterData.LastUpdateDate = DateTime.Now;
                PrevMasterData.GenerationDate = DateTime.Now;
                unitOfWork.SaveChanges();
                
                var FindPeriviousPayHeads = PrevMasterData.HrPaySlipTransactions;

                // add master transaction log, new master data copy
                HrPaySlipTransactionMasterLog hrPayMasterLog = new HrPaySlipTransactionMasterLog();
                hrPayMasterLog.HrPaySlipTransactionMasterLogId = 0;
                hrPayMasterLog.PaySlipMasterHeaderId = PrevMasterData.PaySlipMasterHeaderId; // relational insert
                hrPayMasterLog.PeriodId = PrevMasterData.PeriodId;
                hrPayMasterLog.EmpHeaderId = PrevMasterData.EmpHeaderId;
                hrPayMasterLog.PositionId = poisitionid;
                hrPayMasterLog.PayrollId = payrollid;
                hrPayMasterLog.TotalAmountPayable = 0; // the total amount will be update when a payslip update is complete
                hrPayMasterLog.TotalAdditionAmount = 0;
                hrPayMasterLog.TotalDeductionAmount = 0;
                hrPayMasterLog.PaidStatus = PrevMasterData.PaidStatus;
                hrPayMasterLog.PayslipCurrentStatus = PrevMasterData.PayslipCurrentStatus;
                hrPayMasterLog.PayslipId = PrevMasterData.PayslipId;
                hrPayMasterLog.CreatedBy = userid;
                hrPayMasterLog.CreationDate = DateTime.Now;
                hrPayMasterLog.LastUpdateBy = userid;
                hrPayMasterLog.LastUpdateDate = DateTime.Now;
                hrPayMasterLog.GenerationDate = DateTime.Now;
                unitOfWork.HrPaySlipTransactionMasterLogsRepo.Add(hrPayMasterLog);
                unitOfWork.SaveChanges();

                // add master transaction log history
                HrPaySlipTransactionMasterLogHistory hrPayMasterLogHistory = new HrPaySlipTransactionMasterLogHistory();
                hrPayMasterLogHistory.HrPaySlipTransactionMasterLogId = 0;
                hrPayMasterLogHistory.PaySlipMasterHeaderId = PrevMasterData.PaySlipMasterHeaderId; // relational insert
                hrPayMasterLogHistory.PeriodId = periodId;
                hrPayMasterLogHistory.EmpHeaderId = empHeaderId;
                hrPayMasterLogHistory.PositionId = poisitionid;
                hrPayMasterLogHistory.PayrollId = payrollid;
                hrPayMasterLogHistory.TotalAmountPayable = 0; // the total amount will be update when a payslip generate is complete
                hrPayMasterLogHistory.TotalAdditionAmount = 0;
                hrPayMasterLogHistory.TotalDeductionAmount = 0;
                hrPayMasterLogHistory.PaidStatus = PrevMasterData.PaidStatus;
                hrPayMasterLogHistory.PayslipCurrentStatus = PrevMasterData.PayslipCurrentStatus;
                hrPayMasterLogHistory.PayslipId = PrevMasterData.PayslipId;
                hrPayMasterLogHistory.CreatedBy = userid;
                hrPayMasterLogHistory.CreationDate = DateTime.Now;
                hrPayMasterLogHistory.LastUpdateBy = userid;
                hrPayMasterLogHistory.LastUpdateDate = DateTime.Now;
                hrPayMasterLogHistory.GenerationDate = DateTime.Now;
                unitOfWork.HrPaySlipTransactionMasterLogHistorysRepo.Add(hrPayMasterLogHistory);
                unitOfWork.SaveChanges();

                var basicSalaryId = unitOfWork.HrPayHeadsRepo.FirstOrDefault(s => s.PayHeadShortValue == "bs").PayHeadId;
                var thisEmpBasicSalary = FindPeriviousPayHeads.FirstOrDefault(s => s.PayHeadId == basicSalaryId).Unit;
                var payheadData = unitOfWork.HrPayHeadsRepo.GetAll();

                decimal totalAddition = 0;
                decimal totalDeduction = 0;
                // add new transaction payheads from log table, as the previous payhead amount can be updated during regenerate
                foreach (var item in FindPeriviousPayHeads)
                {
                    var thisPayhead = payheadData.FirstOrDefault(f => f.PayHeadId == item.PayHeadId);
                    var allowUpdate = false;
                    var calculatedValue = item.Unit;
                    var hours = "";
                    var minutes = "";
                    if (latefeePayhead != null && item.PayHeadId == latefeePayhead.PayHeadId) //if its late fee
                    {
                        // no permission check need
                        var getfeedata = paySlipLateFee.GetLateFee(empHeaderId, periodId);
                        calculatedValue = Convert.ToInt16(getfeedata.Fee);
                        hours = getfeedata.Hours;
                        minutes = getfeedata.Minutes;
                        allowUpdate = true;
                    }
                    else if (earlyOutPayhead != null && item.PayHeadId == earlyOutPayhead.PayHeadId) // if its early out
                    {
                        // no permission check need
                        var getfeedata = paySlipEarlyOutFee.GetEarlyOutFee(empHeaderId, periodId);
                        calculatedValue = Convert.ToInt16(getfeedata.Fee);
                        hours = getfeedata.Hours;
                        minutes = getfeedata.Minutes;
                        allowUpdate = true;
                    }
                    else if (overTimePayhead != null && item.PayHeadId == overTimePayhead.PayHeadId)// if its overtime
                    {
                        // no permission check need
                        var getfeedata = paySlipOverTime.GetOvertimeFee(empHeaderId, periodId);
                        calculatedValue = Convert.ToInt16(getfeedata.Fee);
                        hours = getfeedata.Hours;
                        minutes = getfeedata.Minutes;
                        allowUpdate = true;
                    }
                    
                    item.PayHeadId = item.PayHeadId;
                    item.Unit = item.Unit;
                    item.CalculatedValue = allowUpdate == true ? calculatedValue : item.CalculatedValue;
                    item.Attribute1 = allowUpdate == true ? hours : item.Attribute1;
                    item.Attribute2 = allowUpdate == true ? minutes : item.Attribute2;
                    item.Attribute3 = item.Attribute3; // payheadType
                    unitOfWork.SaveChanges();

                    // transaction log table payhead insert
                    HrPaySlipTransactionLog hrPaySlipTransactionLog = new HrPaySlipTransactionLog();
                    hrPaySlipTransactionLog.HrPaySlipTransactionMasterLogId = hrPayMasterLog.HrPaySlipTransactionMasterLogId; // master log table child insert
                    hrPaySlipTransactionLog.PayHeadId = item.PayHeadId;
                    hrPaySlipTransactionLog.Unit = item.Unit;
                    hrPaySlipTransactionLog.CalculatedValue = item.CalculatedValue;
                    hrPaySlipTransactionLog.IsPercentage = item.IsPercentage;
                    hrPaySlipTransactionLog.CreatedBy = userid;
                    hrPaySlipTransactionLog.CreationDate = DateTime.Now;
                    hrPaySlipTransactionLog.Attribute1 = item.Attribute1; // hour
                    hrPaySlipTransactionLog.Attribute2 = item.Attribute2; // minute
                    hrPaySlipTransactionLog.Attribute3 = item.Attribute3; // payheadType
                    unitOfWork.HrPaySlipTransactionLogsRepo.Add(hrPaySlipTransactionLog);
                    unitOfWork.SaveChanges();

                    // transaction log history table payhead insert
                    HrPaySlipTransactionLogHistory hrPaySlipTransactionLogHistory = new HrPaySlipTransactionLogHistory();
                    hrPaySlipTransactionLogHistory.HrPaySlipTransactionMasterLogId = hrPayMasterLogHistory.HrPaySlipTransactionMasterLogId; // master log table child insert
                    hrPaySlipTransactionLogHistory.PayHeadId = item.PayHeadId;
                    hrPaySlipTransactionLogHistory.Unit = item.Unit;
                    hrPaySlipTransactionLogHistory.CalculatedValue = item.CalculatedValue;
                    hrPaySlipTransactionLogHistory.IsPercentage = item.IsPercentage;
                    hrPaySlipTransactionLogHistory.CreatedBy = userid;
                    hrPaySlipTransactionLogHistory.CreationDate = DateTime.Now;
                    hrPaySlipTransactionLogHistory.Attribute1 = item.Attribute1; // hour
                    hrPaySlipTransactionLogHistory.Attribute2 = item.Attribute2; // minute
                    hrPaySlipTransactionLogHistory.Attribute3 = item.Attribute3; // payheadType
                    unitOfWork.HrPaySlipTransactionLogHistorysRepo.Add(hrPaySlipTransactionLogHistory);
                    unitOfWork.SaveChanges();
                    
                    //calculate total addition & total deduction
                    if (thisPayhead != null)
                    {
                        if (thisPayhead.PayHeadType == PayHeadType.Addition)
                            totalAddition = totalAddition + item.CalculatedValue;
                        else if (thisPayhead.PayHeadType == PayHeadType.Deduction)
                            totalDeduction = totalDeduction + item.CalculatedValue;
                    }
                }
                
                //update mastertables payable amounts
                var netSalary = totalAddition - totalDeduction;
                UpdateMasterPayslipTotalAmount(totalAddition, totalDeduction, empHeaderId, periodId, poisitionid, payrollid);
                return true;
            }
            else
                return false;
        }

        //update master data, remove every child table data without history tables
        public bool CancelPayslip(int empHeaderId, int periodId, int? userid, int poisitionid, int payrollid)
        {
            var PrevMasterData = unitOfWork.HrPaySlipTransactionMastersRepo.FirstOrDefault(w => w.PeriodId == periodId && w.EmpHeaderId == empHeaderId && w.PositionId == poisitionid && w.PayrollId == payrollid);
            var payheadData = unitOfWork.HrPayHeadsRepo.GetAll();
            if (PrevMasterData != null)
            {
                //Generate PaySlidId
                var paysilpId = GetNewPayslipId(empHeaderId);

                // update existing master payslip
                PrevMasterData.TotalAmountPayable = 0; // the total amount will be update when a payslip cancel is complete
                PrevMasterData.PaidStatus = PaidStatus.UnPaid;
                PrevMasterData.PayslipCurrentStatus = PayslipCurrentStatus.Canceled;
                PrevMasterData.PayslipId = paysilpId;
                PrevMasterData.LastUpdateBy = userid;
                PrevMasterData.LastUpdateDate = DateTime.Now;
                PrevMasterData.GenerationDate = DateTime.Now;
                unitOfWork.SaveChanges();

                //********* store history data (payslip full data during cancel)
                var PeriviousTransactions = PrevMasterData.HrPaySlipTransactions;
                
                // add master transaction log history
                HrPaySlipTransactionMasterLogHistory hrPayMasterLogHistory = new HrPaySlipTransactionMasterLogHistory();
                hrPayMasterLogHistory.HrPaySlipTransactionMasterLogId = 0;
                hrPayMasterLogHistory.PaySlipMasterHeaderId = PrevMasterData.PaySlipMasterHeaderId; // relational insert
                hrPayMasterLogHistory.PeriodId = periodId;
                hrPayMasterLogHistory.EmpHeaderId = empHeaderId;
                hrPayMasterLogHistory.PositionId = poisitionid;
                hrPayMasterLogHistory.PayrollId = payrollid;
                hrPayMasterLogHistory.TotalAmountPayable = 0; // the total amount will be update when a payslip generate is complete
                hrPayMasterLogHistory.PaidStatus = PaidStatus.UnPaid;
                hrPayMasterLogHistory.PayslipCurrentStatus = PayslipCurrentStatus.Canceled;
                hrPayMasterLogHistory.PayslipId = paysilpId;
                hrPayMasterLogHistory.CreatedBy = userid;
                hrPayMasterLogHistory.CreationDate = DateTime.Now;
                hrPayMasterLogHistory.GenerationDate = DateTime.Now;
                unitOfWork.HrPaySlipTransactionMasterLogHistorysRepo.Add(hrPayMasterLogHistory);
                unitOfWork.SaveChanges();

                if (PeriviousTransactions.Any())
                {
                    // add transaction payheads from HrPaySlipTransactionLog table
                    foreach (var item in PeriviousTransactions)
                    {
                        var thisPayhead = payheadData.FirstOrDefault(f => f.PayHeadId == item.PayHeadId);
                        // transaction log history table payhead insert
                        HrPaySlipTransactionLogHistory hrPaySlipTransactionLogHistory = new HrPaySlipTransactionLogHistory();
                        hrPaySlipTransactionLogHistory.HrPaySlipTransactionMasterLogId = hrPayMasterLogHistory.HrPaySlipTransactionMasterLogId; // master log table child insert
                        hrPaySlipTransactionLogHistory.PayHeadId = item.PayHeadId;
                        hrPaySlipTransactionLogHistory.Unit = item.Unit;
                        hrPaySlipTransactionLogHistory.IsPercentage = item.IsPercentage;
                        hrPaySlipTransactionLogHistory.CreatedBy = userid;
                        hrPaySlipTransactionLogHistory.CreationDate = DateTime.Now;
                        hrPaySlipTransactionLogHistory.Attribute1 = item.Attribute1;
                        hrPaySlipTransactionLogHistory.Attribute2 = item.Attribute2;
                        hrPaySlipTransactionLogHistory.Attribute3 = item.Attribute3; // payheadType
                        unitOfWork.HrPaySlipTransactionLogHistorysRepo.Add(hrPaySlipTransactionLogHistory);
                        unitOfWork.SaveChanges();
                    }
                }
                //********* end store history data

                //******** remove all previous data except master table
                // remove privious transaction payheads
                var payslipTransation = PrevMasterData.HrPaySlipTransactions;
                if (payslipTransation.Any())
                {
                    unitOfWork.HrPaySlipTransactionsRepo.RemoveRange(payslipTransation);
                    unitOfWork.SaveChanges();
                }

                // remove log master tables all child data
                var payslipTransactionlog = PrevMasterData.HrPaySlipTransactionMasterLog;
                foreach (var item in payslipTransactionlog)
                {
                    var masterLogTransactionsAllTransactionlog = item.HrPaySlipTransactionLog;
                    if (masterLogTransactionsAllTransactionlog.Any())
                    {
                        unitOfWork.HrPaySlipTransactionLogsRepo.RemoveRange(masterLogTransactionsAllTransactionlog);
                        unitOfWork.SaveChanges();
                    }
                }
                //remove log master table data
                if (payslipTransactionlog.Any())
                {
                    unitOfWork.HrPaySlipTransactionMasterLogsRepo.RemoveRange(payslipTransactionlog);
                    unitOfWork.SaveChanges();
                }
                //******** end remove all previous data except master table
                return true;
            }
            else
            {
                return false;
            }
        }

        public string GetNewPayslipId(int empHeaderId)
        {
            var now = BdDateTime.Now();
            var day = now.Day.ToString();
            var Month = now.Month.ToString();
            var year = now.Year.ToString();
            var hour = now.Hour.ToString();
            var empId = unitOfWork.EmployeeRepo.Get(empHeaderId) != null ? unitOfWork.EmployeeRepo.Get(empHeaderId).EmpId.ToString() : "";
            return empId + '-' + year + Month + '-' + day + hour + "_" + now.Millisecond.ToString();
        }

        // update total payable amount after a pay slip generate or regenerate
        public void UpdateMasterPayslipTotalAmount(decimal totalAddition, decimal totalDeduction, int? employeeheaderid, int? periodid, int positionId, int payrollId)
        {
            if (employeeheaderid != null && periodid != null)
            {
                var netsalary = totalAddition - totalDeduction;
                var masterdata = unitOfWork.HrPaySlipTransactionMastersRepo.FirstOrDefault(s => s.EmpHeaderId == employeeheaderid && s.PeriodId == periodid && s.PositionId == positionId && s.PayrollId == payrollId);
                if (masterdata != null)
                {
                    masterdata.TotalAmountPayable = netsalary;
                    masterdata.TotalAdditionAmount = totalAddition;
                    masterdata.TotalDeductionAmount = totalDeduction;
                    unitOfWork.SaveChanges();

                    var masterLogData = masterdata.HrPaySlipTransactionMasterLog.OrderByDescending(s => s.HrPaySlipTransactionMasterLogId).FirstOrDefault(); // update only the last log row, as the latest log always be the last.
                    if (masterLogData != null)
                    {
                        masterLogData.TotalAmountPayable = netsalary;
                        masterLogData.TotalAdditionAmount = totalAddition;
                        masterLogData.TotalDeductionAmount = totalDeduction;
                        unitOfWork.SaveChanges();
                    }

                    var masterLogHistoryData = masterdata.GetHrPaySlipTransactionMasterLogHistories.OrderByDescending(s => s.HrPaySlipTransactionMasterLogId).FirstOrDefault(); // update only the last log row, as the latest log always be the last.
                    if (masterLogHistoryData != null)
                    {
                        masterLogHistoryData.TotalAmountPayable = netsalary;
                        masterLogHistoryData.TotalAdditionAmount = totalAddition;
                        masterLogHistoryData.TotalDeductionAmount = totalDeduction;
                        unitOfWork.SaveChanges();
                    }
                }
            }
        }
    }
}
