﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tenant.Entities
{
    public class MultitenantProcess
    {
        [Key]
        public int ProcessHeaderId { get; set; }
        public string ProcessName { get; set; }
        public bool? IsActive { get; set; }
    }
}
