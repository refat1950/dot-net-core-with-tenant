﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class ProcessLog
    {
        [Key]
        public int ProcessLogId { get; set; }
        public DateTime ProcessTime { get; set; }
        public int ClassId { get; set; }
        public int SessionId { get; set; }
        public int EventId { get; set; }
    }
}
