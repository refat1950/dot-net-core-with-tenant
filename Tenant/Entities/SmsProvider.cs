﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tenant.Entities
{
    public class SmsProvider
    {
        [Key]
        public int ProviderId { get; set; }
        public string ProviderName { get; set; }
        public string ProviderUrl { get; set; }
    }
}
