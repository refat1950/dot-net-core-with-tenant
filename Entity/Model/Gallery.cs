using System;
using System.ComponentModel.DataAnnotations;

namespace Entity.Model
{
    public partial class Gallery : BaseEntity
    {
        [Key]
        public int GalleryId { get; set; }

        [StringLength(100)]
        public string GalleryTitle { get; set; }

        public int? GalleryFor { get; set; }

        public int? ReferenceId { get; set; }

        public int? GalleryStatus { get; set; }


    }
}
