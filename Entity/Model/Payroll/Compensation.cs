﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class Compensation : BaseEntity
    {
        [Key]
        public int CompensationId { get; set; }
        public int EmployeeHeaderId { get; set; }
        public string Time { get; set; }
        public DateTime ApprovalDate { get; set; }
        public decimal Rate { get; set; }
    }
}
