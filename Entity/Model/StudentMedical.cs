using System;
using System.ComponentModel.DataAnnotations;

namespace Entity.Model
{
    public partial class StudentMedical : BaseEntity
    {
        [Key]
        public int StudentMedId { get; set; }

        public int? ReferenceId { get; set; }

        public int? MedicalId { get; set; }

        [StringLength(200)]
        public string EmergencyMedAction { get; set; }


    }
}
