﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class ReportProcess
    {
        [Key]
        public int ReportProcessId { get; set; }

        public int ProcessLogId { get; set; }
        public int SessionId { get; set; }
        public int StudentId { get; set; }
        public int SubjectId { get; set; }
        public decimal AssesmentMark { get; set; }
        public decimal SemisterMark { get; set; }

        public decimal TotalMark { get; set; }

    }
}
