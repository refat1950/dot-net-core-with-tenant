﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class BillPeriod : BaseEntity
    {
        [Key]
        public int PeriodId { get; set; }
        public string PeriodName { get; set; }
        public BillPeriodType? PeriodTypeId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int MonthId { get; set; }
        public int Year { get; set; }
        public bool IsVisible { get; set; }
        public bool? Status { get; set; }
    }
    public enum BillPeriodType
    {
        Annual = 1,
        Bi_Annual = 2,
        Tri_Annual = 3,
        Quarterly = 4,
        Monthly = 5,
        One_Time = 6,
        Weekly = 7
    }
}
