﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class ClassExamGrade : BaseEntity
    {
        [Key]
        public int ClassGradeId { get; set; }
        public int ClassId { get; set; }
        public int GradeId { get; set; }
        public string AltName { get; set; }

        public virtual ExamGrade ExamGrade { get; set; }
        public virtual Class Class { get; set; }
    }
}
