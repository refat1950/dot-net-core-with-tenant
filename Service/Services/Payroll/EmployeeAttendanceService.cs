﻿using data.UnitOfWork;
using Entity.Model;
using Repository.Context;
using Service.DataService;
using Service.Helper;
using Service.Services;
using Service.ViewModel;
using Service.ViewModel.Payroll;
using Services.CoreServices;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
namespace Service.Services.Payroll
{
   

    public class EmployeeAttendanceService : IEmployeeAttendanceService
    {
        private readonly IUnitOfWork unitOfWork;
        private FlexValueService flexValueService;
        public EmployeeAttendanceService(IUnitOfWork unitOfWork, FlexValueService flexValueService)
        {
            this.unitOfWork = unitOfWork;
            this.flexValueService = flexValueService;

        }
        public List<VmDateWiseAttendance> GetBetween(int start, int displayLength, string searchValue, string selectedDate, out int totalLength)
        {
            var result = new List<VmDateWiseAttendance>();
            var date = Convert.ToDateTime(DateTimeHelperService.ConvertBDDateStringToDateTimeObject(selectedDate));
            var holidayid = flexValueService.GetFlexValueIdByFlexShortValue("holiday");
            var isHoliday = unitOfWork.EventAndHolidayPlannersRepo.Find(w => w.EventTypeId == holidayid && ((w.EndDate == null && w.StartDate == date) || (w.EndDate != null && w.StartDate <= date && w.EndDate >= date))).Any();

            var DefaultWeekendDays = new List<int>(); // weekends list from default settings
            var weekdayFlex = unitOfWork.FlexValueRepo.Find(f => f.FlexShortValue == "WeekendDay").FirstOrDefault();
            DefaultWeekendDays = weekdayFlex != null ? unitOfWork.FlexValueRepo.Find(f => f.ParentFlexValueId == weekdayFlex.FlexValueId).Select(s => Convert.ToInt32(s.Attribute1)).ToList() : new List<int>();
            if (!DefaultWeekendDays.Any())
                DefaultWeekendDays.Add((int)DayOfWeek.Friday);

            var LateCountAfter = unitOfWork.FlexValueRepo.Find(f => f.FlexShortValue == "LateCountAfter").FirstOrDefault() != null ? Convert.ToInt16(unitOfWork.FlexValueRepo.Find(f => f.FlexShortValue == "LateCountAfter").FirstOrDefault().Attribute1) : 0;
            var EarlyOutBefore = unitOfWork.FlexValueRepo.Find(f => f.FlexShortValue == "EarlyOutBefore").FirstOrDefault() != null ? Convert.ToInt16(unitOfWork.FlexValueRepo.Find(f => f.FlexShortValue == "EarlyOutBefore").FirstOrDefault().Attribute1) : 0;
            var teacherList = unitOfWork.EmployeeRepo.GetAll().Select(i => new VmSelectEmployee
            {
                PunchCardId = i.PunchCardId,
                DeviceId = i.DeviceId,
                EmpHeaderId = i.EmpHeaderId,
                EmpId = i.EmpId,
                DesignationId = i.DesignationId,
                EmpName = i.EmpFirstName + " " + i.EmpMiddleName + " " + i.EmpLastName,
                EmployeeOfficeTime = i.EmployeeOfficeTime,
                EmployeeWeekends = i.EmployeeWeekends.Select(s => s.Day).ToList()
            }).ToList();
            var attendanceList = unitOfWork.AttendanceProcessesRepo.GetAllQueryable().Select(i => new VmAttendanceSelect
            {
                AttendanceId = i.Id,
                PunchDate = i.PunchDate,
                PunchTime = i.PunchTime,
                EmpHeaderId = i.EmpHeaderId,
            }).Where(w => CoreServices.TruncateTime(w.PunchDate) == date);
            foreach (var teacher in teacherList)
            {
                var teacherAttendance = attendanceList.Where(w => w.EmpHeaderId == teacher.EmpHeaderId);
                var data = new VmDateWiseAttendance();

                data.EmpHeaderId = teacher.EmpHeaderId;
                data.EmpId = teacher.EmpId;
                data.Designation = flexValueService.GetFlexValueByFlexValueId(teacher.DesignationId ?? 0);
                data.TeacherName = teacher.EmpFirstName + " " + teacher.EmpMiddleName + " " + teacher.EmpLastName;
                data.StartTime = teacher.EmployeeOfficeTime.Where(w => w.EffectiveFrom <= date && (w.EffectiveTo >= date || w.EffectiveTo == null)).Any() ? teacher.EmployeeOfficeTime.FirstOrDefault(w => w.EffectiveFrom <= date && (w.EffectiveTo >= date || w.EffectiveTo == null)).StartTime : teacher.EmployeeOfficeTime.Any() ? teacher.EmployeeOfficeTime.FirstOrDefault().StartTime : "Not Set";
                data.EndTime = teacher.EmployeeOfficeTime.Where(w => w.EffectiveFrom <= date && (w.EffectiveTo >= date || w.EffectiveTo == null)).Any() ? teacher.EmployeeOfficeTime.FirstOrDefault(w => w.EffectiveFrom <= date && (w.EffectiveTo >= date || w.EffectiveTo == null)).EndTime : teacher.EmployeeOfficeTime.Any() ? teacher.EmployeeOfficeTime.FirstOrDefault().EndTime : "Not Set";

                data.InTime = unitOfWork.AttendanceEditHistorysRepo.FirstOrDefault(s => s.EmployeeHeaderId == teacher.EmpHeaderId && s.Date == date && s.InTime != null && s.isActive == true) != null ? unitOfWork.AttendanceEditHistorysRepo.FirstOrDefault(s => s.EmployeeHeaderId == teacher.EmpHeaderId && s.Date == date && s.InTime != null && s.isActive == true).InTime : teacherAttendance.Any() ? teacherAttendance.FirstOrDefault().PunchTime : "";

                data.OutTime = unitOfWork.AttendanceEditHistorysRepo.FirstOrDefault(s => s.EmployeeHeaderId == teacher.EmpHeaderId && s.Date == date && s.OutTime != null && s.isActive == true) != null ? unitOfWork.AttendanceEditHistorysRepo.FirstOrDefault(s => s.EmployeeHeaderId == teacher.EmpHeaderId && s.Date == date && s.OutTime != null && s.isActive == true).OutTime : teacherAttendance.Any() ? teacherAttendance.OrderByDescending(o => o.AttendanceId).FirstOrDefault().PunchTime : "";

                if (data.InTime != "" && data.OutTime != "")
                {
                    data.WorkingHour = TimeSpan.FromMinutes(DateTime.Parse(data.OutTime).Subtract(DateTime.Parse(data.InTime)).TotalMinutes).ToString(@"hh\:mm");
                }
                else
                {
                    data.WorkingHour = "";
                }
                data.TotalPaunch = teacherAttendance.Count();

                var weekendDays = teacher.EmployeeWeekends.Any() ? teacher.EmployeeWeekends : DefaultWeekendDays;
                var isWeekend = weekendDays.Any(weekday => (int)date.DayOfWeek == weekday);
                data.AttendanceType = GetAttendanceType(isHoliday, date, teacher, teacherAttendance, isWeekend, LateCountAfter, EarlyOutBefore);
                result.Add(data);
            }
            if (!string.IsNullOrEmpty(searchValue))
            {
                result = result.Where(w => (!string.IsNullOrEmpty(w.TeacherName) && w.TeacherName.ToLower().Contains(searchValue.ToLower())) || (!string.IsNullOrEmpty(w.Designation) && w.Designation.ToLower().Contains(searchValue.ToLower())) || (!string.IsNullOrEmpty(w.InTime) && w.InTime.ToLower().Contains(searchValue.ToLower())) || (!string.IsNullOrEmpty(w.OutTime) && w.OutTime.ToLower().Contains(searchValue.ToLower())) || (!string.IsNullOrEmpty(w.AttendanceType) && w.AttendanceType.ToLower().Contains(searchValue.ToLower()))).ToList();
            }
            var displayedValues = displayLength == -1 ? result
                .OrderByDescending(o => o.EmpHeaderId).Skip(start).ToList() : result
                .OrderByDescending(o => o.EmpHeaderId).Skip(start)
                .Take(displayLength).ToList();
            totalLength = result.Count;
            return displayedValues;
        }

        public List<VmDateWiseAttendance> GetDayAttendance(string selectedDate)
        {
            var result = new List<VmDateWiseAttendance>();
            var date = Convert.ToDateTime(DateTimeHelperService.ConvertBDDateStringToDateTimeObject(selectedDate));
            var holidayid = flexValueService.GetFlexValueIdByFlexShortValue("holiday");
            var isHoliday = unitOfWork.EventAndHolidayPlannersRepo.Find(w => w.EventTypeId == holidayid && ((w.EndDate == null && w.StartDate == date) || (w.EndDate != null && w.StartDate <= date && w.EndDate >= date))).Any();

            var DefaultWeekendDays = new List<int>(); // weekends list from default settings
            var weekdayFlex = unitOfWork.FlexValueRepo.Find(f => f.FlexShortValue == "WeekendDay").FirstOrDefault();
            DefaultWeekendDays = weekdayFlex != null ? unitOfWork.FlexValueRepo.Find(f => f.ParentFlexValueId == weekdayFlex.FlexValueId).Select(s => Convert.ToInt32(s.Attribute1)).ToList() : new List<int>();
            if (!DefaultWeekendDays.Any())
                DefaultWeekendDays.Add((int)DayOfWeek.Friday);

            var LateCountAfter = unitOfWork.FlexValueRepo.Find(f => f.FlexShortValue == "LateCountAfter").FirstOrDefault() != null ? Convert.ToInt16(unitOfWork.FlexValueRepo.Find(f => f.FlexShortValue == "LateCountAfter").FirstOrDefault().Attribute1) : 0;
            var EarlyOutBefore = unitOfWork.FlexValueRepo.Find(f => f.FlexShortValue == "EarlyOutBefore").FirstOrDefault() != null ? Convert.ToInt16(unitOfWork.FlexValueRepo.Find(f => f.FlexShortValue == "EarlyOutBefore").FirstOrDefault().Attribute1) : 0;
            var teacherList = unitOfWork.EmployeeRepo.GetAll().Select(i => new VmSelectEmployee
            {
                PunchCardId = i.PunchCardId,
                DeviceId = i.DeviceId,
                EmpHeaderId = i.EmpHeaderId,
                EmpId = i.EmpId,
                DesignationId = i.DesignationId,
                EmpName = i.EmpFirstName + " " + i.EmpMiddleName + " " + i.EmpLastName,
                EmployeeOfficeTime = i.EmployeeOfficeTime,
                EmployeeWeekends = i.EmployeeWeekends.Select(s => s.Day).ToList()
            });
            var attendanceList = unitOfWork.AttendanceProcessesRepo.GetAllQueryable().Select(i => new VmAttendanceSelect
            {
                AttendanceId = i.Id,
                PunchDate = i.PunchDate,
                PunchTime = i.PunchTime,
                EmpHeaderId = i.EmpHeaderId,
            }).Where(w => CoreServices.TruncateTime(w.PunchDate) == date);
            foreach (var teacher in teacherList)
            {
                var teacherAttendance = attendanceList.Where(w => w.EmpHeaderId == teacher.EmpHeaderId);
                var data = new VmDateWiseAttendance();

                data.EmpHeaderId = teacher.EmpHeaderId;
                data.EmpId = teacher.EmpId;
                data.Designation = flexValueService.GetFlexValueByFlexValueId(teacher.DesignationId ?? 0);
                data.TeacherName = teacher.EmpFirstName + " " + teacher.EmpMiddleName + " " + teacher.EmpLastName;
                data.StartTime = teacher.EmployeeOfficeTime.Where(w => w.EffectiveFrom <= date && (w.EffectiveTo >= date || w.EffectiveTo == null)).Any() ? teacher.EmployeeOfficeTime.FirstOrDefault(w => w.EffectiveFrom <= date && (w.EffectiveTo >= date || w.EffectiveTo == null)).StartTime : teacher.EmployeeOfficeTime.Any() ? teacher.EmployeeOfficeTime.FirstOrDefault().StartTime : "Not Set";
                data.EndTime = teacher.EmployeeOfficeTime.Where(w => w.EffectiveFrom <= date && (w.EffectiveTo >= date || w.EffectiveTo == null)).Any() ? teacher.EmployeeOfficeTime.FirstOrDefault(w => w.EffectiveFrom <= date && (w.EffectiveTo >= date || w.EffectiveTo == null)).EndTime : teacher.EmployeeOfficeTime.Any() ? teacher.EmployeeOfficeTime.FirstOrDefault().EndTime : "Not Set";

                data.InTime = unitOfWork.AttendanceEditHistorysRepo.FirstOrDefault(s => s.EmployeeHeaderId == teacher.EmpHeaderId && s.Date == date && s.InTime != null && s.isActive == true) != null ? unitOfWork.AttendanceEditHistorysRepo.FirstOrDefault(s => s.EmployeeHeaderId == teacher.EmpHeaderId && s.Date == date && s.InTime != null && s.isActive == true).InTime : teacherAttendance.Any() ? teacherAttendance.FirstOrDefault().PunchTime : "";

                data.OutTime = unitOfWork.AttendanceEditHistorysRepo.FirstOrDefault(s => s.EmployeeHeaderId == teacher.EmpHeaderId && s.Date == date && s.OutTime != null && s.isActive == true) != null ? unitOfWork.AttendanceEditHistorysRepo.FirstOrDefault(s => s.EmployeeHeaderId == teacher.EmpHeaderId && s.Date == date && s.OutTime != null && s.isActive == true).OutTime : teacherAttendance.Any() ? teacherAttendance.OrderByDescending(o => o.AttendanceId).FirstOrDefault().PunchTime : "";

                if (data.InTime != "" && data.OutTime != "")
                {
                    data.WorkingHour = TimeSpan.FromMinutes(DateTime.Parse(data.OutTime).Subtract(DateTime.Parse(data.InTime)).TotalMinutes).ToString(@"hh\:mm");
                }
                else
                {
                    data.WorkingHour = "";
                }
                var weekendDays = teacher.EmployeeWeekends.Any() ? teacher.EmployeeWeekends : DefaultWeekendDays;
                var isWeekend = weekendDays.Any(weekday => (int)date.DayOfWeek == weekday);
                data.AttendanceType = GetAttendanceType(isHoliday, date, teacher, teacherAttendance, isWeekend, LateCountAfter, EarlyOutBefore);
                result.Add(data);
            }
            return result;
        }

        public VmDateWiseAttendance GetDayAttendanceData(DateTime date, int EmpHeaderId)
        {
            var holidayid = flexValueService.GetFlexValueIdByFlexShortValue("holiday");
            var isHoliday = unitOfWork.EventAndHolidayPlannersRepo.Find(w => w.EventTypeId == holidayid && ((w.EndDate == null && w.StartDate == date) || (w.EndDate != null && w.StartDate <= date && w.EndDate >= date))).Any();

            var DefaultWeekendDays = new List<int>(); // weekends list from default settings
            var weekdayFlex = unitOfWork.FlexValueRepo.Find(f => f.FlexShortValue == "WeekendDay").FirstOrDefault();
            DefaultWeekendDays = weekdayFlex != null ? unitOfWork.FlexValueRepo.Find(f => f.ParentFlexValueId == weekdayFlex.FlexValueId).Select(s => Convert.ToInt32(s.Attribute1)).ToList() : new List<int>();
            if (!DefaultWeekendDays.Any())
                DefaultWeekendDays.Add((int)DayOfWeek.Friday);

            var LateCountAfter = unitOfWork.FlexValueRepo.Find(f => f.FlexShortValue == "LateCountAfter").FirstOrDefault() != null ? Convert.ToInt16(unitOfWork.FlexValueRepo.Find(f => f.FlexShortValue == "LateCountAfter").FirstOrDefault().Attribute1) : 0;
            var EarlyOutBefore = unitOfWork.FlexValueRepo.Find(f => f.FlexShortValue == "EarlyOutBefore").FirstOrDefault() != null ? Convert.ToInt16(unitOfWork.FlexValueRepo.Find(f => f.FlexShortValue == "EarlyOutBefore").FirstOrDefault().Attribute1) : 0;
            var OvertimeCountAfter = unitOfWork.FlexValueRepo.Find(f => f.FlexShortValue == "OvertimeCountAfter").FirstOrDefault() != null ? Convert.ToInt16(unitOfWork.FlexValueRepo.Find(f => f.FlexShortValue == "OvertimeCountAfter").FirstOrDefault().Attribute1) : 0;
            var employeeInfo = unitOfWork.EmployeeRepo.Find(s => s.EmpHeaderId == EmpHeaderId).Select(i => new VmSelectEmployee
            {
                PunchCardId = i.PunchCardId,
                DeviceId = i.DeviceId,
                EmpHeaderId = i.EmpHeaderId,
                EmpId = i.EmpId,
                DesignationId = i.DesignationId,
                EmpName = i.EmpFirstName + " " + i.EmpMiddleName + " " + i.EmpLastName,
                EmployeeOfficeTime = i.EmployeeOfficeTime,
                EmployeeWeekends = i.EmployeeWeekends.Select(s => s.Day).ToList()
            }).FirstOrDefault();

            var teacherAttendance = unitOfWork.AttendanceProcessesRepo.GetAllQueryable().Select(i => new VmAttendanceSelect
            {
                AttendanceId = i.Id,
                PunchDate = i.PunchDate,
                PunchTime = i.PunchTime,
                EmpHeaderId = i.EmpHeaderId,
            }).Where(w => CoreServices.TruncateTime(w.PunchDate) == date && w.EmpHeaderId == employeeInfo.EmpHeaderId).ToList();

            var data = new VmDateWiseAttendance();
            data.EmpHeaderId = employeeInfo.EmpHeaderId;
            data.EmpId = employeeInfo.EmpId;
            data.Designation = flexValueService.GetFlexValueByFlexValueId(employeeInfo.DesignationId ?? 0);
            data.TeacherName = employeeInfo.EmpFirstName + " " + employeeInfo.EmpMiddleName + " " + employeeInfo.EmpLastName;

            data.StartTime = employeeInfo.EmployeeOfficeTime.Where(w => w.EffectiveFrom <= date && (w.EffectiveTo >= date || w.EffectiveTo == null)).Any() ? employeeInfo.EmployeeOfficeTime.FirstOrDefault(w => w.EffectiveFrom <= date && (w.EffectiveTo >= date || w.EffectiveTo == null)).StartTime : employeeInfo.EmployeeOfficeTime.Any() ? employeeInfo.EmployeeOfficeTime.FirstOrDefault().StartTime : "Not Set";

            data.EndTime = employeeInfo.EmployeeOfficeTime.Where(w => w.EffectiveFrom <= date && (w.EffectiveTo >= date || w.EffectiveTo == null)).Any() ? employeeInfo.EmployeeOfficeTime.FirstOrDefault(w => w.EffectiveFrom <= date && (w.EffectiveTo >= date || w.EffectiveTo == null)).EndTime : employeeInfo.EmployeeOfficeTime.Any() ? employeeInfo.EmployeeOfficeTime.FirstOrDefault().EndTime : "Not Set";

            data.InTime = unitOfWork.AttendanceEditHistorysRepo.FirstOrDefault(s => s.EmployeeHeaderId == employeeInfo.EmpHeaderId && s.Date == date && s.InTime != null && s.isActive == true) != null ? unitOfWork.AttendanceEditHistorysRepo.FirstOrDefault(s => s.EmployeeHeaderId == employeeInfo.EmpHeaderId && s.Date == date && s.InTime != null && s.isActive == true).InTime : teacherAttendance.Any() ? teacherAttendance.FirstOrDefault().PunchTime : "";

            data.OutTime = unitOfWork.AttendanceEditHistorysRepo.FirstOrDefault(s => s.EmployeeHeaderId == employeeInfo.EmpHeaderId && s.Date == date && s.OutTime != null && s.isActive == true) != null ? unitOfWork.AttendanceEditHistorysRepo.FirstOrDefault(s => s.EmployeeHeaderId == employeeInfo.EmpHeaderId && s.Date == date && s.OutTime != null && s.isActive == true).OutTime : teacherAttendance.Any() ? teacherAttendance.OrderByDescending(o => o.AttendanceId).FirstOrDefault().PunchTime : "";

            if (data.InTime != "" && data.OutTime != "")
                data.WorkingHour = TimeSpan.FromMinutes(DateTime.Parse(data.OutTime).Subtract(DateTime.Parse(data.InTime)).TotalMinutes).ToString(@"hh\:mm");
            else
                data.WorkingHour = "";

            if (!string.IsNullOrEmpty(data.InTime) && !string.IsNullOrEmpty(data.OutTime) && data.StartTime != "Not Set" && data.EndTime != "Not Set")
            {
                DateTime inTime = DateTime.Parse(data.InTime);
                DateTime outTime = DateTime.Parse(data.OutTime);
                DateTime startTime = DateTime.Parse(data.StartTime);
                DateTime endTime = DateTime.Parse(data.EndTime);

                if (inTime > startTime.AddMinutes(LateCountAfter)) // if comes after intime
                    data.LateTime = TimeSpan.FromMinutes(inTime.Subtract(startTime).TotalMinutes).ToString(@"hh\:mm");
                else
                    data.LateTime = "00:00";

                if (outTime > endTime.AddMinutes(OvertimeCountAfter)) //if do any overtime
                {
                    var isovertimeAssigned = unitOfWork.CompensationsRepo.GetAll().OrderByDescending(w => w.CompensationId).FirstOrDefault(w => w.EmployeeHeaderId == employeeInfo.EmpHeaderId && w.ApprovalDate == date);
                    if (isovertimeAssigned == null)
                        data.OverTime = TimeSpan.FromMinutes(outTime.Subtract(endTime).TotalMinutes).ToString(@"hh\:mm");
                    else
                    {
                        data.OverTime = "A"; // overtime approved
                        data.RealOverTime = TimeSpan.FromMinutes(outTime.Subtract(endTime).TotalMinutes).ToString(@"hh\:mm");
                        data.ApprovedOverTime = isovertimeAssigned.Time;
                    }
                    data.EarlyOut = "00:00";
                }
                else if (endTime > outTime.AddMinutes(EarlyOutBefore)) // if early out
                {
                    data.EarlyOut = TimeSpan.FromMinutes(endTime.Subtract(outTime).TotalMinutes).ToString(@"hh\:mm");
                    data.OverTime = "";
                }
                else
                {
                    data.EarlyOut = "00:00";
                    data.OverTime = "";
                }
            }
            else
            {
                data.EarlyOut = "00:00";
                data.LateTime = "00:00";
                data.OverTime = "";
            }
            var weekendDays = employeeInfo.EmployeeWeekends.Any() ? employeeInfo.EmployeeWeekends : DefaultWeekendDays;
            var isWeekend = weekendDays.Any(weekday => (int)date.DayOfWeek == weekday);
            data.AttendanceType = GetAttendanceType(isHoliday, date, employeeInfo, teacherAttendance, isWeekend, LateCountAfter, EarlyOutBefore);
            return data;
        }


        public string GetAttendanceType(bool isHoliday, DateTime date, VmSelectEmployee employee, IEnumerable<VmAttendanceSelect> teacherAttendance, bool isWeekend, int lateCountAfter, int earlyOutBefore)
        {
            TimeSpan difForLateCheck = new TimeSpan();
            TimeSpan difForEarlyOutCheck = new TimeSpan();
            var firstPunchisEdit = unitOfWork.AttendanceEditHistorysRepo.FirstOrDefault(s => s.EmployeeHeaderId == employee.EmpHeaderId && s.Date == date && s.InTime != null && s.isActive == true);
            var lastPunchisEdit = unitOfWork.AttendanceEditHistorysRepo.FirstOrDefault(s => s.EmployeeHeaderId == employee.EmpHeaderId && s.Date == date && s.OutTime != null && s.isActive == true);

            var ofcTimeOfDay = employee.EmployeeOfficeTime.FirstOrDefault(w => w.EffectiveFrom <= date && (w.EffectiveTo >= date || w.EffectiveTo == null));

            if (firstPunchisEdit != null)
            {
                var firstPunch = firstPunchisEdit;
                if (firstPunch != null && ofcTimeOfDay != null)
                    difForLateCheck = DateTime.Parse(firstPunch.InTime).Subtract(DateTime.Parse(ofcTimeOfDay.StartTime));
            }
            else
            {
                var firstPunch = teacherAttendance.FirstOrDefault();
                if (firstPunch != null && ofcTimeOfDay != null)
                    difForLateCheck = DateTime.Parse(firstPunch.PunchTime).Subtract(DateTime.Parse(ofcTimeOfDay.StartTime));
            }

            if (lastPunchisEdit != null)
            {
                var lastPunch = lastPunchisEdit;
                if (lastPunch != null && ofcTimeOfDay != null)
                    difForEarlyOutCheck = DateTime.Parse(ofcTimeOfDay.EndTime).Subtract(DateTime.Parse(lastPunch.OutTime));
            }
            else
            {
                var lastPunch = teacherAttendance.LastOrDefault();
                if (lastPunch != null && ofcTimeOfDay != null)
                    difForEarlyOutCheck = DateTime.Parse(ofcTimeOfDay.EndTime).Subtract(DateTime.Parse(lastPunch.PunchTime));
            }
            string type = "";
            if (employee == null/* || employee.DeviceId == null || employee.DeviceId == 0 || employee.PunchCardId == null*/ || !employee.EmployeeOfficeTime.Any())
                type = CoreServices.GetEnumList<AttendanceType>().FirstOrDefault(f => f.Id == (int)AttendanceType.NotFound).Name;
            else if (isWeekend)
                type = CoreServices.GetEnumList<AttendanceType>().FirstOrDefault(f => f.Id == (int)AttendanceType.Weekend).Name;
            else if (isHoliday)
                type = CoreServices.GetEnumList<AttendanceType>().FirstOrDefault(f => f.Id == (int)AttendanceType.Holiday).Name;
            else if (unitOfWork.HrEmployeeLeaveApplicationRepo.FirstOrDefault(f => f.EmpHeaderId == employee.EmpHeaderId && f.LeaveStartDate <= date && f.LeaveEndDate >= date && f.LeaveStatus == LeaveStatus.Approved) != null)
                type = CoreServices.GetEnumList<AttendanceType>().FirstOrDefault(f => f.Id == (int)AttendanceType.Leave).Name;
            else if (!teacherAttendance.Any() && firstPunchisEdit == null && lastPunchisEdit == null)
                type = CoreServices.GetEnumList<AttendanceType>().FirstOrDefault(f => f.Id == (int)AttendanceType.Absent).Name;
            else if (difForLateCheck.TotalMinutes > lateCountAfter)
                type = CoreServices.GetEnumList<AttendanceType>().FirstOrDefault(f => f.Id == (int)AttendanceType.Late).Name;
            else if (difForEarlyOutCheck.TotalMinutes > earlyOutBefore)
                type = CoreServices.GetEnumList<AttendanceType>().FirstOrDefault(f => f.Id == (int)AttendanceType.EarlyOut).Name;
            else
                type = CoreServices.GetEnumList<AttendanceType>().FirstOrDefault(f => f.Id == (int)AttendanceType.Present).Name;

            return type;
        }
    }

    public enum AttendanceType
    {
        [Description("Not Found")]
        NotFound = 1,
        Weekend = 2,
        Holiday = 3,
        Leave = 4,
        Absent = 5,
        Late = 6,
        [Description("Early Out")]
        EarlyOut = 7,
        Present = 8
    }
    public class VmSelectEmployee
    {
        public string PunchCardId { get; set; }
        public int? DeviceId { get; set; }
        public int EmpHeaderId { get; set; }
        public string EmpId { get; set; }
        public int? DesignationId { get; set; }
        public string EmpName { get; set; }
        public string EmpFirstName { get; set; }
        public string EmpMiddleName{ get; set; }
        public string EmpLastName { get; set; }
        public virtual List<EmployeeOfficeTime> EmployeeOfficeTime { get; set; }
        public virtual List<int> EmployeeWeekends { get; set; }
    }
    public class VmAttendanceSelect
    {
        public int AttendanceId { get; set; }

        public DateTime PunchDate { get; set; }
        public string PunchTime { get; set; }

        //Device Id
        public int? DoorNumber { get; set; }

        public int? GateNumber { get; set; }
        public string CardNumber { get; set; }

        public int? PermissionTypeId { get; set; }

        public bool? AttendanceStatus { get; set; }
        public int? DevicePort { get; set; }
        public string DeviceIP { get; set; }
        public int? EmpHeaderId { get; set; }
    }
}