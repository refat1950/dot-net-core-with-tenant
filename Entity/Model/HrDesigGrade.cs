using System;
using System.ComponentModel.DataAnnotations;

namespace Entity.Model
{
    public partial class HrDesigGrade : BaseEntity
    {
        [Key]
        public int GradeId { get; set; }

        [StringLength(50)]
        public string GradeName { get; set; }

        public int? StartAmt { get; set; }

        public int? EndAmt { get; set; }

        public int? DesignationId { get; set; }
    }
}
