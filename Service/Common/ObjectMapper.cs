﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Services.Common
{
   public static class ObjectMapper
    {
        public static TTarget MapEntity<TTarget, TBase>(TBase entity) where TTarget : new()
        {
            if (entity == null)
                return default(TTarget);

            IList<TBase> baselist = new List<TBase>();
            baselist.Add(entity);
            IList<TTarget> list = MapEntities<TTarget, TBase>(baselist);

            return list.Count > 0 ? list[0] : default(TTarget);

        }
        public static IList<TTarget> MapEntities<TTarget, TBase>(IList<TBase> entities) where TTarget : new()
        {
            if (entities.Count == 0)
                return default(IList<TTarget>);

            TBase entity = entities[0];
            ValidateMappings<TTarget, TBase>(entity);
            List<PropertyInfo> baseprops = new List<PropertyInfo>(DataMapper.GetSourceProperties(typeof(TBase)));

            IList<TTarget> list = new List<TTarget>();
            for (int i = 0; i < entities.Count; i++)
            {
                TTarget obj = new TTarget();
                foreach (PropertyInfo propsInfo in baseprops)
                {
                    DataMapper.SetPropertyValue(obj, propsInfo.Name, propsInfo.GetValue(entities[i], null));
                }
                list.Add(obj);
            }

            return list;
        }
        private static bool ValidateMappings<TTarget, TBase>(TBase entity)
        {

            List<PropertyInfo> props = new List<PropertyInfo>(DataMapper.GetSourceProperties(typeof(TTarget)));
            List<PropertyInfo> baseprops = new List<PropertyInfo>(DataMapper.GetSourceProperties(typeof(TBase)));

            foreach (PropertyInfo propInfo in baseprops)
            {
                PropertyInfo propinfo = props.Find(
                   delegate(PropertyInfo pi) { return pi.Name == propInfo.Name ? true : false; });
                if (propinfo == null)
                {
                    string err = string.Format("Property '{0}' of type '{1}' is missing from the type '{2}'",
                                               propInfo.Name, propInfo.PropertyType, typeof(TTarget).FullName);
                    throw new Exception(err);
                }
            }

            return true;

        }
    }
}
