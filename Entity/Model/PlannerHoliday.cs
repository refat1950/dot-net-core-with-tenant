using System;
using System.ComponentModel.DataAnnotations;

namespace Entity.Model
{
    public partial class PlannerHoliday : BaseEntity
    {
        [Key]
        public int HolidayId { get; set; }

        public int SessionId { get; set; }

        public DateTime? TrnsDate { get; set; }

        public int? EventId { get; set; }


    }
}
