﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model.Workflow
{
    public class ApproverInfo : BaseEntity
    {
        [Key]
        public int ApproverInfoId { get; set; }
        public int WorkFlowMasterId { get; set; }
        public int Approver { get; set; }
        public int ApprovalState { get; set; }
        public int AlternateApproverId { get; set; }
        public string Remarks { get; set; }
    }
}
