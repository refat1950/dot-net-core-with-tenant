using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entity.Model
{
    public partial class HrEmpEduQuali : BaseEntity
    {
        public int EmpHeaderId { get; set; }

        public int QualificationId { get; set; }

        public int? QualificationType { get; set; }

        public int? Major { get; set; }

        public int? InstitutionId { get; set; }

        public int? BoardId { get; set; }

        [StringLength(50)]
        public string PassingYear { get; set; }

        [StringLength(50)]
        public string Result { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }
    }
}
