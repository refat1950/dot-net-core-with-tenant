using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Entity.Model
{
    public partial class BookAuthor : BaseEntity
    {
        [Key]
        public int AuthorId { get; set; }
        public string AuthorName { get; set; }
        public virtual List<Book> Books { get; set; }
    }
}
