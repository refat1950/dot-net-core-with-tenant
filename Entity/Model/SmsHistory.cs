using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entity.Model
{
    public partial class SmsHistory : BaseEntity
    {
        public long SmsId { get; set; }

        [StringLength(100)]
        public string SentTo { get; set; }

        [StringLength(200)]
        public string SmsSubject { get; set; }

        [StringLength(200)]
        public string SmsContent { get; set; }

        public bool? IsSend { get; set; }

        public int? Status { get; set; }

        [StringLength(200)]
        public string StatusText { get; set; }

        public int? ErrorCode { get; set; }

        [StringLength(200)]
        public string ErrorText { get; set; }


    }
}
