using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entity.Model
{
    public partial class PlannerEvent : BaseEntity
    {

        [Key]
        public int EventId { get; set; }

        [Required]
        [StringLength(50)]
        public string EventName { get; set; }

        [StringLength(30)]
        public string EventShortName { get; set; }

        public int? EventTypeId { get; set; }

        public int? MasterEventId { get; set; }


        public virtual List<PlannerEventDetails> PlannerEventDtls { get; set; }

        public virtual PlannerEventType PlannerEventType { get; set; }
    }
}
