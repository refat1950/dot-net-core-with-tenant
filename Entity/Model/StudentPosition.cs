﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class StudentPosition
    {
        [Key]
        public int PositionId { get; set; }
        public int PositionLogId { get; set; }

        public int StudentId { get; set; }
        public int ClassId { get; set; }
        public int SectionId { get; set; }

        public int StudentPositionClass { get; set; }
        public int StudentPositionSection { get; set; }

        public decimal? Mark { get; set; }
        public decimal? Atten { get; set; }

        public StudentPositionLog StudentPositionLog { get; set; }
    }

    public class StudentPositionLog
    {
        [Key]
        public int PositionLogId { get; set; }
        public int ClassId { get; set; }
        public int SessionId { get; set; }
        public int EventId { get; set; }

        public DateTime LogTime { get; set; }

        public List<StudentPosition> BssStudentPosition { get; set; }
    }
}
