﻿using Entity.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class EmployeeWeekends : BaseEntity
    {
        [Key]
        public int EmployeeWeekendsId { get; set; }
        public int Day { get; set; }
        public int EmpHeaderId { get; set; }
        public virtual HrEmployee Employee { get; set; }
    }
}
