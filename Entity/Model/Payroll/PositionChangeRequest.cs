﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class PositionChangeRequest : BaseEntity
    {
        [Key]
        public int PositionChangeRequestId { get; set; }
        public int EmpHeaderId { get; set; }
        public int PositionId { get; set; }
        public DateTime RequestedChangeDate { get; set; }
        public PositionChangeRequestStatus PositionChangeRequestStatus { get; set; }

    }
    public enum PositionChangeRequestStatus
    {
        Pending = 1,
        Approved = 2,
        Cancel = 3
    }
}
