﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class HrEmployeeLeaveCount : BaseEntity
    {
        [Key]
        public int LeaveCountId { get; set; }
        public int ApplicationId { get; set; }
        public string LeaveQuantity { get; set; }
        public int EmpTotalLeave { get; set; }
        public int? EmpLeaveRemain { get; set; }
    }
}
