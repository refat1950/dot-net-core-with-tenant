﻿using Service.Helper;
using Services.View_Model;
using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace Service.Helper
{
    public class SMSLengthCalculateHelper
    {
        public VmSmsLengthCounter CalculateSmsLength(string text)
        {
            var length = 0;
            var parts = 0;
            bool IsBangla = false;
            var maxLength = 0;
            bool otherlanguage = false;

            for (int i = 0; i < text.Length; i++)
            {
                var charecter = Convert.ToString(text[i]);
                if (IsBanglaCharecter(charecter))
                {
                    IsBangla = true;
                }
                if (!IsBanglaCharecter(charecter) && !IsSpecialCharecter(charecter) && !IsEnglishCharecter(charecter))
                {
                    otherlanguage = true;
                    break;
                }
            }
            if (otherlanguage == true) // if contains any oher language
            {
                length = 0;
                VmSmsLengthCounter vmSmsLengthCounters = new VmSmsLengthCounter();
                vmSmsLengthCounters.IsAllowed = length == 0 ? false : true;
                vmSmsLengthCounters.SmsLength = length;
                return vmSmsLengthCounters;
            }
            if (IsBangla == true)
            {
                length = 0;
                parts = 0;
                for (int i = 0; i < text.Length; i++)
                {
                    var charecter = Convert.ToString(text[i]);
                    if (IsBanglaCharecter(charecter))
                    {
                        length = length + 1;
                    }
                    else if (IsSpecialCharecter(charecter))
                    {
                        length = length + 1; // if text not contains bangla, special charecter will have their own value
                    }
                    else if (IsEnglishCharecter(charecter))
                    {
                        length = length + 1;
                    }

                    else
                    {
                        length = 0;
                    }
                }
                parts = length <= 70 ? 1 : Convert.ToInt32(Math.Ceiling(Convert.ToDouble(length) / 67));
                maxLength = 402;
            }
            else
            {
                length = 0;
                parts = 0;
                for (int i = 0; i < text.Length; i++)
                {
                    var charecter = Convert.ToString(text[i]);
                    if (IsBanglaCharecter(charecter))
                    {
                        length = length + 1;
                        IsBangla = true;
                    }
                    else if (IsSpecialCharecter(charecter))
                    {
                        length = length + GetLength(charecter); // if text not contains bangla, special charecter will have their own value
                    }
                    else if (IsEnglishCharecter(charecter))
                    {
                        length = length + 1;
                    }

                    else
                    {
                        length = 0;
                    }

                }
                parts = length <= 160 ? 1 : Convert.ToInt32(Math.Ceiling(Convert.ToDouble(length) / 153));
                maxLength = 918;
            }
            VmSmsLengthCounter vmSmsLengthCounter = new VmSmsLengthCounter();
            vmSmsLengthCounter.IsAllowed = length == 0 ? false : true;
            vmSmsLengthCounter.SmsLength = length;
            vmSmsLengthCounter.MaxSmsLength = maxLength;
            vmSmsLengthCounter.SmsParts = parts;
            return vmSmsLengthCounter;
        }
        public bool IsEnglishCharecter(string text)
        {
            //var engCharecter = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            //foreach (var item in engCharecter)
            //{
            //    if (text.Contains(item))
            //    {
            //        return true;
            //    }
            //}
            if (Regex.IsMatch(text, @"^[\u0000-\u007F]+$"))
            {
                return true;
            }
            return false;
        }
        public bool IsSpecialCharecter(string text)
        {
            var withoutSpecial = new string(text.Where(c => Char.IsLetterOrDigit(c)).ToArray());
            if (text != withoutSpecial)
            {
                return true;
            }
            return false;
        }
        public bool IsBanglaCharecter(string text)
        {
            if (Regex.IsMatch(text, @"^[\u0980-\u09FF]+$"))
            {
                return true;
            }
            var otherBanglaCharecter = "ω⊙¤°℃℉¢©";
            foreach (var item in otherBanglaCharecter)
            {
                if (text.Contains(item))
                {
                    return true;
                }
            }
            return false;
        }
        public int GetLength(string charecter)
        {
            var length = 0;
            string specialCharForLength_1 = ".,:=+-?()!@#$%&*/;'<>»«£§¿¥£¡_\"([^\"]*)\"";
            string specialCharForLength_2 = @"~^{}[]|\€";

            if (charecter == "\n")
            {
                length = 1;
                return length;
            }
            if (charecter == " ")
            {
                length = 1;
                return length;
            }

            foreach (var item in specialCharForLength_1)
            {
                if (charecter.Contains(item))
                {
                    length = 1;
                    break;
                }
            }
            foreach (var item in specialCharForLength_2)
            {
                if (charecter.Contains(item))
                {
                    length = 2;
                    break;
                }
            }
            return length;
        }
    }
}
