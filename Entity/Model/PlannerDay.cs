using System;
using System.ComponentModel.DataAnnotations;

namespace Entity.Model
{
    public partial class PlannerDay : BaseEntity
    {
        [Key]
        public int DAY_ID { get; set; }

        [Required]
        [StringLength(50)]
        public string DayName { get; set; }

        public bool? IsWeeklyHoliday { get; set; }


    }
}
