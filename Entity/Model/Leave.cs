using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entity.Model
{
    public partial class Leave : BaseEntity
    {
        [Key]
        public int LeaveId { get; set; }

        public int? ReferenceId { get; set; }

        public int? LeaveTypeId { get; set; }

        [StringLength(20)]
        public string UserType { get; set; }

        [DataType(DataType.Date, ErrorMessage = "Date of Birth format should be MM/DD/YYYY")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? FromDate { get; set; }

        [DataType(DataType.Date, ErrorMessage = "Date of Birth format should be MM/DD/YYYY")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? ToDate { get; set; }

        //[Column(TypeName = "ntext")]
        public string Reason { get; set; }

        public int? StatusId { get; set; }


    }
}
