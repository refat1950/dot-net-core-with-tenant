﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.View_Model
{
    public class VMShift
    {
        public int ShiftId { get; set; }
        public string ShiftName { get; set; }
    }
}
