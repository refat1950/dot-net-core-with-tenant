﻿using Repository.Context;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Services.EmailServices
{
    public class Emailer
    {
        //private static string fromEmail = "";
        //private static string fromName = "";
        //private static string password = "";
        private readonly ProjectDbContext db;
        public Emailer(ProjectDbContext db)
        {
            this.db = db;
        }

        //General Email Send
        public bool SendMail(string toEmail, string subject, string body, string fromEmail = null, string fromName = null, List<string> attachmentFilePathList = null, Stream mailStreamAttachment = null, string ccEmail = null, string bccEmail = null)
        {
            SmtpClient smtpClient = new SmtpClient();
            MailMessage mailMsg = new MailMessage();

            MailAddressCollection addressCollection = new MailAddressCollection();
            //addressCollection.Add("softifybd@gmail.com");
            //ProjectDbContext db = new ProjectDbContext();
            var comSetting = db.School.FirstOrDefault() != null ? db.School.FirstOrDefault() : null;
            if (comSetting != null)
            {
                var fromemail = "info@docens.net";
                var fromname = /*comSetting.FromName != null ? comSetting.FromName :*/ "Docens";
                var password = /*comSetting.MailPassword != null ? comSetting.MailPassword : */ "Docens@123";
                var host = /*comSetting.MailHost != null ? comSetting.MailHost :*/ "mail.docens.net";
                var port = "587";
                if (!string.IsNullOrEmpty(fromemail) && !string.IsNullOrEmpty(fromname) && !string.IsNullOrEmpty(password) && !string.IsNullOrEmpty(host) && !string.IsNullOrEmpty(port))
                {
                    try
                    {
                        if (String.IsNullOrEmpty(fromEmail) == true)
                        {
                            fromEmail = fromemail;
                        }

                        if (String.IsNullOrEmpty(fromName) == true)
                        {
                            fromName = fromname;
                        }

                        //Attach file in email, if have any
                        if (attachmentFilePathList != null && attachmentFilePathList.Count > 0)
                        {
                            foreach (string filePath in attachmentFilePathList)
                            {
                                Attachment attachFile = new Attachment(filePath);
                                mailMsg.Attachments.Add(attachFile);
                            }
                        }

                        mailMsg.From = new MailAddress(fromEmail, fromName);
                        mailMsg.To.Add(toEmail);
                        mailMsg.Subject = subject;
                        mailMsg.IsBodyHtml = true;
                        mailMsg.BodyEncoding = Encoding.UTF8;
                        mailMsg.Body = body;
                        mailMsg.Priority = MailPriority.Normal;

                        if (string.IsNullOrEmpty(bccEmail) == false)
                        {
                            MailAddress addressBCC = new MailAddress(bccEmail);
                            mailMsg.Bcc.Add(addressBCC);
                        }

                        if (string.IsNullOrEmpty(ccEmail) == false)
                        {
                            MailAddress addressCC = new MailAddress(ccEmail);
                            mailMsg.CC.Add(addressCC);
                        }

                        //Smtp configuration
                        smtpClient.Credentials = new NetworkCredential(fromemail, password);
                        smtpClient.Host = host;
                        smtpClient.Port = Convert.ToInt32(port);
                        smtpClient.EnableSsl = false;

                        //smtpClient.Credentials = new NetworkCredential("support@softifybd.com", "abc-1234");
                        //smtpClient.Host = "win.xeonbd.com";
                        //smtpClient.Port = 465;
                        //smtpClient.EnableSsl = true;

                        //Finally Send email
                        smtpClient.Send(mailMsg);
                        return true;
                    }
                    catch (SmtpException smtpEx)
                    {
                        throw smtpEx;
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
                return false;
            }
            return false;

        }//end function


        //General Email Send
        public async Task<bool> SendMailAsync(string toEmail, string subject, string body, string fromEmail = null, string fromName = null, List<string> attachmentFilePathList = null, Stream mailStreamAttachment = null, string ccEmail = null, string bccEmail = null)
        {
            SmtpClient smtpClient = new SmtpClient();
            MailMessage mailMsg = new MailMessage();

            MailAddressCollection addressCollection = new MailAddressCollection();
            //addressCollection.Add("softifybd@gmail.com");
            //ProjectDbContext db = new ProjectDbContext();
            var comSetting = db.School.FirstOrDefault() != null ? db.School.FirstOrDefault() : null;
            if (comSetting != null)
            {
                var fromemail = "info@docens.net";
                var fromname = /*comSetting.FromName != null ? comSetting.FromName :*/ "Docens";
                var password = /*comSetting.MailPassword != null ? comSetting.MailPassword : */ "Docens@123";
                var host = /*comSetting.MailHost != null ? comSetting.MailHost :*/ "mail.docens.net";
                var port = "587";
                if (!string.IsNullOrEmpty(fromemail) && !string.IsNullOrEmpty(fromname) && !string.IsNullOrEmpty(password) && !string.IsNullOrEmpty(host) && !string.IsNullOrEmpty(port))
                {
                    try
                    {
                        if (String.IsNullOrEmpty(fromEmail) == true)
                        {
                            fromEmail = fromemail;
                        }

                        if (String.IsNullOrEmpty(fromName) == true)
                        {
                            fromName = fromname;
                        }

                        //Attach file in email, if have any
                        if (attachmentFilePathList != null && attachmentFilePathList.Count > 0)
                        {
                            foreach (string filePath in attachmentFilePathList)
                            {
                                Attachment attachFile = new Attachment(filePath);
                                mailMsg.Attachments.Add(attachFile);
                            }
                        }

                        mailMsg.From = new MailAddress(fromEmail, fromName);
                        mailMsg.To.Add(toEmail);
                        mailMsg.Subject = subject;
                        mailMsg.IsBodyHtml = true;
                        mailMsg.BodyEncoding = Encoding.UTF8;
                        mailMsg.Body = body;
                        mailMsg.Priority = MailPriority.Normal;

                        if (string.IsNullOrEmpty(bccEmail) == false)
                        {
                            MailAddress addressBCC = new MailAddress(bccEmail);
                            mailMsg.Bcc.Add(addressBCC);
                        }

                        if (string.IsNullOrEmpty(ccEmail) == false)
                        {
                            MailAddress addressCC = new MailAddress(ccEmail);
                            mailMsg.CC.Add(addressCC);
                        }

                        //Smtp configuration
                        smtpClient.Credentials = new NetworkCredential(fromemail, password);
                        smtpClient.Host = host;
                        smtpClient.Port = Convert.ToInt32(port);
                        smtpClient.EnableSsl = false;

                        //smtpClient.Credentials = new NetworkCredential("support@softifybd.com", "abc-1234");
                        //smtpClient.Host = "win.xeonbd.com";
                        //smtpClient.Port = 465;
                        //smtpClient.EnableSsl = true;

                        //Finally Send email
                        await smtpClient.SendMailAsync(mailMsg);
                        return true;
                    }
                    catch (SmtpException smtpEx)
                    {
                        throw smtpEx;
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
                return false;
            }
            return false;
        }//end function

        //Email Send with Exact File Attachment
        public bool SendMailWithStreamFileAttachment(string attachmentFileName, Stream mailStreamAttachment, string toEmail, string subject, string body, string fromEmail = null, string fromName = null, string ccEmail = null, string bccEmail = null)
        {
            SmtpClient smtpClient = new SmtpClient();
            MailMessage mailMsg = new MailMessage();
            //ProjectDbContext db = new ProjectDbContext();
            var comSetting = db.School.FirstOrDefault() != null ? db.School.FirstOrDefault() : null;
            if (comSetting != null)
            {
                var fromemail = "info@docens.net";
                var fromname = /*comSetting.FromName != null ? comSetting.FromName :*/ "Docens";
                var password = /*comSetting.MailPassword != null ? comSetting.MailPassword : */ "Docens@123";
                var host = /*comSetting.MailHost != null ? comSetting.MailHost :*/ "mail.docens.net";
                var port = "587";
                if (!string.IsNullOrEmpty(fromemail) && !string.IsNullOrEmpty(fromname) && !string.IsNullOrEmpty(password) && !string.IsNullOrEmpty(host) && !string.IsNullOrEmpty(port))
                {
                    try
                    {
                        if (String.IsNullOrEmpty(fromEmail) == true)
                        {
                            fromEmail = fromemail;
                        }

                        if (String.IsNullOrEmpty(fromName) == true)
                        {
                            fromName = fromname;
                        }

                        //Attach file stream in email, if have any
                        Attachment attachment = new System.Net.Mail.Attachment(mailStreamAttachment, attachmentFileName);
                        mailMsg.Attachments.Add(attachment);

                        mailMsg.From = new MailAddress(fromEmail, fromName);
                        mailMsg.To.Add(toEmail);
                        mailMsg.Subject = subject;
                        mailMsg.IsBodyHtml = true;
                        mailMsg.BodyEncoding = Encoding.UTF8;
                        mailMsg.Body = body;
                        mailMsg.Priority = MailPriority.Normal;

                        if (string.IsNullOrEmpty(bccEmail) == false)
                        {
                            MailAddress addressBCC = new MailAddress(bccEmail);
                            mailMsg.Bcc.Add(addressBCC);
                        }

                        if (string.IsNullOrEmpty(ccEmail) == false)
                        {
                            MailAddress addressCC = new MailAddress(ccEmail);
                            mailMsg.CC.Add(addressCC);
                        }

                        //Smtp configuration
                        smtpClient.Credentials = new NetworkCredential(fromemail, password);
                        smtpClient.Host = host;
                        smtpClient.Port = Convert.ToInt32(port);
                        smtpClient.EnableSsl = false;

                        //Finally Send email
                        smtpClient.Send(mailMsg);
                        return true;
                    }
                    catch (SmtpException smtpEx)
                    {
                        throw smtpEx;
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
                return false;
            }
            return false;
        }//end function

        //Email Send with Byte File Attachment
        public bool SendMailWithMemoryStreamFileAttachment(string attachmentFileName, byte[] mailStreamAttachment, string toEmail, string subject, string body, string fromEmail = null, string fromName = null, string ccEmail = null, string bccEmail = null)
        {
            SmtpClient smtpClient = new SmtpClient();
            MailMessage mailMsg = new MailMessage();
            //ProjectDbContext db = new ProjectDbContext();
            var comSetting = db.School.FirstOrDefault() != null ? db.School.FirstOrDefault() : null;
            if (comSetting != null)
            {
                var fromemail = "info@docens.net";
                var fromname = /*comSetting.FromName != null ? comSetting.FromName :*/ "Docens";
                var password = /*comSetting.MailPassword != null ? comSetting.MailPassword : */ "Docens@123";
                var host = /*comSetting.MailHost != null ? comSetting.MailHost :*/ "mail.docens.net";
                var port = "587";
                if (!string.IsNullOrEmpty(fromemail) && !string.IsNullOrEmpty(fromname) && !string.IsNullOrEmpty(password) && !string.IsNullOrEmpty(host) && !string.IsNullOrEmpty(port))
                {
                    try
                    {
                        if (String.IsNullOrEmpty(fromEmail) == true)
                        {
                            fromEmail = fromemail;
                        }

                        if (String.IsNullOrEmpty(fromName) == true)
                        {
                            fromName = fromname;
                        }

                        //Attach file stream in email, if have any
                        List<MemoryStream> memoStreamList = new List<MemoryStream>();
                        MemoryStream mst = new MemoryStream(mailStreamAttachment);
                        mailMsg.Attachments.Add(new System.Net.Mail.Attachment(mst, attachmentFileName));
                        //File added to stream list
                        memoStreamList.Add(mst);

                        mailMsg.From = new MailAddress(fromEmail, fromName);
                        mailMsg.To.Add(toEmail);
                        mailMsg.Subject = subject;
                        mailMsg.IsBodyHtml = true;
                        mailMsg.BodyEncoding = Encoding.UTF8;
                        mailMsg.Body = body;
                        mailMsg.Priority = MailPriority.Normal;

                        if (string.IsNullOrEmpty(bccEmail) == false)
                        {
                            MailAddress addressBCC = new MailAddress(bccEmail);
                            mailMsg.Bcc.Add(addressBCC);
                        }

                        if (string.IsNullOrEmpty(ccEmail) == false)
                        {
                            MailAddress addressCC = new MailAddress(ccEmail);
                            mailMsg.CC.Add(addressCC);
                        }

                        //Smtp configuration
                        smtpClient.Credentials = new NetworkCredential(fromemail, password);
                        smtpClient.Host = host;
                        smtpClient.Port = Convert.ToInt32(port);
                        smtpClient.EnableSsl = false;

                        //Finally Send email
                        smtpClient.Send(mailMsg);
                        foreach (MemoryStream stream in memoStreamList)
                        {
                            stream.Dispose();
                        }
                        return true;
                    }
                    catch (SmtpException smtpEx)
                    {
                        throw smtpEx;
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
                return false;
            }
            return false;
        }

        public string GetEmailHtmlTemplate(string templatePath)
        {
            string emailBodyContent = "";
            //string fileName = HttpContext.Current.Server.MapPath("~/Email-Template/ReportEmail.html")

            if (System.IO.File.Exists(templatePath) == true)
            {
                using (StreamReader textReader = new System.IO.StreamReader(templatePath))
                {
                    emailBodyContent = textReader.ReadToEnd();
                }

                return emailBodyContent;
            }

            return emailBodyContent;
        }

    }//end class

}
