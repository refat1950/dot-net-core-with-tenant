using System;
using System.ComponentModel.DataAnnotations;

namespace Entity.Model
{
    public partial class PhotoGallery : BaseEntity
    {
        [Key]
        public int PhotoGalleryId { get; set; }

        public int? GalleryId { get; set; }

        [StringLength(50)]
        public string ImageTitle { get; set; }

        [StringLength(100)]
        public string ThumbImageUrl { get; set; }

        [StringLength(100)]
        public string DisplayImageUrl { get; set; }

        [StringLength(100)]
        public string OrginalImageUrl { get; set; }

        public int? DisplayStatus { get; set; }
        public virtual Gallery Gallery { get; set; }
    }
}
