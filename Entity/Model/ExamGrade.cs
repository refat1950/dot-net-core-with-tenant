﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class ExamGrade : BaseEntity
    {
        [Key]
        public int GradeId { get; set; }
        public string Description { get; set; }
        public string ShortDesc { get; set; }
        public string AltName { get; set; }
        public decimal FromScore { get; set; }
        public decimal ToScore { get; set; }
        public string Point { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "Active To")]
        public DateTime? StartDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "Active To")]
        public DateTime? EndDate { get; set; }

        public virtual List<ClassExamGrade> ClassExamGrade { get; set; }
    }
}
