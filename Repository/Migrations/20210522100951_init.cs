﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using MySql.EntityFrameworkCore.Metadata;

namespace Repository.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Attendance",
                columns: table => new
                {
                    AttendanceId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    PunchDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    PunchTime = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: false),
                    SessionId = table.Column<int>(type: "int", nullable: true),
                    SubjectId = table.Column<int>(type: "int", nullable: true),
                    DoorNumber = table.Column<int>(type: "int", nullable: true),
                    GateNumber = table.Column<int>(type: "int", nullable: true),
                    CardNumber = table.Column<string>(type: "text", nullable: true),
                    EmpHeaderId = table.Column<int>(type: "int", nullable: true),
                    PermissionTypeId = table.Column<int>(type: "int", nullable: true),
                    AttendanceStatus = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    DevicePort = table.Column<int>(type: "int", nullable: true),
                    DeviceIP = table.Column<string>(type: "text", nullable: true),
                    CreatedIp = table.Column<string>(type: "text", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Attendance", x => x.AttendanceId);
                });

            migrationBuilder.CreateTable(
                name: "AttendanceChangeRequists",
                columns: table => new
                {
                    AttendanceChangeRequistId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    EmployeeHeaderId = table.Column<int>(type: "int", nullable: true),
                    StudentHeaderId = table.Column<int>(type: "int", nullable: true),
                    PunchDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    OfficeInTime = table.Column<string>(type: "text", nullable: true),
                    OfficeOutTime = table.Column<string>(type: "text", nullable: true),
                    InTime = table.Column<string>(type: "text", nullable: true),
                    OutTime = table.Column<string>(type: "text", nullable: true),
                    RequistedInTime = table.Column<string>(type: "text", nullable: true),
                    RequistedOutTime = table.Column<string>(type: "text", nullable: true),
                    ApprovedInTime = table.Column<string>(type: "text", nullable: true),
                    ApprovedOutTime = table.Column<string>(type: "text", nullable: true),
                    Status = table.Column<int>(type: "int", nullable: false),
                    SeenStatus = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    StudentSeenStatus = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    TeacherSeenStatus = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    InTimeRemarks = table.Column<string>(type: "text", nullable: true),
                    OutTimeRemarks = table.Column<string>(type: "text", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AttendanceChangeRequists", x => x.AttendanceChangeRequistId);
                });

            migrationBuilder.CreateTable(
                name: "AttendanceEditHistories",
                columns: table => new
                {
                    MenualAttendanceId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    EmployeeHeaderId = table.Column<int>(type: "int", nullable: false),
                    Date = table.Column<DateTime>(type: "datetime", nullable: false),
                    InTime = table.Column<string>(type: "text", nullable: true),
                    OutTime = table.Column<string>(type: "text", nullable: true),
                    isActive = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AttendanceEditHistories", x => x.MenualAttendanceId);
                });

            migrationBuilder.CreateTable(
                name: "AttendanceEmployee",
                columns: table => new
                {
                    EmpHeaderId = table.Column<int>(type: "int", nullable: false),
                    PunchDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    CardNumber = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    StartTime = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    EndTime = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    InTime = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    OutTime = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    TimeLate = table.Column<int>(type: "int", nullable: true),
                    EarlyLeave = table.Column<int>(type: "int", nullable: true),
                    DoorNumber = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AttendanceEmployee", x => new { x.EmpHeaderId, x.PunchDate });
                });

            migrationBuilder.CreateTable(
                name: "AttendanceGrade",
                columns: table => new
                {
                    AttGradeId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    Description = table.Column<string>(type: "text", nullable: true),
                    ShortDesc = table.Column<string>(type: "text", nullable: true),
                    AltName = table.Column<string>(type: "text", nullable: true),
                    FromScore = table.Column<decimal>(type: "decimal(18, 2)", nullable: false),
                    ToScore = table.Column<decimal>(type: "decimal(18, 2)", nullable: false),
                    StartDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EndDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AttendanceGrade", x => x.AttGradeId);
                });

            migrationBuilder.CreateTable(
                name: "AttendancePolicy",
                columns: table => new
                {
                    PolicyId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    DeptId = table.Column<int>(type: "int", nullable: true),
                    DesigId = table.Column<int>(type: "int", nullable: true),
                    MaxLate = table.Column<int>(type: "int", nullable: true),
                    MaxEarly = table.Column<int>(type: "int", nullable: true),
                    RainyDay = table.Column<string>(type: "text", nullable: true),
                    Status = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AttendancePolicy", x => x.PolicyId);
                });

            migrationBuilder.CreateTable(
                name: "AttendanceProcesses",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    EmpHeaderId = table.Column<int>(type: "int", nullable: false),
                    PunchDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    PunchTime = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: false),
                    CreatedIp = table.Column<string>(type: "text", nullable: true),
                    Latitude = table.Column<string>(type: "text", nullable: true),
                    Longitude = table.Column<string>(type: "text", nullable: true),
                    LocationException = table.Column<string>(type: "text", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AttendanceProcesses", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AttendanceStudent",
                columns: table => new
                {
                    AttendanceId = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    StudentHeaderId = table.Column<int>(type: "int", nullable: false),
                    CardNumber = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    PunchDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    StartTime = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    EndTime = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    InTime = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    OutTime = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    TimeLate = table.Column<int>(type: "int", nullable: true),
                    EarlyLeave = table.Column<int>(type: "int", nullable: true),
                    DoorNumber = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AttendanceStudent", x => x.AttendanceId);
                });

            migrationBuilder.CreateTable(
                name: "AttendenceInfo",
                columns: table => new
                {
                    Attendanceid = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    Inattendanceid = table.Column<int>(type: "int", nullable: false),
                    EmpId = table.Column<string>(type: "text", nullable: true),
                    FullName = table.Column<string>(type: "text", nullable: true),
                    Dept = table.Column<string>(type: "text", nullable: true),
                    DeptId = table.Column<int>(type: "int", nullable: false),
                    Designation = table.Column<string>(type: "text", nullable: true),
                    CampusId = table.Column<int>(type: "int", nullable: false),
                    Campus = table.Column<string>(type: "text", nullable: true),
                    Note = table.Column<string>(type: "text", nullable: true),
                    OfficeTime = table.Column<DateTime>(type: "datetime", nullable: false),
                    OfficeOutTime = table.Column<DateTime>(type: "datetime", nullable: false),
                    InTime = table.Column<DateTime>(type: "datetime", nullable: true),
                    OutTime = table.Column<DateTime>(type: "datetime", nullable: true),
                    AttendanceCode = table.Column<string>(type: "text", nullable: true),
                    LateTime = table.Column<string>(type: "text", nullable: true),
                    WorkTime = table.Column<string>(type: "text", nullable: true),
                    EarlyOut = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AttendenceInfo", x => x.Attendanceid);
                });

            migrationBuilder.CreateTable(
                name: "Banner",
                columns: table => new
                {
                    BanenrId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    Title = table.Column<string>(type: "varchar(200)", maxLength: 200, nullable: false),
                    ImageUrl = table.Column<string>(type: "text", nullable: true),
                    Details = table.Column<string>(type: "text", nullable: true),
                    BannerStatus = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Banner", x => x.BanenrId);
                });

            migrationBuilder.CreateTable(
                name: "BillPeriod",
                columns: table => new
                {
                    PeriodId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    PeriodName = table.Column<string>(type: "text", nullable: true),
                    PeriodTypeId = table.Column<int>(type: "int", nullable: true),
                    StartDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EndDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    MonthId = table.Column<int>(type: "int", nullable: false),
                    Year = table.Column<int>(type: "int", nullable: false),
                    IsVisible = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    Status = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BillPeriod", x => x.PeriodId);
                });

            migrationBuilder.CreateTable(
                name: "BookAuthors",
                columns: table => new
                {
                    AuthorId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    AuthorName = table.Column<string>(type: "text", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BookAuthors", x => x.AuthorId);
                });

            migrationBuilder.CreateTable(
                name: "BookCategories",
                columns: table => new
                {
                    BookCategoryId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    ParentCategoryId = table.Column<int>(type: "int", nullable: true),
                    CategoryName = table.Column<string>(type: "text", nullable: true),
                    SectionCode = table.Column<string>(type: "text", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BookCategories", x => x.BookCategoryId);
                });

            migrationBuilder.CreateTable(
                name: "BookShelfs",
                columns: table => new
                {
                    ShelfId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    ShelfName = table.Column<string>(type: "text", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BookShelfs", x => x.ShelfId);
                });

            migrationBuilder.CreateTable(
                name: "Calender",
                columns: table => new
                {
                    CalendarDate = table.Column<DateTime>(type: "date", nullable: false),
                    IsHoliday = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    EventDtlsId = table.Column<int>(type: "int", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Calender", x => x.CalendarDate);
                });

            migrationBuilder.CreateTable(
                name: "Campus",
                columns: table => new
                {
                    CampusId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    CampusName = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false),
                    CampusTitle = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false),
                    HoldingNo = table.Column<string>(type: "varchar(20)", maxLength: 20, nullable: true),
                    Street = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    Area = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    City = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    State = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    CountryID = table.Column<int>(type: "int", nullable: true),
                    DistrictID = table.Column<int>(type: "int", nullable: true),
                    CityID = table.Column<int>(type: "int", nullable: true),
                    District = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    PhoneNo = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    MapUrl = table.Column<string>(type: "text", nullable: true),
                    EmailAddress = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    NoOfRooms = table.Column<int>(type: "int", nullable: true),
                    FlexValueId = table.Column<int>(type: "int", nullable: true),
                    Latitude = table.Column<float>(type: "float", nullable: true),
                    Longitude = table.Column<float>(type: "float", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Campus", x => x.CampusId);
                });

            migrationBuilder.CreateTable(
                name: "Career",
                columns: table => new
                {
                    CareerId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    UserId = table.Column<string>(type: "text", nullable: true),
                    EmpFirstName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    EmpMiddleName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    EmpLastName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    BirthDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    FatherName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    MotherName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    NationalId = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    Nationality = table.Column<int>(type: "int", nullable: true),
                    Gender = table.Column<int>(type: "int", nullable: true),
                    Religion = table.Column<int>(type: "int", nullable: true),
                    MaritalStatus = table.Column<int>(type: "int", nullable: true),
                    Mobile = table.Column<string>(type: "varchar(20)", maxLength: 20, nullable: true),
                    Phone = table.Column<string>(type: "varchar(20)", maxLength: 20, nullable: true),
                    Email = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),
                    PresHoldingNo = table.Column<string>(type: "varchar(20)", maxLength: 20, nullable: true),
                    PresStreet = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    PRES_AREA = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    PRES_POLICE_STATION_ID = table.Column<int>(type: "int", nullable: true),
                    PRES_POST_CODE = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: true),
                    PRES_CITY_ID = table.Column<int>(type: "int", nullable: true),
                    PRES_DIVISION_ID = table.Column<int>(type: "int", nullable: true),
                    PRES_COUNTRY_ID = table.Column<int>(type: "int", nullable: true),
                    PERM_HOLDING_NO = table.Column<string>(type: "varchar(20)", maxLength: 20, nullable: true),
                    PERM_STREET = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    PERM_AREA = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    PERM_POLICE_STATION_ID = table.Column<int>(type: "int", nullable: true),
                    PERM_POST_CODE = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: true),
                    PERM_CITY_ID = table.Column<int>(type: "int", nullable: true),
                    PERM_DIVISION_ID = table.Column<int>(type: "int", nullable: true),
                    PERM_COUNTRY_ID = table.Column<int>(type: "int", nullable: true),
                    CAREER_OBJECTIVE = table.Column<string>(type: "varchar(500)", maxLength: 500, nullable: true),
                    PRESENT_SALARY = table.Column<double>(type: "double", nullable: true),
                    EXPECTED_SALARY = table.Column<double>(type: "double", nullable: true),
                    JOB_LEVEL = table.Column<int>(type: "int", nullable: true),
                    JOB_NATURE = table.Column<int>(type: "int", nullable: true),
                    CAREER_SUMMERY = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    SPACIAL_QUALIFICATION = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    KEYWORDS = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    CV_LOCATION = table.Column<string>(type: "varchar(200)", maxLength: 200, nullable: true),
                    IMAGE_LOCATION = table.Column<string>(type: "varchar(200)", maxLength: 200, nullable: true),
                    PERMANENT_OFFICE_PHONE = table.Column<string>(type: "text", nullable: true),
                    PERMANENT_EMAIL = table.Column<string>(type: "text", nullable: true),
                    COMPANY_NAME = table.Column<string>(type: "text", nullable: true),
                    COMPANY_BUSINESS = table.Column<string>(type: "text", nullable: true),
                    COMPANY_LOCATION = table.Column<string>(type: "text", nullable: true),
                    DEPARTMENT = table.Column<string>(type: "text", nullable: true),
                    POSITION_HELD = table.Column<string>(type: "text", nullable: true),
                    RESPONSIBILITIES = table.Column<string>(type: "text", nullable: true),
                    EXPERIENCE_CATEGORY_ID = table.Column<int>(type: "int", nullable: true),
                    SKILLS_ID = table.Column<int>(type: "int", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Career", x => x.CareerId);
                });

            migrationBuilder.CreateTable(
                name: "Class",
                columns: table => new
                {
                    ClassId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    ClassName = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    ClassLevel = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: true),
                    MinAge = table.Column<int>(type: "int", nullable: false),
                    MaxAge = table.Column<int>(type: "int", nullable: false),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Class", x => x.ClassId);
                });

            migrationBuilder.CreateTable(
                name: "ClassBook",
                columns: table => new
                {
                    BookId = table.Column<int>(type: "int", nullable: false),
                    ClassId = table.Column<int>(type: "int", nullable: false),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClassBook", x => new { x.BookId, x.ClassId });
                });

            migrationBuilder.CreateTable(
                name: "ClassCourseMaster",
                columns: table => new
                {
                    CourseMasterId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    ClassId = table.Column<int>(type: "int", nullable: true),
                    SectionId = table.Column<int>(type: "int", nullable: true),
                    Shift = table.Column<int>(type: "int", nullable: true),
                    EventId = table.Column<int>(type: "int", nullable: true),
                    ClassDay = table.Column<int>(type: "int", nullable: true),
                    ClassDate = table.Column<DateTime>(type: "date", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClassCourseMaster", x => x.CourseMasterId);
                });

            migrationBuilder.CreateTable(
                name: "ClassCourseSubject",
                columns: table => new
                {
                    CourseMasterId = table.Column<int>(type: "int", nullable: false),
                    SubjectId = table.Column<int>(type: "int", nullable: false),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClassCourseSubject", x => new { x.CourseMasterId, x.SubjectId });
                });

            migrationBuilder.CreateTable(
                name: "ClassRooms",
                columns: table => new
                {
                    ClassRoomHeaderId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    Roomcapacity = table.Column<int>(type: "int", nullable: true),
                    ClassRoomName = table.Column<string>(type: "text", nullable: true),
                    Status = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClassRooms", x => x.ClassRoomHeaderId);
                });

            migrationBuilder.CreateTable(
                name: "ClassRoutine",
                columns: table => new
                {
                    RoutineId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    RoutineDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    SessionId = table.Column<int>(type: "int", nullable: false),
                    EventId = table.Column<int>(type: "int", nullable: false),
                    Period = table.Column<int>(type: "int", nullable: false),
                    ClassId = table.Column<int>(type: "int", nullable: false),
                    SectionId = table.Column<int>(type: "int", nullable: false),
                    CampusId = table.Column<int>(type: "int", nullable: false),
                    Shift = table.Column<int>(type: "int", nullable: false),
                    SubjectId = table.Column<int>(type: "int", nullable: false),
                    SubgroupId = table.Column<int>(type: "int", nullable: true),
                    BookId = table.Column<int>(type: "int", nullable: true),
                    TeacherId = table.Column<int>(type: "int", nullable: true),
                    IsActive = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    ClassCount = table.Column<int>(type: "int", nullable: false),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClassRoutine", x => x.RoutineId);
                });

            migrationBuilder.CreateTable(
                name: "ClassSubjectWiseExamType",
                columns: table => new
                {
                    ClassSubjectWiseExamTypeId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    ClassId = table.Column<int>(type: "int", nullable: false),
                    ExamId = table.Column<int>(type: "int", nullable: false),
                    SubjectId = table.Column<int>(type: "int", nullable: false),
                    ExamTypeId = table.Column<int>(type: "int", nullable: false),
                    BaseMark = table.Column<decimal>(type: "decimal(18, 2)", nullable: false),
                    PassMark = table.Column<decimal>(type: "decimal(18, 2)", nullable: false),
                    SessionId = table.Column<int>(type: "int", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClassSubjectWiseExamType", x => x.ClassSubjectWiseExamTypeId);
                });

            migrationBuilder.CreateTable(
                name: "Compensations",
                columns: table => new
                {
                    CompensationId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    EmployeeHeaderId = table.Column<int>(type: "int", nullable: false),
                    Time = table.Column<string>(type: "text", nullable: true),
                    ApprovalDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    Rate = table.Column<decimal>(type: "decimal(18, 2)", nullable: false),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Compensations", x => x.CompensationId);
                });

            migrationBuilder.CreateTable(
                name: "Devices",
                columns: table => new
                {
                    DeviceHeaderId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    DeviceCode = table.Column<int>(type: "int", nullable: false),
                    DeviceName = table.Column<string>(type: "text", nullable: true),
                    DeviceDetails = table.Column<string>(type: "text", nullable: true),
                    DeviceStatus = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Devices", x => x.DeviceHeaderId);
                });

            migrationBuilder.CreateTable(
                name: "EmergencyMedicalAction",
                columns: table => new
                {
                    EmergencyMedicalActionId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(type: "varchar(256)", maxLength: 256, nullable: true),
                    Description = table.Column<string>(type: "text", nullable: true),
                    MedicalHistoryId = table.Column<int>(type: "int", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmergencyMedicalAction", x => x.EmergencyMedicalActionId);
                });

            migrationBuilder.CreateTable(
                name: "EmployeeLeaveInfo",
                columns: table => new
                {
                    LeaveInfoId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    EmployeeHeaderId = table.Column<int>(type: "int", nullable: false),
                    LeaveTypeId = table.Column<int>(type: "int", nullable: false),
                    Balance = table.Column<decimal>(type: "decimal(18, 2)", nullable: false),
                    LeaveApplied = table.Column<decimal>(type: "decimal(18, 2)", nullable: false),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmployeeLeaveInfo", x => x.LeaveInfoId);
                });

            migrationBuilder.CreateTable(
                name: "EmployeeManager",
                columns: table => new
                {
                    EmpHeaderId = table.Column<int>(type: "int", nullable: false),
                    ManagerId = table.Column<int>(type: "int", nullable: false),
                    ManagerType = table.Column<int>(type: "int", nullable: false),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmployeeManager", x => new { x.EmpHeaderId, x.ManagerId });
                });

            migrationBuilder.CreateTable(
                name: "EventAndHolidayPlanners",
                columns: table => new
                {
                    EventHeaderId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    EventName = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false),
                    EventShortName = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    EventDescription = table.Column<string>(type: "varchar(300)", maxLength: 300, nullable: true),
                    StartDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    EndDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    ThemeColor = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: true),
                    IsFullDay = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    EventTypeId = table.Column<int>(type: "int", nullable: false),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EventAndHolidayPlanners", x => x.EventHeaderId);
                });

            migrationBuilder.CreateTable(
                name: "Exam",
                columns: table => new
                {
                    ExamId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    ExamName = table.Column<string>(type: "text", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Exam", x => x.ExamId);
                });

            migrationBuilder.CreateTable(
                name: "ExamGrade",
                columns: table => new
                {
                    GradeId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    Description = table.Column<string>(type: "text", nullable: true),
                    ShortDesc = table.Column<string>(type: "text", nullable: true),
                    AltName = table.Column<string>(type: "text", nullable: true),
                    FromScore = table.Column<decimal>(type: "decimal(18, 2)", nullable: false),
                    ToScore = table.Column<decimal>(type: "decimal(18, 2)", nullable: false),
                    Point = table.Column<string>(type: "text", nullable: true),
                    StartDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EndDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExamGrade", x => x.GradeId);
                });

            migrationBuilder.CreateTable(
                name: "ExamPublishInfos",
                columns: table => new
                {
                    PublishInfoId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    SessionId = table.Column<int>(type: "int", nullable: false),
                    ClassId = table.Column<int>(type: "int", nullable: false),
                    SubjectId = table.Column<int>(type: "int", nullable: false),
                    SectionId = table.Column<int>(type: "int", nullable: false),
                    PublishBy = table.Column<int>(type: "int", nullable: true),
                    ExamId = table.Column<int>(type: "int", nullable: false),
                    IsPublished = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExamPublishInfos", x => x.PublishInfoId);
                });

            migrationBuilder.CreateTable(
                name: "ExamRoomInvestigators",
                columns: table => new
                {
                    ExamRoomInvestigatorId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    SessionId = table.Column<int>(type: "int", nullable: false),
                    ExamId = table.Column<int>(type: "int", nullable: false),
                    RoomId = table.Column<int>(type: "int", nullable: false),
                    IsPublished = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExamRoomInvestigators", x => x.ExamRoomInvestigatorId);
                });

            migrationBuilder.CreateTable(
                name: "ExamSchedules",
                columns: table => new
                {
                    ExamScheduleId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    SessionId = table.Column<int>(type: "int", nullable: false),
                    ExamId = table.Column<int>(type: "int", nullable: false),
                    RoomIds = table.Column<string>(type: "text", nullable: true),
                    IsExamSeatPlanPublished = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    IsExamRoutinePublished = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExamSchedules", x => x.ExamScheduleId);
                });

            migrationBuilder.CreateTable(
                name: "ExamType",
                columns: table => new
                {
                    ExamTypeId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    ExamTypeName = table.Column<string>(type: "text", nullable: true),
                    ParentExamTypeId = table.Column<int>(type: "int", nullable: true),
                    SubExamMarkBase = table.Column<int>(type: "int", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExamType", x => x.ExamTypeId);
                });

            migrationBuilder.CreateTable(
                name: "FeeCategory",
                columns: table => new
                {
                    FeeCategoryId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    FeeCategoryName = table.Column<string>(type: "text", nullable: true),
                    ReceiptPrefix = table.Column<string>(type: "text", nullable: true),
                    Description = table.Column<string>(type: "text", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FeeCategory", x => x.FeeCategoryId);
                });

            migrationBuilder.CreateTable(
                name: "FndFlexValueSet",
                columns: table => new
                {
                    FlexValueSetId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    FlexValueSetName = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false),
                    FlexValueShortName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: false),
                    ModuleId = table.Column<int>(type: "int", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FndFlexValueSet", x => x.FlexValueSetId);
                });

            migrationBuilder.CreateTable(
                name: "FndMenu",
                columns: table => new
                {
                    MenuId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    MenuName = table.Column<string>(type: "text", nullable: false),
                    Url = table.Column<string>(type: "text", nullable: false),
                    Sequence = table.Column<int>(type: "int", nullable: false),
                    IsActive = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    ParentMenuId = table.Column<int>(type: "int", nullable: true),
                    RolesId = table.Column<int>(type: "int", nullable: true),
                    GroupId = table.Column<int>(type: "int", nullable: true),
                    FndmenuMenuId = table.Column<int>(type: "int", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FndMenu", x => x.MenuId);
                    table.ForeignKey(
                        name: "FK_FndMenu_FndMenu_FndmenuMenuId",
                        column: x => x.FndmenuMenuId,
                        principalTable: "FndMenu",
                        principalColumn: "MenuId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "FndMenuGroups",
                columns: table => new
                {
                    MenuGroupId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    GroupSerial = table.Column<int>(type: "int", nullable: false),
                    GroupName = table.Column<string>(type: "text", nullable: true),
                    Status = table.Column<int>(type: "int", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FndMenuGroups", x => x.MenuGroupId);
                });

            migrationBuilder.CreateTable(
                name: "FndMenuPage",
                columns: table => new
                {
                    MenuId = table.Column<int>(type: "int", nullable: false),
                    PageId = table.Column<int>(type: "int", nullable: false),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FndMenuPage", x => new { x.MenuId, x.PageId });
                });

            migrationBuilder.CreateTable(
                name: "FndModule",
                columns: table => new
                {
                    ModuleId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    ModuleName = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false),
                    ModuleShortName = table.Column<string>(type: "varchar(20)", maxLength: 20, nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FndModule", x => x.ModuleId);
                });

            migrationBuilder.CreateTable(
                name: "FndPage",
                columns: table => new
                {
                    PageId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    PageName = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: false),
                    Url = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),
                    PageHeading = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),
                    ModuleId = table.Column<int>(type: "int", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FndPage", x => x.PageId);
                });

            migrationBuilder.CreateTable(
                name: "FndPagePics",
                columns: table => new
                {
                    PicId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    PicName = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false),
                    PageId = table.Column<int>(type: "int", nullable: true),
                    Url = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),
                    Position = table.Column<int>(type: "int", nullable: true),
                    XPosition = table.Column<string>(type: "varchar(6)", maxLength: 6, nullable: true),
                    YPosition = table.Column<string>(type: "varchar(6)", maxLength: 6, nullable: true),
                    OrderBy = table.Column<int>(type: "int", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FndPagePics", x => x.PicId);
                });

            migrationBuilder.CreateTable(
                name: "FndPeriods",
                columns: table => new
                {
                    PeriodId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    PeriodName = table.Column<string>(type: "varchar(256)", maxLength: 256, nullable: false),
                    Description = table.Column<string>(type: "varchar(256)", maxLength: 256, nullable: true),
                    StartDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    EndDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    BillingLastDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    Status = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: false),
                    PeriodYear = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),
                    PeriodTypeId = table.Column<int>(type: "int", nullable: true),
                    SessionId = table.Column<int>(type: "int", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FndPeriods", x => x.PeriodId);
                });

            migrationBuilder.CreateTable(
                name: "FndResponceMenu",
                columns: table => new
                {
                    ResponId = table.Column<int>(type: "int", nullable: false),
                    MenuId = table.Column<int>(type: "int", nullable: false),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FndResponceMenu", x => new { x.ResponId, x.MenuId });
                });

            migrationBuilder.CreateTable(
                name: "FndResponsibility",
                columns: table => new
                {
                    RESPON_ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    ResponName = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FndResponsibility", x => x.RESPON_ID);
                });

            migrationBuilder.CreateTable(
                name: "FndUserPage",
                columns: table => new
                {
                    UserId = table.Column<int>(type: "int", nullable: false),
                    PageId = table.Column<int>(type: "int", nullable: false),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FndUserPage", x => new { x.UserId, x.PageId });
                });

            migrationBuilder.CreateTable(
                name: "FndUserResponce",
                columns: table => new
                {
                    UserId = table.Column<int>(type: "int", nullable: false),
                    ResponId = table.Column<int>(type: "int", nullable: false),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FndUserResponce", x => new { x.UserId, x.ResponId });
                });

            migrationBuilder.CreateTable(
                name: "FndUsers",
                columns: table => new
                {
                    UserId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    UserName = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: false),
                    Pwd = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: false),
                    ReferenceId = table.Column<int>(type: "int", nullable: true),
                    UserType = table.Column<string>(type: "varchar(3)", maxLength: 3, nullable: true),
                    PasswdHints = table.Column<string>(type: "varchar(20)", maxLength: 20, nullable: true),
                    SecurityQuestion = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),
                    StartDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EndDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    Status = table.Column<string>(type: "varchar(20)", maxLength: 20, nullable: true),
                    SecurityAnswer = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FndUsers", x => x.UserId);
                });

            migrationBuilder.CreateTable(
                name: "Gallery",
                columns: table => new
                {
                    GalleryId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    GalleryTitle = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),
                    GalleryFor = table.Column<int>(type: "int", nullable: true),
                    ReferenceId = table.Column<int>(type: "int", nullable: true),
                    GalleryStatus = table.Column<int>(type: "int", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Gallery", x => x.GalleryId);
                });

            migrationBuilder.CreateTable(
                name: "GeneralInfoType",
                columns: table => new
                {
                    InfoTypeId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    InfoType = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GeneralInfoType", x => x.InfoTypeId);
                });

            migrationBuilder.CreateTable(
                name: "HelpfulResouce",
                columns: table => new
                {
                    HelpfulResourceId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),
                    Url = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),
                    ParentId = table.Column<int>(type: "int", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HelpfulResouce", x => x.HelpfulResourceId);
                });

            migrationBuilder.CreateTable(
                name: "HrAttendenceFix",
                columns: table => new
                {
                    FixId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    EmpHeaderId = table.Column<int>(type: "int", nullable: false),
                    PunchDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    PunchTime = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: false),
                    OutTime = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: false),
                    DoorNumber = table.Column<int>(type: "int", nullable: false),
                    GateNumber = table.Column<int>(type: "int", nullable: true),
                    CardNumber = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: false),
                    Status = table.Column<int>(type: "int", nullable: false),
                    ApprovedBy = table.Column<int>(type: "int", nullable: true),
                    RequestBy = table.Column<int>(type: "int", nullable: true),
                    IsProcessed = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HrAttendenceFix", x => x.FixId);
                });

            migrationBuilder.CreateTable(
                name: "HrBillPeriodWisePayRollIssueDates",
                columns: table => new
                {
                    PayRollId = table.Column<int>(type: "int", nullable: false),
                    PeriodId = table.Column<int>(type: "int", nullable: false),
                    IssueDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HrBillPeriodWisePayRollIssueDates", x => new { x.PayRollId, x.PeriodId });
                });

            migrationBuilder.CreateTable(
                name: "HrDepartment",
                columns: table => new
                {
                    DepartmentId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    DepartmentName = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    DepartmentShortName = table.Column<string>(type: "varchar(5)", maxLength: 5, nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HrDepartment", x => x.DepartmentId);
                });

            migrationBuilder.CreateTable(
                name: "HrDesigGrade",
                columns: table => new
                {
                    GradeId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    GradeName = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    StartAmt = table.Column<int>(type: "int", nullable: true),
                    EndAmt = table.Column<int>(type: "int", nullable: true),
                    DesignationId = table.Column<int>(type: "int", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HrDesigGrade", x => x.GradeId);
                });

            migrationBuilder.CreateTable(
                name: "HrEmployee",
                columns: table => new
                {
                    EmpHeaderId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    EmpId = table.Column<string>(type: "varchar(15)", maxLength: 15, nullable: false),
                    EmpFirstName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: false),
                    EmpMiddleName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    EmpLastName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    EmpNickName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    BusinessGroupId = table.Column<int>(type: "int", nullable: true),
                    ApplicantId = table.Column<int>(type: "int", nullable: true),
                    DepartmentId = table.Column<int>(type: "int", nullable: true),
                    DesignationId = table.Column<int>(type: "int", nullable: true),
                    PunchCardId = table.Column<string>(type: "varchar(20)", maxLength: 20, nullable: true),
                    StartTime = table.Column<string>(type: "varchar(20)", maxLength: 20, nullable: false),
                    EndTime = table.Column<string>(type: "varchar(20)", maxLength: 20, nullable: false),
                    JoinningDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    JobEndDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    IsPermenent = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    Status = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    BirthDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    DeathDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    JobLocationId = table.Column<int>(type: "int", nullable: true),
                    BaseOfficeId = table.Column<int>(type: "int", nullable: true),
                    UnitId = table.Column<int>(type: "int", nullable: true),
                    InternalLocationId = table.Column<int>(type: "int", nullable: true),
                    PresHoldingNo = table.Column<string>(type: "varchar(20)", maxLength: 20, nullable: true),
                    PresStreet = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    PresArea = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    PresThanaId = table.Column<int>(type: "int", nullable: true),
                    PresPostCode = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: true),
                    PresCityId = table.Column<int>(type: "int", nullable: true),
                    PresDivisionId = table.Column<int>(type: "int", nullable: true),
                    PresCountryId = table.Column<int>(type: "int", nullable: true),
                    PermHoldingNo = table.Column<string>(type: "varchar(20)", maxLength: 20, nullable: true),
                    PermStreet = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    PermArea = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    PermThanaId = table.Column<int>(type: "int", nullable: true),
                    PermPostCode = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: true),
                    PermCityId = table.Column<int>(type: "int", nullable: true),
                    PermDivisionId = table.Column<int>(type: "int", nullable: true),
                    PermCountryId = table.Column<int>(type: "int", nullable: true),
                    Gender = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    Religion = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    PlaceOfBirth = table.Column<string>(type: "varchar(60)", maxLength: 60, nullable: true),
                    BirthCountry = table.Column<int>(type: "int", nullable: true),
                    BirthCity = table.Column<int>(type: "int", nullable: true),
                    Nationality = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    PassportNo = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    NationalId = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    SocialSecurityId = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    DrivingLicenseNo = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    BloodGroup = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: true),
                    MaritalStatus = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: true),
                    IsStudent = table.Column<string>(type: "varchar(1)", maxLength: 1, nullable: true),
                    WorkPhone = table.Column<string>(type: "varchar(20)", maxLength: 20, nullable: true),
                    Mobile = table.Column<string>(type: "varchar(20)", maxLength: 20, nullable: true),
                    Phone = table.Column<string>(type: "varchar(20)", maxLength: 20, nullable: true),
                    Email = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),
                    Email2 = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),
                    FatherFirstName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    FatherMiddleName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    FatherLastName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    FatherContactNo = table.Column<string>(type: "varchar(20)", maxLength: 20, nullable: true),
                    FatherNationalId = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    MotherFirstName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    MotherMiddleName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    MotherLastName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    MotherContactNo = table.Column<string>(type: "varchar(20)", maxLength: 20, nullable: true),
                    MotherNationalId = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    SpouseFirstName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    SpouseMiddleName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    SpouseLastName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    SpouseContactNo = table.Column<string>(type: "varchar(20)", maxLength: 20, nullable: true),
                    SpouseNationalId = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    NoOfDependent = table.Column<int>(type: "int", nullable: true),
                    BackgroundCheckDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    BackgroundCheckBy = table.Column<int>(type: "int", nullable: true),
                    BackgroundCheckStatus = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    LastMedicalStatus = table.Column<string>(type: "varchar(150)", maxLength: 150, nullable: true),
                    LastMedicalCheckDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    MedicalHistory = table.Column<string>(type: "varchar(500)", maxLength: 500, nullable: true),
                    Signature = table.Column<string>(type: "varchar(150)", maxLength: 150, nullable: true),
                    ImgUrl = table.Column<string>(type: "varchar(150)", maxLength: 150, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),
                    Attribute7 = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),
                    Attribute8 = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),
                    Attribute9 = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),
                    Attribute10 = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),
                    Attribute11 = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),
                    Attribute12 = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),
                    Attribute13 = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),
                    Attribute14 = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),
                    Attribute15 = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),
                    PositionId = table.Column<int>(type: "int", nullable: true),
                    UserTypeId = table.Column<int>(type: "int", nullable: true),
                    GenderId = table.Column<int>(type: "int", nullable: true),
                    UserName = table.Column<string>(type: "text", nullable: true),
                    DeviceId = table.Column<int>(type: "int", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HrEmployee", x => x.EmpHeaderId);
                });

            migrationBuilder.CreateTable(
                name: "HrEmployeeHierarcy",
                columns: table => new
                {
                    EmpHeaderId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    SuperiorId = table.Column<int>(type: "int", nullable: true),
                    PositionStructureId = table.Column<int>(type: "int", nullable: true),
                    EmployeePositionId = table.Column<int>(type: "int", nullable: true),
                    SuperiorPositionId = table.Column<int>(type: "int", nullable: true),
                    SuperiorLevel = table.Column<int>(type: "int", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HrEmployeeHierarcy", x => x.EmpHeaderId);
                });

            migrationBuilder.CreateTable(
                name: "HrEmployeeLeaveApplication",
                columns: table => new
                {
                    ApplicationId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    EmpHeaderId = table.Column<int>(type: "int", nullable: false),
                    LeaveCategoryId = table.Column<int>(type: "int", nullable: false),
                    LeaveStartDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    LeaveEndDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    LeaveApplyDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    Subject = table.Column<string>(type: "text", nullable: true),
                    Comment = table.Column<string>(type: "text", nullable: true),
                    LeaveStatus = table.Column<int>(type: "int", nullable: false),
                    IsOnDuty = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    Remarks = table.Column<string>(type: "text", nullable: true),
                    Attachment1 = table.Column<string>(type: "text", nullable: true),
                    Attachment2 = table.Column<string>(type: "text", nullable: true),
                    Attachment3 = table.Column<string>(type: "text", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HrEmployeeLeaveApplication", x => x.ApplicationId);
                });

            migrationBuilder.CreateTable(
                name: "HrEmployeeLeaveCounts",
                columns: table => new
                {
                    LeaveCountId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    ApplicationId = table.Column<int>(type: "int", nullable: false),
                    LeaveQuantity = table.Column<string>(type: "text", nullable: true),
                    EmpTotalLeave = table.Column<int>(type: "int", nullable: false),
                    EmpLeaveRemain = table.Column<int>(type: "int", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HrEmployeeLeaveCounts", x => x.LeaveCountId);
                });

            migrationBuilder.CreateTable(
                name: "HrEmployeePayHeads",
                columns: table => new
                {
                    EmployeePayHeadId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    EmpHeaderId = table.Column<int>(type: "int", nullable: false),
                    PayHeadId = table.Column<int>(type: "int", nullable: false),
                    Unit = table.Column<decimal>(type: "decimal(18, 2)", nullable: false),
                    IsPercentage = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HrEmployeePayHeads", x => x.EmployeePayHeadId);
                });

            migrationBuilder.CreateTable(
                name: "HrEmployeeTiming",
                columns: table => new
                {
                    EmpHeaderId = table.Column<int>(type: "int", nullable: false),
                    WorkingDay = table.Column<string>(type: "varchar(15)", maxLength: 15, nullable: false),
                    StartTime = table.Column<DateTime>(type: "datetime", nullable: false),
                    EndTime = table.Column<DateTime>(type: "datetime", nullable: false),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HrEmployeeTiming", x => new { x.EmpHeaderId, x.WorkingDay, x.StartTime, x.EndTime });
                });

            migrationBuilder.CreateTable(
                name: "HrLeaveInfo",
                columns: table => new
                {
                    EmployeeTypeId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    LeaveTypeId = table.Column<int>(type: "int", nullable: true),
                    NoOfLeave = table.Column<int>(type: "int", nullable: true),
                    Incashable = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    CashAgainst = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HrLeaveInfo", x => x.EmployeeTypeId);
                });

            migrationBuilder.CreateTable(
                name: "HrPayHeads",
                columns: table => new
                {
                    PayHeadId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    PayHeadName = table.Column<string>(type: "text", nullable: true),
                    PayHeadShortValue = table.Column<string>(type: "text", nullable: true),
                    PayHeadType = table.Column<int>(type: "int", nullable: false),
                    Description = table.Column<string>(type: "text", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HrPayHeads", x => x.PayHeadId);
                });

            migrationBuilder.CreateTable(
                name: "HrPayRollPayHeads",
                columns: table => new
                {
                    PayRollPayHeadId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    PayRollId = table.Column<int>(type: "int", nullable: false),
                    PayHeadId = table.Column<int>(type: "int", nullable: false),
                    Unit = table.Column<decimal>(type: "decimal(18, 2)", nullable: false),
                    IsPercentage = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HrPayRollPayHeads", x => x.PayRollPayHeadId);
                });

            migrationBuilder.CreateTable(
                name: "HrPayRolls",
                columns: table => new
                {
                    PayRollId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    PayRollName = table.Column<string>(type: "text", nullable: true),
                    PaymentTypeId = table.Column<int>(type: "int", nullable: true),
                    IssueDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HrPayRolls", x => x.PayRollId);
                });

            migrationBuilder.CreateTable(
                name: "HrPaySlipTransactionMasters",
                columns: table => new
                {
                    PaySlipMasterHeaderId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    PeriodId = table.Column<int>(type: "int", nullable: false),
                    EmpHeaderId = table.Column<int>(type: "int", nullable: false),
                    PositionId = table.Column<int>(type: "int", nullable: false),
                    PayrollId = table.Column<int>(type: "int", nullable: false),
                    TotalAmountPayable = table.Column<decimal>(type: "decimal(18, 2)", nullable: false),
                    TotalAdditionAmount = table.Column<decimal>(type: "decimal(18, 2)", nullable: false),
                    TotalDeductionAmount = table.Column<decimal>(type: "decimal(18, 2)", nullable: false),
                    PaidStatus = table.Column<int>(type: "int", nullable: false),
                    PayslipCurrentStatus = table.Column<int>(type: "int", nullable: false),
                    PayslipId = table.Column<string>(type: "text", nullable: true),
                    GenerationDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HrPaySlipTransactionMasters", x => x.PaySlipMasterHeaderId);
                });

            migrationBuilder.CreateTable(
                name: "HrPositionLeaveCategories",
                columns: table => new
                {
                    PositionLeaveCategoryId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    PositionId = table.Column<int>(type: "int", nullable: false),
                    LeaveCategoryId = table.Column<int>(type: "int", nullable: false),
                    LeaveCount = table.Column<int>(type: "int", nullable: false),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HrPositionLeaveCategories", x => x.PositionLeaveCategoryId);
                });

            migrationBuilder.CreateTable(
                name: "HrPositions",
                columns: table => new
                {
                    PostionId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    PostionName = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false),
                    StartDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EndDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    Status = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    LocationId = table.Column<int>(type: "int", nullable: true),
                    OrganizationId = table.Column<int>(type: "int", nullable: true),
                    SuccessorPositionId = table.Column<int>(type: "int", nullable: true),
                    SupervisorPositionId = table.Column<int>(type: "int", nullable: true),
                    EntryGradeId = table.Column<int>(type: "int", nullable: true),
                    EntryLevel = table.Column<int>(type: "int", nullable: true),
                    PriorPositionId = table.Column<int>(type: "int", nullable: true),
                    WorkingHours = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    StartTime = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    EndTime = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    ProvisionPeriod = table.Column<int>(type: "int", nullable: true),
                    PostionTypeId = table.Column<int>(type: "int", nullable: true),
                    HeadCount = table.Column<int>(type: "int", nullable: true),
                    FTE = table.Column<decimal>(type: "decimal(18, 2)", nullable: true),
                    BergainningUnit = table.Column<decimal>(type: "decimal(18, 2)", nullable: true),
                    earliestHiringdate = table.Column<DateTime>(type: "datetime", nullable: true),
                    PayrollId = table.Column<int>(type: "int", nullable: false),
                    SalaryBasis = table.Column<decimal>(type: "decimal(18, 2)", nullable: true),
                    StatusId = table.Column<int>(type: "int", nullable: true),
                    IsPermanent = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    IsSeasonal = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HrPositions", x => x.PostionId);
                });

            migrationBuilder.CreateTable(
                name: "HrPositionStructure",
                columns: table => new
                {
                    PositionStructuresId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    PositionStructureName = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false),
                    BusinessGroupId = table.Column<int>(type: "int", nullable: true),
                    StartDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EndDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    Status = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HrPositionStructure", x => x.PositionStructuresId);
                });

            migrationBuilder.CreateTable(
                name: "HrShift",
                columns: table => new
                {
                    ShiftId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    ShiftName = table.Column<string>(type: "text", nullable: true),
                    ShiftShortName = table.Column<string>(type: "text", nullable: true),
                    ShiftColor = table.Column<string>(type: "text", nullable: true),
                    DayOfWeek = table.Column<string>(type: "text", nullable: true),
                    DayOfMonth = table.Column<string>(type: "text", nullable: true),
                    WeekOfMonth = table.Column<string>(type: "text", nullable: true),
                    ShiftStartTime = table.Column<string>(type: "text", nullable: true),
                    ShiftEndTime = table.Column<string>(type: "text", nullable: true),
                    StartDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EndDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    Status = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HrShift", x => x.ShiftId);
                });

            migrationBuilder.CreateTable(
                name: "Job",
                columns: table => new
                {
                    JobId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    CategoryIdFlex = table.Column<int>(type: "int", nullable: false),
                    JobTypeIdFlex = table.Column<int>(type: "int", nullable: false),
                    PostName = table.Column<string>(type: "varchar(256)", maxLength: 256, nullable: true),
                    StartDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EndDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    NoOfVacancies = table.Column<int>(type: "int", nullable: true),
                    JobDescription = table.Column<string>(type: "text", nullable: true),
                    EduReq = table.Column<string>(type: "text", nullable: true),
                    ExpReq = table.Column<string>(type: "text", nullable: true),
                    AditionalReq = table.Column<string>(type: "text", nullable: true),
                    JobLocation = table.Column<string>(type: "varchar(256)", maxLength: 256, nullable: true),
                    SalaryRange = table.Column<string>(type: "varchar(256)", maxLength: 256, nullable: true),
                    IsActivate = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Job", x => x.JobId);
                });

            migrationBuilder.CreateTable(
                name: "JobApplicant",
                columns: table => new
                {
                    JobApplicantId = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    EmpFirstName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: false),
                    EmpMiddleName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    EmpLastName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    Email = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false),
                    Mobile = table.Column<string>(type: "varchar(20)", maxLength: 20, nullable: false),
                    Qualification = table.Column<int>(type: "int", nullable: true),
                    Catagory = table.Column<int>(type: "int", nullable: true),
                    Section = table.Column<int>(type: "int", nullable: true),
                    UserId = table.Column<int>(type: "int", nullable: false),
                    BirthDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    FatherName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    MotherName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    NationalId = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    Nationality = table.Column<int>(type: "int", nullable: true),
                    Gender = table.Column<int>(type: "int", nullable: true),
                    Religion = table.Column<int>(type: "int", nullable: true),
                    MaritalStatus = table.Column<int>(type: "int", nullable: true),
                    PermanentHomePhone = table.Column<string>(type: "varchar(20)", maxLength: 20, nullable: true),
                    PresHoldingNo = table.Column<string>(type: "varchar(20)", maxLength: 20, nullable: true),
                    PresStreet = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    PresArea = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    PresPoliceStationId = table.Column<int>(type: "int", nullable: true),
                    PresPostCode = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: true),
                    PresCityId = table.Column<int>(type: "int", nullable: true),
                    PresDivisionId = table.Column<int>(type: "int", nullable: true),
                    PresCountryId = table.Column<int>(type: "int", nullable: true),
                    PermHoldingNo = table.Column<string>(type: "varchar(20)", maxLength: 20, nullable: true),
                    PermStreet = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    PermArea = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    PermPoliceStationId = table.Column<int>(type: "int", nullable: true),
                    PermPostCode = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: true),
                    PermCityId = table.Column<int>(type: "int", nullable: true),
                    PermDivisionId = table.Column<int>(type: "int", nullable: true),
                    PermCountryId = table.Column<int>(type: "int", nullable: true),
                    CareerObjective = table.Column<string>(type: "varchar(500)", maxLength: 500, nullable: true),
                    PresentSalary = table.Column<double>(type: "double", nullable: true),
                    ExpectedSalary = table.Column<double>(type: "double", nullable: true),
                    JobLevel = table.Column<int>(type: "int", nullable: true),
                    JobNature = table.Column<int>(type: "int", nullable: true),
                    CareerSummery = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    SpacialQualification = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CvLocation = table.Column<string>(type: "varchar(200)", maxLength: 200, nullable: true),
                    ImageLocation = table.Column<string>(type: "varchar(200)", maxLength: 200, nullable: true),
                    PermanentOfficePhone = table.Column<string>(type: "text", nullable: true),
                    PermanentEmail = table.Column<string>(type: "text", nullable: true),
                    CompanyName = table.Column<string>(type: "text", nullable: true),
                    CompanyBusiness = table.Column<string>(type: "text", nullable: true),
                    CompanyLocation = table.Column<string>(type: "text", nullable: true),
                    Department = table.Column<string>(type: "text", nullable: true),
                    PositionHeld = table.Column<string>(type: "text", nullable: true),
                    Responsibilities = table.Column<string>(type: "text", nullable: true),
                    EmpHistFromDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EmpHistToDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LevelOfEducation = table.Column<int>(type: "int", nullable: false),
                    ExamDegreeeTitle = table.Column<string>(type: "text", nullable: true),
                    ConcentrationOrMajorGrpoup = table.Column<string>(type: "text", nullable: true),
                    AcademicInstituteName = table.Column<string>(type: "text", nullable: true),
                    Result = table.Column<string>(type: "text", nullable: true),
                    GradeCgpa = table.Column<string>(type: "text", nullable: true),
                    GradeScale = table.Column<string>(type: "text", nullable: true),
                    DivisionResult = table.Column<string>(type: "text", nullable: true),
                    YearOfPassing = table.Column<string>(type: "text", nullable: true),
                    AcademicDuration = table.Column<string>(type: "text", nullable: true),
                    Achievement = table.Column<string>(type: "text", nullable: true),
                    TrainingTitle = table.Column<string>(type: "text", nullable: true),
                    TrainingInstituteName = table.Column<string>(type: "text", nullable: true),
                    TopicCovered = table.Column<string>(type: "text", nullable: true),
                    TrainingCountry = table.Column<int>(type: "int", nullable: true),
                    TrainingLocation = table.Column<string>(type: "text", nullable: true),
                    TrainingYear = table.Column<string>(type: "text", nullable: true),
                    TrainingDuration = table.Column<string>(type: "text", nullable: true),
                    ProfessionalCertification = table.Column<string>(type: "text", nullable: true),
                    ProfessionalInstituteName = table.Column<string>(type: "text", nullable: true),
                    ProfessionalLocation = table.Column<string>(type: "text", nullable: true),
                    ProfessionalFromDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    ProfessionalToDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_JobApplicant", x => x.JobApplicantId);
                });

            migrationBuilder.CreateTable(
                name: "Leave",
                columns: table => new
                {
                    LeaveId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    ReferenceId = table.Column<int>(type: "int", nullable: true),
                    LeaveTypeId = table.Column<int>(type: "int", nullable: true),
                    UserType = table.Column<string>(type: "varchar(20)", maxLength: 20, nullable: true),
                    FromDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    ToDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    Reason = table.Column<string>(type: "text", nullable: true),
                    StatusId = table.Column<int>(type: "int", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Leave", x => x.LeaveId);
                });

            migrationBuilder.CreateTable(
                name: "LeaveMaster",
                columns: table => new
                {
                    LeaveMasterId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    EmployeeHeaderId = table.Column<int>(type: "int", nullable: false),
                    LeaveTypeId = table.Column<int>(type: "int", nullable: false),
                    Status = table.Column<int>(type: "int", nullable: false),
                    FromDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    ToDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    FromTime = table.Column<string>(type: "text", nullable: true),
                    ToTime = table.Column<string>(type: "text", nullable: true),
                    Reason = table.Column<string>(type: "text", nullable: true),
                    AttachmentUrl = table.Column<string>(type: "text", nullable: true),
                    LeaveApprover = table.Column<int>(type: "int", nullable: false),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LeaveMaster", x => x.LeaveMasterId);
                });

            migrationBuilder.CreateTable(
                name: "LeaveSetup",
                columns: table => new
                {
                    LeaveId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    LeaveTypeId = table.Column<int>(type: "int", nullable: false),
                    LeaveDesc = table.Column<string>(type: "text", nullable: true),
                    Designation = table.Column<int>(type: "int", nullable: false),
                    NoOfMonth = table.Column<int>(type: "int", nullable: false),
                    EntitleLeave = table.Column<string>(type: "text", nullable: true),
                    LeaveBalance = table.Column<decimal>(type: "decimal(18, 2)", nullable: false),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LeaveSetup", x => x.LeaveId);
                });

            migrationBuilder.CreateTable(
                name: "LogoType",
                columns: table => new
                {
                    LogoTypeId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    Type = table.Column<string>(type: "varchar(150)", maxLength: 150, nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LogoType", x => x.LogoTypeId);
                });

            migrationBuilder.CreateTable(
                name: "MarksheetStatus",
                columns: table => new
                {
                    MarksheetStatusId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    SessionId = table.Column<int>(type: "int", nullable: false),
                    ExamId = table.Column<int>(type: "int", nullable: false),
                    ClassId = table.Column<int>(type: "int", nullable: false),
                    SectionId = table.Column<int>(type: "int", nullable: false),
                    IsPublished = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MarksheetStatus", x => x.MarksheetStatusId);
                });

            migrationBuilder.CreateTable(
                name: "MedicalHistory",
                columns: table => new
                {
                    MedicalHistroyId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    HistoryName = table.Column<string>(type: "varchar(256)", maxLength: 256, nullable: true),
                    UserType = table.Column<string>(type: "varchar(20)", maxLength: 20, nullable: true),
                    ReferenceId = table.Column<int>(type: "int", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MedicalHistory", x => x.MedicalHistroyId);
                });

            migrationBuilder.CreateTable(
                name: "MenuGroup",
                columns: table => new
                {
                    MenuGroupId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    MenuGroupName = table.Column<string>(type: "text", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MenuGroup", x => x.MenuGroupId);
                });

            migrationBuilder.CreateTable(
                name: "MessageAlertMaster",
                columns: table => new
                {
                    AlertId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    AlertName = table.Column<string>(type: "text", nullable: true),
                    GroupIds = table.Column<string>(type: "text", nullable: true),
                    Mesage = table.Column<string>(type: "text", nullable: true),
                    MessageId = table.Column<string>(type: "text", nullable: true),
                    ResponseMessage = table.Column<string>(type: "text", nullable: true),
                    IsError = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    ErrorCode = table.Column<int>(type: "int", nullable: true),
                    SendingDateTime = table.Column<DateTime>(type: "datetime", nullable: false),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MessageAlertMaster", x => x.AlertId);
                });

            migrationBuilder.CreateTable(
                name: "News",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false),
                    NewsTitle = table.Column<string>(type: "varchar(150)", maxLength: 150, nullable: false),
                    Description = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: false),
                    NewsDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_News", x => new { x.Id, x.NewsTitle, x.Description });
                });

            migrationBuilder.CreateTable(
                name: "Notice",
                columns: table => new
                {
                    NoticeId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    NoticeDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    NoticeType = table.Column<int>(type: "int", nullable: false),
                    NoticeFor = table.Column<int>(type: "int", nullable: false),
                    ReferenceId = table.Column<int>(type: "int", nullable: true),
                    Header = table.Column<string>(type: "varchar(500)", maxLength: 500, nullable: true),
                    Body = table.Column<string>(type: "text", nullable: true),
                    HtmlBody = table.Column<string>(type: "text", nullable: true),
                    Tailer = table.Column<string>(type: "varchar(500)", maxLength: 500, nullable: true),
                    CampusId = table.Column<int>(type: "int", nullable: true),
                    CheckBy = table.Column<int>(type: "int", nullable: true),
                    OrderBy = table.Column<int>(type: "int", nullable: true),
                    ApprovedBy = table.Column<int>(type: "int", nullable: true),
                    ClassId = table.Column<int>(type: "int", nullable: true),
                    SectionId = table.Column<int>(type: "int", nullable: true),
                    ShiftId = table.Column<int>(type: "int", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Notice", x => x.NoticeId);
                });

            migrationBuilder.CreateTable(
                name: "NotifyInfos",
                columns: table => new
                {
                    NotifyInfoId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    NotifyFromRefrenceId = table.Column<int>(type: "int", nullable: false),
                    NotifyToHeaderId = table.Column<int>(type: "int", nullable: false),
                    NotifyToUserType = table.Column<int>(type: "int", nullable: false),
                    TemplateId = table.Column<int>(type: "int", nullable: false),
                    Text = table.Column<string>(type: "text", nullable: true),
                    NotifyType = table.Column<int>(type: "int", nullable: false),
                    NotifyWithSms = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    NotifyWithNotification = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    IsSeen = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NotifyInfos", x => x.NotifyInfoId);
                });

            migrationBuilder.CreateTable(
                name: "NotifyTransactionInfos",
                columns: table => new
                {
                    NotifyTransactionInfoId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    NotifyInfoId = table.Column<int>(type: "int", nullable: false),
                    NotifyFromRefrenceId = table.Column<int>(type: "int", nullable: false),
                    NotifyToHeaderId = table.Column<int>(type: "int", nullable: false),
                    NotifyToUserType = table.Column<int>(type: "int", nullable: false),
                    TemplateId = table.Column<int>(type: "int", nullable: false),
                    Text = table.Column<string>(type: "text", nullable: true),
                    NotifyType = table.Column<int>(type: "int", nullable: false),
                    NotifyWithSms = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    NotifyWithNotification = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    IsSeen = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NotifyTransactionInfos", x => x.NotifyTransactionInfoId);
                });

            migrationBuilder.CreateTable(
                name: "OnlineInquiry",
                columns: table => new
                {
                    OnlineInquiryId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(type: "text", nullable: false),
                    EMail = table.Column<string>(type: "text", nullable: false),
                    Phone = table.Column<string>(type: "text", nullable: true),
                    Info = table.Column<int>(type: "int", nullable: false),
                    Subject = table.Column<string>(type: "text", nullable: false),
                    Message = table.Column<string>(type: "text", nullable: false),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OnlineInquiry", x => x.OnlineInquiryId);
                });

            migrationBuilder.CreateTable(
                name: "PaymentGatewaySettings",
                columns: table => new
                {
                    GatewayHeaderId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    MerchantNumber = table.Column<string>(type: "text", nullable: true),
                    APIorURL = table.Column<string>(type: "text", nullable: true),
                    UserName = table.Column<string>(type: "text", nullable: true),
                    Password = table.Column<string>(type: "text", nullable: true),
                    AccessOrAppkey = table.Column<string>(type: "text", nullable: true),
                    SecretkeyOrToken = table.Column<string>(type: "text", nullable: true),
                    CustomerOrClientCode = table.Column<string>(type: "text", nullable: true),
                    PartnerCode = table.Column<string>(type: "text", nullable: true),
                    BankName = table.Column<string>(type: "text", nullable: true),
                    AccountNumber = table.Column<string>(type: "text", nullable: true),
                    GatewayProvider = table.Column<int>(type: "int", nullable: true),
                    Attribute7 = table.Column<string>(type: "text", nullable: true),
                    Attribute8 = table.Column<string>(type: "text", nullable: true),
                    Attribute9 = table.Column<string>(type: "text", nullable: true),
                    Attribute10 = table.Column<string>(type: "text", nullable: true),
                    PaymentStatus = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    ActivationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    ExpiryDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    CompanyId = table.Column<int>(type: "int", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PaymentGatewaySettings", x => x.GatewayHeaderId);
                });

            migrationBuilder.CreateTable(
                name: "PlannerDay",
                columns: table => new
                {
                    DAY_ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    DayName = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: false),
                    IsWeeklyHoliday = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlannerDay", x => x.DAY_ID);
                });

            migrationBuilder.CreateTable(
                name: "PlannerEventClass",
                columns: table => new
                {
                    EventDtlsId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    ClassId = table.Column<int>(type: "int", nullable: false),
                    Shift = table.Column<int>(type: "int", nullable: false),
                    StartDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EndDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlannerEventClass", x => x.EventDtlsId);
                });

            migrationBuilder.CreateTable(
                name: "PlannerEventType",
                columns: table => new
                {
                    EventTypeId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    EventTypeName = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: false),
                    ShortName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlannerEventType", x => x.EventTypeId);
                });

            migrationBuilder.CreateTable(
                name: "PlannerHoliday",
                columns: table => new
                {
                    HolidayId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    SessionId = table.Column<int>(type: "int", nullable: false),
                    TrnsDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EventId = table.Column<int>(type: "int", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlannerHoliday", x => x.HolidayId);
                });

            migrationBuilder.CreateTable(
                name: "PositionChangeRequests",
                columns: table => new
                {
                    PositionChangeRequestId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    EmpHeaderId = table.Column<int>(type: "int", nullable: false),
                    PositionId = table.Column<int>(type: "int", nullable: false),
                    RequestedChangeDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    PositionChangeRequestStatus = table.Column<int>(type: "int", nullable: false),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PositionChangeRequests", x => x.PositionChangeRequestId);
                });

            migrationBuilder.CreateTable(
                name: "ProcessAssessmentMarks",
                columns: table => new
                {
                    ProcesssAssessmentMarksId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    ReportProcessId = table.Column<int>(type: "int", nullable: false),
                    StudentHeaderId = table.Column<int>(type: "int", nullable: false),
                    ProcessLogId = table.Column<int>(type: "int", nullable: false),
                    ClassId = table.Column<int>(type: "int", nullable: false),
                    SessionId = table.Column<int>(type: "int", nullable: false),
                    SectionId = table.Column<int>(type: "int", nullable: false),
                    SubjectName = table.Column<string>(type: "text", nullable: true),
                    SubjectId = table.Column<int>(type: "int", nullable: true),
                    SubgroupName = table.Column<string>(type: "text", nullable: true),
                    TotalTest = table.Column<int>(type: "int", nullable: false),
                    BestCount = table.Column<int>(type: "int", nullable: false),
                    Best = table.Column<string>(type: "text", nullable: true),
                    Total = table.Column<decimal>(type: "decimal(18, 2)", nullable: true),
                    Assesment1 = table.Column<string>(type: "text", nullable: true),
                    Assesment2 = table.Column<string>(type: "text", nullable: true),
                    Assesment3 = table.Column<string>(type: "text", nullable: true),
                    Assesment4 = table.Column<string>(type: "text", nullable: true),
                    Assesment5 = table.Column<string>(type: "text", nullable: true),
                    Assesment6 = table.Column<string>(type: "text", nullable: true),
                    Assesment7 = table.Column<string>(type: "text", nullable: true),
                    Assesment8 = table.Column<string>(type: "text", nullable: true),
                    Assesment9 = table.Column<string>(type: "text", nullable: true),
                    Assesment10 = table.Column<string>(type: "text", nullable: true),
                    SemesterMark = table.Column<decimal>(type: "decimal(18, 2)", nullable: true),
                    TotalObtainedMark = table.Column<decimal>(type: "decimal(18, 2)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProcessAssessmentMarks", x => x.ProcesssAssessmentMarksId);
                });

            migrationBuilder.CreateTable(
                name: "ProcessedMarksheets",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    StudentHeaderId = table.Column<int>(type: "int", nullable: false),
                    SessionId = table.Column<int>(type: "int", nullable: false),
                    ClassId = table.Column<int>(type: "int", nullable: false),
                    SectionId = table.Column<int>(type: "int", nullable: false),
                    CampusId = table.Column<int>(type: "int", nullable: true),
                    ExamId = table.Column<int>(type: "int", nullable: false),
                    SubjectId = table.Column<int>(type: "int", nullable: false),
                    ExamTypeId = table.Column<int>(type: "int", nullable: false),
                    BaseMarks = table.Column<decimal>(type: "decimal(18, 2)", nullable: false),
                    ObtainedMarks = table.Column<decimal>(type: "decimal(18, 2)", nullable: false),
                    ObtainedMarksText = table.Column<string>(type: "text", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProcessedMarksheets", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ProcessLog",
                columns: table => new
                {
                    ProcessLogId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    ProcessTime = table.Column<DateTime>(type: "datetime", nullable: false),
                    ClassId = table.Column<int>(type: "int", nullable: false),
                    SessionId = table.Column<int>(type: "int", nullable: false),
                    EventId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProcessLog", x => x.ProcessLogId);
                });

            migrationBuilder.CreateTable(
                name: "ProcessQueryTableMailGroup",
                columns: table => new
                {
                    QueryId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    AlertName = table.Column<string>(type: "text", nullable: true),
                    MailGroupId = table.Column<int>(type: "int", nullable: false),
                    Query = table.Column<string>(type: "text", nullable: true),
                    Status = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    StartDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EndDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProcessQueryTableMailGroup", x => x.QueryId);
                });

            migrationBuilder.CreateTable(
                name: "ProcessSchedule",
                columns: table => new
                {
                    ScheduleId = table.Column<int>(type: "int", nullable: false),
                    ProcessId = table.Column<int>(type: "int", nullable: false),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProcessSchedule", x => new { x.ScheduleId, x.ProcessId });
                });

            migrationBuilder.CreateTable(
                name: "Publisher",
                columns: table => new
                {
                    PublisherId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    PublisherName = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: false),
                    Address1 = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    Address2 = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    Address3 = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    CountryId = table.Column<int>(type: "int", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Publisher", x => x.PublisherId);
                });

            migrationBuilder.CreateTable(
                name: "RecipientGroup",
                columns: table => new
                {
                    GroupId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    GroupName = table.Column<string>(type: "text", nullable: true),
                    Query = table.Column<string>(type: "text", nullable: true),
                    Status = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    StartDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EndDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    IsValidated = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RecipientGroup", x => x.GroupId);
                });

            migrationBuilder.CreateTable(
                name: "Rejoins",
                columns: table => new
                {
                    RejoinId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    EmpHeaderId = table.Column<int>(type: "int", nullable: false),
                    RejoinDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    AppointmentLetterUrl = table.Column<string>(type: "text", nullable: true),
                    RejoinPositionId = table.Column<int>(type: "int", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Rejoins", x => x.RejoinId);
                });

            migrationBuilder.CreateTable(
                name: "RemarksGrade",
                columns: table => new
                {
                    RemarksId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    Description = table.Column<string>(type: "text", nullable: true),
                    ShortDescription = table.Column<string>(type: "text", nullable: true),
                    AltName = table.Column<string>(type: "text", nullable: true),
                    FromScore = table.Column<decimal>(type: "decimal(18, 2)", nullable: false),
                    ToScore = table.Column<decimal>(type: "decimal(18, 2)", nullable: false),
                    GenderFlag = table.Column<int>(type: "int", nullable: false),
                    ClassId = table.Column<int>(type: "int", nullable: false),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RemarksGrade", x => x.RemarksId);
                });

            migrationBuilder.CreateTable(
                name: "ReportProcess",
                columns: table => new
                {
                    ReportProcessId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    ProcessLogId = table.Column<int>(type: "int", nullable: false),
                    SessionId = table.Column<int>(type: "int", nullable: false),
                    StudentId = table.Column<int>(type: "int", nullable: false),
                    SubjectId = table.Column<int>(type: "int", nullable: false),
                    AssesmentMark = table.Column<decimal>(type: "decimal(18, 2)", nullable: false),
                    SemisterMark = table.Column<decimal>(type: "decimal(18, 2)", nullable: false),
                    TotalMark = table.Column<decimal>(type: "decimal(18, 2)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReportProcess", x => x.ReportProcessId);
                });

            migrationBuilder.CreateTable(
                name: "Resignations",
                columns: table => new
                {
                    ResignationId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    LatterPath = table.Column<string>(type: "text", nullable: true),
                    LatterReceivedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    ResignDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    EmployeeHeaderId = table.Column<int>(type: "int", nullable: false),
                    ResignationType = table.Column<int>(type: "int", nullable: false),
                    Reason = table.Column<string>(type: "text", nullable: false),
                    Activities = table.Column<string>(type: "text", nullable: true),
                    IsResignationApplied = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Resignations", x => x.ResignationId);
                });

            migrationBuilder.CreateTable(
                name: "Roles",
                columns: table => new
                {
                    RoleId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    RoleName = table.Column<string>(type: "text", nullable: false),
                    Description = table.Column<string>(type: "text", nullable: true),
                    Status = table.Column<int>(type: "int", nullable: true),
                    RedirectionUrl = table.Column<string>(type: "text", nullable: true),
                    RoleType = table.Column<int>(type: "int", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Roles", x => x.RoleId);
                });

            migrationBuilder.CreateTable(
                name: "RoomSetups",
                columns: table => new
                {
                    RoomSetupHeaderId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    ClassRoomHeaderId = table.Column<int>(type: "int", nullable: false),
                    NoofColumn = table.Column<int>(type: "int", nullable: true),
                    SetupFor = table.Column<int>(type: "int", nullable: false),
                    SessionId = table.Column<int>(type: "int", nullable: true),
                    ClassId = table.Column<int>(type: "int", nullable: true),
                    SectionId = table.Column<int>(type: "int", nullable: true),
                    ExamId = table.Column<int>(type: "int", nullable: true),
                    ExamTypeId = table.Column<int>(type: "int", nullable: true),
                    SubjectId = table.Column<int>(type: "int", nullable: true),
                    DefaultLayout = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RoomSetups", x => x.RoomSetupHeaderId);
                });

            migrationBuilder.CreateTable(
                name: "Routine",
                columns: table => new
                {
                    RoutineId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    DayId = table.Column<int>(type: "int", nullable: true),
                    RoutineDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    Period = table.Column<int>(type: "int", nullable: true),
                    StartTime = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: false),
                    EndTime = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: false),
                    ClassCountNo = table.Column<int>(type: "int", nullable: false),
                    CampusId = table.Column<int>(type: "int", nullable: true),
                    SubjectId = table.Column<int>(type: "int", nullable: true),
                    BookId = table.Column<int>(type: "int", nullable: true),
                    ClassId = table.Column<int>(type: "int", nullable: true),
                    SECTION_ID = table.Column<int>(type: "int", nullable: true),
                    Shift = table.Column<int>(type: "int", nullable: true),
                    SectionId = table.Column<int>(type: "int", nullable: true),
                    TeacherId = table.Column<int>(type: "int", nullable: true),
                    GroupName = table.Column<string>(type: "varchar(20)", maxLength: 20, nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Routine", x => x.RoutineId);
                });

            migrationBuilder.CreateTable(
                name: "RoutineSetup",
                columns: table => new
                {
                    RoutineSetupId = table.Column<int>(type: "int", nullable: false),
                    ClassId = table.Column<int>(type: "int", nullable: false),
                    SectionId = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: false),
                    Period = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: false),
                    StartTime = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    EndTime = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    Isbreak = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RoutineSetup", x => new { x.RoutineSetupId, x.ClassId, x.SectionId });
                });

            migrationBuilder.CreateTable(
                name: "Schedule",
                columns: table => new
                {
                    ScheduleId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    ScheduleName = table.Column<string>(type: "text", nullable: true),
                    CronTime = table.Column<string>(type: "text", nullable: true),
                    Status = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Schedule", x => x.ScheduleId);
                });

            migrationBuilder.CreateTable(
                name: "School",
                columns: table => new
                {
                    SchoolId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    SchoolName = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: false),
                    SchoolEMail = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: false),
                    SchoolInquiryMail = table.Column<string>(type: "text", nullable: true),
                    SchoolInquiryMailPass = table.Column<string>(type: "text", nullable: true),
                    SchoolTagLine = table.Column<string>(type: "text", nullable: true),
                    Mobile1 = table.Column<string>(type: "text", nullable: true),
                    Mobile2 = table.Column<string>(type: "text", nullable: true),
                    SmsProviderId = table.Column<int>(type: "int", nullable: false),
                    Address1 = table.Column<string>(type: "text", nullable: true),
                    Address2 = table.Column<string>(type: "text", nullable: true),
                    SmsUserName = table.Column<string>(type: "text", nullable: true),
                    SmsPassword = table.Column<string>(type: "text", nullable: true),
                    SmsSender = table.Column<string>(type: "text", nullable: true),
                    YearStartMonth = table.Column<int>(type: "int", nullable: false),
                    CustomerUserName = table.Column<string>(type: "text", nullable: true),
                    CustomerUserPasword = table.Column<string>(type: "text", nullable: true),
                    AttendenceTypeId = table.Column<int>(type: "int", nullable: true),
                    PositionTypeId = table.Column<int>(type: "int", nullable: true),
                    IsAllowedEditAfterOvertimeAssigned = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    IsAllowedEditAfterIntimeOuttimeApproved = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    IsAllowedEditAfterIntimeOuttimeCanceled = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    IsAllowedEditIfIntimeOuttimeRequestPending = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    LateCountAfterMinutesForStu = table.Column<int>(type: "int", nullable: true),
                    EarlyOutCountAfterMinutesForStu = table.Column<int>(type: "int", nullable: true),
                    SendSmstoStudentAfterBillRecived = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    SendSmstoStudentAfterStudentAdd = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_School", x => x.SchoolId);
                });

            migrationBuilder.CreateTable(
                name: "SchoolCalender",
                columns: table => new
                {
                    CalenderId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    CalDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    EventId = table.Column<int>(type: "int", nullable: true),
                    EventName = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SchoolCalender", x => x.CalenderId);
                });

            migrationBuilder.CreateTable(
                name: "SchoolTask",
                columns: table => new
                {
                    TaskId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    RefId = table.Column<int>(type: "int", nullable: true),
                    RefTypeId = table.Column<int>(type: "int", nullable: true),
                    StartDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EndDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    GenerateTypeId = table.Column<int>(type: "int", nullable: true),
                    IsActive = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    TaskTitle = table.Column<string>(type: "varchar(256)", maxLength: 256, nullable: true),
                    TaskDescription = table.Column<string>(type: "text", nullable: true),
                    TaskStatusId = table.Column<int>(type: "int", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SchoolTask", x => x.TaskId);
                });

            migrationBuilder.CreateTable(
                name: "SchoolType",
                columns: table => new
                {
                    SocialTypeId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    SocialType = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SchoolType", x => x.SocialTypeId);
                });

            migrationBuilder.CreateTable(
                name: "ScoolAlert",
                columns: table => new
                {
                    AlertId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    AlertName = table.Column<string>(type: "varchar(200)", maxLength: 200, nullable: true),
                    AlertDescription = table.Column<string>(type: "varchar(500)", maxLength: 500, nullable: true),
                    AlertDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    ModuleId = table.Column<int>(type: "int", nullable: true),
                    AlertSql = table.Column<string>(type: "text", nullable: true),
                    AlertMessage = table.Column<string>(type: "varchar(1024)", maxLength: 1024, nullable: true),
                    AlertFrequency = table.Column<string>(type: "varchar(5)", maxLength: 5, nullable: true),
                    AlertStartTime = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    StartDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EndDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    Status = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    AlertHistoryId = table.Column<int>(type: "int", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ScoolAlert", x => x.AlertId);
                });

            migrationBuilder.CreateTable(
                name: "Section",
                columns: table => new
                {
                    SectionId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    SectionName = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: false),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Section", x => x.SectionId);
                });

            migrationBuilder.CreateTable(
                name: "Session",
                columns: table => new
                {
                    SessionId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    SessionName = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: false),
                    StartDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EndDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    Status = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    SessionType = table.Column<int>(type: "int", nullable: true),
                    IsVisible = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Session", x => x.SessionId);
                });

            migrationBuilder.CreateTable(
                name: "SmsBuyingLogs",
                columns: table => new
                {
                    SmsBuyingLogId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    CompanyName = table.Column<string>(type: "text", nullable: true),
                    CustomerName = table.Column<string>(type: "text", nullable: true),
                    MobileNumber = table.Column<string>(type: "text", nullable: true),
                    PhoneNumber = table.Column<string>(type: "text", nullable: true),
                    EmailAddress = table.Column<string>(type: "text", nullable: true),
                    CountryId = table.Column<int>(type: "int", nullable: true),
                    DivisionOrStateId = table.Column<int>(type: "int", nullable: true),
                    DistrictId = table.Column<int>(type: "int", nullable: true),
                    ThanaOrUpazilaId = table.Column<int>(type: "int", nullable: true),
                    AddressOne = table.Column<string>(type: "text", nullable: true),
                    AddressTwo = table.Column<string>(type: "text", nullable: true),
                    CustomerUserName = table.Column<string>(type: "text", nullable: true),
                    CustomerUserPasword = table.Column<string>(type: "text", nullable: true),
                    balance = table.Column<decimal>(type: "decimal(18, 2)", nullable: false),
                    transactionId = table.Column<string>(type: "text", nullable: true),
                    TransactionStatus = table.Column<string>(type: "text", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SmsBuyingLogs", x => x.SmsBuyingLogId);
                });

            migrationBuilder.CreateTable(
                name: "SmsHistory",
                columns: table => new
                {
                    SmsId = table.Column<long>(type: "bigint", nullable: false),
                    SentTo = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false),
                    SmsSubject = table.Column<string>(type: "varchar(200)", maxLength: 200, nullable: true),
                    SmsContent = table.Column<string>(type: "varchar(200)", maxLength: 200, nullable: true),
                    IsSend = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    Status = table.Column<int>(type: "int", nullable: true),
                    StatusText = table.Column<string>(type: "varchar(200)", maxLength: 200, nullable: true),
                    ErrorCode = table.Column<int>(type: "int", nullable: true),
                    ErrorText = table.Column<string>(type: "varchar(200)", maxLength: 200, nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SmsHistory", x => new { x.SmsId, x.SentTo });
                });

            migrationBuilder.CreateTable(
                name: "SMSTemplate",
                columns: table => new
                {
                    TemplateId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(type: "text", nullable: true),
                    Template = table.Column<string>(type: "text", nullable: true),
                    Vairables = table.Column<string>(type: "text", nullable: true),
                    Type = table.Column<int>(type: "int", nullable: false),
                    SmsFor = table.Column<int>(type: "int", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SMSTemplate", x => x.TemplateId);
                });

            migrationBuilder.CreateTable(
                name: "Student",
                columns: table => new
                {
                    StudentHeaderId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    StudentId = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: false),
                    FirstName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: false),
                    MiddleName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    LastName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    NickName = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    PunchCardNo = table.Column<string>(type: "text", nullable: true),
                    PresHoldingNo = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    PresStreet = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    PresArea = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    PresThanaId = table.Column<int>(type: "int", nullable: true),
                    PresPostCode = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: true),
                    PresCityId = table.Column<int>(type: "int", nullable: true),
                    PresDivisionId = table.Column<int>(type: "int", nullable: true),
                    PresCountryId = table.Column<int>(type: "int", nullable: true),
                    DeviceNumber = table.Column<int>(type: "int", nullable: true),
                    RollNumber = table.Column<int>(type: "int", nullable: false),
                    Address = table.Column<string>(type: "text", nullable: true),
                    PermHoldingNo = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    PermStreet = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    PermArea = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    PermPostCode = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: true),
                    PermThanaId = table.Column<int>(type: "int", nullable: true),
                    PermCityId = table.Column<int>(type: "int", nullable: true),
                    PermDivisionId = table.Column<int>(type: "int", nullable: true),
                    PermCountryId = table.Column<int>(type: "int", nullable: true),
                    Dob = table.Column<DateTime>(type: "datetime", nullable: true),
                    BirthCity = table.Column<int>(type: "int", nullable: true),
                    BirthCountry = table.Column<int>(type: "int", nullable: true),
                    PlaceOfBirth = table.Column<string>(type: "varchar(60)", maxLength: 60, nullable: true),
                    AdmissionDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    ReadmissionDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    AdmissionSessionId = table.Column<int>(type: "int", nullable: true),
                    ReadmissionSessionId = table.Column<int>(type: "int", nullable: true),
                    AdmissionClassId = table.Column<int>(type: "int", nullable: true),
                    AdmissionSectionId = table.Column<int>(type: "int", nullable: true),
                    AdmissionShift = table.Column<int>(type: "int", nullable: true),
                    SessionId = table.Column<int>(type: "int", nullable: true),
                    Age = table.Column<string>(type: "text", nullable: true),
                    ClassId = table.Column<int>(type: "int", nullable: true),
                    SectionId = table.Column<int>(type: "int", nullable: true),
                    Shift = table.Column<int>(type: "int", nullable: true),
                    CampusId = table.Column<int>(type: "int", nullable: true),
                    EndDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    BirthDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    Status = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    StatusId = table.Column<int>(type: "int", nullable: true),
                    Gender = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: true),
                    GenderId = table.Column<int>(type: "int", nullable: true),
                    Religion = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    ReligionId = table.Column<int>(type: "int", nullable: true),
                    ResPhone = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    Mobile = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    Email = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),
                    BloodGroup = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: true),
                    BloodId = table.Column<int>(type: "int", nullable: true),
                    Nationality = table.Column<string>(type: "varchar(60)", maxLength: 60, nullable: true),
                    NationalityId = table.Column<int>(type: "int", nullable: true),
                    BirthCertificateNo = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    PassportNo = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    MedicalHistory = table.Column<string>(type: "varchar(200)", maxLength: 200, nullable: true),
                    EmergencyMedicalAction = table.Column<string>(type: "varchar(200)", maxLength: 200, nullable: true),
                    FatherFirstName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    FatherMiddleName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    FatherLastName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    FatherEducation = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    FatherEducationDetails = table.Column<string>(type: "varchar(200)", maxLength: 200, nullable: true),
                    FatherOccupation = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    FatherOccupationDetails = table.Column<string>(type: "varchar(150)", maxLength: 150, nullable: true),
                    FatherOfficeAddress = table.Column<string>(type: "varchar(200)", maxLength: 200, nullable: true),
                    FatherOfficePhone = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    FatherMobile = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    FatherNationalId = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    FatherPassportNo = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    MotherFirstName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    MotherMiddleName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    MotherLastName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    MotherEducation = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    MotherEducationDetails = table.Column<string>(type: "varchar(200)", maxLength: 200, nullable: true),
                    MotherOccupation = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    MotherOccupationDetails = table.Column<string>(type: "varchar(150)", maxLength: 150, nullable: true),
                    MotherOfficeAddress = table.Column<string>(type: "varchar(200)", maxLength: 200, nullable: true),
                    LocGuardianOfficeAddress = table.Column<string>(type: "text", nullable: true),
                    MotherOfficePhone = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    MotherMobile = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    MotherNationalId = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    MoherPassportNo = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    LocGuardianFirstName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    LocGuardianMiddleName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    LocGuardianLastName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    LocGuardianRelationship = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    LocGuardianMobile = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    LocalGurdianNationalId = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    LocalGurdianPassportNo = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    StudentPhotoPath = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    ProximityNum = table.Column<string>(type: "varchar(15)", maxLength: 15, nullable: true),
                    ExitReasons = table.Column<string>(type: "varchar(15)", maxLength: 15, nullable: true),
                    StudentCategoryId = table.Column<int>(type: "int", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute7 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute8 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute9 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute10 = table.Column<string>(type: "text", nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Student", x => x.StudentHeaderId);
                });

            migrationBuilder.CreateTable(
                name: "StudentAdmission",
                columns: table => new
                {
                    StudentHeaderId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    StudentId = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    FirstName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    MiddleName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    LastName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    NickName = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    PresHoldingNo = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    PresStreet = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    PresArea = table.Column<int>(type: "int", nullable: false),
                    PresThanaId = table.Column<int>(type: "int", nullable: true),
                    PresPostCode = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: true),
                    PresCityId = table.Column<int>(type: "int", nullable: true),
                    PresDivisionId = table.Column<int>(type: "int", nullable: true),
                    PresCountryId = table.Column<int>(type: "int", nullable: true),
                    PeamHoldingNo = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    PermStreet = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    PermArea = table.Column<int>(type: "int", nullable: true),
                    PermPostCode = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: true),
                    PermThanaId = table.Column<int>(type: "int", nullable: true),
                    PermCityId = table.Column<int>(type: "int", nullable: true),
                    PermDivisionId = table.Column<int>(type: "int", nullable: true),
                    PermCountryId = table.Column<int>(type: "int", nullable: true),
                    Dob = table.Column<DateTime>(type: "datetime", nullable: false),
                    BirthCountry = table.Column<int>(type: "int", nullable: true),
                    PlaceOfBirth = table.Column<int>(type: "int", nullable: true),
                    Gender = table.Column<int>(type: "int", nullable: true),
                    Religion = table.Column<int>(type: "int", nullable: true),
                    ResPhone = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    Mobile = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    Email = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),
                    BloodGroup = table.Column<int>(type: "int", nullable: true),
                    Nationality = table.Column<int>(type: "int", nullable: true),
                    BirthCertificateNo = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    PassportNo = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    MedicalHistory = table.Column<string>(type: "varchar(200)", maxLength: 200, nullable: true),
                    EmergencyMedicalAction = table.Column<string>(type: "varchar(200)", maxLength: 200, nullable: true),
                    FatherFirstName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    FatherMiddleName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    FatherLastName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    FatherEducation = table.Column<int>(type: "int", nullable: true),
                    FatherOccupation = table.Column<int>(type: "int", nullable: true),
                    FatherMobile = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    FatherNationalId = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    FatherPassportNo = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    MotherFirstName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    MotherMiddleName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    MotherLastName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    MotherEducation = table.Column<int>(type: "int", nullable: true),
                    MotherOccupation = table.Column<int>(type: "int", nullable: true),
                    MotherMobile = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    MotherNationalId = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    MoherPassportNo = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    LocGuardianFirstName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    LocGuardianMiddleName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    LocGuardianLastName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    LocGuardianRelationship = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    LocGuardianMobile = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    LocalGurdianNationalId = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    LocalGurdianPassportNo = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    StudentPhotoPath = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentAdmission", x => x.StudentHeaderId);
                });

            migrationBuilder.CreateTable(
                name: "StudentAdmissionMark",
                columns: table => new
                {
                    AdmissionId = table.Column<int>(type: "int", nullable: false),
                    SubjectId = table.Column<int>(type: "int", nullable: false),
                    MarksObtain = table.Column<int>(type: "int", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentAdmissionMark", x => new { x.AdmissionId, x.SubjectId });
                });

            migrationBuilder.CreateTable(
                name: "StudentAdmissionTest",
                columns: table => new
                {
                    StudentAdmissionId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    StudentFirstName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: false),
                    StudentMiddleName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    StudentLastName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    StudentNickName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    AdmissionClassId = table.Column<int>(type: "int", nullable: true),
                    SessionId = table.Column<int>(type: "int", nullable: true),
                    FatherFirstName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    FatherMiddleName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    FatherLastName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    FatherMobile = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    FatherNationalId = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    MotherFirstName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    MotherMiddleName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    MotherLastName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    MotherMobile = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    MotherNationalId = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    PresHoldingNo = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    PresStreet = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    PresArea = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    PermPostCode = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: true),
                    PresThanaId = table.Column<int>(type: "int", nullable: true),
                    PresCityId = table.Column<int>(type: "int", nullable: true),
                    PresDivisionId = table.Column<int>(type: "int", nullable: true),
                    PresCountryId = table.Column<int>(type: "int", nullable: true),
                    Dob = table.Column<DateTime>(type: "datetime", nullable: true),
                    Gender = table.Column<string>(type: "varchar(1)", maxLength: 1, nullable: true),
                    Mobile = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    ResPhone = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    Email = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    Religion = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: true),
                    BloodGroup = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: true),
                    Nationality = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    BirthCertificateNo = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    BirthCity = table.Column<int>(type: "int", nullable: true),
                    BirthCountry = table.Column<int>(type: "int", nullable: true),
                    CampusId = table.Column<int>(type: "int", nullable: true),
                    CampusFloor = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    RoomNo = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: true),
                    ExamDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    StartTime = table.Column<string>(type: "varchar(20)", maxLength: 20, nullable: true),
                    EndTime = table.Column<string>(type: "varchar(20)", maxLength: 20, nullable: true),
                    ResultDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    TotalMarks = table.Column<double>(type: "double", nullable: true),
                    MarksObtained = table.Column<double>(type: "double", nullable: true),
                    Status = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    ImageUrl = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentAdmissionTest", x => x.StudentAdmissionId);
                });

            migrationBuilder.CreateTable(
                name: "StudentAdmitCard",
                columns: table => new
                {
                    TableId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    StudentId = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    StudentName = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    ClassName = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    SectionName = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    RoomNo = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    FloorNo = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    Semester = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    SessionId = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentAdmitCard", x => x.TableId);
                });

            migrationBuilder.CreateTable(
                name: "StudentBillMaster",
                columns: table => new
                {
                    BillHeaderId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    BillNo = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: true),
                    BillDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    SessionId = table.Column<int>(type: "int", nullable: true),
                    StudentHeaderId = table.Column<int>(type: "int", nullable: true),
                    ClassId = table.Column<int>(type: "int", nullable: true),
                    SectionId = table.Column<int>(type: "int", nullable: true),
                    Shift = table.Column<int>(type: "int", nullable: true),
                    SchoolId = table.Column<int>(type: "int", nullable: true),
                    BranchId = table.Column<int>(type: "int", nullable: true),
                    Discount = table.Column<decimal>(type: "decimal(18, 2)", nullable: true),
                    PeriodId = table.Column<int>(type: "int", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<int>(type: "int", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<int>(type: "int", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentBillMaster", x => x.BillHeaderId);
                });

            migrationBuilder.CreateTable(
                name: "StudentBillMasterTemporaries",
                columns: table => new
                {
                    BillHeaderId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    BillNo = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: true),
                    BillDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    SessionId = table.Column<int>(type: "int", nullable: true),
                    StudentHeaderId = table.Column<int>(type: "int", nullable: true),
                    ClassId = table.Column<int>(type: "int", nullable: true),
                    SectionId = table.Column<int>(type: "int", nullable: true),
                    Shift = table.Column<int>(type: "int", nullable: true),
                    SchoolId = table.Column<int>(type: "int", nullable: true),
                    BranchId = table.Column<int>(type: "int", nullable: true),
                    Discount = table.Column<decimal>(type: "decimal(18, 2)", nullable: true),
                    PeriodId = table.Column<int>(type: "int", nullable: true),
                    TransactionId = table.Column<string>(type: "text", nullable: true),
                    PaymentGateway = table.Column<int>(type: "int", nullable: false),
                    IsSynchronized = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<int>(type: "int", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<int>(type: "int", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentBillMasterTemporaries", x => x.BillHeaderId);
                });

            migrationBuilder.CreateTable(
                name: "StudentCategories",
                columns: table => new
                {
                    StudentHeaderId = table.Column<int>(type: "int", nullable: false),
                    StudentCategoryId = table.Column<int>(type: "int", nullable: false),
                    FromSessionId = table.Column<int>(type: "int", nullable: false),
                    ToSessionId = table.Column<int>(type: "int", nullable: false),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentCategories", x => new { x.StudentHeaderId, x.StudentCategoryId });
                });

            migrationBuilder.CreateTable(
                name: "StudentCategoryLogs",
                columns: table => new
                {
                    CategoryLogHeaderId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    FromSessionId = table.Column<int>(type: "int", nullable: false),
                    ToSessionId = table.Column<int>(type: "int", nullable: false),
                    StudentHeaderId = table.Column<int>(type: "int", nullable: false),
                    StudentCategoryId = table.Column<int>(type: "int", nullable: false),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentCategoryLogs", x => x.CategoryLogHeaderId);
                });

            migrationBuilder.CreateTable(
                name: "StudentCurriculumResult",
                columns: table => new
                {
                    CurriId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    SessionId = table.Column<int>(type: "int", nullable: false),
                    ExamId = table.Column<int>(type: "int", nullable: false),
                    StudentHeaderId = table.Column<int>(type: "int", nullable: false),
                    CurriculumId = table.Column<int>(type: "int", nullable: false),
                    GradeId = table.Column<int>(type: "int", nullable: false),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentCurriculumResult", x => x.CurriId);
                });

            migrationBuilder.CreateTable(
                name: "StudentHistory",
                columns: table => new
                {
                    StudentHeaderId = table.Column<int>(type: "int", nullable: false),
                    BkupSessionId = table.Column<int>(type: "int", nullable: false),
                    StudentId = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: false),
                    FirstName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: false),
                    MiddleName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    LastName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    NickName = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    PresHoldingNo = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    PresStreet = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    PresArea = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    PresThanaId = table.Column<int>(type: "int", nullable: true),
                    PresPostCode = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: true),
                    PresCityId = table.Column<int>(type: "int", nullable: true),
                    PresDivisionId = table.Column<int>(type: "int", nullable: true),
                    PresCountryId = table.Column<int>(type: "int", nullable: true),
                    PermHoldingNo = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    PermStreet = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    PermArea = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    PermPostCode = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: true),
                    PermThanaId = table.Column<int>(type: "int", nullable: true),
                    PermCityId = table.Column<int>(type: "int", nullable: true),
                    PermDivisionId = table.Column<int>(type: "int", nullable: true),
                    PermCountryId = table.Column<int>(type: "int", nullable: true),
                    Dob = table.Column<DateTime>(type: "datetime", nullable: true),
                    BirthCity = table.Column<int>(type: "int", nullable: true),
                    BirthCountry = table.Column<int>(type: "int", nullable: true),
                    PlaceOfBirth = table.Column<string>(type: "varchar(60)", maxLength: 60, nullable: true),
                    AdmissionDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    AdmissionSessionId = table.Column<int>(type: "int", nullable: true),
                    AdmissionClassId = table.Column<int>(type: "int", nullable: true),
                    AdmissionSectionId = table.Column<int>(type: "int", nullable: true),
                    AdmissionShift = table.Column<int>(type: "int", nullable: true),
                    SessionId = table.Column<int>(type: "int", nullable: true),
                    ClassId = table.Column<int>(type: "int", nullable: true),
                    SectionId = table.Column<int>(type: "int", nullable: true),
                    Shift = table.Column<int>(type: "int", nullable: true),
                    CampusId = table.Column<int>(type: "int", nullable: true),
                    EndDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    Status = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    Gender = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: true),
                    Religion = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    ResPhone = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    Mobile = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    Email = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),
                    BloodGroup = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: true),
                    Nationality = table.Column<string>(type: "varchar(60)", maxLength: 60, nullable: true),
                    BirthCertificateNo = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    PassportNo = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    MedicalHistory = table.Column<string>(type: "varchar(200)", maxLength: 200, nullable: true),
                    EmergencyMedicalAction = table.Column<string>(type: "varchar(200)", maxLength: 200, nullable: true),
                    FatherFirstName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    FatherMiddleName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    FatherLastName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    FatherEducation = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    FatherEducationDetails = table.Column<string>(type: "varchar(200)", maxLength: 200, nullable: true),
                    FatherOccupation = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    FatherOccupationDetails = table.Column<string>(type: "varchar(150)", maxLength: 150, nullable: true),
                    FatherOfficeAddress = table.Column<string>(type: "varchar(200)", maxLength: 200, nullable: true),
                    FatherOfficePhone = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    FatherMobile = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    FatherNationalId = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    FatherPassportNo = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    MotherFirstName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    MotherMiddleName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    MotherLastName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    MotherEducation = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    MotherEducationDetails = table.Column<string>(type: "varchar(200)", maxLength: 200, nullable: true),
                    MotherOccupation = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    MotherOccupationDetails = table.Column<string>(type: "varchar(150)", maxLength: 150, nullable: true),
                    MotherOfficeAddress = table.Column<string>(type: "varchar(200)", maxLength: 200, nullable: true),
                    MotherOfficePhone = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    MotherMobile = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    MotherNationalId = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    MoherPassportNo = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    LocGuardianFirstName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    LocGuardianMiddleName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    LocGuardianLastName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    LocGuardianRelationship = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    LocGuardianMobile = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    LocalGurdianNationalId = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    LocalGurdianPassportNo = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    StudentPhotoPath = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    ProximityNum = table.Column<string>(type: "varchar(15)", maxLength: 15, nullable: true),
                    ExitReasons = table.Column<string>(type: "varchar(15)", maxLength: 15, nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute7 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute8 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute9 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute10 = table.Column<string>(type: "text", nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentHistory", x => new { x.StudentHeaderId, x.BkupSessionId });
                });

            migrationBuilder.CreateTable(
                name: "StudentLeaveApplications",
                columns: table => new
                {
                    ApplicationId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    StudentHeaderId = table.Column<int>(type: "int", nullable: false),
                    LeaveCategoryId = table.Column<int>(type: "int", nullable: false),
                    LeaveStartDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    LeaveEndDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    LeaveApplyDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    Subject = table.Column<string>(type: "text", nullable: true),
                    Comment = table.Column<string>(type: "text", nullable: true),
                    LeaveStatus = table.Column<int>(type: "int", nullable: false),
                    IsOnDuty = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    Remarks = table.Column<string>(type: "text", nullable: true),
                    Attachment1 = table.Column<string>(type: "text", nullable: true),
                    Attachment2 = table.Column<string>(type: "text", nullable: true),
                    Attachment3 = table.Column<string>(type: "text", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentLeaveApplications", x => x.ApplicationId);
                });

            migrationBuilder.CreateTable(
                name: "StudentLeaveCounts",
                columns: table => new
                {
                    LeaveCountId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    ApplicationId = table.Column<int>(type: "int", nullable: false),
                    LeaveQuantity = table.Column<string>(type: "text", nullable: true),
                    StuTotalLeave = table.Column<int>(type: "int", nullable: false),
                    StuLeaveRemain = table.Column<int>(type: "int", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentLeaveCounts", x => x.LeaveCountId);
                });

            migrationBuilder.CreateTable(
                name: "StudentMedical",
                columns: table => new
                {
                    StudentMedId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    ReferenceId = table.Column<int>(type: "int", nullable: true),
                    MedicalId = table.Column<int>(type: "int", nullable: true),
                    EmergencyMedAction = table.Column<string>(type: "varchar(200)", maxLength: 200, nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentMedical", x => x.StudentMedId);
                });

            migrationBuilder.CreateTable(
                name: "StudentOtherInfo",
                columns: table => new
                {
                    InfoId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    SessionId = table.Column<int>(type: "int", nullable: false),
                    EventId = table.Column<int>(type: "int", nullable: false),
                    StudentHeaderId = table.Column<int>(type: "int", nullable: false),
                    TotalWorkingDay = table.Column<int>(type: "int", nullable: false),
                    TotalAbsent = table.Column<int>(type: "int", nullable: false),
                    TotalLate = table.Column<int>(type: "int", nullable: false),
                    Height = table.Column<decimal>(type: "decimal(18, 2)", nullable: false),
                    Weight = table.Column<decimal>(type: "decimal(18, 2)", nullable: false),
                    Attribute1 = table.Column<string>(type: "text", nullable: true),
                    Attribute2 = table.Column<string>(type: "text", nullable: true),
                    Attribute3 = table.Column<string>(type: "text", nullable: true),
                    CreatedBy = table.Column<int>(type: "int", nullable: false),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<int>(type: "int", nullable: true),
                    LasteUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentOtherInfo", x => x.InfoId);
                });

            migrationBuilder.CreateTable(
                name: "StudentPositionLog",
                columns: table => new
                {
                    PositionLogId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    ClassId = table.Column<int>(type: "int", nullable: false),
                    SessionId = table.Column<int>(type: "int", nullable: false),
                    EventId = table.Column<int>(type: "int", nullable: false),
                    LogTime = table.Column<DateTime>(type: "datetime", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentPositionLog", x => x.PositionLogId);
                });

            migrationBuilder.CreateTable(
                name: "StudentProx",
                columns: table => new
                {
                    StudentHeaderId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    StudentId = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: false),
                    FirstName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: false),
                    MiddleName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    LastName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    NickName = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    PresHoldingNo = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    PresStreet = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    PresArea = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    PresThanaId = table.Column<int>(type: "int", nullable: true),
                    PresPostCode = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: true),
                    PresCityId = table.Column<int>(type: "int", nullable: true),
                    PresDivisionId = table.Column<int>(type: "int", nullable: true),
                    PresCountryId = table.Column<int>(type: "int", nullable: true),
                    PermHoldingNo = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    PermStreet = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    PermArea = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    PermPostCode = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: true),
                    PermThanaId = table.Column<int>(type: "int", nullable: true),
                    PermCityId = table.Column<int>(type: "int", nullable: true),
                    PermDivisionId = table.Column<int>(type: "int", nullable: true),
                    PermCountryId = table.Column<int>(type: "int", nullable: true),
                    Dob = table.Column<DateTime>(type: "datetime", nullable: true),
                    BirthCity = table.Column<int>(type: "int", nullable: true),
                    BirthCountry = table.Column<int>(type: "int", nullable: true),
                    PlaceOfBirth = table.Column<string>(type: "varchar(60)", maxLength: 60, nullable: true),
                    AdmissionDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    AdmissionSessionId = table.Column<int>(type: "int", nullable: true),
                    AdmissionClassId = table.Column<int>(type: "int", nullable: true),
                    AdmissionSectionId = table.Column<int>(type: "int", nullable: true),
                    AdmissionShift = table.Column<int>(type: "int", nullable: true),
                    SessionId = table.Column<int>(type: "int", nullable: true),
                    ClassId = table.Column<int>(type: "int", nullable: true),
                    SectionId = table.Column<int>(type: "int", nullable: true),
                    Shift = table.Column<int>(type: "int", nullable: true),
                    CampusId = table.Column<int>(type: "int", nullable: true),
                    EndDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    Status = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    Gender = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: true),
                    Religion = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    ResPhone = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    Mobile = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    Email = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),
                    BloodGroup = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: true),
                    Nationality = table.Column<string>(type: "varchar(60)", maxLength: 60, nullable: true),
                    BirthCertificateNo = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    PassportNo = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    MedicalHistory = table.Column<string>(type: "varchar(200)", maxLength: 200, nullable: true),
                    EmergencyMedicalAction = table.Column<string>(type: "varchar(200)", maxLength: 200, nullable: true),
                    FatherFirstName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    FatherMiddleName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    FatherLastName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    FatherEducation = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    FatherEducationDetails = table.Column<string>(type: "varchar(200)", maxLength: 200, nullable: true),
                    FatherOccupation = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    FatherOccupationDetails = table.Column<string>(type: "varchar(150)", maxLength: 150, nullable: true),
                    FatherOfficeAddress = table.Column<string>(type: "varchar(200)", maxLength: 200, nullable: true),
                    FatherOfficePhone = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    FatherMobile = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    FatherNationalId = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    FatherPassportNo = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    MotherFirstName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    MotherMiddleName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    MotherLastName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    MotherEducation = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    MotherEducationDetails = table.Column<string>(type: "varchar(200)", maxLength: 200, nullable: true),
                    MotherOccupation = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    MotherOccupationDetails = table.Column<string>(type: "varchar(150)", maxLength: 150, nullable: true),
                    MotherOfficeAddress = table.Column<string>(type: "varchar(200)", maxLength: 200, nullable: true),
                    MotherOfficePhone = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    MotherMobile = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    MotherNationalId = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    MoherPassportNo = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    LocGuardianFirstName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    LocGuardianMiddleName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    LocGuardianLastName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    LocGuardianRelationship = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    LocGuardianMobile = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    LocalGurdianNationalId = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    LocalGurdianPassportNo = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    StudentPhotoPath = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentProx", x => x.StudentHeaderId);
                });

            migrationBuilder.CreateTable(
                name: "StudentRagistration",
                columns: table => new
                {
                    StudentAdmissionId = table.Column<string>(type: "varchar(767)", nullable: false),
                    StudentFirstName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    StudentMiddleName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    StudentLastName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    AdmissionClassId = table.Column<int>(type: "int", nullable: true),
                    FatherFirstName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    FatherMiddleName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    FatherLastName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    MotherFirstName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    MotherMiddleName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    MotherLastName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    PresHoldingNo = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    PresStreet = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    PresArea = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    PresThanaId = table.Column<int>(type: "int", nullable: true),
                    PresCityId = table.Column<int>(type: "int", nullable: true),
                    PresDivisionId = table.Column<int>(type: "int", nullable: true),
                    PresCountryId = table.Column<int>(type: "int", nullable: true),
                    Dob = table.Column<DateTime>(type: "datetime", nullable: true),
                    Gender = table.Column<string>(type: "text", nullable: true),
                    Mobile = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    Email = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentRagistration", x => x.StudentAdmissionId);
                });

            migrationBuilder.CreateTable(
                name: "StudentRemarks",
                columns: table => new
                {
                    RemarkId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    StudentHeaderId = table.Column<int>(type: "int", nullable: false),
                    Remarks = table.Column<string>(type: "text", nullable: true),
                    Attribute1 = table.Column<string>(type: "text", nullable: true),
                    Attribute2 = table.Column<string>(type: "text", nullable: true),
                    Attribute3 = table.Column<string>(type: "text", nullable: true),
                    CreatedBy = table.Column<int>(type: "int", nullable: false),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    LastUpdateBy = table.Column<int>(type: "int", nullable: false),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentRemarks", x => x.RemarkId);
                });

            migrationBuilder.CreateTable(
                name: "StudentSiblings",
                columns: table => new
                {
                    SiblingsId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    StudentHeaderId = table.Column<int>(type: "int", nullable: false),
                    GurdianId = table.Column<int>(type: "int", nullable: true),
                    SiblingsStudentId = table.Column<int>(type: "int", nullable: true),
                    SiblingsName = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),
                    SiblingsGender = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: true),
                    SiblingsDob = table.Column<DateTime>(type: "datetime", nullable: true),
                    SiblingsSchoolId = table.Column<int>(type: "int", nullable: true),
                    SiblingsClassId = table.Column<int>(type: "int", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentSiblings", x => x.SiblingsId);
                });

            migrationBuilder.CreateTable(
                name: "StudentSubject",
                columns: table => new
                {
                    StudentSubId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    SessionId = table.Column<int>(type: "int", nullable: false),
                    StudentHeaderId = table.Column<int>(type: "int", nullable: false),
                    ClassId = table.Column<int>(type: "int", nullable: false),
                    SubjectId = table.Column<int>(type: "int", nullable: false),
                    SubGroupId = table.Column<int>(type: "int", nullable: false),
                    SectionId = table.Column<int>(type: "int", nullable: false),
                    StartDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EndDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentSubject", x => x.StudentSubId);
                });

            migrationBuilder.CreateTable(
                name: "StuDocument",
                columns: table => new
                {
                    DocId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    ActivateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    UserType = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: true),
                    ReferenceId = table.Column<int>(type: "int", nullable: true),
                    CampusId = table.Column<int>(type: "int", nullable: true),
                    ClassId = table.Column<int>(type: "int", nullable: true),
                    SectionId = table.Column<int>(type: "int", nullable: true),
                    TypeId = table.Column<int>(type: "int", nullable: true),
                    ShiftId = table.Column<int>(type: "int", nullable: true),
                    SubjectId = table.Column<int>(type: "int", nullable: true),
                    SessionId = table.Column<int>(type: "int", nullable: true),
                    Path = table.Column<string>(type: "varchar(150)", maxLength: 150, nullable: true),
                    FileType = table.Column<string>(type: "varchar(20)", maxLength: 20, nullable: true),
                    FileSize = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StuDocument", x => x.DocId);
                });

            migrationBuilder.CreateTable(
                name: "Subject",
                columns: table => new
                {
                    SubjectId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    SubjectName = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    ShortName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    Major = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    Active = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Subject", x => x.SubjectId);
                });

            migrationBuilder.CreateTable(
                name: "SubjectSubgroup",
                columns: table => new
                {
                    SubgorupId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    SubgroupDesc = table.Column<string>(type: "varchar(150)", maxLength: 150, nullable: true),
                    ShortName = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    AltName = table.Column<string>(type: "varchar(150)", maxLength: 150, nullable: true),
                    SubjectId = table.Column<int>(type: "int", nullable: true),
                    BestCount = table.Column<int>(type: "int", nullable: true),
                    StartDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EndDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubjectSubgroup", x => x.SubgorupId);
                });

            migrationBuilder.CreateTable(
                name: "SubTeacher",
                columns: table => new
                {
                    TeacherId = table.Column<int>(type: "int", nullable: false),
                    SubjectId = table.Column<int>(type: "int", nullable: false),
                    Major = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    SubHead = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    Active = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    StartTime = table.Column<DateTime>(type: "datetime", nullable: true),
                    EndTime = table.Column<DateTime>(type: "datetime", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubTeacher", x => new { x.TeacherId, x.SubjectId });
                });

            migrationBuilder.CreateTable(
                name: "TeacherLeaveCount",
                columns: table => new
                {
                    LeaveCountId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    ApplicationId = table.Column<int>(type: "int", nullable: false),
                    LeaveQuantity = table.Column<string>(type: "text", nullable: true),
                    TeacherTotalLeave = table.Column<int>(type: "int", nullable: false),
                    TeacherLeaveRemain = table.Column<int>(type: "int", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TeacherLeaveCount", x => x.LeaveCountId);
                });

            migrationBuilder.CreateTable(
                name: "TempLeave",
                columns: table => new
                {
                    Tempid = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    Empheaderid = table.Column<int>(type: "int", nullable: false),
                    NoOfLeave = table.Column<int>(type: "int", nullable: false),
                    LeaveDays = table.Column<string>(type: "text", nullable: true),
                    PeriodId = table.Column<int>(type: "int", nullable: false),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TempLeave", x => x.Tempid);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    UserId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    UserName = table.Column<string>(type: "text", nullable: false),
                    ReferrenceId = table.Column<int>(type: "int", nullable: true),
                    FirstName = table.Column<string>(type: "text", nullable: true),
                    LastName = table.Column<string>(type: "text", nullable: true),
                    EmailAddress = table.Column<string>(type: "text", nullable: true),
                    EmailConfirmed = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    IsTemporaryPassword = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    Password = table.Column<string>(type: "text", nullable: false),
                    UserType = table.Column<int>(type: "int", nullable: false),
                    Status = table.Column<int>(type: "int", nullable: true),
                    About = table.Column<string>(type: "text", nullable: true),
                    ImageCaption = table.Column<string>(type: "text", nullable: true),
                    ImagePath = table.Column<string>(type: "text", nullable: true),
                    ImageSize = table.Column<long>(type: "bigint", nullable: true),
                    AddressOne = table.Column<string>(type: "text", nullable: true),
                    AddressTwo = table.Column<string>(type: "text", nullable: true),
                    Country = table.Column<string>(type: "text", nullable: true),
                    Region = table.Column<string>(type: "text", nullable: true),
                    City = table.Column<string>(type: "text", nullable: true),
                    PostCode = table.Column<string>(type: "text", nullable: true),
                    PhoneNumber = table.Column<string>(type: "text", nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    Position = table.Column<string>(type: "text", nullable: true),
                    IsActivated = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    GuidIdNumber = table.Column<string>(type: "text", nullable: true),
                    IsTrialUser = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    RegistrationConfirmed = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    LockoutDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LockoutEnabled = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    AccessFailedCount = table.Column<int>(type: "int", nullable: true),
                    SecurityStamp = table.Column<string>(type: "text", nullable: true),
                    TwoFactorEnabled = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.UserId);
                });

            migrationBuilder.CreateTable(
                name: "VoteMaster",
                columns: table => new
                {
                    VoteId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    VoteDescription = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),
                    ProjectId = table.Column<int>(type: "int", nullable: true),
                    VoteFor = table.Column<int>(type: "int", nullable: true),
                    PassPct = table.Column<int>(type: "int", nullable: true),
                    AvgPct = table.Column<int>(type: "int", nullable: true),
                    SelectedOptionId = table.Column<int>(type: "int", nullable: true),
                    StartDate = table.Column<DateTime>(type: "date", nullable: true),
                    EndDate = table.Column<DateTime>(type: "date", nullable: true),
                    Status = table.Column<int>(type: "int", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VoteMaster", x => x.VoteId);
                });

            migrationBuilder.CreateTable(
                name: "VoteMember",
                columns: table => new
                {
                    VotePersonId = table.Column<int>(type: "int", nullable: false),
                    ReferenceId = table.Column<int>(type: "int", nullable: false),
                    VoteId = table.Column<int>(type: "int", nullable: false),
                    ReferenceType = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    VoteOptionId = table.Column<int>(type: "int", nullable: true),
                    Status = table.Column<int>(type: "int", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VoteMember", x => new { x.VotePersonId, x.ReferenceId, x.VoteId });
                });

            migrationBuilder.CreateTable(
                name: "VoteOption",
                columns: table => new
                {
                    VoteOptionId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    VoteOptionDesc = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),
                    VoteId = table.Column<int>(type: "int", nullable: true),
                    Status = table.Column<int>(type: "int", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VoteOption", x => x.VoteOptionId);
                });

            migrationBuilder.CreateTable(
                name: "Years",
                columns: table => new
                {
                    YearId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    YearName = table.Column<int>(type: "int", nullable: false),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Years", x => x.YearId);
                });

            migrationBuilder.CreateTable(
                name: "Book",
                columns: table => new
                {
                    BookId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    BookName = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: false),
                    PurchaseDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    BillNo = table.Column<string>(type: "text", nullable: true),
                    ISBNNo = table.Column<string>(type: "text", nullable: true),
                    BookNo = table.Column<string>(type: "text", nullable: true),
                    AuthorId = table.Column<int>(type: "int", nullable: false),
                    Edition = table.Column<string>(type: "varchar(20)", maxLength: 20, nullable: true),
                    BookCategoryId = table.Column<int>(type: "int", nullable: false),
                    PublisherName = table.Column<string>(type: "text", nullable: true),
                    NoOfCopies = table.Column<int>(type: "int", nullable: true),
                    ShelfId = table.Column<int>(type: "int", nullable: false),
                    BookPosition = table.Column<int>(type: "int", nullable: true),
                    BookCost = table.Column<decimal>(type: "decimal(18, 2)", nullable: true),
                    Language = table.Column<int>(type: "int", nullable: false),
                    BookCondition = table.Column<int>(type: "int", nullable: false),
                    PublishYear = table.Column<DateTime>(type: "datetime", nullable: true),
                    IsActive = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    BookShelfsShelfId = table.Column<int>(type: "int", nullable: true),
                    BookAuthorAuthorId = table.Column<int>(type: "int", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Book", x => x.BookId);
                    table.ForeignKey(
                        name: "FK_Book_BookAuthors_BookAuthorAuthorId",
                        column: x => x.BookAuthorAuthorId,
                        principalTable: "BookAuthors",
                        principalColumn: "AuthorId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Book_BookCategories_BookCategoryId",
                        column: x => x.BookCategoryId,
                        principalTable: "BookCategories",
                        principalColumn: "BookCategoryId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Book_BookShelfs_BookShelfsShelfId",
                        column: x => x.BookShelfsShelfId,
                        principalTable: "BookShelfs",
                        principalColumn: "ShelfId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ClassExamGrade",
                columns: table => new
                {
                    ClassGradeId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    ClassId = table.Column<int>(type: "int", nullable: false),
                    GradeId = table.Column<int>(type: "int", nullable: false),
                    AltName = table.Column<string>(type: "text", nullable: true),
                    ExamGradeGradeId = table.Column<int>(type: "int", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClassExamGrade", x => x.ClassGradeId);
                    table.ForeignKey(
                        name: "FK_ClassExamGrade_Class_ClassId",
                        column: x => x.ClassId,
                        principalTable: "Class",
                        principalColumn: "ClassId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ClassExamGrade_ExamGrade_ExamGradeGradeId",
                        column: x => x.ExamGradeGradeId,
                        principalTable: "ExamGrade",
                        principalColumn: "GradeId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DateWiseInvestigators",
                columns: table => new
                {
                    DateWiseInvestigatorId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    Date = table.Column<DateTime>(type: "datetime", nullable: false),
                    InvestigatorId = table.Column<int>(type: "int", nullable: false),
                    ExamRoomInvestigatorId = table.Column<int>(type: "int", nullable: false),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DateWiseInvestigators", x => x.DateWiseInvestigatorId);
                    table.ForeignKey(
                        name: "FK_DateWiseInvestigators_ExamRoomInvestigators_ExamRoomInvestig~",
                        column: x => x.ExamRoomInvestigatorId,
                        principalTable: "ExamRoomInvestigators",
                        principalColumn: "ExamRoomInvestigatorId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ExamScheduleLines",
                columns: table => new
                {
                    ExamScheduleLineId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    ClassId = table.Column<int>(type: "int", nullable: false),
                    SectionId = table.Column<int>(type: "int", nullable: false),
                    ExamStartdate = table.Column<DateTime>(type: "datetime", nullable: false),
                    ExamEnddate = table.Column<DateTime>(type: "datetime", nullable: false),
                    ExamScheduleId = table.Column<int>(type: "int", nullable: false),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExamScheduleLines", x => x.ExamScheduleLineId);
                    table.ForeignKey(
                        name: "FK_ExamScheduleLines_ExamSchedules_ExamScheduleId",
                        column: x => x.ExamScheduleId,
                        principalTable: "ExamSchedules",
                        principalColumn: "ExamScheduleId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "FeeSubCategory",
                columns: table => new
                {
                    FeeSubCategoryId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    FeeSubCategoryName = table.Column<string>(type: "text", nullable: true),
                    FeeCategoryId = table.Column<int>(type: "int", nullable: false),
                    SessionId = table.Column<int>(type: "int", nullable: true),
                    Amount = table.Column<decimal>(type: "decimal(18, 2)", nullable: false),
                    FeeType = table.Column<int>(type: "int", nullable: false),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FeeSubCategory", x => x.FeeSubCategoryId);
                    table.ForeignKey(
                        name: "FK_FeeSubCategory_FeeCategory_FeeCategoryId",
                        column: x => x.FeeCategoryId,
                        principalTable: "FeeCategory",
                        principalColumn: "FeeCategoryId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "FndFlexValue",
                columns: table => new
                {
                    FlexValueId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    FlexValue = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false),
                    FlexShortValue = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    FlexValueSetId = table.Column<int>(type: "int", nullable: false),
                    ParentFlexValueId = table.Column<int>(type: "int", nullable: true),
                    ParentFlexValueSetId = table.Column<int>(type: "int", nullable: true),
                    FndFlexValueSetsFlexValueSetId = table.Column<int>(type: "int", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FndFlexValue", x => x.FlexValueId);
                    table.ForeignKey(
                        name: "FK_FndFlexValue_FndFlexValueSet_FndFlexValueSetsFlexValueSetId",
                        column: x => x.FndFlexValueSetsFlexValueSetId,
                        principalTable: "FndFlexValueSet",
                        principalColumn: "FlexValueSetId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PhotoGallery",
                columns: table => new
                {
                    PhotoGalleryId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    GalleryId = table.Column<int>(type: "int", nullable: true),
                    ImageTitle = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    ThumbImageUrl = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),
                    DisplayImageUrl = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),
                    OrginalImageUrl = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),
                    DisplayStatus = table.Column<int>(type: "int", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PhotoGallery", x => x.PhotoGalleryId);
                    table.ForeignKey(
                        name: "FK_PhotoGallery_Gallery_GalleryId",
                        column: x => x.GalleryId,
                        principalTable: "Gallery",
                        principalColumn: "GalleryId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ClassSectionGroupTeachers",
                columns: table => new
                {
                    ClassSectionGroupId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    ClassId = table.Column<int>(type: "int", nullable: false),
                    SectionId = table.Column<int>(type: "int", nullable: false),
                    SessionId = table.Column<int>(type: "int", nullable: true),
                    SubjectId = table.Column<int>(type: "int", nullable: false),
                    TeacherId = table.Column<int>(type: "int", nullable: true),
                    HrEmployeeEmpHeaderId = table.Column<int>(type: "int", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClassSectionGroupTeachers", x => x.ClassSectionGroupId);
                    table.ForeignKey(
                        name: "FK_ClassSectionGroupTeachers_HrEmployee_HrEmployeeEmpHeaderId",
                        column: x => x.HrEmployeeEmpHeaderId,
                        principalTable: "HrEmployee",
                        principalColumn: "EmpHeaderId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "EmployeeOfficeTimes",
                columns: table => new
                {
                    EmployeeOfficeTimeHeaderId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    EmpHeaderId = table.Column<int>(type: "int", nullable: false),
                    StartTime = table.Column<string>(type: "text", nullable: true),
                    EndTime = table.Column<string>(type: "text", nullable: true),
                    HrEmployeeEmpHeaderId = table.Column<int>(type: "int", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmployeeOfficeTimes", x => x.EmployeeOfficeTimeHeaderId);
                    table.ForeignKey(
                        name: "FK_EmployeeOfficeTimes_HrEmployee_HrEmployeeEmpHeaderId",
                        column: x => x.HrEmployeeEmpHeaderId,
                        principalTable: "HrEmployee",
                        principalColumn: "EmpHeaderId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "EmployeeStatusLogs",
                columns: table => new
                {
                    EmployeeStatusLogId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    EmpHeaderId = table.Column<int>(type: "int", nullable: false),
                    Status = table.Column<int>(type: "int", nullable: false),
                    EmployeeEmpHeaderId = table.Column<int>(type: "int", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmployeeStatusLogs", x => x.EmployeeStatusLogId);
                    table.ForeignKey(
                        name: "FK_EmployeeStatusLogs_HrEmployee_EmployeeEmpHeaderId",
                        column: x => x.EmployeeEmpHeaderId,
                        principalTable: "HrEmployee",
                        principalColumn: "EmpHeaderId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "EmployeeWeekends",
                columns: table => new
                {
                    EmployeeWeekendsId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    Day = table.Column<int>(type: "int", nullable: false),
                    EmpHeaderId = table.Column<int>(type: "int", nullable: false),
                    EmployeeEmpHeaderId = table.Column<int>(type: "int", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmployeeWeekends", x => x.EmployeeWeekendsId);
                    table.ForeignKey(
                        name: "FK_EmployeeWeekends_HrEmployee_EmployeeEmpHeaderId",
                        column: x => x.EmployeeEmpHeaderId,
                        principalTable: "HrEmployee",
                        principalColumn: "EmpHeaderId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "HrEmpEduQuali",
                columns: table => new
                {
                    EmpHeaderId = table.Column<int>(type: "int", nullable: false),
                    QualificationId = table.Column<int>(type: "int", nullable: false),
                    QualificationType = table.Column<int>(type: "int", nullable: true),
                    Major = table.Column<int>(type: "int", nullable: true),
                    InstitutionId = table.Column<int>(type: "int", nullable: true),
                    BoardId = table.Column<int>(type: "int", nullable: true),
                    PassingYear = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    Result = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    StartDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EndDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    HrEmployeeEmpHeaderId = table.Column<int>(type: "int", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HrEmpEduQuali", x => new { x.EmpHeaderId, x.QualificationId });
                    table.ForeignKey(
                        name: "FK_HrEmpEduQuali_HrEmployee_HrEmployeeEmpHeaderId",
                        column: x => x.HrEmployeeEmpHeaderId,
                        principalTable: "HrEmployee",
                        principalColumn: "EmpHeaderId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "HrEmpExperience",
                columns: table => new
                {
                    EmpHeaderId = table.Column<int>(type: "int", nullable: false),
                    ExperienceId = table.Column<int>(type: "int", nullable: false),
                    CompanyName = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    Designation = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    Location = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    Responsibility = table.Column<string>(type: "varchar(200)", maxLength: 200, nullable: true),
                    StartDate = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    EndDate = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    HrEmployeeEmpHeaderId = table.Column<int>(type: "int", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HrEmpExperience", x => new { x.EmpHeaderId, x.ExperienceId });
                    table.ForeignKey(
                        name: "FK_HrEmpExperience_HrEmployee_HrEmployeeEmpHeaderId",
                        column: x => x.HrEmployeeEmpHeaderId,
                        principalTable: "HrEmployee",
                        principalColumn: "EmpHeaderId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "HrEmployeeRefrence",
                columns: table => new
                {
                    EmpHeaderId = table.Column<int>(type: "int", nullable: false),
                    ReferenceId = table.Column<int>(type: "int", nullable: false),
                    ReferenceName = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    Designation = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),
                    Address1 = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),
                    Address2 = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),
                    Address3 = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),
                    ResPhone = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    OffPhone = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    Mobile1 = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    Mobile2 = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    Email = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    Relation = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    HrEmployeeEmpHeaderId = table.Column<int>(type: "int", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HrEmployeeRefrence", x => new { x.EmpHeaderId, x.ReferenceId });
                    table.ForeignKey(
                        name: "FK_HrEmployeeRefrence_HrEmployee_HrEmployeeEmpHeaderId",
                        column: x => x.HrEmployeeEmpHeaderId,
                        principalTable: "HrEmployee",
                        principalColumn: "EmpHeaderId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "HrEmployeeTransaction",
                columns: table => new
                {
                    TransactionId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    EmpHeaderId = table.Column<int>(type: "int", nullable: false),
                    EmpId = table.Column<string>(type: "varchar(15)", maxLength: 15, nullable: false),
                    EmpFirstName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: false),
                    EmpMiddleName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    EmpLastName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    EmpNickName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    BkupDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    BkupUserId = table.Column<int>(type: "int", nullable: false),
                    BusinessGroupId = table.Column<int>(type: "int", nullable: true),
                    ApplicantId = table.Column<int>(type: "int", nullable: true),
                    DeptId = table.Column<int>(type: "int", nullable: true),
                    DesigId = table.Column<int>(type: "int", nullable: true),
                    PunchCardId = table.Column<string>(type: "varchar(20)", maxLength: 20, nullable: true),
                    StartTime = table.Column<string>(type: "varchar(20)", maxLength: 20, nullable: false),
                    EndTime = table.Column<string>(type: "varchar(20)", maxLength: 20, nullable: false),
                    JoiningDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    JobEndDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    IsPermenent = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    Status = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    BirthDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    DeathDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    JobLocationId = table.Column<int>(type: "int", nullable: true),
                    BaseOfficeId = table.Column<int>(type: "int", nullable: true),
                    UnitId = table.Column<int>(type: "int", nullable: true),
                    InternalLocationId = table.Column<int>(type: "int", nullable: true),
                    PresHoldingNo = table.Column<string>(type: "varchar(20)", maxLength: 20, nullable: true),
                    PresStreet = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    PresArea = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    PresThanaId = table.Column<int>(type: "int", nullable: true),
                    PresPostCode = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: true),
                    PresCityId = table.Column<int>(type: "int", nullable: true),
                    PresDivisionId = table.Column<int>(type: "int", nullable: true),
                    PresCountryId = table.Column<int>(type: "int", nullable: true),
                    PermHoldingNo = table.Column<string>(type: "varchar(20)", maxLength: 20, nullable: true),
                    PermStreet = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    PermArea = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    PermThanaId = table.Column<int>(type: "int", nullable: true),
                    PermPostCode = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: true),
                    PermCityId = table.Column<int>(type: "int", nullable: true),
                    PermDivisionId = table.Column<int>(type: "int", nullable: true),
                    PermCountryId = table.Column<int>(type: "int", nullable: true),
                    Gender = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    Religion = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    PlaceOfBirth = table.Column<string>(type: "varchar(60)", maxLength: 60, nullable: true),
                    BirthCountry = table.Column<int>(type: "int", nullable: true),
                    BirthCity = table.Column<int>(type: "int", nullable: true),
                    Nationality = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    PassportNo = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    NationalId = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    SocialSecurityId = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    DrivingLicenseNo = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    BloodGroup = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: true),
                    MaritalStatus = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: true),
                    IsStudent = table.Column<string>(type: "varchar(1)", maxLength: 1, nullable: true),
                    WorkPhone = table.Column<string>(type: "varchar(20)", maxLength: 20, nullable: true),
                    Mobile = table.Column<string>(type: "varchar(20)", maxLength: 20, nullable: true),
                    Phone = table.Column<string>(type: "varchar(20)", maxLength: 20, nullable: true),
                    Email = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),
                    Email2 = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),
                    FatherFirstName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    FatherMiddleName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    FatherLastName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    FatherContactNo = table.Column<string>(type: "varchar(20)", maxLength: 20, nullable: true),
                    FatherNationalId = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    MotherFirstName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    MotherMiddleName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    MotherLastName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    MotherContactNo = table.Column<string>(type: "varchar(20)", maxLength: 20, nullable: true),
                    MotherNationalId = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    SpouseFirstName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    SpouseMiddleName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    SpouseLastName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    SpouseContactNo = table.Column<string>(type: "varchar(20)", maxLength: 20, nullable: true),
                    SpouseNationalId = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    NoOfDependent = table.Column<int>(type: "int", nullable: true),
                    BackgroundCheckDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    BackgroundCheckBy = table.Column<int>(type: "int", nullable: true),
                    BackgroundCheckStatus = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    LastMedicalStatus = table.Column<string>(type: "varchar(150)", maxLength: 150, nullable: true),
                    LastMedicalCheckDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    MedicalHistory = table.Column<string>(type: "varchar(500)", maxLength: 500, nullable: true),
                    Signature = table.Column<string>(type: "varchar(150)", maxLength: 150, nullable: true),
                    ImgUrl = table.Column<string>(type: "varchar(150)", maxLength: 150, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),
                    Attribute7 = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),
                    Attribute8 = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),
                    Attribute9 = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),
                    Attribute10 = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),
                    Attribute11 = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),
                    Attribute12 = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),
                    Attribute13 = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),
                    Attribute14 = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),
                    Attribute15 = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),
                    PositionId = table.Column<int>(type: "int", nullable: true),
                    UserTypeId = table.Column<int>(type: "int", nullable: true),
                    GenderId = table.Column<int>(type: "int", nullable: true),
                    StatusId = table.Column<int>(type: "int", nullable: true),
                    UserName = table.Column<string>(type: "text", nullable: true),
                    HrEmployeeEmpHeaderId = table.Column<int>(type: "int", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HrEmployeeTransaction", x => x.TransactionId);
                    table.ForeignKey(
                        name: "FK_HrEmployeeTransaction_HrEmployee_HrEmployeeEmpHeaderId",
                        column: x => x.HrEmployeeEmpHeaderId,
                        principalTable: "HrEmployee",
                        principalColumn: "EmpHeaderId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "HrExcepShift",
                columns: table => new
                {
                    EmpHeaderId = table.Column<int>(type: "int", nullable: false),
                    ShiftDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    ExcepType = table.Column<int>(type: "int", nullable: false),
                    ShiftStartTime = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    ShiftEndTime = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    Remarks = table.Column<string>(type: "varchar(200)", maxLength: 200, nullable: true),
                    HrEmployeeEmpHeaderId = table.Column<int>(type: "int", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HrExcepShift", x => new { x.EmpHeaderId, x.ShiftDate });
                    table.ForeignKey(
                        name: "FK_HrExcepShift_HrEmployee_HrEmployeeEmpHeaderId",
                        column: x => x.HrEmployeeEmpHeaderId,
                        principalTable: "HrEmployee",
                        principalColumn: "EmpHeaderId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MailGroup",
                columns: table => new
                {
                    MailGroupTableId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    EmpHeaderId = table.Column<int>(type: "int", nullable: false),
                    MailGroupId = table.Column<int>(type: "int", nullable: false),
                    HrEmployeeEmpHeaderId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MailGroup", x => x.MailGroupTableId);
                    table.ForeignKey(
                        name: "FK_MailGroup_HrEmployee_HrEmployeeEmpHeaderId",
                        column: x => x.HrEmployeeEmpHeaderId,
                        principalTable: "HrEmployee",
                        principalColumn: "EmpHeaderId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "LeaveApplicationFile",
                columns: table => new
                {
                    FileId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    FileName = table.Column<string>(type: "text", nullable: true),
                    FileUrl = table.Column<string>(type: "text", nullable: true),
                    ApplicationId = table.Column<int>(type: "int", nullable: false),
                    HrEmployeeLeaveApplicationApplicationId = table.Column<int>(type: "int", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LeaveApplicationFile", x => x.FileId);
                    table.ForeignKey(
                        name: "FK_LeaveApplicationFile_HrEmployeeLeaveApplication_HrEmployeeLe~",
                        column: x => x.HrEmployeeLeaveApplicationApplicationId,
                        principalTable: "HrEmployeeLeaveApplication",
                        principalColumn: "ApplicationId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "HrPaySlipPayLogs",
                columns: table => new
                {
                    HrPaySlipPayLogId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    PaySlipMasterHeaderId = table.Column<int>(type: "int", nullable: false),
                    PaidAmount = table.Column<decimal>(type: "decimal(18, 2)", nullable: false),
                    PaidDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    Remarks = table.Column<string>(type: "text", nullable: true),
                    PayslipCurrentStatus = table.Column<int>(type: "int", nullable: false),
                    HrPaySlipTransactionMasterPaySlipMasterHeaderId = table.Column<int>(type: "int", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HrPaySlipPayLogs", x => x.HrPaySlipPayLogId);
                    table.ForeignKey(
                        name: "FK_HrPaySlipPayLogs_HrPaySlipTransactionMasters_HrPaySlipTransa~",
                        column: x => x.HrPaySlipTransactionMasterPaySlipMasterHeaderId,
                        principalTable: "HrPaySlipTransactionMasters",
                        principalColumn: "PaySlipMasterHeaderId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "HrPaySlipTransactionMasterLogHistorys",
                columns: table => new
                {
                    HrPaySlipTransactionMasterLogId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    PaySlipMasterHeaderId = table.Column<int>(type: "int", nullable: false),
                    PeriodId = table.Column<int>(type: "int", nullable: true),
                    EmpHeaderId = table.Column<int>(type: "int", nullable: true),
                    PositionId = table.Column<int>(type: "int", nullable: false),
                    PayrollId = table.Column<int>(type: "int", nullable: false),
                    TotalAmountPayable = table.Column<decimal>(type: "decimal(18, 2)", nullable: false),
                    TotalAdditionAmount = table.Column<decimal>(type: "decimal(18, 2)", nullable: false),
                    TotalDeductionAmount = table.Column<decimal>(type: "decimal(18, 2)", nullable: false),
                    PaidStatus = table.Column<int>(type: "int", nullable: false),
                    PayslipCurrentStatus = table.Column<int>(type: "int", nullable: false),
                    PayslipId = table.Column<string>(type: "text", nullable: true),
                    GenerationDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    HrPaySlipTransactionMasterPaySlipMasterHeaderId = table.Column<int>(type: "int", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HrPaySlipTransactionMasterLogHistorys", x => x.HrPaySlipTransactionMasterLogId);
                    table.ForeignKey(
                        name: "FK_HrPaySlipTransactionMasterLogHistorys_HrPaySlipTransactionMa~",
                        column: x => x.HrPaySlipTransactionMasterPaySlipMasterHeaderId,
                        principalTable: "HrPaySlipTransactionMasters",
                        principalColumn: "PaySlipMasterHeaderId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "HrPaySlipTransactionMasterLogs",
                columns: table => new
                {
                    HrPaySlipTransactionMasterLogId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    PaySlipMasterHeaderId = table.Column<int>(type: "int", nullable: false),
                    PeriodId = table.Column<int>(type: "int", nullable: true),
                    EmpHeaderId = table.Column<int>(type: "int", nullable: true),
                    PositionId = table.Column<int>(type: "int", nullable: false),
                    PayrollId = table.Column<int>(type: "int", nullable: false),
                    TotalAmountPayable = table.Column<decimal>(type: "decimal(18, 2)", nullable: false),
                    TotalAdditionAmount = table.Column<decimal>(type: "decimal(18, 2)", nullable: false),
                    TotalDeductionAmount = table.Column<decimal>(type: "decimal(18, 2)", nullable: false),
                    PaidStatus = table.Column<int>(type: "int", nullable: false),
                    PayslipCurrentStatus = table.Column<int>(type: "int", nullable: false),
                    PayslipId = table.Column<string>(type: "text", nullable: true),
                    GenerationDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    HrPaySlipTransactionMasterPaySlipMasterHeaderId = table.Column<int>(type: "int", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HrPaySlipTransactionMasterLogs", x => x.HrPaySlipTransactionMasterLogId);
                    table.ForeignKey(
                        name: "FK_HrPaySlipTransactionMasterLogs_HrPaySlipTransactionMasters_H~",
                        column: x => x.HrPaySlipTransactionMasterPaySlipMasterHeaderId,
                        principalTable: "HrPaySlipTransactionMasters",
                        principalColumn: "PaySlipMasterHeaderId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "HrPaySlipTransactions",
                columns: table => new
                {
                    PaySlipTransactionHeaderId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    PayHeadId = table.Column<int>(type: "int", nullable: false),
                    Unit = table.Column<decimal>(type: "decimal(18, 2)", nullable: false),
                    CalculatedValue = table.Column<decimal>(type: "decimal(18, 2)", nullable: false),
                    IsPercentage = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    PaySlipMasterHeaderId = table.Column<int>(type: "int", nullable: false),
                    HrPaySlipTransactionMasterPaySlipMasterHeaderId = table.Column<int>(type: "int", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HrPaySlipTransactions", x => x.PaySlipTransactionHeaderId);
                    table.ForeignKey(
                        name: "FK_HrPaySlipTransactions_HrPaySlipTransactionMasters_HrPaySlipT~",
                        column: x => x.HrPaySlipTransactionMasterPaySlipMasterHeaderId,
                        principalTable: "HrPaySlipTransactionMasters",
                        principalColumn: "PaySlipMasterHeaderId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "HrEmployeeShift",
                columns: table => new
                {
                    EmpHeaderId = table.Column<int>(type: "int", nullable: false),
                    ShiftId = table.Column<int>(type: "int", nullable: false),
                    StartDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    EndDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    ShiftType = table.Column<int>(type: "int", nullable: true),
                    Status = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    HrEmployeeEmpHeaderId = table.Column<int>(type: "int", nullable: true),
                    HrShiftShiftId = table.Column<int>(type: "int", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HrEmployeeShift", x => new { x.EmpHeaderId, x.ShiftId });
                    table.ForeignKey(
                        name: "FK_HrEmployeeShift_HrEmployee_HrEmployeeEmpHeaderId",
                        column: x => x.HrEmployeeEmpHeaderId,
                        principalTable: "HrEmployee",
                        principalColumn: "EmpHeaderId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_HrEmployeeShift_HrShift_HrShiftShiftId",
                        column: x => x.HrShiftShiftId,
                        principalTable: "HrShift",
                        principalColumn: "ShiftId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "JobApply",
                columns: table => new
                {
                    JobApplyId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    ApplyDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    BssJobId = table.Column<int>(type: "int", nullable: false),
                    JobApplicantId = table.Column<long>(type: "bigint", nullable: false),
                    JobId = table.Column<int>(type: "int", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_JobApply", x => x.JobApplyId);
                    table.ForeignKey(
                        name: "FK_JobApply_Job_JobId",
                        column: x => x.JobId,
                        principalTable: "Job",
                        principalColumn: "JobId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_JobApply_JobApplicant_JobApplicantId",
                        column: x => x.JobApplicantId,
                        principalTable: "JobApplicant",
                        principalColumn: "JobApplicantId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PlannerEvent",
                columns: table => new
                {
                    EventId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    EventName = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: false),
                    EventShortName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    EventTypeId = table.Column<int>(type: "int", nullable: true),
                    MasterEventId = table.Column<int>(type: "int", nullable: true),
                    PlannerEventTypeEventTypeId = table.Column<int>(type: "int", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlannerEvent", x => x.EventId);
                    table.ForeignKey(
                        name: "FK_PlannerEvent_PlannerEventType_PlannerEventTypeEventTypeId",
                        column: x => x.PlannerEventTypeEventTypeId,
                        principalTable: "PlannerEventType",
                        principalColumn: "EventTypeId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "OfficialResignRules",
                columns: table => new
                {
                    ResignationPropertiesId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    FlexValueId = table.Column<int>(type: "int", nullable: false),
                    IsApplied = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    ResignationId = table.Column<int>(type: "int", nullable: false),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OfficialResignRules", x => x.ResignationPropertiesId);
                    table.ForeignKey(
                        name: "FK_OfficialResignRules_Resignations_ResignationId",
                        column: x => x.ResignationId,
                        principalTable: "Resignations",
                        principalColumn: "ResignationId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ClassRoomColumns",
                columns: table => new
                {
                    ClassRoomColumnId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    RoomSetupHeaderId = table.Column<int>(type: "int", nullable: false),
                    ColumnSerial = table.Column<int>(type: "int", nullable: false),
                    NoOfRow = table.Column<int>(type: "int", nullable: false),
                    RoomSetupHeaderId1 = table.Column<int>(type: "int", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClassRoomColumns", x => x.ClassRoomColumnId);
                    table.ForeignKey(
                        name: "FK_ClassRoomColumns_RoomSetups_RoomSetupHeaderId1",
                        column: x => x.RoomSetupHeaderId1,
                        principalTable: "RoomSetups",
                        principalColumn: "RoomSetupHeaderId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "GeneralInfo",
                columns: table => new
                {
                    InfoId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    InfoTypeId = table.Column<int>(type: "int", nullable: false),
                    SchoolId = table.Column<int>(type: "int", nullable: false),
                    Sequnece = table.Column<int>(type: "int", nullable: true),
                    InfoTitle = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false),
                    InfoDescription = table.Column<string>(type: "text", nullable: true),
                    GeneralInfoTypeInfoTypeId = table.Column<int>(type: "int", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GeneralInfo", x => x.InfoId);
                    table.ForeignKey(
                        name: "FK_GeneralInfo_GeneralInfoType_GeneralInfoTypeInfoTypeId",
                        column: x => x.GeneralInfoTypeInfoTypeId,
                        principalTable: "GeneralInfoType",
                        principalColumn: "InfoTypeId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_GeneralInfo_School_SchoolId",
                        column: x => x.SchoolId,
                        principalTable: "School",
                        principalColumn: "SchoolId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Logo",
                columns: table => new
                {
                    LogoId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    LogoTypeId = table.Column<int>(type: "int", nullable: false),
                    SchoolId = table.Column<int>(type: "int", nullable: false),
                    LogoTitle = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false),
                    LogoUrl = table.Column<string>(type: "varchar(150)", maxLength: 150, nullable: false),
                    LogoDetails = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Logo", x => x.LogoId);
                    table.ForeignKey(
                        name: "FK_Logo_LogoType_LogoTypeId",
                        column: x => x.LogoTypeId,
                        principalTable: "LogoType",
                        principalColumn: "LogoTypeId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Logo_School_SchoolId",
                        column: x => x.SchoolId,
                        principalTable: "School",
                        principalColumn: "SchoolId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SchoolBranch",
                columns: table => new
                {
                    SchoolId = table.Column<int>(type: "int", nullable: false),
                    BranchId = table.Column<int>(type: "int", nullable: false),
                    BranchName = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SchoolBranch", x => new { x.SchoolId, x.BranchId });
                    table.ForeignKey(
                        name: "FK_SchoolBranch_School_SchoolId",
                        column: x => x.SchoolId,
                        principalTable: "School",
                        principalColumn: "SchoolId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SchoolLinks",
                columns: table => new
                {
                    SocialId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    SocialTypeId = table.Column<int>(type: "int", nullable: false),
                    SchoolId = table.Column<int>(type: "int", nullable: false),
                    SocialUrl = table.Column<string>(type: "varchar(150)", maxLength: 150, nullable: true),
                    SocialIcon = table.Column<string>(type: "varchar(150)", maxLength: 150, nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SchoolLinks", x => x.SocialId);
                    table.ForeignKey(
                        name: "FK_SchoolLinks_School_SchoolId",
                        column: x => x.SchoolId,
                        principalTable: "School",
                        principalColumn: "SchoolId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SchoolLinks_SchoolType_SocialTypeId",
                        column: x => x.SocialTypeId,
                        principalTable: "SchoolType",
                        principalColumn: "SocialTypeId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AlertHistory",
                columns: table => new
                {
                    AlertHistoryId = table.Column<int>(type: "int", nullable: false),
                    RunTime = table.Column<DateTime>(type: "datetime", nullable: false),
                    Status = table.Column<int>(type: "int", nullable: true),
                    ErrorMsg = table.Column<string>(type: "varchar(500)", maxLength: 500, nullable: true),
                    SchoolAlertAlertId = table.Column<int>(type: "int", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AlertHistory", x => x.AlertHistoryId);
                    table.ForeignKey(
                        name: "FK_AlertHistory_ScoolAlert_SchoolAlertAlertId",
                        column: x => x.SchoolAlertAlertId,
                        principalTable: "ScoolAlert",
                        principalColumn: "AlertId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "BillInfo",
                columns: table => new
                {
                    BillInfoId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    SchoolId = table.Column<int>(type: "int", nullable: true),
                    CampusId = table.Column<int>(type: "int", nullable: true),
                    SessionId = table.Column<int>(type: "int", nullable: false),
                    FeeSubCategoryId = table.Column<int>(type: "int", nullable: false),
                    ClassId = table.Column<int>(type: "int", nullable: false),
                    BillAtm = table.Column<decimal>(type: "decimal(18, 2)", nullable: true),
                    MaxDiscPct = table.Column<decimal>(type: "decimal(18, 2)", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BillInfo", x => x.BillInfoId);
                    table.ForeignKey(
                        name: "FK_BillInfo_Class_ClassId",
                        column: x => x.ClassId,
                        principalTable: "Class",
                        principalColumn: "ClassId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BillInfo_School_SchoolId",
                        column: x => x.SchoolId,
                        principalTable: "School",
                        principalColumn: "SchoolId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BillInfo_Session_SessionId",
                        column: x => x.SessionId,
                        principalTable: "Session",
                        principalColumn: "SessionId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "StudentSubjectMasters",
                columns: table => new
                {
                    StudentSubjectMasterId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    SessionId = table.Column<int>(type: "int", nullable: false),
                    ClassId = table.Column<int>(type: "int", nullable: false),
                    SectionId = table.Column<int>(type: "int", nullable: false),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentSubjectMasters", x => x.StudentSubjectMasterId);
                    table.ForeignKey(
                        name: "FK_StudentSubjectMasters_Class_ClassId",
                        column: x => x.ClassId,
                        principalTable: "Class",
                        principalColumn: "ClassId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_StudentSubjectMasters_Section_SectionId",
                        column: x => x.SectionId,
                        principalTable: "Section",
                        principalColumn: "SectionId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_StudentSubjectMasters_Session_SessionId",
                        column: x => x.SessionId,
                        principalTable: "Session",
                        principalColumn: "SessionId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "StudentTransaction",
                columns: table => new
                {
                    TransactionId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    StudentHeaderId = table.Column<int>(type: "int", nullable: false),
                    StudentId = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: false),
                    FirstName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: false),
                    MiddleName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    LastName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    NickName = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    PunchCardNo = table.Column<string>(type: "text", nullable: true),
                    PresHoldingNo = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    PresStreet = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    PresArea = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    PresThanaId = table.Column<int>(type: "int", nullable: true),
                    PresPostCode = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: true),
                    PresCityId = table.Column<int>(type: "int", nullable: true),
                    PresDivisionId = table.Column<int>(type: "int", nullable: true),
                    PresCountryId = table.Column<int>(type: "int", nullable: true),
                    DeviceNumber = table.Column<int>(type: "int", nullable: true),
                    RollNumber = table.Column<int>(type: "int", nullable: true),
                    PermHoldingNo = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    PermStreet = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    PermArea = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    PermPostCode = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: true),
                    PermThanaId = table.Column<int>(type: "int", nullable: true),
                    PermCityId = table.Column<int>(type: "int", nullable: true),
                    PermDivisionId = table.Column<int>(type: "int", nullable: true),
                    PermCountryId = table.Column<int>(type: "int", nullable: true),
                    Dob = table.Column<DateTime>(type: "datetime", nullable: true),
                    BirthCity = table.Column<int>(type: "int", nullable: true),
                    BirthCountry = table.Column<int>(type: "int", nullable: true),
                    PlaceOfBirth = table.Column<string>(type: "varchar(60)", maxLength: 60, nullable: true),
                    AdmissionDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    ReadmissionDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    AdmissionSessionId = table.Column<int>(type: "int", nullable: true),
                    ReadmissionSessionId = table.Column<int>(type: "int", nullable: true),
                    AdmissionClassId = table.Column<int>(type: "int", nullable: true),
                    AdmissionSectionId = table.Column<int>(type: "int", nullable: true),
                    AdmissionShift = table.Column<int>(type: "int", nullable: true),
                    SessionId = table.Column<int>(type: "int", nullable: true),
                    Age = table.Column<string>(type: "text", nullable: true),
                    ClassId = table.Column<int>(type: "int", nullable: true),
                    SectionId = table.Column<int>(type: "int", nullable: true),
                    Shift = table.Column<int>(type: "int", nullable: true),
                    CampusId = table.Column<int>(type: "int", nullable: true),
                    EndDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    BirthDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    Status = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    StatusId = table.Column<int>(type: "int", nullable: true),
                    Gender = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: true),
                    GenderId = table.Column<int>(type: "int", nullable: true),
                    Religion = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    ReligionId = table.Column<int>(type: "int", nullable: true),
                    ResPhone = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    Mobile = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    Email = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),
                    BloodGroup = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: true),
                    BloodId = table.Column<int>(type: "int", nullable: true),
                    Nationality = table.Column<string>(type: "varchar(60)", maxLength: 60, nullable: true),
                    NationalityId = table.Column<int>(type: "int", nullable: true),
                    BirthCertificateNo = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    PassportNo = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    MedicalHistory = table.Column<string>(type: "varchar(200)", maxLength: 200, nullable: true),
                    EmergencyMedicalAction = table.Column<string>(type: "varchar(200)", maxLength: 200, nullable: true),
                    FatherFirstName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    FatherMiddleName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    FatherLastName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    FatherEducation = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    FatherEducationDetails = table.Column<string>(type: "varchar(200)", maxLength: 200, nullable: true),
                    FatherOccupation = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    FatherOccupationDetails = table.Column<string>(type: "varchar(150)", maxLength: 150, nullable: true),
                    FatherOfficeAddress = table.Column<string>(type: "varchar(200)", maxLength: 200, nullable: true),
                    FatherOfficePhone = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    FatherMobile = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    FatherNationalId = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    FatherPassportNo = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    MotherFirstName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    MotherMiddleName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    MotherLastName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    MotherEducation = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    MotherEducationDetails = table.Column<string>(type: "varchar(200)", maxLength: 200, nullable: true),
                    MotherOccupation = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    MotherOccupationDetails = table.Column<string>(type: "varchar(150)", maxLength: 150, nullable: true),
                    MotherOfficeAddress = table.Column<string>(type: "varchar(200)", maxLength: 200, nullable: true),
                    LocGuardianOfficeAddress = table.Column<string>(type: "text", nullable: true),
                    MotherOfficePhone = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    MotherMobile = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    MotherNationalId = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    MoherPassportNo = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    LocGuardianFirstName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    LocGuardianMiddleName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    LocGuardianLastName = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    LocGuardianRelationship = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    LocGuardianMobile = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    LocalGurdianNationalId = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    LocalGurdianPassportNo = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true),
                    StudentPhotoPath = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    ProximityNum = table.Column<string>(type: "varchar(15)", maxLength: 15, nullable: true),
                    ExitReasons = table.Column<string>(type: "varchar(15)", maxLength: 15, nullable: true),
                    StudentCategoryId = table.Column<int>(type: "int", nullable: true),
                    BkupDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    BkupUserId = table.Column<int>(type: "int", nullable: true),
                    StudentsStudentHeaderId = table.Column<int>(type: "int", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute7 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute8 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute9 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute10 = table.Column<string>(type: "text", nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentTransaction", x => x.TransactionId);
                    table.ForeignKey(
                        name: "FK_StudentTransaction_Student_StudentsStudentHeaderId",
                        column: x => x.StudentsStudentHeaderId,
                        principalTable: "Student",
                        principalColumn: "StudentHeaderId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "StudentBillLine",
                columns: table => new
                {
                    BillHeaderId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    BillDtlId = table.Column<int>(type: "int", nullable: true),
                    RcvType = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: true),
                    FeeSubCategoryId = table.Column<int>(type: "int", nullable: true),
                    BillAmount = table.Column<decimal>(type: "decimal(18, 2)", nullable: true),
                    ActualRcvAmount = table.Column<decimal>(type: "decimal(18, 2)", nullable: true),
                    DiscAmount = table.Column<decimal>(type: "decimal(18, 2)", nullable: true),
                    PeriodId = table.Column<int>(type: "int", nullable: true),
                    Remarks = table.Column<string>(type: "varchar(200)", maxLength: 200, nullable: true),
                    VatAmt = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),
                    WaiberPercentage = table.Column<decimal>(type: "decimal(18, 2)", nullable: true),
                    WaiberAmount = table.Column<decimal>(type: "decimal(18, 2)", nullable: true),
                    LateFee = table.Column<decimal>(type: "decimal(18, 2)", nullable: true),
                    YearId = table.Column<int>(type: "int", nullable: false),
                    Segment1 = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: true),
                    Segment2 = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: true),
                    Segment3 = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: true),
                    Segment4 = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: true),
                    Segment5 = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: true),
                    Segment6 = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: true),
                    Segment7 = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: true),
                    DiscAcc = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentBillLine", x => x.BillHeaderId);
                    table.ForeignKey(
                        name: "FK_StudentBillLine_StudentBillMaster_BillDtlId",
                        column: x => x.BillDtlId,
                        principalTable: "StudentBillMaster",
                        principalColumn: "BillHeaderId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "StudentBillLineTemporaries",
                columns: table => new
                {
                    BillHeaderId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    BillDtlId = table.Column<int>(type: "int", nullable: true),
                    RcvType = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: true),
                    FeeSubCategoryId = table.Column<int>(type: "int", nullable: true),
                    BillAmount = table.Column<decimal>(type: "decimal(18, 2)", nullable: true),
                    ActualRcvAmount = table.Column<decimal>(type: "decimal(18, 2)", nullable: true),
                    DiscAmount = table.Column<decimal>(type: "decimal(18, 2)", nullable: true),
                    PeriodId = table.Column<int>(type: "int", nullable: true),
                    Remarks = table.Column<string>(type: "varchar(200)", maxLength: 200, nullable: true),
                    VatAmt = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),
                    WaiberPercentage = table.Column<decimal>(type: "decimal(18, 2)", nullable: true),
                    WaiberAmount = table.Column<decimal>(type: "decimal(18, 2)", nullable: true),
                    LateFee = table.Column<decimal>(type: "decimal(18, 2)", nullable: true),
                    YearId = table.Column<int>(type: "int", nullable: false),
                    Segment1 = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: true),
                    Segment2 = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: true),
                    Segment3 = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: true),
                    Segment4 = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: true),
                    Segment5 = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: true),
                    Segment6 = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: true),
                    Segment7 = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: true),
                    DiscAcc = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentBillLineTemporaries", x => x.BillHeaderId);
                    table.ForeignKey(
                        name: "FK_StudentBillLineTemporaries_StudentBillMasterTemporaries_Bill~",
                        column: x => x.BillDtlId,
                        principalTable: "StudentBillMasterTemporaries",
                        principalColumn: "BillHeaderId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "StudentPosition",
                columns: table => new
                {
                    PositionId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    PositionLogId = table.Column<int>(type: "int", nullable: false),
                    StudentId = table.Column<int>(type: "int", nullable: false),
                    ClassId = table.Column<int>(type: "int", nullable: false),
                    SectionId = table.Column<int>(type: "int", nullable: false),
                    StudentPositionClass = table.Column<int>(type: "int", nullable: false),
                    StudentPositionSection = table.Column<int>(type: "int", nullable: false),
                    Mark = table.Column<decimal>(type: "decimal(18, 2)", nullable: true),
                    Atten = table.Column<decimal>(type: "decimal(18, 2)", nullable: true),
                    StudentPositionLogPositionLogId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentPosition", x => x.PositionId);
                    table.ForeignKey(
                        name: "FK_StudentPosition_StudentPositionLog_StudentPositionLogPositio~",
                        column: x => x.StudentPositionLogPositionLogId,
                        principalTable: "StudentPositionLog",
                        principalColumn: "PositionLogId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AlternateSubject",
                columns: table => new
                {
                    AlternateSubjectId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    SubjectId = table.Column<int>(type: "int", nullable: false),
                    ReferenceType = table.Column<string>(type: "text", nullable: true),
                    ReferenceId = table.Column<int>(type: "int", nullable: false),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AlternateSubject", x => x.AlternateSubjectId);
                    table.ForeignKey(
                        name: "FK_AlternateSubject_Subject_SubjectId",
                        column: x => x.SubjectId,
                        principalTable: "Subject",
                        principalColumn: "SubjectId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ClassSectionRoutine",
                columns: table => new
                {
                    RoutineId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    ClassId = table.Column<int>(type: "int", nullable: false),
                    SectionId = table.Column<int>(type: "int", nullable: false),
                    DayInTheWeek = table.Column<int>(type: "int", nullable: false),
                    SessionId = table.Column<int>(type: "int", nullable: true),
                    SubjectId = table.Column<int>(type: "int", nullable: true),
                    StartTime = table.Column<string>(type: "text", nullable: true),
                    EndTime = table.Column<string>(type: "text", nullable: true),
                    ClassRoomHeaderId = table.Column<int>(type: "int", nullable: false),
                    ClassRoomHeaderId1 = table.Column<int>(type: "int", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClassSectionRoutine", x => x.RoutineId);
                    table.ForeignKey(
                        name: "FK_ClassSectionRoutine_ClassRooms_ClassRoomHeaderId1",
                        column: x => x.ClassRoomHeaderId1,
                        principalTable: "ClassRooms",
                        principalColumn: "ClassRoomHeaderId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ClassSectionRoutine_Subject_SubjectId",
                        column: x => x.SubjectId,
                        principalTable: "Subject",
                        principalColumn: "SubjectId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ClassSectionRoutineTransacitons",
                columns: table => new
                {
                    ClassRoutineTransacitonId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    ClassId = table.Column<int>(type: "int", nullable: false),
                    SectionId = table.Column<int>(type: "int", nullable: false),
                    SessionId = table.Column<int>(type: "int", nullable: true),
                    DayInTheWeek = table.Column<int>(type: "int", nullable: false),
                    SubjectId = table.Column<int>(type: "int", nullable: true),
                    StartTime = table.Column<string>(type: "text", nullable: true),
                    EndTime = table.Column<string>(type: "text", nullable: true),
                    ClassRoomHeaderId = table.Column<int>(type: "int", nullable: false),
                    ClassRoomHeaderId1 = table.Column<int>(type: "int", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClassSectionRoutineTransacitons", x => x.ClassRoutineTransacitonId);
                    table.ForeignKey(
                        name: "FK_ClassSectionRoutineTransacitons_ClassRooms_ClassRoomHeaderId1",
                        column: x => x.ClassRoomHeaderId1,
                        principalTable: "ClassRooms",
                        principalColumn: "ClassRoomHeaderId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ClassSectionRoutineTransacitons_Subject_SubjectId",
                        column: x => x.SubjectId,
                        principalTable: "Subject",
                        principalColumn: "SubjectId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ClassSubject",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    ClassId = table.Column<int>(type: "int", nullable: false),
                    SubjectId = table.Column<int>(type: "int", nullable: false),
                    ResultSerial = table.Column<int>(type: "int", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClassSubject", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ClassSubject_Class_ClassId",
                        column: x => x.ClassId,
                        principalTable: "Class",
                        principalColumn: "ClassId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ClassSubject_Subject_SubjectId",
                        column: x => x.SubjectId,
                        principalTable: "Subject",
                        principalColumn: "SubjectId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "EventRoutine",
                columns: table => new
                {
                    RoutineId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    SessionId = table.Column<int>(type: "int", nullable: true),
                    EventDtlsId = table.Column<int>(type: "int", nullable: true),
                    ClassId = table.Column<int>(type: "int", nullable: false),
                    SectionId = table.Column<int>(type: "int", nullable: true),
                    SubjectId = table.Column<int>(type: "int", nullable: false),
                    StartDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EndDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EventRoutine", x => x.RoutineId);
                    table.ForeignKey(
                        name: "FK_EventRoutine_Class_ClassId",
                        column: x => x.ClassId,
                        principalTable: "Class",
                        principalColumn: "ClassId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EventRoutine_Subject_SubjectId",
                        column: x => x.SubjectId,
                        principalTable: "Subject",
                        principalColumn: "SubjectId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserLoginTracer",
                columns: table => new
                {
                    LoginTracerId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    LoginUserId = table.Column<int>(type: "int", nullable: true),
                    IPAddress = table.Column<string>(type: "text", nullable: true),
                    CountryName = table.Column<string>(type: "text", nullable: true),
                    Region = table.Column<string>(type: "text", nullable: true),
                    CityName = table.Column<string>(type: "text", nullable: true),
                    PostalCode = table.Column<string>(type: "text", nullable: true),
                    Latitude = table.Column<string>(type: "text", nullable: true),
                    Longitude = table.Column<string>(type: "text", nullable: true),
                    TimeZone = table.Column<string>(type: "text", nullable: true),
                    Organization = table.Column<string>(type: "text", nullable: true),
                    LoggedInDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LoggedOutDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    IsBlocked = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    BlockReason = table.Column<string>(type: "text", nullable: true),
                    BlockDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    RedirectURL = table.Column<string>(type: "text", nullable: true),
                    Status = table.Column<int>(type: "int", nullable: true),
                    UserId = table.Column<int>(type: "int", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserLoginTracer", x => x.LoginTracerId);
                    table.ForeignKey(
                        name: "FK_UserLoginTracer_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserRoles",
                columns: table => new
                {
                    UserId = table.Column<int>(type: "int", nullable: false),
                    RoleId = table.Column<int>(type: "int", nullable: false),
                    Status = table.Column<int>(type: "int", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_UserRoles_Roles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "Roles",
                        principalColumn: "RoleId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserRoles_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "IssuedBooks",
                columns: table => new
                {
                    IssuedBookId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    BookId = table.Column<int>(type: "int", nullable: false),
                    AuthorId = table.Column<int>(type: "int", nullable: false),
                    UserType = table.Column<int>(type: "int", nullable: false),
                    UserReferenceId = table.Column<int>(type: "int", nullable: false),
                    IssueDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    DueDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    ReturnDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    IssueStatus = table.Column<int>(type: "int", nullable: false),
                    Remarks = table.Column<string>(type: "text", nullable: true),
                    DailyFineAmount = table.Column<decimal>(type: "decimal(18, 2)", nullable: true),
                    TotalFineAmount = table.Column<decimal>(type: "decimal(18, 2)", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IssuedBooks", x => x.IssuedBookId);
                    table.ForeignKey(
                        name: "FK_IssuedBooks_Book_BookId",
                        column: x => x.BookId,
                        principalTable: "Book",
                        principalColumn: "BookId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DateWiseSubjects",
                columns: table => new
                {
                    DateWiseSubjectId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    Date = table.Column<DateTime>(type: "datetime", nullable: false),
                    IsDateAssigned = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    SubjectId = table.Column<int>(type: "int", nullable: false),
                    StartTime = table.Column<string>(type: "text", nullable: true),
                    EndTime = table.Column<string>(type: "text", nullable: true),
                    ExamScheduleLineId = table.Column<int>(type: "int", nullable: false),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DateWiseSubjects", x => x.DateWiseSubjectId);
                    table.ForeignKey(
                        name: "FK_DateWiseSubjects_ExamScheduleLines_ExamScheduleLineId",
                        column: x => x.ExamScheduleLineId,
                        principalTable: "ExamScheduleLines",
                        principalColumn: "ExamScheduleLineId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "FeeSubCategoryFine",
                columns: table => new
                {
                    SubCategoryFineId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    FeeSubCategoryId = table.Column<int>(type: "int", nullable: false),
                    IsPercentage = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    Amount = table.Column<decimal>(type: "decimal(18, 2)", nullable: false),
                    IsIncremental = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    IncrementType = table.Column<int>(type: "int", nullable: true),
                    DaysForIncrement = table.Column<int>(type: "int", nullable: true),
                    MaximumIncrementAmount = table.Column<decimal>(type: "decimal(18, 2)", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FeeSubCategoryFine", x => x.SubCategoryFineId);
                    table.ForeignKey(
                        name: "FK_FeeSubCategoryFine_FeeSubCategory_FeeSubCategoryId",
                        column: x => x.FeeSubCategoryId,
                        principalTable: "FeeSubCategory",
                        principalColumn: "FeeSubCategoryId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "FeeSubCategoryWaiver",
                columns: table => new
                {
                    FeeSubCategoryWaiverId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    FeeSubCategoryId = table.Column<int>(type: "int", nullable: false),
                    Category_GenderID = table.Column<int>(type: "int", nullable: false),
                    WaiverType = table.Column<int>(type: "int", nullable: false),
                    Percentage = table.Column<decimal>(type: "decimal(18, 2)", nullable: false),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FeeSubCategoryWaiver", x => x.FeeSubCategoryWaiverId);
                    table.ForeignKey(
                        name: "FK_FeeSubCategoryWaiver_FeeSubCategory_FeeSubCategoryId",
                        column: x => x.FeeSubCategoryId,
                        principalTable: "FeeSubCategory",
                        principalColumn: "FeeSubCategoryId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SubCategoryFeePeriods",
                columns: table => new
                {
                    BillPeriodId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    PeriodName = table.Column<string>(type: "text", nullable: true),
                    FeeSubCategoryId = table.Column<int>(type: "int", nullable: false),
                    StartDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    EndDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    Year = table.Column<int>(type: "int", nullable: true),
                    Month = table.Column<int>(type: "int", nullable: true),
                    Day = table.Column<int>(type: "int", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubCategoryFeePeriods", x => x.BillPeriodId);
                    table.ForeignKey(
                        name: "FK_SubCategoryFeePeriods_FeeSubCategory_FeeSubCategoryId",
                        column: x => x.FeeSubCategoryId,
                        principalTable: "FeeSubCategory",
                        principalColumn: "FeeSubCategoryId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SubCategoryFeeType",
                columns: table => new
                {
                    SubCategoryFeeTypeId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    FeeSubCategoryId = table.Column<int>(type: "int", nullable: false),
                    StartDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    EndDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    DueDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubCategoryFeeType", x => x.SubCategoryFeeTypeId);
                    table.ForeignKey(
                        name: "FK_SubCategoryFeeType_FeeSubCategory_FeeSubCategoryId",
                        column: x => x.FeeSubCategoryId,
                        principalTable: "FeeSubCategory",
                        principalColumn: "FeeSubCategoryId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ClassSection",
                columns: table => new
                {
                    ClassSectionId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    ClassId = table.Column<int>(type: "int", nullable: false),
                    SectionId = table.Column<int>(type: "int", nullable: false),
                    Shift = table.Column<int>(type: "int", nullable: false),
                    CampusId = table.Column<int>(type: "int", nullable: true),
                    StartTime = table.Column<string>(type: "text", nullable: true),
                    EndTime = table.Column<string>(type: "text", nullable: true),
                    NoOfStudent = table.Column<int>(type: "int", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClassSection", x => x.ClassSectionId);
                    table.ForeignKey(
                        name: "FK_ClassSection_Class_ClassId",
                        column: x => x.ClassId,
                        principalTable: "Class",
                        principalColumn: "ClassId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ClassSection_FndFlexValue_Shift",
                        column: x => x.Shift,
                        principalTable: "FndFlexValue",
                        principalColumn: "FlexValueId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ClassSection_Section_SectionId",
                        column: x => x.SectionId,
                        principalTable: "Section",
                        principalColumn: "SectionId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "HrPaySlipTransactionLogHistorys",
                columns: table => new
                {
                    HrPaySlipTransactionLogId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    HrPaySlipTransactionMasterLogId = table.Column<int>(type: "int", nullable: false),
                    PayHeadId = table.Column<int>(type: "int", nullable: false),
                    Unit = table.Column<decimal>(type: "decimal(18, 2)", nullable: false),
                    CalculatedValue = table.Column<decimal>(type: "decimal(18, 2)", nullable: false),
                    IsPercentage = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    HrPaySlipTransactionMasterLogHistoryHrPaySlipTransactionMasterL = table.Column<int>(name: "HrPaySlipTransactionMasterLogHistoryHrPaySlipTransactionMasterL~", type: "int", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HrPaySlipTransactionLogHistorys", x => x.HrPaySlipTransactionLogId);
                    table.ForeignKey(
                        name: "FK_HrPaySlipTransactionLogHistorys_HrPaySlipTransactionMasterLo~",
                        column: x => x.HrPaySlipTransactionMasterLogHistoryHrPaySlipTransactionMasterL,
                        principalTable: "HrPaySlipTransactionMasterLogHistorys",
                        principalColumn: "HrPaySlipTransactionMasterLogId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "HrPaySlipTransactionLogAfterPayheadChanges",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    HrPaySlipTransactionMasterLogId = table.Column<int>(type: "int", nullable: false),
                    PayHeadId = table.Column<int>(type: "int", nullable: false),
                    Unit = table.Column<decimal>(type: "decimal(18, 2)", nullable: false),
                    CalculatedValue = table.Column<decimal>(type: "decimal(18, 2)", nullable: false),
                    IsPercentage = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HrPaySlipTransactionLogAfterPayheadChanges", x => x.Id);
                    table.ForeignKey(
                        name: "FK_HrPaySlipTransactionLogAfterPayheadChanges_HrPaySlipTransact~",
                        column: x => x.HrPaySlipTransactionMasterLogId,
                        principalTable: "HrPaySlipTransactionMasterLogs",
                        principalColumn: "HrPaySlipTransactionMasterLogId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "HrPaySlipTransactionLogs",
                columns: table => new
                {
                    HrPaySlipTransactionLogId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    HrPaySlipTransactionMasterLogId = table.Column<int>(type: "int", nullable: false),
                    PayHeadId = table.Column<int>(type: "int", nullable: false),
                    Unit = table.Column<decimal>(type: "decimal(18, 2)", nullable: false),
                    CalculatedValue = table.Column<decimal>(type: "decimal(18, 2)", nullable: false),
                    IsPercentage = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HrPaySlipTransactionLogs", x => x.HrPaySlipTransactionLogId);
                    table.ForeignKey(
                        name: "FK_HrPaySlipTransactionLogs_HrPaySlipTransactionMasterLogs_HrPa~",
                        column: x => x.HrPaySlipTransactionMasterLogId,
                        principalTable: "HrPaySlipTransactionMasterLogs",
                        principalColumn: "HrPaySlipTransactionMasterLogId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PlannerEventDetails",
                columns: table => new
                {
                    EventDtlsId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    SessionId = table.Column<int>(type: "int", nullable: false),
                    EventId = table.Column<int>(type: "int", nullable: false),
                    StartDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    EndDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true),
                    PlannerEventEventId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlannerEventDetails", x => x.EventDtlsId);
                    table.ForeignKey(
                        name: "FK_PlannerEventDetails_PlannerEvent_PlannerEventEventId",
                        column: x => x.PlannerEventEventId,
                        principalTable: "PlannerEvent",
                        principalColumn: "EventId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "StudentMarks",
                columns: table => new
                {
                    MarksId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    SessionId = table.Column<int>(type: "int", nullable: false),
                    StudentHeaderId = table.Column<int>(type: "int", nullable: false),
                    ClassId = table.Column<int>(type: "int", nullable: false),
                    SectionId = table.Column<int>(type: "int", nullable: false),
                    Shift = table.Column<int>(type: "int", nullable: false),
                    TeacherId = table.Column<int>(type: "int", nullable: false),
                    SubjectId = table.Column<int>(type: "int", nullable: false),
                    ExamId = table.Column<int>(type: "int", nullable: true),
                    ExamTypeId = table.Column<int>(type: "int", nullable: false),
                    EventId = table.Column<int>(type: "int", nullable: true),
                    OutOfMarks = table.Column<decimal>(type: "decimal(18, 2)", nullable: false),
                    ObtainedMarks = table.Column<decimal>(type: "decimal(18, 2)", nullable: true),
                    AttendanceType = table.Column<int>(type: "int", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentMarks", x => x.MarksId);
                    table.ForeignKey(
                        name: "FK_StudentMarks_Class_ClassId",
                        column: x => x.ClassId,
                        principalTable: "Class",
                        principalColumn: "ClassId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_StudentMarks_PlannerEvent_EventId",
                        column: x => x.EventId,
                        principalTable: "PlannerEvent",
                        principalColumn: "EventId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_StudentMarks_Section_SectionId",
                        column: x => x.SectionId,
                        principalTable: "Section",
                        principalColumn: "SectionId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_StudentMarks_Subject_SubjectId",
                        column: x => x.SubjectId,
                        principalTable: "Subject",
                        principalColumn: "SubjectId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SubjectSubgroupClass",
                columns: table => new
                {
                    SubgroupClassId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    SubgroupId = table.Column<int>(type: "int", nullable: true),
                    ClassId = table.Column<int>(type: "int", nullable: true),
                    ShortName = table.Column<string>(type: "text", nullable: true),
                    MasterEventId = table.Column<int>(type: "int", nullable: true),
                    EventId = table.Column<int>(type: "int", nullable: true),
                    NoOfExam = table.Column<int>(type: "int", nullable: true),
                    BestCount = table.Column<int>(type: "int", nullable: true),
                    StartDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EndDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LasteUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubjectSubgroupClass", x => x.SubgroupClassId);
                    table.ForeignKey(
                        name: "FK_SubjectSubgroupClass_PlannerEvent_EventId",
                        column: x => x.EventId,
                        principalTable: "PlannerEvent",
                        principalColumn: "EventId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SubjectSubgroupClass_SubjectSubgroup_SubgroupId",
                        column: x => x.SubgroupId,
                        principalTable: "SubjectSubgroup",
                        principalColumn: "SubgorupId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SubjectSubgroupClassSection",
                columns: table => new
                {
                    TableId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    SubgroupId = table.Column<int>(type: "int", nullable: false),
                    ClassId = table.Column<int>(type: "int", nullable: false),
                    SectionId = table.Column<int>(type: "int", nullable: true),
                    TeacherId = table.Column<int>(type: "int", nullable: false),
                    SessionId = table.Column<int>(type: "int", nullable: false),
                    MasterEventId = table.Column<int>(type: "int", nullable: false),
                    EventId = table.Column<int>(type: "int", nullable: false),
                    NoOfExam = table.Column<int>(type: "int", nullable: true),
                    BestCount = table.Column<int>(type: "int", nullable: true),
                    StartDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EndDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    Attribute1 = table.Column<string>(type: "text", nullable: true),
                    Attribute2 = table.Column<string>(type: "text", nullable: true),
                    Attribute3 = table.Column<string>(type: "text", nullable: true),
                    Attribute4 = table.Column<string>(type: "text", nullable: true),
                    Attribute5 = table.Column<string>(type: "text", nullable: true),
                    CreatedBy = table.Column<int>(type: "int", nullable: false),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    LastUpdateBy = table.Column<int>(type: "int", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubjectSubgroupClassSection", x => x.TableId);
                    table.ForeignKey(
                        name: "FK_SubjectSubgroupClassSection_Class_ClassId",
                        column: x => x.ClassId,
                        principalTable: "Class",
                        principalColumn: "ClassId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SubjectSubgroupClassSection_PlannerEvent_EventId",
                        column: x => x.EventId,
                        principalTable: "PlannerEvent",
                        principalColumn: "EventId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SubjectSubgroupClassSection_Section_SectionId",
                        column: x => x.SectionId,
                        principalTable: "Section",
                        principalColumn: "SectionId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SubjectSubgroupClassSection_SubjectSubgroup_SubgroupId",
                        column: x => x.SubgroupId,
                        principalTable: "SubjectSubgroup",
                        principalColumn: "SubgorupId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ClassRoomColumnRows",
                columns: table => new
                {
                    ClassRoomColumnRowId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    ClassRoomColumnId = table.Column<int>(type: "int", nullable: false),
                    RowSerial = table.Column<int>(type: "int", nullable: false),
                    NoofSeat = table.Column<int>(type: "int", nullable: false),
                    SeatSerial = table.Column<int>(type: "int", nullable: false),
                    Status = table.Column<int>(type: "int", nullable: false),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClassRoomColumnRows", x => x.ClassRoomColumnRowId);
                    table.ForeignKey(
                        name: "FK_ClassRoomColumnRows_ClassRoomColumns_ClassRoomColumnId",
                        column: x => x.ClassRoomColumnId,
                        principalTable: "ClassRoomColumns",
                        principalColumn: "ClassRoomColumnId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "StudentSubjectLines",
                columns: table => new
                {
                    StudentSubjectLineId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    StudentHeaderId = table.Column<int>(type: "int", nullable: false),
                    CompulsorySubjectId = table.Column<int>(type: "int", nullable: true),
                    OptionalSubjectId = table.Column<int>(type: "int", nullable: true),
                    StudentSubjectMasterId = table.Column<int>(type: "int", nullable: false),
                    StudentHeaderId1 = table.Column<int>(type: "int", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentSubjectLines", x => x.StudentSubjectLineId);
                    table.ForeignKey(
                        name: "FK_StudentSubjectLines_Student_StudentHeaderId1",
                        column: x => x.StudentHeaderId1,
                        principalTable: "Student",
                        principalColumn: "StudentHeaderId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_StudentSubjectLines_StudentSubjectMasters_StudentSubjectMast~",
                        column: x => x.StudentSubjectMasterId,
                        principalTable: "StudentSubjectMasters",
                        principalColumn: "StudentSubjectMasterId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SubCategoryYears",
                columns: table => new
                {
                    BillPeriodId = table.Column<int>(type: "int", nullable: false),
                    YearId = table.Column<int>(type: "int", nullable: false),
                    SubCategoryFeePeriodBillPeriodId = table.Column<int>(type: "int", nullable: true),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubCategoryYears", x => new { x.BillPeriodId, x.YearId });
                    table.ForeignKey(
                        name: "FK_SubCategoryYears_SubCategoryFeePeriods_SubCategoryFeePeriodB~",
                        column: x => x.SubCategoryFeePeriodBillPeriodId,
                        principalTable: "SubCategoryFeePeriods",
                        principalColumn: "BillPeriodId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SubCategoryYears_Years_YearId",
                        column: x => x.YearId,
                        principalTable: "Years",
                        principalColumn: "YearId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ClassTeacher",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    SessionId = table.Column<int>(type: "int", nullable: false),
                    ClassTeacherId = table.Column<int>(type: "int", nullable: false),
                    ClassSectionId = table.Column<int>(type: "int", nullable: false),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClassTeacher", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ClassTeacher_ClassSection_ClassSectionId",
                        column: x => x.ClassSectionId,
                        principalTable: "ClassSection",
                        principalColumn: "ClassSectionId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ExamSeatPlans",
                columns: table => new
                {
                    ExamSeatPlanId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    ClassRoomColumnRowId = table.Column<int>(type: "int", nullable: false),
                    RollNumber = table.Column<int>(type: "int", nullable: false),
                    DateWiseSubjectId = table.Column<int>(type: "int", nullable: false),
                    ExamScheduleLineId = table.Column<int>(type: "int", nullable: false),
                    RoomSetupHeaderId = table.Column<int>(type: "int", nullable: false),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExamSeatPlans", x => x.ExamSeatPlanId);
                    table.ForeignKey(
                        name: "FK_ExamSeatPlans_ClassRoomColumnRows_ClassRoomColumnRowId",
                        column: x => x.ClassRoomColumnRowId,
                        principalTable: "ClassRoomColumnRows",
                        principalColumn: "ClassRoomColumnRowId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ExamSeatPlans_ExamScheduleLines_ExamScheduleLineId",
                        column: x => x.ExamScheduleLineId,
                        principalTable: "ExamScheduleLines",
                        principalColumn: "ExamScheduleLineId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "StudentWiseSubjects",
                columns: table => new
                {
                    StudentWiseSubjectId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    SubjectId = table.Column<int>(type: "int", nullable: false),
                    IsSubjectAssigned = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    StudentSubjectLineId = table.Column<int>(type: "int", nullable: false),
                    Attribute1 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute2 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute3 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute4 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute5 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Attribute6 = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    CreatedBy = table.Column<long>(type: "bigint", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastUpdateBy = table.Column<long>(type: "bigint", nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveFrom = table.Column<DateTime>(type: "datetime", nullable: true),
                    EffectiveTo = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentWiseSubjects", x => x.StudentWiseSubjectId);
                    table.ForeignKey(
                        name: "FK_StudentWiseSubjects_StudentSubjectLines_StudentSubjectLineId",
                        column: x => x.StudentSubjectLineId,
                        principalTable: "StudentSubjectLines",
                        principalColumn: "StudentSubjectLineId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_StudentWiseSubjects_Subject_SubjectId",
                        column: x => x.SubjectId,
                        principalTable: "Subject",
                        principalColumn: "SubjectId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AlertHistory_SchoolAlertAlertId",
                table: "AlertHistory",
                column: "SchoolAlertAlertId");

            migrationBuilder.CreateIndex(
                name: "IX_AlternateSubject_SubjectId",
                table: "AlternateSubject",
                column: "SubjectId");

            migrationBuilder.CreateIndex(
                name: "IX_BillInfo_ClassId",
                table: "BillInfo",
                column: "ClassId");

            migrationBuilder.CreateIndex(
                name: "IX_BillInfo_SchoolId",
                table: "BillInfo",
                column: "SchoolId");

            migrationBuilder.CreateIndex(
                name: "IX_BillInfo_SessionId",
                table: "BillInfo",
                column: "SessionId");

            migrationBuilder.CreateIndex(
                name: "IX_Book_BookAuthorAuthorId",
                table: "Book",
                column: "BookAuthorAuthorId");

            migrationBuilder.CreateIndex(
                name: "IX_Book_BookCategoryId",
                table: "Book",
                column: "BookCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Book_BookShelfsShelfId",
                table: "Book",
                column: "BookShelfsShelfId");

            migrationBuilder.CreateIndex(
                name: "IX_ClassExamGrade_ClassId",
                table: "ClassExamGrade",
                column: "ClassId");

            migrationBuilder.CreateIndex(
                name: "IX_ClassExamGrade_ExamGradeGradeId",
                table: "ClassExamGrade",
                column: "ExamGradeGradeId");

            migrationBuilder.CreateIndex(
                name: "IX_ClassRoomColumnRows_ClassRoomColumnId",
                table: "ClassRoomColumnRows",
                column: "ClassRoomColumnId");

            migrationBuilder.CreateIndex(
                name: "IX_ClassRoomColumns_RoomSetupHeaderId1",
                table: "ClassRoomColumns",
                column: "RoomSetupHeaderId1");

            migrationBuilder.CreateIndex(
                name: "IX_ClassSection_ClassId",
                table: "ClassSection",
                column: "ClassId");

            migrationBuilder.CreateIndex(
                name: "IX_ClassSection_SectionId",
                table: "ClassSection",
                column: "SectionId");

            migrationBuilder.CreateIndex(
                name: "IX_ClassSection_Shift",
                table: "ClassSection",
                column: "Shift");

            migrationBuilder.CreateIndex(
                name: "IX_ClassSectionGroupTeachers_HrEmployeeEmpHeaderId",
                table: "ClassSectionGroupTeachers",
                column: "HrEmployeeEmpHeaderId");

            migrationBuilder.CreateIndex(
                name: "IX_ClassSectionRoutine_ClassRoomHeaderId1",
                table: "ClassSectionRoutine",
                column: "ClassRoomHeaderId1");

            migrationBuilder.CreateIndex(
                name: "IX_ClassSectionRoutine_SubjectId",
                table: "ClassSectionRoutine",
                column: "SubjectId");

            migrationBuilder.CreateIndex(
                name: "IX_ClassSectionRoutineTransacitons_ClassRoomHeaderId1",
                table: "ClassSectionRoutineTransacitons",
                column: "ClassRoomHeaderId1");

            migrationBuilder.CreateIndex(
                name: "IX_ClassSectionRoutineTransacitons_SubjectId",
                table: "ClassSectionRoutineTransacitons",
                column: "SubjectId");

            migrationBuilder.CreateIndex(
                name: "IX_ClassSubject_ClassId",
                table: "ClassSubject",
                column: "ClassId");

            migrationBuilder.CreateIndex(
                name: "IX_ClassSubject_SubjectId",
                table: "ClassSubject",
                column: "SubjectId");

            migrationBuilder.CreateIndex(
                name: "IX_ClassTeacher_ClassSectionId",
                table: "ClassTeacher",
                column: "ClassSectionId");

            migrationBuilder.CreateIndex(
                name: "IX_DateWiseInvestigators_ExamRoomInvestigatorId",
                table: "DateWiseInvestigators",
                column: "ExamRoomInvestigatorId");

            migrationBuilder.CreateIndex(
                name: "IX_DateWiseSubjects_ExamScheduleLineId",
                table: "DateWiseSubjects",
                column: "ExamScheduleLineId");

            migrationBuilder.CreateIndex(
                name: "IX_EmployeeOfficeTimes_HrEmployeeEmpHeaderId",
                table: "EmployeeOfficeTimes",
                column: "HrEmployeeEmpHeaderId");

            migrationBuilder.CreateIndex(
                name: "IX_EmployeeStatusLogs_EmployeeEmpHeaderId",
                table: "EmployeeStatusLogs",
                column: "EmployeeEmpHeaderId");

            migrationBuilder.CreateIndex(
                name: "IX_EmployeeWeekends_EmployeeEmpHeaderId",
                table: "EmployeeWeekends",
                column: "EmployeeEmpHeaderId");

            migrationBuilder.CreateIndex(
                name: "IX_EventRoutine_ClassId",
                table: "EventRoutine",
                column: "ClassId");

            migrationBuilder.CreateIndex(
                name: "IX_EventRoutine_SubjectId",
                table: "EventRoutine",
                column: "SubjectId");

            migrationBuilder.CreateIndex(
                name: "IX_ExamScheduleLines_ExamScheduleId",
                table: "ExamScheduleLines",
                column: "ExamScheduleId");

            migrationBuilder.CreateIndex(
                name: "IX_ExamSeatPlans_ClassRoomColumnRowId",
                table: "ExamSeatPlans",
                column: "ClassRoomColumnRowId");

            migrationBuilder.CreateIndex(
                name: "IX_ExamSeatPlans_ExamScheduleLineId",
                table: "ExamSeatPlans",
                column: "ExamScheduleLineId");

            migrationBuilder.CreateIndex(
                name: "IX_FeeSubCategory_FeeCategoryId",
                table: "FeeSubCategory",
                column: "FeeCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_FeeSubCategoryFine_FeeSubCategoryId",
                table: "FeeSubCategoryFine",
                column: "FeeSubCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_FeeSubCategoryWaiver_FeeSubCategoryId",
                table: "FeeSubCategoryWaiver",
                column: "FeeSubCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_FndFlexValue_FndFlexValueSetsFlexValueSetId",
                table: "FndFlexValue",
                column: "FndFlexValueSetsFlexValueSetId");

            migrationBuilder.CreateIndex(
                name: "IX_FndMenu_FndmenuMenuId",
                table: "FndMenu",
                column: "FndmenuMenuId");

            migrationBuilder.CreateIndex(
                name: "IX_GeneralInfo_GeneralInfoTypeInfoTypeId",
                table: "GeneralInfo",
                column: "GeneralInfoTypeInfoTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_GeneralInfo_SchoolId",
                table: "GeneralInfo",
                column: "SchoolId");

            migrationBuilder.CreateIndex(
                name: "IX_HrEmpEduQuali_HrEmployeeEmpHeaderId",
                table: "HrEmpEduQuali",
                column: "HrEmployeeEmpHeaderId");

            migrationBuilder.CreateIndex(
                name: "IX_HrEmpExperience_HrEmployeeEmpHeaderId",
                table: "HrEmpExperience",
                column: "HrEmployeeEmpHeaderId");

            migrationBuilder.CreateIndex(
                name: "IX_HrEmployeeRefrence_HrEmployeeEmpHeaderId",
                table: "HrEmployeeRefrence",
                column: "HrEmployeeEmpHeaderId");

            migrationBuilder.CreateIndex(
                name: "IX_HrEmployeeShift_HrEmployeeEmpHeaderId",
                table: "HrEmployeeShift",
                column: "HrEmployeeEmpHeaderId");

            migrationBuilder.CreateIndex(
                name: "IX_HrEmployeeShift_HrShiftShiftId",
                table: "HrEmployeeShift",
                column: "HrShiftShiftId");

            migrationBuilder.CreateIndex(
                name: "IX_HrEmployeeTransaction_HrEmployeeEmpHeaderId",
                table: "HrEmployeeTransaction",
                column: "HrEmployeeEmpHeaderId");

            migrationBuilder.CreateIndex(
                name: "IX_HrExcepShift_HrEmployeeEmpHeaderId",
                table: "HrExcepShift",
                column: "HrEmployeeEmpHeaderId");

            migrationBuilder.CreateIndex(
                name: "IX_HrPaySlipPayLogs_HrPaySlipTransactionMasterPaySlipMasterHead~",
                table: "HrPaySlipPayLogs",
                column: "HrPaySlipTransactionMasterPaySlipMasterHeaderId");

            migrationBuilder.CreateIndex(
                name: "IX_HrPaySlipTransactionLogAfterPayheadChanges_HrPaySlipTransact~",
                table: "HrPaySlipTransactionLogAfterPayheadChanges",
                column: "HrPaySlipTransactionMasterLogId");

            migrationBuilder.CreateIndex(
                name: "IX_HrPaySlipTransactionLogHistorys_HrPaySlipTransactionMasterLo~",
                table: "HrPaySlipTransactionLogHistorys",
                column: "HrPaySlipTransactionMasterLogHistoryHrPaySlipTransactionMasterL~");

            migrationBuilder.CreateIndex(
                name: "IX_HrPaySlipTransactionLogs_HrPaySlipTransactionMasterLogId",
                table: "HrPaySlipTransactionLogs",
                column: "HrPaySlipTransactionMasterLogId");

            migrationBuilder.CreateIndex(
                name: "IX_HrPaySlipTransactionMasterLogHistorys_HrPaySlipTransactionMa~",
                table: "HrPaySlipTransactionMasterLogHistorys",
                column: "HrPaySlipTransactionMasterPaySlipMasterHeaderId");

            migrationBuilder.CreateIndex(
                name: "IX_HrPaySlipTransactionMasterLogs_HrPaySlipTransactionMasterPay~",
                table: "HrPaySlipTransactionMasterLogs",
                column: "HrPaySlipTransactionMasterPaySlipMasterHeaderId");

            migrationBuilder.CreateIndex(
                name: "IX_HrPaySlipTransactions_HrPaySlipTransactionMasterPaySlipMaste~",
                table: "HrPaySlipTransactions",
                column: "HrPaySlipTransactionMasterPaySlipMasterHeaderId");

            migrationBuilder.CreateIndex(
                name: "IX_IssuedBooks_BookId",
                table: "IssuedBooks",
                column: "BookId");

            migrationBuilder.CreateIndex(
                name: "IX_JobApply_JobApplicantId",
                table: "JobApply",
                column: "JobApplicantId");

            migrationBuilder.CreateIndex(
                name: "IX_JobApply_JobId",
                table: "JobApply",
                column: "JobId");

            migrationBuilder.CreateIndex(
                name: "IX_LeaveApplicationFile_HrEmployeeLeaveApplicationApplicationId",
                table: "LeaveApplicationFile",
                column: "HrEmployeeLeaveApplicationApplicationId");

            migrationBuilder.CreateIndex(
                name: "IX_Logo_LogoTypeId",
                table: "Logo",
                column: "LogoTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Logo_SchoolId",
                table: "Logo",
                column: "SchoolId");

            migrationBuilder.CreateIndex(
                name: "IX_MailGroup_HrEmployeeEmpHeaderId",
                table: "MailGroup",
                column: "HrEmployeeEmpHeaderId");

            migrationBuilder.CreateIndex(
                name: "IX_OfficialResignRules_ResignationId",
                table: "OfficialResignRules",
                column: "ResignationId");

            migrationBuilder.CreateIndex(
                name: "IX_PhotoGallery_GalleryId",
                table: "PhotoGallery",
                column: "GalleryId");

            migrationBuilder.CreateIndex(
                name: "IX_PlannerEvent_PlannerEventTypeEventTypeId",
                table: "PlannerEvent",
                column: "PlannerEventTypeEventTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_PlannerEventDetails_PlannerEventEventId",
                table: "PlannerEventDetails",
                column: "PlannerEventEventId");

            migrationBuilder.CreateIndex(
                name: "IX_SchoolLinks_SchoolId",
                table: "SchoolLinks",
                column: "SchoolId");

            migrationBuilder.CreateIndex(
                name: "IX_SchoolLinks_SocialTypeId",
                table: "SchoolLinks",
                column: "SocialTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_StudentBillLine_BillDtlId",
                table: "StudentBillLine",
                column: "BillDtlId");

            migrationBuilder.CreateIndex(
                name: "IX_StudentBillLineTemporaries_BillDtlId",
                table: "StudentBillLineTemporaries",
                column: "BillDtlId");

            migrationBuilder.CreateIndex(
                name: "IX_StudentMarks_ClassId",
                table: "StudentMarks",
                column: "ClassId");

            migrationBuilder.CreateIndex(
                name: "IX_StudentMarks_EventId",
                table: "StudentMarks",
                column: "EventId");

            migrationBuilder.CreateIndex(
                name: "IX_StudentMarks_SectionId",
                table: "StudentMarks",
                column: "SectionId");

            migrationBuilder.CreateIndex(
                name: "IX_StudentMarks_SubjectId",
                table: "StudentMarks",
                column: "SubjectId");

            migrationBuilder.CreateIndex(
                name: "IX_StudentPosition_StudentPositionLogPositionLogId",
                table: "StudentPosition",
                column: "StudentPositionLogPositionLogId");

            migrationBuilder.CreateIndex(
                name: "IX_StudentSubjectLines_StudentHeaderId1",
                table: "StudentSubjectLines",
                column: "StudentHeaderId1");

            migrationBuilder.CreateIndex(
                name: "IX_StudentSubjectLines_StudentSubjectMasterId",
                table: "StudentSubjectLines",
                column: "StudentSubjectMasterId");

            migrationBuilder.CreateIndex(
                name: "IX_StudentSubjectMasters_ClassId",
                table: "StudentSubjectMasters",
                column: "ClassId");

            migrationBuilder.CreateIndex(
                name: "IX_StudentSubjectMasters_SectionId",
                table: "StudentSubjectMasters",
                column: "SectionId");

            migrationBuilder.CreateIndex(
                name: "IX_StudentSubjectMasters_SessionId",
                table: "StudentSubjectMasters",
                column: "SessionId");

            migrationBuilder.CreateIndex(
                name: "IX_StudentTransaction_StudentsStudentHeaderId",
                table: "StudentTransaction",
                column: "StudentsStudentHeaderId");

            migrationBuilder.CreateIndex(
                name: "IX_StudentWiseSubjects_StudentSubjectLineId",
                table: "StudentWiseSubjects",
                column: "StudentSubjectLineId");

            migrationBuilder.CreateIndex(
                name: "IX_StudentWiseSubjects_SubjectId",
                table: "StudentWiseSubjects",
                column: "SubjectId");

            migrationBuilder.CreateIndex(
                name: "IX_SubCategoryFeePeriods_FeeSubCategoryId",
                table: "SubCategoryFeePeriods",
                column: "FeeSubCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_SubCategoryFeeType_FeeSubCategoryId",
                table: "SubCategoryFeeType",
                column: "FeeSubCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_SubCategoryYears_SubCategoryFeePeriodBillPeriodId",
                table: "SubCategoryYears",
                column: "SubCategoryFeePeriodBillPeriodId");

            migrationBuilder.CreateIndex(
                name: "IX_SubCategoryYears_YearId",
                table: "SubCategoryYears",
                column: "YearId");

            migrationBuilder.CreateIndex(
                name: "IX_SubjectSubgroupClass_EventId",
                table: "SubjectSubgroupClass",
                column: "EventId");

            migrationBuilder.CreateIndex(
                name: "IX_SubjectSubgroupClass_SubgroupId",
                table: "SubjectSubgroupClass",
                column: "SubgroupId");

            migrationBuilder.CreateIndex(
                name: "IX_SubjectSubgroupClassSection_ClassId",
                table: "SubjectSubgroupClassSection",
                column: "ClassId");

            migrationBuilder.CreateIndex(
                name: "IX_SubjectSubgroupClassSection_EventId",
                table: "SubjectSubgroupClassSection",
                column: "EventId");

            migrationBuilder.CreateIndex(
                name: "IX_SubjectSubgroupClassSection_SectionId",
                table: "SubjectSubgroupClassSection",
                column: "SectionId");

            migrationBuilder.CreateIndex(
                name: "IX_SubjectSubgroupClassSection_SubgroupId",
                table: "SubjectSubgroupClassSection",
                column: "SubgroupId");

            migrationBuilder.CreateIndex(
                name: "IX_UserLoginTracer_UserId",
                table: "UserLoginTracer",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_UserRoles_RoleId",
                table: "UserRoles",
                column: "RoleId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AlertHistory");

            migrationBuilder.DropTable(
                name: "AlternateSubject");

            migrationBuilder.DropTable(
                name: "Attendance");

            migrationBuilder.DropTable(
                name: "AttendanceChangeRequists");

            migrationBuilder.DropTable(
                name: "AttendanceEditHistories");

            migrationBuilder.DropTable(
                name: "AttendanceEmployee");

            migrationBuilder.DropTable(
                name: "AttendanceGrade");

            migrationBuilder.DropTable(
                name: "AttendancePolicy");

            migrationBuilder.DropTable(
                name: "AttendanceProcesses");

            migrationBuilder.DropTable(
                name: "AttendanceStudent");

            migrationBuilder.DropTable(
                name: "AttendenceInfo");

            migrationBuilder.DropTable(
                name: "Banner");

            migrationBuilder.DropTable(
                name: "BillInfo");

            migrationBuilder.DropTable(
                name: "BillPeriod");

            migrationBuilder.DropTable(
                name: "Calender");

            migrationBuilder.DropTable(
                name: "Campus");

            migrationBuilder.DropTable(
                name: "Career");

            migrationBuilder.DropTable(
                name: "ClassBook");

            migrationBuilder.DropTable(
                name: "ClassCourseMaster");

            migrationBuilder.DropTable(
                name: "ClassCourseSubject");

            migrationBuilder.DropTable(
                name: "ClassExamGrade");

            migrationBuilder.DropTable(
                name: "ClassRoutine");

            migrationBuilder.DropTable(
                name: "ClassSectionGroupTeachers");

            migrationBuilder.DropTable(
                name: "ClassSectionRoutine");

            migrationBuilder.DropTable(
                name: "ClassSectionRoutineTransacitons");

            migrationBuilder.DropTable(
                name: "ClassSubject");

            migrationBuilder.DropTable(
                name: "ClassSubjectWiseExamType");

            migrationBuilder.DropTable(
                name: "ClassTeacher");

            migrationBuilder.DropTable(
                name: "Compensations");

            migrationBuilder.DropTable(
                name: "DateWiseInvestigators");

            migrationBuilder.DropTable(
                name: "DateWiseSubjects");

            migrationBuilder.DropTable(
                name: "Devices");

            migrationBuilder.DropTable(
                name: "EmergencyMedicalAction");

            migrationBuilder.DropTable(
                name: "EmployeeLeaveInfo");

            migrationBuilder.DropTable(
                name: "EmployeeManager");

            migrationBuilder.DropTable(
                name: "EmployeeOfficeTimes");

            migrationBuilder.DropTable(
                name: "EmployeeStatusLogs");

            migrationBuilder.DropTable(
                name: "EmployeeWeekends");

            migrationBuilder.DropTable(
                name: "EventAndHolidayPlanners");

            migrationBuilder.DropTable(
                name: "EventRoutine");

            migrationBuilder.DropTable(
                name: "Exam");

            migrationBuilder.DropTable(
                name: "ExamPublishInfos");

            migrationBuilder.DropTable(
                name: "ExamSeatPlans");

            migrationBuilder.DropTable(
                name: "ExamType");

            migrationBuilder.DropTable(
                name: "FeeSubCategoryFine");

            migrationBuilder.DropTable(
                name: "FeeSubCategoryWaiver");

            migrationBuilder.DropTable(
                name: "FndMenu");

            migrationBuilder.DropTable(
                name: "FndMenuGroups");

            migrationBuilder.DropTable(
                name: "FndMenuPage");

            migrationBuilder.DropTable(
                name: "FndModule");

            migrationBuilder.DropTable(
                name: "FndPage");

            migrationBuilder.DropTable(
                name: "FndPagePics");

            migrationBuilder.DropTable(
                name: "FndPeriods");

            migrationBuilder.DropTable(
                name: "FndResponceMenu");

            migrationBuilder.DropTable(
                name: "FndResponsibility");

            migrationBuilder.DropTable(
                name: "FndUserPage");

            migrationBuilder.DropTable(
                name: "FndUserResponce");

            migrationBuilder.DropTable(
                name: "FndUsers");

            migrationBuilder.DropTable(
                name: "GeneralInfo");

            migrationBuilder.DropTable(
                name: "HelpfulResouce");

            migrationBuilder.DropTable(
                name: "HrAttendenceFix");

            migrationBuilder.DropTable(
                name: "HrBillPeriodWisePayRollIssueDates");

            migrationBuilder.DropTable(
                name: "HrDepartment");

            migrationBuilder.DropTable(
                name: "HrDesigGrade");

            migrationBuilder.DropTable(
                name: "HrEmpEduQuali");

            migrationBuilder.DropTable(
                name: "HrEmpExperience");

            migrationBuilder.DropTable(
                name: "HrEmployeeHierarcy");

            migrationBuilder.DropTable(
                name: "HrEmployeeLeaveCounts");

            migrationBuilder.DropTable(
                name: "HrEmployeePayHeads");

            migrationBuilder.DropTable(
                name: "HrEmployeeRefrence");

            migrationBuilder.DropTable(
                name: "HrEmployeeShift");

            migrationBuilder.DropTable(
                name: "HrEmployeeTiming");

            migrationBuilder.DropTable(
                name: "HrEmployeeTransaction");

            migrationBuilder.DropTable(
                name: "HrExcepShift");

            migrationBuilder.DropTable(
                name: "HrLeaveInfo");

            migrationBuilder.DropTable(
                name: "HrPayHeads");

            migrationBuilder.DropTable(
                name: "HrPayRollPayHeads");

            migrationBuilder.DropTable(
                name: "HrPayRolls");

            migrationBuilder.DropTable(
                name: "HrPaySlipPayLogs");

            migrationBuilder.DropTable(
                name: "HrPaySlipTransactionLogAfterPayheadChanges");

            migrationBuilder.DropTable(
                name: "HrPaySlipTransactionLogHistorys");

            migrationBuilder.DropTable(
                name: "HrPaySlipTransactionLogs");

            migrationBuilder.DropTable(
                name: "HrPaySlipTransactions");

            migrationBuilder.DropTable(
                name: "HrPositionLeaveCategories");

            migrationBuilder.DropTable(
                name: "HrPositions");

            migrationBuilder.DropTable(
                name: "HrPositionStructure");

            migrationBuilder.DropTable(
                name: "IssuedBooks");

            migrationBuilder.DropTable(
                name: "JobApply");

            migrationBuilder.DropTable(
                name: "Leave");

            migrationBuilder.DropTable(
                name: "LeaveApplicationFile");

            migrationBuilder.DropTable(
                name: "LeaveMaster");

            migrationBuilder.DropTable(
                name: "LeaveSetup");

            migrationBuilder.DropTable(
                name: "Logo");

            migrationBuilder.DropTable(
                name: "MailGroup");

            migrationBuilder.DropTable(
                name: "MarksheetStatus");

            migrationBuilder.DropTable(
                name: "MedicalHistory");

            migrationBuilder.DropTable(
                name: "MenuGroup");

            migrationBuilder.DropTable(
                name: "MessageAlertMaster");

            migrationBuilder.DropTable(
                name: "News");

            migrationBuilder.DropTable(
                name: "Notice");

            migrationBuilder.DropTable(
                name: "NotifyInfos");

            migrationBuilder.DropTable(
                name: "NotifyTransactionInfos");

            migrationBuilder.DropTable(
                name: "OfficialResignRules");

            migrationBuilder.DropTable(
                name: "OnlineInquiry");

            migrationBuilder.DropTable(
                name: "PaymentGatewaySettings");

            migrationBuilder.DropTable(
                name: "PhotoGallery");

            migrationBuilder.DropTable(
                name: "PlannerDay");

            migrationBuilder.DropTable(
                name: "PlannerEventClass");

            migrationBuilder.DropTable(
                name: "PlannerEventDetails");

            migrationBuilder.DropTable(
                name: "PlannerHoliday");

            migrationBuilder.DropTable(
                name: "PositionChangeRequests");

            migrationBuilder.DropTable(
                name: "ProcessAssessmentMarks");

            migrationBuilder.DropTable(
                name: "ProcessedMarksheets");

            migrationBuilder.DropTable(
                name: "ProcessLog");

            migrationBuilder.DropTable(
                name: "ProcessQueryTableMailGroup");

            migrationBuilder.DropTable(
                name: "ProcessSchedule");

            migrationBuilder.DropTable(
                name: "Publisher");

            migrationBuilder.DropTable(
                name: "RecipientGroup");

            migrationBuilder.DropTable(
                name: "Rejoins");

            migrationBuilder.DropTable(
                name: "RemarksGrade");

            migrationBuilder.DropTable(
                name: "ReportProcess");

            migrationBuilder.DropTable(
                name: "Routine");

            migrationBuilder.DropTable(
                name: "RoutineSetup");

            migrationBuilder.DropTable(
                name: "Schedule");

            migrationBuilder.DropTable(
                name: "SchoolBranch");

            migrationBuilder.DropTable(
                name: "SchoolCalender");

            migrationBuilder.DropTable(
                name: "SchoolLinks");

            migrationBuilder.DropTable(
                name: "SchoolTask");

            migrationBuilder.DropTable(
                name: "SmsBuyingLogs");

            migrationBuilder.DropTable(
                name: "SmsHistory");

            migrationBuilder.DropTable(
                name: "SMSTemplate");

            migrationBuilder.DropTable(
                name: "StudentAdmission");

            migrationBuilder.DropTable(
                name: "StudentAdmissionMark");

            migrationBuilder.DropTable(
                name: "StudentAdmissionTest");

            migrationBuilder.DropTable(
                name: "StudentAdmitCard");

            migrationBuilder.DropTable(
                name: "StudentBillLine");

            migrationBuilder.DropTable(
                name: "StudentBillLineTemporaries");

            migrationBuilder.DropTable(
                name: "StudentCategories");

            migrationBuilder.DropTable(
                name: "StudentCategoryLogs");

            migrationBuilder.DropTable(
                name: "StudentCurriculumResult");

            migrationBuilder.DropTable(
                name: "StudentHistory");

            migrationBuilder.DropTable(
                name: "StudentLeaveApplications");

            migrationBuilder.DropTable(
                name: "StudentLeaveCounts");

            migrationBuilder.DropTable(
                name: "StudentMarks");

            migrationBuilder.DropTable(
                name: "StudentMedical");

            migrationBuilder.DropTable(
                name: "StudentOtherInfo");

            migrationBuilder.DropTable(
                name: "StudentPosition");

            migrationBuilder.DropTable(
                name: "StudentProx");

            migrationBuilder.DropTable(
                name: "StudentRagistration");

            migrationBuilder.DropTable(
                name: "StudentRemarks");

            migrationBuilder.DropTable(
                name: "StudentSiblings");

            migrationBuilder.DropTable(
                name: "StudentSubject");

            migrationBuilder.DropTable(
                name: "StudentTransaction");

            migrationBuilder.DropTable(
                name: "StudentWiseSubjects");

            migrationBuilder.DropTable(
                name: "StuDocument");

            migrationBuilder.DropTable(
                name: "SubCategoryFeeType");

            migrationBuilder.DropTable(
                name: "SubCategoryYears");

            migrationBuilder.DropTable(
                name: "SubjectSubgroupClass");

            migrationBuilder.DropTable(
                name: "SubjectSubgroupClassSection");

            migrationBuilder.DropTable(
                name: "SubTeacher");

            migrationBuilder.DropTable(
                name: "TeacherLeaveCount");

            migrationBuilder.DropTable(
                name: "TempLeave");

            migrationBuilder.DropTable(
                name: "UserLoginTracer");

            migrationBuilder.DropTable(
                name: "UserRoles");

            migrationBuilder.DropTable(
                name: "VoteMaster");

            migrationBuilder.DropTable(
                name: "VoteMember");

            migrationBuilder.DropTable(
                name: "VoteOption");

            migrationBuilder.DropTable(
                name: "ScoolAlert");

            migrationBuilder.DropTable(
                name: "ExamGrade");

            migrationBuilder.DropTable(
                name: "ClassRooms");

            migrationBuilder.DropTable(
                name: "ClassSection");

            migrationBuilder.DropTable(
                name: "ExamRoomInvestigators");

            migrationBuilder.DropTable(
                name: "ClassRoomColumnRows");

            migrationBuilder.DropTable(
                name: "ExamScheduleLines");

            migrationBuilder.DropTable(
                name: "GeneralInfoType");

            migrationBuilder.DropTable(
                name: "HrShift");

            migrationBuilder.DropTable(
                name: "HrPaySlipTransactionMasterLogHistorys");

            migrationBuilder.DropTable(
                name: "HrPaySlipTransactionMasterLogs");

            migrationBuilder.DropTable(
                name: "Book");

            migrationBuilder.DropTable(
                name: "Job");

            migrationBuilder.DropTable(
                name: "JobApplicant");

            migrationBuilder.DropTable(
                name: "HrEmployeeLeaveApplication");

            migrationBuilder.DropTable(
                name: "LogoType");

            migrationBuilder.DropTable(
                name: "HrEmployee");

            migrationBuilder.DropTable(
                name: "Resignations");

            migrationBuilder.DropTable(
                name: "Gallery");

            migrationBuilder.DropTable(
                name: "School");

            migrationBuilder.DropTable(
                name: "SchoolType");

            migrationBuilder.DropTable(
                name: "StudentBillMaster");

            migrationBuilder.DropTable(
                name: "StudentBillMasterTemporaries");

            migrationBuilder.DropTable(
                name: "StudentPositionLog");

            migrationBuilder.DropTable(
                name: "StudentSubjectLines");

            migrationBuilder.DropTable(
                name: "Subject");

            migrationBuilder.DropTable(
                name: "SubCategoryFeePeriods");

            migrationBuilder.DropTable(
                name: "Years");

            migrationBuilder.DropTable(
                name: "PlannerEvent");

            migrationBuilder.DropTable(
                name: "SubjectSubgroup");

            migrationBuilder.DropTable(
                name: "Roles");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "FndFlexValue");

            migrationBuilder.DropTable(
                name: "ClassRoomColumns");

            migrationBuilder.DropTable(
                name: "ExamSchedules");

            migrationBuilder.DropTable(
                name: "HrPaySlipTransactionMasters");

            migrationBuilder.DropTable(
                name: "BookAuthors");

            migrationBuilder.DropTable(
                name: "BookCategories");

            migrationBuilder.DropTable(
                name: "BookShelfs");

            migrationBuilder.DropTable(
                name: "Student");

            migrationBuilder.DropTable(
                name: "StudentSubjectMasters");

            migrationBuilder.DropTable(
                name: "FeeSubCategory");

            migrationBuilder.DropTable(
                name: "PlannerEventType");

            migrationBuilder.DropTable(
                name: "FndFlexValueSet");

            migrationBuilder.DropTable(
                name: "RoomSetups");

            migrationBuilder.DropTable(
                name: "Class");

            migrationBuilder.DropTable(
                name: "Section");

            migrationBuilder.DropTable(
                name: "Session");

            migrationBuilder.DropTable(
                name: "FeeCategory");
        }
    }
}
