﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model.Workflow
{
    public class WorkFlowRole : BaseEntity
    {
        [Key]
        public int WfRoleId { get; set; }
        public string WfRoleName { get; set; }
    }
}
