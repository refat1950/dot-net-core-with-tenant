using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entity.Model
{
    public partial class ClassSubjectTeacher : BaseEntity
    {
        [Key, Column(Order = 0)]
        public int TableId { get; set; }

        public int? ClassId { get; set; }

        public int? SectionId { get; set; }

        public int? Shift { get; set; }
        public int? SubjectId { get; set; }

        public int? TeacherId { get; set; }

        [StringLength(1)]
        public string Major { get; set; }

        public int? SessionId { get; set; }

        public int? ParentGroupId { get; set; }
        public int? SubGroupId { get; set; }
        public int? ResultSerial { get; set; }

        public virtual Subject Subject { get; set; }
        public virtual Class Class { get; set; }
        public virtual Section Section { get; set; }

    }
}
