﻿using Entity.Model;
using Entity.Model.Alert;
using Entity.Model.Process;
using Entity.Model.Transaction;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Tenant.Provider;

namespace Repository.Context
{
    public class ProjectDbContext : DbContext
    {

        private readonly ITenantProvider tenantIdentifier;
        private readonly IConfiguration configuration;

        //public ProjectDbContext(DbContextOptions<ProjectDbContext> options) : base(options) // only during database update uncomment this 
        //{

        //}

        public ProjectDbContext(IConfiguration configuration, ITenantProvider tenantIdentifier) // only during database update comment this 
        {
            this.configuration = configuration;
            this.tenantIdentifier = tenantIdentifier;
        }

        //public ProjectDbContext() : base(GetOptions("name = schoolconnectionstring"))
        //{

        //}
        //public ProjectDbContext(string connectionStringName) : base(GetOptions(connectionStringName))
        //{
        //}

        //public ProjectDbContext(ITenantProvider tenantProvider) : base(GetOptions(tenantProvider.GetConnectionString()))
        //{

        //}

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // define composite primary key
            modelBuilder.Entity<UserRole>().HasKey(vf => new { vf.UserId, vf.RoleId });
            modelBuilder.Entity<SchoolBranch>().HasKey(vf => new { vf.SchoolId, vf.BranchId });
            modelBuilder.Entity<AttendanceEmployee>().HasKey(vf => new { vf.EmpHeaderId, vf.PunchDate });
            modelBuilder.Entity<ClassBook>().HasKey(vf => new { vf.BookId, vf.ClassId });
            modelBuilder.Entity<ClassCourseSubject>().HasKey(vf => new { vf.CourseMasterId, vf.SubjectId });
            modelBuilder.Entity<EmployeeManager>().HasKey(vf => new { vf.EmpHeaderId, vf.ManagerId });
            modelBuilder.Entity<FndMenuPage>().HasKey(vf => new { vf.MenuId, vf.PageId });
            modelBuilder.Entity<FndResponceMenu>().HasKey(vf => new { vf.ResponId, vf.MenuId });
            modelBuilder.Entity<FndUserPage>().HasKey(vf => new { vf.UserId, vf.PageId });
            modelBuilder.Entity<FndUserResponce>().HasKey(vf => new { vf.UserId, vf.ResponId });
            modelBuilder.Entity<HrBillPeriodWisePayRollIssueDate>().HasKey(vf => new { vf.PayRollId, vf.PeriodId });
            modelBuilder.Entity<HrEmpEduQuali>().HasKey(vf => new { vf.EmpHeaderId, vf.QualificationId });
            modelBuilder.Entity<HrEmpExperience>().HasKey(vf => new { vf.EmpHeaderId, vf.ExperienceId });
            modelBuilder.Entity<HrEmployeeRefrence>().HasKey(vf => new { vf.EmpHeaderId, vf.ReferenceId });
            modelBuilder.Entity<HrEmployeeShift>().HasKey(vf => new { vf.EmpHeaderId, vf.ShiftId });
            modelBuilder.Entity<HrEmployeeTiming>().HasKey(vf => new { vf.EmpHeaderId, vf.WorkingDay, vf.StartTime, vf.EndTime });
            modelBuilder.Entity<HrExcepShift>().HasKey(vf => new { vf.EmpHeaderId, vf.ShiftDate });
            modelBuilder.Entity<News>().HasKey(vf => new { vf.Id, vf.NewsTitle, vf.Description });
            modelBuilder.Entity<ProcessSchedule>().HasKey(vf => new { vf.ScheduleId, vf.ProcessId });
            modelBuilder.Entity<RoutineSetup>().HasKey(vf => new { vf.RoutineSetupId, vf.ClassId , vf.SectionId });
            modelBuilder.Entity<SmsHistory>().HasKey(vf => new { vf.SmsId, vf.SentTo });
            modelBuilder.Entity<StudentAdmissionMark>().HasKey(vf => new { vf.AdmissionId, vf.SubjectId });
            modelBuilder.Entity<StudentCategory>().HasKey(vf => new { vf.StudentHeaderId, vf.StudentCategoryId });
            modelBuilder.Entity<StudentHistory>().HasKey(vf => new { vf.StudentHeaderId, vf.BkupSessionId });
            modelBuilder.Entity<SubCategoryYear>().HasKey(vf => new { vf.BillPeriodId, vf.YearId });
            modelBuilder.Entity<SubTeacher>().HasKey(vf => new { vf.TeacherId, vf.SubjectId });
            modelBuilder.Entity<VoteMember>().HasKey(vf => new { vf.VotePersonId, vf.ReferenceId , vf.VoteId });
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (configuration != null && tenantIdentifier != null)
            {
                optionsBuilder.UseMySQL(tenantIdentifier.GetConnectionString());
                base.OnConfiguring(optionsBuilder);
            }
            else
                base.OnConfiguring(optionsBuilder);
        }
        private static DbContextOptions GetOptions(string connectionString)
        {
            return MySQLDbContextOptionsExtensions.UseMySQL(new DbContextOptionsBuilder(), connectionString).Options;
        }

        #region "User & Role Related Tables"
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<UserRole> UserRoles { get; set; }
        #endregion

        public virtual DbSet<ScoolAlert> ScoolAlert { get; set; }
        public virtual DbSet<AlertHistory> AlertHistory { get; set; }
        public virtual DbSet<AttendanceStudent> AttendanceStudent { get; set; }
        public virtual DbSet<Banner> Banner { get; set; }
        public virtual DbSet<BillInfo> BillInfo { get; set; }
        public virtual DbSet<Book> Book { get; set; }
        public virtual DbSet<Calender> Calender { get; set; }

        #region "HRMnPayroll DbContext"
        public virtual DbSet<Attendance> Attendance { get; set; }
        public virtual DbSet<AttendanceProcess> AttendanceProcesses { get; set; }
        public virtual DbSet<AttendanceEditHistory> AttendanceEditHistories { get; set; }
        public virtual DbSet<HrPayHead> HrPayHeads { get; set; }
        public virtual DbSet<HrPayRoll> HrPayRolls { get; set; }
        public virtual DbSet<HrPayRollPayHead> HrPayRollPayHeads { get; set; }
        public virtual DbSet<HrEmployeePayHead> HrEmployeePayHeads { get; set; }
        public virtual DbSet<HrBillPeriodWisePayRollIssueDate> HrBillPeriodWisePayRollIssueDates { get; set; }
        public virtual DbSet<HrPosition> HrPositions { get; set; }
        public virtual DbSet<HrPaySlipTransactionMaster> HrPaySlipTransactionMasters { get; set; }
        public virtual DbSet<HrPaySlipTransactionMasterLog> HrPaySlipTransactionMasterLogs { get; set; }
        public virtual DbSet<HrPaySlipTransactionMasterLogHistory> HrPaySlipTransactionMasterLogHistorys { get; set; }
        public virtual DbSet<HrPaySlipTransaction> HrPaySlipTransactions { get; set; }
        public virtual DbSet<HrPaySlipTransactionLog> HrPaySlipTransactionLogs { get; set; }
        public virtual DbSet<HrPaySlipTransactionLogHistory> HrPaySlipTransactionLogHistorys { get; set; }
        public virtual DbSet<PositionChangeRequest> PositionChangeRequests { get; set; }
        public virtual DbSet<HrPaySlipPayLog> HrPaySlipPayLogs { get; set; }
        public virtual DbSet<HrPaySlipTransactionLogAfterPayheadChange> HrPaySlipTransactionLogAfterPayheadChanges { get; set; }
        public virtual DbSet<Compensation> Compensations { get; set; }
        public virtual DbSet<EmployeeOfficeTime> EmployeeOfficeTimes { get; set; }
        public virtual DbSet<EventAndHolidayPlanner> EventAndHolidayPlanners { get; set; }
        public virtual DbSet<HrPositionLeaveCategory> HrPositionLeaveCategories { get; set; }
        public virtual DbSet<HrEmployeeLeaveApplication> HrEmployeeLeaveApplication { get; set; }
        public virtual DbSet<HrEmployeeLeaveCount> HrEmployeeLeaveCounts { get; set; }
        public virtual DbSet<EmployeeStatusLog> EmployeeStatusLogs { get; set; }
        public virtual DbSet<Resignation> Resignations { get; set; }
        public virtual DbSet<OfficialResignRules> OfficialResignRules { get; set; }
        public virtual DbSet<Rejoin> Rejoins { get; set; }
        public virtual DbSet<EmployeeWeekends> EmployeeWeekends { get; set; }
        #endregion


        public virtual DbSet<Campus> Campus { get; set; }
        public virtual DbSet<Class> Class { get; set; }
        public virtual DbSet<ClassBook> ClassBook { get; set; }
        public virtual DbSet<ClassCourseMaster> ClassCourseMaster { get; set; }
        public virtual DbSet<ClassCourseSubject> ClassCourseSubject { get; set; }
        public virtual DbSet<ClassSection> ClassSection { get; set; }
        public virtual DbSet<ClassTeacher> ClassTeacher { get; set; }
        public virtual DbSet<ClassSectionRoutine> ClassSectionRoutine { get; set; }
        //public virtual DbSet<ClassSubjectTeacher> ClassSubjectTeacher { get; set; }
        public virtual DbSet<EventRoutine> EventRoutine { get; set; }
        public virtual DbSet<Gallery> Gallery { get; set; }
        public virtual DbSet<HelpfulResouce> HelpfulResouce { get; set; }
        public virtual DbSet<Job> Job { get; set; }
        public virtual DbSet<JobApply> JobApply { get; set; }
        public virtual DbSet<Leave> Leave { get; set; }
        public virtual DbSet<Logo> Logo { get; set; }
        public virtual DbSet<LogoType> LogoType { get; set; }
        public virtual DbSet<Notice> Notice { get; set; }
        public virtual DbSet<PhotoGallery> PhotoGallery { get; set; }
        public virtual DbSet<Routine> Routine { get; set; }
        public virtual DbSet<RoutineSetup> RoutineSetup { get; set; }
        public virtual DbSet<MarksheetStatus> MarksheetStatus { get; set; }
        public virtual DbSet<School> School { get; set; }
        public virtual DbSet<SchoolBranch> SchoolBranch { get; set; }
        public virtual DbSet<Section> Section { get; set; }
        public virtual DbSet<Session> Session { get; set; }
        public virtual DbSet<StuDocument> StuDocument { get; set; }
        public virtual DbSet<Student> Student { get; set; }
        public virtual DbSet<StudentAdmissionTest> StudentAdmissionTest { get; set; }
        public virtual DbSet<StudentAdmissionMark> StudentAdmissionMark { get; set; }
        public virtual DbSet<StudentBillMaster> StudentBillMaster { get; set; }
        public virtual DbSet<StudentBillLine> StudentBillLine { get; set; }
        public virtual DbSet<StudentBillMasterTemporary> StudentBillMasterTemporaries { get; set; }
        public virtual DbSet<StudentBillLineTemporary> StudentBillLineTemporaries { get; set; }
        public virtual DbSet<StudentMarks> StudentMarks { get; set; }
        public virtual DbSet<StudentMedical> StudentMedical { get; set; }
        public virtual DbSet<StudentProx> StudentProx { get; set; }
        public virtual DbSet<StudentSiblings> StudentSiblings { get; set; }
        public virtual DbSet<SubTeacher> SubTeacher { get; set; }
        public virtual DbSet<SchoolTask> SchoolTask { get; set; }
        public virtual DbSet<VoteMaster> VoteMaster { get; set; }
        public virtual DbSet<VoteMember> VoteMember { get; set; }
        public virtual DbSet<VoteOption> VoteOption { get; set; }
        public virtual DbSet<SchoolCalender> SchoolCalender { get; set; }
        public virtual DbSet<EmergencyMedicalAction> EmergencyMedicalAction { get; set; }
        public virtual DbSet<FndFlexValue> FndFlexValue { get; set; }
        public virtual DbSet<FndFlexValueSet> FndFlexValueSet { get; set; }
        public virtual DbSet<FndMenu> FndMenu { get; set; }
        public virtual DbSet<FndMenuGroup> FndMenuGroups { get; set; }
        public virtual DbSet<FndMenuPage> FndMenuPage { get; set; }
        public virtual DbSet<FndModule> FndModule { get; set; }
        public virtual DbSet<FndPage> FndPage { get; set; }
        public virtual DbSet<FndPagePics> FndPagePics { get; set; }
        public virtual DbSet<FndPeriods> FndPeriods { get; set; }
        public virtual DbSet<FndResponceMenu> FndResponceMenu { get; set; }
        public virtual DbSet<FndResponsibility> FndResponsibility { get; set; }
        public virtual DbSet<FndUserPage> FndUserPage { get; set; }
        public virtual DbSet<FndUsers> FndUsers { get; set; }
        public virtual DbSet<GeneralInfo> GeneralInfo { get; set; }
        public virtual DbSet<GeneralInfoType> GeneralInfoType { get; set; }
        public virtual DbSet<HrDepartment> HrDepartment { get; set; }
        public virtual DbSet<HrDesigGrade> HrDesigGrade { get; set; }
        public virtual DbSet<HrEmployee> HrEmployee { get; set; }
        public virtual DbSet<HrEmpExperience> HrEmpExperience { get; set; }
        public virtual DbSet<HrEmployeeHierarcy> HrEmployeeHierarcy { get; set; }
        public virtual DbSet<HrEmployeeRefrence> HrEmployeeRefrence { get; set; }
        public virtual DbSet<HrLeaveInfo> HrLeaveInfo { get; set; }
        public virtual DbSet<HrPositionStructure> HrPositionStructure { get; set; }
        public virtual DbSet<MedicalHistory> MedicalHistory { get; set; }
        public virtual DbSet<PlannerDay> PlannerDay { get; set; }
        public virtual DbSet<PlannerEvent> PlannerEvent { get; set; }
        public virtual DbSet<PlannerEventClass> PlannerEventClass { get; set; }
        public virtual DbSet<PlannerEventDetails> PlannerEventDetails { get; set; }
        public virtual DbSet<PlannerEventType> PlannerEventType { get; set; }
        public virtual DbSet<PlannerHoliday> PlannerHoliday { get; set; }
        public virtual DbSet<Publisher> Publisher { get; set; }
        public virtual DbSet<SchoolLinks> SchoolLinks { get; set; }
        public virtual DbSet<SchoolType> SchoolType { get; set; }
        public virtual DbSet<StudentAdmitCard> StudentAdmitCard { get; set; }
        public virtual DbSet<ClassSubject> ClassSubject { get; set; }
        public virtual DbSet<News> News { get; set; }
        public virtual DbSet<SmsHistory> SmsHistory { get; set; }
        public virtual DbSet<MenuGroup> MenuGroup { get; set; }

        public virtual DbSet<Subject> Subject { get; set; }
        public virtual DbSet<Career> Career { get; set; }
        public virtual DbSet<FndUserResponce> FndUserResponce { get; set; }
        public virtual DbSet<HrEmpEduQuali> HrEmpEduQuali { get; set; }
        public virtual DbSet<HrEmployeeTiming> HrEmployeeTiming { get; set; }
        public virtual DbSet<OnlineInquiry> OnlineInquiry { get; set; }
        public virtual DbSet<StudentRagistration> StudentRagistration { get; set; }
        public virtual DbSet<JobApplicant> JobApplicant { get; set; }
        public virtual DbSet<SubjectSubgroupClass> SubjectSubgroupClass { get; set; }
        public virtual DbSet<SubjectSubgroup> SubjectSubgroup { get; set; }
        public virtual DbSet<AlternateSubject> AlternateSubject { get; set; }
        public virtual DbSet<StudentAdmission> StudentAdmission { get; set; }
        public virtual DbSet<ExamGrade> ExamGrade { get; set; }
        public virtual DbSet<ClassExamGrade> ClassExamGrade { get; set; }
        public virtual DbSet<StudentCurriculumResult> StudentCurriculumResult { get; set; }
        public virtual DbSet<StudentOtherInfo> StudentOtherInfo { get; set; }
        public virtual DbSet<ProcessLog> ProcessLog { get; set; }
        public virtual DbSet<ProcessAssessmentMarks> ProcessAssessmentMarks { get; set; }
        public virtual DbSet<ReportProcess> ReportProcess { get; set; }
        public virtual DbSet<AttendanceGrade> AttendanceGrade { get; set; }
        public virtual DbSet<StudentSubject> StudentSubject { get; set; }
        public virtual DbSet<StudentPosition> StudentPosition { get; set; }
        public virtual DbSet<StudentPositionLog> StudentPositionLog { get; set; }
        public virtual DbSet<StudentRemarks> StudentRemarks { get; set; }
        public virtual DbSet<SubjectSubgroupClassSection> SubjectSubgroupClassSection { get; set; }
        public virtual DbSet<StudentHistory> StudentHistory { get; set; }
        public virtual DbSet<ClassRoutine> ClassRoutine { get; set; }
        public virtual DbSet<AttendenceInfo> AttendenceInfo { get; set; }

        public virtual DbSet<HrShift> HrShift { get; set; }
        public virtual DbSet<HrEmployeeShift> HrEmployeeShift { get; set; }
        public virtual DbSet<HrExcepShift> HrExcepShift { get; set; }
        public virtual DbSet<AttendanceEmployee> AttendanceEmployee { get; set; }
        public virtual DbSet<AttendancePolicy> AttendancePolicy { get; set; }

        public virtual DbSet<TempLeave> TempLeave { get; set; }
        public virtual DbSet<LeaveSetup> LeaveSetup { get; set; }
        public virtual DbSet<EmployeeLeaveInfo> EmployeeLeaveInfo { get; set; }
        public virtual DbSet<LeaveMaster> LeaveMaster { get; set; }
        public virtual DbSet<EmployeeManager> EmployeeManager { get; set; }
        public virtual DbSet<HrAttendenceFix> HrAttendenceFix { get; set; }
        public virtual DbSet<StudentTransaction> StudentTransaction { get; set; }
        public virtual DbSet<HrEmployeeTransaction> HrEmployeeTransaction { get; set; }
        public virtual DbSet<MailGroup> MailGroup { get; set; }
        public virtual DbSet<ProcessQueryTableMailGroup> ProcessQueryTableMailGroup { get; set; }
        public virtual DbSet<Schedule> Schedule { get; set; }
        public virtual DbSet<ProcessSchedule> ProcessSchedule { get; set; }
        public virtual DbSet<RecipientGroup> RecipientGroup { get; set; }
        public virtual DbSet<MessageAlertMaster> MessageAlertMaster { get; set; }
        public virtual DbSet<RemarksGrade> RemarksGrade { get; set; }

        public virtual DbSet<FeeCategory> FeeCategory { get; set; }
        public virtual DbSet<FeeSubCategory> FeeSubCategory { get; set; }
        public virtual DbSet<SubCategoryFeeType> SubCategoryFeeType { get; set; }
        public virtual DbSet<FeeSubCategoryFine> FeeSubCategoryFine { get; set; }
        public virtual DbSet<FeeSubCategoryWaiver> FeeSubCategoryWaiver { get; set; }
        public virtual DbSet<BillPeriod> BillPeriod { get; set; }
        public virtual DbSet<LeaveApplicationFile> LeaveApplicationFile { get; set; }
        public virtual DbSet<Exam> Exam { get; set; }
        public virtual DbSet<ExamType> ExamType { get; set; }
        public virtual DbSet<ClassSubjectWiseExamType> ClassSubjectWiseExamType { get; set; }
        public virtual DbSet<StudentCategory> StudentCategories { get; set; }
        public virtual DbSet<SubCategoryFeePeriod> SubCategoryFeePeriods { get; set; }
        public virtual DbSet<StudentCategoryLog> StudentCategoryLogs { get; set; }
        public virtual DbSet<StudentLeaveApplication> StudentLeaveApplications { get; set; }
        public virtual DbSet<StudentLeaveCount> StudentLeaveCounts { get; set; }
        public virtual DbSet<Device> Devices { get; set; }
        public virtual DbSet<TeacherLeaveCount> TeacherLeaveCount { get; set; }
        public virtual DbSet<ExamPublishInfo> ExamPublishInfos { get; set; }
        public virtual DbSet<BookCategory> BookCategories { get; set; }
        public virtual DbSet<SMSTemplate> SMSTemplate { get; set; }
        public virtual DbSet<BookShelfs> BookShelfs { get; set; }
        public virtual DbSet<SmsBuyingLog> SmsBuyingLogs { get; set; }
        public virtual DbSet<ClassSectionGroupTeacherMapping> ClassSectionGroupTeachers { get; set; }
        public virtual DbSet<Year> Years { get; set; }
        public virtual DbSet<IssuedBook> IssuedBooks { get; set; }
        public virtual DbSet<SubCategoryYear> SubCategoryYears { get; set; }
        public virtual DbSet<BookAuthor> BookAuthors { get; set; }
        public virtual DbSet<ClassSectionRoutineTransaciton> ClassSectionRoutineTransacitons { get; set; }
        public virtual DbSet<ExamSchedule> ExamSchedules { get; set; }
        public virtual DbSet<ExamScheduleLine> ExamScheduleLines { get; set; }
        public virtual DbSet<ExamSeatPlan> ExamSeatPlans { get; set; }
        public virtual DbSet<ExamRoomInvestigator> ExamRoomInvestigators { get; set; }
        public virtual DbSet<DateWiseInvestigator> DateWiseInvestigators { get; set; }
        public virtual DbSet<DateWiseSubject> DateWiseSubjects { get; set; }
        public virtual DbSet<RoomSetup> RoomSetups { get; set; }
        public virtual DbSet<ClassRoom> ClassRooms { get; set; }
        public virtual DbSet<ClassRoomColumn> ClassRoomColumns { get; set; }
        public virtual DbSet<ClassRoomColumnRow> ClassRoomColumnRows { get; set; }
        public virtual DbSet<AttendanceChangeRequist> AttendanceChangeRequists { get; set; }
        public virtual DbSet<PaymentGatewaySetting> PaymentGatewaySettings { get; set; }
        public virtual DbSet<NotifyInfo> NotifyInfos { get; set; }
        public virtual DbSet<NotifyTransactionInfo> NotifyTransactionInfos { get; set; }
        public virtual DbSet<ProcessedMarksheet> ProcessedMarksheets { get; set; }
        public virtual DbSet<StudentSubjectMaster> StudentSubjectMasters { get; set; }
        public virtual DbSet<StudentSubjectLine> StudentSubjectLines { get; set; }
        public virtual DbSet<StudentWiseSubject> StudentWiseSubjects { get; set; }
        public IEnumerable<object> LeaveApplicationFiles { get; set; }
    }

}
