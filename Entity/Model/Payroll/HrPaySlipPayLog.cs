﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class HrPaySlipPayLog : BaseEntity
    {
        [Key]
        public int HrPaySlipPayLogId { get; set; }
        public int PaySlipMasterHeaderId { get; set; }
        public decimal PaidAmount { get; set; }
        public DateTime PaidDate { get; set; }
        public string Remarks { get; set; }
        public PayslipCurrentStatus PayslipCurrentStatus { get; set; } // current status everytime during a payment
        public virtual HrPaySlipTransactionMaster HrPaySlipTransactionMaster { get; set; }
    }
    // Attrabute1 is used for isDeleted or not
}
