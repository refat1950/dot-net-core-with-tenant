﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class RecipientGroup : BaseEntity
    {
        [Key]
        public int GroupId { get; set; }
        public string GroupName { get; set; }
        public string Query { get; set; }
        public bool Status { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool IsValidated { get; set; }
    }
}
