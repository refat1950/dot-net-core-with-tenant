﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class History
    {
        [Key]
        public int HistoryId { get; set; }
        public string HistoryDetails { get; set; }
        [DataType(DataType.Upload)]
        [Display(Name = "Upload File")]
        [Required(ErrorMessage = "Please choose file to upload.")]
        public string HistoryImage { get; set; }
        public int LastUpdateBy { get; set; }
        public DateTime LastUpdateDate { get; set; }
        public object EffectiveTo { get; set; }
        public object EffectiveFrom { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreationDate { get; set; }
    }
}
