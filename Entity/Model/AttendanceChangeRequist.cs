using System;
using System.ComponentModel.DataAnnotations;

namespace Entity.Model
{
    /// <summary>
    /// This Entity is required for Attendance Data processing. If Need to Change Pls Check with corresponding person.
    /// </summary>
    public partial class AttendanceChangeRequist : BaseEntity
    {
        [Key]
        public int AttendanceChangeRequistId { get; set; }
        public int? EmployeeHeaderId { get; set; }
        public int? StudentHeaderId { get; set; }
        public DateTime PunchDate { get; set; }
        public string OfficeInTime { get; set; }
        public string OfficeOutTime { get; set; }
        public string InTime { get; set; }
        public string OutTime { get; set; }
        public string RequistedInTime { get; set; }
        public string RequistedOutTime { get; set; }
        public string ApprovedInTime { get; set; }
        public string ApprovedOutTime { get; set; }
        public AttendanceApprovedStatus Status { get; set; }
        public bool SeenStatus { get; set; }
        public bool StudentSeenStatus { get; set; }
        public bool TeacherSeenStatus { get; set; }
        public string InTimeRemarks { get; set; }
        public string OutTimeRemarks { get; set; }
    }
    public enum AttendanceApprovedStatus
    {
        Pending = 1,
        Approved = 2,
        Cancel = 3
    }
}