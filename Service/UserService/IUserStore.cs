﻿using Entity.Model;
using System;
using System.Threading.Tasks;

namespace Services.UserService
{
    /// <summary>
    ///     Interface that exposes basic user management apis
    /// </summary>
    /// <typeparam name="TUser"></typeparam>
    //public interface IUserStore<TUser> : IUserStore<TUser> where TUser : class, IUser
    //{
    //}

    /// <summary>
    ///     Interface that exposes basic user management apis
    /// </summary>
    /// <typeparam name="TUser"></typeparam>
    /// <typeparam name="TKey"></typeparam>
    public interface IUserStore<TUser> : IDisposable where TUser : class, IUser
    {
        /// <summary>
        ///     Insert a new user
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        Task CreateAsync(TUser user);

        /// <summary>
        ///     Update a user
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        Task UpdateAsync(TUser user);

        /// <summary>
        ///     Delete a user
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        Task DeleteAsync(TUser user);

        /// <summary>
        ///     Finds a user
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<TUser> FindByIdAsync(int userId);

        /// <summary>
        ///     Finds a user
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        TUser FindById(int userId);

        /// <summary>
        ///     Find a user by name
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        Task<TUser> FindByNameAsync(string userName);

        TUser FindByName(string userName);
        TUser FindByReferenceId(int? ReferenceId, UserType userType);
    }
}