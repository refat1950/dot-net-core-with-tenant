﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Services.View_Model
{
    public class VmSmsLengthCounter
    {
        public int SmsLength { get; set; }
        public int MaxSmsLength { get; set; }
        public int SmsParts { get; set; }
        public bool IsAllowed { get; set; }
    }
}