using System;
using System.ComponentModel.DataAnnotations;

namespace Entity.Model
{
    public class ClassRoutine : BaseEntity
    {
        [Key]
        public int RoutineId { get; set; }

        public DateTime RoutineDate { get; set; }

        public int SessionId { get; set; }

        public int EventId { get; set; }

        public int Period { get; set; }

        public int ClassId { get; set; }

        public int SectionId { get; set; }

        public int CampusId { get; set; }

        public int Shift { get; set; }

        public int SubjectId { get; set; }

        public int? SubgroupId { get; set; }

        public int? BookId { get; set; }

        public int? TeacherId { get; set; }

        public bool IsActive { get; set; }

        public int ClassCount { get; set; }
    }
}
