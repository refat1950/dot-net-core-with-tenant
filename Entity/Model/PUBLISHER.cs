using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entity.Model
{
    [Table("Publisher")]
    public partial class Publisher : BaseEntity
    {
        [Key]
        public int PublisherId { get; set; }

        [Required]
        [StringLength(50)]
        public string PublisherName { get; set; }

        [StringLength(50)]
        public string Address1 { get; set; }

        [StringLength(50)]
        public string Address2 { get; set; }

        [StringLength(50)]
        public string Address3 { get; set; }

        public int? CountryId { get; set; }


    }
}
