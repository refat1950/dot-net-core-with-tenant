﻿using data.UnitOfWork;
using Entity.Model;
using Repository.Context;
using Service.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Services
{
    public class StudentPaymentService 
    {
        private IUnitOfWork _unitOfWork;
       

        public StudentPaymentService( IUnitOfWork unitOfWork) {
            _unitOfWork = unitOfWork;
        }


        public void StudentOnlinePayment(string transactionId ,int userid)
        {
            var tempbillmasterData = _unitOfWork.StudentBillMasterTemporariesRepo.FirstOrDefault(s => s.TransactionId == transactionId);
            var newrecord = new StudentBillMaster
            {
                BillNo = tempbillmasterData.BillNo,
                BillDate = DateTime.UtcNow,
                SessionId = tempbillmasterData.SessionId,
                StudentHeaderId = tempbillmasterData.StudentHeaderId,
                ClassId = tempbillmasterData.ClassId,
                SectionId = tempbillmasterData.SectionId,
                Shift = tempbillmasterData.Shift,
                Discount = tempbillmasterData.Discount,
                CreatedBy = userid,
                CreationDate = DateTime.UtcNow
            };
            _unitOfWork.StudentBillMasterRepo.Add(newrecord);
            tempbillmasterData.IsSynchronized = true;
            _unitOfWork.SaveChanges();

            var masterBillHeaderId = _unitOfWork.StudentBillMasterRepo.FirstOrDefault(s => s.BillNo == tempbillmasterData.BillNo).BillHeaderId;
            var linedata = _unitOfWork.StudentBillLineTemporariesRepo.Find(w => w.BillDtlId == tempbillmasterData.BillHeaderId).ToList();
            foreach (var item in linedata)
            {
                var newbill = new StudentBillLine
                {
                    BillDtlId = masterBillHeaderId,
                    PeriodId = item.PeriodId,
                    YearId = item.YearId,
                    FeeSubCategoryId = item.FeeSubCategoryId,
                    WaiberPercentage = Convert.ToDecimal(item.WaiberPercentage),
                    WaiberAmount = item.WaiberAmount,
                    LateFee = item.LateFee,
                    BillAmount = item.BillAmount,
                    ActualRcvAmount = item.ActualRcvAmount,
                    DiscAmount = item.DiscAmount,
                    Remarks = item.Remarks,
                    CreatedBy = userid,
                    CreationDate = DateTime.UtcNow
                };

                _unitOfWork.StudentBillLineRepo.Add(newbill);
            }
            _unitOfWork.SaveChanges();

        }

    }

}
