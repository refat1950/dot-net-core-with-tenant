﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class SubjectSubgroup //:BaseEntity
    {
        [Key]
        public int SubgorupId { get; set; }

        [StringLength(150)]
        public string SubgroupDesc { get; set; }

        [StringLength(50)]

        public string ShortName { get; set; }

        [StringLength(150)]
        public string AltName { get; set; }

        public int? SubjectId { get; set; }

        public int? BestCount { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }



        [StringLength(250)]
        public string Attribute1 { get; set; }

        [StringLength(250)]
        public string Attribute2 { get; set; }

        [StringLength(250)]
        public string Attribute3 { get; set; }

        public long? CreatedBy { get; set; }

        [DataType(DataType.Date, ErrorMessage = "Date format should be MM/DD/YYYY")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? CreationDate { get; set; }


        public long? LastUpdateBy { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? LastUpdateDate { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "Active From")]
        public DateTime? EffectiveFrom { get; set; }


        [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "Active To")]
        public DateTime? EffectiveTo { get; set; }
        //public virtual SubjectSubgroupClassSection SubjectSubgroupClassSection { get; set; }

    }
}
