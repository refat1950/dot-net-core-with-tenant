﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Services.View_Model
{
    public class VmStudentDocument
    {
        public string ClassName { get; set; }
        public string SectionName { get; set; }
        public string SubjectName { get; set; }
        public string DocumentType { get; set; }
        public string PublishDate { get; set; }
        public string EffectiveTo { get; set; }
        public string Path { get; set; }
    }
    public class VmDocumentList
    {
        public int DocId { get; set; }
        public string ClassName { get; set; }
        public string SectionName { get; set; }
        public string SessionName { get; set; }
        public string ShiftName { get; set; }
        public string SubjectName { get; set; }
        public string DocumentType { get; set; }
        public string PublishDate { get; set; }
        public string EffectiveTo { get; set; }
        public string DocumentName { get; set; }
        public string Path { get; set; }
        public string FileName { get; set; }
    }
}