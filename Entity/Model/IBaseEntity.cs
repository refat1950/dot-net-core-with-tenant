﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    interface IBaseEntity
    {
        DateTime EffectiveFrom { get; set; }
        DateTime? EffectiveTo { get; set; }
        string Attribute1 { get; set; }

        string Attribute2 { get; set; }

        string Attribute3 { get; set; }

        string Attribute4 { get; set; }

        string Attribute5 { get; set; }
        long CreatedBy { get; set; }
        DateTime CreatedOn { get; set; }
        long LastUpdatedBy { get; set; }
        DateTime LastUpdatedDate { get; set; }
    }
}
