﻿using Entity.Model;
using System;
using System.Threading.Tasks;

namespace Services.UserService
{
    /// <summary>
    ///     Stores a user's password hash
    /// </summary>
    /// <typeparam name="TUser"></typeparam>
    //public interface IUserPasswordStore<TUser> : IUserPasswordStore<TUser, int> where TUser : class, IUser<int>
    //{
    //}

    /// <summary>
    ///     Stores a user's password hash
    /// </summary>
    /// <typeparam name="TUser"></typeparam>
    /// <typeparam name="TKey"></typeparam>
    public interface IUserPasswordStore<TUser> : IUserStore<TUser> where TUser : class, IUser
    {
        /// <summary>
        ///     Set the user password hash
        /// </summary>
        /// <param name="user"></param>
        /// <param name="passwordHash"></param>
        /// <returns></returns>
        Task SetPasswordHashAsync(TUser user, string passwordHash);

        /// <summary>
        ///     Get the user password hash
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<string> GetPasswordHashAsync(TUser user);
        string GetPasswordHash(TUser user);

        /// <summary>
        ///     Returns true if a user has a password set
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<bool> HasPasswordAsync(TUser user);
    }
}