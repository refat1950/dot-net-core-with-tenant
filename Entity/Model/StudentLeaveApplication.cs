﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class StudentLeaveApplication : BaseEntity
    {
        [Key]
        public int ApplicationId { get; set; }
        public int StudentHeaderId { get; set; }
        public int LeaveCategoryId { get; set; }
        public DateTime LeaveStartDate { get; set; }
        public DateTime LeaveEndDate { get; set; }
        public DateTime LeaveApplyDate { get; set; }
        public string Subject { get; set; }
        public string Comment { get; set; }
        public LeaveStatus LeaveStatus { get; set; }
        public bool? IsOnDuty { get; set; }
        public string Remarks { get; set; }
        public string Attachment1 { get; set; }
        public string Attachment2 { get; set; }
        public string Attachment3 { get; set; }
    }

}
