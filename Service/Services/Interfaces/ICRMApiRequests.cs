﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Entity.Model;

namespace Repository.Provider.Interface
{
    public interface ICRMApiRequests
    {
        int StoreInvoiceAndSmsBalance(decimal balance, string transactionId);
        int StoreQueue(List<SenderInfo> senderInfos, int queueType);
        VmApiSmsTransection GetBalance();
        List<VmApiSmsTransectionHistory> GetSmsTransectionHistory(int yearId);
        List<VmApiSmsSendHistory> GetSmsSendHistory(string monthYear);
        VmPaymentGatewaySettings GetPaymentSettings(int providerId);
    }
    public class VmApiCustomer
    {
        public int CustomerHeaderId { get; set; }
        public string CompanyName { get; set; }
        public string CustomerName { get; set; }
        public string MobileNumber { get; set; }
        public string PhoneNumber { get; set; }
        public string EmailAddress { get; set; }
        public int? CountryId { get; set; }
        public int? DivisionOrStateId { get; set; }
        public int? DistrictId { get; set; }
        public int? ThanaOrUpazilaId { get; set; }
        public string AddressOne { get; set; }
        public string AddressTwo { get; set; }
        public string Attribute1 { get; set; } // School Tag Line

        public string CustomerUserName { get; set; }
        public string CustomerUserPasword { get; set; }

        public decimal balance { get; set; }
        public string transactionId { get; set; }
    }
    public class VmApiCustomerLoginData
    {
        public string CustomerUserName { get; set; }
        public string CustomerUserPasword { get; set; }
        public int YearId { get; set; }
        public int ProviderId { get; set; }
        public string MonthYear { get; set; }
    }
    public class VmApiSmsTransection
    {
        public decimal balance { get; set; }
        public decimal CumulativeBalance { get; set; }
        public decimal Cost { get; set; }
    }
    public class VmApiSmsTransectionHistory
    {
        public decimal Balance { get; set; }
        public string TransactionDate { get; set; }
    }
    public class VmQueue
    {
        public List<SenderInfo> senderInfos { get; set; }
        public int QueueType { get; set; }
        public string SendOn { get; set; }
        public string CustomerUserName { get; set; }
        public string CustomerUserPasword { get; set; }
    }
    public class SenderInfo
    {
        public string Text { get; set; }
        public string Receivers { get; set; }
    }
    public class VmApiSmsSendHistory
    {
        public int TotalSend { get; set; }
        public string SendnDate { get; set; }
    }
    public class VmPaymentGatewaySettings : BaseEntity
    {
        public int GatewayHeaderId { get; set; }
        public string MerchantNumber { get; set; }
        public string APIorURL { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string AccessOrAppkey { get; set; }
        public string SecretkeyOrToken { get; set; }
        public string CustomerOrClientCode { get; set; }
        public string PartnerCode { get; set; }
        public bool PaymentMode { get; set; }
        public string BankName { get; set; }
        public string AccountNumber { get; set; }
        public int? GatewayProvider { get; set; }
        public string Attribute7 { get; set; }
        public string Attribute8 { get; set; }
        public string Attribute9 { get; set; }
        public string Attribute10 { get; set; }
        public bool? PaymentStatus { get; set; }
        public DateTime? ActivationDate { get; set; }
        public string ActivationDateString { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public int? CompanyId { get; set; }
        public string ExpiryDateString { get; set; }
    }
}
