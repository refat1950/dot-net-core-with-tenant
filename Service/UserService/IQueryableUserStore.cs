﻿
using Entity.Model;
using System;
using System.Linq;

namespace Services.UserService
{
    /// <summary>
    ///     Interface that exposes an IQueryable users
    /// </summary>
    /// <typeparam name="TUser"></typeparam>
    //public interface IQueryableUserStore<TUser> : IQueryableUserStore<TUser, int> where TUser : class, IUser<int>
    //{
    //}

    /// <summary>
    ///     Interface that exposes an IQueryable users
    /// </summary>
    /// <typeparam name="TUser"></typeparam>
    /// <typeparam name="TKey"></typeparam>
    public interface IQueryableUserStore<TUser> : IUserStore<TUser> where TUser : class, IUser
    {
        /// <summary>
        ///     IQueryable users
        /// </summary>
        IQueryable<TUser> Users { get; }
    }
}