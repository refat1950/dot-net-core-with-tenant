﻿using Service.ViewModel.Payroll;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Services.Interfaces
{
    public interface IPaySlipOverTime
    {
        CalculatedFee GetOvertimeFee(int empHeaderId, int periodId);
    }
}
