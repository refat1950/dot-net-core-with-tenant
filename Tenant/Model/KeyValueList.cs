﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tenant.Model
{
    public class KeyValueList
    {
        public int Serial { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
