﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.View_Model
{
    public class VmBillInformation
    {
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public int PeriodId { get; set; }
        public string PeriodName { get; set; }
        public decimal BillAmount { get; set; }
        public string WaiberPercentage { get; set; }
        public decimal WaiberAmount { get; set; }
        public decimal LateFee { get; set; }
        public decimal ActualAmount { get; set; }
        public decimal Discount { get; set; }
        public decimal Total { get; set; }
        public decimal TotalAvailablePeriods { get; set; }
        public string DateDistance { get; set; }
        public string Remarks { get; set; }
        public int YearId { get; set; }
        public int YearName { get; set; }
    }
    public class VmBillLinelist
    {
        public int billLineId { get; set; }
        public decimal Latefee { get; set; }
        public decimal WaiverAmount { get; set; }
        public string BillDate { get; set; }
        public decimal? BillAmount { get; set; }
        public string Period { get; set; }
        public string Category { get; set; }
        public int SubCategoryId { get; set; }
        public decimal? Amount { get; set; }
        public decimal? Discount { get; set; }
        public int TotalBillCounter { get; set; }
    }

}
