﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class HrPayRollPayHead : BaseEntity
    {
        [Key]
        public int PayRollPayHeadId { get; set; }
        public int PayRollId { get; set; }
        public int PayHeadId { get; set; }
        public decimal Unit { get; set; }
        public bool IsPercentage { get; set; }
    }
}
