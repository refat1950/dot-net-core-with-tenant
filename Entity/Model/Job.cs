using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Entity.Model
{
    public partial class Job : BaseEntity
    {
        [Key]
        public int JobId { get; set; }

        [DisplayName("Job Catagory")]
        public int CategoryIdFlex { get; set; }

        [DisplayName("Job Nature")]
        public int JobTypeIdFlex { get; set; }

        [StringLength(256)]
        public string PostName { get; set; }

        public DateTime? StartDate { get; set; }

        [DisplayName("Application Deadline")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? EndDate { get; set; }

        [DisplayName("No. of Vacancies")]
        public int? NoOfVacancies { get; set; }


        [DisplayName("Job Description / Responsibility")]
        //[Column(TypeName = "ntext")]
        public string JobDescription { get; set; }


        [DisplayName("Educational Requirements")]
        public string EduReq { get; set; }


        [DisplayName("Experience Requirements")]
        public string ExpReq { get; set; }


        [DisplayName("Additional Job Requirements")]
        //[Column(TypeName = "ntext")]
        public string AditionalReq { get; set; }

        [DisplayName("Job Location")]
        [StringLength(256)]
        public string JobLocation { get; set; }

        [DisplayName("Salary Range")]
        [StringLength(256)]
        public string SalaryRange { get; set; }

        public bool? IsActivate { get; set; }


        public virtual List<JobApply> JobApply { get; set; }
    }
}
