using System;
using System.ComponentModel.DataAnnotations;

namespace Entity.Model
{
    public partial class FndPagePics : BaseEntity
    {
        [Key]
        public int PicId { get; set; }

        [Required]
        [StringLength(100)]
        public string PicName { get; set; }

        public int? PageId { get; set; }

        [StringLength(100)]
        public string Url { get; set; }

        public int? Position { get; set; }

        [StringLength(6)]
        public string XPosition { get; set; }

        [StringLength(6)]
        public string YPosition { get; set; }

        public int? OrderBy { get; set; }
    }
}
