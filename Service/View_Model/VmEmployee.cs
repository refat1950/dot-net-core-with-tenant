﻿using Entity.Model;
using Microsoft.AspNetCore.Http;
using Repository.DefaultValues;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Web.Models
{
    public class VmEmployee
    {
        public int EmpHeaderId { get; set; }

        [Required]
        [StringLength(15)]
        public string EmpId { get; set; }

        [Required(ErrorMessage = "Please Provide First Name.")]
        public string EmpFirstName { get; set; }

        [StringLength(30)]
        public string EmpMiddleName { get; set; }

        [Required(ErrorMessage = "Please Provide Last Name.")]
        public string EmpLastName { get; set; }

        [StringLength(30)]
        public string EmpName { get; set; }
        public string EmpNickName { get; set; }

        public int? BusinessGroupId { get; set; }

        public int? ApplicantId { get; set; }

        public int? DepartmentId { get; set; }

        public int? DesignationId { get; set; }

        [StringLength(20)]
        public string PunchCardId { get; set; }

        public string JoinningDate { get; set; }

        public string JobEndDate { get; set; }

        public bool? IsPermenent { get; set; }

        public bool? Status { get; set; }

        public int? DeviceId { get; set; }

        public string BirthDate { get; set; }

        public DateTime? DeathDate { get; set; }

        public int? JobLocationId { get; set; }

        public int? BaseOfficeId { get; set; }

        public int? UnitId { get; set; }

        public int? InternalLocationId { get; set; }

        [StringLength(20)]
        public string PresHoldingNo { get; set; }

        [StringLength(50)]
        public string PresStreet { get; set; }

        [StringLength(50)]
        public string PresArea { get; set; }

        public int? PresThanaId { get; set; }

        [StringLength(10)]
        public string PresPostCode { get; set; }

        public int? PresCityId { get; set; }

        public int? PresDivisionId { get; set; }

        public int? PresCountryId { get; set; }

        [StringLength(20)]
        public string PermHoldingNo { get; set; }

        [StringLength(50)]
        public string PermStreet { get; set; }

        [StringLength(50)]
        public string PermArea { get; set; }

        public int? PermThanaId { get; set; }

        [StringLength(10)]
        public string PermPostCode { get; set; }

        public int? PermCityId { get; set; }

        public int? PermDivisionId { get; set; }

        public int? PermCountryId { get; set; }

        public bool? Gender { get; set; }

        [StringLength(30)]
        public string Religion { get; set; }

        [StringLength(60)]
        public string PlaceOfBirth { get; set; }

        public int? BirthCountry { get; set; }

        public int? BirthCity { get; set; }

        [StringLength(50)]
        public string Nationality { get; set; }

        [StringLength(30)]
        public string PassportNo { get; set; }

        [StringLength(30)]
        public string NationalId { get; set; }

        [StringLength(30)]
        public string SocialSecurityId { get; set; }

        [StringLength(30)]
        public string DrivingLicenseNo { get; set; }

        [StringLength(10)]
        public string BloodGroup { get; set; }

        [StringLength(10)]
        public string MaritalStatus { get; set; }

        [StringLength(1)]
        public string IsStudent { get; set; }

        [StringLength(20)]
        public string WorkPhone { get; set; }

        [StringLength(20)]
        public string Mobile { get; set; }

        [StringLength(20)]
        public string Phone { get; set; }

        [StringLength(100)]
        public string Email { get; set; }

        [StringLength(100)]
        public string Email2 { get; set; }

        [StringLength(30)]
        public string FatherFirstName { get; set; }

        [StringLength(30)]
        public string FatherMiddleName { get; set; }

        [StringLength(30)]
        public string FatherLastName { get; set; }

        [StringLength(20)]
        public string FatherContactNo { get; set; }

        [StringLength(30)]
        public string FatherNationalId { get; set; }

        [StringLength(30)]
        public string MotherFirstName { get; set; }

        [StringLength(30)]
        public string MotherMiddleName { get; set; }

        [StringLength(30)]
        public string MotherLastName { get; set; }

        [StringLength(20)]
        public string MotherContactNo { get; set; }

        [StringLength(30)]
        public string MotherNationalId { get; set; }

        [StringLength(30)]
        public string SpouseFirstName { get; set; }

        [StringLength(30)]
        public string SpouseMiddleName { get; set; }

        [StringLength(30)]
        public string SpouseLastName { get; set; }

        [StringLength(20)]
        public string SpouseContactNo { get; set; }

        [StringLength(30)]
        public string SpouseNationalId { get; set; }

        public int? NoOfDependent { get; set; }

        public DateTime? BackgroundCheckDate { get; set; }

        public int? BackgroundCheckBy { get; set; }

        public bool? BackgroundCheckStatus { get; set; }

        [StringLength(150)]
        public string LastMedicalStatus { get; set; }

        public string LastMedicalCheckDate { get; set; }

        [StringLength(500)]
        public string MedicalHistory { get; set; }

        [StringLength(150)]
        public string Signature { get; set; }

        [StringLength(150)]
        public string ImgUrl { get; set; }

        [StringLength(100)]
        public string Attribute6 { get; set; }

        [StringLength(100)]
        public string Attribute7 { get; set; }

        [StringLength(100)]
        public string Attribute8 { get; set; }

        [StringLength(100)]
        public string Attribute9 { get; set; }

        [StringLength(100)]
        public string Attribute10 { get; set; }

        [StringLength(100)]
        public string Attribute11 { get; set; }

        [StringLength(100)]
        public string Attribute12 { get; set; }

        [StringLength(100)]
        public string Attribute13 { get; set; }

        [StringLength(100)]
        public string Attribute14 { get; set; }

        [StringLength(100)]
        public string Attribute15 { get; set; }

        public int? PositionId { get; set; }
        public UserType? UserTypeId { get; set; }
        public int? GenderId { get; set; }
        public int? StatusId { get; set; }
        public IFormFile File { get; set; }

        public List<VmEmpEduQualification> vmEmpEduQualifications { get; set; }
        public List<VmEmpExperience> vmEmpExperiences { get; set; }
        public List<VmEmployeeRefrence> vmEmployeeRefrences { get; set; }

        public bool? IsPositionChangeShowed { get; set; }
        public string PositionAssignFrom { get; set; }
        public bool PositionCancelReqClicked { get; set; }

    }

    public class VmEmpEduQualification
    {
        public int EmpHeaderId { get; set; }

        public int QualificationId { get; set; }

        public int? QualificationType { get; set; }

        public int? Major { get; set; }

        public int? InstitutionId { get; set; }

        public int? BoardId { get; set; }

        [StringLength(50)]
        public string PassingYear { get; set; }

        [StringLength(50)]
        public string Result { get; set; }

        public string StartDate { get; set; }

        public string EndDate { get; set; }
    }

    public class VmEmpExperience
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int EmpHeaderId { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ExperienceId { get; set; }

        [StringLength(50)]
        public string CompanyName { get; set; }

        [StringLength(50)]
        public string Designation { get; set; }

        [StringLength(50)]
        public string Location { get; set; }

        [StringLength(200)]
        public string Responsibility { get; set; }

        [StringLength(50)]
        public string StartDate { get; set; }

        [StringLength(50)]
        public string EndDate { get; set; }
    }

    public class VmEmployeeRefrence
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int EmpHeaderId { get; set; }

        [Key]
        [Column(Order = 1)]
        public int ReferenceId { get; set; }

        [StringLength(50)]
        public string ReferenceName { get; set; }

        [StringLength(100)]
        public string Designation { get; set; }

        [StringLength(100)]
        public string Address1 { get; set; }

        [StringLength(100)]
        public string Address2 { get; set; }

        [StringLength(100)]
        public string Address3 { get; set; }

        [StringLength(30)]
        public string ResPhone { get; set; }

        [StringLength(30)]
        public string OffPhone { get; set; }

        [StringLength(30)]
        public string Mobile1 { get; set; }

        [StringLength(30)]
        public string Mobile2 { get; set; }

        [StringLength(50)]
        public string Email { get; set; }

        [StringLength(30)]
        public string Relation { get; set; }
    }
}