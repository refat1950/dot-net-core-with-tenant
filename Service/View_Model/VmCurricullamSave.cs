﻿using System.Collections.Generic;

namespace Services.View_Model
{
    public class VmCurricullamSave
    {
        public int ClassId { get; set; }
        public int SectionId { get; set; }
        public int EventId { get; set; }
        public List<StudentCurricullam> StudentCurricullams { get; set; }
    }

    public class StudentCurricullam
    {
        public int StudentId { get; set; }
        public List<VmCurricullamList> CurricullamList { get; set; }
    }

    public class VmCurricullamList
    {
        public int Id { get; set; }
        public int val { get; set; }
    }
}