using System;
using System.ComponentModel.DataAnnotations;

namespace Entity.Model
{
    public partial class TaskStatus : BaseEntity
    {
        [Key]
        public int TaskStatusId { get; set; }

        public int? Status { get; set; }


    }
}
