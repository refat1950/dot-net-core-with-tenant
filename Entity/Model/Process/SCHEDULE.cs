﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model.Process
{
    public class Schedule : BaseEntity
    {
        [Key]
        public int ScheduleId { get; set; }
        public string ScheduleName { get; set; }
        public string CronTime { get; set; }
        public bool Status { get; set; }
    }
}
