﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class ProcessAssessmentMarks
    {
        [Key]
        public int ProcesssAssessmentMarksId { get; set; }


        public int ReportProcessId { get; set; }

        public int StudentHeaderId { get; set; }

        public int ProcessLogId { get; set; }
        public int ClassId { get; set; }
        public int SessionId { get; set; }
        public int SectionId { get; set; }

        public string SubjectName { get; set; }
        public int? SubjectId { get; set; }
        public string SubgroupName { get; set; }
        public int TotalTest { get; set; }

        public int BestCount { get; set; }
        public string Best { get; set; }
        public decimal? Total { get; set; }


        public string Assesment1 { get; set; }
        public string Assesment2 { get; set; }
        public string Assesment3 { get; set; }
        public string Assesment4 { get; set; }
        public string Assesment5 { get; set; }
        public string Assesment6 { get; set; }
        public string Assesment7 { get; set; }
        public string Assesment8 { get; set; }
        public string Assesment9 { get; set; }
        public string Assesment10 { get; set; }

        public decimal? SemesterMark { get; set; }
        public decimal? TotalObtainedMark { get; set; }

        //   public int TotalWorkingDay { get; set; }
        //   public int TotalAbsent { get; set; }
        // public int TotalLate { get; set; }
        // public decimal Height { get; set; }
        // public decimal Weight { get; set; }
        // public string MARKS_ATTRIBUTE1 { get; set; }
    }
}