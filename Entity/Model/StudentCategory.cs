﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entity.Model
{
    public class StudentCategory : BaseEntity
    {
        public int FromSessionId { get; set; }
        public int ToSessionId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int StudentHeaderId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int StudentCategoryId { get; set; }
    }
}
