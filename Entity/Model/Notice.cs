using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Entity.Model
{
    public partial class Notice : BaseEntity
    {
        [Key]
        public int NoticeId { get; set; }

        public DateTime NoticeDate { get; set; }

        [Required(ErrorMessage = "Notice Type Required")]
        public int? NoticeType { get; set; }


        [Required(ErrorMessage = "Notice For Required")]
        public int? NoticeFor { get; set; }

        public int? ReferenceId { get; set; }

        [StringLength(500)]
        public string Header { get; set; }

        //[Column(TypeName = "ntext")]
        public string Body { get; set; }

        //[Column(TypeName = "ntext")]
        public string HtmlBody { get; set; }

        [StringLength(500)]
        public string Tailer { get; set; }

        public int? CampusId { get; set; }

        public int? CheckBy { get; set; }

        public int? OrderBy { get; set; }

        public int? ApprovedBy { get; set; }

        public int? ClassId { get; set; }

        public int? SectionId { get; set; }

        public int? ShiftId { get; set; }
    }
}
