﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class EmployeeStatusLog : BaseEntity
    {
        [Key]
        public int EmployeeStatusLogId { get; set; }
        public int EmpHeaderId { get; set; }
        public EmployeeJoiningStatus Status { get; set; }
        public virtual HrEmployee Employee { get; set; }
    }
    public enum EmployeeJoiningStatus
    {
        Joined = 1,
        Left = 2,
        Rejoined = 3,
        Not_Set = 4
    }
}
