﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class Exam : BaseEntity
    {
        [Key]
        public int ExamId { get; set; }
        public string ExamName { get; set; }
    }
}
