﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class User : BaseEntity, IUser
    {
        [Key]
        public int UserId { get; set; }
        [Required]
        public string UserName { get; set; }
        public int? ReferrenceId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public bool? EmailConfirmed { get; set; }
        public bool? IsTemporaryPassword { get; set; }
        [Required]
        public string Password { get; set; }
        public UserType UserType { get; set; }
        public int? Status { get; set; }
        public string About { get; set; }
        public string ImageCaption { get; set; }
        public string ImagePath { get; set; }
        public long? ImageSize { get; set; }
        public string AddressOne { get; set; }
        public string AddressTwo { get; set; }
        public string Country { get; set; }
        public string Region { get; set; }
        public string City { get; set; }
        public string PostCode { get; set; }
        public string PhoneNumber { get; set; }
        public bool? PhoneNumberConfirmed { get; set; }
        public string Position { get; set; }
        public bool? IsActivated { get; set; }
        public string GuidIdNumber { get; set; }
        public bool? IsTrialUser { get; set; }
        public bool? RegistrationConfirmed { get; set; }
        public DateTime? LockoutDate { get; set; }
        public bool? LockoutEnabled { get; set; }
        public int? AccessFailedCount { get; set; }
        public string SecurityStamp { get; set; }
        public bool? TwoFactorEnabled { get; set; }
        public virtual ICollection<UserRole> UserRoles { get; set; }
        public virtual ICollection<UserLoginTracer> Logins { get; set; }
    }

    public enum UserType
    {
        Superadmin = 1,
        AppAdmin = 2,
        Teacher = 3,
        Employee = 4,
        Student = 5
    }
}
