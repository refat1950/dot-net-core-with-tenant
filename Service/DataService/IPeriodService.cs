﻿using Entity.Model;
using Service.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Service.DataService
{
    public interface IPeriodService
    {
        void AddRange(IEnumerable<BillPeriod> periods);
        bool AlreadyExists(int year);
        int SaveChanges();
        IEnumerable<BillPeriod> GetActivePeriods();
        IEnumerable<VmPeriodYear> GetYears();
        BillPeriod GetCurrentPeriod();
        IEnumerable<BillPeriod> Search(Expression<Func<BillPeriod, bool>> predicate);
        int GetPeriodDistance(int fromMonth,int toMonth);
        BillPeriod GetPeriodByDate(DateTime date);
        BillPeriod GetNextPeriod(int PeriodId);
        BillPeriod GetPreviousPeriod(int PeriodId);
        bool IsCurrentPeriodGreaterOrNot(int oldPeriodId, int currentPeriodId);
        BillPeriod GetPeriodById(int id);
    }
}
