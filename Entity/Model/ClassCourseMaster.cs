using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entity.Model
{
    public partial class ClassCourseMaster : BaseEntity
    {
        [Key]
        public int CourseMasterId { get; set; }

        public int? ClassId { get; set; }

        public int? SectionId { get; set; }

        public int? Shift { get; set; }

        public int? EventId { get; set; }

        public int? ClassDay { get; set; }

        [Column(TypeName = "date")]
        public DateTime? ClassDate { get; set; }


    }
}
