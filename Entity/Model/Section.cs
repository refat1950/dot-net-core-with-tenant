using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Entity.Model
{
    public partial class Section : BaseEntity
    {
        [Key]
        public int SectionId { get; set; }

        [Required]
        [StringLength(50)]
        public string SectionName { get; set; }

        public virtual List<StudentMarks> StudentMarks { get; set; }
    }
}
