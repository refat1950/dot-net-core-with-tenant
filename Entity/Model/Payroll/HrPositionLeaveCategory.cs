﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class HrPositionLeaveCategory : BaseEntity
    {
        [Key]
        public int PositionLeaveCategoryId { get; set; }
        public int PositionId { get; set; }
        public int LeaveCategoryId { get; set; }
        public int LeaveCount { get; set; }
    }
}
