﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tenant.Entities
{
    public class MultitenantProcessLog
    {
        [Key]
        public int ProcessLogHeaderId { get; set; }
        public int TenantId { get; set; }
        public int ProcessHeaderId { get; set; }
        public int? MACResellerId { get; set; }
        public DateTime ProcessTime { get; set; }
        public int Status { get; set; }
        public string Error { get; set; }
        public string ErrorFileName { get; set; }
        public int? ErrorLineNumber { get; set; }
    }
}
