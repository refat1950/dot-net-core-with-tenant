using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entity.Model
{
    public partial class AlertHistory : BaseEntity
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int AlertHistoryId { get; set; }
        public DateTime RunTime { get; set; }

        public int? Status { get; set; }

        [StringLength(500)]
        public string ErrorMsg { get; set; }

        public virtual ScoolAlert SchoolAlert { get; set; }
    }
}
