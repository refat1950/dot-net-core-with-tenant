using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entity.Model
{
    [Table("Career")]
    public partial class Career : BaseEntity
    {

        [Key]
        public int CareerId { get; set; }

        public string UserId { get; set; }

        [StringLength(30)]
        public string EmpFirstName { get; set; }

        [StringLength(30)]
        public string EmpMiddleName { get; set; }

        [StringLength(30)]
        public string EmpLastName { get; set; }

        public DateTime? BirthDate { get; set; }

        [StringLength(30)]
        public string FatherName { get; set; }

        [StringLength(30)]
        public string MotherName { get; set; }

        [StringLength(30)]
        public string NationalId { get; set; }

        public int? Nationality { get; set; }

        public int? Gender { get; set; }

        public int? Religion { get; set; }

        public int? MaritalStatus { get; set; }

        [StringLength(20)]
        public string Mobile { get; set; }

        [StringLength(20)]
        public string Phone { get; set; }

        [StringLength(100)]

        public string Email { get; set; }

        [StringLength(20)]
        public string PresHoldingNo { get; set; }

        [StringLength(50)]
        public string PresStreet { get; set; }

        [StringLength(50)]
        public string PRES_AREA { get; set; }

        public int? PRES_POLICE_STATION_ID { get; set; }

        [StringLength(10)]
        public string PRES_POST_CODE { get; set; }

        public int? PRES_CITY_ID { get; set; }

        public int? PRES_DIVISION_ID { get; set; }

        public int? PRES_COUNTRY_ID { get; set; }

        [StringLength(20)]
        public string PERM_HOLDING_NO { get; set; }

        [StringLength(50)]
        public string PERM_STREET { get; set; }

        [StringLength(50)]
        public string PERM_AREA { get; set; }

        public int? PERM_POLICE_STATION_ID { get; set; }

        [StringLength(10)]
        public string PERM_POST_CODE { get; set; }

        public int? PERM_CITY_ID { get; set; }

        public int? PERM_DIVISION_ID { get; set; }

        public int? PERM_COUNTRY_ID { get; set; }

        [StringLength(500)]
        public string CAREER_OBJECTIVE { get; set; }

        public double? PRESENT_SALARY { get; set; }

        public double? EXPECTED_SALARY { get; set; }

        public int? JOB_LEVEL { get; set; }

        public int? JOB_NATURE { get; set; }

        [StringLength(250)]
        public string CAREER_SUMMERY { get; set; }

        [StringLength(250)]
        public string SPACIAL_QUALIFICATION { get; set; }

        [StringLength(50)]
        public string KEYWORDS { get; set; }

        [StringLength(200)]
        public string CV_LOCATION { get; set; }

        [StringLength(200)]
        public string IMAGE_LOCATION { get; set; }


        public string PERMANENT_OFFICE_PHONE { get; set; }
        public string PERMANENT_EMAIL { get; set; }

        //**********************************************Employment Hisory**************************************

        public string COMPANY_NAME { get; set; }
        public string COMPANY_BUSINESS { get; set; }

        public string COMPANY_LOCATION { get; set; }

        public string DEPARTMENT { get; set; }
        public string POSITION_HELD { get; set; }
        public string RESPONSIBILITIES { get; set; }

        public int? EXPERIENCE_CATEGORY_ID { get; set; }

        public int? SKILLS_ID { get; set; }
    }
}
