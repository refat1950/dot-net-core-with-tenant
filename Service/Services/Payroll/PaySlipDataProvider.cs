﻿using data.UnitOfWork;
using Entity.Model;
using Repository.Context;
using Repository.Utility;
using Service.DataService;
using Service.Helper;
using Service.Services.Interfaces;
using Service.ViewModel;
using Service.ViewModel.Payroll;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Services.Payroll
{
    public class PaySlipDataProvider : IPaySlipDataProvider
    {
        private IUnitOfWork unitOfWork;
        private PayrollBillPeriodService billPeriodService;
        private IEmployeeService employeeService;
        public PaySlipDataProvider(IUnitOfWork unitOfWork, PayrollBillPeriodService billPeriodService, IEmployeeService employeeService)
        {
            this.employeeService = employeeService;
            this.billPeriodService = billPeriodService;
            this.unitOfWork = unitOfWork;
        }
        
        public VmPaySlip GetPaySlipData(int empHeaderId, int periodId, int positionid, int payrollid)
        {
            var empInfo = unitOfWork.EmployeeRepo.FirstOrDefault(w => w.EmpHeaderId == empHeaderId);
            var empPayHead = unitOfWork.HrEmployeePayHeadsRepo.Find(w => w.EmpHeaderId == empHeaderId).ToList();

            var positionName = unitOfWork.HrPositionsRepo.FirstOrDefault(f => f.PostionId == positionid) != null ? unitOfWork.HrPositionsRepo.FirstOrDefault(f => f.PostionId == positionid).PostionName : "";
            var period = unitOfWork.PeriodRepo.Get(periodId);
            var periodName = period != null ? period.PeriodName : "";
            var periodStart = period != null ? period.StartDate.Value.ToString("dd/MM/yyy") : "";
            var periodEnd = period != null ? period.EndDate.Value.ToString("dd/MM/yyy") : "";

            var masterdata = unitOfWork.HrPaySlipTransactionMastersRepo.FirstOrDefault(s => s.EmpHeaderId == empHeaderId && s.PeriodId == periodId && s.PositionId == positionid && s.PayrollId == payrollid);

            var timingFee = unitOfWork.HrPayHeadsRepo.Find(s => s.PayHeadShortValue == "lf" || s.PayHeadShortValue == "eo" || s.PayHeadShortValue == "ot").Select(s => s.PayHeadId);

            List<VmPayHeadDataList> PayHeadlist = new List<VmPayHeadDataList>();
            if (masterdata != null)
            {
                //current payslip data
                foreach (var eachTransaction in masterdata.HrPaySlipTransactions)
                {
                    var payhead = unitOfWork.HrPayHeadsRepo.FirstOrDefault(f => f.PayHeadId == eachTransaction.PayHeadId);
                    if (payhead != null)
                    {
                        var payheadType = Convert.ToInt32(!String.IsNullOrEmpty(eachTransaction.Attribute3) ? eachTransaction.Attribute3 : "0");
                        VmPayHeadDataList vmPayHeadDataList = new VmPayHeadDataList();
                        vmPayHeadDataList.PayHeadId = eachTransaction.PayHeadId;
                        vmPayHeadDataList.PayHeadName = payhead.PayHeadName;
                        vmPayHeadDataList.PayHeadType = payheadType == (int)PayHeadType.Addition ? PayHeadType.Addition.ToString() : payheadType == (int)PayHeadType.Deduction ? PayHeadType.Deduction.ToString() : "";
                        vmPayHeadDataList.Unit = eachTransaction.Unit;
                        vmPayHeadDataList.CalculatedValue = eachTransaction.CalculatedValue;
                        vmPayHeadDataList.IsPercentage = eachTransaction.IsPercentage;
                        vmPayHeadDataList.IsTimingFees = timingFee.Contains(eachTransaction.PayHeadId) ? true : false;
                        PayHeadlist.Add(vmPayHeadDataList);
                    }

                }
                VmPaySlip vmPaySlip = new VmPaySlip();
                vmPaySlip.EmpHeaderId = empInfo.EmpHeaderId;
                vmPaySlip.EmpId = empInfo.EmpId;
                vmPaySlip.EmpName = empInfo.EmpFirstName + " " + empInfo.EmpMiddleName + " " + empInfo.EmpLastName;
                vmPaySlip.Email = empInfo.Email;
                vmPaySlip.PositionId = positionid;
                vmPaySlip.PositionName = positionName;
                vmPaySlip.PayrollId = payrollid;
                vmPaySlip.EmpMobileNumber = empInfo.Mobile;
                vmPaySlip.JoinningDate = empInfo.JoinningDate.ToString("dd/MM/yyy");
                vmPaySlip.PeriodName = periodName;
                vmPaySlip.StartDate = periodStart;
                vmPaySlip.EndDate = periodEnd;
                vmPaySlip.PaySlipId = masterdata.PayslipId != null ? masterdata.PayslipId : "";
                vmPaySlip.vmPayHeadDataList = PayHeadlist;
                vmPaySlip.PeriodId = periodId;
                vmPaySlip.PaidStatus = masterdata.PaidStatus;
                vmPaySlip.TotalAmountPayable = masterdata.TotalAmountPayable;
                vmPaySlip.TotalAdditionAmount = masterdata.TotalAdditionAmount;
                vmPaySlip.TotalDeductionAmount = masterdata.TotalDeductionAmount;

                return vmPaySlip;
            }

            return new VmPaySlip();
        }

        //with ado.net
        //public VmPeriodList GetViewMoreSectionData(VmPayrollPositionWisePeriodList period, int empHeaderId,string connectionString,int? branchId)
        //{

        //    VmPeriodList vmPeriodList = new VmPeriodList();
        //    var PeriodName = "";
        //    var PositionName = "";
        //    decimal totalpaidAmount = 0;
        //    string connString = ConfigurationManager.ConnectionStrings[connectionString].ConnectionString;
        //    SqlConnection conn = new SqlConnection(connString);
        //    conn.Open();
        //    SqlDataReader thisBillPeriod = new SqlCommand("Select MonthName From BillPeriods where BillPeriodId = " + period.PeriodId + " and BranchId = " + branchId + ";", conn).ExecuteReader();
        //    SqlDataReader thisPositionData = new SqlCommand("Select PostionName From HrPositions where PostionId = " + period.PositionId + " and BranchId = " + branchId + ";", conn).ExecuteReader();
        //    SqlDataReader masterData = new SqlCommand("Select * From HrPaySlipTransactionMasters where PeriodId = " + period.PeriodId + " and EmpHeaderId = " + empHeaderId + " and PositionId = " + period.PositionId + " and PayrollId = " + period.PayrollId + " and BranchId = " + branchId + ";", conn).ExecuteReader();

        //    while (thisBillPeriod.Read())
        //        PeriodName = thisBillPeriod["Monthname"].ToString();
        //    while (thisPositionData.Read())
        //        PositionName = thisPositionData["PostionName"].ToString();
        //    vmPeriodList.Id = period.PeriodId;
        //    vmPeriodList.Name = PeriodName + " (" + PositionName + ")";

        //    var masterdata = unitOfWork.HrPaySlipTransactionMastersRepo.FirstOrDefault(s => s.PeriodId == period.PeriodId && s.EmpHeaderId == empHeaderId && s.PositionId == period.PositionId && s.PayrollId == period.PayrollId);
        //    var masterHeadID = 0;
        //    while (masterData.Read())
        //    {
        //        masterHeadID = Convert.ToInt16(masterData["PaySlipMasterHeaderId"]);
        //        SqlDataReader totalPaidAmount = new SqlCommand("Select sum(PaidAmount) as PaidAmount From HrPaySlipPayLogs where PaySlipMasterHeaderId = " + masterHeadID + ";", conn).ExecuteReader();
        //        while (totalPaidAmount.Read())
        //            totalpaidAmount = totalPaidAmount["PaidAmount"].ToString() != "" ?  Convert.ToDecimal(totalPaidAmount["PaidAmount"]) : 0;
        //        var totalAmountPayable = Convert.ToDecimal(masterData["TotalAmountPayable"]);
        //        vmPeriodList.PositionId = period.PositionId;
        //        vmPeriodList.PayrollId = period.PayrollId;
        //        vmPeriodList.TotalAmountPayable = totalAmountPayable;
        //        vmPeriodList.TotalPaidAmount = totalpaidAmount;
        //        vmPeriodList.GenerateStatus = (PayslipCurrentStatus)masterData["PayslipCurrentStatus"];
        //        vmPeriodList.GenerateStatusName = vmPeriodList.GenerateStatus.ToString();
        //        vmPeriodList.GenerationDate = Convert.ToDateTime(masterData["GenerationDate"]).ToString("dd MMMM yyyy");
        //        vmPeriodList.LastUpdateDate = masterData["LastUpdateDate"].ToString() != "" ? Convert.ToDateTime(masterData["LastUpdateDate"]).ToString("dd MMMM yyyy") : "";
                
        //        SqlDataReader paidRows = new SqlCommand("Select PaidAmount,PaidDate From HrPaySlipPayLogs where PaySlipMasterHeaderId = " + masterHeadID + " order by HrPaySlipPayLogId DESC;", conn).ExecuteReader();

        //        if ((PaidStatus)masterData["PaidStatus"] == PaidStatus.PartiallyPaid)
        //        {
        //            vmPeriodList.PaidStatus = PaidStatus.PartiallyPaid;
        //            var paidList = new List<VmParciallyPaidList>();
        //            while (paidRows.Read())
        //            {
        //                var vm = new VmParciallyPaidList();
        //                vm.Amount = Convert.ToDecimal(paidRows["PaidAmount"]);
        //                vm.PaidDate = Convert.ToDateTime(paidRows["PaidDate"]).ToString("dd MMMM yyyy");
        //                paidList.Add(vm);
        //            }
        //            vmPeriodList.vmParciallyPaidLists = paidList;
        //        }
        //        else if ((PaidStatus)masterData["PaidStatus"] == PaidStatus.FullyPaid)
        //        {
        //            vmPeriodList.PaidStatus = PaidStatus.FullyPaid;
        //            while (paidRows.Read())
        //                vmPeriodList.PaidDate = Convert.ToDateTime(paidRows["PaidDate"]).ToString("dd MMMM yyyy"); // if fully paid there will be one paid row
        //        }
        //        else if ((PaidStatus)masterData["PaidStatus"] == PaidStatus.UnPaid)
        //        {
        //            vmPeriodList.PaidStatus = PaidStatus.UnPaid;
        //        }

        //        // permission for update payslip
        //        var timingFee = unitOfWork.HrPayHeadsRepo.Find(s => s.PayHeadShortValue == "lf" || s.PayHeadShortValue == "eo" || s.PayHeadShortValue == "ot").Select(s => s.PayHeadId);
        //        if ((vmPeriodList.GenerateStatus == PayslipCurrentStatus.Generated || vmPeriodList.GenerateStatus == PayslipCurrentStatus.ReGenerated || vmPeriodList.GenerateStatus == PayslipCurrentStatus.Updated) && (PaidStatus)masterData["PaidStatus"] == PaidStatus.PartiallyPaid && totalAmountPayable > totalpaidAmount)
        //        {
        //            vmPeriodList.IsUpdateable = masterdata.HrPaySlipTransactions.Where(s => timingFee.Contains(s.PayHeadId)).Any();
        //        }
        //    }
        //    if (masterHeadID == 0)
        //    {
        //        vmPeriodList.IsUpdateable = false;
        //        vmPeriodList.TotalAmountPayable = 0;
        //        vmPeriodList.TotalPaidAmount = 0;
        //        vmPeriodList.PaidStatus = PaidStatus.UnPaid;
        //        vmPeriodList.GenerateStatus = PayslipCurrentStatus.NotGenerated; // not generated
        //        vmPeriodList.GenerateStatusName = PayslipCurrentStatus.NotGenerated.ToString();
        //    }

        //    //if (masterdata != null) // if this pay slip generated for this employee
        //    //{

        //    //}
        //    //else
        //    //{
        //    //    vmPeriodList.IsUpdateable = false;
        //    //    vmPeriodList.TotalAmountPayable = 0;
        //    //    vmPeriodList.TotalPaidAmount = 0;
        //    //    vmPeriodList.PaidStatus = PaidStatus.UnPaid;
        //    //    vmPeriodList.GenerateStatus = PayslipCurrentStatus.NotGenerated; // not generated
        //    //    vmPeriodList.GenerateStatusName = PayslipCurrentStatus.NotGenerated.ToString();
        //    //}
        //    conn.Close();
        //    return vmPeriodList;
        //}
        public VmPeriodList GetViewMoreSectionData(List<HrPaySlipTransactionMaster> masterData, VmPayrollPositionWisePeriodList eachPeriod, List<VmBillPeriodsData> billPeriods, List<VmHrPositionData> positionData, int serial,List<int> timingFee)
        {
            VmPeriodList vmPeriodList = new VmPeriodList();
            try
            {
                var positionName = positionData.FirstOrDefault(s => s.PostionId == eachPeriod.PositionId) != null ? positionData.FirstOrDefault(s => s.PostionId == eachPeriod.PositionId).PostionName : "";
                vmPeriodList.SerialReverse = serial;
                vmPeriodList.Id = eachPeriod.PeriodId;
                vmPeriodList.Name = billPeriods.FirstOrDefault(s => s.BillPeriodId == eachPeriod.PeriodId) != null ? billPeriods.FirstOrDefault(s => s.BillPeriodId == eachPeriod.PeriodId).MonthName + " (" + positionName + ")" : "";

                var masterdata = masterData.FirstOrDefault(s => s.PeriodId == eachPeriod.PeriodId && s.PositionId == eachPeriod.PositionId && s.PayrollId == eachPeriod.PayrollId);

                if (masterdata != null) // if this pay slip generated for this employee
                {
                    var totalpaidAmount = masterdata.HrPaySlipPayLogs.Where(w => w.Attribute1 == null).Select(s => s.PaidAmount).Sum();
                    var totalAmountPayable = masterdata.TotalAmountPayable;
                    vmPeriodList.PositionId = eachPeriod.PositionId;
                    vmPeriodList.PayrollId = eachPeriod.PayrollId;
                    vmPeriodList.TotalAmountPayable = totalAmountPayable;
                    vmPeriodList.TotalPaidAmount = totalpaidAmount;
                    vmPeriodList.GenerateStatus = masterdata.PayslipCurrentStatus;
                    vmPeriodList.GenerateStatusName = masterdata.PayslipCurrentStatus.ToString();
                    vmPeriodList.GenerationDate = masterdata.GenerationDate != null ? masterdata.GenerationDate.ToString("dd MMM yyyy") : "";
                    vmPeriodList.LastUpdateDate = masterdata.LastUpdateDate != null ? masterdata.LastUpdateDate.Value.ToString("dd MMMM yyyy") : "";
                    if (masterdata.PaidStatus == PaidStatus.PartiallyPaid)
                    {
                        vmPeriodList.PaidStatus = PaidStatus.PartiallyPaid;
                        vmPeriodList.vmParciallyPaidLists = masterdata.HrPaySlipPayLogs.Where(w => w.Attribute1 == null).Select(s => new { s.HrPaySlipPayLogId, s.PaidAmount, s.PaidDate }).OrderByDescending(s => s.HrPaySlipPayLogId).Select(ss => new VmParciallyPaidList
                        {
                            Amount = ss.PaidAmount,
                            PaidDate = ss.PaidDate.ToString("dd MMMM yyyy")
                        }).ToList();
                    }
                    else if (masterdata.PaidStatus == PaidStatus.FullyPaid)
                    {
                        vmPeriodList.PaidStatus = PaidStatus.FullyPaid;
                        vmPeriodList.PaidDate = masterdata.HrPaySlipPayLogs.FirstOrDefault() != null ? masterdata.HrPaySlipPayLogs.FirstOrDefault().PaidDate.ToString("dd MMMM yyyy") : ""; // if fully paid there will be one paid row
                    }
                    else if (masterdata.PaidStatus == PaidStatus.UnPaid)
                    {
                        vmPeriodList.PaidStatus = PaidStatus.UnPaid;
                    }

                    // permission for update payslip
                    if ((masterdata.PayslipCurrentStatus == PayslipCurrentStatus.Generated || masterdata.PayslipCurrentStatus == PayslipCurrentStatus.ReGenerated || masterdata.PayslipCurrentStatus == PayslipCurrentStatus.Updated) && masterdata.PaidStatus == PaidStatus.PartiallyPaid /*&& totalAmountPayable > totalpaidAmount*/)
                    {
                        vmPeriodList.IsUpdateable = masterdata.HrPaySlipTransactions.Where(s => timingFee.Contains(s.PayHeadId)).Any();
                    }
                }
                else
                {
                    vmPeriodList.IsUpdateable = false;
                    vmPeriodList.TotalAmountPayable = 0;
                    vmPeriodList.TotalPaidAmount = 0;
                    vmPeriodList.PaidStatus = PaidStatus.UnPaid;
                    vmPeriodList.GenerateStatus = PayslipCurrentStatus.NotGenerated; // not generated
                    vmPeriodList.GenerateStatusName = PayslipCurrentStatus.NotGenerated.ToString();
                }
            }
            catch (Exception ex) { }
            return vmPeriodList;
        }

        public List<VmEmpoyeePeriod> GetEmployesAllPeriodsPayslipData(List<int> EmpIds, EmpStatus status)
        {
            List<VmEmpoyeePeriod> vmEmpoyeePeriodsList = new List<VmEmpoyeePeriod>();
            var billPeriods = unitOfWork.PeriodRepo.GetAllQueryable().Select(s => new VmBillPeriodsData
            {
                BillPeriodId = s.PeriodId,
                MonthName = s.PeriodName,
                PeriodTypeId = s.PeriodTypeId,
                StartDate = s.StartDate,
                EndDate = s.EndDate,
                IsVisible = s.IsVisible
            }).ToList();

            var positionData = unitOfWork.HrPositionsRepo.GetAllQueryable().Select(s => new VmHrPositionData
            {
                PostionId = s.PostionId,
                PostionName = s.PostionName,
                PayrollId = s.PayrollId
            }).ToList();

            var payrollData = unitOfWork.HrPayRollsRepo.GetAll().ToList();
            var rejoinRepo = unitOfWork.RejoinRepo.GetAllQueryable().Select(s => new VmEmpRejoinData { RejoinId = s.RejoinId, EmpHeaderId = s.EmpHeaderId, RejoinDate = s.RejoinDate }).OrderByDescending(o => o.RejoinId).ToList();
            var resignRepo = unitOfWork.ResignationRepo.GetAll().Select(s => new VmEmpResignData
            {
                ResignationId = s.ResignationId,
                EmployeeHeaderId = s.EmployeeHeaderId,
                ResignDate = s.ResignDate
            }).OrderByDescending(s => s.ResignationId).ToList();
            var timingFee = unitOfWork.HrPayHeadsRepo.Find(s => s.PayHeadShortValue == "lf" || s.PayHeadShortValue == "eo" || s.PayHeadShortValue == "ot").Select(s => s.PayHeadId).ToList();
            var payrollMigratePeriodflexRepo = unitOfWork.FlexSetRepo.FirstOrDefault(s => s.FlexValueShortName == "pmp");
            var payrollMigratePeriodflexRepoChields = payrollMigratePeriodflexRepo.FndFlexValue.ToList();
            if (EmpIds.Any())
            {
                var allEmployees = new List<VmEmployeeData>();
                if (status == EmpStatus.Active)
                    allEmployees = employeeService.GetActiveEmployees().Where(w=> EmpIds.Contains(w.EmpHeaderId)).Select(s => new VmEmployeeData { EmpHeaderId = s.EmpHeaderId, EmpName = s.EmpFirstName + " " + s.EmpMiddleName + " " + s.EmpLastName, PositionId = s.PositionId, JoiningDate = s.JoinningDate,EmployeeJoiningStatus = employeeService.GetEmployeeCurrentStatus(s.EmpHeaderId) }).ToList();
                else if (status == EmpStatus.Left)
                    allEmployees = employeeService.GetLeftEmployees().Where(w => EmpIds.Contains(w.EmpHeaderId)).Select(s => new VmEmployeeData { EmpHeaderId = s.EmpHeaderId, EmpName = s.EmpFirstName + " " + s.EmpMiddleName + " " + s.EmpLastName, PositionId = s.PositionId, JoiningDate = s.JoinningDate, EmployeeJoiningStatus = employeeService.GetEmployeeCurrentStatus(s.EmpHeaderId) }).ToList();
                else if (status == EmpStatus.All)
                {
                    allEmployees = unitOfWork.EmployeeRepo.Find(w => EmpIds.Contains(w.EmpHeaderId)).Select(s => new VmEmployeeData { EmpHeaderId = s.EmpHeaderId, EmpName = s.EmpFirstName + " " + s.EmpMiddleName + " " + s.EmpLastName, PositionId = s.PositionId, JoiningDate = s.JoinningDate, EmployeeJoiningStatus = employeeService.GetEmployeeCurrentStatus(s.EmpHeaderId) }).ToList();
                }

                var empIds = allEmployees.Select(ss => ss.EmpHeaderId).ToList();
                var masterData = unitOfWork.HrPaySlipTransactionMastersRepo.Find(s => empIds.Contains(s.EmpHeaderId))
                .Select(s => new HrPaySlipTransactionMaster
                {
                    EmpHeaderId = s.EmpHeaderId,
                    PeriodId = s.PeriodId,
                    PositionId = s.PositionId,
                    PayrollId = s.PayrollId,
                    HrPaySlipPayLogs = s.HrPaySlipPayLogs,
                    TotalAmountPayable = s.TotalAmountPayable,
                    PayslipCurrentStatus = s.PayslipCurrentStatus,
                    CreationDate = s.CreationDate,
                    LastUpdateDate = s.LastUpdateDate,
                    PaidStatus = s.PaidStatus,
                    HrPaySlipTransactions = s.HrPaySlipTransactions,
                    GenerationDate = s.GenerationDate
                }).ToList();
                foreach (var customEmpInfo in allEmployees)
                {
                    try
                    {
                        var empPeriodsData = GetEmployeePeriodsData(customEmpInfo, masterData, positionData, rejoinRepo, resignRepo,  billPeriods, payrollData, payrollMigratePeriodflexRepoChields, timingFee);
                        vmEmpoyeePeriodsList.Add(empPeriodsData);
                    }
                    catch (Exception ex) { }
                }
            }

            return vmEmpoyeePeriodsList;
        }

        public VmEmpoyeePeriod GetEmployeeBillPeriods(int EmpHeaderId)
        {
            var empIdList = new List<int>();
            empIdList.Add(EmpHeaderId);
            return GetEmployesAllPeriodsPayslipData(empIdList, EmpStatus.All).FirstOrDefault();
        }

        public VmEmpoyeePeriod GetEmployeePeriodsData(VmEmployeeData customEmpInfo,List<HrPaySlipTransactionMaster> hrPaySlipTransactionMasters, List<VmHrPositionData> positionData, List<VmEmpRejoinData> rejoinRepo , List<VmEmpResignData> resignRepo, List<VmBillPeriodsData> billPeriods,List<HrPayRoll> payrollData, List<FndFlexValue> payrollMigratePeriodflexRepoChields, List<int> timingFee)
        {
            VmEmpoyeePeriod vmEmpoyeePeriod = new VmEmpoyeePeriod();
            vmEmpoyeePeriod.EmpId = customEmpInfo.EmpHeaderId;
            vmEmpoyeePeriod.EmpName = !string.IsNullOrEmpty(customEmpInfo.EmpName) ? customEmpInfo.EmpName : "";
            var empCurrentStatus = customEmpInfo.EmployeeJoiningStatus;
            var thisEmpJoiningDate = customEmpInfo.JoiningDate != null ? customEmpInfo.JoiningDate : BdDateTime.Now();

            var PeriodsTobeAppend = new List<VmPayrollPositionWisePeriodList>();
            List<VmPeriodList> periodDataList = new List<VmPeriodList>();
            var masterData = hrPaySlipTransactionMasters.Where(s => s.EmpHeaderId == customEmpInfo.EmpHeaderId).ToList();

            // all generated Periods
            PeriodsTobeAppend = masterData.Select(data => new VmPayrollPositionWisePeriodList
            {
                PeriodId = data.PeriodId,
                PositionId = data.PositionId,
                PayrollId = data.PayrollId,
            }).ToList();
            //if (masterData.Any())
            //{
            //    // all generated Periods
            //    foreach (var data in masterData)
            //    {
            //        try
            //        {
            //            if (data != null)
            //            {
            //                var periodListForAppend = new VmPayrollPositionWisePeriodList
            //                {
            //                    PeriodId = data.PeriodId,
            //                    PositionId = data.PositionId,
            //                    PayrollId = data.PayrollId,
            //                };
            //                PeriodsTobeAppend.Add(periodListForAppend);
            //            }
            //        }
            //        catch (Exception ex) { }
            //    }
            //}
            if (customEmpInfo.PositionId != null && customEmpInfo.PositionId > 0)// position assigned
            {
                var currentPosition = positionData.FirstOrDefault(s => s.PostionId == customEmpInfo.PositionId);
                if (masterData.Any()) // if any transaction exist
                {
                    //append new not_generated period after last position's generated periods
                    if (PeriodsTobeAppend.Any())
                    {
                        var previousGeneratedPeriods = PeriodsTobeAppend.Select(s => s.PeriodId).ToList();
                        var lastPeriodId = billPeriodService.GetBiggestPeriodByPeriodId(billPeriods, previousGeneratedPeriods).BillPeriodId;
                        var lastGeneratedPositionId = PeriodsTobeAppend.Last().PositionId;
                        var lastGeneratedPayrollId = PeriodsTobeAppend.Last().PayrollId;

                        var employeeLastPositionFromTransection = unitOfWork.HrEmployeeTransactionRepo.Find(s => s.EmpHeaderId == customEmpInfo.EmpHeaderId && s.PositionId != null).OrderByDescending(s => s.TransactionId).Select(s => s.PositionId).FirstOrDefault();

                        // joining, left & rejoin date
                        var empJoiningdate = new DateTime(thisEmpJoiningDate.Year, thisEmpJoiningDate.Month, 1);

                        var empRejoiningFullDate = rejoinRepo.FirstOrDefault(f => f.EmpHeaderId == customEmpInfo.EmpHeaderId) != null ? rejoinRepo.FirstOrDefault(f => f.EmpHeaderId == customEmpInfo.EmpHeaderId).RejoinDate : new DateTime();
                        var empRejoiningDate = new DateTime(empRejoiningFullDate.Year, empRejoiningFullDate.Month, 1);

                        var empleftFullDate = resignRepo.FirstOrDefault(f => f.EmployeeHeaderId == customEmpInfo.EmpHeaderId) != null ? resignRepo.FirstOrDefault(f => f.EmployeeHeaderId == customEmpInfo.EmpHeaderId).ResignDate : new DateTime();
                        var empleftDate = new DateTime(empleftFullDate.Year, empleftFullDate.Month, 1);

                        if (employeeLastPositionFromTransection != null && currentPosition != null)
                        {
                            //if position changed so new position's payroll's periods will be add after last position's generated period
                            //or if position's payroll changed so last position's new payroll's periods will be add after last position's generated period
                            if (employeeLastPositionFromTransection != lastGeneratedPositionId || currentPosition.PayrollId != lastGeneratedPayrollId)
                            {
                                var payrollTypeId = billPeriodService.GetPayrollTypeIdByPayrollId(billPeriods,currentPosition.PayrollId);

                                if (payrollTypeId == BillPeriodType.Weekly)
                                {
                                    empJoiningdate = thisEmpJoiningDate.Date;
                                    empRejoiningDate = empRejoiningFullDate.Date;
                                    empleftDate = empleftFullDate.Date;
                                }

                                var thisPayrollAllPeriods = billPeriods.Where(w => w.PeriodTypeId == payrollTypeId).ToList();
                                var lastGeneratedPeriodInfo = billPeriods.FirstOrDefault(f => f.BillPeriodId == lastPeriodId);
                                    //billPeriodService.GetPeriodById(lastPeriodId);
                                if (lastGeneratedPeriodInfo != null && lastGeneratedPeriodInfo.EndDate != null)
                                {
                                    var count = 0;
                                    var lastGeneratedPeriodEndingDate = lastGeneratedPeriodInfo.EndDate.Value.Date;
                                    var position = positionData.FirstOrDefault(s => s.PostionId == customEmpInfo.PositionId);
                                    var payroll = payrollData.FirstOrDefault(s => s.PayRollId == currentPosition.PayrollId);
                                    foreach (var period in thisPayrollAllPeriods)
                                    {
                                        try
                                        {
                                            if (period != null && period.StartDate != null && period.EndDate != null)
                                            {
                                                var nextPeriodEndingDate = payrollTypeId != BillPeriodType.Weekly ? new DateTime(period.EndDate.Value.Year, period.EndDate.Value.Month, 1) : period.EndDate.Value.Date;

                                                var isAllowedForNextPeriod = empCurrentStatus == EmployeeJoiningStatus.Joined ?
                                                (nextPeriodEndingDate > lastGeneratedPeriodEndingDate && empJoiningdate <= nextPeriodEndingDate && period.IsVisible == true)
                                                    : empCurrentStatus == EmployeeJoiningStatus.Left ?
                                                // don't check period active during left, so suggest period by force
                                                (nextPeriodEndingDate > lastGeneratedPeriodEndingDate && empleftDate >= nextPeriodEndingDate)
                                                    : empCurrentStatus == EmployeeJoiningStatus.Rejoined ?
                                                (nextPeriodEndingDate > lastGeneratedPeriodEndingDate && empleftDate <= nextPeriodEndingDate && empRejoiningDate <= nextPeriodEndingDate && period.IsVisible == true)
                                                    : false;

                                                if (isAllowedForNextPeriod)
                                                {
                                                    count++;
                                                    if (count == 1) // suggest only this new 1st period
                                                    {
                                                        vmEmpoyeePeriod.PayslipPeriodSettings = (int)PayslipPeriodSettings.PeriodSuggestible;
                                                        vmEmpoyeePeriod.PeriodId = period.BillPeriodId;
                                                        vmEmpoyeePeriod.PeriodName = period.MonthName;
                                                        vmEmpoyeePeriod.PositionId = position != null ? position.PostionId : 0;
                                                        vmEmpoyeePeriod.PositionName = position != null ? position.PostionName : "";
                                                        vmEmpoyeePeriod.PayrollId = payroll != null ? payroll.PayRollId : 0;
                                                        vmEmpoyeePeriod.PayrollName = payroll != null ? payroll.PayRollName : "";
                                                    }

                                                    var periodListForAppend = new VmPayrollPositionWisePeriodList();
                                                    periodListForAppend.PeriodId = period.BillPeriodId;
                                                    periodListForAppend.PositionId = customEmpInfo.PositionId ?? 0;
                                                    periodListForAppend.PayrollId = currentPosition.PayrollId;
                                                    PeriodsTobeAppend.Add(periodListForAppend);
                                                }
                                            }
                                        }
                                        catch (Exception ex) { }
                                    }
                                }
                            }
                            else // position not changed & position's payroll not changed, so add last position's new periods after last position's generated period
                            {
                                var count = 0;
                                var firstPeriodNotAdded = true;
                                var payroll = payrollData.FirstOrDefault(s => s.PayRollId == lastGeneratedPayrollId);
                                var position = positionData.FirstOrDefault(s => s.PostionId == lastGeneratedPositionId);
                                var lastGeneratedPeriodEndingDate = billPeriods.FirstOrDefault(f => f.BillPeriodId == lastPeriodId).EndDate.Value.Date;
                                    //billPeriodService.GetPeriodById(lastPeriodId).EndDate.Value.Date;

                                var payrollTypeId = billPeriodService.GetPayrollTypeIdByPayrollId(billPeriods,payroll.PayRollId);
                                if (payrollTypeId == BillPeriodType.Weekly)
                                {
                                    empJoiningdate = thisEmpJoiningDate.Date;
                                    empRejoiningDate = empRejoiningFullDate.Date;
                                    empleftDate = empleftFullDate.Date;
                                }
                                var nextPeriod = new VmBillPeriodsData();
                                while (true)
                                {
                                    try
                                    {
                                        nextPeriod = billPeriodService.GetNextPeriod(billPeriods,lastPeriodId);
                                        if (nextPeriod != null)
                                        {
                                            var nextPeriodEndingDate = payrollTypeId != BillPeriodType.Weekly ? new DateTime(nextPeriod.EndDate.Value.Year, nextPeriod.EndDate.Value.Month, 1) : nextPeriod.EndDate.Value.Date;

                                            var isAllowedForNextPeriod = empCurrentStatus == EmployeeJoiningStatus.Joined ?
                                                (nextPeriodEndingDate > lastGeneratedPeriodEndingDate && empJoiningdate <= nextPeriodEndingDate && nextPeriod.IsVisible == true)
                                                : empCurrentStatus == EmployeeJoiningStatus.Left ?
                                                    // don't check period active during left, so suggest period by force
                                                    (nextPeriodEndingDate > lastGeneratedPeriodEndingDate && empleftDate >= nextPeriodEndingDate)
                                                : empCurrentStatus == EmployeeJoiningStatus.Rejoined ?
                                                    (nextPeriodEndingDate > lastGeneratedPeriodEndingDate && empleftDate <= nextPeriodEndingDate && empRejoiningDate <= nextPeriodEndingDate && nextPeriod.IsVisible == true)
                                                : false;

                                            if (isAllowedForNextPeriod)
                                            {
                                                count++;
                                                if (count == 1) // suggest only this 1st period
                                                {
                                                    firstPeriodNotAdded = false;
                                                    var period = billPeriods.FirstOrDefault(f => f.BillPeriodId == nextPeriod.BillPeriodId);
                                                        //billPeriodService.GetPeriodById(nextPeriod.BillPeriodId);
                                                    vmEmpoyeePeriod.PayslipPeriodSettings = (int)PayslipPeriodSettings.PeriodSuggestible;
                                                    vmEmpoyeePeriod.PeriodId = nextPeriod.BillPeriodId;
                                                    vmEmpoyeePeriod.PeriodName = period.MonthName;
                                                    vmEmpoyeePeriod.PositionId = position != null ? position.PostionId : 0;
                                                    vmEmpoyeePeriod.PositionName = position != null ? position.PostionName : "";
                                                    vmEmpoyeePeriod.PayrollId = payroll != null ? payroll.PayRollId : 0;
                                                    vmEmpoyeePeriod.PayrollName = payroll != null ? payroll.PayRollName : "";
                                                }

                                                var periodListForAppend = new VmPayrollPositionWisePeriodList();
                                                periodListForAppend.PeriodId = nextPeriod.BillPeriodId;
                                                periodListForAppend.PositionId = lastGeneratedPositionId;
                                                periodListForAppend.PayrollId = lastGeneratedPayrollId;
                                                PeriodsTobeAppend.Add(periodListForAppend);
                                                lastPeriodId = nextPeriod.BillPeriodId;
                                            }
                                        }
                                        else
                                        {
                                            if (firstPeriodNotAdded)
                                            {
                                                vmEmpoyeePeriod.PayslipPeriodSettings = (int)PayslipPeriodSettings.AllPayslipGenerateComplete;
                                                vmEmpoyeePeriod.PeriodId = 0;
                                                vmEmpoyeePeriod.PeriodName = "AllPeriodGenerateComplete";
                                                vmEmpoyeePeriod.PositionId = position != null ? position.PostionId : 0;
                                                vmEmpoyeePeriod.PositionName = position != null ? position.PostionName : "";
                                                vmEmpoyeePeriod.PayrollId = payroll != null ? payroll.PayRollId : 0;
                                                vmEmpoyeePeriod.PayrollName = payroll != null ? payroll.PayRollName : "";
                                            }
                                            break;
                                        }
                                        lastPeriodId = nextPeriod.BillPeriodId;
                                    }
                                    catch (Exception ex) { }
                                }
                            }
                        }
                    }
                }
                else //1st transaction, so bind all periods of current payroll
                {
                    if (currentPosition != null)
                    {
                        var payrollTypeId = billPeriodService.GetPayrollTypeIdByPayrollId(billPeriods,currentPosition.PayrollId);
                        var thisPayrollAllPeriods = billPeriods.Where(w => w.PeriodTypeId == payrollTypeId && w.IsVisible == true).ToList();
                        var payroll = payrollData.FirstOrDefault(s => s.PayRollId == currentPosition.PayrollId);
                        var position = positionData.FirstOrDefault(s => s.PostionId == customEmpInfo.PositionId);
                        var count = 0;

                        var migrationStartFromPeriodId = "0";
                        if (payrollMigratePeriodflexRepoChields.Any())
                        {
                            var thisPayrollTypeDefaultValue = new FndFlexValue();
                            if (payrollTypeId == BillPeriodType.Monthly)
                                thisPayrollTypeDefaultValue = payrollMigratePeriodflexRepoChields.FirstOrDefault(f => f.FlexShortValue == "monthlyPayroll");
                            else if (payrollTypeId == BillPeriodType.Annual)
                                thisPayrollTypeDefaultValue = payrollMigratePeriodflexRepoChields.FirstOrDefault(f => f.FlexShortValue == "annualPayroll");
                            else if (payrollTypeId == BillPeriodType.Bi_Annual)
                                thisPayrollTypeDefaultValue = payrollMigratePeriodflexRepoChields.FirstOrDefault(f => f.FlexShortValue == "bi_annualPayroll");
                            else if (payrollTypeId == BillPeriodType.Tri_Annual)
                                thisPayrollTypeDefaultValue = payrollMigratePeriodflexRepoChields.FirstOrDefault(f => f.FlexShortValue == "tri_annualPayroll");
                            else if (payrollTypeId == BillPeriodType.Quarterly)
                                thisPayrollTypeDefaultValue = payrollMigratePeriodflexRepoChields.FirstOrDefault(f => f.FlexShortValue == "quarterlyPayroll");
                            else if (payrollTypeId == BillPeriodType.One_Time)
                                thisPayrollTypeDefaultValue = payrollMigratePeriodflexRepoChields.FirstOrDefault(f => f.FlexShortValue == "oneTimePayroll");
                            else if (payrollTypeId == BillPeriodType.Weekly)
                                thisPayrollTypeDefaultValue = payrollMigratePeriodflexRepoChields.FirstOrDefault(f => f.FlexShortValue == "weeklyPayroll");
                            migrationStartFromPeriodId = thisPayrollTypeDefaultValue != null ? !String.IsNullOrEmpty(thisPayrollTypeDefaultValue.Attribute1) ? thisPayrollTypeDefaultValue.Attribute1 : "0" : "0";
                        }

                        var migrationStartFromEndDate = Convert.ToInt16(migrationStartFromPeriodId) != 0 ? billPeriodService.GetPeriodById(Convert.ToInt16(migrationStartFromPeriodId)).EndDate : new DateTime();

                        foreach (var thisPeriod in thisPayrollAllPeriods)
                        {
                            try
                            {
                                if (thisPeriod != null && thisPeriod.StartDate != null)
                                {
                                    // check if this period is bigger then joining date, check only month if its not weekly, else check full date

                                    var empJoiningdate = new DateTime(thisEmpJoiningDate.Year, thisEmpJoiningDate.Month, 1);
                                    var empRejoiningFullDate = rejoinRepo.FirstOrDefault(f => f.EmpHeaderId == customEmpInfo.EmpHeaderId) != null ? rejoinRepo.FirstOrDefault(f => f.EmpHeaderId == customEmpInfo.EmpHeaderId).RejoinDate : new DateTime();
                                    var empRejoiningDate = new DateTime(empRejoiningFullDate.Year, empRejoiningFullDate.Month, 1);
                                    var thisPeriodStart = new DateTime(thisPeriod.StartDate.Value.Year, thisPeriod.StartDate.Value.Month, 1);
                                    var thisPeriodEnd = new DateTime(thisPeriod.EndDate.Value.Year, thisPeriod.EndDate.Value.Month, 1);
                                    if (payrollTypeId == BillPeriodType.Weekly)
                                    {
                                        empJoiningdate = thisEmpJoiningDate.Date;
                                        empRejoiningDate = empRejoiningFullDate.Date;
                                        thisPeriodStart = thisPeriod.StartDate.Value.Date;
                                        thisPeriodEnd = thisPeriod.EndDate.Value.Date;
                                    }

                                    var isAllowedForNextPeriod = empCurrentStatus == EmployeeJoiningStatus.Joined ?
                                            (thisPeriodEnd >= empJoiningdate && thisPeriod.EndDate >= migrationStartFromEndDate)
                                        : empCurrentStatus == EmployeeJoiningStatus.Rejoined ?
                                            (thisPeriodEnd >= empRejoiningDate)
                                        : empCurrentStatus == EmployeeJoiningStatus.Left ?
                                            false :
                                        false;

                                    if (isAllowedForNextPeriod)
                                    {
                                        count++;
                                        if (count == 1) // suggest only this 1st period
                                        {
                                            //var period = billPeriodService.GetPeriodById(periodId);
                                            vmEmpoyeePeriod.PayslipPeriodSettings = (int)PayslipPeriodSettings.PeriodSuggestible;
                                            vmEmpoyeePeriod.PeriodId = thisPeriod.BillPeriodId;
                                            vmEmpoyeePeriod.PeriodName = thisPeriod.MonthName;
                                            vmEmpoyeePeriod.PositionId = position != null ? position.PostionId : 0;
                                            vmEmpoyeePeriod.PositionName = position != null ? position.PostionName : "";
                                            vmEmpoyeePeriod.PayrollId = payroll != null ? payroll.PayRollId : 0;
                                            vmEmpoyeePeriod.PayrollName = payroll != null ? payroll.PayRollName : "";
                                        }

                                        var periodListForAppend = new VmPayrollPositionWisePeriodList();
                                        periodListForAppend.PeriodId = thisPeriod.BillPeriodId;
                                        periodListForAppend.PositionId = customEmpInfo.PositionId ?? 0;
                                        periodListForAppend.PayrollId = currentPosition.PayrollId;
                                        PeriodsTobeAppend.Add(periodListForAppend);
                                    }
                                }
                            }
                            catch (Exception ex) { }
                        }
                    }
                }
                // if no periods generated yet
                vmEmpoyeePeriod.PayslipPeriodSettings = String.IsNullOrEmpty(vmEmpoyeePeriod.PeriodName) ? (int)PayslipPeriodSettings.AllPayslipGenerateComplete : vmEmpoyeePeriod.PayslipPeriodSettings;
                // bind every induvidual data for every period for view_more section
                if (PeriodsTobeAppend.Any())
                {
                    // add a serial number
                    var serial = 1;
                    foreach (var item in PeriodsTobeAppend)
                    {
                        item.Serial = serial;
                        serial++;
                    }
                    var periodSerial = 0;
                    foreach (var eachPeriod in PeriodsTobeAppend.OrderByDescending(s => s.Serial))
                    {
                        periodSerial++;
                        var pData = GetViewMoreSectionData(masterData, eachPeriod, billPeriods, positionData, periodSerial, timingFee);
                        periodDataList.Add(pData);
                    }
                    //update suggest period for generate to 1st canceled period
                    var firstcanceledPeriod = periodDataList.OrderByDescending(o => o.SerialReverse).FirstOrDefault(s => s.GenerateStatus == PayslipCurrentStatus.Canceled);
                    if (firstcanceledPeriod != null)
                    {
                        var payroll = payrollData.FirstOrDefault(s => s.PayRollId == firstcanceledPeriod.PayrollId);
                        var position = positionData.FirstOrDefault(s => s.PostionId == firstcanceledPeriod.PositionId);
                        vmEmpoyeePeriod.PayslipPeriodSettings = (int)PayslipPeriodSettings.PeriodSuggestible;
                        vmEmpoyeePeriod.PeriodId = firstcanceledPeriod.Id;
                        vmEmpoyeePeriod.PeriodName = firstcanceledPeriod.Name.Split(' ')[0];
                        vmEmpoyeePeriod.PositionId = position != null ? position.PostionId : 0;
                        vmEmpoyeePeriod.PositionName = position != null ? position.PostionName : "";
                        vmEmpoyeePeriod.PayrollId = payroll != null ? payroll.PayRollId : 0;
                        vmEmpoyeePeriod.PayrollName = payroll != null ? payroll.PayRollName : "";
                    }
                }
            }
            else if (PeriodsTobeAppend.Any()) //once a time this employee had a possition,then his position was set to null, so now he has no position assigned, so bind only his previous generedted periods
            {
                // add a serial number
                var serial = 1;
                foreach (var item in PeriodsTobeAppend)
                {
                    item.Serial = serial;
                    serial++;
                }
                var periodSerial = 0;
                foreach (var eachPeriod in PeriodsTobeAppend.OrderByDescending(s => s.Serial))
                {
                    periodSerial++;
                    var pData = GetViewMoreSectionData(masterData, eachPeriod, billPeriods, positionData, periodSerial, timingFee);
                    periodDataList.Add(pData);
                }
                vmEmpoyeePeriod.PayslipPeriodSettings = (int)PayslipPeriodSettings.PreviousPositionUnassigned;
                vmEmpoyeePeriod.PeriodId = 0;
                vmEmpoyeePeriod.PeriodName = "";

                //update suggest period for generate to 1st canceled period
                var firstcanceledPeriod = periodDataList.OrderByDescending(o => o.SerialReverse).FirstOrDefault(s => s.GenerateStatus == PayslipCurrentStatus.Canceled);
                if (firstcanceledPeriod != null)
                {
                    var payroll = payrollData.FirstOrDefault(s => s.PayRollId == firstcanceledPeriod.PayrollId);
                    var position = positionData.FirstOrDefault(s => s.PostionId == firstcanceledPeriod.PositionId);
                    vmEmpoyeePeriod.PayslipPeriodSettings = (int)PayslipPeriodSettings.PreviousPositionUnassigned;
                    vmEmpoyeePeriod.PeriodId = firstcanceledPeriod.Id;
                    vmEmpoyeePeriod.PeriodName = firstcanceledPeriod.Name.Split(' ')[0];
                    vmEmpoyeePeriod.PositionId = position != null ? position.PostionId : 0;
                    vmEmpoyeePeriod.PositionName = position != null ? position.PostionName : "";
                    vmEmpoyeePeriod.PayrollId = payroll != null ? payroll.PayRollId : 0;
                    vmEmpoyeePeriod.PayrollName = payroll != null ? payroll.PayRollName : "";
                }
            }
            else
            {
                vmEmpoyeePeriod.PayslipPeriodSettings = (int)PayslipPeriodSettings.PositionNotAssignedYet;
                vmEmpoyeePeriod.PeriodId = 0;
                vmEmpoyeePeriod.PeriodName = "";
            }
            var serialAsn = 1;
            periodDataList.OrderByDescending(o => o.SerialReverse).ToList().ForEach(f => f.Serial = serialAsn++);
            vmEmpoyeePeriod.PeriodList = periodDataList.OrderByDescending(o => o.SerialReverse).ToList();
            return vmEmpoyeePeriod;
        }
    }
}
