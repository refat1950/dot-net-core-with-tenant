﻿using Entity;
using Entity.Model;
using Entity.Model.Alert;
using Entity.Model.Process;
using Entity.Model.Transaction;
using Entity.Model.Workflow;
using Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace data.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<FndFlexValueSet> FlexSetRepo { get; }
        IRepository<FndFlexValue> FlexValueRepo { get; }
        IRepository<Session> SessionRepo { get; }
        IRepository<School> SchoolRepo { get; }
        IRepository<BillPeriod> PeriodRepo { get; }
        IRepository<Class> ClassRepo { get; }


        IRepository<ActionHistory> ActionHistoryRepo { get; }
        IRepository<ScoolAlert> ScoolAlertRepo { get; }
        IRepository<AlertHistory> AlertHistoryRepo { get; }
        IRepository<AttendanceStudent> AttendanceStudentRepo { get; }
        IRepository<Banner> BannerRepo { get; }
        IRepository<BillInfo> BillInfoRepo { get; }
        IRepository<Book> BookRepo { get; }
        IRepository<Calender> CalenderRepo { get; }
        IRepository<Campus> CampusRepo { get; }
        IRepository<ClassBook> ClassBookRepo { get; }
        IRepository<ClassCourseMaster> ClassCourseMasterRepo { get; }
        IRepository<ClassCourseSubject> ClassCourseSubjectRepo { get; }
        IRepository<ClassSection> ClassSectionRepo { get; }
        IRepository<ClassTeacher> ClassTeacherRepo { get; }
        IRepository<ClassSectionRoutine> ClassSectionRoutineRepo { get; }
        IRepository<ClassSubjectTeacher> ClassSubjectTeacherRepo { get; }
        IRepository<EventRoutine> EventRoutineRepo { get; }
        IRepository<Gallery> GalleryRepo { get; }
        IRepository<HelpfulResouce> HelpfulResouceRepo { get; }
        IRepository<Job> JobRepo { get; }
        IRepository<JobApply> JobApplyRepo { get; }
        IRepository<Leave> LeaveRepo { get; }
        IRepository<Logo> LogoRepo { get; }
        IRepository<LogoType> LogoTypeRepo { get; }
        IRepository<Notice> NoticeRepo { get; }
        IRepository<PhotoGallery> PhotoGalleryRepo { get; }
        IRepository<Routine> RoutineRepo { get; }
        IRepository<RoutineSetup> RoutineSetupRepo { get; }
        IRepository<MarksheetStatus> MarksheetStatusRepo { get; }
        IRepository<SchoolBranch> SchoolBranchRepo { get; }
        IRepository<Section> SectionRepo { get; }
        IRepository<StuDocument> StuDocumentRepo { get; }
        IRepository<Student> StudentRepo { get; }
        IRepository<StudentAdmissionTest> StudentAdmissionTestRepo { get; }
        IRepository<StudentAdmissionMark> StudentAdmissionMarkRepo { get; }
        IRepository<StudentBillMaster> StudentBillMasterRepo { get; }
        IRepository<StudentBillLine> StudentBillLineRepo { get; }
        IRepository<StudentBillMasterTemporary> StudentBillMasterTemporariesRepo { get; }
        IRepository<StudentBillLineTemporary> StudentBillLineTemporariesRepo { get; }
        IRepository<StudentMarks> StudentMarksRepo { get; }
        IRepository<StudentMedical> StudentMedicalRepo { get; }
        IRepository<StudentProx> StudentProxRepo { get; }
        IRepository<StudentSiblings> StudentSiblingsRepo { get; }
        IRepository<SubTeacher> SubTeacherRepo { get; }
        IRepository<SchoolTask> SchoolTaskRepo { get; }
        IRepository<VoteMaster> VoteMasterRepo { get; }
        IRepository<VoteMember> VoteMemberRepo { get; }
        IRepository<VoteOption> VoteOptionRepo { get; }
        IRepository<SchoolCalender> SchoolCalenderRepo { get; }
        IRepository<EmergencyMedicalAction> EmergencyMedicalActionRepo { get; }
        IRepository<FndMenu> FndMenuRepo { get; }
        IRepository<FndMenuGroup> FndMenuGroupsRepo { get; }
        IRepository<FndMenuPage> FndMenuPageRepo { get; }
        IRepository<FndModule> FndModuleRepo { get; }
        IRepository<FndPage> FndPageRepo { get; }
        IRepository<FndPagePics> FndPagePicsRepo { get; }
        IRepository<FndPeriods> FndPeriodsRepo { get; }
        IRepository<FndResponceMenu> FndResponceMenuRepo { get; }
        IRepository<FndResponsibility> FndResponsibilitRepo { get; }
        IRepository<FndUserPage> FndUserPageRepo { get; }
        IRepository<FndUsers> FndUsersRepo { get; }
        IRepository<GeneralInfo> GeneralInfoRepo { get; }
        IRepository<GeneralInfoType> GeneralInfoTypeRepo { get; }
        IRepository<HrDepartment> HrDepartmentRepo { get; }
        IRepository<HrDesigGrade> HrDesigGradeRepo { get; }
        IRepository<HrEmpExperience> HrEmpExperienceRepo { get; }
        IRepository<HrEmployeeHierarcy> HrEmployeeHierarcyRepo { get; }
        IRepository<HrEmployeeRefrence> HrEmployeeRefrenceRepo { get; }
        IRepository<HrLeaveInfo> HrLeaveInfoRepo { get; }
        IRepository<HrPositionStructure> HrPositionStructureRepo { get; }
        IRepository<MedicalHistory> MedicalHistoryRepo { get; }
        IRepository<PlannerDay> PlannerDayRepo { get; }
        IRepository<PlannerEvent> PlannerEventRepo { get; }
        IRepository<PlannerEventClass> PlannerEventClassRepo { get; }
        IRepository<PlannerEventDetails> PlannerEventDetailsRepo { get; }
        IRepository<PlannerEventType> PlannerEventTypeRepo { get; }
        IRepository<PlannerHoliday> PlannerHolidayRepo { get; }
        IRepository<Publisher> PublisherRepo { get; }
        IRepository<SchoolLinks> SchoolLinksRepo { get; }
        IRepository<SchoolType> SchoolTypeRepo { get; }
        IRepository<StudentAdmitCard> StudentAdmitCardRepo { get; }
        IRepository<ClassSubject> ClassSubjectRepo { get; }
        IRepository<News> NewsRepo { get; }
        IRepository<SmsHistory> SmsHistoryRepo { get; }
        IRepository<MenuGroup> MenuGroupRepo { get; }
        IRepository<Subject> SubjectRepo { get; }
        IRepository<Career> CareerRepo { get; }
        IRepository<FndUserResponce> FndUserResponceRepo { get; }
        IRepository<HrEmpEduQuali> HrEmpEduQualiRepo { get; }
        IRepository<HrEmployeeTiming> HrEmployeeTimingRepo { get; }
        IRepository<OnlineInquiry> OnlineInquiryRepo { get; }
        IRepository<StudentRagistration> StudentRagistrationRepo { get; }
        IRepository<JobApplicant> JobApplicantRepo { get; }
        IRepository<SubjectSubgroupClass> SubjectSubgroupClassRepo { get; }
        IRepository<SubjectSubgroup> SubjectSubgroupRepo { get; }
        IRepository<AlternateSubject> AlternateSubjectRepo { get; }
        IRepository<StudentAdmission> StudentAdmissionRepo { get; }
        IRepository<ExamGrade> ExamGradeRepo { get; }
        IRepository<ClassExamGrade> ClassExamGradeRepo { get; }
        IRepository<StudentCurriculumResult> StudentCurriculumResultRepo { get; }
        IRepository<StudentOtherInfo> StudentOtherInfoRepo { get; }
        IRepository<ProcessLog> ProcessLogRepo { get; }
        IRepository<ProcessAssessmentMarks> ProcessAssessmentMarksRepo { get; }
        IRepository<ReportProcess> ReportProcessRepo { get; }
        IRepository<AttendanceGrade> AttendanceGradeRepo { get; }
        IRepository<StudentSubject> StudentSubjectRepo { get; }
        IRepository<StudentPosition> StudentPositionRepo { get; }
        IRepository<StudentPositionLog> StudentPositionLogRepo { get; }
        IRepository<StudentRemarks> StudentRemarksRepo { get; }
        IRepository<SubjectSubgroupClassSection> SubjectSubgroupClassSectionRepo { get; }
        IRepository<StudentHistory> StudentHistoryRepo { get; }
        IRepository<ClassRoutine> ClassRoutineRepo { get; }
        IRepository<AttendenceInfo> AttendenceInfoRepo { get; }
        IRepository<HrShift> HrShiftRepo { get; }
        IRepository<HrEmployeeShift> HrEmployeeShiftRepo { get; }
        IRepository<HrExcepShift> HrExcepShiftRepo { get; }
        IRepository<AttendanceEmployee> AttendanceEmployeeRepo { get; }
        IRepository<AttendancePolicy> AttendancePolicyRepo { get; }
        IRepository<TempLeave> TempLeaveRepo { get; }
        IRepository<LeaveSetup> LeaveSetupRepo { get; }
        IRepository<EmployeeLeaveInfo> EmployeeLeaveInfoRepo { get; }
        IRepository<LeaveMaster> LeaveMasterRepo { get; }
        IRepository<WorkFlow> WorkFlowRepo { get; }
        IRepository<ApprovalFlow> ApprovalFlowRepo { get; }
        IRepository<ApproverInfo> ApproverInfoRepo { get; }
        IRepository<WorkFlowRole> WorkFlowRoleRepo { get; }
        IRepository<WorkFlowUserRole> WorkFlowUserRoleRepo { get; }
        IRepository<EmployeeManager> EmployeeManagerRepo { get; }
        IRepository<AlternativeApprovalFlow> AlternativeApprovalFlowRepo { get; }
        IRepository<HrAttendenceFix> HrAttendenceFixRepo { get; }
        IRepository<StudentTransaction> StudentTransactionRepo { get; }
        IRepository<MailGroup> MailGroupRepo { get; }
        IRepository<ProcessQueryTableMailGroup> ProcessQueryTableMailGroupRepo { get; }
        IRepository<Schedule> ScheduleRepo { get; }
        IRepository<ProcessSchedule> ProcessScheduleRepo { get; }
        IRepository<RecipientGroup> RecipientGroupRepo { get; }
        IRepository<MessageAlertMaster> MessageAlertMasterRepo { get; }
        IRepository<RemarksGrade> RemarksGradeRepo { get; }
        IRepository<FeeCategory> FeeCategoryRepo { get; }
        IRepository<FeeSubCategory> FeeSubCategoryRepo { get; }
        IRepository<SubCategoryFeeType> SubCategoryFeeTypeRepo { get; }
        IRepository<FeeSubCategoryFine> FeeSubCategoryFineRepo { get; }
        IRepository<FeeSubCategoryWaiver> FeeSubCategoryWaiverRepo { get; }
        IRepository<LeaveApplicationFile> LeaveApplicationFileRepo { get; }
        IRepository<Exam> ExamRepo { get; }
        IRepository<ExamType> ExamTypeRepo { get; }
        IRepository<ClassSubjectWiseExamType> ClassSubjectWiseExamTypeRepo { get; }
        IRepository<StudentCategory> StudentCategoriesRepo { get; }
        IRepository<SubCategoryFeePeriod> SubCategoryFeePeriodsRepo { get; }
        IRepository<StudentCategoryLog> StudentCategoryLogsRepo { get; }
        IRepository<StudentLeaveApplication> StudentLeaveApplicationsRepo { get; }
        IRepository<StudentLeaveCount> StudentLeaveCountsRepo { get; }
        IRepository<Device> DevicesRepo { get; }
        IRepository<TeacherLeaveCount> TeacherLeaveCountRepo { get; }
        IRepository<ExamPublishInfo> ExamPublishInfosRepo { get; }
        IRepository<BookCategory> BookCategoriesRepo { get; }
        IRepository<SMSTemplate> SMSTemplateRepo { get; }
        IRepository<BookShelfs> BookShelfsRepo { get; }
        IRepository<SmsBuyingLog> SmsBuyingLogsRepo { get; }
        IRepository<ClassSectionGroupTeacherMapping> ClassSectionGroupTeachersRepo { get; }
        IRepository<Year> YearsRepo { get; }
        IRepository<IssuedBook> IssuedBooksRepo { get; }
        IRepository<SubCategoryYear> SubCategoryYearsRepo { get; }
        IRepository<BookAuthor> BookAuthorsRepo { get; }
        IRepository<ClassSectionRoutineTransaciton> ClassSectionRoutineTransacitonsRepo { get; }
        IRepository<ExamSchedule> ExamSchedulesRepo { get; }
        IRepository<ExamScheduleLine> ExamScheduleLinesRepo { get; }
        IRepository<ExamSeatPlan> ExamSeatPlansRepo { get; }
        IRepository<ExamRoomInvestigator> ExamRoomInvestigatorsRepo { get; }
        IRepository<DateWiseInvestigator> DateWiseInvestigatorsRepo { get; }
        IRepository<DateWiseSubject> DateWiseSubjectsRepo { get; }
        IRepository<RoomSetup> RoomSetupsRepo { get; }
        IRepository<ClassRoom> ClassRoomsRepo { get; }
        IRepository<ClassRoomColumn> ClassRoomColumnsRepo { get; }
        IRepository<ClassRoomColumnRow> ClassRoomColumnRowsRepo { get; }
        IRepository<AttendanceChangeRequist> AttendanceChangeRequistsRepo { get; }
        IRepository<PaymentGatewaySetting> PaymentGatewaySettingsRepo { get; }
        IRepository<NotifyInfo> NotifyInfosRepo { get; }
        IRepository<NotifyTransactionInfo> NotifyTransactionInfosRepo { get; }
        IRepository<ProcessedMarksheet> ProcessedMarksheetsRepo { get; }
        IRepository<StudentSubjectMaster> StudentSubjectMastersRepo { get; }
        IRepository<StudentSubjectLine> StudentSubjectLinesRepo { get; }
        IRepository<StudentWiseSubject> StudentWiseSubjectsRepo { get; }




        #region Hrm & Payroll
        IRepository<Attendance> AttendancesRepo { get; }
        IRepository<AttendanceProcess> AttendanceProcessesRepo { get; }
        IRepository<HrPayHead> HrPayHeadsRepo { get; }
        IRepository<HrPayRoll> HrPayRollsRepo { get; }
        IRepository<HrPayRollPayHead> HrPayRollPayHeadsRepo { get; }
        IRepository<HrEmployeePayHead> HrEmployeePayHeadsRepo { get; }
        IRepository<HrBillPeriodWisePayRollIssueDate> HrBillPeriodWisePayRollIssueDatesRepo { get; }
        IRepository<HrPosition> HrPositionsRepo { get; }
        IRepository<HrPaySlipTransactionMaster> HrPaySlipTransactionMastersRepo { get; }
        IRepository<HrPaySlipTransactionMasterLog> HrPaySlipTransactionMasterLogsRepo { get; }
        IRepository<HrPaySlipTransactionMasterLogHistory> HrPaySlipTransactionMasterLogHistorysRepo { get; }
        IRepository<HrPaySlipTransaction> HrPaySlipTransactionsRepo { get; }
        IRepository<HrPaySlipTransactionLog> HrPaySlipTransactionLogsRepo { get; }
        IRepository<HrPaySlipTransactionLogHistory> HrPaySlipTransactionLogHistorysRepo { get; }
        IRepository<PositionChangeRequest> PositionChangeRequestsRepo { get; }
        IRepository<HrPaySlipPayLog> HrPaySlipPayLogsRepo { get; }
        IRepository<HrPaySlipTransactionLogAfterPayheadChange> HrPaySlipTransactionLogAfterPayheadChangesRepo { get; }
        IRepository<Compensation> CompensationsRepo { get; }
        IRepository<EmployeeOfficeTime> EmployeeOfficeTimesRepo { get; }
        IRepository<AttendanceEditHistory> AttendanceEditHistorysRepo { get; }
        IRepository<EventAndHolidayPlanner> EventAndHolidayPlannersRepo { get; }
        IRepository<HrPositionLeaveCategory> HrPositionLeaveCategoriesRepo { get; }
        IRepository<HrEmployeeLeaveApplication> HrEmployeeLeaveApplicationRepo { get; }
        IRepository<HrEmployeeLeaveCount> HrEmployeeLeaveCountRepo { get; }
        IRepository<EmployeeStatusLog> EmployeeStatusLogRepo { get; }
        IRepository<Resignation> ResignationRepo { get; }
        IRepository<Rejoin> RejoinRepo { get; }
        IRepository<OfficialResignRules> OfficialResignRulesRepo { get; }
        IRepository<HrEmployee> EmployeeRepo { get; }
        IRepository<HrEmployeeTransaction> HrEmployeeTransactionRepo { get; }
        IRepository<EmployeeWeekends> EmployeeWeekendsRepo { get; }
        #endregion

        int SaveChanges();
        Task<int> SaveChangesAsync();
    }
}
