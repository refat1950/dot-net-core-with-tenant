﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class ClassSectionRoutineTransaciton : BaseEntity
    {
        [Key]
        public int ClassRoutineTransacitonId { get; set; }

        //public int RoutineId { get; set; }
        public int ClassId { get; set; }
        public int SectionId { get; set; }
        public int? SessionId { get; set; }
        public int DayInTheWeek { get; set; }
        public int? SubjectId { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public virtual Subject Subject { get; set; }
        public int ClassRoomHeaderId { get; set; }
        public virtual ClassRoom ClassRoom { get; set; }

    }
}
