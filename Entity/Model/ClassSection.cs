using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entity.Model
{
    /// <summary>
    /// This Entity is required for Attendance Data processing. If Need to Change Pls Check with corresponding person.
    /// </summary>
    public partial class ClassSection : BaseEntity
    {
        [Key]
        public int ClassSectionId { get; set; }
        public int ClassId { get; set; }
        public int SectionId { get; set; }
        public int Shift { get; set; }
        public int? CampusId { get; set; }

        public string StartTime { get; set; }

        public string EndTime { get; set; }
        public int? NoOfStudent { get; set; }
        public virtual Class Class { get; set; }
        public virtual Section Section { get; set; }
        [ForeignKey("Shift")]
        public virtual FndFlexValue FlexValue { get; set; }
        public virtual List<ClassTeacher> ClassTeachers { get; set; }
    }
}
