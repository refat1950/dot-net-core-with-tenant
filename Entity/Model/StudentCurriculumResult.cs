﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class StudentCurriculumResult : BaseEntity
    {
        [Key]
        [Column(Order = 0)]
        public int CurriId { get; set; }

        [Column(Order = 1)]
        public int SessionId { get; set; }

        [Column(Order = 2)]
        public int ExamId { get; set; }

        [Column(Order = 3)]
        public int StudentHeaderId { get; set; }

        [Column(Order = 4)]
        public int CurriculumId { get; set; }

        [Column(Order = 5)]
        public int GradeId { get; set; }
    }
}
