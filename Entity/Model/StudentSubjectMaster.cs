﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class StudentSubjectMaster : BaseEntity
    {
        [Key]
        public int StudentSubjectMasterId { get; set; }
        public int SessionId { get; set; }
        public int ClassId { get; set; }
        public int SectionId { get; set; }
        public List<StudentSubjectLine> StudentSubjectLines { get; set; }
        public virtual Session Session { get; set; }
        public virtual Class Class { get; set; }
        public virtual Section Section { get; set; }
    }
}
