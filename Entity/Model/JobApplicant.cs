﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class JobApplicant : BaseEntity
    {
        [Key]
        public long JobApplicantId { get; set; }

        [Required]
        [StringLength(30)]
        public string EmpFirstName { get; set; }

        [StringLength(30)]
        public string EmpMiddleName { get; set; }

        [StringLength(30)]
        public string EmpLastName { get; set; }

        //public int Gender { get; set; }
        [Required]
        [StringLength(100)]
        public string Email { get; set; }

        [Required]

        [StringLength(20)]
        public string Mobile { get; set; }

        public int? Qualification { get; set; }
        public int? Catagory { get; set; }
        public int? Section { get; set; }

        public int UserId { get; set; }

        public virtual List<JobApply> JobApply { get; set; }

        public DateTime? BirthDate { get; set; }

        [StringLength(30)]
        public string FatherName { get; set; }

        [StringLength(30)]
        public string MotherName { get; set; }

        [StringLength(30)]
        public string NationalId { get; set; }

        public int? Nationality { get; set; }

        public int? Gender { get; set; }

        public int? Religion { get; set; }

        public int? MaritalStatus { get; set; }


        [StringLength(20)]
        public string PermanentHomePhone { get; set; }

        [StringLength(20)]
        public string PresHoldingNo { get; set; }

        [StringLength(50)]
        public string PresStreet { get; set; }

        [StringLength(50)]
        public string PresArea { get; set; }

        public int? PresPoliceStationId { get; set; }

        [StringLength(10)]
        public string PresPostCode { get; set; }

        public int? PresCityId { get; set; }

        public int? PresDivisionId { get; set; }

        public int? PresCountryId { get; set; }

        [StringLength(20)]
        public string PermHoldingNo { get; set; }

        [StringLength(50)]
        public string PermStreet { get; set; }

        [StringLength(50)]
        public string PermArea { get; set; }

        public int? PermPoliceStationId { get; set; }

        [StringLength(10)]
        public string PermPostCode { get; set; }

        public int? PermCityId { get; set; }

        public int? PermDivisionId { get; set; }

        public int? PermCountryId { get; set; }

        [StringLength(500)]
        public string CareerObjective { get; set; }

        public double? PresentSalary { get; set; }

        public double? ExpectedSalary { get; set; }

        public int? JobLevel { get; set; }

        public int? JobNature { get; set; }

        [StringLength(250)]
        public string CareerSummery { get; set; }

        [StringLength(250)]
        public string SpacialQualification { get; set; }


        [StringLength(200)]
        public string CvLocation { get; set; }

        [StringLength(200)]
        public string ImageLocation { get; set; }


        public string PermanentOfficePhone { get; set; }
        public string PermanentEmail { get; set; }

        //**********************************************Employment Hisory**************************************

        public string CompanyName { get; set; }
        public string CompanyBusiness { get; set; }

        public string CompanyLocation { get; set; }

        public string Department { get; set; }
        public string PositionHeld { get; set; }
        public string Responsibilities { get; set; }



        public DateTime? EmpHistFromDate { get; set; }
        public DateTime? EmpHistToDate { get; set; }



        //******************************************Education Information*********************************

        [Required(ErrorMessage = "Please select class")]
        public int LevelOfEducation { get; set; }
        public string ExamDegreeeTitle { get; set; }

        public string ConcentrationOrMajorGrpoup { get; set; }
        public string AcademicInstituteName { get; set; }
        public string Result { get; set; }

        public string GradeCgpa { get; set; }
        public string GradeScale { get; set; }


        public string DivisionResult { get; set; }

        public string YearOfPassing { get; set; }
        public string AcademicDuration { get; set; }
        public string Achievement { get; set; }
        public string TrainingTitle { get; set; }
        public string TrainingInstituteName { get; set; }
        public string TopicCovered { get; set; }
        public int? TrainingCountry { get; set; }
        public string TrainingLocation { get; set; }
        public string TrainingYear { get; set; }
        public string TrainingDuration { get; set; }
        public string ProfessionalCertification { get; set; }
        public string ProfessionalInstituteName { get; set; }
        public string ProfessionalLocation { get; set; }
        public DateTime? ProfessionalFromDate { get; set; }
        public DateTime? ProfessionalToDate { get; set; }



    }
}
