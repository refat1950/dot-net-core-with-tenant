﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class RemarksGrade : BaseEntity
    {
        [Key]
        public int RemarksId { get; set; }
        public string Description { get; set; }
        public string ShortDescription { get; set; }
        public string AltName { get; set; }
        public decimal FromScore { get; set; }
        public decimal ToScore { get; set; }
        public int GenderFlag { get; set; }
        public int ClassId { get; set; }
    }
}
