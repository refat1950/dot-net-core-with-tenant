﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Entity.Model
{
    public class SubCategoryFeePeriod : BaseEntity
    {
        [Key]
        public int BillPeriodId { get; set; }
        public string PeriodName { get; set; }
        public int FeeSubCategoryId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        //public DateTime? DueDate { get; set; }
        public int? Year { get; set; }
        public int? Month { get; set; }
        public int? Day { get; set; }
        public virtual FeeSubCategory FeeSubCategory { get; set; }
    }
}
