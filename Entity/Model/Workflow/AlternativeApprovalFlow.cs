﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model.Workflow
{
    public class AlternativeApprovalFlow : BaseEntity
    {
        [Key]
        [Column(Order = 1)]
        public int ExistingUserRoleId { get; set; }
        [Key]
        [Column(Order = 2)]
        public int WorkFlowId { get; set; }
        [Key]
        [Column(Order = 3)]
        public int AlternativeUserRoleId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
