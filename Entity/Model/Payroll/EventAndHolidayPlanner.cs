﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class EventAndHolidayPlanner : BaseEntity
    {
        [Key]
        public int EventHeaderId { get; set; }

        [Required(ErrorMessage = "Please provide events/holiday name.")]
        [StringLength(100)]
        public string EventName { get; set; }

        [StringLength(50)]
        public string EventShortName { get; set; }

        [StringLength(300)]
        public string EventDescription { get; set; }

        [Required(ErrorMessage = "Please provide events/holiday start date.")]
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        [StringLength(10)]
        public string ThemeColor { get; set; }

        public bool IsFullDay { get; set; }

        [Required(ErrorMessage = "Please select an event type.")]
        public int EventTypeId { get; set; }
    }
}
