﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.View_Model
{
   public class VmFeeSubCategory
    {
       public int MaxPayablePeriods { get; set; }
       public List<VmPeriodList> PeriodList { get; set; }
    }

    public class VmPeriodList
    {
        public int PeriodId { get; set; }
        public string PeriodName { get; set; }
    }
}
