using System;
using System.ComponentModel.DataAnnotations;

namespace Entity.Model
{
    public partial class HrPositionStructure : BaseEntity
    {
        [Key]
        public int PositionStructuresId { get; set; }

        [Required]
        [StringLength(100)]
        public string PositionStructureName { get; set; }

        public int? BusinessGroupId { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public bool? Status { get; set; }
    }
}
