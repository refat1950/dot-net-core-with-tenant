﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class AttendenceInfo
    {
        [Key]
        public int Attendanceid { get; set; }
        public int Inattendanceid { get; set; }
        public string EmpId { get; set; }
        public string FullName { get; set; }
        public string Dept { get; set; }
        public int DeptId { get; set; }
        public string Designation { get; set; }
        public int CampusId { get; set; }
        public string Campus { get; set; }
        public string Note { get; set; }
        public DateTime OfficeTime { get; set; }
        public DateTime OfficeOutTime { get; set; }
        public DateTime? InTime { get; set; }
        public DateTime? OutTime { get; set; }
        public string AttendanceCode { get; set; }
        public string LateTime { get; set; }
        public string WorkTime { get; set; }
        public string EarlyOut { get; set; }
    }



}
