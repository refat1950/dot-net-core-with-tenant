using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Entity.Model
{
    public partial class PlannerEventDetails //: BaseEntity
    {

        [Key]
        public int EventDtlsId { get; set; }

        public int SessionId { get; set; }

        public int EventId { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        [StringLength(250)]
        public string Attribute1 { get; set; }
        [StringLength(250)]
        public string Attribute2 { get; set; }
        [StringLength(250)]
        public string Attribute3 { get; set; }

        public long? CreatedBy { get; set; }


        /// <summary>
        /// WHO Column
        /// </summary>
        [DataType(DataType.Date, ErrorMessage = "Date format should be MM/DD/YYYY")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? CreationDate { get; set; }

        public long? LastUpdateBy { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? LastUpdateDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "Active From")]
        public DateTime? EffectiveFrom { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "Active To")]
        public DateTime? EffectiveTo { get; set; }
        public virtual PlannerEvent PlannerEvent { get; set; }
    }
}
