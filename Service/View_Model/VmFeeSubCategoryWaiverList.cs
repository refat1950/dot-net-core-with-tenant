﻿using Entity.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.View_Model
{
    public class VmFeeSubCategoryWaiverList
    {
        public int FeeSubCategoryWaiverId { get; set; }
        public int FeeCategoryId { get; set; }
        public string FeeCategoryName { get; set; }
        public int FeeSubCategoryId { get; set; }
        public string FeeSubCategoryName { get; set; }
        public int Category_GenderID { get; set; }
        public string Category_GenderName { get; set; }
        public int WaiverType { get; set; }
        public string WaiverTypeName { get; set; }
        public decimal Percentage { get; set; }
    }
}
