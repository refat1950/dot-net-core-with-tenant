﻿using Entity.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Entity.Model
{
    public class SubjectSubgroupClass //: BaseEntity
    {
        [Key]
        public int SubgroupClassId { get; set; }
        public int? SubgroupId { get; set; }
        public int? ClassId { get; set; }
        public string ShortName { get; set; }
        public int? MasterEventId { get; set; }
        public int? EventId { get; set; }

        public int? NoOfExam { get; set; }
        public int? BestCount { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }


        [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? LasteUpdateDate { get; set; }


        [StringLength(250)]
        public string Attribute1 { get; set; }

        [StringLength(250)]
        public string Attribute2 { get; set; }

        [StringLength(250)]
        public string Attribute3 { get; set; }

        [StringLength(250)]
        public string Attribute4 { get; set; }

        [StringLength(250)]
        public string Attribute5 { get; set; }


        public long? CreatedBy { get; set; }

        [DataType(DataType.Date, ErrorMessage = "Date format should be MM/DD/YYYY")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? CreationDate { get; set; }


        public long? LastUpdateBy { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? LastUpdateDate { get; set; }



        [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? EffectiveFrom { get; set; }


        [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? EffectiveTo { get; set; }

        [ForeignKey("SubgroupId")]
        public virtual SubjectSubgroup SubjectSubgroup { get; set; }

        [ForeignKey("EventId")]
        public virtual PlannerEvent Plannerevent { get; set; }
    }
}