﻿using System;
using System.Collections.Generic;
using Service.View_Model;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Services.Interfaces
{
    public interface IFeeSubCategoryWaiverService
    {
        int Add(VmFeeSubCategoryWaiverAdd model, int userId);
        List<VmFeeSubCategoryWaiverList> GetBetween(int start, int displayLength, string searchValue, out int totalLength);
    }
}
