using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entity.Model
{
    public class Subject : BaseEntity
    {
        [Key]
        [Column(Order = 0)]
        public int SubjectId { get; set; }

        [StringLength(50)]
        public string SubjectName { get; set; }

        [StringLength(30)]
        public string ShortName { get; set; }

        public bool Major { get; set; }
        public bool? Active { get; set; }
        public virtual List<StudentMarks> StudentMarks { get; set; }
        public virtual List<ClassSubject> ClassSubject { get; set; }
    }
}