﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Helper
{
    public class DateTimeDistinctionCalculation
    {
        public VmDateDistance CalculateDistinctionOfTwoDateTime(DateTime EndDate, DateTime StartDate)
        {
            VmDateDistance vmDateDistance = new VmDateDistance();
            double dateVariance = (EndDate - StartDate).TotalHours;
            double fractionDateVariance = (dateVariance / 24); //divided by 24 hours as oneday = 24 hours.
            string formatFractionDateVariance = fractionDateVariance.ToString("N2");  // take 2 digit after decimal point.
            var SplitDateVariance = formatFractionDateVariance.Split('.');
            int convertSplitedIntigerPartForDays = Convert.ToInt32(SplitDateVariance[0]);

            if (convertSplitedIntigerPartForDays == 0)
            {
                double fractionDateVarianceToHour = (fractionDateVariance * 24); //multiply with 24 to get the hour calculation from decimal fraction.
                string formatFractionDateVarianceToHour = fractionDateVarianceToHour.ToString("N2"); // take 2 digit after decimal point.
                var SplitractionDateVarianceToHour = formatFractionDateVarianceToHour.Split('.');
                int fractionDateVarianceToHourIntigerPart = Convert.ToInt32(SplitractionDateVarianceToHour[0]);
                int fractionDateVarianceToHourDecimalPart = Convert.ToInt32(SplitractionDateVarianceToHour[1]);
                if (fractionDateVarianceToHourDecimalPart >= 50)
                {
                    fractionDateVarianceToHourIntigerPart = (fractionDateVarianceToHourIntigerPart + 1);
                }

                vmDateDistance.Day = 0 + " Day";
                vmDateDistance.Hour = fractionDateVarianceToHourIntigerPart == 1 ? 1 + " Hour" : fractionDateVarianceToHourIntigerPart > 1 ? fractionDateVarianceToHourIntigerPart + " Hours" : 0 + " Hour";
            }

            else if (convertSplitedIntigerPartForDays > 0)
            {
                double splitedIntigerPart = Convert.ToDouble(SplitDateVariance[0]);
                double splitedDecimalPart = (fractionDateVariance - splitedIntigerPart);

                //working with decimal part from here
                double decimalPartToHour = (splitedDecimalPart * 24); //multiply with 24 to get the hour calculation from decimal fraction.
                string formatDecimalPartToHour = decimalPartToHour.ToString("N2");  // take 2 digit after decimal point.
                var SplitDecimalPartToHour = formatDecimalPartToHour.Split('.');
                int decimalPartToHourIntigerPart = Convert.ToInt32(SplitDecimalPartToHour[0]);
                int decimalPartToHourDecimalPart = Convert.ToInt32(SplitDecimalPartToHour[1]);
                if (decimalPartToHourDecimalPart >= 50)
                {
                    decimalPartToHourIntigerPart = (decimalPartToHourIntigerPart + 1);
                }

                //create No of days & No of Hours

                vmDateDistance.Day = convertSplitedIntigerPartForDays == 1 ? 1 + " Day" : convertSplitedIntigerPartForDays > 1 ? convertSplitedIntigerPartForDays + " Days" : 0 + " Day";
                vmDateDistance.Hour = decimalPartToHourIntigerPart == 1 ? 1 + " Hour" : decimalPartToHourIntigerPart > 1 ? decimalPartToHourIntigerPart + " Hours" : 0 + " Hour"; ;
            }

            return vmDateDistance;
        }

        public int[] monthDay = new int[12] { 31, -1, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
        public string CalculateDateDifference(DateTime d1, DateTime d2) 
        {
            int increment;

            DateTime fromDate;
            DateTime toDate;

            int year;
            int month;
            int day;


            //To Date must be greater
            if (d1 > d2)
            {
                 fromDate = d2;
                 toDate = d1;
            }
            else
            {
                 fromDate = d1;
                 toDate = d2;
            }

            /// Day Calculation
            increment = 0;


            if ( fromDate.Day >  toDate.Day)
            {
                increment =  monthDay[ fromDate.Month - 1];
            }

            /// if it is february month

            /// if it's to day is less then from day

            if (increment == -1)
            {
                if (DateTime.IsLeapYear( fromDate.Year))
                {
                    // leap year february contain 29 days
                    increment = 29;
                }
                else
                {
                    increment = 28;
                }
            }

            if (increment != 0)
            {
                day = ( toDate.Day + increment) -  fromDate.Day;
                increment = 1;
            }
            else
            {
                day =  toDate.Day -  fromDate.Day;
            }

            ///month calculation
            if (( fromDate.Month + increment) >  toDate.Month)
            {
                 month = ( toDate.Month + 12) - ( fromDate.Month + increment);
                increment = 1;
            }
            else
            {
                 month = ( toDate.Month) - ( fromDate.Month + increment);
                increment = 0;
            }

            /// year calculation
             year =  toDate.Year - ( fromDate.Year + increment);


            var DateDistance = "";

            DateDistance = year != 0 ? year > 1 ? year + " Years " : year + " Year " : " ";
            DateDistance += month != 0 ? month > 1 ? month + " Months " : month + " Month " : " ";
            DateDistance += day != 0 ? day > 1 ? day + " Days " : day + " Day " : " ";

            return DateDistance;
        }
    }
}



//}
    
//    //Public type Constructor
//}


//        public override string ToString()
//        {
//            //return base.ToString();
//            return this.year + " Year(s), " + this.month + " month(s), " + this.day + " day(s)";
//        }


//        public int Years
//        {
//            get { return this.year; }
//        }

//        public int Months
//        {
//            get { return this.month; }
//        }

//        public int Days
//        {
//            get { return this.day; }
//        }
//    }
    //public class CalculateDateDifference

    //{
    //    /// defining Number of days in month; index 0 represents january and 11 represents December
    //    /// february contain either 28 or 29 days, so here its value is -1 which will be calculate later.

    //    private int[] monthDay = new int[12] { 31, -1, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

    //    private DateTime fromDate;
    //    private DateTime toDate;

    //    private int year;
    //    private int month;
    //    private int day;

    //    //Public type Constructor
    //    public CalculateDateDifference(DateTime d1, DateTime d2)

    //    {
    //        int increment;

    //        //To Date must be greater
    //        if (d1 > d2)
    //        {
    //            this.fromDate = d2;
    //            this.toDate = d1;
    //        }
    //        else
    //        {
    //            this.fromDate = d1;
    //            this.toDate = d2;
    //        }

    //        /// Day Calculation
    //        increment = 0;


    //        if (this.fromDate.Day > this.toDate.Day)
    //        {
    //            increment = this.monthDay[this.fromDate.Month - 1];
    //        }

    //        /// if it is february month

    //        /// if it's to day is less then from day

    //        if (increment == -1)
    //        {
    //            if (DateTime.IsLeapYear(this.fromDate.Year))
    //            {
    //                // leap year february contain 29 days
    //                increment = 29;
    //            }
    //            else
    //            {
    //                increment = 28;
    //            }
    //        }

    //        if (increment != 0)
    //        {
    //            day = (this.toDate.Day + increment) - this.fromDate.Day;
    //            increment = 1;
    //        }
    //        else
    //        {
    //            day = this.toDate.Day - this.fromDate.Day;
    //        }

    //        ///month calculation
    //        if ((this.fromDate.Month + increment) > this.toDate.Month)
    //        {
    //            this.month = (this.toDate.Month + 12) - (this.fromDate.Month + increment);
    //            increment = 1;
    //        }
    //        else
    //        {
    //            this.month = (this.toDate.Month) - (this.fromDate.Month + increment);
    //            increment = 0;
    //        }

    //        /// year calculation
    //        this.year = this.toDate.Year - (this.fromDate.Year + increment);
    //    }


    //    public override string ToString()
    //    {
    //        //return base.ToString();
    //        return this.year + " Year(s), " + this.month + " month(s), " + this.day + " day(s)";
    //    }


    //    public int Years
    //    {
    //        get { return this.year; }
    //    }

    //    public int Months
    //    {
    //        get { return this.month; }
    //    }

    //    public int Days
    //    {
    //        get { return this.day; }
    //    }
    //}
//}

    public class VmDateDistance
    {
        public string Hour { get; set; }
        public string Day { get; set; }
    }
