﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Services.View_Model
{
    public class VmStudentAttendance
    {
        public string Date { get; set; }        
        public string Type { get; set; }     
        public string CardNumber { get; set; }     
        public string InTime { get; set; }
        public string OutTime { get; set; }
        public string PunchDate { get; set; }
        public string DateFormateForToday { get; set; }
        public DateTime PunchDateWithoutConvert { get; set; }
        public string ClassInTime { get; set; }
        public string ClassOutTime { get; set; }
        public string PreviousClassInTime { get; set; }
        public string PreviousClassOutTime { get; set; }
        public int CurrentStatusInTime { get; set; }
        public int CurrentStatusOutTime { get; set; }
        public string RequistedInTime { get; set; }
        public string RequistedOutTime { get; set; }
        public string ApprovedInTime { get; set; }
        public string ApprovedOutTime { get; set; }
        public int? LateCountAfterMinutes { get; set; }
        public int? EarlyOutCountAfterMinutes { get; set; }
        public string LateTime { get; set; }
        public string EarlyOut { get; set; }
        public string OverTime { get; set; }
        public string StudyHourDistance { get; set; }
        public int RequestedInTimeHeaderId { get; set; }
        public int RequestedOutTimeHeaderId { get; set; }
        public bool? IsAllowedEditIfIntimeOuttimeRequestPending { get; set; }
    }
}