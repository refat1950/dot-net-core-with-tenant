using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Entity.Model
{
    public class NotifyInfo : BaseEntity
    {
        [Key]
        public int NotifyInfoId { get; set; }
        public int NotifyFromRefrenceId { get; set; }
        public int NotifyToHeaderId { get; set; }
        public int NotifyToUserType { get; set; }
        public int TemplateId { get; set; }
        public string Text { get; set; }
        public int NotifyType { get; set; }
        public bool NotifyWithSms { get; set; }
        public bool NotifyWithNotification { get; set; }
        public bool IsSeen { get; set; }
    }
}
