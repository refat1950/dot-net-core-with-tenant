﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class ClassRoomColumn : BaseEntity
    {
        [Key]
        public int ClassRoomColumnId { get; set; }
        public int RoomSetupHeaderId { get; set; }
        public int ColumnSerial { get; set; }
        public int NoOfRow { get; set; }
        public virtual RoomSetup RoomSetup { get; set; }
        public virtual List<ClassRoomColumnRow> ClassRoomColumnRows { get; set; }
    }
}
