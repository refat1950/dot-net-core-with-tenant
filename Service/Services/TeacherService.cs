﻿using System;
using System.Collections.Generic;
using System.Linq;
using data.UnitOfWork;
using Entity.Model;
using Repository.Context;
using Repository.Provider;
using Services.Interfaces;
using Services.View_Model;

namespace Services
{
    public class TeacherService : ITeacherService
    {

        private const int NoticeForAllId = 354;
        private const int NoticeForTeacher = 356;
        private const int NoticeType = 353;
        private const int NewsType = 5079;

        private IUnitOfWork _unitOfWork;
      
        //private readonly GenericRepository<ClassRoutine> _bssClassRoutine = new GenericRepository<ClassRoutine>(new ProjectDbContext());


        public TeacherService( IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        private int GetCurrentSession()
        {
            var currentSesssion = _unitOfWork.SessionRepo.FirstOrDefault(f => f.Status == true);
            return currentSesssion.SessionId;
        }

        public VmTeacherDashbord TeacherInfo(int hrHeaderId, DateTime routineDate)
        {
            var startDate = new DateTime(routineDate.Year, routineDate.Month, 1);
            var endDate = startDate.AddMonths(1);

            var teacherHr = _unitOfWork.EmployeeRepo.FirstOrDefault(s => s.EmpHeaderId == hrHeaderId);
            var fullName = (teacherHr.EmpFirstName + " " + teacherHr.EmpMiddleName + " " + teacherHr.EmpLastName).Replace("  ", " ");

            var campusId = teacherHr.JobLocationId != null ? teacherHr.JobLocationId.Value : 0;

            var campus = _unitOfWork.CampusRepo.FirstOrDefault(s => s.CampusId == campusId);

            var teacherInfo = new VmTeacherDashbord
            {
                FullName = fullName,
                TeacherId = teacherHr.EmpId,
                ImageUrl = teacherHr.ImgUrl.Replace("~", ""),

                Department = _unitOfWork.FlexValueRepo.FirstOrDefault(f => f.FlexValueId == teacherHr.DepartmentId).FlexValue,
                Designation = _unitOfWork.FlexValueRepo.FirstOrDefault(f => f.FlexValueId == teacherHr.DesignationId).FlexValue,
                Section = campus.CampusTitle.ToLower(),
                Campus = campus.CampusName.ToLower(),
                Email = teacherHr.Email,
                ClassRoutine = TeacherClassRoutine(teacherHr.EmpHeaderId, routineDate),
                Attendance = TeacherAttendance(teacherHr.EmpHeaderId, startDate, endDate),
                TeacherNews = TeacherNotice(teacherHr.EmpHeaderId, campusId),
                News = GetNews(teacherHr.EmpHeaderId, campusId)
            };
            return teacherInfo;
        }

        private List<VmTeacherClassRoutine> TeacherClassRoutine(int teacherHeadId, DateTime routineDate)
        {
            var sessionId = GetCurrentSession();

            //var teacherClassRoutine = _bssClassSubjectTeacherRepo.SearchFor(s => s.TeacherId == teacherHeadId && s.SessionId == sessionId)
            //        .Select(s => new VmTeacherClassRoutine
            //        {
            //            Class = s.Class.ClassName,
            //            Section = s.Section.SectionName,
            //            Period = s.Shift.ToString(),
            //            Subject = s.Subject.SubjectName
            //        })
            //        .ToList();

            var teacherClassRoutine =_unitOfWork.ClassRoutineRepo.Find(s => s.TeacherId == teacherHeadId && s.SectionId == sessionId && s.RoutineDate == routineDate)
                    .AsEnumerable()
                    .Select(s =>
                    {
                        var bssClass = _unitOfWork.ClassRepo.FirstOrDefault(f => f.ClassId == s.ClassId);
                        var section = _unitOfWork.SectionRepo.FirstOrDefault(f => f.SectionId == s.SectionId);
                        var subject = _unitOfWork.SubjectRepo.FirstOrDefault(f => f.SubjectId == s.SubjectId);
                        var period = _unitOfWork.FlexValueRepo.FirstOrDefault(f => f.FlexValueId == s.Period);
                        return new VmTeacherClassRoutine
                        {
                            Class = bssClass != null ? bssClass.ClassName : "",
                            Section = section != null ? section.SectionName : "",
                            Period = period != null ? period.FlexValue : "",
                            Subject = subject != null ? subject.SubjectName : ""
                        };
                    })
                    .ToList();

            return teacherClassRoutine;
        }

        private List<TeacherNews> TeacherNotice(int teacherHeaderId, int campusId)
        {
            try
            {
                return GetNotice(NoticeType, teacherHeaderId, campusId)
                    .Select(s => new TeacherNews
                    {
                        NewsHead = s.Header,
                        NewsUrl = "#"
                    })
                    .ToList();
            }
            catch (Exception)
            {
                return new List<TeacherNews>();
            }
        }

        public List<string> GetNews(int teacherHeadId, int campusId)
        {
            try
            {
                return GetNotice(NewsType, teacherHeadId, campusId).Select(s => s.Body).ToList();
            }
            catch (Exception)
            {
                return new List<string>();
            }
        }

        private IQueryable<Notice> GetNotice(int noticeType, int teacherHeadId, int campusId)
        {
            var sessionId = GetCurrentSession();

            var routine =_unitOfWork.ClassSubjectTeacherRepo.Find(s => s.TeacherId == teacherHeadId && s.SessionId == sessionId);
            var classIds = routine.Select(s => s.ClassId).ToArray();
            var sectionIds = routine.Select(s => s.SectionId).ToArray();

            return _unitOfWork.NoticeRepo.Find(w => (w.NoticeFor == NoticeForTeacher || w.NoticeFor == NoticeForAllId) &&
                             w.NoticeType == noticeType &&
                             (w.EffectiveFrom <= DateTime.Today || w.EffectiveFrom == null) &&
                             (w.EffectiveTo >= DateTime.Today || w.EffectiveTo == null) &&
                             (w.ClassId == null || w.ClassId == 0 || classIds.Contains(w.ClassId)) &&
                             (w.SectionId == null || w.SectionId == 0 || sectionIds.Contains(w.SectionId)) &&
                             (w.CampusId == null || w.CampusId == 0 || w.CampusId == campusId) &&
                             (NewsType == noticeType || w.OrderBy != null)).AsQueryable();
        }
        private View_Model.AttendanceVm TeacherAttendance(int empHEaderId, DateTime startDate, DateTime endDate)
        {
            var attendance = _unitOfWork.AttendanceEmployeeRepo.Find(w => w.EmpHeaderId == empHEaderId && w.PunchDate >= startDate && w.PunchDate <= endDate);


            return new View_Model.AttendanceVm
            {
                TotalWorkingDay = attendance.Count(),
                Present = attendance.Count(c => c.EarlyLeave <= 0 && c.TimeLate <= 0 && c.InTime != null),
                Late = attendance.Count( c => c.TimeLate > 0),
                EarlyLeave = attendance.Count( c => c.EarlyLeave > 0),
                Absent = attendance.Count(c=> c.InTime == null),

            };
            //var h = _unitOfWork.HrEmployeeShift.Where(w=>w.Empheaderid == empHEaderId && w.Status )
        }

        public string GetTeacherName(int subjectid)
        {
            var TeacherIds = _unitOfWork.ClassSectionGroupTeachersRepo.Find(x => x.SubjectId == subjectid).Select(a => a.TeacherId).Distinct().ToList();

            var TeacherNames = _unitOfWork.EmployeeRepo.Find(a => TeacherIds.Contains(a.EmpHeaderId)).ToList();
            var names = "";
            foreach (var item in TeacherNames)
            {
                names += _unitOfWork.EmployeeRepo.Find(s => s.EmpHeaderId == item.EmpHeaderId).Select(s => s.EmpFirstName + " " + s.EmpMiddleName + " " + s.EmpLastName).FirstOrDefault();
                names += ",";

            }

            names = names.Length != 0 ? names.Substring(0, names.Length - 1) : "";
            return names;
        }
    }
}