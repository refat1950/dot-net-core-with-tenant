using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entity.Model
{
    public partial class SubTeacher : BaseEntity
    {
        public int TeacherId { get; set; }
        public int SubjectId { get; set; }

        public bool Major { get; set; }

        public bool SubHead { get; set; }

        public bool Active { get; set; }

        public DateTime? StartTime { get; set; }

        public DateTime? EndTime { get; set; }


    }
}
