using System;
using System.ComponentModel.DataAnnotations;

namespace Entity.Model
{
    public partial class HrEmployeeHierarcy : BaseEntity
    {
        [Key]
        public int EmpHeaderId { get; set; }
        public int? SuperiorId { get; set; }
        public int? PositionStructureId { get; set; }
        public int? EmployeePositionId { get; set; }
        public int? SuperiorPositionId { get; set; }
        public int? SuperiorLevel { get; set; }
    }
}
