using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entity.Model
{
    public partial class Campus : BaseEntity
    {
        [Key]
        public int CampusId { get; set; }

        [Required]
        [StringLength(100)]
        public string CampusName { get; set; }

        [Required]
        [StringLength(100)]
        public string CampusTitle { get; set; }

        [StringLength(20)]
        public string HoldingNo { get; set; }

        [StringLength(50)]
        public string Street { get; set; }

        [StringLength(50)]
        public string Area { get; set; }

        [StringLength(50)]
        public string City { get; set; }

        [StringLength(50)]
        public string State { get; set; }

        public int? CountryID { get; set; }
        public int? DistrictID { get; set; }
        public int? CityID { get; set; }

        [StringLength(50)]
        public string District { get; set; }

        [StringLength(30)]
        public string PhoneNo { get; set; }

        public string MapUrl { get; set; }

        [StringLength(50)]
        [DataType(DataType.EmailAddress)]
        public string EmailAddress { get; set; }

        public int? NoOfRooms { get; set; }

        public int? FlexValueId { get; set; }


        public float? Latitude { get; set; }
        public float? Longitude { get; set; }

        //public virtual List<StuDocument> StuDocument { get; set; }
    }
}
