﻿using Entity.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.View_Model
{
    public class VmDateWiseAttendance
    {
        public int EmpHeaderId { get; set; }
        public string EmpId { get; set; }
        public string TeacherName { get; set; }
        public string Designation { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string InTime { get; set; }
        public string OutTime { get; set; }
        public string WorkingHour { get; set; }
        public string AttendanceType { get; set; }
        public int TotalPaunch { get; set; }
    }

    public class VmStudentDateWiseAttendance
    {
        public int StudentHeaderId { get; set; }
        public string StudentId { get; set; }
        public string StudentName { get; set; }
        public string Designation { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string InTime { get; set; }
        public string OutTime { get; set; }
        public string StudyHour { get; set; }
        public string AttendanceType { get; set; }
        public int TotalPaunch { get; set; }


        public string ApprovedInTime { get; set; }
        public string ApprovedOutTime { get; set; }
        public AttendanceApprovedStatus RequestedInTimeStatus { get; set; }
        public AttendanceApprovedStatus RequestedOutTimeStatus { get; set; }
        public int RequestedInTimeHeaderId { get; set; }
        public int RequestedOutTimeHeaderId { get; set; }
        public string RequestedInTime { get; set; }
        public string RequestedOutTime { get; set; }

        public string LateTime { get; set; }
        public string EarlyOut { get; set; }
        public string OverTime { get; set; }
        public string StudyHourDistance { get; set; }
    }
}
