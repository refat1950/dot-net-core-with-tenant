using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entity.Model
{
    public class ClassTeacher : BaseEntity
    {
        [Key]
        public int Id { get; set; }
        public int SessionId { get; set; }
        public int ClassTeacherId { get; set; }
        public int ClassSectionId { get; set; }
        public virtual ClassSection ClassSection { get; set; }
    }
}