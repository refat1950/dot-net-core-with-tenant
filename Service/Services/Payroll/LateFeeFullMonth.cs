﻿using data.UnitOfWork;
using Repository.Context;
using Service.DataService;
using Service.Helper;
using Service.Services.Interfaces;
using Service.ViewModel.Payroll;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Service.Services.Payroll
{
    public class LateFeeFullMonth : IPaySlipLateFee
    {
        private IUnitOfWork unitOfWork;
        private IEmployeeAttendanceService employeeAttendanceService;
        public LateFeeFullMonth(IUnitOfWork unitOfWork, IEmployeeAttendanceService employeeAttendanceService)
        {
            this.unitOfWork = unitOfWork;
            this.employeeAttendanceService = employeeAttendanceService;
        }

        public CalculatedFee GetLateFee(int empHeaderId, int periodId)
        {
            var period = unitOfWork.PeriodRepo.FirstOrDefault(s => s.PeriodId == periodId);
            if (period !=null)
            {
                var latefeePayheadId = unitOfWork.HrPayHeadsRepo.FirstOrDefault(s => s.PayHeadShortValue == "lf");
                var thisEmpFeePerHour = latefeePayheadId != null ? unitOfWork.HrEmployeePayHeadsRepo.FirstOrDefault(s => s.EmpHeaderId == empHeaderId && s.PayHeadId == latefeePayheadId.PayHeadId) != null ? unitOfWork.HrEmployeePayHeadsRepo.FirstOrDefault(s => s.EmpHeaderId == empHeaderId && s.PayHeadId == latefeePayheadId.PayHeadId).Unit : 0 : 0;
                var thisEmpFeePerMinute = (decimal)(Convert.ToDouble(thisEmpFeePerHour) / 60.0);
                var totalMinutes = 0;

                var startDate = period.StartDate.Value.Date;
                var endDate = period.EndDate.Value.Date;
                var dayDistance = (endDate - startDate).TotalDays;
                for (var i = 0; i <= dayDistance; i++)
                {
                    var date = startDate.AddDays(i);
                    var atnData = employeeAttendanceService.GetDayAttendanceData(date, empHeaderId);
                    if ( !String.IsNullOrEmpty(atnData.LateTime) && atnData.LateTime != "00:00")
                    {
                        var time = DateTime.Parse(atnData.LateTime);
                        var totalMinute = (time.Hour > 0 ? time.Hour * 60 : 0) + time.Minute;
                        totalMinutes = totalMinutes + totalMinute;
                    }
                }
                var d = new CalculatedFee();
                d.Fee = Math.Round(thisEmpFeePerMinute * totalMinutes, 0);
                d.Hours = Math.Round((decimal)(totalMinutes / 60.0), 2).ToString();
                d.Minutes = totalMinutes.ToString();
                return d;
            }
            else
                return new CalculatedFee();
        }
    }
}
