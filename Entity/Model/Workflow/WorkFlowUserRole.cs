﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model.Workflow
{
    public class WorkFlowUserRole : BaseEntity
    {
        [Key]
        public int WfUserRoleId { get; set; }
        public int WfRoleId { get; set; }
        public int UserId { get; set; }
    }
}
