using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entity.Model
{
    public partial class LogoType : BaseEntity
    {
        public LogoType()
        {
            BssLogo = new HashSet<Logo>();
        }

        [Key]
        public int LogoTypeId { get; set; }

        [StringLength(150)]
        public string Type { get; set; }

        public virtual ICollection<Logo> BssLogo { get; set; }
    }
}
