﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.View_Model
{
    public class VMNotice
    {
        public int ID { get; set; }
        public string Header { get; set; }
        public string Notice { get; set; }
        public string NoticeType { get; set; }
        public string NoticeFor { get; set; }
        public DateTime NoticeDate { get; set; }
        public string Tailer { get; set; }
        public string OrderBy { get; set; }
        public string Designation { get; set; }
    }

    public class VmStuCurrentNoticeList
    {
        public int NoticeId { get; set; }
        public string NoticeType { get; set; }
        public string NoticeFor { get; set; }
        public string Header { get; set; }
        public string Body { get; set; }
        public string EffectiveFrom { get; set; }
        public string EffectiveTo { get; set; }
    }


}
