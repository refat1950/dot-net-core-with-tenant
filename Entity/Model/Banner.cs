using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Entity.Model
{
    public class Banner : BaseEntity
    {
        [Key]
        public int BanenrId { get; set; }

        [Required]
        [StringLength(200)]
        [Display(Name = "Titel")]
        public string Title { get; set; }

        [Display(Name = "Upload Image")]
        public string ImageUrl { get; set; }

        [Display(Name = "Detailes")]
        public string Details { get; set; }

        [Display(Name = "Banner Status")]
        public bool BannerStatus { get; set; }
    }
}
