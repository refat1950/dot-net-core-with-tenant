﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class HrPaySlipTransactionMaster : BaseEntity
    {
        [Key]
        public int PaySlipMasterHeaderId { get; set; }
        public int PeriodId { get; set; }
        public int EmpHeaderId { get; set; }
        public int PositionId { get; set; }
        public int PayrollId { get; set; }
        public decimal TotalAmountPayable { get; set; }
        public decimal TotalAdditionAmount { get; set; }
        public decimal TotalDeductionAmount { get; set; }
        public PaidStatus PaidStatus { get; set; }
        public PayslipCurrentStatus PayslipCurrentStatus { get; set; }
        public string PayslipId { get; set; }
        public DateTime GenerationDate { get; set; }
        public virtual ICollection<HrPaySlipTransaction> HrPaySlipTransactions { get; set; }
        public virtual ICollection<HrPaySlipPayLog> HrPaySlipPayLogs { get; set; }
        public virtual ICollection<HrPaySlipTransactionMasterLog> HrPaySlipTransactionMasterLog { get; set; } // full transection logs
        public virtual ICollection<HrPaySlipTransactionMasterLogHistory> GetHrPaySlipTransactionMasterLogHistories { get; set; } // full transection logs
    }
    public enum PaidStatus
    {
        PartiallyPaid = 1,
        FullyPaid = 2,
        UnPaid = 3
    }
    public enum PayslipCurrentStatus
    {
        Generated = 1,// generate with current employee payheads
        ReGenerated = 2, // generate again with employee's previous payheads from temporary log table, during regenerate the previous payheads amount can be change only, no new payhead cannot be add
        Updated = 3, // if a pay slip generated and partially paid, then pay slip can be updated. Update payslip only re-calculates Late Fee & Overtime payheads,
        NotGenerated = 4, // if a payslip suggested on Payslip Manager but not generated yet
        Canceled = 5 // remove all data from log tables, only update master table and history tabes . if a payslip is canceled, it will suggest again for generate this payslip
    }

    public enum PayslipPeriodSettings
    {
        PeriodSuggestible = 0,
        AllPayslipGenerateComplete = 1,
        PositionNotAssignedYet = 2,
        PreviousPositionUnassigned = 3
    }
}
