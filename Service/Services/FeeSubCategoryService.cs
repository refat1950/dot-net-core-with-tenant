﻿using data.UnitOfWork;
using Entity.Model;
using Repository.Context;
using Service.Helper;
using Service.Services.Interfaces;
using Service.View_Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Service.Services
{
    public class FeeSubCategoryService : IFeeSubCategoryService
    {
        private IUnitOfWork _unitOfWork;
     

        public FeeSubCategoryService(IUnitOfWork unitOfWork)
        {   _unitOfWork = unitOfWork;
        }

        public int Add(VmFeeSubCategoryAdd model, int userId)
        {
            //var allYear = _unitOfWork.Years.Select(i=> new { i.YearId, i.YearName}).ToList();
            if (model.FeeSubCategoryId == 0)
            {
                FeeSubCategory newData = new FeeSubCategory();
                newData.FeeSubCategoryId = 0;
                newData.FeeCategoryId = model.FeeCategoryId;
                newData.FeeSubCategoryName = model.FeeSubCategoryName;
                newData.Amount = model.Amount;
                newData.FeeType = model.FeeType;
                newData.CreationDate = BdDateTime.Now();
                newData.CreatedBy = userId;
                newData.LastUpdateDate = BdDateTime.Now();
                newData.LastUpdateBy = userId;
                _unitOfWork.FeeSubCategoryRepo.Add(newData);
                _unitOfWork.SaveChanges();
                
                var FeeSubCategoryId = newData.FeeSubCategoryId;
                foreach(var j in model.FeeTypeList)
                {
                    var startDate = Convert.ToDateTime(DateTimeHelperService.ConvertBDDateStringToDateTimeObject(j.StartDate + "/" + DateTime.Now.Year));
                    var endDate = Convert.ToDateTime(DateTimeHelperService.ConvertBDDateStringToDateTimeObject(j.EndDate + "/" + DateTime.Now.Year));

                    var periodName = startDate.ToString("MMM-dd") + "-"+ endDate.ToString("MMM-dd");
                    var feeType = new SubCategoryFeePeriod();
                    feeType.CreatedBy = userId;
                    feeType.CreationDate = BdDateTime.Now();
                    //feeType.DueDate = new DateTime(2000, dueDate.Month, dueDate.Day);
                    feeType.StartDate = new DateTime(2000,startDate.Month, startDate.Day);
                    feeType.EndDate =  new DateTime(2000, endDate.Month, endDate.Day);
                    feeType.PeriodName = periodName;
                    feeType.FeeSubCategoryId = FeeSubCategoryId;
                    feeType.BillPeriodId = 0;
                    feeType.LastUpdateBy = userId;
                    feeType.Year = j.Year;
                    feeType.Month = j.Month;
                    feeType.Day = j.Day;
                    feeType.LastUpdateDate = BdDateTime.Now();
                    _unitOfWork.SubCategoryFeePeriodsRepo.Add(feeType);
                    _unitOfWork.SaveChanges();
                    var allYear = _unitOfWork.YearsRepo.GetAll().Select(i => new { i.YearId, i.YearName }).ToList();
                    foreach (var year in allYear)
                    {
                        var existingBillPeriod = _unitOfWork.SubCategoryYearsRepo.FirstOrDefault(s => s.BillPeriodId == feeType.BillPeriodId&& s.YearId == year.YearId);
                        if (existingBillPeriod == null)
                        {
                            var subcategoryYear = new SubCategoryYear();
                            subcategoryYear.BillPeriodId = feeType.BillPeriodId;
                            subcategoryYear.YearId = year.YearId;
                            _unitOfWork.SubCategoryYearsRepo.Add(subcategoryYear);
                            _unitOfWork.SaveChanges();
                        }
                    }
                }
            }
            else
            {
                var existingData = _unitOfWork.FeeSubCategoryRepo.Get(model.FeeSubCategoryId);
                if (existingData != null)
                {
                    existingData.FeeCategoryId = model.FeeCategoryId;
                    existingData.FeeSubCategoryName = model.FeeSubCategoryName;
                    existingData.Amount = model.Amount;
                    existingData.FeeType = model.FeeType;
                    existingData.LastUpdateDate = BdDateTime.Now();
                    existingData.LastUpdateBy = userId;
                }
                //var headerId = model.FeeSubCategoryId;
                foreach (var j in model.FeeTypeList)
                {
                    if (j.SubCategoryFeeTypeId > 0)
                    {
                        var existingType = _unitOfWork.SubCategoryFeePeriodsRepo.Get(j.SubCategoryFeeTypeId??0);
                        //existingType.DueDate = new DateTime(2000, dueDate.Month, dueDate.Day);
                        existingType.Year = j.Year;
                        existingType.Month = j.Month;
                        existingType.Day = j.Day;
                        existingType.LastUpdateBy = userId;
                        existingType.LastUpdateDate = BdDateTime.Now();
                    }
                }
                _unitOfWork.SaveChanges();

                //add this year if not existd
                int CurrentYear = DateTime.Now.Year;
                var thisYear = _unitOfWork.YearsRepo.FirstOrDefault(s => s.YearName == CurrentYear);
                if (thisYear == null)
                {
                    var newYearData = new Year();
                    newYearData.YearName = CurrentYear;
                    _unitOfWork.YearsRepo.Add(newYearData);
                    _unitOfWork.SaveChanges();
                }
                var allYear = _unitOfWork.YearsRepo.GetAll().Select(i => new { i.YearId, i.YearName }).ToList();
                // all this category to all year if not exists
                var existingFeeType = _unitOfWork.SubCategoryFeePeriodsRepo.Find(w => w.FeeSubCategoryId == model.FeeSubCategoryId).ToList();
                foreach (var i in existingFeeType)
                {
                    foreach (var year in allYear)
                    {
                        var existingBillPeriod = _unitOfWork.SubCategoryYearsRepo.FirstOrDefault(s => s.BillPeriodId == i.BillPeriodId && s.YearId == year.YearId);
                        if (existingBillPeriod == null)
                        {
                            var subcategoryYear = new SubCategoryYear();
                            subcategoryYear.BillPeriodId = i.BillPeriodId;
                            subcategoryYear.YearId = year.YearId;
                            _unitOfWork.SubCategoryYearsRepo.Add(subcategoryYear);
                            _unitOfWork.SaveChanges();
                        }
                    }
                }
                //_unitOfWork.SaveChanges();
            }
            return _unitOfWork.SaveChanges();
        }

        public List<FeeSubCategory> GetBetween(int start, int displayLength, string searchValue, out int totalLength)
        {
            var allValues = _unitOfWork.FeeSubCategoryRepo.GetAllQueryable();

            if (!string.IsNullOrEmpty(searchValue))
            {
                allValues = allValues.Where(w => (!string.IsNullOrEmpty(w.FeeSubCategoryName) && w.FeeSubCategoryName.ToLower().Contains(searchValue.ToLower())) || (!string.IsNullOrEmpty(w.Amount.ToString()) && w.Amount.ToString().ToLower().Contains(searchValue.ToLower())));
            }

            var displayedValues = displayLength == -1 ? allValues
                .OrderBy(o => o.FeeSubCategoryId).Skip(start).OrderBy(o=>o.FeeSubCategoryId).ToList() : allValues
                .OrderBy(o => o.FeeSubCategoryId).Skip(start)
                .Take(displayLength).ToList();
            totalLength = allValues.Count();
            return displayedValues;
        }
    }
}
