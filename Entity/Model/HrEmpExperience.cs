using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entity.Model
{
    public partial class HrEmpExperience : BaseEntity
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int EmpHeaderId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ExperienceId { get; set; }

        [StringLength(50)]
        public string CompanyName { get; set; }

        [StringLength(50)]
        public string Designation { get; set; }

        //[Key]
        //[Column(Order = 1)]
        //[DatabaseGenerated(DatabaseGeneratedOption.None)]
        //public int CompanyId { get; set; }

        //[Key]
        //[Column(Order = 2)]
        //[DatabaseGenerated(DatabaseGeneratedOption.None)]
        //public int PositionId { get; set; }

        [StringLength(50)]
        public string Location { get; set; }

        [StringLength(200)]
        public string Responsibility { get; set; }

        [StringLength(50)]
        public string StartDate { get; set; }

        [StringLength(50)]
        public string EndDate { get; set; }

    }
}
