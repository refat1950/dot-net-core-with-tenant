using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entity.Model
{
    public partial class SchoolType : BaseEntity
    {
        public SchoolType()
        {
            SocialLinks = new HashSet<SchoolLinks>();
        }

        [Key]
        public int SocialTypeId { get; set; }

        [Column("SocialType")]
        [Required]
        [StringLength(100)]
        public string SocialType1 { get; set; }

        public virtual ICollection<SchoolLinks> SocialLinks { get; set; }
    }
}
