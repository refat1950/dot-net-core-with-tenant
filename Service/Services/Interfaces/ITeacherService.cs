﻿using System;
using Services.View_Model;

namespace Services.Interfaces
{
    public interface ITeacherService
    {
        VmTeacherDashbord TeacherInfo(int userId, DateTime routineDate);
        string GetTeacherName(int subjectid);
    }
}