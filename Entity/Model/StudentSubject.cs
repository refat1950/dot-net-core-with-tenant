
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class StudentSubject : BaseEntity
    {
        [Key, Column(Order = 0)]
        public int StudentSubId { get; set; }

        [Column(Order = 1)]
        public int SessionId { get; set; }

        [Column(Order = 2)]
        public int StudentHeaderId { get; set; }

        [Column(Order = 3)]
        public int ClassId { get; set; }

        [Column(Order = 4)]
        public int SubjectId { get; set; }

        [Column(Order = 5)]
        public int SubGroupId { get; set; }

        [Column(Order = 6)]
        public int SectionId { get; set; }

        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}

