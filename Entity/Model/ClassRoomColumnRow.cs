﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class ClassRoomColumnRow : BaseEntity
    {
        [Key]
        public int ClassRoomColumnRowId { get; set; }
        public int ClassRoomColumnId { get; set; }
        public int RowSerial { get; set; }
        public int NoofSeat { get; set; }
        public int SeatSerial { get; set; }
        public SeatStatus Status { get; set; }
        public virtual ClassRoomColumn ClassRoomColumn { get; set; }
        public virtual IEnumerable<ExamSeatPlan> ExamSeatPlans { get; set; }
    }

    public enum SeatStatus
    {
        Added = 1,
        Canceled = 2,
        Booked = 3,
        NotBooked = 4
    }
}
