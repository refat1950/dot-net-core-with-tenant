using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entity.Model
{
    public partial class SchoolLinks : BaseEntity
    {
        [Key]
        public int SocialId { get; set; }

        public int SocialTypeId { get; set; }

        public int SchoolId { get; set; }

        [StringLength(150)]
        public string SocialUrl { get; set; }

        [StringLength(150)]
        public string SocialIcon { get; set; }

        public virtual School School { get; set; }

        public virtual SchoolType SocialType { get; set; }
    }
}
