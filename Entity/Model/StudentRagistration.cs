﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class StudentRagistration : BaseEntity
    {

        [Key]
        public string StudentAdmissionId { get; set; }

        [StringLength(30)]
        public string StudentFirstName { get; set; }

        [StringLength(30)]
        public string StudentMiddleName { get; set; }

        [StringLength(30)]
        public string StudentLastName { get; set; }

        public int? AdmissionClassId { get; set; }

        [StringLength(30)]
        public string FatherFirstName { get; set; }

        [StringLength(30)]

        public string FatherMiddleName { get; set; }

        [StringLength(30)]
        public string FatherLastName { get; set; }

        [StringLength(30)]
        public string MotherFirstName { get; set; }

        [StringLength(30)]
        public string MotherMiddleName { get; set; }

        [StringLength(30)]
        public string MotherLastName { get; set; }

        [StringLength(30)]
        public string PresHoldingNo { get; set; }

        [StringLength(50)]
        public string PresStreet { get; set; }

        [StringLength(50)]
        public string PresArea { get; set; }
        //public string PermPostCode { get; set; }
        public int? PresThanaId { get; set; }
        public int? PresCityId { get; set; }
        public int? PresDivisionId { get; set; }
        public int? PresCountryId { get; set; }
        public DateTime? Dob { get; set; }
        public string Gender { get; set; }

        [StringLength(30)]
        public string Mobile { get; set; }

        [StringLength(50)]
        public string Email { get; set; }





    }
}
