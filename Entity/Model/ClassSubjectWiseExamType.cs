﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class ClassSubjectWiseExamType : BaseEntity
    {
        [Key]
        public int ClassSubjectWiseExamTypeId { get; set; }
        public int ClassId { get; set; }
        public int ExamId { get; set; }
        public int SubjectId { get; set; }
        public int ExamTypeId { get; set; }
        public decimal BaseMark { get; set; }
        public decimal PassMark { get; set; }
        public int? SessionId { get; set; }
    }
}
