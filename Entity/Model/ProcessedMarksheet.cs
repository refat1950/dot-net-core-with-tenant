using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entity.Model
{
    public class ProcessedMarksheet : BaseEntity
    {
        [Key]
        public int Id { get; set; }
        public int StudentHeaderId { get; set; }
        public int SessionId { get; set; }
        public int ClassId { get; set; }
        public int SectionId { get; set; }
        public int? CampusId { get; set; }
        public int ExamId { get; set; }
        public int SubjectId { get; set; }
        public int ExamTypeId { get; set; }
        public decimal BaseMarks { get; set; }
        public decimal ObtainedMarks { get; set; }
        public string ObtainedMarksText { get; set; }
    }
}
