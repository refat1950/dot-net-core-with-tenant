using System;
using System.ComponentModel.DataAnnotations;

namespace Entity.Model
{
    public partial class FndPage : BaseEntity
    {
        [Key]
        public int PageId { get; set; }

        [Required]
        [StringLength(50)]
        public string PageName { get; set; }

        [StringLength(100)]
        public string Url { get; set; }

        [StringLength(100)]
        public string PageHeading { get; set; }

        public int? ModuleId { get; set; }


    }
}
