﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Web.Models.View_Model;

namespace Service.View_Model
{
    public class VmExamSchedule
    {
        public int ExamScheduleId { get; set; }
        public int SessionId { get; set; }
        public string SessionName { get; set; }
        public int ExamId { get; set; }
        public string ExamName { get; set; }
        public string RoomNames { get; set; }
        public string RoomIds { get; set; }
        public string InvigilatorIds { get; set; }
        public string Invigilators { get; set; }
        public bool IsAnyDateWiseSubjectAssigned { get; set; }
        public bool? IsExamSeatPlanPublished { get; set; }
        public bool? IsExamRoutinePublished { get; set; }
        public List<VmExamScheduleLine> vmExamScheduleLines { get; set; }
    }

    public class VmExamScheduleLine
    {
        public int ExamScheduleLineId { get; set; }
        public int SessionId { get; set; }
        public string SessionName { get; set; }
        public int ClassId { get; set; }
        public string ClassName { get; set; }
        public int SectionId { get; set; }
        public string SectionName { get; set; }
        public int ExamId { get; set; }
        public string ExamName { get; set; }
        public string ExamStartdate { get; set; }
        public string ExamEnddate { get; set; }
        public List<VmDateWiseSubject> vmDateWiseSubjects { get; set; }
    }

    public class VmDateWiseSubject
    {

        public int DateWiseSubjectId { get; set; }
        public int ExamScheduleLineId { get; set; }
        public DateTime Date { get; set; }
        public string DateText { get; set; }
        public int SelectdSubjectid { get; set; }
        public string SubjectName { get; set; }
        public List<VmSelectListService> SubjectList { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public bool isDateAssigned { get; set; }
    }


    public class VmExamRoomInvestigator
    {
        public List<int> ExamRoomInvestigatorIds { get; set; }
        public int SessionId { get; set; }
        public string SessionName { get; set; }
        public int ExamId { get; set; }
        public string ExamName { get; set; }
        public int RoomId { get; set; }
        public string RoomName { get; set; }
        public bool? IsPublished { get; set; }
        public List<VmExamDateWiseInvestigator> vmExamDateWiseInvestigators { get; set; }

    }
    public class VmExamDateWiseInvestigator
    {
        public List<int> DateWiseInvestigatorIds { get; set; }
        public string Date { get; set; }
        public DateTime Datetime { get; set; }
        public string Investigators { get; set; }
    }
    public class VmDateWiseRoom
    {
        public string ExamName { get; set; }
        public string Date { get; set; }
        public string RoomName { get; set; }
    }


    public class VmRoomsData
    {
        public int RoomId { get; set; }
        public string RommName { get; set; }
        public List<VmInvigilator> Invigilators { get; set; }
    }

    public class VmInvigilator
    {
        public int InvigilatorId { get; set; }
        public string InvigilatorName { get; set; }
     
    }

    public class VmExamRoutine
    {
        public int ExamScheduleId { get; set; }
        public int SubjectId { get; set; }
        public string SubjectName { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }
        public string RoomName { get; set; }
        public int RoomId { get; set; }
    }



}