﻿using Services.View_Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Services.CoreServices
{
    public static class CoreServices
    {
        //public static List<VmSelectList> GetEnumList<T>() where T : struct
        //{
        //    var list = new List<VmSelectList>();
        //    foreach (int e in Enum.GetValues(typeof(T)))
        //    {
        //        var d = new VmSelectList();
        //        d.Id = e;
        //        d.Name = Enum.GetName(typeof(T), e);
        //        list.Add(d);
        //    }
        //    return list;
        //}
        public static DateTime TruncateTime(DateTime date)
        {
            return date.Date;
        }
        public static DateTime TruncateTime(DateTime? date)
        {
            return date.Value.Date;
        }
        public static List<VMSelectList> GetEnumList<T>() where T : struct // get enum list with discription
        {
            var list = new List<VMSelectList>();
            var counter = 1;
            foreach (int e in Enum.GetValues(typeof(T)))
            {
                var d = new VMSelectList();
                d.Id = e;
                d.Name = GetEnumDescriptions<T>(counter);
                list.Add(d);
                counter++;
            }
            return list;
        }


        private static string GetEnumDescriptions<T>(int counter)
        {
            var type = typeof(T);
            if (!type.IsEnum) throw new ArgumentException("Only Enum types allowed");
            var result = "";
            var c = 1;
            foreach (var value in Enum.GetValues(type).Cast<Enum>())
            {
                result =  getEnumDescriptions(value);
                if (c == counter)
                    break;
                c++;
            }
            return result;
        }

        public static string getEnumDescriptions(Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());
            DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

            if (attributes != null && attributes.Length > 0)
                return attributes[0].Description;
            else
                return value.ToString();
        }
    }
}
