﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class HrPaySlipTransactionLog : BaseEntity
    {
        [Key]
        public int HrPaySlipTransactionLogId { get; set; }
        public int HrPaySlipTransactionMasterLogId { get; set; }  // for relation with master log table
        public int PayHeadId { get; set; }
        public decimal Unit { get; set; }
        public decimal CalculatedValue { get; set; }
        public bool IsPercentage { get; set; }
        public virtual HrPaySlipTransactionMasterLog HrPaySlipTransactionMasterLog { get; set; }
        //attribute1 : hours
        //attribute2 : minutes
        //attribute3 : addition/deduction
    }
}
