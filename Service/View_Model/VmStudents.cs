﻿using Entity.Model;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Web.Models.View_Model
{
    public class VmStudentData
    {
        public int SerialNo { get; set; }
        public int StudentHeaderId { get; set; }

        [StringLength(30)]
        public string StudentId { get; set; }

        [Required(ErrorMessage = "Please Provide First Name.")]
        public string FirstName { get; set; }

        [StringLength(30)]
        public string MiddleName { get; set; }

        [Required(ErrorMessage = "Please Provide Last Name.")]
        public string LastName { get; set; }
        public string SassionName { get; set; }

        [StringLength(50)]
        public string NickName { get; set; }
        public string PunchCardNo { get; set; }


        public IFormFile File { get; set; }
        [StringLength(30)]
        public string PresHoldingNo { get; set; }

        [StringLength(50)]
        public string PresStreet { get; set; }

        public string ClassName { get; set; }
        public string ShiftName { get; set; }
        public string Section { get; set; }
        public string StudentStatus { get; set; }
        [StringLength(50)]
        public string PresArea { get; set; }
        public string Age { get; set; }

        public int? PresThanaId { get; set; }

        [StringLength(10)]
        public string PresPostCode { get; set; }

        public int? PresCityId { get; set; }

        public int? DeviceNumber { get; set; }

        [Required(ErrorMessage = "Please Provide Roll Number.")]
        public int RollNumber { get; set; }

        public int? PresDivisionId { get; set; }

        public int? PresCountryId { get; set; }

        [StringLength(30)]
        public string PermHoldingNo { get; set; }

        [StringLength(50)]
        public string PermStreet { get; set; }

        [StringLength(50)]
        public string PermArea { get; set; }

        [StringLength(10)]
        public string PermPostCode { get; set; }

        public int? PermThanaId { get; set; }

        public int? PermCityId { get; set; }

        public int? PermDivisionId { get; set; }

        public int? PermCountryId { get; set; }

        public DateTime? Dob { get; set; }

        public int? BirthCity { get; set; }

        public int? BirthCountry { get; set; }

        [StringLength(60)]
        public string PlaceOfBirth { get; set; }

        [Required(ErrorMessage = "Please Select Admission Date.")]
        public string AdmissionDate { get; set; }
        public string ReadmissionDate { get; set; }

        [Required(ErrorMessage = "Please Select Admission Session.")]
        public int? AdmissionSessionId { get; set; }
        public int? ReadmissionSessionId { get; set; }

        [Required(ErrorMessage = "Please Select Admission Class.")]
        public int? AdmissionClassId { get; set; }

        [Required(ErrorMessage = "Please Select Admission Section.")]
        public int? AdmissionSectionId { get; set; }

        [Required(ErrorMessage = "Please Select Admission Shift.")]
        public int? AdmissionShift { get; set; }

        [Required(ErrorMessage = "Please Select Current Session.")]
        public int? SessionId { get; set; }

        [Required(ErrorMessage = "Please Select Current Class.")]
        public int? ClassId { get; set; }

        [Required(ErrorMessage = "Please Select Current Section.")]
        public int? SectionId { get; set; }

        [Required(ErrorMessage = "Please Select Current Shift.")]
        public int? Shift { get; set; }

        public int? CampusId { get; set; }

        public string EndDate { get; set; }

        [Required(ErrorMessage = "Please Select Birth Date.")]
        public string BirthDate { get; set; }
        public bool? Status { get; set; }

        [Required(ErrorMessage = "Please Select Status.")]
        public int StatusId { get; set; }
        [StringLength(10)]
        public string Gender { get; set; }

        [Required(ErrorMessage = "Please Select Gender.")]
        public int GenderId { get; set; }

        [StringLength(30)]
        public string Religion { get; set; }
        public int? ReligionId { get; set; }

        [StringLength(30)]
        public string ResPhone { get; set; }

        [StringLength(30)]
        public string Mobile { get; set; }

        [StringLength(100)]
        public string Email { get; set; }
        public string LocGuardianOfficeAddress { get; set; }

        [StringLength(10)]
        public string BloodGroup { get; set; }
        public int? BloodId { get; set; }
        [StringLength(60)]
        public string Nationality { get; set; }
        public int? NationalityId { get; set; }

        [StringLength(30)]
        public string BirthCertificateNo { get; set; }

        [StringLength(30)]
        public string PassportNo { get; set; }

        [StringLength(200)]
        public string MedicalHistory { get; set; }

        [StringLength(200)]
        public string EmergencyMedicalAction { get; set; }

        [StringLength(30)]
        public string FatherFirstName { get; set; }

        [StringLength(30)]
        public string FatherMiddleName { get; set; }

        [StringLength(30)]
        public string FatherLastName { get; set; }

        [StringLength(50)]
        public string FatherEducation { get; set; }

        [StringLength(200)]
        public string FatherEducationDetails { get; set; }

        [StringLength(30)]
        public string FatherOccupation { get; set; }

        [StringLength(150)]
        public string FatherOccupationDetails { get; set; }

        [StringLength(200)]
        public string FatherOfficeAddress { get; set; }

        [StringLength(30)]
        public string FatherOfficePhone { get; set; }

        [StringLength(30)]
        public string FatherMobile { get; set; }

        [StringLength(30)]
        public string FatherNationalId { get; set; }

        [StringLength(30)]
        public string FatherPassportNo { get; set; }

        [StringLength(30)]
        public string MotherFirstName { get; set; }

        [StringLength(30)]
        public string MotherMiddleName { get; set; }

        [StringLength(30)]
        public string MotherLastName { get; set; }

        [StringLength(50)]
        public string MotherEducation { get; set; }

        [StringLength(200)]
        public string MotherEducationDetails { get; set; }

        [StringLength(30)]
        public string MotherOccupation { get; set; }

        [StringLength(150)]
        public string MotherOccupationDetails { get; set; }

        [StringLength(200)]
        public string MotherOfficeAddress { get; set; }

        [StringLength(30)]
        public string MotherOfficePhone { get; set; }

        [StringLength(30)]
        public string MotherMobile { get; set; }

        [StringLength(30)]
        public string MotherNationalId { get; set; }

        [StringLength(30)]
        public string MoherPassportNo { get; set; }

        [StringLength(30)]
        public string LocGuardianFirstName { get; set; }

        [StringLength(30)]
        public string LocGuardianMiddleName { get; set; }

        [StringLength(30)]
        public string LocGuardianLastName { get; set; }

        [StringLength(30)]
        public string LocGuardianRelationship { get; set; }

        [StringLength(30)]
        public string LocGuardianMobile { get; set; }

        [StringLength(30)]
        public string LocalGurdianNationalId { get; set; }

        [StringLength(30)]
        public string LocalGurdianPassportNo { get; set; }

        [StringLength(500)]
        public string StudentPhotoPath { get; set; }

        [StringLength(15)]
        public string ProximityNum { get; set; }

        [StringLength(15)]
        public string ExitReasons { get; set; }


        public virtual FndFlexValue FndFlexValues { get; set; }
    }



}