﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Entity.Model
{
    public class FeeSubCategory : BaseEntity
    {
        [Key]
        public int FeeSubCategoryId { get; set; }
        public string FeeSubCategoryName { get; set; }
        public int FeeCategoryId { get; set; }
        public int? SessionId { get; set; }
        public decimal Amount { get; set; }
        public int FeeType { get; set; }
        public virtual FeeCategory FeeCategory { get; set; }
        public virtual ICollection<SubCategoryFeeType> SubCategoryFeeType { get; set; }
        public virtual ICollection<FeeSubCategoryFine> FeeSubCategoryFine { get; set; }
        public virtual ICollection<FeeSubCategoryWaiver> FeeSubCategoryWaiver { get; set; }
    }
}
