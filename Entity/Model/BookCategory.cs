using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Entity.Model
{
    public partial class BookCategory : BaseEntity
    {
        [Key]
        public int BookCategoryId { get; set; }
        public int? ParentCategoryId { get; set; }
        public string CategoryName { get; set; }
        public string SectionCode { get; set; }
    }
}
