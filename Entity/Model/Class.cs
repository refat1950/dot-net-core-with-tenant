using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Entity.Model
{
    public partial class Class : BaseEntity
    {
        [Key]
        public int ClassId { get; set; }

        [StringLength(50)]
        public string ClassName { get; set; }

        [StringLength(10)]
        public string ClassLevel { get; set; }

        public int MinAge { get; set; }
        public int MaxAge { get; set; }
        public int? te { get; set; }
        public virtual List<ClassExamGrade> ClassExamGrade { get; set; }
        public virtual List<StudentMarks> StudentMarks { get; set; }
    }
}
// IsOptional is saved on attribute2