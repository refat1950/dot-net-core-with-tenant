﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class HrPosition : BaseEntity
    {
        [Key]
        public int PostionId { get; set; }

        [Required]
        [StringLength(100)]
        public string PostionName { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public bool? Status { get; set; }

        public int? LocationId { get; set; }

        public int? OrganizationId { get; set; }

        public int? SuccessorPositionId { get; set; }

        public int? SupervisorPositionId { get; set; }

        public int? EntryGradeId { get; set; }

        public int? EntryLevel { get; set; }

        public int? PriorPositionId { get; set; }

        [StringLength(50)]
        public string WorkingHours { get; set; }

        [StringLength(50)]
        public string StartTime { get; set; }

        [StringLength(50)]
        public string EndTime { get; set; }

        public int? ProvisionPeriod { get; set; }

        public PositionType? PostionTypeId { get; set; }

        //new field
        public int? HeadCount { get; set; }
        public Decimal? FTE { get; set; }
        public Decimal? BergainningUnit { get; set; }
        public DateTime? earliestHiringdate { get; set; }
        public int PayrollId { get; set; }
        public Decimal? SalaryBasis { get; set; }
        public PositionStatus? StatusId { get; set; }
        public bool? IsPermanent { get; set; }
        public bool? IsSeasonal { get; set; }
    }

    public enum PositionType
    {
        Single = 1,
        Shared = 2
    }
    public enum PositionStatus
    {
        Active = 1,
        Inactive = 2
    }
}
