﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Entity.Model
{
    public class UserLoginTracer : BaseEntity
    {
        [Key]
        public int LoginTracerId { get; set; }
        public int? LoginUserId { get; set; }
        public string IPAddress { get; set; }
        public string CountryName { get; set; }
        public string Region { get; set; }
        public string CityName { get; set; }
        public string PostalCode { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string TimeZone { get; set; }
        public string Organization { get; set; }
        public DateTime? LoggedInDate { get; set; }
        public DateTime? LoggedOutDate { get; set; }
        public bool? IsBlocked { get; set; }
        public string BlockReason { get; set; }
        public DateTime? BlockDate { get; set; }
        public string RedirectURL { get; set; }
        public int? Status { get; set; }
    }
}
