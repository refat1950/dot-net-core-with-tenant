﻿using System;

namespace Services.View_Model
{
    public class VmAttendanceStudent
    {
        public int STUDENT_HEADER_ID { get; set; }

        public string CARD_NUMBER { get; set; }

        public DateTime PUNCH_DATE { get; set; }

        public string START_TIME { get; set; }

        public string END_TIME { get; set; }

        public TimeSpan IN_TIME { get; set; }

        public TimeSpan OUT_TIME { get; set; }

        public int? TIME_LATE { get; set; }

        public int? EARLY_LEAVE { get; set; }

        public int? DOOR_NUMBER { get; set; }
    }

    public class AttendenceInfoViewMOdel
    {
        public int Serial { get; set; }
        public DateTime Date { get; set; }
        public string InTime { get; set; }
        public string OutTime { get; set; }
        public int DoorNo { get; set; }
        public string Type { get; set; }
    }

    public class AttendenceViewModel
    {
        public string AttenTotalDay { get; set; }

        public int AttenPresentOnTime { get; set; }

        public int AttenLate { get; set; }

        public int AttenAbsent { get; set; }

        public string Atten1StSeme { get; set; }

        public string Atten2NdSeme { get; set; }

        public string AttenAverage { get; set; }

        public string AttenComment { get; set; }
    }


    public class VmStuLeaveApplyList
    {
        public int ApplicationId { get; set; }
        public string LeaveCategory { get; set; }
        public int LeaveCategoryId { get; set; }
        public string LeaveStartDate { get; set; }
        public string LeaveEndDate { get; set; }
        public string LeaveApplyDate { get; set; }
        public string Subject { get; set; }
        public string LeaveStatus { get; set; }
        public string StudentName { get; set; }
        public string Comment { get; set; }
        public string ClassName { get; set; }
        public string SectionName { get; set; }
    }
}