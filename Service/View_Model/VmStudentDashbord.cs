﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Services.View_Model
{
    public class VmStudentDashbord
    {
        public string StudentFullName { get; set; }

        public string StudentId { get; set; }

        public int? ClassId { get; set; }

        public string Class { get; set; }

        public int? SectionId { get; set; }

        public string Section { get; set; }

        public int? CampusId { get; set; }

        public string Campus { get; set; }

        public string Height { get; set; }

        public string Weight { get; set; }

        public string Mail { get; set; }

        public string PhotoPath { get; set; }
        public string AttenTotalDay { get; set; }

        public int AttenPresentOnTime { get; set; }

        public int AttenLate { get; set; }

        public int AttenAbsent { get; set; }

        public string Atten1StSeme { get; set; }

        public string Atten2NdSeme { get; set; }

        public string AttenAverage { get; set; }

        public string AttenComment { get; set; }

        public string RankSession { get; set; }

        public decimal RankPercentageObtained { get; set; }

        public decimal RankPercentageLeft { get; set; }

        public string RankTotalPerObtain { get; set; }

        public string RankPosSection { get; set; }

        public string RankPosClass { get; set; }

    }

    public class StudentClassRoutineViewModel
    {
        public string Period { get; set; }

        public string Subject { get; set; }
    }

    public class StudentNoticeBoardViewModel
    {
        public string Text { get; set; }

        public string Link { get; set; }
    }

    public class StudentDocumentViewModel
    {
        public DateTime? Date { get; set; }

        public string DocumentType { get; set; }

        public string Link { get; set; }
    }

    public class StudentSubjectMarksViewModel
    {
        public int SubjectId { get; set; }
        public string SubjectName { get; set; }
        public string SubjectCode { get; set; }
        public decimal Marks50 { get; set; }
        public decimal Mark100 { get; set; }
        public decimal Mark150 { get; set; }
        public decimal Avg { get; set; }
        public string Grade { get; set; }
        public string color { get; set; }
    }

    public class StudentBillInfoViewModel
    {
        public string CurrentStatus { get; set; }

        public string LastPaidMonth { get; set; }

        public string DueAmmount { get; set; }

        public string Notice { get; set; }
    }


    public class VmRoutineList
    {
        public int DayOfWeek { get; set; }
        public string Day { get; set; }
        public List<VmRoutineData> Data { get; set; }
    }
    public class VmRoutineData
    {
        public int RoutineId { get; set; }
        public int? SubjectId { get; set; }
        public string SubjectName { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string Teachernames { get; set; }
    }

}