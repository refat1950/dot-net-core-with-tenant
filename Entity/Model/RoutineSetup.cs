using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entity.Model
{
    public partial class RoutineSetup : BaseEntity
    {
        public int RoutineSetupId { get; set; }

        public int ClassId { get; set; }

        [StringLength(10)]
        public string SectionId { get; set; }

        [Required]
        [StringLength(50)]
        public string Period { get; set; }

        [StringLength(50)]
        public string StartTime { get; set; }

        [StringLength(50)]
        public string EndTime { get; set; }

        public bool? Isbreak { get; set; }


    }
}
