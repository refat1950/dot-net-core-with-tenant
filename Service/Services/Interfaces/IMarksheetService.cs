﻿using System;
using System.Collections.Generic;
using System.Linq;
using Entity.Model;
using Services.View_Model;
using Web.Models.View_Model;

namespace Services.Interfaces
{
    public interface IMarksheetService
    {
        VmMarkSheet ProcesseMarksData(int StudentHeaderId, int sessionId, int examid, int ClassId, int SectionId, List<ExamGrade> examGrades);
        VmMarkSheet GetProcessedMarkSheetData(int studentHeaderId, int sessionId, int examid, int classId, int sectionId, List<ExamGrade> examGrades);
        void StoreProcessedMarkSheet(VmMarkSheet vmMarkSheet ,int userid);
        bool IsMarkSeetProcessed(int studentHeaderId, int sessionId, int examid, int classId, int sectionId);
    }
}
