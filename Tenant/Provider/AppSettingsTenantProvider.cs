﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Tenant.Model;

namespace Tenant.Provider
{
    public class AppSettingsTenantProvider : ITenantProvider
    {
        private int tenantId = 0;
        private string connectionString;
        private IConfiguration configuration;
        public AppSettingsTenantProvider(IHttpContextAccessor accessor, IConfiguration configuration)
        {
            this.configuration = configuration;
            var host = accessor.HttpContext.Request.Host.Host; //.Url.Host;
            var serial = 0;
            var allTenants = configuration.GetSection("TenantSetings").GetChildren().Select(s => new KeyValueList
            {
                Serial = serial++,
                Key = s.Key,
                Value = s.Value
            }).OrderByDescending(o => o.Serial).ToList();

            connectionString = allTenants.FirstOrDefault(t => t.Key == host) != null ? allTenants.FirstOrDefault(t => t.Key == host).Value : "";
        }
        public int GetTenantId()
        {
            return tenantId;
        }

        public string GetConnectionString()
        {
            return connectionString;
        }

        public string GetConnectionStringName()
        {
            return connectionString;
        }
    }
}
