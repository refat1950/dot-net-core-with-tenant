﻿using Service.ViewModel.Payroll;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.DataService
{
    public interface IEmployeeAttendanceService
    {
        List<VmDateWiseAttendance> GetBetween(int start, int displayLength, string searchValue, string selectedDate, out int totalLength);
        List<VmDateWiseAttendance> GetDayAttendance(string selectedDate);
        VmDateWiseAttendance GetDayAttendanceData(DateTime date, int EmpHeaderId);
    }
}
