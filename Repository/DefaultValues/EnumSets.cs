﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.DefaultValues
{
    public enum ConfigValueSets
    {
        
        Status= 1,
        ClassLevel= 2,
        SessionType= 3,
        Country = 4,
        District = 5,
        Division = 6,
        Religion = 7,
        Shift = 8,
        Thana = 9,
        Gender = 10,
        PositionType = 11,
        BloodGroup = 12,
        Nationality = 13,
        AttendanceType = 14,
        PayHeadType = 15,
        PaymentType = 16,
        MaritalStatus = 17,
        QualificationType = 18,
        Major = 19,
        Institution = 20,
        Board = 21,
        Department = 22,
        ClassGroup = 23,
        JobLocation = 24,
        BaseLocation = 25,
        LeaveCategory = 26,
        StudentCategory = 27,
        DocumentType = 28,
        EventType = 29,
        Designation = 30,
        Weekend = 31,
        NoticeType = 32,
        NoticeFor = 33
    }

    public enum FeeSubCategoryType
    {
        Annual = 1,
        Bi_Annual = 2,
        Tri_Annual = 3,
        Quarterly = 4,
        Monthly = 5,
        One_Time = 6
    }

    public enum IncrementType
    {
        Monthly = 1,
        Daily = 2
    }
    public enum IsPercentage
    {
        Amount = 1,
        Percentage = 2
    }
    public enum Incremental
    {
        Fixed = 1,
        Incremental = 2
    }
    public enum UserCookieData
    {
        UserName,
        UserId,
        CompanyId,
        UserOrCompanyLogo,
        RememberMePassword,
        UserCreatedDate,
        UserIdUserRoleAndUserName,
        UserFirstName,
        UserLastName,
        DisplayName,
        PaidUserStatus,
        TrialUserStatus,
        SelectedAssesRiskId,
        PackageName,
        LegislationModalStatus
    }

    public enum QueueRequestStatus
    {
        Success = 1,
        Pending = 2,
        Failed = 3
    }

    public enum QueueType
    {
        Sms = 1,
        Email = 2
    }
 
}
