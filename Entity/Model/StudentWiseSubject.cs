﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class StudentWiseSubject : BaseEntity
    {
        [Key]
        public int StudentWiseSubjectId { get; set; }
        public int SubjectId { get; set; }
        public bool IsSubjectAssigned { get; set; }
        public int StudentSubjectLineId { get; set; }
        public StudentSubjectLine StudentSubjectLine { get; set; }
        public virtual Subject Subject { get; set; }
    }
}
