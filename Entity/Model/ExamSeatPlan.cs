﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class ExamSeatPlan : BaseEntity
    {
        [Key]
        public int ExamSeatPlanId { get; set; }
        public int ClassRoomColumnRowId { get; set; } // each seat
        public int RollNumber { get; set; }
        public int DateWiseSubjectId { get; set; }
        public int ExamScheduleLineId { get; set; }
        public int RoomSetupHeaderId { get; set; }  //layoutid
        public virtual ExamScheduleLine ExamScheduleLine { get; set; }
        public virtual ClassRoomColumnRow ClassRoomColumnRow { get; set; }
    }
}
