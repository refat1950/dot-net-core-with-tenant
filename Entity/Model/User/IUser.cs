﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    /// <summary>
    ///     Minimal interface for a user with id and username
    /// </summary>
    //public interface IUser : IUser
    //{
    //}

    /// <summary>
    ///     Minimal interface for a user with id and username
    /// </summary>
    /// <typeparam name="TKey"></typeparam>
    public interface IUser
    {
        /// <summary>
        ///     Unique key for the user
        /// </summary>
        int UserId { get; }

        /// <summary>
        ///     Unique username
        /// </summary>
        string UserName { get; set; }
        //bool? EmailConfirmed { get; set; }
        //string EmailAddress { get; set; }
        //DateTime? LockoutDate { get; set; }
        //int? AccessFailedCount { get; set; }
    }
}
