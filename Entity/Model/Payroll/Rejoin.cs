﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class Rejoin : BaseEntity
    {
        [Key]
        public int RejoinId { get; set; }
        public int EmpHeaderId { get; set; }
        public DateTime RejoinDate { get; set; }
        public string AppointmentLetterUrl { get; set; }
        public int? RejoinPositionId { get; set; }
    }
}
