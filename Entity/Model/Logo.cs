using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entity.Model
{
    public partial class Logo : BaseEntity
    {
        [Key]
        public int LogoId { get; set; }

        public int LogoTypeId { get; set; }

        public int SchoolId { get; set; }

        [Required]
        [StringLength(100)]
        public string LogoTitle { get; set; }

        [Required]
        [StringLength(150)]
        public string LogoUrl { get; set; }

        [StringLength(250)]
        public string LogoDetails { get; set; }

        public virtual LogoType LogoType { get; set; }

        public virtual School School { get; set; }
    }
}
