﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Repository.Context;
using Entity.Model;
using Entity.Model.Transaction;
using Entity;
using Entity.Model.Workflow;
using Entity.Model.Process;
using Entity.Model.Alert;
using Repository.Repositories;

namespace data.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
       private readonly ProjectDbContext _context;
       private EFRepository<FndFlexValueSet> flexSetRepo;
       private EFRepository<FndFlexValue> flexValueRepo;
       private EFRepository<Session> sessionRepo;
       private EFRepository<School> schoolRepo;
       private EFRepository<BillPeriod> periodRepo;
       private EFRepository<Class> classRepo;
       private EFRepository<ActionHistory> actionHistoryRepo;
       private EFRepository<ScoolAlert> scoolAlertRepo;
       private EFRepository<AlertHistory> alertHistoryRepo;
       private EFRepository<AttendanceStudent> attendanceStudentRepo;
       private EFRepository<Banner> bannerRepo;
       private EFRepository<BillInfo> billInfoRepo;
       private EFRepository<Book> bookRepo;
       private EFRepository<Calender> calenderRepo;
       private EFRepository<Campus> campusRepo;
       private EFRepository<ClassBook> classBookRepo;
       private EFRepository<ClassCourseMaster> classCourseMasterRepo;
       private EFRepository<ClassCourseSubject> classCourseSubjectRepo;
       private EFRepository<ClassSection> classSectionRepo;
       private EFRepository<ClassTeacher> classTeacherRepo;
       private EFRepository<ClassSectionRoutine> classSectionRoutineRepo;
       private EFRepository<ClassSubjectTeacher> classSubjectTeacherRepo;
       private EFRepository<EventRoutine> eventRoutineRepo;
       private EFRepository<Gallery> galleryRepo;
       private EFRepository<HelpfulResouce> helpfulResouceRepo;
       private EFRepository<Job> jobRepo;
       private EFRepository<JobApply> jobApplyRepo;
       private EFRepository<Leave> leaveRepo;

       private EFRepository<Logo> logoRepo;
       private EFRepository<LogoType> logoTypeRepo;
       private EFRepository<Notice> noticeRepo;
       private EFRepository<PhotoGallery> photoGalleryRepo;
       private EFRepository<Routine> routineRepo;
       private EFRepository<RoutineSetup> routineSetupRepo;
       private EFRepository<MarksheetStatus> marksheetStatusRepo;
       private EFRepository<SchoolBranch> schoolBranchRepo;
       private EFRepository<Section> sectionRepo;
       private EFRepository<StuDocument> stuDocumentRepo;


       private EFRepository<Student> studentRepo;
       private EFRepository<StudentAdmissionTest> studentAdmissionTestRepo;
       private EFRepository<StudentAdmissionMark> studentAdmissionMarkRepo;
       private EFRepository<StudentBillMaster> studentBillMasterRepo;
       private EFRepository<StudentBillLine> studentBillLineRepo;
       private EFRepository<StudentBillMasterTemporary> studentBillMasterTemporariesRepo;
       private EFRepository<StudentBillLineTemporary> studentBillLineTemporariesRepo;
       private EFRepository<StudentMarks> studentMarksRepo;
       private EFRepository<StudentMedical> studentMedicalRepo;
       private EFRepository<StudentProx> studentProxRepo;
       private EFRepository<StudentSiblings> studentSiblingsRepo;

       private EFRepository<SubTeacher> subTeacherRepo;
       private EFRepository<SchoolTask> schoolTaskRepo;
       private EFRepository<VoteMaster> voteMasterRepo;
       private EFRepository<VoteMember> voteMemberRepo;
       private EFRepository<VoteOption> voteOptionRepo;
       private EFRepository<SchoolCalender> schoolCalenderRepo;
       private EFRepository<EmergencyMedicalAction> emergencyMedicalActionRepo;
       private EFRepository<FndMenu> fndMenuRepo;
       private EFRepository<FndMenuGroup> fndMenuGroupsRepo;
       private EFRepository<FndMenuPage> fndMenuPageRepo;
       private EFRepository<FndModule> fndModuleRepo;
       private EFRepository<FndPage> fndPageRepo;

       private EFRepository<FndPagePics> fndPagePicsRepo;
       private EFRepository<FndPeriods> fndPeriodsRepo;
       private EFRepository<FndResponceMenu> fndResponceMenuRepo;
       private EFRepository<FndResponsibility> fndResponsibilitRepo;
       private EFRepository<FndUserPage> fndUserPageRepo;
       private EFRepository<FndUsers> fndUsersRepo;
       private EFRepository<GeneralInfo> feneralInfoRepo;
       private EFRepository<GeneralInfoType> generalInfoTypeRepo;
       private EFRepository<HrDepartment> hrDepartmentRepo;
       private EFRepository<HrDesigGrade> hrDesigGradeRepo;
       private EFRepository<HrEmpExperience> hrEmpExperienceRepo;


       private EFRepository<HrEmployeeHierarcy> hrEmployeeHierarcyRepo;
       private EFRepository<HrEmployeeRefrence> hrEmployeeRefrenceRepo;
       private EFRepository<HrLeaveInfo> hrLeaveInfoRepo;
       private EFRepository<HrPositionStructure> hrPositionStructureRepo;
       private EFRepository<MedicalHistory> medicalHistoryRepo;
       private EFRepository<PlannerDay> plannerDayRepo;
       private EFRepository<PlannerEvent> plannerEventRepo;
       private EFRepository<PlannerEventClass> plannerEventClassRepo;
       private EFRepository<PlannerEventDetails> plannerEventDetailsRepo;
       private EFRepository<PlannerEventType> plannerEventTypeRepo;
       private EFRepository<PlannerHoliday> plannerHolidayRepo;
       private EFRepository<Publisher> publisherRepo;
       private EFRepository<SchoolLinks> schoolLinksRepo;

       private EFRepository<SchoolType> schoolTypeRepo;
       private EFRepository<StudentAdmitCard> studentAdmitCardRepo;
       private EFRepository<ClassSubject> classSubjectRepo;
       private EFRepository<News> newsRepo;
       private EFRepository<SmsHistory> smsHistoryRepo;
       private EFRepository<MenuGroup> menuGroupRepo;
       private EFRepository<Subject> subjectRepo;
       private EFRepository<Career> careerRepo;
       private EFRepository<FndUserResponce> fndUserResponceRepo;
       private EFRepository<HrEmpEduQuali> hrEmpEduQualiRepo;
       private EFRepository<HrEmployeeTiming> hrEmployeeTimingRepo;
       private EFRepository<OnlineInquiry> onlineInquiryRepo;
       private EFRepository<StudentRagistration> studentRagistrationRepo;


       private EFRepository<JobApplicant> jobApplicantRepo;
       private EFRepository<SubjectSubgroupClass> subjectSubgroupClassRepo;
       private EFRepository<SubjectSubgroup> subjectSubgroupRepo;
       private EFRepository<AlternateSubject> alternateSubjectRepo;
       private EFRepository<StudentAdmission> studentAdmissionRepo;
       private EFRepository<ExamGrade> examGradeRepo;
       private EFRepository<ClassExamGrade> classExamGradeRepo;
       private EFRepository<StudentCurriculumResult> studentCurriculumResultRepo;
       private EFRepository<StudentOtherInfo> studentOtherInfoRepo;
       private EFRepository<ProcessLog> processLogRepo;
       private EFRepository<ProcessAssessmentMarks> processAssessmentMarksRepo;


       private EFRepository<ReportProcess> reportProcessRepo;
       private EFRepository<AttendanceGrade> attendanceGradeRepo;
       private EFRepository<StudentSubject> studentSubjectRepo;
       private EFRepository<StudentPosition> studentPositionRepo;
       private EFRepository<StudentPositionLog> studentPositionLogRepo;
       private EFRepository<StudentRemarks> studentRemarksRepo;
       private EFRepository<SubjectSubgroupClassSection> subjectSubgroupClassSectionRepo;
       private EFRepository<StudentHistory> studentHistoryRepo;
       private EFRepository<ClassRoutine> classRoutineRepo;
       private EFRepository<AttendenceInfo> attendenceInfoRepo;
       private EFRepository<HrShift> hrShiftRepo;
       private EFRepository<HrEmployeeShift> hrEmployeeShiftRepo;


       private EFRepository<HrExcepShift> hrExcepShiftRepo;
       private EFRepository<AttendanceEmployee> attendanceEmployeeRepo;
       private EFRepository<AttendancePolicy> attendancePolicyRepo;
       private EFRepository<TempLeave> tempLeaveRepo;
       private EFRepository<LeaveSetup> leaveSetupRepo;
       private EFRepository<EmployeeLeaveInfo> employeeLeaveInfoRepo;
       private EFRepository<LeaveMaster> leaveMasterRepo;
       private EFRepository<WorkFlow> workFlowRepo;
       private EFRepository<ApprovalFlow> approvalFlowRepo;
       private EFRepository<ApproverInfo> approverInfoRepo;
       private EFRepository<WorkFlowRole> workFlowRoleRepo;


       private EFRepository<WorkFlowUserRole> workFlowUserRoleRepo;
       private EFRepository<EmployeeManager> employeeManagerRepo;
       private EFRepository<AlternativeApprovalFlow> alternativeApprovalFlowRepo;
       private EFRepository<HrAttendenceFix> hrAttendenceFixRepo;
       private EFRepository<StudentTransaction> studentTransactionRepo;
       private EFRepository<MailGroup> mailGroupRepo;
       private EFRepository<ProcessQueryTableMailGroup> processQueryTableMailGroupRepo;
       private EFRepository<Schedule> scheduleRepo;
       private EFRepository<ProcessSchedule> processScheduleRepo;
       private EFRepository<RecipientGroup> recipientGroupRepo;
       private EFRepository<MessageAlertMaster> messageAlertMasterRepo;
       private EFRepository<RemarksGrade> remarksGradeRepo;
       private EFRepository<FeeCategory> feeCategoryRepo;


       private EFRepository<FeeSubCategory> feeSubCategoryRepo;
       private EFRepository<SubCategoryFeeType> subCategoryFeeTypeRepo;
       private EFRepository<FeeSubCategoryFine> feeSubCategoryFineRepo;
       private EFRepository<FeeSubCategoryWaiver> feeSubCategoryWaiverRepo;
       private EFRepository<LeaveApplicationFile> leaveApplicationFileRepo;
       private EFRepository<Exam> examRepo;
       private EFRepository<ExamType> examTypeRepo;
       private EFRepository<ClassSubjectWiseExamType> classSubjectWiseExamTypeRepo;
       private EFRepository<StudentCategory> studentCategoriesRepo;
       private EFRepository<SubCategoryFeePeriod> subCategoryFeePeriodsRepo;
       private EFRepository<StudentCategoryLog> studentCategoryLogsRepo;
       private EFRepository<StudentLeaveApplication> studentLeaveApplicationsRepo;


       private EFRepository<StudentLeaveCount> studentLeaveCountsRepo;
       private EFRepository<Device> devicesRepo;
       private EFRepository<TeacherLeaveCount> teacherLeaveCountRepo;
       private EFRepository<ExamPublishInfo> examPublishInfosRepo;
       private EFRepository<BookCategory> bookCategoriesRepo;
       private EFRepository<SMSTemplate> sMSTemplateRepo;
       private EFRepository<BookShelfs> bookShelfsRepo;
       private EFRepository<SmsBuyingLog> smsBuyingLogsRepo;
       private EFRepository<ClassSectionGroupTeacherMapping> classSectionGroupTeachersRepo;
       private EFRepository<Year> yearsRepo;
       private EFRepository<IssuedBook> issuedBooksRepo;
       private EFRepository<SubCategoryYear> subCategoryYearsRepo;
       private EFRepository<BookAuthor> bookAuthorsRepo;
       private EFRepository<ClassSectionRoutineTransaciton> classSectionRoutineTransacitonsRepo;


       private EFRepository<ExamSchedule> examSchedulesRepo;
       private EFRepository<ExamScheduleLine> examScheduleLinesRepo;
       private EFRepository<ExamSeatPlan> examSeatPlansRepo;
       private EFRepository<ExamRoomInvestigator> examRoomInvestigatorsRepo;
       private EFRepository<DateWiseInvestigator> dateWiseInvestigatorsRepo;
       private EFRepository<DateWiseSubject> dateWiseSubjectsRepo;
       private EFRepository<RoomSetup> roomSetupsRepo;
       private EFRepository<ClassRoom> classRoomsRepo;
       private EFRepository<ClassRoomColumn> classRoomColumnsRepo;
       private EFRepository<ClassRoomColumnRow> classRoomColumnRowsRepo;
       private EFRepository<AttendanceChangeRequist> attendanceChangeRequistsRepo;
       private EFRepository<PaymentGatewaySetting> paymentGatewaySettingsRepo;
       private EFRepository<NotifyInfo> notifyInfosRepo;
       private EFRepository<NotifyTransactionInfo> notifyTransactionInfosRepo;
       private EFRepository<ProcessedMarksheet> processedMarksheetsRepo;
       private EFRepository<StudentSubjectMaster> studentSubjectMastersRepo;
       private EFRepository<StudentSubjectLine> studentSubjectLinesRepo;
       private EFRepository<StudentWiseSubject> studentWiseSubjectsRepo;


        #region "HRM & Payroll"
        private EFRepository<Attendance> attendancesRepo;
        private EFRepository<AttendanceProcess> attendanceProcessesRepo;
        private EFRepository<HrPayHead> hrPayHeadsRepo;
        private EFRepository<HrPayRoll> hrPayRollsRepo;
        private EFRepository<HrPayRollPayHead> hrPayRollPayHeadsRepo;
        private EFRepository<HrEmployeePayHead> hrEmployeePayHeadsRepo;
        private EFRepository<HrBillPeriodWisePayRollIssueDate> hrBillPeriodWisePayRollIssueDatesRepo;
        private EFRepository<HrPosition> hrPositionsRepo;
        private EFRepository<HrPaySlipTransactionMaster> hrPaySlipTransactionMastersRepo;
        private EFRepository<HrPaySlipTransactionMasterLog> hrPaySlipTransactionMasterLogsRepo;
        private EFRepository<HrPaySlipTransactionMasterLogHistory> hrPaySlipTransactionMasterLogHistorysRepo;
        private EFRepository<HrPaySlipTransaction> hrPaySlipTransactionsRepo;
        private EFRepository<HrPaySlipTransactionLog> hrPaySlipTransactionLogsRepo;
        private EFRepository<HrPaySlipTransactionLogHistory> hrPaySlipTransactionLogHistorysRepo;
        private EFRepository<PositionChangeRequest> positionChangeRequestsRepo;
        private EFRepository<HrPaySlipPayLog> hrPaySlipPayLogsRepo;
        private EFRepository<HrPaySlipTransactionLogAfterPayheadChange> hrPaySlipTransactionLogAfterPayheadChangesRepo;
        private EFRepository<Compensation> compensationsRepo;
        private EFRepository<EmployeeOfficeTime> employeeOfficeTimesRepo;
        private EFRepository<AttendanceEditHistory> attendanceEditHistorysRepo;
        private EFRepository<EventAndHolidayPlanner> eventAndHolidayPlannersRepo;
        private EFRepository<HrPositionLeaveCategory> hrPositionLeaveCategoriesRepo;
        private EFRepository<HrEmployeeLeaveApplication> hrEmployeeLeaveApplicationRepo;
        private EFRepository<HrEmployeeLeaveCount> hrEmployeeLeaveCountRepo;
        private EFRepository<EmployeeStatusLog> employeeStatusLogRepo;
        private EFRepository<Resignation> resignationRepo;
        private EFRepository<Rejoin> rejoinRepo;
        private EFRepository<OfficialResignRules> officialResignRulesRepo;
        private EFRepository<HrEmployee> employeeRepo;
        private EFRepository<HrEmployeeTransaction> hrEmployeeTransactionRepo;
        private EFRepository<EmployeeWeekends> employeeWeekendsRepo;
        #endregion

        public UnitOfWork(ProjectDbContext context)
        {
            _context = context;
        }

      

        public IRepository<Class> ClassRepo
        {
            get
            {
                if (classRepo == null)
                {
                    classRepo = new EFRepository<Class>(_context);
                }
                return classRepo;
            }
        }
        public IRepository<ActionHistory> ActionHistoryRepo
        {
            get
            {
                if (actionHistoryRepo == null)
                {
                    actionHistoryRepo = new EFRepository<ActionHistory>(_context);
                }
                return actionHistoryRepo;
            }
        }
        public IRepository<ScoolAlert> ScoolAlertRepo
        {
            get
            {
                if (scoolAlertRepo == null)
                {
                    scoolAlertRepo = new EFRepository<ScoolAlert>(_context);
                }
                return scoolAlertRepo;
            }
        }
        public IRepository<AttendanceStudent> AttendanceStudentRepo
        {
            get
            {
                if (attendanceStudentRepo == null)
                {
                    attendanceStudentRepo = new EFRepository<AttendanceStudent>(_context);
                }
                return attendanceStudentRepo;
            }
        }
        public IRepository<AlertHistory> AlertHistoryRepo
        {
            get
            {
                if (alertHistoryRepo == null)
                {
                    alertHistoryRepo = new EFRepository<AlertHistory>(_context);
                }
                return alertHistoryRepo;
            }
        }
        public IRepository<Banner> BannerRepo
        {
            get
            {
                if (bannerRepo == null)
                {
                    bannerRepo = new EFRepository<Banner>(_context);
                }
                return bannerRepo;
            }
        }
        public IRepository<BillInfo> BillInfoRepo
        {
            get
            {
                if (billInfoRepo == null)
                {
                    billInfoRepo = new EFRepository<BillInfo>(_context);
                }
                return billInfoRepo;
            }
        }
        public IRepository<Book> BookRepo
        {
            get
            {
                if (bookRepo == null)
                {
                    bookRepo = new EFRepository<Book>(_context);
                }
                return bookRepo;
            }
        }
        public IRepository<Calender> CalenderRepo
        {
            get
            {
                if (calenderRepo == null)
                {
                    calenderRepo = new EFRepository<Calender>(_context);
                }
                return calenderRepo;
            }
        }
        public IRepository<Campus> CampusRepo
        {
            get
            {
                if (campusRepo == null)
                {
                    campusRepo = new EFRepository<Campus>(_context);
                }
                return campusRepo;
            }
        }
        public IRepository<ClassBook> ClassBookRepo
        {
            get
            {
                if (classBookRepo == null)
                {
                    classBookRepo = new EFRepository<ClassBook>(_context);
                }
                return classBookRepo;
            }
        }
        public IRepository<ClassCourseMaster> ClassCourseMasterRepo
        {
            get
            {
                if (classCourseMasterRepo == null)
                {
                    classCourseMasterRepo = new EFRepository<ClassCourseMaster>(_context);
                }
                return classCourseMasterRepo;
            }
        }
        public IRepository<ClassCourseSubject> ClassCourseSubjectRepo
        {
            get
            {
                if (classCourseSubjectRepo == null)
                {
                    classCourseSubjectRepo = new EFRepository<ClassCourseSubject>(_context);
                }
                return classCourseSubjectRepo;
            }
        }
        public IRepository<ClassSection> ClassSectionRepo
        {
            get
            {
                if (classSectionRepo == null)
                {
                    classSectionRepo = new EFRepository<ClassSection>(_context);
                }
                return classSectionRepo;
            }
        }
        public IRepository<ClassTeacher> ClassTeacherRepo
        {
            get
            {
                if (classTeacherRepo == null)
                {
                    classTeacherRepo = new EFRepository<ClassTeacher>(_context);
                }
                return classTeacherRepo;
            }
        }
        public IRepository<ClassSectionRoutine> ClassSectionRoutineRepo
        {
            get
            {
                if (classSectionRoutineRepo == null)
                {
                    classSectionRoutineRepo = new EFRepository<ClassSectionRoutine>(_context);
                }
                return classSectionRoutineRepo;
            }
        }
        public IRepository<ClassSubjectTeacher> ClassSubjectTeacherRepo
        {
            get
            {
                if (classSubjectTeacherRepo == null)
                {
                    classSubjectTeacherRepo = new EFRepository<ClassSubjectTeacher>(_context);
                }
                return classSubjectTeacherRepo;
            }
        }
        public IRepository<EventRoutine> EventRoutineRepo
        {
            get
            {
                if (eventRoutineRepo == null)
                {
                    eventRoutineRepo = new EFRepository<EventRoutine>(_context);
                }
                return eventRoutineRepo;
            }
        }
        public IRepository<Gallery> GalleryRepo
        {
            get
            {
                if (galleryRepo == null)
                {
                    galleryRepo = new EFRepository<Gallery>(_context);
                }
                return galleryRepo;
            }
        }
        public IRepository<HelpfulResouce> HelpfulResouceRepo
        {
            get
            {
                if (helpfulResouceRepo == null)
                {
                    helpfulResouceRepo = new EFRepository<HelpfulResouce>(_context);
                }
                return helpfulResouceRepo;
            }
        }
        public IRepository<Job> JobRepo
        {
            get
            {
                if (jobRepo == null)
                {
                    jobRepo = new EFRepository<Job>(_context);
                }
                return jobRepo;
            }
        }
        public IRepository<JobApply> JobApplyRepo
        {
            get
            {
                if (jobApplyRepo == null)
                {
                    jobApplyRepo = new EFRepository<JobApply>(_context);
                }
                return jobApplyRepo;
            }
        }
        public IRepository<Leave> LeaveRepo
        {
            get
            {
                if (leaveRepo == null)
                {
                    leaveRepo = new EFRepository<Leave>(_context);
                }
                return leaveRepo;
            }
        }
        public IRepository<Logo> LogoRepo
        {
            get
            {
                if (logoRepo == null)
                {
                    logoRepo = new EFRepository<Logo>(_context);
                }
                return logoRepo;
            }
        }
        public IRepository<LogoType> LogoTypeRepo
        {
            get
            {
                if (logoTypeRepo == null)
                {
                    logoTypeRepo = new EFRepository<LogoType>(_context);
                }
                return logoTypeRepo;
            }
        }
        public IRepository<Notice> NoticeRepo
        {
            get
            {
                if (noticeRepo == null)
                {
                    noticeRepo = new EFRepository<Notice>(_context);
                }
                return noticeRepo;
            }
        }
        public IRepository<PhotoGallery> PhotoGalleryRepo
        {
            get
            {
                if (photoGalleryRepo == null)
                {
                    photoGalleryRepo = new EFRepository<PhotoGallery>(_context);
                }
                return photoGalleryRepo;
            }
        }
        public IRepository<Routine> RoutineRepo
        {
            get
            {
                if (routineRepo == null)
                {
                    routineRepo = new EFRepository<Routine>(_context);
                }
                return routineRepo;
            }
        }
        public IRepository<RoutineSetup> RoutineSetupRepo
        {
            get
            {
                if (routineSetupRepo == null)
                {
                    routineSetupRepo = new EFRepository<RoutineSetup>(_context);
                }
                return routineSetupRepo;
            }
        }
        public IRepository<MarksheetStatus> MarksheetStatusRepo
        {
            get
            {
                if (marksheetStatusRepo == null)
                {
                    marksheetStatusRepo = new EFRepository<MarksheetStatus>(_context);
                }
                return marksheetStatusRepo;
            }
        }
        public IRepository<SchoolBranch> SchoolBranchRepo
        {
            get
            {
                if (schoolBranchRepo == null)
                {
                    schoolBranchRepo = new EFRepository<SchoolBranch>(_context);
                }
                return schoolBranchRepo;
            }
        }
        public IRepository<Section> SectionRepo
        {
            get
            {
                if (sectionRepo == null)
                {
                    sectionRepo = new EFRepository<Section>(_context);
                }
                return sectionRepo;
            }
        }
        public IRepository<StuDocument> StuDocumentRepo
        {
            get
            {
                if (stuDocumentRepo == null)
                {
                    stuDocumentRepo = new EFRepository<StuDocument>(_context);
                }
                return stuDocumentRepo;
            }
        }
        public IRepository<Student> StudentRepo
        {
            get
            {
                if (studentRepo == null)
                {
                    studentRepo = new EFRepository<Student>(_context);
                }
                return studentRepo;
            }
        }
        public IRepository<StudentAdmissionTest> StudentAdmissionTestRepo
        {
            get
            {
                if (studentAdmissionTestRepo == null)
                {
                    studentAdmissionTestRepo = new EFRepository<StudentAdmissionTest>(_context);
                }
                return studentAdmissionTestRepo;
            }
        }
        public IRepository<StudentAdmissionMark> StudentAdmissionMarkRepo
        {
            get
            {
                if (studentAdmissionMarkRepo == null)
                {
                    studentAdmissionMarkRepo = new EFRepository<StudentAdmissionMark>(_context);
                }
                return studentAdmissionMarkRepo;
            }
        }
        public IRepository<StudentBillMaster> StudentBillMasterRepo
        {
            get
            {
                if (studentBillMasterRepo == null)
                {
                    studentBillMasterRepo = new EFRepository<StudentBillMaster>(_context);
                }
                return studentBillMasterRepo;
            }
        }
        public IRepository<StudentBillLine> StudentBillLineRepo
        {
            get
            {
                if (studentBillLineRepo == null)
                {
                    studentBillLineRepo = new EFRepository<StudentBillLine>(_context);
                }
                return studentBillLineRepo;
            }
        }
        public IRepository<StudentBillMasterTemporary> StudentBillMasterTemporariesRepo
        {
            get
            {
                if (studentBillMasterTemporariesRepo == null)
                {
                    studentBillMasterTemporariesRepo = new EFRepository<StudentBillMasterTemporary>(_context);
                }
                return studentBillMasterTemporariesRepo;
            }
        }
        public IRepository<StudentBillLineTemporary> StudentBillLineTemporariesRepo
        {
            get
            {
                if (studentBillLineTemporariesRepo == null)
                {
                    studentBillLineTemporariesRepo = new EFRepository<StudentBillLineTemporary>(_context);
                }
                return studentBillLineTemporariesRepo;
            }
        }
        public IRepository<StudentMarks> StudentMarksRepo
        {
            get
            {
                if (studentMarksRepo == null)
                {
                    studentMarksRepo = new EFRepository<StudentMarks>(_context);
                }
                return studentMarksRepo;
            }
        }
        public IRepository<StudentMedical> StudentMedicalRepo
        {
            get
            {
                if (studentMedicalRepo == null)
                {
                    studentMedicalRepo = new EFRepository<StudentMedical>(_context);
                }
                return studentMedicalRepo;
            }
        }
        public IRepository<StudentProx> StudentProxRepo
        {
            get
            {
                if (studentProxRepo == null)
                {
                    studentProxRepo = new EFRepository<StudentProx>(_context);
                }
                return studentProxRepo;
            }
        }
        public IRepository<StudentSiblings> StudentSiblingsRepo
        {
            get
            {
                if (studentSiblingsRepo == null)
                {
                    studentSiblingsRepo = new EFRepository<StudentSiblings>(_context);
                }
                return studentSiblingsRepo;
            }
        }
        public IRepository<SubTeacher> SubTeacherRepo
        {
            get
            {
                if (subTeacherRepo == null)
                {
                    subTeacherRepo = new EFRepository<SubTeacher>(_context);
                }
                return subTeacherRepo;
            }
        }
        public IRepository<SchoolTask> SchoolTaskRepo
        {
            get
            {
                if (schoolTaskRepo == null)
                {
                    schoolTaskRepo = new EFRepository<SchoolTask>(_context);
                }
                return schoolTaskRepo;
            }
        }
        public IRepository<VoteMaster> VoteMasterRepo
        {
            get
            {
                if (voteMasterRepo == null)
                {
                    voteMasterRepo = new EFRepository<VoteMaster>(_context);
                }
                return voteMasterRepo;
            }
        }
        public IRepository<VoteMember> VoteMemberRepo
        {
            get
            {
                if (voteMemberRepo == null)
                {
                    voteMemberRepo = new EFRepository<VoteMember>(_context);
                }
                return voteMemberRepo;
            }
        }
        public IRepository<VoteOption> VoteOptionRepo
        {
            get
            {
                if (voteOptionRepo == null)
                {
                    voteOptionRepo = new EFRepository<VoteOption>(_context);
                }
                return voteOptionRepo;
            }
        }
        public IRepository<SchoolCalender> SchoolCalenderRepo
        {
            get
            {
                if (schoolCalenderRepo == null)
                {
                    schoolCalenderRepo = new EFRepository<SchoolCalender>(_context);
                }
                return schoolCalenderRepo;
            }
        }
        public IRepository<EmergencyMedicalAction> EmergencyMedicalActionRepo
        {
            get
            {
                if (emergencyMedicalActionRepo == null)
                {
                    emergencyMedicalActionRepo = new EFRepository<EmergencyMedicalAction>(_context);
                }
                return emergencyMedicalActionRepo;
            }
        }
        public IRepository<FndMenu> FndMenuRepo
        {
            get
            {
                if (fndMenuRepo == null)
                {
                    fndMenuRepo = new EFRepository<FndMenu>(_context);
                }
                return fndMenuRepo;
            }
        }
        public IRepository<FndMenuGroup> FndMenuGroupsRepo
        {
            get
            {
                if (fndMenuGroupsRepo == null)
                {
                    fndMenuGroupsRepo = new EFRepository<FndMenuGroup>(_context);
                }
                return fndMenuGroupsRepo;
            }
        }
        public IRepository<FndMenuPage> FndMenuPageRepo
        {
            get
            {
                if (fndMenuPageRepo == null)
                {
                    fndMenuPageRepo = new EFRepository<FndMenuPage>(_context);
                }
                return fndMenuPageRepo; 
            }
        }
        public IRepository<FndModule> FndModuleRepo
        {
            get
            {
                if (fndModuleRepo == null)
                {
                    fndModuleRepo = new EFRepository<FndModule>(_context);
                }
                return fndModuleRepo; 
            }
        }
        public IRepository<FndPage> FndPageRepo
        {
            get
            {
                if (fndPageRepo == null)
                {
                    fndPageRepo = new EFRepository<FndPage>(_context);
                }
                return fndPageRepo;
            }
        }
        public IRepository<FndPagePics> FndPagePicsRepo
        {
            get
            {
                if (fndPagePicsRepo == null)
                {
                    fndPagePicsRepo = new EFRepository<FndPagePics>(_context);
                }
                return fndPagePicsRepo;
            }

        }
        public IRepository<FndPeriods> FndPeriodsRepo
        {
            get
            {
                if (fndPeriodsRepo == null)
                {
                    fndPeriodsRepo = new EFRepository<FndPeriods>(_context);
                }
                return fndPeriodsRepo;
            }
        }
        public IRepository<FndResponceMenu> FndResponceMenuRepo
        {
            get
            {
                if (fndResponceMenuRepo == null)
                {
                    fndResponceMenuRepo = new EFRepository<FndResponceMenu>(_context);
                }
                return fndResponceMenuRepo;
            }
        }
        public IRepository<FndResponsibility> FndResponsibilitRepo
        {
            get
            {
                if (fndResponsibilitRepo == null)
                {
                    fndResponsibilitRepo = new EFRepository<FndResponsibility>(_context);
                }
                return fndResponsibilitRepo;
            }

        }
        public IRepository<FndUserPage> FndUserPageRepo
        {
            get
            {
                if (fndUserPageRepo == null)
                {
                    fndUserPageRepo = new EFRepository<FndUserPage>(_context);
                }
                return fndUserPageRepo;
            }

        }
        public IRepository<FndUsers> FndUsersRepo
        {
            get
            {
                if (fndUsersRepo == null)
                {
                    fndUsersRepo = new EFRepository<FndUsers>(_context);
                }
                return fndUsersRepo;
            }

        }
        public IRepository<GeneralInfo> GeneralInfoRepo
        {
            get
            {
                if (feneralInfoRepo == null)
                {
                    feneralInfoRepo  = new EFRepository<GeneralInfo>(_context);
                }
                return feneralInfoRepo;
            }

        }
        public IRepository<GeneralInfoType> GeneralInfoTypeRepo
        {
            get
            {
                if (generalInfoTypeRepo == null)
                {
                    generalInfoTypeRepo = new EFRepository<GeneralInfoType>(_context);
                }
                return generalInfoTypeRepo; 
            }

        }
        public IRepository<HrDepartment> HrDepartmentRepo
        {
            get
            {
                if (hrDepartmentRepo == null)
                {
                    hrDepartmentRepo = new EFRepository<HrDepartment>(_context);
                }
                return hrDepartmentRepo;
            }

        }
        public IRepository<HrDesigGrade> HrDesigGradeRepo
        {
            get
            {
                if (hrDesigGradeRepo == null)
                {
                    hrDesigGradeRepo = new EFRepository<HrDesigGrade>(_context);
                }
                return hrDesigGradeRepo;
            }

        }
        public IRepository<HrEmpExperience> HrEmpExperienceRepo
        {
            get
            {
                if (hrEmpExperienceRepo == null)
                {
                    hrEmpExperienceRepo = new EFRepository<HrEmpExperience>(_context);
                }
                return hrEmpExperienceRepo;
            }

        }
        public IRepository<FndFlexValueSet> FlexSetRepo
        {
            get
            {
                if (flexSetRepo == null)
                {
                    flexSetRepo = new EFRepository<FndFlexValueSet>(_context);
                }
                return flexSetRepo;
            }
        }
        public IRepository<FndFlexValue> FlexValueRepo
        {
            get
            {
                if (flexValueRepo == null)
                {
                    flexValueRepo = new EFRepository<FndFlexValue>(_context);
                }
                return flexValueRepo;
            }
        }
        public IRepository<BillPeriod> PeriodRepo
        {
            get
            {
                if (periodRepo == null)
                {
                    periodRepo = new EFRepository<BillPeriod>(_context);
                }
                return periodRepo;
            }
        }
        public IRepository<Session> SessionRepo
        {
            get
            {
                if (sessionRepo == null)
                {
                    sessionRepo = new EFRepository<Session>(_context);
                }
                return sessionRepo;
            }
        }
        public IRepository<School> SchoolRepo
        {
            get
            {
                if (schoolRepo == null)
                {
                    schoolRepo = new EFRepository<School>(_context);
                }
                return schoolRepo;
            }
        }
        public IRepository<HrEmployeeHierarcy> HrEmployeeHierarcyRepo
        {
            get
            {
                if (hrEmployeeHierarcyRepo == null)
                {
                    hrEmployeeHierarcyRepo = new EFRepository<HrEmployeeHierarcy>(_context);
                }
                return hrEmployeeHierarcyRepo;
            }
        }
        public IRepository<HrEmployeeRefrence> HrEmployeeRefrenceRepo
        {
            get
            {
                if (hrEmployeeRefrenceRepo == null)
                {
                    hrEmployeeRefrenceRepo = new EFRepository<HrEmployeeRefrence>(_context);
                }
                return hrEmployeeRefrenceRepo;
            }
        }
        public IRepository<HrLeaveInfo> HrLeaveInfoRepo
        {
            get
            {
                if (hrLeaveInfoRepo == null)
                {
                    hrLeaveInfoRepo = new EFRepository<HrLeaveInfo>(_context);
                }
                return hrLeaveInfoRepo;
            }
        }
        public IRepository<HrPositionStructure> HrPositionStructureRepo
        {
            get
            {
                if (hrPositionStructureRepo == null)
                {
                    hrPositionStructureRepo = new EFRepository<HrPositionStructure>(_context);
                }
                return hrPositionStructureRepo;
            }
        }
        public IRepository<MedicalHistory> MedicalHistoryRepo
        {
            get
            {
                if (medicalHistoryRepo == null)
                {
                    medicalHistoryRepo = new EFRepository<MedicalHistory>(_context);
                }
                return medicalHistoryRepo;
            }
        }
        public IRepository<PlannerDay> PlannerDayRepo
        {
            get
            {
                if (plannerDayRepo == null)
                {
                    plannerDayRepo = new EFRepository<PlannerDay>(_context);
                }
                return plannerDayRepo;
            }
        }
        public IRepository<PlannerEvent> PlannerEventRepo
        {
            get
            {
                if (plannerEventRepo == null)
                {
                    plannerEventRepo = new EFRepository<PlannerEvent>(_context);
                }
                return plannerEventRepo;
            }
        }
        public IRepository<PlannerEventClass> PlannerEventClassRepo
        {
            get
            {
                if (plannerEventClassRepo == null)
                {
                    plannerEventClassRepo = new EFRepository<PlannerEventClass>(_context);
                }
                return plannerEventClassRepo;
            }
        }
        public IRepository<PlannerEventDetails> PlannerEventDetailsRepo
        {
            get
            {
                if (plannerEventDetailsRepo == null)
                {
                    plannerEventDetailsRepo = new EFRepository<PlannerEventDetails>(_context);
                }
                return plannerEventDetailsRepo;
            }
        }
        public IRepository<PlannerEventType> PlannerEventTypeRepo
        {
            get
            {
                if (plannerEventTypeRepo == null)
                {
                    plannerEventTypeRepo = new EFRepository<PlannerEventType>(_context);
                }
                return plannerEventTypeRepo;
            }
        }
        public IRepository<PlannerHoliday> PlannerHolidayRepo
        {
            get
            {
                if (plannerHolidayRepo == null)
                {
                    plannerHolidayRepo = new EFRepository<PlannerHoliday>(_context);
                }
                return plannerHolidayRepo;
            }
        }
        public IRepository<Publisher> PublisherRepo
        {
            get
            {
                if (publisherRepo == null)
                {
                    publisherRepo = new EFRepository<Publisher>(_context);
                }
                return publisherRepo;
            }
        }
        public IRepository<SchoolLinks> SchoolLinksRepo
        {
            get
            {
                if (schoolLinksRepo == null)
                {
                    schoolLinksRepo = new EFRepository<SchoolLinks>(_context);
                }
                return schoolLinksRepo;
            }
        }
        public IRepository<Career> CareerRepo
        {
            get
            {
                if (careerRepo == null)
                {
                    careerRepo = new EFRepository<Career>(_context);
                }
                return careerRepo;
            }
        }
        public IRepository<FndUserResponce> FndUserResponceRepo
        {
            get
            {
                if (fndUserResponceRepo == null)
                {
                    fndUserResponceRepo = new EFRepository<FndUserResponce>(_context);
                }
                return fndUserResponceRepo;
            }
        }
        public IRepository<HrEmpEduQuali> HrEmpEduQualiRepo
        {
            get
            {
                if (hrEmpEduQualiRepo == null)
                {
                    hrEmpEduQualiRepo = new EFRepository<HrEmpEduQuali>(_context);
                }
                return hrEmpEduQualiRepo;
            }
        }
        public IRepository<HrEmployeeTiming> HrEmployeeTimingRepo
        {
            get
            {
                if (hrEmployeeTimingRepo == null)
                {
                    hrEmployeeTimingRepo = new EFRepository<HrEmployeeTiming>(_context);
                }
                return hrEmployeeTimingRepo;
            }
        }
        public IRepository<OnlineInquiry> OnlineInquiryRepo
        {
            get
            {
                if (onlineInquiryRepo == null)
                {
                    onlineInquiryRepo = new EFRepository<OnlineInquiry>(_context);
                }
                return onlineInquiryRepo;
            }
        }
        public IRepository<StudentRagistration> StudentRagistrationRepo
        {
            get
            {
                if (studentRagistrationRepo == null)
                {
                    studentRagistrationRepo = new EFRepository<StudentRagistration>(_context);
                }
                return studentRagistrationRepo;
            }
        }
        public IRepository<SchoolType> SchoolTypeRepo
        {
            get
            {
                if (schoolTypeRepo == null)
                {
                    schoolTypeRepo = new EFRepository<SchoolType>(_context);
                }
                return schoolTypeRepo;
            }
        }
        public IRepository<StudentAdmitCard> StudentAdmitCardRepo
        {
            get
            {
                if (studentAdmitCardRepo == null)
                {
                    studentAdmitCardRepo = new EFRepository<StudentAdmitCard>(_context);
                }
                return studentAdmitCardRepo;
            }
        }
        public IRepository<ClassSubject> ClassSubjectRepo
        {
            get
            {
                if (classSubjectRepo == null)
                {
                    classSubjectRepo = new EFRepository<ClassSubject>(_context);
                }
                return classSubjectRepo;
            }
        }
        public IRepository<News> NewsRepo
        {
            get
            {
                if (newsRepo == null)
                {
                    newsRepo = new EFRepository<News>(_context);
                }
                return newsRepo;
            }
        }
        public IRepository<SmsHistory> SmsHistoryRepo
        {
            get
            {
                if (smsHistoryRepo == null)
                {
                    smsHistoryRepo = new EFRepository<SmsHistory>(_context);
                }
                return smsHistoryRepo;
            }
        }
        public IRepository<MenuGroup> MenuGroupRepo
        {
            get
            {
                if (menuGroupRepo == null)
                {
                    menuGroupRepo = new EFRepository<MenuGroup>(_context);
                }
                return menuGroupRepo;
            }
        }
        public IRepository<Subject> SubjectRepo
        {
            get
            {
                if (subjectRepo == null)
                {
                    subjectRepo = new EFRepository<Subject>(_context);
                }
                return subjectRepo;
            }
        }
        public IRepository<JobApplicant> JobApplicantRepo
        {
            get
            {
                if (jobApplicantRepo == null)
                {
                    jobApplicantRepo = new EFRepository<JobApplicant>(_context);
                }
                return jobApplicantRepo;
            }
        }

        public IRepository<SubjectSubgroupClass> SubjectSubgroupClassRepo
        {
            get
            {
                if (subjectSubgroupClassRepo == null)
                {
                    subjectSubgroupClassRepo = new EFRepository<SubjectSubgroupClass>(_context);
                }
                return subjectSubgroupClassRepo;
            }
        }


        public IRepository<SubjectSubgroup> SubjectSubgroupRepo
        {
            get
            {
                if (subjectSubgroupRepo == null)
                {
                    subjectSubgroupRepo = new EFRepository<SubjectSubgroup>(_context);
                }
                return subjectSubgroupRepo;
            }
        }


        public IRepository<AlternateSubject> AlternateSubjectRepo
        {
            get
            {
                if (alternateSubjectRepo == null)
                {
                    alternateSubjectRepo = new EFRepository<AlternateSubject>(_context);
                }
                return alternateSubjectRepo;
            }
        }


        public IRepository<StudentAdmission> StudentAdmissionRepo
        {
            get
            {
                if (studentAdmissionRepo == null)
                {
                    studentAdmissionRepo = new EFRepository<StudentAdmission>(_context);
                }
                return studentAdmissionRepo;
            }
        }


        public IRepository<ExamGrade> ExamGradeRepo
        {
            get
            {
                if (examGradeRepo == null)
                {
                    examGradeRepo = new EFRepository<ExamGrade>(_context);
                }
                return examGradeRepo;
            }
        }


        public IRepository<ClassExamGrade> ClassExamGradeRepo
        {
            get
            {
                if (classExamGradeRepo == null)
                {
                    classExamGradeRepo = new EFRepository<ClassExamGrade>(_context);
                }
                return classExamGradeRepo;
            }
        }
        public IRepository<StudentCurriculumResult> StudentCurriculumResultRepo
        {
            get
            {
                if (studentCurriculumResultRepo == null)
                {
                    studentCurriculumResultRepo = new EFRepository<StudentCurriculumResult>(_context);
                }
                return studentCurriculumResultRepo;
            }
        }

        public IRepository<StudentOtherInfo> StudentOtherInfoRepo
        {
            get
            {
                if (studentOtherInfoRepo == null)
                {
                    studentOtherInfoRepo = new EFRepository<StudentOtherInfo>(_context);
                }
                return studentOtherInfoRepo;
            }
        }

        public IRepository<ProcessLog> ProcessLogRepo
        {
            get
            {
                if (processLogRepo == null)
                {
                    processLogRepo = new EFRepository<ProcessLog>(_context);
                }
                return processLogRepo;
            }
        }

        public IRepository<ProcessAssessmentMarks> ProcessAssessmentMarksRepo
        {
            get
            {
                if (processAssessmentMarksRepo == null)
                {
                    processAssessmentMarksRepo = new EFRepository<ProcessAssessmentMarks>(_context);
                }
                return processAssessmentMarksRepo;
            }
        }
        public IRepository<ReportProcess> ReportProcessRepo
        {
            get
            {
                if (reportProcessRepo == null)
                {
                    reportProcessRepo = new EFRepository<ReportProcess>(_context);
                }
                return reportProcessRepo;
            }
        }

        public IRepository<AttendanceGrade> AttendanceGradeRepo
        {
            get
            {
                if (attendanceGradeRepo == null)
                {
                    attendanceGradeRepo = new EFRepository<AttendanceGrade>(_context);
                }
                return attendanceGradeRepo;
            }
        }

        public IRepository<StudentSubject> StudentSubjectRepo
        {
            get
            {
                if (studentSubjectRepo == null)
                {
                    studentSubjectRepo = new EFRepository<StudentSubject>(_context);
                }
                return studentSubjectRepo;
            }
        }

        public IRepository<StudentPosition> StudentPositionRepo
        {
            get
            {
                if (studentPositionRepo == null)
                {
                    studentPositionRepo = new EFRepository<StudentPosition>(_context);
                }
                return studentPositionRepo;
            }
        }

        public IRepository<StudentPositionLog> StudentPositionLogRepo
        {
            get
            {
                if (studentPositionLogRepo == null)
                {
                    studentPositionLogRepo = new EFRepository<StudentPositionLog>(_context);
                }
                return studentPositionLogRepo;
            }
        }

        public IRepository<StudentRemarks> StudentRemarksRepo
        {
            get
            {
                if (studentRemarksRepo == null)
                {
                    studentRemarksRepo = new EFRepository<StudentRemarks>(_context);
                }
                return studentRemarksRepo;
            }
        }

        public IRepository<SubjectSubgroupClassSection> SubjectSubgroupClassSectionRepo
        {
            get
            {
                if (subjectSubgroupClassSectionRepo == null)
                {
                    subjectSubgroupClassSectionRepo = new EFRepository<SubjectSubgroupClassSection>(_context);
                }
                return subjectSubgroupClassSectionRepo;
            }
        }
        public IRepository<StudentHistory> StudentHistoryRepo
        {
            get
            {
                if (studentHistoryRepo == null)
                {
                    studentHistoryRepo = new EFRepository<StudentHistory>(_context);
                }
                return studentHistoryRepo;
            }
        }

        public IRepository<ClassRoutine> ClassRoutineRepo
        {
            get
            {
                if (classRoutineRepo == null)
                {
                    classRoutineRepo = new EFRepository<ClassRoutine>(_context);
                }
                return classRoutineRepo;
            }
        }

        public IRepository<AttendenceInfo> AttendenceInfoRepo
        {
            get
            {
                if (attendenceInfoRepo == null)
                {
                    attendenceInfoRepo = new EFRepository<AttendenceInfo>(_context);
                }
                return attendenceInfoRepo;
            }
        }

        public IRepository<HrShift> HrShiftRepo
        {
            get
            {
                if (hrShiftRepo == null)
                {
                    hrShiftRepo = new EFRepository<HrShift>(_context);
                }
                return hrShiftRepo;
            }
        }

        public IRepository<HrEmployeeShift> HrEmployeeShiftRepo
        {
            get
            {
                if (hrEmployeeShiftRepo == null)
                {
                    hrEmployeeShiftRepo = new EFRepository<HrEmployeeShift>(_context);
                }
                return hrEmployeeShiftRepo;
            }
        }
        public IRepository<HrExcepShift> HrExcepShiftRepo
        {
            get
            {
                if (hrExcepShiftRepo == null)
                {
                    hrExcepShiftRepo = new EFRepository<HrExcepShift>(_context);
                }
                return hrExcepShiftRepo;
            }
        }

        public IRepository<AttendanceEmployee> AttendanceEmployeeRepo
        {
            get
            {
                if (attendanceEmployeeRepo == null)
                {
                    attendanceEmployeeRepo = new EFRepository<AttendanceEmployee>(_context);
                }
                return attendanceEmployeeRepo;
            }
        }

        public IRepository<AttendancePolicy> AttendancePolicyRepo
        {
            get
            {
                if (attendancePolicyRepo == null)
                {
                    attendancePolicyRepo = new EFRepository<AttendancePolicy>(_context);
                }
                return attendancePolicyRepo;
            }
        }

        public IRepository<TempLeave> TempLeaveRepo
        {
            get
            {
                if (tempLeaveRepo == null)
                {
                    tempLeaveRepo = new EFRepository<TempLeave>(_context);
                }
                return tempLeaveRepo;
            }
        }
        public IRepository<LeaveSetup> LeaveSetupRepo
        {
            get
            {
                if (leaveSetupRepo == null)
                {
                    leaveSetupRepo = new EFRepository<LeaveSetup>(_context);
                }
                return leaveSetupRepo;
            }
        }

        public IRepository<EmployeeLeaveInfo> EmployeeLeaveInfoRepo
        {
            get
            {
                if (employeeLeaveInfoRepo == null)
                {
                    employeeLeaveInfoRepo = new EFRepository<EmployeeLeaveInfo>(_context);
                }
                return employeeLeaveInfoRepo;
            }
        }

        public IRepository<LeaveMaster> LeaveMasterRepo
        {
            get
            {
                if (leaveMasterRepo == null)
                {
                    leaveMasterRepo = new EFRepository<LeaveMaster>(_context);
                }
                return leaveMasterRepo;
            }
        }
        public IRepository<WorkFlow> WorkFlowRepo
        {
            get
            {
                if (workFlowRepo == null)
                {
                    workFlowRepo = new EFRepository<WorkFlow>(_context);
                }
                return workFlowRepo;
            }
        }

        public IRepository<ApprovalFlow> ApprovalFlowRepo
        {
            get
            {
                if (approvalFlowRepo == null)
                {
                    approvalFlowRepo = new EFRepository<ApprovalFlow>(_context);
                }
                return approvalFlowRepo;
            }
        }

        public IRepository<ApproverInfo> ApproverInfoRepo
        {
            get
            {
                if (approverInfoRepo == null)
                {
                    approverInfoRepo = new EFRepository<ApproverInfo>(_context);
                }
                return approverInfoRepo;
            }
        }

        public IRepository<WorkFlowRole> WorkFlowRoleRepo
        {
            get
            {
                if (workFlowRoleRepo == null)
                {
                    workFlowRoleRepo = new EFRepository<WorkFlowRole>(_context);
                }
                return workFlowRoleRepo;
            }
        }

        public IRepository<WorkFlowUserRole> WorkFlowUserRoleRepo
        {
            get
            {
                if (workFlowUserRoleRepo == null)
                {
                    workFlowUserRoleRepo = new EFRepository<WorkFlowUserRole>(_context);
                }
                return workFlowUserRoleRepo;
            }
        }

        public IRepository<EmployeeManager> EmployeeManagerRepo
        {
            get
            {
                if (employeeManagerRepo == null)
                {
                    employeeManagerRepo = new EFRepository<EmployeeManager>(_context);
                }
                return employeeManagerRepo;
            }
        }
        public IRepository<AlternativeApprovalFlow> AlternativeApprovalFlowRepo
        {
            get
            {
                if (alternativeApprovalFlowRepo == null)
                {
                    alternativeApprovalFlowRepo = new EFRepository<AlternativeApprovalFlow>(_context);
                }
                return alternativeApprovalFlowRepo;
            }
        }

        public IRepository<HrAttendenceFix> HrAttendenceFixRepo
        {
            get
            {
                if (hrAttendenceFixRepo == null)
                {
                    hrAttendenceFixRepo = new EFRepository<HrAttendenceFix>(_context);
                }
                return hrAttendenceFixRepo;
            }
        }

        public IRepository<StudentTransaction> StudentTransactionRepo
        {
            get
            {
                if (studentTransactionRepo == null)
                {
                    studentTransactionRepo = new EFRepository<StudentTransaction>(_context);
                }
                return studentTransactionRepo;
            }
        }

        public IRepository<MailGroup> MailGroupRepo
        {
            get
            {
                if (mailGroupRepo == null)
                {
                    mailGroupRepo = new EFRepository<MailGroup>(_context);
                }
                return mailGroupRepo;
            }
        }

        public IRepository<ProcessQueryTableMailGroup> ProcessQueryTableMailGroupRepo
        {
            get
            {
                if (processQueryTableMailGroupRepo == null)
                {
                    processQueryTableMailGroupRepo = new EFRepository<ProcessQueryTableMailGroup>(_context);
                }
                return processQueryTableMailGroupRepo;
            }
        }

        public IRepository<Schedule> ScheduleRepo
        {
            get
            {
                if (scheduleRepo == null)
                {
                    scheduleRepo = new EFRepository<Schedule>(_context);
                }
                return scheduleRepo;
            }
        }

        public IRepository<ProcessSchedule> ProcessScheduleRepo
        {
            get
            {
                if (processScheduleRepo == null)
                {
                    processScheduleRepo = new EFRepository<ProcessSchedule>(_context);
                }
                return processScheduleRepo;
            }
        }

        public IRepository<RecipientGroup> RecipientGroupRepo
        {
            get
            {
                if (recipientGroupRepo == null)
                {
                    recipientGroupRepo = new EFRepository<RecipientGroup>(_context);
                }
                return recipientGroupRepo;
            }
        }
        public IRepository<MessageAlertMaster> MessageAlertMasterRepo
        {
            get
            {
                if (messageAlertMasterRepo == null)
                {
                    messageAlertMasterRepo = new EFRepository<MessageAlertMaster>(_context);
                }
                return messageAlertMasterRepo;
            }
        }

        public IRepository<RemarksGrade> RemarksGradeRepo
        {
            get
            {
                if (remarksGradeRepo == null)
                {
                    remarksGradeRepo = new EFRepository<RemarksGrade>(_context);
                }
                return remarksGradeRepo;
            }
        }

        public IRepository<FeeCategory> FeeCategoryRepo
        {
            get
            {
                if (feeCategoryRepo == null)
                {
                    feeCategoryRepo = new EFRepository<FeeCategory>(_context);
                }
                return feeCategoryRepo;
            }
        }

        public IRepository<FeeSubCategory> FeeSubCategoryRepo
        {
            get
            {
                if (feeSubCategoryRepo == null)
                {
                    feeSubCategoryRepo = new EFRepository<FeeSubCategory>(_context);
                }
                return feeSubCategoryRepo;
            }
        }

        public IRepository<SubCategoryFeeType> SubCategoryFeeTypeRepo
        {
            get
            {
                if (subCategoryFeeTypeRepo == null)
                {
                    subCategoryFeeTypeRepo = new EFRepository<SubCategoryFeeType>(_context);
                }
                return subCategoryFeeTypeRepo;
            }
        }
        public IRepository<FeeSubCategoryFine> FeeSubCategoryFineRepo
        {
            get
            {
                if (feeSubCategoryFineRepo == null)
                {
                    feeSubCategoryFineRepo = new EFRepository<FeeSubCategoryFine>(_context);
                }
                return feeSubCategoryFineRepo;
            }
        }

        public IRepository<FeeSubCategoryWaiver> FeeSubCategoryWaiverRepo
        {
            get
            {
                if (feeSubCategoryWaiverRepo == null)
                {
                    feeSubCategoryWaiverRepo = new EFRepository<FeeSubCategoryWaiver>(_context);
                }
                return feeSubCategoryWaiverRepo;
            }
        }

        public IRepository<LeaveApplicationFile> LeaveApplicationFileRepo
        {
            get
            {
                if (leaveApplicationFileRepo == null)
                {
                    leaveApplicationFileRepo = new EFRepository<LeaveApplicationFile>(_context);
                }
                return leaveApplicationFileRepo;
            }
        }

        public IRepository<Exam> ExamRepo
        {
            get
            {
                if (examRepo == null)
                {
                    examRepo = new EFRepository<Exam>(_context);
                }
                return examRepo;
            }
        }

        public IRepository<ExamType> ExamTypeRepo
        {
            get
            {
                if (examTypeRepo == null)
                {
                    examTypeRepo = new EFRepository<ExamType>(_context);
                }
                return examTypeRepo;
            }
        }

        public IRepository<ClassSubjectWiseExamType> ClassSubjectWiseExamTypeRepo
        {
            get
            {
                if (classSubjectWiseExamTypeRepo == null)
                {
                    classSubjectWiseExamTypeRepo = new EFRepository<ClassSubjectWiseExamType>(_context);
                }
                return classSubjectWiseExamTypeRepo;
            }
        }

        public IRepository<StudentCategory> StudentCategoriesRepo
        {
            get
            {
                if (studentCategoriesRepo == null)
                {
                    studentCategoriesRepo = new EFRepository<StudentCategory>(_context);
                }
                return studentCategoriesRepo;
            }
        }

        public IRepository<SubCategoryFeePeriod> SubCategoryFeePeriodsRepo
        {
            get
            {
                if (subCategoryFeePeriodsRepo == null)
                {
                    subCategoryFeePeriodsRepo = new EFRepository<SubCategoryFeePeriod>(_context);
                }
                return subCategoryFeePeriodsRepo;
            }
        }
        public IRepository<StudentCategoryLog> StudentCategoryLogsRepo
        {
            get
            {
                if (studentCategoryLogsRepo == null)
                {
                    studentCategoryLogsRepo = new EFRepository<StudentCategoryLog>(_context);
                }
                return studentCategoryLogsRepo;
            }
        }

        public IRepository<StudentLeaveApplication> StudentLeaveApplicationsRepo
        {
            get
            {
                if (studentLeaveApplicationsRepo == null)
                {
                    studentLeaveApplicationsRepo = new EFRepository<StudentLeaveApplication>(_context);
                }
                return studentLeaveApplicationsRepo;
            }
        }

        public IRepository<StudentLeaveCount> StudentLeaveCountsRepo
        {
            get
            {
                if (studentLeaveCountsRepo == null)
                {
                    studentLeaveCountsRepo = new EFRepository<StudentLeaveCount>(_context);
                }
                return studentLeaveCountsRepo;
            }
        }

        public IRepository<Device> DevicesRepo
        {
            get
            {
                if (devicesRepo == null)
                {
                    devicesRepo = new EFRepository<Device>(_context);
                }
                return devicesRepo;
            }
        }

        public IRepository<TeacherLeaveCount> TeacherLeaveCountRepo
        {
            get
            {
                if (teacherLeaveCountRepo == null)
                {
                    teacherLeaveCountRepo = new EFRepository<TeacherLeaveCount>(_context);
                }
                return teacherLeaveCountRepo;
            }
        }
        public IRepository<ExamPublishInfo> ExamPublishInfosRepo
        {
            get
            {
                if (examPublishInfosRepo == null)
                {
                    examPublishInfosRepo = new EFRepository<ExamPublishInfo>(_context);
                }
                return examPublishInfosRepo;
            }
        }

        public IRepository<BookCategory> BookCategoriesRepo
        {
            get
            {
                if (bookCategoriesRepo == null)
                {
                    bookCategoriesRepo = new EFRepository<BookCategory>(_context);
                }
                return bookCategoriesRepo;
            }
        }

        public IRepository<SMSTemplate> SMSTemplateRepo
        {
            get
            {
                if (sMSTemplateRepo == null)
                {
                    sMSTemplateRepo = new EFRepository<SMSTemplate>(_context);
                }
                return sMSTemplateRepo;
            }
        }

        public IRepository<BookShelfs> BookShelfsRepo
        {
            get
            {
                if (bookShelfsRepo == null)
                {
                    bookShelfsRepo = new EFRepository<BookShelfs>(_context);
                }
                return bookShelfsRepo;
            }
        }

        public IRepository<SmsBuyingLog> SmsBuyingLogsRepo
        {
            get
            {
                if (smsBuyingLogsRepo == null)
                {
                    smsBuyingLogsRepo = new EFRepository<SmsBuyingLog>(_context);
                }
                return smsBuyingLogsRepo;
            }
        }

        public IRepository<ClassSectionGroupTeacherMapping> ClassSectionGroupTeachersRepo
        {
            get
            {
                if (classSectionGroupTeachersRepo == null)
                {
                    classSectionGroupTeachersRepo = new EFRepository<ClassSectionGroupTeacherMapping>(_context);
                }
                return classSectionGroupTeachersRepo;
            }
        }
        public IRepository<Year> YearsRepo
        {
            get
            {
                if (yearsRepo == null)
                {
                    yearsRepo = new EFRepository<Year>(_context);
                }
                return yearsRepo;
            }
        }

        public IRepository<IssuedBook> IssuedBooksRepo
        {
            get
            {
                if (issuedBooksRepo == null)
                {
                    issuedBooksRepo = new EFRepository<IssuedBook>(_context);
                }
                return issuedBooksRepo;
            }
        }

        public IRepository<SubCategoryYear> SubCategoryYearsRepo
        {
            get
            {
                if (subCategoryYearsRepo == null)
                {
                    subCategoryYearsRepo = new EFRepository<SubCategoryYear>(_context);
                }
                return subCategoryYearsRepo;
            }
        }

        public IRepository<BookAuthor> BookAuthorsRepo
        {
            get
            {
                if (bookAuthorsRepo == null)
                {
                    bookAuthorsRepo = new EFRepository<BookAuthor>(_context);
                }
                return bookAuthorsRepo;
            }
        }

        public IRepository<ClassSectionRoutineTransaciton> ClassSectionRoutineTransacitonsRepo
        {
            get
            {
                if (classSectionRoutineTransacitonsRepo == null)
                {
                    classSectionRoutineTransacitonsRepo = new EFRepository<ClassSectionRoutineTransaciton>(_context);
                }
                return classSectionRoutineTransacitonsRepo;
            }
        }
        public IRepository<ExamSchedule> ExamSchedulesRepo
        {
            get
            {
                if (examSchedulesRepo == null)
                {
                    examSchedulesRepo = new EFRepository<ExamSchedule>(_context);
                }
                return examSchedulesRepo;
            }
        }

        public IRepository<ExamScheduleLine> ExamScheduleLinesRepo
        {
            get
            {
                if (examScheduleLinesRepo == null)
                {
                    examScheduleLinesRepo = new EFRepository<ExamScheduleLine>(_context);
                }
                return examScheduleLinesRepo;
            }
        }

        public IRepository<ExamSeatPlan> ExamSeatPlansRepo
        {
            get
            {
                if (examSeatPlansRepo == null)
                {
                    examSeatPlansRepo = new EFRepository<ExamSeatPlan>(_context);
                }
                return examSeatPlansRepo;
            }
        }

        public IRepository<ExamRoomInvestigator> ExamRoomInvestigatorsRepo
        {
            get
            {
                if (examRoomInvestigatorsRepo == null)
                {
                    examRoomInvestigatorsRepo = new EFRepository<ExamRoomInvestigator>(_context);
                }
                return examRoomInvestigatorsRepo;
            }
        }

        public IRepository<DateWiseInvestigator> DateWiseInvestigatorsRepo
        {
            get
            {
                if (dateWiseInvestigatorsRepo == null)
                {
                    dateWiseInvestigatorsRepo = new EFRepository<DateWiseInvestigator>(_context);
                }
                return dateWiseInvestigatorsRepo;
            }
        }

        public IRepository<DateWiseSubject> DateWiseSubjectsRepo
        {
            get
            {
                if (dateWiseSubjectsRepo == null)
                {
                    dateWiseSubjectsRepo = new EFRepository<DateWiseSubject>(_context);
                }
                return dateWiseSubjectsRepo;
            }
        }
        public IRepository<RoomSetup> RoomSetupsRepo
        {
            get
            {
                if (roomSetupsRepo == null)
                {
                    roomSetupsRepo = new EFRepository<RoomSetup>(_context);
                }
                return roomSetupsRepo;
            }
        }

        public IRepository<ClassRoom> ClassRoomsRepo
        {
            get
            {
                if (classRoomsRepo == null)
                {
                    classRoomsRepo = new EFRepository<ClassRoom>(_context);
                }
                return classRoomsRepo;
            }
        }

        public IRepository<ClassRoomColumn> ClassRoomColumnsRepo
        {
            get
            {
                if (classRoomColumnsRepo == null)
                {
                    classRoomColumnsRepo = new EFRepository<ClassRoomColumn>(_context);
                }
                return classRoomColumnsRepo;
            }
        }

        public IRepository<ClassRoomColumnRow> ClassRoomColumnRowsRepo
        {
            get
            {
                if (classRoomColumnRowsRepo == null)
                {
                    classRoomColumnRowsRepo = new EFRepository<ClassRoomColumnRow>(_context);
                }
                return classRoomColumnRowsRepo;
            }
        }

        public IRepository<AttendanceChangeRequist> AttendanceChangeRequistsRepo
        {
            get
            {
                if (attendanceChangeRequistsRepo == null)
                {
                    attendanceChangeRequistsRepo = new EFRepository<AttendanceChangeRequist>(_context);
                }
                return attendanceChangeRequistsRepo;
            }
        }

        public IRepository<PaymentGatewaySetting> PaymentGatewaySettingsRepo
        {
            get
            {
                if (paymentGatewaySettingsRepo == null)
                {
                    paymentGatewaySettingsRepo = new EFRepository<PaymentGatewaySetting>(_context);
                }
                return paymentGatewaySettingsRepo;
            }
        }

        public IRepository<NotifyInfo> NotifyInfosRepo
        {
            get
            {
                if (notifyInfosRepo == null)
                {
                    notifyInfosRepo = new EFRepository<NotifyInfo>(_context);
                }
                return notifyInfosRepo;
            }
        }
        public IRepository<NotifyTransactionInfo> NotifyTransactionInfosRepo
        {
            get
            {
                if (notifyTransactionInfosRepo == null)
                {
                    notifyTransactionInfosRepo = new EFRepository<NotifyTransactionInfo>(_context);
                }
                return notifyTransactionInfosRepo;
            }
        }


        public IRepository<ProcessedMarksheet> ProcessedMarksheetsRepo
        {
            get
            {
                if (processedMarksheetsRepo == null)
                {
                    processedMarksheetsRepo = new EFRepository<ProcessedMarksheet>(_context);
                }
                return processedMarksheetsRepo;
            }
        }
        public IRepository<StudentSubjectMaster> StudentSubjectMastersRepo
        {
            get
            {
                if (studentSubjectMastersRepo == null)
                {
                    studentSubjectMastersRepo = new EFRepository<StudentSubjectMaster>(_context);
                }
                return studentSubjectMastersRepo;
            }
        }
        public IRepository<StudentSubjectLine> StudentSubjectLinesRepo
        {
            get
            {
                if (studentSubjectLinesRepo == null)
                {
                    studentSubjectLinesRepo = new EFRepository<StudentSubjectLine>(_context);
                }
                return studentSubjectLinesRepo;
            }
        }
        public IRepository<StudentWiseSubject> StudentWiseSubjectsRepo
        {
            get
            {
                if (studentWiseSubjectsRepo == null)
                {
                    studentWiseSubjectsRepo = new EFRepository<StudentWiseSubject>(_context);
                }
                return studentWiseSubjectsRepo;
            }
        }









































        #region HRM & Payroll
        public IRepository<Attendance> AttendancesRepo
        {
            get
            {
                if (attendancesRepo == null)
                {
                    attendancesRepo = new EFRepository<Attendance>(_context);
                }
                return attendancesRepo;
            }
        }
        public IRepository<AttendanceProcess> AttendanceProcessesRepo
        {
            get
            {
                if (attendanceProcessesRepo == null)
                {
                    attendanceProcessesRepo = new EFRepository<AttendanceProcess>(_context);
                }
                return attendanceProcessesRepo;
            }
        }
        public IRepository<HrPayHead> HrPayHeadsRepo
        {
            get
            {
                if (hrPayHeadsRepo == null)
                {
                    hrPayHeadsRepo = new EFRepository<HrPayHead>(_context);
                }
                return hrPayHeadsRepo;
            }
        }
        public IRepository<HrPayRoll> HrPayRollsRepo
        {
            get
            {
                if (hrPayRollsRepo == null)
                {
                    hrPayRollsRepo = new EFRepository<HrPayRoll>(_context);
                }
                return hrPayRollsRepo;
            }
        }
        public IRepository<HrPayRollPayHead> HrPayRollPayHeadsRepo
        {
            get
            {
                if (hrPayRollPayHeadsRepo == null)
                {
                    hrPayRollPayHeadsRepo = new EFRepository<HrPayRollPayHead>(_context);
                }
                return hrPayRollPayHeadsRepo;
            }
        }
        public IRepository<HrEmployeePayHead> HrEmployeePayHeadsRepo
        {
            get
            {
                if (hrEmployeePayHeadsRepo == null)
                {
                    hrEmployeePayHeadsRepo = new EFRepository<HrEmployeePayHead>(_context);
                }
                return hrEmployeePayHeadsRepo;
            }
        }
        public IRepository<HrBillPeriodWisePayRollIssueDate> HrBillPeriodWisePayRollIssueDatesRepo
        {
            get
            {
                if (hrBillPeriodWisePayRollIssueDatesRepo == null)
                {
                    hrBillPeriodWisePayRollIssueDatesRepo = new EFRepository<HrBillPeriodWisePayRollIssueDate>(_context);
                }
                return hrBillPeriodWisePayRollIssueDatesRepo;
            }
        }
        public IRepository<HrPosition> HrPositionsRepo
        {
            get
            {
                if (hrPositionsRepo == null)
                {
                    hrPositionsRepo = new EFRepository<HrPosition>(_context);
                }
                return hrPositionsRepo;
            }
        }
        public IRepository<HrPaySlipTransactionMaster> HrPaySlipTransactionMastersRepo
        {
            get
            {
                if (hrPaySlipTransactionMastersRepo == null)
                {
                    hrPaySlipTransactionMastersRepo = new EFRepository<HrPaySlipTransactionMaster>(_context);
                }
                return hrPaySlipTransactionMastersRepo;
            }
        }
        public IRepository<HrPaySlipTransactionMasterLog> HrPaySlipTransactionMasterLogsRepo
        {
            get
            {
                if (hrPaySlipTransactionMasterLogsRepo == null)
                {
                    hrPaySlipTransactionMasterLogsRepo = new EFRepository<HrPaySlipTransactionMasterLog>(_context);
                }
                return hrPaySlipTransactionMasterLogsRepo;
            }
        }
        public IRepository<HrPaySlipTransactionMasterLogHistory> HrPaySlipTransactionMasterLogHistorysRepo
        {
            get
            {
                if (hrPaySlipTransactionMasterLogHistorysRepo == null)
                {
                    hrPaySlipTransactionMasterLogHistorysRepo = new EFRepository<HrPaySlipTransactionMasterLogHistory>(_context);
                }
                return hrPaySlipTransactionMasterLogHistorysRepo;
            }
        }
        public IRepository<HrPaySlipTransaction> HrPaySlipTransactionsRepo
        {
            get
            {
                if (hrPaySlipTransactionsRepo == null)
                {
                    hrPaySlipTransactionsRepo = new EFRepository<HrPaySlipTransaction>(_context);
                }
                return hrPaySlipTransactionsRepo;
            }
        }
        public IRepository<HrPaySlipTransactionLog> HrPaySlipTransactionLogsRepo
        {
            get
            {
                if (hrPaySlipTransactionLogsRepo == null)
                {
                    hrPaySlipTransactionLogsRepo = new EFRepository<HrPaySlipTransactionLog>(_context);
                }
                return hrPaySlipTransactionLogsRepo;
            }
        }
        public IRepository<HrPaySlipTransactionLogHistory> HrPaySlipTransactionLogHistorysRepo
        {
            get
            {
                if (hrPaySlipTransactionLogHistorysRepo == null)
                {
                    hrPaySlipTransactionLogHistorysRepo = new EFRepository<HrPaySlipTransactionLogHistory>(_context);
                }
                return hrPaySlipTransactionLogHistorysRepo;
            }
        }
        public IRepository<PositionChangeRequest> PositionChangeRequestsRepo
        {
            get
            {
                if (positionChangeRequestsRepo == null)
                {
                    positionChangeRequestsRepo = new EFRepository<PositionChangeRequest>(_context);
                }
                return positionChangeRequestsRepo;
            }
        }
        public IRepository<HrPaySlipPayLog> HrPaySlipPayLogsRepo
        {
            get
            {
                if (hrPaySlipPayLogsRepo == null)
                {
                    hrPaySlipPayLogsRepo = new EFRepository<HrPaySlipPayLog>(_context);
                }
                return hrPaySlipPayLogsRepo;
            }
        }
        public IRepository<HrPaySlipTransactionLogAfterPayheadChange> HrPaySlipTransactionLogAfterPayheadChangesRepo
        {
            get
            {
                if (hrPaySlipTransactionLogAfterPayheadChangesRepo == null)
                {
                    hrPaySlipTransactionLogAfterPayheadChangesRepo = new EFRepository<HrPaySlipTransactionLogAfterPayheadChange>(_context);
                }
                return hrPaySlipTransactionLogAfterPayheadChangesRepo;
            }
        }
        public IRepository<OfficialResignRules> OfficialResignRulesRepo
        {
            get
            {
                if (officialResignRulesRepo == null)
                {
                    officialResignRulesRepo = new EFRepository<OfficialResignRules>(_context);
                }
                return officialResignRulesRepo;
            }
        }
        public IRepository<Rejoin> RejoinRepo
        {
            get
            {
                if (rejoinRepo == null)
                {
                    rejoinRepo = new EFRepository<Rejoin>(_context);
                }
                return rejoinRepo;
            }
        }
        public IRepository<Resignation> ResignationRepo
        {
            get
            {
                if (resignationRepo == null)
                {
                    resignationRepo = new EFRepository<Resignation>(_context);
                }
                return resignationRepo;
            }
        }
        public IRepository<HrEmployeeLeaveCount> HrEmployeeLeaveCountRepo
        {
            get
            {
                if (hrEmployeeLeaveCountRepo == null)
                {
                    hrEmployeeLeaveCountRepo = new EFRepository<HrEmployeeLeaveCount>(_context);
                }
                return hrEmployeeLeaveCountRepo;
            }
        }
        public IRepository<EmployeeStatusLog> EmployeeStatusLogRepo
        {
            get
            {
                if (employeeStatusLogRepo == null)
                {
                    employeeStatusLogRepo = new EFRepository<EmployeeStatusLog>(_context);
                }
                return employeeStatusLogRepo;
            }
        }
        public IRepository<HrEmployeeLeaveApplication> HrEmployeeLeaveApplicationRepo
        {
            get
            {
                if (hrEmployeeLeaveApplicationRepo == null)
                {
                    hrEmployeeLeaveApplicationRepo = new EFRepository<HrEmployeeLeaveApplication>(_context);
                }
                return hrEmployeeLeaveApplicationRepo;
            }
        }
        public IRepository<HrPositionLeaveCategory> HrPositionLeaveCategoriesRepo
        {
            get
            {
                if (hrPositionLeaveCategoriesRepo == null)
                {
                    hrPositionLeaveCategoriesRepo = new EFRepository<HrPositionLeaveCategory>(_context);
                }
                return hrPositionLeaveCategoriesRepo;
            }
        }
        public IRepository<Compensation> CompensationsRepo
        {
            get
            {
                if (compensationsRepo == null)
                {
                    compensationsRepo = new EFRepository<Compensation>(_context);
                }
                return compensationsRepo;
            }
        }
        public IRepository<EmployeeOfficeTime> EmployeeOfficeTimesRepo
        {
            get
            {
                if (employeeOfficeTimesRepo == null)
                {
                    employeeOfficeTimesRepo = new EFRepository<EmployeeOfficeTime>(_context);
                }
                return employeeOfficeTimesRepo;
            }
        }
        public IRepository<AttendanceEditHistory> AttendanceEditHistorysRepo
        {
            get
            {
                if (attendanceEditHistorysRepo == null)
                {
                    attendanceEditHistorysRepo = new EFRepository<AttendanceEditHistory>(_context);
                }
                return attendanceEditHistorysRepo;
            }
        }
        public IRepository<EventAndHolidayPlanner> EventAndHolidayPlannersRepo
        {
            get
            {
                if (eventAndHolidayPlannersRepo == null)
                {
                    eventAndHolidayPlannersRepo = new EFRepository<EventAndHolidayPlanner>(_context);
                }
                return eventAndHolidayPlannersRepo;
            }
        }
        public IRepository<HrEmployee> EmployeeRepo
        {
            get
            {
                if (employeeRepo == null)
                {
                    employeeRepo = new EFRepository<HrEmployee>(_context);
                }
                return employeeRepo;
            }
        }
        public IRepository<HrEmployeeTransaction> HrEmployeeTransactionRepo
        {
            get
            {
                if (hrEmployeeTransactionRepo == null)
                {
                    hrEmployeeTransactionRepo = new EFRepository<HrEmployeeTransaction>(_context);
                }
                return hrEmployeeTransactionRepo;
            }
        }
        public IRepository<EmployeeWeekends> EmployeeWeekendsRepo
        {
            get
            {
                if (employeeWeekendsRepo == null)
                {
                    employeeWeekendsRepo = new EFRepository<EmployeeWeekends>(_context);
                }
                return employeeWeekendsRepo;
            }
        }
        #endregion
        public void Dispose()
        {
            _context.Dispose();
        }

        public int SaveChanges()
        {
            return _context.SaveChanges();
        }

        public async Task<int> SaveChangesAsync()
        {
            return await _context.SaveChangesAsync();
        }
    }
}
