﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Utility
{
    
    public enum SLLPaymentStatus
    {
        success = 1,
        failed = 2,
        cancel = 3
    }
    public enum PayslipGenerationType
    {
        New = 1,
        Canceled = 2
    }
    public enum Month
    {
        January = 1,
        February = 2,
        March = 3,
        April = 4,
        May = 5,
        June = 6,
        July = 7,
        August = 8,
        Septembe = 9,
        October = 10,
        November = 11,
        December = 12,
    }
    public enum Years
    {
        Year___2019 = 2019,
        Year___2020 = 2020,
        Year___2021 = 2021,
        Year___2022 = 2022,
        Year___2023 = 2023,
        Year___2024 = 2024,
        Year___2025 = 2025,
        Year___2026 = 2026,
        Year___2027 = 2027,
        Year___2028 = 2028,
        Year___2029 = 2029,
        Year___2030 = 2030
    }

    public enum Weakdays
    {
        Saturday = 6,
        Sunday = 0,
        Monday = 1,
        Tuesday = 2,
        Wednesday = 3,
        Thursday = 4,
        Friday = 5
    }
    public enum NotifyType
    {
        Librarian = 1
    }
    public enum LogintoVirtualClassAs
    {
        Teacher = 1,
        Student = 2
    }
    public enum ArrangeSeatingPlan
    {
        OneByOne = 1,
        Vertically = 2,
       /* Horizontally = 3*/
      
    }
    public enum DownloadRoutineFor
    {
        ClassSectionWise = 1,
        RoomWise = 2,
        TeacherRoutine = 3
    }
    public enum EmpStatus
    {
        Active = 1,
        Left = 2,
        All = 3
    }
}
