using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entity.Model
{
    public partial class FndPeriods : BaseEntity
    {
        [Key]
        public int PeriodId { get; set; }

        [Required]
        [StringLength(256)]
        public string PeriodName { get; set; }

        [StringLength(256)]
        public string Description { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public DateTime? BillingLastDate { get; set; }

        [Required]
        [StringLength(50)]
        public string Status { get; set; }

        [StringLength(100)]
        public string PeriodYear { get; set; }

        public int? PeriodTypeId { get; set; }

        public int? SessionId { get; set; }

    }
}
