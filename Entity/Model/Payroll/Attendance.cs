﻿using Entity.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    // <summary>
    /// This Entity is required for Attendance Data processing. If Need to Change Pls Check with corresponding person.
    /// </summary>
    public partial class Attendance : BaseEntity
    {
        [Key]
        public int AttendanceId { get; set; }

        public DateTime PunchDate { get; set; }

        [Required]
        [StringLength(50)]
        public string PunchTime { get; set; }
        public int? SessionId { get; set; }
        public int? SubjectId { get; set; }
        public int? DoorNumber { get; set; }
        public int? GateNumber { get; set; }
        //[Required]
        //[StringLength(50)]
        public string CardNumber { get; set; }
        public int? EmpHeaderId { get; set; }
        public int? PermissionTypeId { get; set; }
        public bool? AttendanceStatus { get; set; }
        public int? DevicePort { get; set; }
        public string DeviceIP { get; set; }
        public string CreatedIp { get; set; }
    }
}
