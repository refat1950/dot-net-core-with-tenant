﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class StudentLeaveCount : BaseEntity
    {
        [Key]
        public int LeaveCountId { get; set; }
        public int ApplicationId { get; set; }
        public string LeaveQuantity { get; set; }
        public int StuTotalLeave { get; set; }
        public int? StuLeaveRemain { get; set; }
    }
}
