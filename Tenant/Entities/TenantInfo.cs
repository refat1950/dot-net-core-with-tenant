﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tenant.Entities
{
    public class TenantInfo
    {
        [Key]
        public int TenantId { get; set; }
        public string Url { get; set; }
        public string ConnectionStringName { get; set; }
        public string AlternateUrl1 { get; set; }
        public string AlternateUrl2 { get; set; }
        public string ConnectionString { get; set; }
        public string ServerUrl { get; set; }
        public string DbName { get; set; }
        public string DbPassword { get; set; }
        public bool? IsActive { get; set; }
        public string SchoolName { get; set; }
        public string MobileNo { get; set; }
        public decimal? MonthlyCharge { get; set; }
        public DateTime? CreationDate { get; set; }
        public DateTime? LastUpdateDate { get; set; }
        public DateTime? EffectiveFrom { get; set; }
        public DateTime? EffectiveTo { get; set; }
        public string Attribute1 { get; set; }
        public string Attribute2 { get; set; }
        public string Attribute3 { get; set; }
        public string Attribute4 { get; set; }
        public string Attribute5 { get; set; }
        public int? MinDeviceId { get; set; }
        public int? MaxDeviceId { get; set; }
    }
}
