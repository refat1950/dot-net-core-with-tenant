﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using data.UnitOfWork;
using Entity.Model;
using Entity.Model.Transaction;
using Repository.Context;
using Service.Helper;
using Service.Service.Interfaces.Payroll;
using Service.ViewModel;
using Service.ViewModel.Payroll;
using Web.Models;

namespace Service.DataService
{
    public class EmployeeService : IEmployeeService
    {
        private IUnitOfWork _unitOfWork;
        IMigratedPayrollDataProvider migratedPayrollDataProvider;
        private ProjectDbContext _db;
        public EmployeeService(IUnitOfWork unitOfWork, IMigratedPayrollDataProvider migratedPayrollDataProvider, ProjectDbContext _db)
        {
            _unitOfWork = unitOfWork;
            this.migratedPayrollDataProvider = migratedPayrollDataProvider;
            this._db = _db;
        }
        //public IEnumerable<VmEmpStatusLog> GetEmployeesCurrentStatus(List<int> empHeaderIds)
        //{
        //    var empStatusList = new List<VmEmpStatusLog>();
        //    try
        //    {
        //        if (empHeaderIds != null && empHeaderIds.Any())
        //        {
        //            foreach (var empid in empHeaderIds)
        //            {
        //                var empStatus = new VmEmpStatusLog();
        //                empStatus.EmpHeaderId = empid;
        //                empStatus.EmployeeJoiningStatus = GetEmployeeCurrentStatus(empid);
        //                empStatusList.Add(empStatus);
        //            }
        //        }
        //        return empStatusList;
        //    }
        //    catch (Exception ex)
        //    {
        //        return empStatusList;
        //    }
        //}
        public EmployeeJoiningStatus GetEmployeeCurrentStatus(int EmpHeaderId)
        {
            try
            {
                var exLog = _unitOfWork.EmployeeRepo.FirstOrDefault(w => w.EmpHeaderId == EmpHeaderId).EmployeeStatusLogs;
                return (exLog != null && exLog.Any()) ? exLog.OrderByDescending(o => o.EmployeeStatusLogId).FirstOrDefault() != null ? exLog.OrderByDescending(o => o.EmployeeStatusLogId).FirstOrDefault().Status : EmployeeJoiningStatus.Not_Set : EmployeeJoiningStatus.Not_Set;
            }
            catch (Exception ex)
            {
                return EmployeeJoiningStatus.Not_Set;
            }
        }

        public IEnumerable<HrEmployee> GetActiveEmployees()
        {
            return _unitOfWork.EmployeeRepo.GetAll().Where(w=> w.EmployeeStatusLogs.OrderByDescending(o => o.EmployeeStatusLogId).Any() && w.EmployeeStatusLogs.OrderByDescending(o => o.EmployeeStatusLogId).FirstOrDefault().Status != EmployeeJoiningStatus.Left);
        }
        public IEnumerable<HrEmployee> GetLeftEmployees()
        {
            return _unitOfWork.EmployeeRepo.GetAll().Where(w => w.EmployeeStatusLogs.OrderByDescending(o => o.EmployeeStatusLogId).Any() && w.EmployeeStatusLogs.OrderByDescending(o => o.EmployeeStatusLogId).FirstOrDefault().Status == EmployeeJoiningStatus.Left);
        }

        public int Add(VmEmployee vmEmployee, int userId)
        {
            HrEmployee hrEmployee = new HrEmployee();
            hrEmployee.ImgUrl = vmEmployee.ImgUrl;
            hrEmployee.EmpHeaderId = 0;
            hrEmployee.EmpFirstName = vmEmployee.EmpFirstName;
            hrEmployee.EmpMiddleName = vmEmployee.EmpMiddleName;
            hrEmployee.EmpLastName = vmEmployee.EmpLastName;
            hrEmployee.EmpNickName = vmEmployee.EmpNickName;
            hrEmployee.EmpId = vmEmployee.EmpId;
            hrEmployee.GenderId = vmEmployee.GenderId;
            var birthdate = DateTimeHelperService.ConvertBDDateStringToDateTimeObject(vmEmployee.BirthDate);
            hrEmployee.BirthDate = Convert.ToDateTime(birthdate);
            hrEmployee.Email = vmEmployee.Email;
            hrEmployee.Email2 = vmEmployee.Email2;
            hrEmployee.Mobile = vmEmployee.Mobile;
            hrEmployee.Phone = vmEmployee.Phone;
            hrEmployee.PunchCardId = vmEmployee.PunchCardId;

            hrEmployee.PositionId = vmEmployee.PositionId;
            hrEmployee.JobLocationId = vmEmployee.JobLocationId;
            hrEmployee.BaseOfficeId = vmEmployee.BaseOfficeId;
            hrEmployee.UserTypeId = vmEmployee.UserTypeId;
            hrEmployee.WorkPhone = vmEmployee.WorkPhone;
            hrEmployee.DepartmentId = vmEmployee.DepartmentId;
            hrEmployee.DesignationId = vmEmployee.DesignationId;
            hrEmployee.DeviceId = vmEmployee.DeviceId;
            if (!string.IsNullOrEmpty(vmEmployee.JoinningDate))
            {
                var joinningdate = DateTimeHelperService.ConvertBDDateStringToDateTimeObject(vmEmployee.JoinningDate);
                hrEmployee.JoinningDate = Convert.ToDateTime(joinningdate);
            }

            hrEmployee.PresHoldingNo = vmEmployee.PresHoldingNo;
            hrEmployee.PresStreet = vmEmployee.PresStreet;
            hrEmployee.PresArea = vmEmployee.PresArea;
            hrEmployee.PresPostCode = vmEmployee.PresPostCode;
            hrEmployee.PresCountryId = vmEmployee.PresCountryId;
            hrEmployee.PresDivisionId = vmEmployee.PresDivisionId;
            hrEmployee.PresCityId = vmEmployee.PresCityId;
            hrEmployee.PresThanaId = vmEmployee.PresThanaId;

            hrEmployee.PermHoldingNo = vmEmployee.PermHoldingNo;
            hrEmployee.PermStreet = vmEmployee.PermStreet;
            hrEmployee.PermArea = vmEmployee.PermArea;
            hrEmployee.PermPostCode = vmEmployee.PermPostCode;
            hrEmployee.PermCountryId = vmEmployee.PermCountryId;
            hrEmployee.PermDivisionId = vmEmployee.PermDivisionId;
            hrEmployee.PermCityId = vmEmployee.PermCityId;
            hrEmployee.PermThanaId = vmEmployee.PermThanaId;

            hrEmployee.FatherFirstName = vmEmployee.FatherFirstName;
            hrEmployee.FatherMiddleName = vmEmployee.FatherMiddleName;
            hrEmployee.FatherLastName = vmEmployee.FatherLastName;
            hrEmployee.FatherContactNo = vmEmployee.FatherContactNo;
            hrEmployee.FatherNationalId = vmEmployee.FatherNationalId;

            hrEmployee.MotherFirstName = vmEmployee.MotherFirstName;
            hrEmployee.MotherMiddleName = vmEmployee.MotherMiddleName;
            hrEmployee.MotherLastName = vmEmployee.MotherLastName;
            hrEmployee.MotherContactNo = vmEmployee.MotherContactNo;
            hrEmployee.MotherNationalId = vmEmployee.MotherNationalId;

            hrEmployee.SpouseFirstName = vmEmployee.SpouseFirstName;
            hrEmployee.SpouseMiddleName = vmEmployee.SpouseMiddleName;
            hrEmployee.SpouseLastName = vmEmployee.SpouseLastName;
            hrEmployee.SpouseContactNo = vmEmployee.SpouseContactNo;
            hrEmployee.SpouseNationalId = vmEmployee.SpouseNationalId;

            hrEmployee.BloodGroup = vmEmployee.BloodGroup;
            hrEmployee.Religion = vmEmployee.Religion;
            hrEmployee.Nationality = vmEmployee.Nationality;
            hrEmployee.MaritalStatus = vmEmployee.MaritalStatus;
            hrEmployee.NationalId = vmEmployee.NationalId;
            hrEmployee.PassportNo = vmEmployee.PassportNo;
            hrEmployee.BirthCountry = vmEmployee.BirthCountry;
            hrEmployee.PlaceOfBirth = vmEmployee.PlaceOfBirth;
            if (!string.IsNullOrEmpty(vmEmployee.LastMedicalCheckDate))
            {
                var lastmedicalcheckout = DateTimeHelperService.ConvertBDDateStringToDateTimeObject(vmEmployee.LastMedicalCheckDate);
                hrEmployee.LastMedicalCheckDate = Convert.ToDateTime(lastmedicalcheckout);
            }
            hrEmployee.LastMedicalStatus = vmEmployee.LastMedicalStatus;
            hrEmployee.MedicalHistory = vmEmployee.MedicalHistory;
            hrEmployee.DrivingLicenseNo = vmEmployee.DrivingLicenseNo;
            hrEmployee.StartTime = vmEmployee.Attribute10; //in time
            hrEmployee.EndTime = vmEmployee.Attribute11; // out time
            hrEmployee.UserName = vmEmployee.EmpId.Substring(3);

            hrEmployee.CreatedBy = userId;
            hrEmployee.LastUpdateBy = userId;
            hrEmployee.CreationDate = DateTime.Now;
            _unitOfWork.EmployeeRepo.Add(hrEmployee);
            var isSaved = _unitOfWork.SaveChanges();

            if (isSaved > 0)
            {
                #region Position Data Add or payhead assign
                var getPositionData = _unitOfWork.HrPositionsRepo.FirstOrDefault(f => f.PostionId == hrEmployee.PositionId);

                if (getPositionData != null)
                {
                    var getPayroll = getPositionData.PayrollId;
                    var getpayheadOfpayroll = _unitOfWork.HrPayRollPayHeadsRepo.Find(w => w.PayRollId == getPayroll).ToList();

                    if (getpayheadOfpayroll != null)
                    {
                        if (getpayheadOfpayroll.Count > 0)
                        {
                            foreach (var payhead in getpayheadOfpayroll)
                            {
                                HrEmployeePayHead hrEmployeePayHead = new HrEmployeePayHead();
                                hrEmployeePayHead.EmpHeaderId = hrEmployee.EmpHeaderId;
                                hrEmployeePayHead.PayHeadId = payhead.PayHeadId;
                                hrEmployeePayHead.Unit = payhead.Unit;
                                hrEmployeePayHead.IsPercentage = payhead.IsPercentage;
                                hrEmployeePayHead.CreatedBy = userId;
                                hrEmployeePayHead.CreationDate = DateTime.Now;
                                _unitOfWork.HrEmployeePayHeadsRepo.Add(hrEmployeePayHead);
                            }
                        }
                    }
                }
                _unitOfWork.SaveChanges();
                #endregion

                #region OfficeTimeEntry
                var timing = new EmployeeOfficeTime
                {
                    EmpHeaderId = hrEmployee.EmpHeaderId,
                    EmployeeOfficeTimeHeaderId = 0,
                    StartTime = vmEmployee.Attribute10,
                    EndTime = vmEmployee.Attribute11,
                    EffectiveFrom = BdDateTime.Today(),
                    CreatedBy = userId,
                    CreationDate = BdDateTime.Now(),
                    LastUpdateBy = userId,
                    LastUpdateDate = BdDateTime.Now()
                };
                _db.EmployeeOfficeTimes.Add(timing);
                _db.SaveChanges();
                #endregion

                if (vmEmployee.vmEmpEduQualifications != null)
                {
                    if (vmEmployee.vmEmpEduQualifications.Count > 0)
                    {
                        foreach (var qualification in vmEmployee.vmEmpEduQualifications)
                        {
                            HrEmpEduQuali hrEmpEduQuali = new HrEmpEduQuali();
                            hrEmpEduQuali.EmpHeaderId = hrEmployee.EmpHeaderId;
                            hrEmpEduQuali.QualificationId = QualificationId();
                            hrEmpEduQuali.QualificationType = qualification.QualificationType;
                            hrEmpEduQuali.Major = qualification.Major;
                            hrEmpEduQuali.InstitutionId = qualification.InstitutionId;
                            hrEmpEduQuali.BoardId = qualification.BoardId;
                            hrEmpEduQuali.PassingYear = qualification.PassingYear;
                            hrEmpEduQuali.Result = qualification.Result;
                            if (!string.IsNullOrEmpty(qualification.StartDate))
                            {
                                qualification.StartDate = DateTimeHelperService.ConvertBDDateStringToDateTimeObject(qualification.StartDate);
                                hrEmpEduQuali.StartDate = Convert.ToDateTime(qualification.StartDate);
                            }
                            if (!string.IsNullOrEmpty(qualification.EndDate))
                            {
                                qualification.EndDate = DateTimeHelperService.ConvertBDDateStringToDateTimeObject(qualification.EndDate);
                                hrEmpEduQuali.EndDate = Convert.ToDateTime(qualification.EndDate);
                            }
                            hrEmpEduQuali.CreatedBy = userId;
                            hrEmpEduQuali.CreationDate = DateTime.Now;
                            _db.HrEmpEduQuali.Add(hrEmpEduQuali);
                            _db.SaveChanges();
                        }
                    }
                }

                if (vmEmployee.vmEmpExperiences != null)
                {
                    if (vmEmployee.vmEmpExperiences.Count > 0)
                    {
                        foreach (var experience in vmEmployee.vmEmpExperiences)
                        {
                            HrEmpExperience hrEmpExperience = new HrEmpExperience();
                            hrEmpExperience.EmpHeaderId = hrEmployee.EmpHeaderId;
                            hrEmpExperience.ExperienceId = ExperienceId();
                            hrEmpExperience.CompanyName = experience.CompanyName;
                            hrEmpExperience.Designation = experience.Designation;
                            hrEmpExperience.Location = experience.Location;
                            hrEmpExperience.Responsibility = experience.Responsibility;
                            if (!string.IsNullOrEmpty(experience.StartDate))
                            {
                                experience.StartDate = DateTimeHelperService.ConvertBDDateStringToDateTimeObject(experience.StartDate);
                                hrEmpExperience.StartDate = Convert.ToDateTime(experience.StartDate).ToString();
                            }
                            if (!string.IsNullOrEmpty(experience.EndDate))
                            {
                                experience.EndDate = DateTimeHelperService.ConvertBDDateStringToDateTimeObject(experience.EndDate);
                                hrEmpExperience.EndDate = Convert.ToDateTime(experience.EndDate).ToString();
                            }
                            hrEmpExperience.CreatedBy = userId;
                            hrEmpExperience.CreationDate = DateTime.Now;
                            _db.HrEmpExperience.Add(hrEmpExperience);
                            _db.SaveChanges();
                        }
                    }
                }

                if (vmEmployee.vmEmployeeRefrences != null)
                {
                    if (vmEmployee.vmEmployeeRefrences.Count > 0)
                    {
                        foreach (var reference in vmEmployee.vmEmployeeRefrences)
                        {
                            HrEmployeeRefrence hrEmployeeRefrence = new HrEmployeeRefrence();
                            hrEmployeeRefrence.EmpHeaderId = hrEmployee.EmpHeaderId;
                            hrEmployeeRefrence.ReferenceId = ReferenceId();
                            hrEmployeeRefrence.ReferenceName = reference.ReferenceName;
                            hrEmployeeRefrence.Designation = reference.Designation;
                            hrEmployeeRefrence.Address1 = reference.Address1;
                            hrEmployeeRefrence.Address2 = reference.Address2;
                            hrEmployeeRefrence.Address3 = reference.Address3;
                            hrEmployeeRefrence.ResPhone = reference.ResPhone;
                            hrEmployeeRefrence.OffPhone = reference.OffPhone;
                            hrEmployeeRefrence.Mobile1 = reference.Mobile1;
                            hrEmployeeRefrence.Mobile2 = reference.Mobile2;
                            hrEmployeeRefrence.Email = reference.Email;
                            hrEmployeeRefrence.Relation = reference.Relation;
                            hrEmployeeRefrence.CreatedBy = userId;
                            hrEmployeeRefrence.CreationDate = DateTime.Now;
                            _db.HrEmployeeRefrence.Add(hrEmployeeRefrence);
                            _db.SaveChanges();
                        }
                    }
                }

                #region Add Employee Status Log
                var empStatus = GetEmployeeCurrentStatus(hrEmployee.EmpHeaderId);
                //add status log, check for duplicate entry
                if (empStatus != EmployeeJoiningStatus.Joined)
                {
                    var d = new EmployeeStatusLog
                    {
                        EmpHeaderId = hrEmployee.EmpHeaderId,
                        Status = EmployeeJoiningStatus.Joined,
                        CreatedBy = userId,
                        CreationDate = DateTime.Now,
                    };
                    _unitOfWork.EmployeeStatusLogRepo.Add(d);
                }
                _unitOfWork.SaveChanges();
                #endregion
            }

            return isSaved > 0 ? hrEmployee.EmpHeaderId : 0;
        }

        public HrEmployee Get(int id)
        {
            return _unitOfWork.EmployeeRepo.Get(id);
        }

        public int SaveChanges()
        {
            return _unitOfWork.SaveChanges();
        }

        public int Edit(VmEmployee employeeEdit, int userId)
        {
            try
            {
                var obj = Get(employeeEdit.EmpHeaderId);
                //position change date calculation
                //migratedPayrollDataProvider.StorPositionChangeRequestLog(employee, userId);
                //end

                obj.EmpFirstName = employeeEdit.EmpFirstName;
                obj.EmpMiddleName = employeeEdit.EmpMiddleName;
                obj.EmpLastName = employeeEdit.EmpLastName;
                obj.EmpNickName = employeeEdit.EmpNickName;
                obj.EmpId = employeeEdit.EmpId;
                obj.GenderId = employeeEdit.GenderId;
                employeeEdit.BirthDate = DateTimeHelperService.ConvertBDDateStringToDateTimeObject(employeeEdit.BirthDate);
                obj.BirthDate = Convert.ToDateTime(employeeEdit.BirthDate);
                obj.StartTime = employeeEdit.Attribute10; //in time
                obj.EndTime = employeeEdit.Attribute11;// out time
                obj.Email = employeeEdit.Email;
                obj.Email2 = employeeEdit.Email2;
                obj.Mobile = employeeEdit.Mobile;
                obj.Phone = employeeEdit.Phone;
                obj.PunchCardId = employeeEdit.PunchCardId;
                obj.DeviceId = employeeEdit.DeviceId;

                obj.JobLocationId = employeeEdit.JobLocationId;
                obj.BaseOfficeId = employeeEdit.BaseOfficeId;

                obj.UserTypeId = employeeEdit.UserTypeId;
                obj.WorkPhone = employeeEdit.WorkPhone;
                obj.DepartmentId = employeeEdit.DepartmentId;
                obj.DesignationId = employeeEdit.DesignationId;
                if (!string.IsNullOrEmpty(employeeEdit.JoinningDate))
                {
                    employeeEdit.JoinningDate = DateTimeHelperService.ConvertBDDateStringToDateTimeObject(employeeEdit.JoinningDate);
                    obj.JoinningDate = Convert.ToDateTime(employeeEdit.JoinningDate);
                }
                if (!string.IsNullOrEmpty(employeeEdit.JobEndDate))
                {
                    employeeEdit.JobEndDate = DateTimeHelperService.ConvertBDDateStringToDateTimeObject(employeeEdit.JobEndDate);
                    obj.JobEndDate = Convert.ToDateTime(employeeEdit.JobEndDate);
                }

                obj.PresHoldingNo = employeeEdit.PresHoldingNo;
                obj.PresStreet = employeeEdit.PresStreet;
                obj.PresArea = employeeEdit.PresArea;
                obj.PresPostCode = employeeEdit.PresPostCode;
                obj.PresCountryId = employeeEdit.PresCountryId;
                obj.PresDivisionId = employeeEdit.PresDivisionId;
                obj.PresCityId = employeeEdit.PresCityId;
                obj.PresThanaId = employeeEdit.PresThanaId;

                obj.PermHoldingNo = employeeEdit.PermHoldingNo;
                obj.PermStreet = employeeEdit.PermStreet;
                obj.PermArea = employeeEdit.PermArea;
                obj.PermPostCode = employeeEdit.PermPostCode;
                obj.PermCountryId = employeeEdit.PermCountryId;
                obj.PermDivisionId = employeeEdit.PermDivisionId;
                obj.PermCityId = employeeEdit.PermCityId;
                obj.PermThanaId = employeeEdit.PermThanaId;

                obj.FatherFirstName = employeeEdit.FatherFirstName;
                obj.FatherMiddleName = employeeEdit.FatherMiddleName;
                obj.FatherLastName = employeeEdit.FatherLastName;
                obj.FatherContactNo = employeeEdit.FatherContactNo;
                obj.FatherNationalId = employeeEdit.FatherNationalId;

                obj.MotherFirstName = employeeEdit.MotherFirstName;
                obj.MotherMiddleName = employeeEdit.MotherMiddleName;
                obj.MotherLastName = employeeEdit.MotherLastName;
                obj.MotherContactNo = employeeEdit.MotherContactNo;
                obj.MotherNationalId = employeeEdit.MotherNationalId;

                obj.SpouseFirstName = employeeEdit.SpouseFirstName;
                obj.SpouseMiddleName = employeeEdit.SpouseMiddleName;
                obj.SpouseLastName = employeeEdit.SpouseLastName;
                obj.SpouseContactNo = employeeEdit.SpouseContactNo;
                obj.SpouseNationalId = employeeEdit.SpouseNationalId;

                obj.BloodGroup = employeeEdit.BloodGroup;
                obj.Religion = employeeEdit.Religion;
                obj.Nationality = employeeEdit.Nationality;
                obj.MaritalStatus = employeeEdit.MaritalStatus;
                obj.NationalId = employeeEdit.NationalId;
                obj.PassportNo = employeeEdit.PassportNo;
                obj.BirthCountry = employeeEdit.BirthCountry;
                obj.PlaceOfBirth = employeeEdit.PlaceOfBirth;
                if (!string.IsNullOrEmpty(employeeEdit.LastMedicalCheckDate))
                {
                    employeeEdit.LastMedicalCheckDate = DateTimeHelperService.ConvertBDDateStringToDateTimeObject(employeeEdit.LastMedicalCheckDate);
                    obj.LastMedicalCheckDate = Convert.ToDateTime(employeeEdit.LastMedicalCheckDate);
                }
                obj.LastMedicalStatus = employeeEdit.LastMedicalStatus;
                obj.MedicalHistory = employeeEdit.MedicalHistory;
                obj.DrivingLicenseNo = employeeEdit.DrivingLicenseNo;

                obj.StartTime = employeeEdit.Attribute10; //in time 
                obj.EndTime = employeeEdit.Attribute11; // out time

                obj.CreatedBy = userId;
                obj.LastUpdateBy = userId;
                obj.CreationDate = DateTime.Now;
                obj.LastUpdateDate = DateTime.Now;
                _unitOfWork.SaveChanges();

                if (employeeEdit.vmEmpEduQualifications != null)
                {
                    if (employeeEdit.vmEmpEduQualifications.Count > 0)
                    {
                        foreach (var qualification in employeeEdit.vmEmpEduQualifications)
                        {
                            var qualObj = _db.HrEmpEduQuali.FirstOrDefault(f => f.QualificationId == qualification.QualificationId);
                            if (qualObj != null)
                            {
                                qualObj.QualificationType = qualification.QualificationType;
                                qualObj.Major = qualification.Major;
                                qualObj.InstitutionId = qualification.InstitutionId;
                                qualObj.BoardId = qualification.BoardId;
                                qualObj.PassingYear = qualification.PassingYear;
                                qualObj.Result = qualification.Result;
                                if (!string.IsNullOrEmpty(qualification.StartDate))
                                {
                                    qualification.StartDate = DateTimeHelperService.ConvertBDDateStringToDateTimeObject(qualification.StartDate);
                                    qualObj.StartDate = Convert.ToDateTime(qualification.StartDate);
                                }
                                if (!string.IsNullOrEmpty(qualification.EndDate))
                                {
                                    qualification.EndDate = DateTimeHelperService.ConvertBDDateStringToDateTimeObject(qualification.EndDate);
                                    qualObj.EndDate = Convert.ToDateTime(qualification.EndDate);
                                }
                                qualObj.LastUpdateBy = userId;
                                qualObj.LastUpdateDate = DateTime.Now;
                                _db.SaveChanges();
                            }
                            else
                            {
                                HrEmpEduQuali hrEmpEduQualiAdd = new HrEmpEduQuali();
                                hrEmpEduQualiAdd.EmpHeaderId = obj.EmpHeaderId;
                                hrEmpEduQualiAdd.QualificationId = QualificationId();
                                hrEmpEduQualiAdd.QualificationType = qualification.QualificationType;
                                hrEmpEduQualiAdd.Major = qualification.Major;
                                hrEmpEduQualiAdd.InstitutionId = qualification.InstitutionId;
                                hrEmpEduQualiAdd.BoardId = qualification.BoardId;
                                hrEmpEduQualiAdd.PassingYear = qualification.PassingYear;
                                hrEmpEduQualiAdd.Result = qualification.Result;
                                if (!string.IsNullOrEmpty(qualification.StartDate))
                                {
                                    qualification.StartDate = DateTimeHelperService.ConvertBDDateStringToDateTimeObject(qualification.StartDate);
                                    hrEmpEduQualiAdd.StartDate = Convert.ToDateTime(qualification.StartDate);
                                }
                                if (!string.IsNullOrEmpty(qualification.EndDate))
                                {
                                    qualification.EndDate = DateTimeHelperService.ConvertBDDateStringToDateTimeObject(qualification.EndDate);
                                    hrEmpEduQualiAdd.EndDate = Convert.ToDateTime(qualification.EndDate);
                                }
                                hrEmpEduQualiAdd.CreatedBy = userId;
                                hrEmpEduQualiAdd.CreationDate = DateTime.Now;
                                _db.HrEmpEduQuali.Add(hrEmpEduQualiAdd);
                                _db.SaveChanges();
                            }
                        }
                    }
                }

                if (employeeEdit.vmEmpExperiences != null)
                {
                    if (employeeEdit.vmEmpExperiences.Count > 0)
                    {
                        foreach (var experience in employeeEdit.vmEmpExperiences)
                        {
                            var objExp = _db.HrEmpExperience.FirstOrDefault(f => f.ExperienceId == experience.ExperienceId);
                            if (objExp != null)
                            {
                                objExp.CompanyName = experience.CompanyName;
                                objExp.Designation = experience.Designation;
                                objExp.Location = experience.Location;
                                objExp.Responsibility = experience.Responsibility;
                                if (!string.IsNullOrEmpty(experience.StartDate))
                                {
                                    experience.StartDate = DateTimeHelperService.ConvertBDDateStringToDateTimeObject(experience.StartDate);
                                    objExp.StartDate = Convert.ToDateTime(experience.StartDate).ToString();
                                }
                                if (!string.IsNullOrEmpty(experience.EndDate))
                                {
                                    experience.EndDate = DateTimeHelperService.ConvertBDDateStringToDateTimeObject(experience.EndDate);
                                    objExp.EndDate = Convert.ToDateTime(experience.EndDate).ToString();
                                }
                                objExp.LastUpdateBy = userId;
                                objExp.LastUpdateDate = DateTime.Now;
                                _db.SaveChanges();
                            }
                            else
                            {
                                HrEmpExperience hrEmpExperienceAdd = new HrEmpExperience();
                                hrEmpExperienceAdd.EmpHeaderId = obj.EmpHeaderId;
                                hrEmpExperienceAdd.ExperienceId = ExperienceId();
                                hrEmpExperienceAdd.CompanyName = experience.CompanyName;
                                hrEmpExperienceAdd.Designation = experience.Designation;
                                hrEmpExperienceAdd.Location = experience.Location;
                                hrEmpExperienceAdd.Responsibility = experience.Responsibility;
                                if (!string.IsNullOrEmpty(experience.StartDate))
                                {
                                    experience.StartDate = DateTimeHelperService.ConvertBDDateStringToDateTimeObject(experience.StartDate);
                                    hrEmpExperienceAdd.StartDate = Convert.ToDateTime(experience.StartDate).ToString();
                                }
                                if (!string.IsNullOrEmpty(experience.EndDate))
                                {
                                    experience.EndDate = DateTimeHelperService.ConvertBDDateStringToDateTimeObject(experience.EndDate);
                                    hrEmpExperienceAdd.EndDate = Convert.ToDateTime(experience.EndDate).ToString();
                                }
                                hrEmpExperienceAdd.CreatedBy = userId;
                                hrEmpExperienceAdd.CreationDate = DateTime.Now;
                                _db.HrEmpExperience.Add(hrEmpExperienceAdd);
                                _db.SaveChanges();
                            }
                        }
                    }
                }

                if (employeeEdit.vmEmployeeRefrences != null)
                {
                    if (employeeEdit.vmEmployeeRefrences.Count > 0)
                    {
                        foreach (var reference in employeeEdit.vmEmployeeRefrences)
                        {
                            var objRef = _db.HrEmployeeRefrence.FirstOrDefault(f => f.ReferenceId == reference.ReferenceId);
                            if (objRef != null)
                            {
                                objRef.ReferenceName = reference.ReferenceName;
                                objRef.Designation = reference.Designation;
                                objRef.Address1 = reference.Address1;
                                objRef.Address2 = reference.Address2;
                                objRef.Address3 = reference.Address3;
                                objRef.ResPhone = reference.ResPhone;
                                objRef.OffPhone = reference.OffPhone;
                                objRef.Mobile1 = reference.Mobile1;
                                objRef.Mobile2 = reference.Mobile2;
                                objRef.Email = reference.Email;
                                objRef.Relation = reference.Relation;
                                objRef.LastUpdateBy = userId;
                                objRef.LastUpdateDate = DateTime.Now;
                                _db.SaveChanges();
                            }
                            else
                            {
                                HrEmployeeRefrence hrEmployeeRefrenceAdd = new HrEmployeeRefrence();
                                hrEmployeeRefrenceAdd.EmpHeaderId = obj.EmpHeaderId;
                                hrEmployeeRefrenceAdd.ReferenceId = ReferenceId();
                                hrEmployeeRefrenceAdd.ReferenceName = reference.ReferenceName;
                                hrEmployeeRefrenceAdd.Designation = reference.Designation;
                                hrEmployeeRefrenceAdd.Address1 = reference.Address1;
                                hrEmployeeRefrenceAdd.Address2 = reference.Address2;
                                hrEmployeeRefrenceAdd.Address3 = reference.Address3;
                                hrEmployeeRefrenceAdd.ResPhone = reference.ResPhone;
                                hrEmployeeRefrenceAdd.OffPhone = reference.OffPhone;
                                hrEmployeeRefrenceAdd.Mobile1 = reference.Mobile1;
                                hrEmployeeRefrenceAdd.Mobile2 = reference.Mobile2;
                                hrEmployeeRefrenceAdd.Email = reference.Email;
                                hrEmployeeRefrenceAdd.Relation = reference.Relation;
                                hrEmployeeRefrenceAdd.CreatedBy = userId;
                                hrEmployeeRefrenceAdd.CreationDate = DateTime.Now;
                                _db.HrEmployeeRefrence.Add(hrEmployeeRefrenceAdd);
                                _db.SaveChanges();
                            }
                        }
                    }
                }

                #region Position data Update or payhead update
                if (employeeEdit.PositionId == null || obj.PositionId != employeeEdit.PositionId)
                {
                    //remove old payhead of this employee
                    var oldPayheads = _unitOfWork.HrEmployeePayHeadsRepo.Find(s => s.EmpHeaderId == obj.EmpHeaderId).ToList();
                    _unitOfWork.HrEmployeePayHeadsRepo.RemoveRange(oldPayheads);
                    _unitOfWork.SaveChanges();

                    //assign new payheads
                    var payRolleId = _unitOfWork.HrPositionsRepo.FirstOrDefault(s => s.PostionId == employeeEdit.PositionId) != null ? _unitOfWork.HrPositionsRepo.FirstOrDefault(s => s.PostionId == employeeEdit.PositionId).PayrollId : 0;

                    var thisPositionPayheads = _unitOfWork.HrPayRollPayHeadsRepo.Find(s => s.PayRollId == payRolleId).ToList();

                    foreach (var item in thisPositionPayheads)
                    {
                        var newrecord = new HrEmployeePayHead
                        {
                            EmployeePayHeadId = 0,
                            EmpHeaderId = employeeEdit.EmpHeaderId,
                            PayHeadId = item.PayHeadId,
                            Unit = item.Unit,
                            IsPercentage = item.IsPercentage,
                            CreationDate = DateTime.Now,
                            CreatedBy = userId
                        };
                        _unitOfWork.HrEmployeePayHeadsRepo.Add(newrecord);
                    }
                    _unitOfWork.SaveChanges();
                }
                #endregion

                if (employeeEdit.PositionId == null || obj.PositionId != employeeEdit.PositionId)
                    obj.PositionId = employeeEdit.PositionId;

                if (!string.IsNullOrWhiteSpace(employeeEdit.ImgUrl))
                    obj.ImgUrl = employeeEdit.ImgUrl;
                _unitOfWork.SaveChanges();

                #region OfficeTimeUpdate
                var oldTiming = obj.EmployeeOfficeTime.OrderByDescending(o => o.EmployeeOfficeTimeHeaderId).FirstOrDefault();
                if (oldTiming != null && (oldTiming.StartTime != employeeEdit.Attribute10 || oldTiming.EndTime != employeeEdit.Attribute11))
                {
                    var priviousDay = BdDateTime.Today().AddDays(-1).Date;
                    if (priviousDay < oldTiming.EffectiveFrom.Value.Date)
                    {
                        oldTiming.EffectiveFrom = BdDateTime.Today();
                        oldTiming.StartTime = employeeEdit.Attribute10;
                        oldTiming.EndTime = employeeEdit.Attribute11;
                        oldTiming.LastUpdateDate = BdDateTime.Now();
                        oldTiming.LastUpdateBy = userId;
                    }
                    else
                    {
                        oldTiming.EffectiveTo = BdDateTime.Today().AddDays(-1);
                        oldTiming.LastUpdateDate = BdDateTime.Now();
                        oldTiming.LastUpdateBy = userId;

                        var timing = new EmployeeOfficeTime
                        {
                            EmpHeaderId = obj.EmpHeaderId,
                            EmployeeOfficeTimeHeaderId = 0,
                            StartTime = employeeEdit.Attribute10,
                            EndTime = employeeEdit.Attribute11,
                            EffectiveFrom = BdDateTime.Today(),
                            CreatedBy = userId,
                            CreationDate = BdDateTime.Now(),
                            LastUpdateBy = userId,
                            LastUpdateDate = BdDateTime.Now()
                        };
                        _db.EmployeeOfficeTimes.Add(timing);
                    }
                    _db.SaveChanges();
                }
                #endregion
                return 1;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public void AddEmployeeTransaction(int HeadId)
        {
            var vmEmployee = Get(HeadId);
            HrEmployeeTransaction hrEmployeeTransaction = new HrEmployeeTransaction();
            hrEmployeeTransaction.ImgUrl = vmEmployee.ImgUrl;
            hrEmployeeTransaction.EmpHeaderId = HeadId;
            hrEmployeeTransaction.EmpFirstName = vmEmployee.EmpFirstName;
            hrEmployeeTransaction.EmpMiddleName = vmEmployee.EmpMiddleName;
            hrEmployeeTransaction.EmpLastName = vmEmployee.EmpLastName;
            hrEmployeeTransaction.EmpNickName = vmEmployee.EmpNickName;
            hrEmployeeTransaction.EmpId = vmEmployee.EmpId;
            hrEmployeeTransaction.GenderId = vmEmployee.GenderId;
            hrEmployeeTransaction.BirthDate = vmEmployee.BirthDate;
            hrEmployeeTransaction.Email = vmEmployee.Email;
            hrEmployeeTransaction.Email2 = vmEmployee.Email2;
            hrEmployeeTransaction.Mobile = vmEmployee.Mobile;
            hrEmployeeTransaction.Phone = vmEmployee.Phone;
            hrEmployeeTransaction.PunchCardId = vmEmployee.PunchCardId;

            hrEmployeeTransaction.PositionId = vmEmployee.PositionId;
            hrEmployeeTransaction.JobLocationId = vmEmployee.JobLocationId;
            hrEmployeeTransaction.BaseOfficeId = vmEmployee.BaseOfficeId;
            hrEmployeeTransaction.UserTypeId = vmEmployee.UserTypeId;
            hrEmployeeTransaction.WorkPhone = vmEmployee.WorkPhone;
            hrEmployeeTransaction.DeptId = vmEmployee.DepartmentId;
            hrEmployeeTransaction.DesigId = vmEmployee.DesignationId;
            hrEmployeeTransaction.JoiningDate = vmEmployee.JoinningDate;

            hrEmployeeTransaction.PresHoldingNo = vmEmployee.PresHoldingNo;
            hrEmployeeTransaction.PresStreet = vmEmployee.PresStreet;
            hrEmployeeTransaction.PresArea = vmEmployee.PresArea;
            hrEmployeeTransaction.PresPostCode = vmEmployee.PresPostCode;
            hrEmployeeTransaction.PresCountryId = vmEmployee.PresCountryId;
            hrEmployeeTransaction.PresDivisionId = vmEmployee.PresDivisionId;
            hrEmployeeTransaction.PresCityId = vmEmployee.PresCityId;
            hrEmployeeTransaction.PresThanaId = vmEmployee.PresThanaId;

            hrEmployeeTransaction.PermHoldingNo = vmEmployee.PermHoldingNo;
            hrEmployeeTransaction.PermStreet = vmEmployee.PermStreet;
            hrEmployeeTransaction.PermArea = vmEmployee.PermArea;
            hrEmployeeTransaction.PermPostCode = vmEmployee.PermPostCode;
            hrEmployeeTransaction.PermCountryId = vmEmployee.PermCountryId;
            hrEmployeeTransaction.PermDivisionId = vmEmployee.PermDivisionId;
            hrEmployeeTransaction.PermCityId = vmEmployee.PermCityId;
            hrEmployeeTransaction.PermThanaId = vmEmployee.PermThanaId;

            hrEmployeeTransaction.FatherFirstName = vmEmployee.FatherFirstName;
            hrEmployeeTransaction.FatherMiddleName = vmEmployee.FatherMiddleName;
            hrEmployeeTransaction.FatherLastName = vmEmployee.FatherLastName;
            hrEmployeeTransaction.FatherContactNo = vmEmployee.FatherContactNo;
            hrEmployeeTransaction.FatherNationalId = vmEmployee.FatherNationalId;

            hrEmployeeTransaction.MotherFirstName = vmEmployee.MotherFirstName;
            hrEmployeeTransaction.MotherMiddleName = vmEmployee.MotherMiddleName;
            hrEmployeeTransaction.MotherLastName = vmEmployee.MotherLastName;
            hrEmployeeTransaction.MotherContactNo = vmEmployee.MotherContactNo;
            hrEmployeeTransaction.MotherNationalId = vmEmployee.MotherNationalId;

            hrEmployeeTransaction.SpouseFirstName = vmEmployee.SpouseFirstName;
            hrEmployeeTransaction.SpouseMiddleName = vmEmployee.SpouseMiddleName;
            hrEmployeeTransaction.SpouseLastName = vmEmployee.SpouseLastName;
            hrEmployeeTransaction.SpouseContactNo = vmEmployee.SpouseContactNo;
            hrEmployeeTransaction.SpouseNationalId = vmEmployee.SpouseNationalId;

            hrEmployeeTransaction.BloodGroup = vmEmployee.BloodGroup;
            hrEmployeeTransaction.Religion = vmEmployee.Religion;
            hrEmployeeTransaction.Nationality = vmEmployee.Nationality;
            hrEmployeeTransaction.MaritalStatus = vmEmployee.MaritalStatus;
            hrEmployeeTransaction.NationalId = vmEmployee.NationalId;
            hrEmployeeTransaction.PassportNo = vmEmployee.PassportNo;
            hrEmployeeTransaction.BirthCountry = vmEmployee.BirthCountry;
            hrEmployeeTransaction.PlaceOfBirth = vmEmployee.PlaceOfBirth;
            hrEmployeeTransaction.LastMedicalCheckDate = vmEmployee.LastMedicalCheckDate;

            hrEmployeeTransaction.LastMedicalStatus = vmEmployee.LastMedicalStatus;
            hrEmployeeTransaction.MedicalHistory = vmEmployee.MedicalHistory;
            hrEmployeeTransaction.DrivingLicenseNo = vmEmployee.DrivingLicenseNo;

            hrEmployeeTransaction.StartTime = vmEmployee.StartTime;
            hrEmployeeTransaction.EndTime = vmEmployee.EndTime;
            hrEmployeeTransaction.UserName = vmEmployee.EmpId.Substring(3);

            hrEmployeeTransaction.CreatedBy = vmEmployee.CreatedBy;
            hrEmployeeTransaction.CreationDate = DateTime.Now;
            _unitOfWork.HrEmployeeTransactionRepo.Add(hrEmployeeTransaction);
            _unitOfWork.SaveChanges();
        }


        #region "Generated Id"
        public string GenerateEmpId()
        {
            var emp = _db.HrEmployee.OrderByDescending(o => o.EmpHeaderId).FirstOrDefault();
            if (emp != null)
            {
                var empId = emp.EmpId;
                if (!string.IsNullOrEmpty(empId))
                {
                    var epmIdSplit = empId.Split('P');
                    var splitedId = Convert.ToInt32(epmIdSplit[1]);
                    return "EMP" + (splitedId + 1).ToString("D4");
                }
            }
            return "EMP0001";
        }

        private int QualificationId()
        {
            int count = _db.HrEmpEduQuali.ToList().Count();
            int QId = 1;

            if (count > 0)
            {
                int QualificationId = _db.HrEmpEduQuali.OrderByDescending(o => o.EmpHeaderId).FirstOrDefault().QualificationId;
                int qualiId = QualificationId + 1;
                return qualiId;
            }
            else
            {
                return QId;
            }
        }

        private int ExperienceId()
        {
            int count = _db.HrEmpExperience.ToList().Count();
            int ExpId = 1;

            if (count > 0)
            {
                int experienceId = _db.HrEmpExperience.OrderByDescending(o => o.EmpHeaderId).FirstOrDefault().ExperienceId;
                int expId = experienceId + 1;
                return expId;
            }
            else
            {
                return ExpId;
            }
        }

        private int ReferenceId()
        {
            int count = _db.HrEmployeeRefrence.ToList().Count();
            int Refrence = 1;

            if (count > 0)
            {
                int referenceId = _db.HrEmployeeRefrence.OrderByDescending(o => o.EmpHeaderId).FirstOrDefault().ReferenceId;
                int refrence = referenceId + 1;
                return refrence;
            }
            else
            {
                return Refrence;
            }
        }
        #endregion

    }
}
