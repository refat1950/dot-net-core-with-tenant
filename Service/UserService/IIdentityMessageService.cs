﻿using Mandrill;
using Mandrill.Models;
using Mandrill.Requests.Messages;
using Services.EmailServices;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services.UserService
{
    /// <summary>
    ///     Expose a way to send messages (i.e. email/sms)
    /// </summary>
    public interface IIdentityMessageService
    {
        /// <summary>
        ///     This method should send the message
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        Task SendAsync(IdentityMessage message);
    }

    public class IdentityEmailService : IIdentityMessageService
    {
        private readonly Emailer emailer;

        public IdentityEmailService(Emailer emailer)
        {
            this.emailer = emailer;
        }
        public async Task SendAsync(IdentityMessage message)
        {
            await Task.Run(() => emailer.SendMail(message.Destination, message.Subject, message.Body));
        }
    }

    /// <summary>
    ///     Represents a message
    /// </summary>
    public class IdentityMessage
    {
        /// <summary>
        ///     Destination, i.e. To email, or SMS phone number
        /// </summary>
        public virtual string Destination { get; set; }

        /// <summary>
        ///     Subject
        /// </summary>
        public virtual string Subject { get; set; }

        /// <summary>
        ///     Message contents
        /// </summary>
        public virtual string Body { get; set; }
    }

    public class EmailService : IIdentityMessageService
    {
        private readonly MandrillApi _mandrill;
        private const string EmailFromAddress = "no-reply@mynotes.com";
        private const string EmailFromName = "My Notes";
        public EmailService()
        {
            _mandrill = new MandrillApi("2eb5e11f9cb8c9617b0f97975b5bf453-us3");
        }

        public Task SendAsync(IdentityMessage message)
        {
            SendMessageRequest sendMessageRequest = new SendMessageRequest(new EmailMessage
            {
                FromEmail = EmailFromAddress,
                FromName = EmailFromName,
                Subject = message.Subject,
                To = new List<Mandrill.Models.EmailAddress> { new EmailAddress(message.Destination) },
                Html = message.Body
            });
            var task = _mandrill.SendMessage(sendMessageRequest);

            return task;
        }

        //public Task SendWelcomeEmail(string firstName, string email)
        //{
        //    const string subject = "Welcome to My Notes";

        //    var emailMessage = new EmailMessage
        //    {
        //        from_email = EmailFromAddress,
        //        from_name = EmailFromName,
        //        subject = subject,
        //        to = new List<Mandrill.EmailAddress> { new EmailAddress(email) },
        //        merge = true,
        //    };

        //    emailMessage.AddGlobalVariable("subject", subject);
        //    emailMessage.AddGlobalVariable("first_name", firstName);

        //    var task = _mandrill.SendMessageAsync(emailMessage, "welcome-my-notes-saas", null);

        //    task.Wait();

        //    return task;
        //}

        //public Task SendResetPasswordEmail(string firstName, string email, string resetLink)
        //{
        //    const string subject = "Reset My Notes Password Request";

        //    var emailMessage = new EmailMessage
        //    {
        //        from_email = EmailFromAddress,
        //        from_name = EmailFromName,
        //        subject = subject,
        //        to = new List<Mandrill.EmailAddress> { new EmailAddress(email) }
        //    };
        //    emailMessage.AddGlobalVariable("subject", subject);
        //    emailMessage.AddGlobalVariable("FIRST_NAME", firstName);
        //    emailMessage.AddGlobalVariable("RESET_PASSWORD_LINK", resetLink);

        //    var task = _mandrill.SendMessageAsync(emailMessage, "reset-password-my-notes-saas", null);

        //    return task;
        //}

    }
}