﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model.Process
{
    public class ProcessQueryTableMailGroup : BaseEntity
    {
        [Key]
        public int QueryId { get; set; }
        public string AlertName { get; set; }
        public int MailGroupId { get; set; }
        public string Query { get; set; }
        public bool Status { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}
