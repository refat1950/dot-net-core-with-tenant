using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entity.Model
{
    public partial class FndMenu : BaseEntity
    {
        [Key]
        public int MenuId { get; set; }

        [Required(ErrorMessage = "Menu Name is required")]
        [Display(Name = "Menu Name")]
        public string MenuName { get; set; }

        [Required(ErrorMessage = "Menu Url is required")]
        [Display(Name = "Url")]
        public string Url { get; set; }

        [Range(0, int.MaxValue, ErrorMessage = "Sequence must be number")]
        [Required(ErrorMessage = "Menu Sequence Required")]
        [Display(Name = "Sequence")]
        public int Sequence { get; set; }

        [Display(Name = "Is Active")]
        public bool IsActive { get; set; }

        [Display(Name = "Parent Menu")]
        public int? ParentMenuId { get; set; }

        public int? RolesId { get; set; }

        public int? GroupId { get; set; }

        public virtual FndMenu Fndmenu { get; set; }
    }
}
