﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class ExamRoomInvestigator : BaseEntity
    {
        [Key]
        public int ExamRoomInvestigatorId { get; set; }
        public int SessionId { get; set; }
        public int ExamId { get; set; }
        public int RoomId { get; set; }
        public bool IsPublished { get; set; }
        public List<DateWiseInvestigator> DateWiseInvestigators { get; set; }

    }
}
