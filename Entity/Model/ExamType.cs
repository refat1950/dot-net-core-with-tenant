﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class ExamType : BaseEntity
    {
        [Key]
        public int ExamTypeId { get; set; }
        public string ExamTypeName { get; set; }
        public int? ParentExamTypeId { get; set; }
        //public bool? HasSubExamType { get; set; }
        public SubExamMarkBase? SubExamMarkBase { get; set; }
    }

    public enum SubExamMarkBase
    {
        Average = 1,
        Highest = 2
    }
}
