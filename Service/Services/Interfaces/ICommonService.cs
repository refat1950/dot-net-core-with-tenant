﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Services;
using Entity.Model;
using Services.View_Model;

namespace Services.Interfaces
{
   public interface ICommonService
    {
       int GetActiveSession();
       IEnumerable<VMSection> GetSectionByClassId(int classId, int teacherId,int examId, bool isAdmin);
       IQueryable<ClassSection> GetSectionByClassId(int classId);
       List<VMClass> GetClassListByTeacherId(int teacherId,bool isAdmin);
       List<VMShift> GetShiftByClassSectionId(int classId, int sectionId, int teacherId, bool isAdmin);
       List<VMSubject> GetSubjectsByClassTeacherId(int teacherId, int classId, int sectionId, bool isAdmin);
       List<VMSubjectSubgroupClassSection> GetSubgroupByClassSectionSubjectId(int classId, int subjectId, int examId, int? teacherId,bool isAdmimn);
       List<VMEvent> GetPlannerEventsByTypeId(int typeId);
       Session GetCurrentSession();
       HrEmployee GetCurrentEmployee(int employeeId);
       List<VMClass> GetClassListFromClassSubjectTeacher(int teacherId, bool isAdmin);
       List<VMClass> GetClassListFromSubjectSubgroupClassSection(int teacherId, bool isAdmin);
       //string GetFlexValuesBySetId(int setId);
    }
}
