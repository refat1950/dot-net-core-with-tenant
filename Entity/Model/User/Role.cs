﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class Role : BaseEntity, IRole
    {
        [Key]
        public int RoleId { get; set; }
        [Required]
        public string RoleName { get; set; }
        public string Description { get; set; }
        public int? Status { get; set; }
        public string RedirectionUrl { get; set; }
        public RoleType? RoleType { get; set; }
        //public virtual ICollection<UserRole<Tkey>> Users { get; }

    }

    public enum RoleType
    {
        Default = 1,
        Editable = 2
    }

}
