﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
   public class ModelStateWrapper:IValidationDictionary
    {
        private ModelStateDictionary _modelState;

        public ModelStateWrapper()
        {
            _modelState = new ModelStateDictionary(); //modelState;
        }

        #region IValidationDictionary Members

        public void AddError(string key, string errorMessage)
        {
            _modelState.AddModelError(key, errorMessage);
        }

        public bool IsValid
        {
            get { return _modelState.IsValid; }
        }
        #endregion
    }
}
