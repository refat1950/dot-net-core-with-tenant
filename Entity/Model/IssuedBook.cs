using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entity.Model
{
    public class IssuedBook : BaseEntity
    {
        [Key]
        public int IssuedBookId { get; set; }
        public int BookId { get; set; }
        public int AuthorId { get; set; }
        public int UserType { get; set; }
        public int UserReferenceId { get; set; }
        public DateTime IssueDate { get; set; }
        public DateTime DueDate { get; set; }
        public DateTime? ReturnDate { get; set; }
        public IssueStatus IssueStatus { get; set; }
        public string Remarks { get; set; }
        public decimal? DailyFineAmount { get; set; }
        public decimal? TotalFineAmount { get; set; }
        public virtual Book Book { get; set; }
        //public virtual BookAuthor BookAuthor { get; set; }
    }

    public enum IssueStatus
    {
        Renew = 1,
        Return = 2
    }
}
