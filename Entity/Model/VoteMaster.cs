using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entity.Model
{
    public partial class VoteMaster : BaseEntity
    {
        [Key]
        public int VoteId { get; set; }

        [StringLength(100)]
        public string VoteDescription { get; set; }

        public int? ProjectId { get; set; }

        public int? VoteFor { get; set; }

        public int? PassPct { get; set; }

        public int? AvgPct { get; set; }

        public int? SelectedOptionId { get; set; }

        [Column(TypeName = "date")]
        public DateTime? StartDate { get; set; }

        [Column(TypeName = "date")]
        public DateTime? EndDate { get; set; }

        public int? Status { get; set; }
    }
}
