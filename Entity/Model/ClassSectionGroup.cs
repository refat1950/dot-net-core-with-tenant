using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Entity.Model
{
    public partial class ClassSectionGroupTeacherMapping : BaseEntity
    {
        [Key]
        public int ClassSectionGroupId { get; set; }
        public int ClassId { get; set; }
        public int SectionId { get; set; }
        public int? SessionId { get; set; }
        public int SubjectId { get; set; }
        public int? TeacherId { get; set; }
        public virtual HrEmployee HrEmployee { get; set; }
    }
}
