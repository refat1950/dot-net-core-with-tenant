﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class HrPaySlipTransactionMasterLogHistory : BaseEntity
    {
        [Key]
        public int HrPaySlipTransactionMasterLogId { get; set; }
        public int PaySlipMasterHeaderId { get; set; } // for relation with master table
        public int? PeriodId { get; set; }
        public int? EmpHeaderId { get; set; }
        public int PositionId { get; set; }
        public int PayrollId { get; set; }
        public decimal TotalAmountPayable { get; set; }
        public decimal TotalAdditionAmount { get; set; }
        public decimal TotalDeductionAmount { get; set; }
        public PaidStatus PaidStatus { get; set; }
        public PayslipCurrentStatus PayslipCurrentStatus { get; set; }
        public string PayslipId { get; set; }
        public DateTime GenerationDate { get; set; }
        public virtual HrPaySlipTransactionMaster HrPaySlipTransactionMaster { get; set; }
        public virtual ICollection<HrPaySlipTransactionLogHistory> HrPaySlipTransactionLogHistories { get; set; } // this log's payheads
    }
}
