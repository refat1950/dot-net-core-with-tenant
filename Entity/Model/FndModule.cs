using System;
using System.ComponentModel.DataAnnotations;

namespace Entity.Model
{
    public partial class FndModule : BaseEntity
    {
        [Key]
        public int ModuleId { get; set; }

        [Required]
        [StringLength(100)]
        public string ModuleName { get; set; }

        [StringLength(20)]
        public string ModuleShortName { get; set; }
    }
}
