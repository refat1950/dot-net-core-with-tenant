﻿using Entity.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Repository.Context;
using Services.UserService;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Web.Models;

namespace Web.Controllers
{
    public class HomeController : Controller
    {
        private UserManager<User> UserManager;
        private readonly ILogger<HomeController> _logger;
        private ProjectDbContext db;
        public HomeController(ILogger<HomeController> logger, ProjectDbContext db, UserManager<User> UserManager)
        {
            _logger = logger;
            this.db = db;
            this.UserManager = UserManager;
        }

        public IActionResult Index()
        {
            var clas = db.Class.FirstOrDefault() != null ? db.Class.FirstOrDefault().ClassName : "";

            var asasas = UserManager.FindByName("");
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()    
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
