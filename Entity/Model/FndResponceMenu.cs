using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entity.Model
{
    public partial class FndResponceMenu : BaseEntity
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ResponId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int MenuId { get; set; }

    }
}
