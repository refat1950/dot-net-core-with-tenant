﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class HrPaySlipTransaction : BaseEntity
    {
        [Key]
        public int PaySlipTransactionHeaderId { get; set; }
        public int PayHeadId { get; set; }
        public decimal Unit { get; set; }
        public decimal CalculatedValue { get; set; }
        public bool IsPercentage { get; set; }
        public int PaySlipMasterHeaderId { get; set; }
        public virtual HrPaySlipTransactionMaster HrPaySlipTransactionMaster { get; set; }
    }

    //Payhead Type stored in Attribute3
}
