﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class LeaveSetup : BaseEntity
    {
        [Key]
        public int LeaveId { get; set; }
        public int LeaveTypeId { get; set; }
        public string LeaveDesc { get; set; }
        public int Designation { get; set; }
        public int NoOfMonth { get; set; }
        public string EntitleLeave { get; set; }
        public decimal LeaveBalance { get; set; }
    }
}
