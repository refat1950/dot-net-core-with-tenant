﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entity.Model
{
    public partial class FndMenuGroup : BaseEntity
    {
        [Key]
        public int MenuGroupId { get; set; }
        public int GroupSerial { get; set; }
        public string GroupName { get; set; }
        public int? Status { get; set; }
    }
}
