﻿using Service.ViewModel.Payroll;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Services.Interfaces
{
    public interface IPaySlipLateFee
    {
        CalculatedFee GetLateFee(int empHeaderId,int periodId);
    }
}
