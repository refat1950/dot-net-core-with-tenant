﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class EmployeeOfficeTime : BaseEntity
    {
        [Key]
        public int EmployeeOfficeTimeHeaderId { get; set; }
        public int EmpHeaderId { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
    }
}
