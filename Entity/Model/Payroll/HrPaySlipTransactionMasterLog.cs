﻿using Entity.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class HrPaySlipTransactionMasterLog : BaseEntity
    {
        [Key]
        public int HrPaySlipTransactionMasterLogId { get; set; }
        public int PaySlipMasterHeaderId { get; set; } // for relation with master table
        public int? PeriodId { get; set; }
        public int? EmpHeaderId { get; set; }
        public int PositionId { get; set; }
        public int PayrollId { get; set; }
        public decimal TotalAmountPayable { get; set; } //net saraly
        public decimal TotalAdditionAmount { get; set; }
        public decimal TotalDeductionAmount { get; set; }
        public PaidStatus PaidStatus { get; set; }
        public PayslipCurrentStatus PayslipCurrentStatus { get; set; }
        public string PayslipId { get; set; }
        public DateTime GenerationDate { get; set; }
        public virtual HrPaySlipTransactionMaster HrPaySlipTransactionMaster { get; set; }
        public virtual ICollection<HrPaySlipTransactionLog> HrPaySlipTransactionLog { get; set; } // this log's payheads
        public virtual ICollection<HrPaySlipTransactionLogAfterPayheadChange> HrPaySlipTransactionLogAfterPayheadChanges { get; set; } //for a previously generated period that is unpaid, payhead amount mab be need to change for that period when a pay slip needs regenerate, the payhead amount edit is available in Payslip manager at view_more section
    }
}
