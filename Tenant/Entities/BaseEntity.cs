﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tenant.Entities
{
    public class BaseEntity
    {
        /// <summary>
        /// ATTRIBUTEs 
        /// </summary>
        [StringLength(250)]
        public string Attribute1 { get; set; }
        [StringLength(250)]
        public string Attribute2 { get; set; }
        [StringLength(250)]
        public string Attribute3 { get; set; }
        [StringLength(250)]
        public string Attribute4 { get; set; }
        [StringLength(250)]
        public string Attribute5 { get; set; }
        [StringLength(250)]
        public string Attribute6 { get; set; }

        public long? CreatedBy { get; set; }


        /// <summary>
        /// WHO Column
        /// </summary>
        [DataType(DataType.Date, ErrorMessage = "Date format should be MM/DD/YYYY")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? CreationDate { get; set; }


        public long? LastUpdateBy { get; set; }

        [DataType(DataType.Date, ErrorMessage = "Date format should be MM/DD/YYYY")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? LastUpdateDate { get; set; }



        /// <summary>
        /// EFFECTIVE Column
        /// </summary>
        [DataType(DataType.Date, ErrorMessage = "Date format should be MM/DD/YYYY")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "Active From")]
        public DateTime? EffectiveFrom { get; set; }


        [DataType(DataType.Date, ErrorMessage = "Date format should be MM/DD/YYYY")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "Active To")]
        public DateTime? EffectiveTo { get; set; }
    }
}
