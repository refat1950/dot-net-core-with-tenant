﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Entity.Model;
using Repository.Context;
using Repository.Provider;
using Services.Interfaces;
using Services.View_Model;
using data.UnitOfWork;
using Fluentx;

namespace Services
{
    public class RoutineService : IRoutineService
    {
        private IUnitOfWork _unitOfWork;
    

        private List<PlannerEventDetails> _holidayEventDetls;
        private List<ClassRoutine> _classPeriodRoutine;
        private List<ClassRoutine> _classSubjectRoutine;

        public RoutineService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;

        }

        public ReturnGenerateRoutineView GenerateRoutine(GenerateRoutineView formdata)
        {
            var semester = _unitOfWork.PlannerEventDetailsRepo.FirstOrDefault(f => f.SessionId == formdata.SessionId && f.EventId == formdata.SemesterId);

            var periodFlexSet = _unitOfWork.FlexSetRepo.FirstOrDefault(s => s.FlexValueShortName == "period");
            var periodFlexs = _unitOfWork.FlexValueRepo.Find(s => s.FlexValueSetId == periodFlexSet.FlexValueSetId);


            var bssClass = _unitOfWork.ClassRepo.FirstOrDefault(f => f.ClassId == formdata.ClassId);
            var bssSection = _unitOfWork.SectionRepo.FirstOrDefault(f => f.SectionId == formdata.SectionId);
            var period = periodFlexs.FirstOrDefault(f => f.FlexValueId == formdata.PeriodId);


            var holidayShortCode = new[] { "HOLI", "EXAM", "SCRIC", "Report card" };

            var holidaysEventTypes = _unitOfWork.PlannerEventTypeRepo.Find(w => holidayShortCode.Contains(w.ShortName));
            var holidaysEventTypesIds = holidaysEventTypes.Select(s => s.EventTypeId).ToArray();
            var holidayEvents = _unitOfWork.PlannerEventRepo.Find(w => holidaysEventTypesIds.Contains(w.EventTypeId.Value));
            var holidayEventsIds = holidayEvents.Select(s => s.EventId).ToArray();
            _holidayEventDetls = _unitOfWork.PlannerEventDetailsRepo.Find(w => w.SessionId == formdata.SessionId && holidayEventsIds.Contains(w.EventId)).ToList();

            var startdate = DateTime.ParseExact(formdata.StartDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

            // ReSharper disable once ConvertConditionalTernaryToNullCoalescing
            var endDateString = formdata.EndDate != null ? formdata.EndDate : DateTime.Today.ToString("dd/MM/yyyy").Replace("-", "/");
            var enddate = DateTime.ParseExact(endDateString, "dd/MM/yyyy", CultureInfo.InvariantCulture);

            var genericClassRoutine =

                _unitOfWork.ClassRoutineRepo.Find(w => w.ClassId == formdata.ClassId && w.SessionId == formdata.SectionId && w.SessionId == formdata.SessionId && w.Shift == formdata.Shift);


            _classPeriodRoutine = genericClassRoutine.Where(w => w.Period == formdata.PeriodId).ToList();
            _classSubjectRoutine = genericClassRoutine.Where(w => w.SubjectId == formdata.SubjectId).ToList();

            var routineSetup = _unitOfWork.RoutineSetupRepo
                                    .FirstOrDefault(w => w.ClassId == formdata.ClassId && w.SectionId == formdata.SectionId.ToString() && w.Period == formdata.PeriodId.ToString());

            var dayNames = _unitOfWork.PlannerDayRepo.Find(w => formdata.Day.Contains(w.DAY_ID)).Select(s => s.DayName.ToUpper()).ToArray();


            var routinedates = new List<Routinedates>();
            var excludeRoutineDates = new List<ExcludeRoutineDates>();
            if (bssClass != null && bssSection != null && routineSetup != null && period != null)
                if (formdata.DayCount > 0)
                {
                    var dayCount = formdata.DayCount;
                    var dayLoop = 0;
                    if (dayCount > 0 && enddate == DateTime.Today)
                    {
                        var isDone = true;
                        var date = startdate;
                        do
                        {
                            if (dayNames.Contains(date.ToString("dddd").ToUpper()))
                            {
                                var result = IsValidDate(date);
                                if (result.IsValid)
                                {
                                    routinedates.Add(new Routinedates
                                    {
                                        Date = date.ToString("dd/MM/yyyy"),
                                        DateName = date.ToString("dddd"),
                                        Class = bssClass.ClassName,
                                        Section = bssSection.SectionName,
                                        StartTime = routineSetup.StartTime,
                                        EndTime = routineSetup.EndTime,
                                        Period = period.FlexValue
                                    });
                                    dayLoop++;
                                }
                                else
                                {
                                    excludeRoutineDates.Add(new ExcludeRoutineDates
                                    {
                                        Date = date.ToString("dd/MM/yyyy"),
                                        DateName = date.ToString("dddd"),
                                        Reason = result.InValidReson
                                    });
                                }

                            }

                            date = date.AddDays(1);
                            if (dayCount <= dayLoop)
                            {
                                isDone = false;
                            }
                        } while (semester != null && (isDone && semester.EndDate >= date));
                    }
                }
                else
                {
                    for (var date = startdate; date <= enddate; date = date.AddDays(1))
                    {
                        if (dayNames.Contains(date.ToString("dddd").ToUpper()))
                        {
                            var result = IsValidDate(date);
                            if (result.IsValid)
                            {
                                routinedates.Add(new Routinedates
                                {
                                    Date = date.ToString("dd/MM/yyyy"),
                                    DateName = date.ToString("dddd"),
                                    Class = bssClass.ClassName,
                                    Section = bssSection.SectionName,
                                    StartTime = routineSetup.StartTime,
                                    EndTime = routineSetup.EndTime,
                                    Period = period.FlexValue
                                });
                            }
                            else
                            {
                                excludeRoutineDates.Add(new ExcludeRoutineDates
                                {
                                    Date = date.ToString("dd/MM/yyyy"),
                                    DateName = date.ToString("dddd"),
                                    Reason = result.InValidReson
                                });
                            }

                        }
                    }
                }

            return new ReturnGenerateRoutineView { Routinedates = routinedates, ExcludeDate = excludeRoutineDates, ValidDateCount = routinedates.Count };
        }

        public ReturnGenerateRoutineView SaveRoutine(GenerateRoutineView formdata, int userId)
        {
            try
            {
                var routineDates = GenerateRoutine(formdata);

                var campusId = 0;
                var bssClassSection = _unitOfWork.ClassSectionRepo.FirstOrDefault(f => f.ClassId == formdata.ClassId && f.SectionId == formdata.SectionId && f.Shift == formdata.Shift);

                if (bssClassSection != null)
                    if (bssClassSection.CampusId != null)
                        campusId = bssClassSection.CampusId.Value;


                var previousClass =
                    _unitOfWork.ClassRoutineRepo.Find(w =>
                           w.SessionId == formdata.SessionId && w.ClassId == formdata.ClassId &&
                           w.SessionId == formdata.SectionId && w.SubjectId == formdata.SubjectId);
                var classCount = previousClass.Count();

                foreach (var data in routineDates.Routinedates)
                {
                    var date = DateTime.ParseExact(data.Date.Replace("-", "/"), "dd/MM/yyyy",
                        CultureInfo.InvariantCulture);

                    var isExist = _unitOfWork.ClassRoutineRepo.FirstOrDefault(f => f.EventId == formdata.SemesterId &&
                                                                       f.Shift == formdata.Shift &&
                                                                       f.SectionId == formdata.SectionId &&
                                                                       f.CampusId == campusId &&
                                                                       //f.SubjectId == formdata.SubjectId &&
                                                                       f.ClassId == formdata.ClassId &&
                                                                       f.Period == formdata.PeriodId &&
                                                                       f.RoutineDate == date &&
                                                                       f.SessionId == formdata.SessionId
                                                                       //f.SubgroupId == formdata.SubjectSubgroupId
                                                                       );


                    if (isExist != null)
                    {
                        isExist.EventId = formdata.SemesterId;
                        isExist.Shift = formdata.Shift;
                        isExist.SectionId = formdata.SectionId;
                        isExist.CampusId = campusId;
                        isExist.SubjectId = formdata.SubjectId;
                        isExist.ClassId = formdata.ClassId;
                        isExist.Period = formdata.PeriodId;
                        isExist.BookId = formdata.BookId;
                        isExist.RoutineDate = date;//DateTime.ParseExact(data.Date.Replace("-", "/"), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                        isExist.TeacherId = formdata.TeacherId;
                        isExist.SessionId = formdata.SessionId;
                        isExist.IsActive = true;
                        //isExist.ClassCount = classCount;
                        isExist.SubgroupId = formdata.SubjectSubgroupId;
                        isExist.LastUpdateBy = userId;
                        isExist.LastUpdateDate = DateTime.Today;
                    }
                    else
                    {
                        classCount++;
                        _unitOfWork.ClassRoutineRepo.Add(new ClassRoutine
                        {
                            EventId = formdata.SemesterId,
                            Shift = formdata.Shift,
                            SectionId = formdata.SectionId,
                            CampusId = campusId,
                            SubjectId = formdata.SubjectId,
                            ClassId = formdata.ClassId,
                            Period = formdata.PeriodId,
                            BookId = formdata.BookId,
                            RoutineDate = date,//DateTime.ParseExact(data.Date.Replace("-", "/"), "dd/MM/yyyy", CultureInfo.InvariantCulture),
                            TeacherId = formdata.TeacherId,
                            SessionId = formdata.SessionId,
                            IsActive = true,
                            ClassCount = classCount,
                            SubgroupId = formdata.SubjectSubgroupId,
                            CreatedBy = userId,
                            CreationDate = DateTime.Today
                        });
                    }
                }

                _unitOfWork.SaveChanges();

                return new ReturnGenerateRoutineView
                {
                    Routinedates = routineDates.Routinedates,
                    ExcludeDate = routineDates.ExcludeDate,
                    ValidDateCount = routineDates.ValidDateCount
                };
            }
            catch (Exception ex)
            {
                return new ReturnGenerateRoutineView();
            }
        }

        private IsValidDateViewModel IsValidDate(DateTime date)
        {
            var re = new IsValidDateViewModel();

            var isPlannerHoliday = _holidayEventDetls.Any(a => a.StartDate <= date && date <= a.EndDate);
            var plannerEventDtls = _holidayEventDetls.FirstOrDefault(a => a.StartDate <= date && date <= a.EndDate);

            if (isPlannerHoliday)
            {
                re.IsValid = false;
                re.InValidReson = plannerEventDtls != null ? plannerEventDtls.PlannerEvent.EventName : "";
            }
            //else if (_classPeriodRoutine.Any(a => a.RoutineDate == date))
            //{
            //    re.IsValid = false;
            //    re.InValidReson = "Class Exist on period";
            //}
            //else if (_classSubjectRoutine.Any(a => a.RoutineDate == date))
            //{
            //    re.IsValid = false;
            //    re.InValidReson = "Subject Exist on this Day";
            //}
            else
            {
                re.IsValid = true;
            }

            return re;
        }

        public List<ExamClassSubjects> GenerateExamRoutine(ExamRoutineGen formdata)
        {
            var sessionId = formdata.SessionId;
            var classId = formdata.ClassId;
            var sectionId = formdata.SectionId;
            var examId = formdata.ExamId;

            var exestedRoutine = _unitOfWork.EventRoutineRepo.Find(w => w.SessionId == sessionId &&
                                                   w.ClassId == classId &&
                                                   w.EventDtlsId == examId &&
                                                   (formdata.SectionId == 0 || w.SectionId == sectionId));

            var i = _unitOfWork.ClassSubjectRepo.Find(w => w.ClassId == classId)
                                            .OrderByDescending(o => o.Subject.Major).ThenByDescending(t => t.SubjectId)
                                            .AsEnumerable()
                                            .Select(s =>
                                            {
                                                var bssEventRoutine = exestedRoutine.FirstOrDefault(f=>f.ClassId == s.ClassId && f.SubjectId == s.SubjectId);
                                                var examDate = bssEventRoutine != null && bssEventRoutine.StartDate != null ? bssEventRoutine.StartDate.Value.ToString("MM/dd/yyyy").Replace("-","/") : "";
                                                var startTime = bssEventRoutine != null && bssEventRoutine.StartDate != null ? bssEventRoutine.StartDate.Value.ToString("hh:mm tt") : "";
                                                var endTime = bssEventRoutine != null && bssEventRoutine.EndDate != null ? bssEventRoutine.EndDate.Value.ToString("hh:mm tt") : "";
                                                return new ExamClassSubjects
                                                {
                                                    SubjectId = s.SubjectId,
                                                    SubjectName = s.Subject.SubjectName,
                                                    ExamDate = examDate,
                                                    StartTime = startTime,
                                                    EndTime = endTime
                                                };
                                            }).Where(w=> w != null);
            return i.ToList();
        }

        public string ExamRoutineSave(ExamRoutineSave examRoutine, int userId)
        {
            var msg = string.Empty;
            var ClassId = examRoutine.ClassId;

            var exestedRoutine = _unitOfWork.EventRoutineRepo.Find(w => w.SessionId == examRoutine.SessionId &&
                                                   w.ClassId == ClassId &&
                                                   w.EventDtlsId == examRoutine.ExamId &&
                                                   (examRoutine.SectionId == 0 || w.SectionId == examRoutine.SectionId));

            foreach (var routine in examRoutine.ExamClassSubject.Where(w=> w!= null))
            {
                var stringStartDate = routine.ExamDate + " " + routine.StartTime;
                var stringEndDate = routine.ExamDate + " " + routine.EndTime;


                DateTime startDate;
                DateTime.TryParseExact(stringStartDate, "MM/dd/yyyy hh:mm tt", CultureInfo.InvariantCulture, DateTimeStyles.None, out startDate);

                DateTime endDate;
                DateTime.TryParseExact(stringEndDate, "MM/dd/yyyy hh:mm tt", CultureInfo.InvariantCulture, DateTimeStyles.None, out endDate);

                if (exestedRoutine.Any(a => a.SubjectId == routine.SubjectId))
                {
                    var subjectId = routine.SubjectId;
                    exestedRoutine.Where(a => a.SubjectId == subjectId).ForEach(f =>
                    {
                        f.StartDate = startDate;
                        f.EndDate = endDate;
                        f.LastUpdateBy = userId;
                        f.LastUpdateDate = DateTime.Now;
                    });
                }
                else
                {
                    var sectionId = examRoutine.SectionId;
                    int[] sectionIds;
                    if (sectionId == 0)
                    {
                        sectionIds = _unitOfWork.ClassSectionRepo.Find(w => w.ClassId == ClassId).Select(s => s.SectionId).ToArray();
                    }
                    else
                    {
                        sectionIds = new [] { sectionId };
                    }

                    foreach (var id in sectionIds)
                    {
                        _unitOfWork.EventRoutineRepo.Add(new EventRoutine
                        {
                            StartDate = startDate,
                            EndDate = endDate,
                            SessionId = examRoutine.SessionId,
                            ClassId = examRoutine.ClassId,
                            SectionId = id,
                            EventDtlsId = examRoutine.ExamId,
                            SubjectId = routine.SubjectId,
                            CreatedBy = userId,
                            CreationDate = DateTime.Now
                        });
                    }
                }
            }
            _unitOfWork.SaveChanges();
            return msg = "Done";
        }


        public long GetRoutineAjaxCount(string sSearch, string startDate, string endDate, int eventId, int shiftId, int campusId, int sectionId, int classId, int subjectId)
        {
            var routine = GetFilteredRoutine(sSearch, startDate, endDate, eventId, shiftId, campusId, sectionId, classId, subjectId);

            var allCount = routine.Count();
            return allCount;
        }
        public IEnumerable<string[]> GetRoutineAjax(string sSearch, int iDisplayStart, int iDisplayLength, string startDate, string endDate, int eventId, int shiftId, int campusId, int sectionId, int classId, int subjectId)
        {
            var routine = GetFilteredRoutine(sSearch, startDate, endDate, eventId, shiftId, campusId, sectionId, classId, subjectId);

            routine = routine.OrderBy(o => new { o.RoutineDate, o.Period });

            var routineSetuo = _unitOfWork.RoutineSetupRepo;

            var filterRoutine = routine.Skip(iDisplayStart).Take(iDisplayLength).AsEnumerable().Select(s =>
            {

                var semester = _unitOfWork.PlannerEventRepo.FirstOrDefault(f => f.EventId == s.EventId);
                var routinedate = s.RoutineDate.ToString("dd/MMM/yyyy");
                var subjectName = _unitOfWork.SubjectRepo.FirstOrDefault(f => f.SubjectId == s.SubjectId);
                var section = _unitOfWork.SectionRepo.FirstOrDefault(f => f.SectionId == s.SectionId);
                var shift = _unitOfWork.FlexValueRepo.FirstOrDefault(f => f.FlexValueId == s.Shift);
                var Class = _unitOfWork.ClassRepo.FirstOrDefault(f => f.ClassId == s.ClassId);

                var period = _unitOfWork.FlexValueRepo.FirstOrDefault(f => f.FlexValueId == s.Period);
                var i = routineSetuo.FirstOrDefault(w => w.ClassId == s.ClassId && w.SectionId == s.SectionId.ToString() && w.Period == s.Period.ToString());

                var displayTime = i != null ? i.StartTime + "-" + i.EndTime : "";

                var campus = _unitOfWork.CampusRepo.FirstOrDefault(f => f.CampusId == s.CampusId);

                return new
                {
                    semester = semester != null ? semester.EventName : "",
                    date = routinedate,
                    Subject = subjectName != null ? subjectName.SubjectName : "",
                    Class = Class != null ? Class.ClassName : "",
                    Section = section != null ? section.SectionName : "",
                    Shift = shift != null ? shift.FlexValue : "",
                    ClassNo = s.ClassCount.ToString(),
                    period = period != null ? period.FlexValue : "",
                    classTime = displayTime,
                    campus = campus != null ? campus.CampusName : ""
                };
            });

            var result = from c in filterRoutine.ToList()
                         select new[]
                        {
                            c.semester,
                            c.date,
                            c.period,
                            c.classTime,
                            c.Subject,
                            c.Class,
                            c.Section,
                            c.Shift,
                            c.campus,
                            c.ClassNo
                        };

            return result;
        }

        private IQueryable<ClassRoutine> GetFilteredRoutine(string sSearch, string startDate, string endDate, int eventId, int shiftId, int campusId, int sectionId, int classId, int subjectId)
        {
            var sessionId = 0;
            var session = _unitOfWork.SessionRepo.FirstOrDefault(f => f.Status == true);
            if (session != null)
            {
                sessionId = session.SessionId;
            }

            DateTime sstartDate;
            DateTime.TryParseExact(startDate, "MM/dd/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out sstartDate);

            DateTime sendDate;
            DateTime.TryParseExact(endDate, "MM/dd/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out sendDate);
            if (sstartDate.Year == 1)
            {
                sstartDate = DateTime.Today;
            }


            var routine = _unitOfWork.ClassRoutineRepo.Find(w => w.RoutineDate >= sstartDate && w.SessionId == sessionId).AsQueryable();

            if (sendDate.Year != 1)
            {
                routine = routine.Where(w => w.RoutineDate <= sendDate);
            }
            if (eventId != 0)
            {
                routine = routine.Where(w => w.EventId == eventId);
            }
            if (shiftId != 0)
            {
                routine = routine.Where(w => w.Shift == shiftId);
            }
            if (campusId != 0)
            {
                routine = routine.Where(w => w.CampusId == campusId);
            }
            if (sectionId != 0)
            {
                routine = routine.Where(w => w.SectionId == sectionId);
            }
            if (classId != 0)
            {
                routine = routine.Where(w => w.ClassId == classId);
            }
            if (subjectId != 0)
            {
                routine = routine.Where(w => w.SubjectId == subjectId);
            }
            if (!string.IsNullOrEmpty(sSearch))
            {
                //routine = routine.Where(w => w.SubjectId == subjectId);
            }

            return routine;
        }
    }

    public class IsValidDateViewModel
    {
        public bool IsValid { get; set; }
        public string InValidReson { get; set; }
    }
}
