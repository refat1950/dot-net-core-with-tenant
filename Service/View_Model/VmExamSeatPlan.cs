﻿using Entity.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Web.Models.View_Model;

namespace Service.View_Model
{
    public class VmExamSeatPlan
    {
        public int ExamScheduleId { get; set; }
        public List<VmRoomList> vmRoomLists { get; set; }
        public List<VmExamScheduleLine> vmExamScheduleLines { get; set; }
    }

    public class VmRoomList
    {
        public bool isExamSeatSetup { get; set; }
        public int RoomId { get; set; }
        public string RoomName { get; set; }
    }

    public class VmSeatDetails
    {
        public List<int> SubjectId { get; set; }
        public int ExamScheduleLineId { get; set; }
        public int RollNumber { get; set; }
        public bool isDateAssigned { get; set; }
    }

    public class VmSaveSeat
    {
        public string DateWiseSubjectIds { get; set; }
        public int RollNumber { get; set; }
        public int ClassRoomColumnRowId { get; set; }
        public int ExamScheduleLineId { get; set; }
        public int RoomSetupHeaderId { get; set; }
    }

    public class VmStudentSeat
    {
        public int? RoomId { get; set; }
        public int ExamScheduleId { get; set; }
        public int ExamScheduleLineId { get; set; }
    }


    public class VmRoomSeatPlan
    {
        public int RoomId { get; set; }
        public string RoomName { get; set; }
        public int SeatCapacityLeft { get; set; }
        public List<VmRoomColumn> vmRoomColumns { get; set; }
        public List<VmRoomColumnRow> vmRoomColumnRows { get; set; }
        public List<VmSaveSeat> vmSaveSeat { get; set; }
    }

    public class VmRoomColumn
    {
        public int RoomSetupHeaderId { get; set; }
        public int ColumnId { get; set; }
        public int RoomId { get; set; }
        public int ColumnSerial { get; set; }
        public int NoOfRow { get; set; }
        public bool DefaultLayout { get; set; }
        public int SeatCapacityLeft { get; set; }
    }
    public class VmRoomColumnRow
    {
        public int StudentHeaderId { get; set; }
        public int ClassRoomColumnRowId { get; set; }
        public int ColumnId { get; set; }
        public int RowSerial { get; set; }
        public int NoOfSeat { get; set; }
        public int SeatSerial { get; set; }
        public SeatStatus Status { get; set; }
        public string DateWiseSubjectIds { get; set; }
        public int ClassId { get; set; }
        public string ClassName { get; set; }
        public int SectionId { get; set; }
        public string SectionName { get; set; }
        public int RollNumber { get; set; }
        public int ExamScheduleId { get; set; }
        public int ExamScheduleLineId { get; set; }
    }

}